<?php
return array(
    'modules' => array(
        'Manager',
        'Estrutura',
        'Gerador',
        'Site',
        'Acl',
        'Tribunal',

        /*LIBRARY*/
        'DOMPDFModule',
    ),
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor',
            './vendor/core',
        ),
        'config_glob_paths' => array(
                'config/autoload/{,*.}{global,'.APPLICATION_ENV.'}.php',
                'config/autoload/global.php'
        )
    )
);
