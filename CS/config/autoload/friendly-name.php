<?php

return [
    'user'=>'Usuário',
    'acl-profile'=>'Perfis',
    'acl-module'=>'Módulos',
    'acl-service'=>'Serviços',
    'group'=>'Grupo de Usuário',
    'GroupUser'=>'Usuários',
    'report'=>'Relatório',
    'robot-files'=>'Arquivos Sincronizados',
    'robot-processo'=>'Processos',
    'robot-tipo'=>'Tipo de Peças Processuais',
    'robot-relacionamento'=>'Triagem',
    'robot-autodelete'=>'Ignorar pela Publicação',
    'robot-tipo-documento'=>'Tipos de Documento',
    'robot-pagamentos'=>'Pagamentos',
    'robot-manifestacao'=>'Manifestação de Interesse',
];