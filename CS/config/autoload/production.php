<?php
$database = 'COSTA_SILVA_TRIBUNAIS';
return array(
    'db' => array(
        'username' => 'root',
        'password' => 'In73grA6855',
        'host' => 'localhost',
        'banco' => $database,
        'dsn'      => 'mysql:dbname='.$database.';host=localhost',
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    'INTEGRA'=>[
        'folder'=>'/var/www/integra/data/integra',
        'Archer'=>[
            'url_api'=>'http://arteria.costaesilvaadv.com.br/RSAarcher/api/',
            'url_soap'=>'http://arteria.costaesilvaadv.com.br/RSAarcher/ws/search.asmx',
            'login'=>'integra.arteria',
            'ambiente'=>'Produção',
            'senha'=>'Int3gr44rt3r14',
        ],
        'ftp'=>[
            'host'=>'160.20.197.21',
            'port'=>21,
            'user'=>'integra.ct',
            'pass'=>'C0st43s1lv@int3gr4'
        ]
    ]
);
