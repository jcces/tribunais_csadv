<?php
return [
    'db' => [
        'driver' => 'Pdo',
        'driver_options' => [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''],
    ],
    'service_manager' => [
        'factories' => [
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ],
    ],
    'nomeProjeto' => [
        'long'=>'CS Tribunais',
        'short'=>'CS'
    ],
    'UserData' => [
        "profile-path=C:\Users\DELL VOSTRO\AppData\Local\Google\Chrome\User Data\Default",
        "user-data-dir=C:\Users\DELL VOSTRO\AppData\Local\Google\Chrome\User Data"
    ],
    'versao' => '0.0',//major version . minor version . release version
    'path_anexos'=>'./data/upload',
    'salt' => 'sH4&bW2@&*'
];