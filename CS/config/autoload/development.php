<?php
$database = 'COSTA_SILVA_TRIBUNAIS';
return array(
    'db' => array(
        'username' => 'root',
        'password' => 'root',
//        'password' => 'In73grA6855',
        'host' => 'localhost',
        'banco' => $database,
        'dsn'      => 'mysql:dbname='.$database.';host=localhost',
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    'INTEGRA'=>[
        'folder'=>'/var/www/integra/data/integra',
        'Archer'=>[
            'url_api'=>'http://devarteria.ddns.net/RSAarcher/api/',
            'url_soap'=>'http://devarteria.ddns.net/RSAarcher/ws/search.asmx',
//            'url_api'=>'http://192.168.1.201/RSAarcher/api/',
//            'url_soap'=>'http://192.168.1.201/RSAarcher/ws/search.asmx',
            'login'=>'integra.arteria',
            'ambiente'=>'Desenvolvimento',
            'senha'=>'Int3gr44rt3r14',
        ],
        'ftp'=>[
            'host'=>'devarteria.ddns.net',
            'port'=>21100,
            'user'=>'integra',
            'pass'=>'12345678'
        ]
    ]
);
