<?php

namespace Gerador\Controller;

use Estrutura\Controller\AbstractEstruturaController;
use Estrutura\Service\Conexao;
use Gerador\Service\Gerador;
use Gerador\Service\GeradorColuna;
use Gerador\Service\GeradorTabela;

class GeradorController extends AbstractEstruturaController
{
    protected $restrict = ['MANAGER'];

    protected function playAction()
    {
        $service = new GeradorTabela();

        $tabelas = $service->filtrarObjeto();
        $colunas = new GeradorColuna();

        $mapa = [];
        foreach ($tabelas as $tabela) {

            if (!preg_match('/TRIBUNAL_JOB/', $tabela->getTableName())) continue;

            $gerar = true;
            foreach ($this->restrict as $restrict) {
                if (preg_match('/' . $restrict . '_/', $tabela->getTableName())) $gerar = false;
            }
            if ($gerar) {
                $colunas->setTableName($tabela->getTableName());
                $mapa[$tabela->getTableName()] = $colunas->filtrarObjeto();
            }
        }

        $gerador = new Gerador($mapa);
        $gerador->gerar();
        debug(1);
        $this->addSuccessMessage('Efetuado com sucesso');

        return $this->redirect()->toRoute('gerador-home');
    }

    public function createTableAction()
    {
        $sql = "CREATE TABLE `" . $this->params('id') . "` (
                `ID` CHAR(36) NOT NULL,
                `DT_CREATED` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de Criação',
                `DT_UPDATED` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data de Atualização',
                `UPDATED_BY` CHAR(36) NULL DEFAULT NULL COMMENT 'Criado Por',
                `CREATED_BY` CHAR(36) NULL DEFAULT NULL COMMENT 'Atualizado Por',
                `DELETED` CHAR(1) NULL DEFAULT '0' COMMENT 'Deletado',
                PRIMARY KEY (`ID`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            ;";

        Conexao::execSql($sql);
        debug("Feito");
    }
} 