<?php

namespace Gerador\Service;


class Gerador {

    protected $mapa;
    protected $controllers;

    public function __construct($mapa){
        $this->mapa = $mapa;
    }

    public function gerar(){
        foreach($this->mapa as $tabela => $campos){
            $nameSpace = $this->getNameSpace($tabela);
            $this->criarPastas($nameSpace);
            $this->gerarTable($nameSpace, $tabela, $campos);
            $this->gerarEntitys($nameSpace, $tabela, $campos);
            $this->gerarServices($nameSpace, $tabela, $campos);
            $this->gerarForms($nameSpace,$tabela,$campos);
            $this->gerarControllers($nameSpace, $tabela, $campos);
        }

        foreach($this->controllers as $nameSpace => $item){
            $string = '';
            $route = '';
            foreach($item as $controller){
                $lower = strtolower($nameSpace);
                $class = $controller['class'];
                $classHifen = strtolower($controller['classHifem']);

                $string .= '            \''.$lower.'-'.$classHifen.'\' => \''.$nameSpace.'\Controller\\'.ucfirst($class).'Controller\','."\r\n";
                $route .= "            '".$lower."-".$classHifen."' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/".$lower."-".$classHifen."[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => '".$lower."-".$classHifen."',
                        'action'     => 'index',
                    ),
                ),
            ),"."\r\n";
            }

            $this->gerarConfigNameSpace($nameSpace, $string, $route);
        }

        chmod(BASE_PATCH.'/module', 0777);
    }

    public function gerarTable($nameSpace, $tabela, $campos){
        $stringCampos = $this->gerarTabelaCampos($campos);
        $modelo = $this->getModelo('Table');
        $classe = $this->getClasseName($nameSpace, $tabela);

        $arquivo = $this->tratarModelo( ['namespace'=>$nameSpace,'classe'=>$classe, 'tabela'=>$tabela,'stringCampos'=>$stringCampos], $modelo );

        $location = $this->getLocation($nameSpace, 'Table', $classe);
        $this->escreverArquivo($location['file'], $arquivo);
    }

    public function getNameSpace($tabela){
        $tabela = strtolower($tabela);
        $dadosTabela = explode('_',$tabela);
        return ucfirst($dadosTabela[0]);
    }

    private function gerarTabelaCampos($campos)
    {
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */
        foreach($campos as $item){
            $string .= "        '{$item->getColumnName()}'=>'{$this->tratarNome($item->getColumnName())}', \r\n";
        }

        return $string;
    }

    private function gerarEntityCampos($campos){
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */
        foreach($campos as $item){
            $string .= "        ".'protected $'."{$this->tratarNome($item->getColumnName())}; \r\n";
        }

        return $string;
    }

    private function gerarEntityGetset($campos){
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */

        foreach($campos as $item){
            $campo = $this->tratarNome($item->getColumnName());
            $string .= "        ".'public function get'."{$campo}()
            {
                return ".'$this->'."{$campo};
            } \r\n";

            $string .= "        ".'public function set'."{$campo}(".'$'."{$campo})
            {
                return ".'$this->'."{$campo} = ".'$'."{$campo};
            } \r\n";
        }

        return $string;
    }

    public function getModelo($modelo){
        $file = __DIR__.'/../Modelo/'.$modelo.'.modelo';
        return file_get_contents($file);
    }

    private function getClasseName($nameSpace, $tabela,$file='')
    {
        $nameSpace = strtolower($nameSpace);
        $tabela = strtolower($tabela);
        $nameSpaceSize = strlen($nameSpace.'_');
        $principal = substr($tabela,$nameSpaceSize);
        return $this->tratarNome($principal,$file);
    }

    private function tratarNome($principal,$file=''){
        $principal = strtolower($principal);
        $dados = explode('_',$principal);
        $string = [];
        foreach($dados as $item){
            $string[] = ucfirst($item);
        }
        return implode($file,$string);
    }

    private function getLocation($nameSpace, $tipo=false, $classe=''){
        $gerador = require(BASE_PATCH.'/config/autoload/gerador.php');
        if(!$tipo){
            return $gerador['location'].$nameSpace;
        }
        $caminho = $gerador['location'].$nameSpace.'/src/'.$nameSpace.'/'.$tipo.'/';
        $dados = [
            'file'=>$caminho.$classe.'.php',
            'folder'=>$caminho
        ];
        return $dados;
    }

    private function gerarConfigNamespace($nameSpace, $controllers, $route)
    {
        $folder = $this->getLocation($nameSpace);

        /// Module.php
        $modelo = $this->getModelo('Module');
        $module = $this->tratarModelo(['nameSpace'=>$nameSpace], $modelo);
        $this->escreverArquivo($folder.'/Module.php', $module);

        /// module.config.php
        $folder = $folder.'/config';
        $modelo = $this->getModelo('Module.Config');
        $module = $this->tratarModelo(['invokables'=>$controllers,'namespace'=>$nameSpace,'route'=>strtolower($nameSpace),'routeAdd'=>$route], $modelo);
        $this->escreverArquivo($folder.'/module.config.php', $module);
    }

    public function tratarModelo($array, $modelo){
        $modeloArray = [];
        foreach($array as $name => $item){
            $modeloArray[] = '{{'.$name.'}}';
        }

        $tratado = str_replace( $modeloArray, array_values($array),$modelo);
        return $tratado;
    }

    public function escreverArquivo($arquivo, $conteudo){
        if(!file_exists($arquivo)){
            $handle = fopen($arquivo, 'x+');
            fwrite($handle, $conteudo);
            fclose($handle);
        };
    }

    private function criarPastas($nameSpace)
    {
        $pastas = ['config',
            'view/'.strtolower($nameSpace),
            'src/'.$nameSpace.'/Controller',
            'src/'.$nameSpace.'/Service',
            'src/'.$nameSpace.'/Table',
            'src/'.$nameSpace.'/Entity',
            'src/'.$nameSpace.'/Form'];

        $folder = $this->getLocation($nameSpace);

        foreach($pastas as $pasta){
            if(!file_exists($folder.'/'.$pasta)){
                mkdir($folder.'/'.$pasta, 0777, true);
            }
        }
    }

    private function gerarEntitys($nameSpace, $tabela, $campos)
    {
        $stringCampos = $this->gerarEntityCampos($campos);
        $stringGetSet = $this->gerarEntityGetset($campos);

        $modelo = $this->getModelo('Entity');
        $classe = $this->getClasseName($nameSpace, $tabela);

        $arquivo = $this->tratarModelo( ['namespace'=>$nameSpace,'classe'=>$classe, 'campos'=>$stringCampos,'getAndSet'=>$stringGetSet], $modelo );

        $location = $this->getLocation($nameSpace, 'Entity', $classe);
        $this->escreverArquivo($location['file'], $arquivo);
    }

    private function gerarServices($nameSpace, $tabela, $campos)
    {
        $modelo = $this->getModelo('Service');
        $classe = $this->getClasseName($nameSpace, $tabela);

        $stringCampos = $this->gerarCamposService($nameSpace, $campos);

        $arquivo = $this->tratarModelo( ['namespace'=>$nameSpace,'classe'=>$classe,'metodos'=>$stringCampos], $modelo );

        $location = $this->getLocation($nameSpace, 'Service', $classe);
        $this->escreverArquivo($location['file'], $arquivo);
    }

    private function gerarCamposService($nameSpace, $campos){
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */
        $camposOption = '';
        $camposClass = '';
        foreach($campos as $item){
            $type = $this->getTypeForm($item);
            if($type == 'select'){

                $name = $this->tratarNome($item->getColumnName());
                $camposOption .= "        ".'public $options'.$name.' = [""=>"Selecione..."];'."\n";
                $string .= "        ".'public function getLabel'.$name.'(){
            $mapa = $this->options'.$name.';
            if(isset($mapa[$this->get'.$name.'()])) return $mapa[$this->get'.$name.'()];
        }'."\n\r";
            }
            if($type == 'combo'){
                $name = $this->tratarNome($item->getColumnName());
                $nameAmigavel  = $this->tratarNome(str_replace('ID_','',$item->getColumnName()));

                $camposClass .= "        ".'public $class'.$nameAmigavel.' = \'\\'.$nameSpace.'\Service\\'.$nameAmigavel.'\';'."\n";

                $string .= "        ".'public function getObj'.$nameAmigavel.'(){
            $class = $this->class'.$nameAmigavel.';
            $service = new $class;
            $service->setId($this->get'.$name.'());
            $service->load();
            return $service;
        }'."\n\r";
            }
        }

        return $camposClass.$camposOption."\n".$string;
    }

    private function gerarForms($nameSpace, $tabela, $campos)
    {
        $stringCampos = $this->gerarFormCampos($campos, $nameSpace);
        $modelo = $this->getModelo('Form');
        $classe = $this->getClasseName($nameSpace, $tabela);

        $service = "        ".'$service = new \\'.$nameSpace.'\Service\\'.$classe.'();';

        $arquivo = $this->tratarModelo( ['namespace'=>$nameSpace,'classe'=>$classe, 'campos'=>$stringCampos,'formName'=>strtolower($classe),'service'=>$service], $modelo );

        $location = $this->getLocation($nameSpace, 'Form', $classe);
        $this->escreverArquivo($location['file'], $arquivo);
    }

    private function gerarFormCampos($campos, $nameSpace)
    {
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */
        foreach($campos as $item){
            $type = $this->getTypeForm($item);

            if($type == 'hidden')
                $item->setIsNullable('YES');

            $name = $this->tratarNome($item->getColumnName());
            $label =$item->getColumnComment();
            $nulo = ($item->getIsNullable() == 'YES') ? 'false' : 'true';

            if($type == 'combo'){
                $service = str_replace('ID_','',$item->getColumnName());
                $service = $this->tratarNome($service);
                $string .= "        ".'$objForm->'.$type.'("'.$name.'", $service->class'.$service.')->required('.$nulo.')->label("'.$label.'")->setAttribute(\'col\',\'col-md-6\');'."  \r\n";
            }elseif($type == 'select'){
                $coluna = $item->getColumnName();
                $coluna = '$service->options'.$this->tratarNome($coluna);
                $string .= "        ".'$objForm->'.$type.'("'.$name.'", '.$coluna.')->required('.$nulo.')->label("'.$label.'")->setAttribute(\'col\',\'col-md-6\');'."  \r\n";
            }elseif($type == 'textarea'){
                $string .= "        ".'$objForm->'.$type.'("'.$name.'")->required('.$nulo.')->label("'.$label.'")->setAttribute(\'col\',\'col-md-12\');'."  \r\n";
            }elseif($type == 'object'){
                $field = str_replace('OBJ_','',$item->getColumnName());
                $string .= "        ".'$objForm->'.$type.'("'.$name.'","'.$field.'")->required('.$nulo.')->label("'.$label.'")->setAttribute(\'col\',\'col-md-6\');'."  \r\n";
            }else{
                $string .= "        ".'$objForm->'.$type.'("'.$name.'")->required('.$nulo.')->label("'.$label.'")->setAttribute(\'col\',\'col-md-6\');'."  \r\n";
            }
        }

        return $string;
    }

    private function getTypeForm($item)
    {
        /** @var $item \Gerador\Service\GeradorColuna */
        switch($item->getDataType()){
            case 'int':
                if(preg_match('/auto_increment/', $item->getExtra())){
                    $tipo = 'hidden';
                }elseif(preg_match('/OBJ_/',$item->getColumnName())){
                    $tipo = 'object';
                }elseif(preg_match('/ID_/',$item->getColumnName())){
                    $tipo = 'combo';
                }else{
                    $tipo = 'int';
                }
                break;
            case 'text':
                $tipo = 'textarea';
                break;
            case 'char':
                $tipo = 'select';
                if($item->getCharacterMaximumLength() == 36 || $item->getColumnName() == 'DELETED') {
                    $tipo = 'hidden';
                }
                if(preg_match('/ID_/',$item->getColumnName())){
                    $tipo = 'combo';
                }
                if(preg_match('/OBJ_/',$item->getColumnName())){
                    $tipo = 'object';
                }
                break;
            case 'date':
                $tipo = 'date';
                break;
            case 'datetime':
                $tipo = 'datetime';
                if(preg_match('/DT_CREATED/',$item->getColumnName()) || preg_match('/DT_UPDATED/',$item->getColumnName())) {
                    $tipo = 'hidden';
                }
                break;
            case 'double':
                $tipo = 'money';
                break;
            case 'float':
                $tipo = 'money';
                break;
            case 'decimal':
                $tipo = 'money';
                break;
            default:
                if(preg_match('/CPF/',$item->getColumnName())){
                    $tipo = 'cpf';
                }elseif(preg_match('/CNPJ/',$item->getColumnName())){
                    $tipo = 'cnpj';
                }elseif(preg_match('/CEP/',$item->getColumnName())){
                    $tipo = 'cep';
                }elseif(preg_match('/EMAIL/',$item->getColumnName())){
                    $tipo = 'email';
                }elseif(preg_match('/TELEFONE/',$item->getColumnName())){
                    $tipo = 'telefone';
                }elseif(preg_match('/DT_/',$item->getColumnName())){
                    $tipo = 'date';
                }elseif(preg_match('/DH_/',$item->getColumnName())){
                    $tipo = 'datetime';
                }elseif(preg_match('/IMG_/',$item->getColumnName())){
                    $tipo = 'image';
                }else{
                    $tipo = 'text';
                }
                break;
        }

        return $tipo;
    }

    private function gerarControllers($nameSpace, $tabela, $campos)
    {
        $modelo = $this->getModelo('Controller');
        $classe = $this->getClasseName($nameSpace, $tabela);

        $arquivo = $this->tratarModelo( ['namespace'=>$nameSpace,'classe'=>$classe], $modelo );

        $location = $this->getLocation($nameSpace, 'Controller', $classe.'Controller');
        $this->escreverArquivo($location['file'], $arquivo);

        $classHifem = $this->getClasseName($nameSpace, $tabela,'-');
        $location = $this->folderView($nameSpace, $classHifem);

        /// Index
        $modelo = $this->getModelo('Index');
        $th = $this->gerarIndexTh($campos);
        $td = $this->gerarIndexTd($campos);

        $arquivo = $this->tratarModelo( ['th'=>$th,'td'=>$td,'namespace'=>$nameSpace,'classe'=>$classe], $modelo );

        $this->escreverArquivo($location.'/index.phtml', $arquivo);

        /// Cadastro
        $modelo = $this->getModelo('Cadastro');
        $stringCampos = $this->gerarCadastroCampos($campos);
        $arquivo = $this->tratarModelo( ['campos'=>$stringCampos,'namespace'=>$nameSpace,'classe'=>$classe,'route'=>strtolower($nameSpace)], $modelo );

        $this->escreverArquivo($location.'/cadastro.phtml', $arquivo);

        $this->controllers[$nameSpace][] = ['class'=>$classe,'classHifem'=>$classHifem];

    }

    private function folderView($namespace, $classe)
    {
        $location = $this->getLocation($namespace);
        $view = $location.'/view/'.strtolower($namespace.'/'.$classe);
        if(!file_exists($view)){
            mkdir($view, 0777, true);
        }
        return $view;
    }

    private function gerarCadastroCampos($campos)
    {
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */
        foreach($campos as $item){
            $tipo = $this->getTypeForm($item);
            if($tipo == 'hidden') continue;

            $name = $this->tratarNome($item->getColumnName());

            $html = '<?php foreach($form->getElements() as $formItem){?>';
            $html .= '<div class="form-group ">
                                          <label for="fullname" class="control-label col-lg-2"><?=$formItem->getLabel()?></label>
                                          <div class="col-lg-10">
                                              <?=$this->formRowNoLabel($formItem)?>
                                          </div>
                                      </div>';
            $html .='<?php }; ?>';

            $html2 = '<div class="form-group">
                    <div class="col-md-4">
                        <?=$this->formRow($form->get(\''.$name.'\'))?>
                    </div>
                </div>';

            $string .= $html;
        }

        return $string;
    }

    protected $classHidden = 'hidden-xs hidden-sm';

    private function gerarIndexTh($campos)
    {
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */
        foreach($campos as $item){
            $type = $this->getTypeForm($item);
            if($type == 'textarea') continue;
            if($type == 'hidden') continue;

            $name = $this->tratarNome($item->getColumnName());
            $hide = (in_array($name,['Id','Nome'])) ? '' : $this->classHidden;

            if($name == 'Id'){
                $style = 'style="width:5%"';
            }elseif($name == 'Nome'){
                $style = 'style="width:25%"';
            }else{
                $style = '';
            }

            $html = "<th class=\"".$hide."\" $style>{$item->getColumnComment()}</th> \r\n                                ";
            $string .= $html;
        }

        return $string;
    }

    private function gerarIndexTd($campos)
    {
        $string = '';
        /** @var $item \Gerador\Service\GeradorColuna */
        foreach($campos as $item){
            $name = $this->tratarNome($item->getColumnName());

            $type = $this->getTypeForm($item);
            if($type == 'textarea') continue;

            if($type == 'select'){
                $html = '<?=$item->getLabel'.$name.'()?>';
            }elseif($type == 'combo'){
                $coluna = str_replace('ID_','',$item->getColumnName());
                $name = $this->tratarNome($coluna);
                $html = '<?=$item->getObj'.$name.'()->getName()?>';
            }elseif($type == 'object'){
                $html = '<?=$item->getObject("'.$name.'")->getName()?>';
            }elseif($type == 'date'){
                $html = '<?=$this->Datetime($item->get'.$name.'(),false,false,\'d/m/Y\')?>';
            }elseif($type == 'datetime'){
                $html = '<?=$this->Datetime($item->get'.$name.'(),false,false,\'d/m/Y H:i:s\')?>';
            }elseif($type == 'image'){
                $html = '<?=$this->ShowAttach($item->get'.$name.'())?>';
            }elseif($type == 'telefone'){
                $html = '<?=$this->MascaraNumero($item->get'.$name.'(),\'telefone\')?>';
            }elseif($type == 'hidden'){
                $html = false;
            }
            elseif(in_array($type,['cpf','cnpj','cep','money'])){
                $html = '<?=$this->MascaraNumero($item->get'.$name.'(),\''.$type.'\')?>';
            }else{
                $html = '<?=$item->get'.$name.'()?>';
            }
            $hide = (in_array($name,['Id','Name'])) ? '' : $this->classHidden;
            if($html){
                $td = '<td class="'.$hide.'">'.$html.'</td> '."\r\n".'                                ';
                if($string) $string .= '    ';
                $string .= $td;
            };
        }

        return $string;
    }
} 