<?php

namespace Gerador\Service;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class GeradorTabela extends \Gerador\Entity\GeradorTabela{
    public function select($where=null){

        $select = new Select();
        $select->from($this->getTable()->table);
        $select->columns($this->getTable()->getColumns());
        $select->where($where);

        if(!$this->getTableSchema()) $this->getConfig();

        /** @var $lista GeradorTabela[] */
        /** @var $tratado GeradorTabela[] */
        $lista = $this->getTable()->select($select);

        $tratado = [];
        foreach($lista as $item){
            if($item->getTableSchema() == $this->getTableSchema()){
                $tratado[] = $item;
            }
        }

        return $tratado;
    }

    public function getConfig(){
        $gerador      = require(BASE_PATCH.'/config/autoload/gerador.php');
        $this->setTableSchema( $gerador['database'] );
        if(!$this->config){
            $this->config = $gerador['db'];
        }

        return $this->config;
    }
}