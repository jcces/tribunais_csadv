<?php
/**
 * Classe de abstração para as controllers de crud do sistema
 * Define as funções principais dos cruds do sistema
 */

namespace Estrutura\Controller;

use Estrutura\Service\Config;
use Manager\Service\Attach;
use Manager\Service\Usuario;
use Manager\Service\Usuarios;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

abstract class AbstractCrudController extends AbstractEstruturaController
{

    protected $salt = '';
    protected $hash = '';

    public function index($service,$form,$atributos=[]){
        $dadosView = [
            'service'=>$service,
            'form'=>$form,
            'lista'=>$service->filtrarObjeto(),
            'controller'=>$this->params('controller'),
            'action'=>$this->params('action'),
            'atributos'=>$atributos
        ];
        return new ViewModel($dadosView);
    }

    public function cadastro($service,$form,$atributos=[]){
        $id = $this->params("id");
        if($id){
            $service = $service->buscar($id);
            if(!$service){
                $this->addErrorMessage('Registro não localizado');
                return $this->redirect()->toUrl('/'.$this->params('controller'));
            }
            $form->setData($service->toArray());
        }

        if($post = $this->getPost()){
            $form->setData($post);
        }

        $dadosView = [
            'service'=>$service,
            'form'=>$form,
            'controller'=>$this->params('controller'),
            'action'=>$this->params('action'),
            'atributos'=>$atributos
        ];

        return new ViewModel($dadosView);
    }

    public function gravar($service, $form,$forcePost=false){
        try{
            $controller = str_replace("manager-", "", $this->params('controller'));
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos');
            }

            if($forcePost){
                if(!is_array($forcePost)) $post = $forcePost->toArray();
            }else{
                $post = $request->getPost()->toArray();
            }

            $files = $request->getFiles();
            $upload = $this->uploadFile($files,$controller);

            $post = array_merge($post, $upload);

            $form->setData($post);

            if(!$form->isValid()){
                $this->addValidateMessages($form);
                return $this->errorRedirect($controller, $post);
            }

            $service->exchangeArray($form->getData());

            $service->salvar();

            if(isset($post['Anexo'])){
                $service->saveAttach($post['Anexo']);
            }

            $this->posSave();
            $this->addSuccessMessage('Registro salvo com sucesso!');
            if($controller == 'settings') return $this->redirect()->toUrl('/'.$controller);
            return $this->redirect()->toUrl('/'.$controller.'/cadastro/'.$service->getId());
        }catch (\Exception $e){
            $this->addErrorMessage($e->getMessage());
            return $this->errorRedirect($controller, $post);
        }
    }

    public function posSave(){}

    public function errorRedirect($controller, $post,$action='cadastro'){
        $this->setPost($post);
        $url = '/'.$controller.'/'.$action;
        if(isset($post['Id']) && $post['Id'] != '') $url .= '/'.$post['Id'];
        return $this->redirect()->toUrl($url);
    }

    public function excluir($service,$form,$atributos=[],$ajax=false){
        try{
            $request = $this->getRequest();
            if($request->isPost()){
                return new JsonModel();
            }

            $controller = str_replace("manager-", "", $this->params('controller'));

            $id = $this->params('id');
            $service->setId($id);

            $dados = $service->filtrarObjeto()->current();

            if(!$dados) throw new \Exception('Registro não encontrado');

            $service->excluir();

            if(method_exists($service,'getPath')){
                if(file_exists($dados->getPath())) unlink($dados->getPath());
            }

            if($ajax) return new JsonModel(['error'=>false]);
            $this->addSuccessMessage('Registro excluido com sucesso');

        }catch (\Exception $e){
            if($ajax) return new JsonModel(['error'=>true,'message'=>$e->getMessage()]);
            $this->addErrorMessage($e->getMessage());
        }

        return $this->redirect()->toUrl('/'.$controller);
    }

    public function uploadFile($files,$diretory=false)
    {
        $retorno = [];
        foreach($files as $key => $file){
            if(isset($file['name'])){
                $success = $this->processUpload($file,$diretory);
                if($success){
                    $retorno[$key] = $success;
                }
            }else{
                foreach($file as $item){
                    $success = $this->processUpload($item,$diretory);
                    if($success){
                        $retorno[$key][] = $success;
                    }

                }
            }

        }
        return $retorno;
    }

    public function processUpload($file, $directory=false){
        $path = Config::getConfig('path_anexos').'/';
        if($directory) $path.$directory.'/';

        $filter = new \Zend\Filter\File\RenameUpload($path);
        $filter->setUseUploadName(true);
        $filter->setUseUploadExtension(true);
        $filter->setRandomize(true);
        $filter->setOverwrite(false);
        $name = $filter->filter($file);
        return $name['tmp_name'];
    }

    public function checkLoggedUser($field='Login')
    {
        $sessao = new Container('autenticacao');
        return $sessao->autenticacao[$field];
    }

}