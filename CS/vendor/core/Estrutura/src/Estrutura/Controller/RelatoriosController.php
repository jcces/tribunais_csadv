<?php

namespace Estrutura\Controller;

use Estrutura\Service\Conexao;
use Zend\View\Model\ViewModel;

class RelatoriosController extends AbstractEstruturaController{
    public function indexAction(){
        return new ViewModel();
    }

    public function demoAction(){
        $lista = Conexao::listarSql("SELECT * FROM TESTE_TESTE");
        return $this->gerarRelatorio($lista,'Demonstracao');
    }

    private function gerarCsv($lista, $nome='Relatorio'){
        if(!count($lista)){
            echo 'Sem dados para o relatório';
            die;
        }

        $valores = [];
        foreach($lista as $item){
            $item = (array) $item;
            $keys = array_keys($item);
            $valores[] = (array)array_values($item);
        }

        $csv = implode(';',$keys)."\n";
        foreach($valores as $item){
            $csv.=implode(';',$item)."\n";
        }

        $response = $this->getResponse();
        $response->setContent(($csv));

        $headers = $response->getHeaders();
        $headers->clearHeaders()
            ->addHeaderLine('Content-Type', 'text/csv')
            ->addHeaderLine('Content-Disposition', 'attachment; filename="'.$nome.'.csv"');

        return $this->response;
    }

    public function gerarRelatorio($dados,$nome,$template='relatorio'){
        $this->layout('layout/relatorio');

        $view = new \Zend\View\Model\ViewModel();
        $view->setTemplate('/estrutura/relatorios/'.$template);
        $view->setVariable('nome',$nome);

        if(!count($dados)){
            $view->setVariable('vazio',true);
            return $view;
        }else{
            $view->setVariable('vazio',false);

            $header = array_keys((array) $dados[0]);

            $view->setVariable('header',$header);
            $view->setVariable('body',$dados);
        }

        return $view;
    }

} 