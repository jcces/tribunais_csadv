<?php

namespace Estrutura\Form;

use Estrutura\Form\Element\Combo;
use Estrutura\Form\Element\Cpf;
use Estrutura\Form\Element\FloatElement;
use Estrutura\Form\Element\Vetor;
use Zend\Form\Element\Hidden;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Zend\Session\Container;

class FormObject {

    const TIPO_TEXT = 'Text';
    const TIPO_CPF = '\Estrutura\Form\Element\Cpf';
    const TIPO_DATE = 'Date';
    const TIPO_HIDDEN = 'Hidden';
    const TIPO_EMAIL = 'Email';

    protected $namespace;

    protected $riskManager = false;

    /**
     * @var AbstractForm
     */
    protected $form;
    protected $elements = [];

    /**
     * @param $name
     * @return FormElement
     * @throws \Exception
     */
    public function get($name){
        if (!array_key_exists($name,$this->elements))
            throw new \Exception("Elemento $name não encontrado.");
        return $this->elements[$name];
    }

    public function __construct($namespace, AbstractForm $form, \Zend\InputFilter\InputFilter $inputFilter) {
        $this->namespace = $namespace;
        $this->form = $form;
        $this->inputFilter = $inputFilter;
    }

    /**
     * @param Element $element
     * @return FormElement
     */
    public function add(Element $element) {
        $name = $element->getName();
        if (!isset($this->elements[$name])) {
            $element->setAttributes(array(
                'id' => $name,
                'class' => 'form-control input-sm pop',
            ));
            $this->inputFilter->add(['name' => $element->getName(), 'required' => false]);
            $formElement = new FormElement($element, $this->inputFilter);
            $this->form->add($element);
            $this->elements[$name] = $formElement;
        }

        return $this->elements[$name];
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function hidden($name) {
        return $this->add(new Hidden($name));
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function userSession($name) {
        $sessao = new Container('autenticacao');
        $element = $this->add(new Hidden($name));
        $element->setAttribute('value', (isset($sessao->autenticacao['Login']))?$sessao->autenticacao['Login']:'');
        return $element;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function date($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'date');
        return $element;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function datetime($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'datetimepicker');
        $element->addClass('datetimepicker');
        return $element;
    }

    public function datepicker($name,$empty=false){
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'datepicker');
        $classes = $element->getAttribute('class');
        $element->setAttribute('class', $classes.' datepicker');
        $element->setAttribute('value',($empty)?'':date('d/m/Y'));

        return $element;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function captcha($name) {
        $captchaService = $this->form->sm()->get('Captcha');
        $captcha = new Element\Captcha($name);
        $captcha->setCaptcha($captchaService);
        $element = $this->add($captcha);
        return $element;
    }

    /**
     * @param $name
     * @param $service
     * @param string $chave
     * @param string $valor
     * @param string $metodo
     * @param array $params
     * @return FormElement
     */
    public function combo($name, $service, $chave = 'id', $valor = 'name', $metodo = 'fetchAll', $params = []) {
        $element = $this->add(new Combo($name));
        $element->setOptions(['service' => $service, 'chave' => $chave, 'valor' => $valor, 'metodo' => $metodo, 'params' => $params]);
        $element->setAttribute('data-chosen','true');
        return $element;
    }

    /**
     * @param $name
     * @param $service
     * @param string $chave
     * @param string $valor
     * @param string $metodo
     * @param array $params
     * @return FormElement
     */
    public function comboSimple($name, $service, $chave = 'id', $valor = 'name', $metodo = 'fetchAll', $params = []) {
        $element = $this->add(new Combo($name));
        $element->setOptions(['service' => $service, 'chave' => $chave, 'valor' => $valor, 'metodo' => $metodo, 'params' => $params]);
        return $element;
    }

    /**
     *
     * @param type $name
     * @param type $valueOption
     * @return FormElement
     */
    public function select($name, $valueOption = [],$multiple=false) {
        $element = $this->add(new Element\Select($name));
        ksort($valueOption);
        $element->setOptions(['value_options' => $valueOption]);
        $element->setAttribute('data-chosen','true');
        if($multiple) $element->setAttribute('multiple','multiple');
        return $element;
    }

    public function selectInsert($nome,$valores,$service,$padrao=array()){
        $element = $this->select($nome,$valores);
        $element->setAttribute('data-service',$service);
        $element->setAttribute('data-callback-field',$nome);
        $element->setAttribute('SelectInsert',true);
        if(count($padrao)) $element->setAttribute('data-padrao',json_encode($padrao));
        return $element;
    }

    /**
     *
     * @param type $name
     * @param type $valueOption
     * @return FormElement
     */
    public function selectSimple($name, $valueOption = [],$multiple=false,$asort=true) {
        $element = $this->add(new Element\Select($name));
        if($asort){
            asort($valueOption);
        }
        $element->setOptions(['value_options' => $valueOption]);
        if($multiple) $element->setAttribute('multiple','multiple');
        return $element;
    }

    /**
     *
     * @param type $name
     * @param type $valueOption
     * @return FormElement
     */
    public function radio($name, $valueOption = []) {
        $element = $this->add(new Element\Radio($name));
        $element->setOptions(['value_options' => $valueOption]);
        $element->setAttribute('class', 'pop');
        return $element;
    }

    /**
     *
     * @param type $name
     * @param type $valueOption
     * @return FormElement
     */
    public function checkbox($name, $valueOption = []) {
        $element = $this->add(new Element\Checkbox($name));
        $element->setOptions(['value_options' => $valueOption]);
        $element->setAttribute('class', 'pop');
        return $element;
    }

    /**
     *
     * @param type $name
     * @param type $valueOption
     * @return FormElement
     */
    public function multiCheckbox($name, $valueOption = []) {
        $element = $this->add(new Element\MultiCheckbox($name));
        $element->setOptions(['value_options' => $valueOption]);
        $element->setAttribute('class', 'pop');
        return $element;
    }

    public function mapLocation($name,$fieldLatitude='Latitude',$fieldLongitude='Longitude'){
        $element = $this->text($name);
        $element->setAttribute('data-latitude',$fieldLatitude);
        $element->setAttribute('data-longitude',$fieldLongitude);
        $element->setAttribute('FieldMap', true);
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function text($name) {
        $element = $this->add(new Element\Text($name));
        $element->addTextValidatorsAndFilters();
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function textNoTags($name) {
        $element = $this->add(new Element\Text($name));
        $element->addTextValidatorsAndFilters();
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function money($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'money');
        $element->setAttribute('value', 0);
        $element->addFilter('Estrutura\Filter\Decimal');
        return $element;
    }

    public function peso($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'peso');
        $element->setAttribute('value', 0);
        $element->addFilter('Estrutura\Filter\Decimal');
        return $element;
    }

    public function percentage($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'percentage');
        $element->setAttribute('value', 0);
        $element->addFilter('Estrutura\Filter\Decimal');
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function password($name, $min=false, $max=false) {
        $element = $this->add(new Element\Password($name));
        $element->setAttribute('minlength',$min);
        $element->setAttribute('maxlength',$max);
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function file($name, $multiple = false) {
        $element = $this->add(new Element\File($name));
        $element->setAttribute('class', '')
            ->setAttribute('multiple', $multiple);

        return $element;
    }

    public function image($name){
        $element = $this->file($name);
        $element->setAttribute('ImageField',true);
        $element->setAttribute('accept','image/*');
        return $element;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function collection($name, $tipoElemento = null) {
        if (!$tipoElemento)
            $tipoElemento = new Element\Text();
        $element = $this->add(new Vetor($name, [ 'target_element' => $tipoElemento]));
        return $element;
    }

    /**
     *
     * @param type $name
     * @return FormElement
     */
    public function textarea($name) {
        $element = $this->add(new Element\Textarea($name));
        //$element->addTextValidatorsAndFilters();
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function telefone($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'telefone');
        $element->addFilter('\Estrutura\Filter\ApenasNumeros');
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function cnpj($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'cnpj');
        $element->addValidator('Estrutura\Validator\Cnpj');
        $element->addFilter('Estrutura\Filter\ApenasNumeros');
        $element->addTextValidatorsAndFilters();
        return $element;
    }

    public function ie($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute('data-mask', 'ie');
        $element->addValidator('Estrutura\Validator\Ie');
        //$element->addFilter('Estrutura\Filter\Cpf');
        $element->addTextValidatorsAndFilters();
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function cpf($name) {
        $element = $this->add(new Cpf($name));
        $element->setAttribute('data-mask', 'cpf');
        $element->addValidator('Estrutura\Validator\Cpf');
        $element->addFilter('Estrutura\Filter\ApenasNumeros');
        $element->addTextValidatorsAndFilters();
        return $element;
    }

    /**
     * @param $name
     * @param null $label
     * @return FormElement
     */
    public function email($name) {
        $element = $this->add(new Element\Email($name));
        $element->addTextValidatorsAndFilters();
        $element->addValidator('\Estrutura\Validator\EmailAddress', ['domain' => false, 'useMxCheck' => false]);
        return $element;
    }

    public function url($name) {
        $element = $this->add(new Element\Url($name));
        return $element;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function float($name) {
        $element = $this->add(new FloatElement($name));
        $element->addFilter('Estrutura\Filter\Decimal');
        return $element;
    }

    /**
     * @param $name
     * @param $label
     * @return FormElement
     */
    public function submit($name, $label) {
        $element = $this->add(new Element\Submit($name));
        $element->value($label);
        return $element;
    }

    /**
     * @param $name
     * @param $size
     * @return FormElement
     */
    public function number($name, $min=false, $max=false) {

        $element = $this->add(new Element\Number($name));
        $element->setAttribute('min',$min);
        $element->setAttribute('max',$max);
//        $element->setAttribute('data-mask', 'int');
//        $element->addFilter('Estrutura\Filter\Decimal');
//        $element->mask('' . number_format(str_repeat('9', $size), 0, ',', '.') . '');
//        $element->maxLength($size);
        return $element;
    }

    /**
     * @param $name
     * @return FormObject
     */
    public function subForm($name) {
        $form = new AbstractForm($name);
        $this->inputFilter->add(['name' => $form->getName(), 'required' => false]);
        $formElement = new FormElement($form, $this->inputFilter);
        $this->form->add($form);
        $this->elements[$name] = $formElement;
        $inputFilter = new InputFilter();
        $objForm = new \Estrutura\Form\FormObject('cadastrarUsuario-' . $name, $form, $inputFilter);
        return $objForm;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function cep($name) {
        $element = $this->add(new Element\Text($name));
        $element->setAttribute("data-mask", 'cep');
        $element->addFilter('\Estrutura\Filter\ApenasNumeros');
        $element->addValidator('\Estrutura\Validator\Cep');
        return $element;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function integer($name) {
        $element = $this->add(new Element\Number($name));
        $element->addValidator('\Estrutura\Validator\Number');
        $element->addFilter('StringTrim');
        return $element;
    }

    /**
     * @param $name
     * @return FormElement
     */
    public function int($name) {
        return $this->integer($name);
    }

    public function slider($name, $min, $max){
        $element = $this->integer($name);
        $element->setAttribute('data-slider','1');
        $element->setAttribute('data-slider-min',$min);
        $element->setAttribute('data-slider-max',$max);
        return $element;
    }

    public function selectTags($name,$options){
        $element = $this->select($name,$options);
        $element->setAttribute('data-tag',true);
        $element->setAttribute('multiple',true);
        return $element;
    }

    public function cpfCnpj($name) {
        $element = $this->add(new Element\Text($name));
        $element->addFilter('\Estrutura\Filter\ApenasNumeros');
        $element->addValidator('\Estrutura\Validator\CpfCnpj');
        return $element;
    }

    public function selectEstados($name,$multiple=false)
    {
        $estados = array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");
        $element = $this->select($name,$estados,$multiple);
        return $element;

    }

    public function anexo($multiple=true)
    {
        $element = $this->file('Anexo', $multiple);
        $element->setAttribute('FieldAnexo', true);
        return $element;
    }

    public function tableRel($name,$service,$relField,$fields=[],$callback = null)
    {
        $element = $this->text('TableRel-'.$name);
        $element->setAttribute('rel-field', $relField);
        $element->setAttribute('FieldTableRel', true);
        $element->setAttribute('service', $service);
        $element->setAttribute('fields', $fields);
        $element->setAttribute('callback', $callback);
        return $element;
    }

    public function organizationObject($string, $type)
    {
        $service = new \Object\Service\Object();
        $service->setIdType($type);
        $list = $service->filtrarObjeto();
        $options = [''=>'Selecione'];
        foreach ($list as $item){
            $options[$item->getId()] = $item->getName();
        }
        return $this->select($string,$options);
    }

    public function tableLink($name,$service,$relField,$fields=[],$url='',$callback = null)
    {
        $element = $this->text('TableRel-'.$name);
        $element->setAttribute('rel-field', $relField);
        $element->setAttribute('FieldTableLink', true);
        $element->setAttribute('service', $service);
        $element->setAttribute('fields', $fields);
        $element->setAttribute('callback', $callback);
        $element->setAttribute('data-url', $url);
        return $element;
    }

    /**
     * Retorna os nomes de elementos do Formulário
     * @return array
     */
    protected function getElementsKeys(){
        return array_keys($this->elements);
    }

}