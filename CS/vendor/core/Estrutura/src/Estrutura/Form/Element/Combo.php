<?php



namespace Estrutura\Form\Element;



use Zend\Form\Element;



class Combo extends Element\Select

{

    public function setOptions($options)

    {

        parent::setOptions($options);



        if (!isset($this->options['service'])) throw new \Exception('O atributo service é obrigatório na opções do Elemento '. $this->getName() .' do Tipo Combo.', E_USER_ERROR );

        if (!isset($this->options['chave'])) $this->options['chave'] = 'id';

        if (!isset($this->options['valor'])) $this->options['valor'] = 'nome';

        if (!isset($this->options['metodo'])) $this->options['metodo'] = 'fetchAll';

        if (!isset($this->options['params'])) $this->options['params'] = array();



        $this->buscarValores($this->options['service'],$this->options['chave'],$this->options['valor'],$this->options['metodo'],$this->options['params']);



    }



    protected function buscarValores($service,$chave,$valor,$metodo,$params)

    {

        if(! class_exists($service)) throw new \Exception( "Service $service não encontrado ao construir combo");

        $objService = new $service;

        $rs = call_user_func_array( [$objService,$metodo] , $params );

        $arrValores = array(''=>'Selecione...');
        if(!$rs) $rs = [];
        foreach($rs as $item)

        {

            $arrValores[$item->{'get'.$chave}()] = $item->{'get'.$valor}();

        }

        $this->setValueOptions($arrValores);

    }









}

