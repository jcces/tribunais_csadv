<?php
namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class OpenLayers extends AbstractHelper
{
    
    public function __invoke()
    {
        return new \Estrutura\Service\OpenLayers();
    }
}