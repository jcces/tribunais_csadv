<?php
namespace Estrutura\View\Helper;

use Zend\View\Helper\AbstractHelper;

class SideBarToggle extends AbstractHelper
{
    /**
     * @param string $data
     * @param bool $formato
     * @return \DateTime
     */
    public function __invoke()
    {
        $sessao = new \Zend\Session\Container('autenticacao');
        $className = '';
        if(isset($sessao->autenticacao['sideBarToggleOpen']) && $sessao->autenticacao['sideBarToggleOpen'] == 0){
            $className = 'sidebar-collapse';
        }
        return $className;
    }
}