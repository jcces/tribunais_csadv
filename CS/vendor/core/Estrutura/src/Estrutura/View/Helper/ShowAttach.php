<?php
namespace Estrutura\View\Helper;
use Manager\Service\Attach;
use Zend\View\Helper\AbstractHelper;

class ShowAttach extends AbstractHelper
{
    public function __invoke($anexo,$size=false,$offset='img-rounded',$link=true,$resize=[])
    {
        if($anexo instanceof \Manager\Service\Attach){

        }else{
            $file = $anexo;
            $anexo = new Attach();
            $anexo->setPath($file);
        }


        if(file_exists($anexo->getPath())) {
            if(preg_match('/image/',$anexo->getType())) return $this->renderImage($anexo,$size,$offset,$link,$resize);
            return $this->render($anexo);
        }else{
            if($anexo->getId()) $anexo->excluir();
        }

        return '';
    }

    private function render($anexo){
        return '<span class="label label-success"><a href="/download/attach/'.$anexo->getName().'" style="color:white;" target="_blank">'.$anexo->getName().'</a></span>';
    }

    private function renderImage($anexo,$size,$offset='img-rounded',$link=false,$resize=[]){
        $class = (!$size)  ? 'anexos' : 'anexos-'.$size;
        $dataLink = ($link) ? 'data-action="img-link"' : '';
        $stringSize = '';
        if(count($resize)){
            foreach($resize as $type => $px){
                $stringSize .= $type.'="'.$px.'px" ';
            }
        }
        return '<img '.$stringSize.' class=" '.$offset.' '.$class.'" '.$dataLink.' data-id="'.$anexo->getId().'" src="data:image/jpeg;base64,'.base64_encode(file_get_contents($anexo->getPath())).'"/>';
    }
}