<?php
namespace Estrutura\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FormButtons extends AbstractHelper
{

    public function __invoke($controller,$service)
    {
        $cancel = "<a href='/{$controller}' class='btn btn-default'>Cancelar</a>";
        if(!$service->getId()){
            $resetOrDelete = '<button type="reset" class="btn btn-default">Limpar</button>';
        }else{
            $resetOrDelete = "<a href=\"/{$controller}/excluir/{$service->getId()}\" class=\"btn btn-danger btn-confirma\">Excluir</a>";
        }

        if($controller == 'settings'){
            $cancel = '';
            $resetOrDelete = '';
        }

        $buttons = "
        <div class='box-footer'>
            <div class='form-group hidden-print pull-right'>
                {$cancel}
                {$resetOrDelete}
                <button type='submit' class='btn btn-success' data-submit='true' data-insert='true'>Salvar</button>
            </div>
        </div>";

        return $buttons;
    }

}