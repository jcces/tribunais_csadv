<?php
namespace Estrutura\View\Helper;

use Estrutura\Service\Config;
use Zend\View\Helper\AbstractHelper;

class Version extends AbstractHelper
{

    public function __invoke()
    {
        $svnrev = '.0';
        if (file_exists('./.svn/wc.db')) {
            $sql = new \SQLite3('./.svn/wc.db');
            $result = $sql->query('SELECT changed_revision FROM nodes WHERE local_relpath = "" LIMIT 1');
            if ($result) {
                $row = $result->fetchArray();
                if ($row['changed_revision'] != "") {
                    $svnrev = "." . $row['changed_revision'];
                }
            }
        }
        $version = Config::getConfig('versao');

        return $version . $svnrev;
    }
}