<?php
namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class RegisterDetails extends AbstractHelper
{
    public function __invoke($service)
    {
        if(!$service->getId() || !$service->getRegisterDetails()) return '';
        return '<div class="user-block register-details"><span class="description no-margin"><i class="fa fa-info-circle"></i> Última edição feita por <i>'.$service->getRegisterDetails()->UPDATED_BY.'</i> em <i>'.$this->formatDateTime($service->getRegisterDetails()->UPDATED).'</i>, e criado por <i>'.$service->getRegisterDetails()->CREATED_BY.'</i> em <i>'.$this->formatDateTime($service->getRegisterDetails()->CREATED).'</i></span></div>';
    }

    public function formatDateTime($datetime)
    {
        return str_replace(' ',' às ',$datetime);
    }
}