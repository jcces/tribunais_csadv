<?php

namespace Estrutura\View\Helper;

use Zend\View\Helper\AbstractHelper;

class BuildDial extends AbstractHelper
{

    public function __invoke($value, $color = '', $title, $round = false, $colorReverse = false)
    {
        $tooltip = 'tooltip';
        if (!$title) $tooltip = '';
        if ($round) $value = number_format($value, 0);

        if (!$color) {
            if ($colorReverse) {
                if ($value <= 100) $color = 'red';
                if ($value <= 75) $color = 'orange';
                if ($value <= 50) $color = 'gold';
                if ($value <= 25) $color = 'green';
            } else {
                if ($value <= 100) $color = 'green';
                if ($value <= 75) $color = 'gold';
                if ($value <= 50) $color = 'orange';
                if ($value <= 25) $color = 'red';
            }
        }

        echo '
        <input type="text" class="dial small" data-toggle="' . $tooltip . '" data-title="' . $title . '"
                                                       value="' . $value . '"
                                                       data-min="0"
                                                       data-max="100"
                                                       data-width="30"
                                                       data-height="30"
                                                       data-fgColor="' . $color . '"
                                                       data-bgColor="#ccc"
                                                       readonly="true" />';
    }
}