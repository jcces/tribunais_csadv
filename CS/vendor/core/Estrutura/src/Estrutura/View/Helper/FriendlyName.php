<?php
namespace Estrutura\View\Helper;
use Manager\Service\Attach;
use Zend\View\Helper\AbstractHelper;

class FriendlyName extends AbstractHelper
{
    public function __invoke($name,$compose=[])
    {
        $names = require(BASE_PATCH.'/config/autoload/friendly-name.php');
        if(isset($names[$name])) return $names[$name];

        $string = $this->convertName($name);
        return $this->composerName($string,$compose);
    }

    private function convertName($name){
        $data = explode('-',$name);
        $nData = [];
        foreach($data as $item){
            $nData[] = ucfirst($item);
        }
        return implode(' ',$nData);
    }

    public function composerName($string,$array=[]){
        return $string;
    }
}