<?php
namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class Formatter extends AbstractHelper
{
    public function __invoke($method,$arg=[])
    {
        $this->$method($arg);
    }

    public function extractClass($arg = [])
    {
        return str_replace(['\Manager\Service\\','\\'],'',$arg[0]);
    }

}