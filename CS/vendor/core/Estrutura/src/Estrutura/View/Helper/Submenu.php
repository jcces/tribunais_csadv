<?php

namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class Submenu extends AbstractHelper
{
    public function __invoke($service,$method='',$render=false)
    {
        $submenu = '';
        if($method){
            if(method_exists($service,$method)){
                $submenu = $service->$method();
            }
        }else if(method_exists($service,'submenu')){
            $submenu = $service->submenu();
        }
        return $submenu;
    }
}