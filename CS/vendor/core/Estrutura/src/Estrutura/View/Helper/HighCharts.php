<?php
namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class HighCharts extends AbstractHelper
{
    
    public function __invoke()
    {
        return new \Estrutura\Service\HighCharts();
    }
}