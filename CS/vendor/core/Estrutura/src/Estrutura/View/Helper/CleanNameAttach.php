<?php

namespace Estrutura\View\Helper;

use Zend\View\Helper\AbstractHelper;

class CleanNameAttach extends AbstractHelper
{
    public function __invoke($fileUrl)
    {
        $arr = [];
        if($fileUrl){
            $arr = explode('/',$fileUrl);
            $ret = end($arr);
        }
        return $ret;
    }

}