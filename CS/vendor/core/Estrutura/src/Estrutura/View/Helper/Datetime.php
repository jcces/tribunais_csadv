<?php
namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class Datetime extends AbstractHelper
{
    /**
     * @param string $data
     * @param bool $formato
     * @return \DateTime
     */
    public function __invoke($data, $retornoDatetime=true, $formatoEntrada=false,$formatoSaida='d/m/Y')
    {
        if(!$formatoEntrada) $formato = (preg_match('/-/',$data)) ?  'Y-m-d' : 'd/m/Y';
        if(strlen($data) > 10) $formato .= ' H:i:s';

        $dateTime = \DateTime::createFromFormat($formato, $data);
        if($retornoDatetime){
            if(!$dateTime) return new \Datetime();
        }else{
            if(!$dateTime) return '-';
            return $dateTime->format($formatoSaida);
        }
        return $dateTime;
    }
}