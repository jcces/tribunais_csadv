<?php
/**
 * Created by PhpStorm.
 * User: bruno.silva
 * Date: 11/02/2015
 * Time: 16:36
 */

namespace Estrutura\View\Helper;

use Estrutura\Service\HtmlHelper;
use \Zend\Form\View\Helper\FormRow;
use Zend\Form\ElementInterface;

class FormRowNoLabel extends FormRow{
    /**
     * Invoke helper as functor
     *
     * Proxies to {@link render()}.
     *
     * @param null|ElementInterface $element
     * @param null|string $labelPosition
     * @param bool $renderErrors
     * @param string|null $partial
     * @return string|FormRow
     */
    public function __invoke(ElementInterface $element = null, $idRel = null, $labelPosition = null, $renderErrors = null, $partial = null)
    {
        if (!$element) {
            return $this;
        }

        if($element->getAttribute('FieldTableRel') == true){
            return $this->renderTableRel($element,$idRel, false);
        }

        if($element->getAttribute('FieldTableLink') == true){
            return $this->renderTableRel($element,$idRel, true);
        }

        if($element->getAttribute('SelectInsert') == true){
            return $this->renderSelectInsert($element,$idRel);
        }

        if($element->getAttribute('FieldMap') == true){
            return $this->renderMap($element);
        }

        if($element->getAttribute('type') == 'checkbox'){
            return $this->renderCheckbox($element);
        }

        $label = $element->getLabel();
        $element->setLabel('');

        if ($labelPosition !== null) {
            $this->setLabelPosition($labelPosition);
        } elseif ($this->labelPosition === null) {
            $this->setLabelPosition(self::LABEL_PREPEND);
        }
        if ($renderErrors !== null) {
            $this->setRenderErrors($renderErrors);
        }
        if ($partial !== null) {
            $this->setPartial($partial);
        }

        if($element->getAttribute('data-insert')){
            if($element->getAttribute('data-insert')) return $this->renderInsert($element,$label,$element->getAttribute('required'));
        }

        $mapaGroup = [
            'date'=>'fa-calendar',
            'telefone'=>'fa-phone'
        ];

        if(in_array($element->getAttribute('data-mask'), array_keys($mapaGroup))){
            return $this->renderGroup($element, $mapaGroup[$element->getAttribute('data-mask')]);
        }

        return $this->render($element).$this->helpBlock($element);
    }

    private function renderGroup($element, $class){
        $string = $this->render($element);
        return '<div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa '.$class.'"></i>
                    </div>
                    '.$string.'
                </div>'.$this->helpBlock($element);
    }

    private function renderInsert($element, $label, $required)
    {
        return '<div class="form-group">
                   <label class="control-label">'.$label.' '.$required.'</label>
                    <div class="input-group">
                       '.$this->render($element).'
                        <span class="input-group-btn">
                            <button class="btn btn-inverse btn-sm btn-flat adicionar-resource" type="button" data-resource="'.$element->getAttribute('data-service').'" data-label="'.$label.'" data-callback-field="'.$element->getName().'"><i class="material-icons">add</i></button>
                        </span>
                     </div>
                </div>'.$this->helpBlock($element);
    }

    private function renderTableRel($obj,$idRel,$link=false)
    {
        if(!$idRel) return '';
        $relField = $obj->getAttribute('rel-field');
        $service = $obj->getAttribute('service');
        $fields = $obj->getAttribute('fields');
        $callback = $obj->getAttribute('callback');
        $classesName = ($obj->getAttribute('class'))?$obj->getAttribute('class'):'';
        $order = ($obj->getAttribute('data-columnOrder'))?$obj->getAttribute('data-columnOrder'):'';
        $orderDefs = ($obj->getAttribute('data-orderDefs'))?$obj->getAttribute('data-orderDefs'):'';
        $url = ($link) ? $obj->getAttribute('data-url').'/'.$idRel : '';


        $serviceObj = new $service();

        //PREPARING AN HEAD
        $tableHeader = '';
        foreach($fields as $key => $value){
            $mask = '';
            if(preg_match('/Vl/',$key)) $mask = 'money';
            $tableHeader .= '<th data-field="'.$key.'" data-mask="'.$mask.'">'.$value.'</th>';
        }

        $tableHeader = '<thead><tr class="modelo">'.$tableHeader.'</tr></thead>';

        //PREPARING A BODY
        $addTitleArr = explode('\\',$service);
        $addTitle = $this->view->FriendlyName(end($addTitleArr));

        $method = 'set'.$relField;
        $serviceObj->{$method}($idRel);
        $data = $serviceObj->filtrarObjeto();

        $tableId = 'tableRel'.time();
        $tableBody = '';
        if(count($data)){
            $tableBody = '<tbody>';
            foreach ($data as $item) {
                $tableBody .= "<tr data-table-rel='true' data-rel-field='{$relField}' data-id='{$item->getId()}' data-service='{$service}' data-table='#{$tableId}' data-parent='{$idRel}' data-callback='{$callback}' data-title='Editando {$addTitle}'>";

                $fieldsEntity = array_keys($fields);
                foreach ($fieldsEntity as $entity) {
                    $method = 'get'.$entity;
                    if(preg_match('/Vl/',$method)){
                        $valor = $this->view->MascaraNumero($item->{$method}(),'money');
                        $tableBody .= "<td>{$valor}</td>";
                        continue;
                    }
                    $tableBody .= "<td>{$item->{$method}()}</td>";
                }
                $tableBody .= '</tr>';
            }
            $tableBody .= '</tbody>';
        }else{
            $countThs = substr_count($tableHeader,'</th>');
            $tableBody = "<tbody><tr class='no-records'><td colspan='{$countThs}' class='text-center'>Nenhum registro encontrado</td></tr></tbody>";
        }
        $classesName = str_replace(['form-control','input-sm'],'',$classesName);
        $table = '<table id="'.$tableId.'" class="table small table-bordered table-hover table-striped table-condensed '.$classesName.'" data-table-obj="'.$tableId.'" data-columnOrder="'.$order.'" data-columnDefs="'.$orderDefs.'">{head}{body}</table>';
        $tableId = '#'.$tableId;
        $table = str_replace('{head}',$tableHeader,$table);
        $table = str_replace('{body}',$tableBody,$table);

        $link = ($url) ? $url : 'javascript:void(0)';
        $container =
            '<h5 class="sub-title">'.$addTitle.'</h5>
                <div class="table-rel-container">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <a href="'.$link.'" class="btn btn-primary btn-fab btn-fab-mini btn-raised pull-right tableRelButton" data-table-rel="true" data-table="'.$tableId.'" data-id="" data-parent="'.$idRel.'" data-title="Adicionando '.$addTitle.'" data-service="'.$service.'" data-callback="'.$callback.'" data-rel-field="'.$relField.'"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="table-container">{table}</div>
            </div>';

        $container = str_replace('{table}',$table,$container);

        return $container;
    }

    public function renderSelectInsert($element){
        $label = $element->getLabel();
        $element->setLabel('');
        $string = $this->render($element);

        $service = $element->getAttribute('data-service');
        $fieldReturn = $element->getAttribute('data-callback-field');
        $defaltData = $element->getAttribute('data-padrao');
        $disabled = $element->getAttribute('disabled');

        if($disabled) return $string;

        return '<div class="input-group">
                    '.$string.'
                    <div class="input-group-addon">
                        <i class="fa fa-plus" data-insert="true " data-service="'.$service.'" data-callback-field="'.$fieldReturn.'" data-padrao=\''.$defaltData.'\'></i>
                    </div>
                </div>'.$this->helpBlock($element);
    }

    private function renderMap($element)
    {
        $FieldLatitude = $element->getAttribute('data-latitude');
        $FieldLongitude = $element->getAttribute('data-longitude');
        $old = ($element->getValue()) ? (explode(',',$element->getValue())) : false;
        $this->view->OpenLayers()->LatLongMap($FieldLatitude,$FieldLongitude,$old);
    }

    public function renderCheckbox($element)
    {
        $value = $element->getValue();
        $checked = '';
        $display = 'block';
        if($value){
            $checked = 'checked="checked"';
            $display = 'none';
        }
        return '<div class="form-group checkbox">
                    <label>
                    <input type="hidden" id="'.$element->getAttribute('name').'_hidden" name="'.$element->getAttribute('name').'" value="0" style="display:'.$display.'">
                    <input type="checkbox" id="'.$element->getAttribute('name').'" name="'.$element->getAttribute('name').'" '.$checked.' value="1">
                    '.$element->getLabel().'
                    </label>
                </div>'.$this->helpBlock($element);
    }

    public function helpBlock($element)
    {
        if($element->getAttribute('help-block')){
            return '<p class="help-block">'.$element->getAttribute('help-block').'</p>';
        }
    }

}