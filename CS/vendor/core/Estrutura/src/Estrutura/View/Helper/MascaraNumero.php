<?php
namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class MascaraNumero extends AbstractHelper
{
    /**
     * @param string $data
     * @param bool $formato
     * @return \DateTime
     */
    public function __invoke($numero, $formato)
    {
        if($numero == '') return '';
        if($formato == 'money') return $this->maskMoney($numero);
        if($formato == 'percentage') return $this->maskPercentage($numero);
        $tipos = [
            'cnpj' => '##.###.###/####-##',
            'cpf' => '###.###.###-##',
            'cep' => '##.###-###'
        ];
        $mask = (in_array($formato, array_keys($tipos))) ? $tipos[$formato] : $formato;
        if($mask == 'telefone'){
            $mask = (strlen($numero) == 11) ? '(##) # ####-####' : '(##) ####-####';
        }
        return $this->mask($numero,$mask);
    }

    private function maskMoney($valor){
        return 'R$ '.number_format($valor,2,',','.');
    }

    private function maskPercentage($valor){
        return number_format($valor,2,',','.').' %';
    }

    private function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}