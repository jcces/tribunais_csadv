<?php
namespace Estrutura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class ControlAccess extends AbstractHelper
{
    public function __invoke($type,$text,$modulo,$action,$service=false,$att=[],$icon=false,$confirma=false)
    {
        if($type == 'button') return $this->renderButton($text,$modulo,$action,$service,$att,$icon,$confirma);
        if($type == 'link') return $this->renderLink($text,$modulo,$action,$service,$att,$icon,$confirma);
    }

    private function renderButton($text,$modulo,$action,$service,$att,$icon=false){
        $title = 'title="'.$text.'"';
        $stringAttr = '';
        if(!isset($att['class'])) $att['class'] = 'btn btn-default';
        foreach($att as $at => $value){
            $stringAttr .= $at.'="'.$value.'"';
        }
        $iconHtml = ($icon) ? "<i class='fa fa-$icon'></i>" : '';
        if($icon) $text = '';
        $source = $text.$iconHtml;
        return "<button $stringAttr $title>$source</button>";
    }

    private function renderLink($text,$modulo,$action,$service,$att,$icon,$confirma){
        $title = 'title="'.$text.'"';
        $stringAttr = '';
        foreach($att as $at => $value){
            $stringAttr .= $at.'="'.$value.'"';
        }
        $url = $this->buildUrl($modulo,$action,$service->getId());
        $iconHtml = ($icon) ? "<i class='fa fa-$icon'></i>" : '';
        $confirmaHtml = ($confirma) ? 'data-confirma="true" data-confirma-label="'.$text.'"' : '';
        if($icon) $text = '';
        $source = $text.$iconHtml;
        return "<a href=\"$url\" $stringAttr $confirmaHtml $title>$source</a>";
    }

    private function buildUrl($modulo,$action,$id=false){
        $url = '/'.$modulo.'/'.$action;
        if($id) $url .= '/'.$id;
        return $url;
    }
}