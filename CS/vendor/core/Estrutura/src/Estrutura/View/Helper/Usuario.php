<?php
namespace Estrutura\View\Helper;
use Zend\Session\Container;
use Zend\View\Helper\AbstractHelper;

class Usuario extends AbstractHelper
{
    public function __invoke()
    {
        $container = new Container('Usuario');
        $id = $container->offsetGet('id');

        $objUsuario = new \Usuario\Service\Usuario();
        if(!$id){
            $objUsuario->setNome('Não Logado');
            return $objUsuario;
        };

        $usuario = $objUsuario->buscar($id);
        return $usuario;
    }
}