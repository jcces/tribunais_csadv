<?php



namespace Estrutura\Validator;



use Zend\Validator\AbstractValidator;

use Zend\Validator\Exception;


/**
 * Ie = Inscriçao Estadual
 */

class Ie extends AbstractValidator

{

    const INVALID           = 'invalid';

    const WRONG_LENGTH      = 'wrongLength';



    /**

     * Validation failure message template definitions

     *

     * @var array

     */

    protected $messageTemplates = array(

        self::INVALID           => "A Inscrição Estadual informada é inválida",

        self::WRONG_LENGTH      => "A Inscrição Estadual informada deve conter 13 números",

    );



    /**

     * Sets validator options

     *

     * @param  array|Traversable $options

     * @throws Exception\InvalidArgumentException

     */

    public function __construct($options = null)

    {

        parent::__construct($options);

    }



    /**

     * Returns true if and only if $value is greater than min option

     *

     * @param  mixed $cpf

     * @return bool

     */

    public function isValid($ie)

    {

        $ie = preg_replace('/[^0-9.]+/', '', $ie);

        if(strlen($ie) != 13){
            $this->error(self::WRONG_LENGTH);
            return false;
        }
        if ($ie == '0000000000000') {
            $this->error(self::INVALID);
            return false;
        }

        return true;

    }



}