<?php

namespace Estrutura\Validator;

use Zend\Validator\AbstractValidator;

use Zend\Validator\Exception;

class Cpf extends AbstractValidator
{
    const INVALID           = 'invalid';
    const WRONG_LENGTH      = 'wrongLength';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID           => "O %s informado é inválido",
        self::WRONG_LENGTH      => "O %s informado deve conter 11 números",
    );

    /**
     * Sets validator options
     *
     * @param  array|Traversable $options
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($options = null)
    {
        parent::__construct($options);
    }

    /**
     * Returns true if and only if $value is greater than min option

     *

     * @param  mixed $cpf

     * @return bool

     */

    public function isValid($cpf)
    {
        $cleaned = '';
        for ($i = 0; $i < strlen($cpf); $i++) {
            $num = substr($cpf, $i, 1);
            if (ord($num) >= 48 && ord($num) <= 57) {
                $cleaned .= $num;
            }
        }

        $cpf = $cleaned;

        if (strlen($cpf) != 11) {
            $this->error(self::WRONG_LENGTH);
            return false;
        } elseif ($cpf == '00000000000') {
            $this->error(self::INVALID);
            return false;
        } else {
            $cpfTeste = '';
            for( $i = 0 ; $i < 10 ; $i++ )
            {
                $cpfTeste = str_pad( $cpfTeste , 11 , $i );
                if( $cpfTeste == $cpf )
                {
                    $this->error(self::INVALID);
                    return false;
                }
                $cpfTeste = '';
            }

            $number[0]  = intval(substr($cpf, 0, 1));
            $number[1]  = intval(substr($cpf, 1, 1));
            $number[2]  = intval(substr($cpf, 2, 1));
            $number[3]  = intval(substr($cpf, 3, 1));
            $number[4]  = intval(substr($cpf, 4, 1));
            $number[5]  = intval(substr($cpf, 5, 1));
            $number[6]  = intval(substr($cpf, 6, 1));
            $number[7]  = intval(substr($cpf, 7, 1));
            $number[8]  = intval(substr($cpf, 8, 1));
            $number[9]  = intval(substr($cpf, 9, 1));
            $number[10] = intval(substr($cpf, 10, 1));

            $sum = 10*$number[0]+9*$number[1]+8*$number[2]+7*$number[3]+
                6*$number[4]+5*$number[5]+4*$number[6]+3*$number[7]+
                2*$number[8];

            $sum -= (11*(intval($sum/11)));

            if ($sum == 0 || $sum == 1) {
                $result1 = 0;
            } else {
                $result1 = 11-$sum;
            }

            if ($result1 == $number[9]) {
                $sum = $number[0]*11+$number[1]*10+$number[2]*9+$number[3]*8+
                    $number[4]*7+$number[5]*6+$number[6]*5+$number[7]*4+
                    $number[8]*3+$number[9]*2;
                $sum -= (11*(intval($sum/11)));

                if ($sum == 0 || $sum == 1) {
                    $result2 = 0;
                } else {
                    $result2 = 11-$sum;
                }

                if ($result2 == $number[10]) {
                    return true;
                } else {
                    $this->error(self::INVALID);
                    return false;
                }
            } else {
                $this->error(self::INVALID);
                return false;
            }
        }
    }
}