<?php

namespace Estrutura\Filter;

use Zend\Filter\AbstractFilter;

class Decimal extends AbstractFilter
{
    public function filter($value)
    {
        $value = str_replace(['.',' ','R$'],'',$value);
        $value = str_replace(',','.',$value);
        return (float) $value;
    }
}
