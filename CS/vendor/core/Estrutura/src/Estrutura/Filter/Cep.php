<?php

namespace Estrutura\Filter;

use Zend\Filter\AbstractFilter;

class Cep extends AbstractFilter
{
    public function filter($value)
    {
        $value =  preg_replace("/[^0-9]/","", $value);;
        return $value;
    }
}
