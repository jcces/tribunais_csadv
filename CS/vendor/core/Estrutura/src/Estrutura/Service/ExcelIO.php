<?php

namespace Estrutura\Service;


class ExcelIO
{

    public function __construct(){
        /** PHPExcel */
        require_once 'vendor/PHPExcel/Classes/PHPExcel.php';
        /** PHPExcel_Writer_Excel2007 */
        require_once 'vendor/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
        /** PHPExcel_IOFactory */
        require_once 'vendor/PHPExcel/Classes/PHPExcel/IOFactory.php';
    }

    /***
     * Doc https://github.com/PHPOffice/PHPExcel
     */


    public $alphabet = [ 'A', 'B', 'C', 'D', 'E','F', 'G', 'H', 'I', 'J','K', 'L', 'M', 'N', 'O','P', 'Q', 'R', 'S', 'T','U', 'V', 'W', 'X', 'Y', 'Z'];

    /*
     * $params = [columnName string,type string,option array]
     * Ex.: type => Selection Options (options);
     *      option => [A,B,C]
     * */
    public function exportFromArray($data = [],$fileName,$params=[],$newLines=true,$path='')
    {


        $objPHPExcel = new \PHPExcel();
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Sentinel Live Analytics")
            ->setLastModifiedBy("Sentinel Live Analytics")
            ->setTitle($fileName)
            ->setSubject("Export ".$fileName)
            ->setDescription("Export ".$fileName)
            ->setKeywords("Export ".$fileName)
            ->setCategory("Export");

        $worksheetIndex = 0;
        foreach ($data as $worksheetName => $dataArr) {

            //DEFINE WORKSHEET
            if($worksheetIndex > 0){
                $objPHPExcel->createSheet($worksheetIndex);
            }

            $objPHPExcel->setActiveSheetIndex($worksheetIndex);
            // Rename worksheet
            $objPHPExcel->getActiveSheet()->setTitle($worksheetName);

            //PROTECTION
            $objPHPExcel->getActiveSheet()->getProtection()->setPassword('QZwsxedcRV');
            $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

            //DEFINE HEADER
            $header = array_keys($dataArr[0]);
            $line=1;
            foreach ($header as $key => $name) {
                $objPHPExcel->getActiveSheet()->setCellValue($this->alphabet[$key].$line, $name);
            }
            $styleHeader = array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '3c8dbc')
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => 'FFFFFF')
                ));
            $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->applyFromArray($styleHeader);
            $objPHPExcel->getActiveSheet()->freezePane( "A2" );


            $styleNormal = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD')
                    )
                )
            );

            $styleDisabled = array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ecf0f5')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD')
                    )
                )
            );

            $styleNewLines = array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'fffdc7')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'DDDDDD')
                    )
                )
            );

            //DEFINE DATA
            $colsProtected = [];
            $colsOptions = [];
            $col = 0;
            $model = false;
            foreach ($dataArr as $item) {
                if(!count(@array_count_values($item))) $model = true;

                $line++;
                $col = 0;
                foreach ($item as $key => $valueItem) {
                    $objPHPExcel->getActiveSheet()->setCellValue($this->alphabet[$col].$line, $valueItem);
                    if(isset($params[$key])){
                        //SELECT OPTION FEATURE
                        if(isset($params[$key]['option'])){
                            $objValidation2 = $objPHPExcel->getActiveSheet()->getCell($this->alphabet[$col].$line)->getDataValidation();
                            $objValidation2->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                            $objValidation2->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                            $objValidation2->setAllowBlank(false);
                            $objValidation2->setShowInputMessage(true);
                            $objValidation2->setShowDropDown(true);
                            $objValidation2->setPromptTitle('Resposta');
                            $objValidation2->setPrompt('Selecione');
                            $objValidation2->setErrorTitle('Ocorreu um erro.');
                            $objValidation2->setError('Opção de resposta não está dentro da lista.');
                            $objValidation2->setFormula1('"'.implode(",", $params[$key]['option']).'"');

                            $colsOptions[$this->alphabet[$col]] = $objValidation2->getFormula1();
                        }

                        //MARK AS PROTECTED
                        if(isset($params[$key]['disabled']) && $params[$key]['disabled']){
                            $objPHPExcel->getActiveSheet()->getStyle($this->alphabet[$col].$line)->applyFromArray($styleDisabled);
                            $colsProtected[] = $this->alphabet[$col];
                        }else{
                            $objPHPExcel->getActiveSheet()->getStyle($this->alphabet[$col].$line)->applyFromArray($styleNormal);
                            //UNPROTECTION
                            $objPHPExcel->getActiveSheet()->getStyle($this->alphabet[$col].$line.':'.$this->alphabet[$col].$line)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                        }

                    }else{
                        //UNPROTECTION
                        $objPHPExcel->getActiveSheet()->getStyle($this->alphabet[$col].$line.':'.$this->alphabet[$col].$line)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                    }
                    $col++;
                }
            }

            if($newLines){
                //UNPROTECTION MORE 100 LINES TO ADD MORE ROWS.
                $newline = $line+1;
                if($model) $newline = 2;
                $newlineMore100 = $newline+100;
                $collimited = $col;
                for ($i=$newline;$i<$newlineMore100;$i++){
                    for ($iC=0;$iC<$collimited;$iC++){
                        if(isset($colsOptions[$this->alphabet[$iC]])){
                            $objValidation2 = $objPHPExcel->getActiveSheet()->getCell($this->alphabet[$iC].$i)->getDataValidation();
                            $objValidation2->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                            $objValidation2->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                            $objValidation2->setAllowBlank(false);
                            $objValidation2->setShowInputMessage(true);
                            $objValidation2->setShowDropDown(true);
                            $objValidation2->setPromptTitle('Resposta');
                            $objValidation2->setPrompt('Selecione');
                            $objValidation2->setErrorTitle('Ocorreu um erro.');
                            $objValidation2->setError('Opção de resposta não está dentro da lista.');
                            $objValidation2->setFormula1($colsOptions[$this->alphabet[$iC]]);
                        }
                        if(in_array($this->alphabet[$iC],$colsProtected)){
                            $objPHPExcel->getActiveSheet()->getStyle($this->alphabet[$iC].$i)->applyFromArray($styleDisabled);
                        }else{
                            $objPHPExcel->getActiveSheet()->getStyle($this->alphabet[$iC].$i)->applyFromArray($styleNewLines);
                            $objPHPExcel->getActiveSheet()->getStyle($this->alphabet[$iC].$i)->getProtection()->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                        }
                    }
                }
            }

            $worksheetIndex++;

        }

        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $outPath = 'php://output';
        if($path) $outPath = $path.$fileName.'.xlsx';
        $objWriter->save($outPath);

        exit;

    }

    public function fileToArray($file)
    {


//        $filename = $files['fileXls']['tmp_name'];
        $type = \PHPExcel_IOFactory::identify($file);
        $objReader = \PHPExcel_IOFactory::createReader($type);
        $objPHPExcel = $objReader->load($file);

        $worksheets = [];
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $worksheets[$worksheet->getTitle()] = $worksheet->toArray();
        }

        if (!count($worksheets)) throw new \Exception('Planilha está vazia.');

        $countHeader = 0;
        $header = [];
        $tratados = [];
        foreach ($worksheets as $worksheetName => $data) {
            foreach ($data as $row => $value) {
                if(str_replace(' ','',implode('',$value)) == '') continue;
                if ($row == 0) { //HEADER
                    $header = array_filter($value);
                    $countHeader = count($header);
                } else {
                    $aux = [];
                    for ($i = 0; $i < $countHeader; $i++) {
                        $aux[$header[$i]] = $value[$i];
                    }
                    $tratados[$worksheetName][] = $aux;
                }
            }
        }

        return $tratados;
    }
    
    public function createReader($file,$type='Excel2007'){

        $excel2 = \PHPExcel_IOFactory::createReader($type);
        $excel2 = $excel2->load($file);
        return $excel2;
    }

    public function clearCache(){
        \PHPExcel_Calculation::getInstance()->clearCalculationCache();
    }
    
    public function output($excel, $output,$type='Excel2007',$file=false){

        if($file){
            ob_start();
            // Redirect output to a client’s web browser (Excel5)
            @header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            @header('Content-Disposition: attachment;filename="'.$output.'.xlsx"');
            $objWriter = \PHPExcel_IOFactory::createWriter($excel, $type);
            $objWriter->save('php://output');
            $data = ob_get_contents();
            ob_clean();

            file_put_contents($file,$data);

            //echo 'Salvo no Arquivo';
        }else{
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$output.'.xlsx"');
            $objWriter = \PHPExcel_IOFactory::createWriter($excel, $type);
            //$objWriter->setPreCalculateFormulas();
            $objWriter->save('php://output');
        }
    }

}