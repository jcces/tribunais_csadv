<?php

namespace Estrutura\Service;

use Estrutura\Table\AbstractEstruturaTable;
use Manager\Service\Attach;
use Manager\Service\Log;
use Manager\Service\User;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Form\Element\DateTime;
use Zend\Session\Container;
use Zend\Stdlib\Hydrator\ArraySerializable;

class AbstractEstruturaService{
    /**
     * @var AbstractEstruturaTable
     */
    protected $table;
    protected $config;
    protected $hydrator;
    protected static $adapter;
    protected static $sm;

    public $admin = 'b6fa8ef2-fe44-4268-b587-e3e59d46a568';

    public static function setServiceManager($sm)
    {
        self::$sm = $sm;
    }

    public function getServiceLocator(){
        return self::$sm;
    }

    public function exchangeArray($data){
        foreach($data as $chave => $item){
            $metodo = 'set'.strtoupper($chave);
            if(method_exists($this, $metodo)){
                $this->$metodo(trim($item));
            }
        }
    }

    public function getTable(){
        if(!$this->table)
        {
            $dbAdapter = $this->getAdapter();
            $tableName = $this->getTableName();

            if(class_exists($tableName)) $this->table = new $tableName();

            $this->hydrator  = (isset($this->table) && isset($this->table->table) ) ? new \Estrutura\Table\TableEntityMapper($this->table->campos) : new ArraySerializable();
            $rowObjectPrototype = $this;

            $resultSet = new \Zend\Db\ResultSet\HydratingResultSet($this->hydrator,$rowObjectPrototype);
            $tableGateway = new TableGateway(isset($this->table->table)?$this->table->table:'', $dbAdapter, null, $resultSet);
            $this->table->setTableGateway($tableGateway);
        }
        return $this->table;
    }

    public function toArray(){
        $classe = new \ReflectionClass($this);
        $item = [];
        foreach( $classe->getProperties() as $property){
            if(!preg_match('/Entity/',$property->getDeclaringClass()->getName())) continue;
            $valor = method_exists( $this , 'get'.ucfirst($property->getName()) ) ? $this->{'get'.ucfirst($property->getName())}() : null ;

            if($valor instanceof AbstractEstruturaService )
                $item[$property->getName()] = $valor->toArray();
            elseif ($valor instanceof \DateTime )
                $item[$property->getName()] = $valor->format('d/m/Y');
            elseif ( preg_match('/Dt/', $property->getName()) ){
                if($valor == '0000-00-00'){
                    $valor = '';
                }else {
                    if (preg_match('/-/', $valor)) {
                        $format = 'Y-m-d';
                        $retorno = 'd/m/Y';
                        if (strlen($valor) > 10) {
                            $format .= ' H:i:s';
                            $retorno .= ' H:i:s';
                        }
                        $dateTime = \DateTime::createFromFormat($format, $valor);
                        $valor = $dateTime->format($retorno);
                    }
                }

                $item[$property->getName()] = $valor;
            }else
                $item[$property->getName()] = $valor;
        }
        return $item;
    }

    public function hydrate($attribute=null,$clear=false){
        $this->getTable();
        $obj = $this;
        $arr = $this->hydrator->extract($obj);

        if($clear)
        {
            $arr = array_filter($arr,function($item){ return $item!==null;});
        }

        if($attribute)
        {
            if(is_string($attribute)) $attribute = array($attribute);
            $arrFields = array_intersect($this->table->campos,$attribute);
            $arrFields = array_keys($arrFields);

            $arrFiltrado = array();
            foreach($arrFields as $field )
            {
                $arrFiltrado[$field] = (array_key_exists($field,$arr))? $arr[$field] : null;
            }
            $arr = $arrFiltrado;
        }
        foreach($arr as $key => $item){
            if((preg_match('/DT_/',$key) && preg_match('@/@', $item)) || preg_match('/_DT/',$key) && preg_match('@/@', $item)){

                $format = 'd/m/Y';
                $saida = 'Y-m-d';

                $tamanho = strlen($item);
                if($tamanho > 10){
                    if($tamanho < 19) $item .= ':00';
                    $format .= ' H:i:s';
                    $saida  .= ' H:i:s';
                }

                $dateTime = \DateTime::createFromFormat($format, $item);
                $arr[$key] = $dateTime->format($saida);
            }

            if((preg_match('/DT_/',$key) && $item == '') || preg_match('/_DT/',$key) && $item == ''){
                $arr[$key] = null;
            }
        }
        return($arr);
    }

    private function getTableName()
    {
        $obj = str_replace('\Service\\','\Table\\', get_class($this));
        return $obj;
    }

    public function getConfig(){
        if(!$this->config){
            $this->config = Config::getConfig('db');
        }
        return $this->config;
    }

    public function getAdapter(){
        if(self::$adapter) return self::$adapter;
        self::$adapter = new \Zend\Db\Adapter\Adapter($this->getConfig());

        return self::$adapter;
    }

    public function fetchAll(){
        $where = ['DELETED'=>0];
        return $this->select($where);
    }

    public function select($where=null){
        return $this->getTable()->select($where);
    }

    public function selectFull($where=false,$order='ID',$limit=1000)
    {
        $select = new Select();
        $select->from($this->getTable()->table);

        $select->where($where);
        $select->order($order);
        $select->limit($limit);

        return $this->getTable()->selectWith($select);

    }

    public function filtrarObjeto(){
        if(method_exists($this,'setDeleted')) $this->setDeleted(0);
        $where = $this->hydrate();
        $wTratado = new Where();
        foreach($where as $chave => $valor){
            if($chave == 'NOME'){
                $wTratado->like($chave, '%'.$valor.'%');
            }else{
                $wTratado->equalTo($chave, $valor);
            }
        }

        return $this->select($wTratado);
    }

    public function saveAttach($anexos = array())
    {
        if(!is_array($anexos) || !count($anexos)) return;
        foreach($anexos as $item){
            $anexo = new Attach();
            $anexo->setIdOrigin($this->getId());
            $anexo->setOrigin(get_class($this));
            $anexo->setPath($item);
            $anexo->salvar();
        }
    }

    public function fetchAttach(){
        if(!$this->getId()) return [];

        $anexo = new Attach();
        $anexo->setIdOrigin($this->Id);
        $anexo->setOrigin(get_class($this));
        return $anexo->filtrarObjeto();
    }

    public function deleteAttach(){
        $attach = new Attach();
        $attach->setOrigin(get_class($this));
        $attach->setIdOrigin($this->getId());

        $attach->deleteAttach();
    }

    public function salvar($admin=false){
        $this->preSave();

        $this->setDefaultValues($admin);

        $dados = $this->hydrate();

        $where = null;

        if($this->getId()){
            if(!$field = $this->fieldName('id')){
                $field = $this->fieldName('Id');
            }
            $where = [$field =>$this->getId()];
        }else{
            $dados['ID'] = $this->generateGUID();
        }

        $result = $this->getTable()->salvar($dados, $where);
        if(is_string($result)){
            $this->setId($dados['ID']);
        }

        if($where){
            $this->logRegister($this,'update');
        }else{
            $this->logRegister($this,'insert');
        }

        $this->posSave();
        return $this;
    }

    public function preSave(){}
    public function posSave(){}

    public function preDelete(){}
    public function posDelete(){}

    public function excluir($forced=false){
        if($forced){
            $this->preDelete();
            $arr = $this->hydrate();
            $ret = $this->getTable()->delete($arr);
            $this->posDelete();
            return $ret;
        }else{
            //$this->deleteImg();
            $this->preDelete();
            $this->logRegister($this,'delete');

            $this->setDeleted(1);
            $ret = $this->salvar();
            //$arr = $this->hydrate();
            //$ret = $this->getTable()->delete($arr);
            $this->posDelete();

            /// Deleta os Attach
            $this->deleteAttach();
            return $ret;
        }
    }

    public function deleteImg(){
        $table = $this->getTable();
        $exclude = [];
        foreach($table->campos as $field => $method){
            if(preg_match('/IMG_/',$field)) $exclude[] = $method;
        }
        if(count($exclude)){
            $listaObj = $this->filtrarObjeto();
            foreach($listaObj as $item){
               foreach($exclude as $attr){
                   $method = 'get'.$attr;
                   if($item->{$method}()){
                       @unlink($item->{$method}());
                   }
               }
            }
        }
    }

    public function load(){
        $arr = $this->hydrate(['Id']);
        $key = md5(json_encode($arr).get_class($this));
        $dados = $this->getItemCache($key);

        if(!$dados){
            $dados = $this->select($arr)->current();
            if(!$dados) return false;
            $dados = $dados->toArray();
            $this->setItemCache($key,$dados);
        }

        $this->exchangeArray($dados);
        return $this;
    }

    public function buscar($id)
    {
        $this->setId($id);
        return $this->filtrarObjeto()->current();
    }

    public function fieldName($attribute)
    {
        return (($chave = array_search($attribute,$this->table->campos))!==false) ? $chave : null ;
    }

    public function toDate($campo,$formato='Y-m-d'){
        $method = 'get'.$campo;
        $date = \DateTime::createFromFormat($formato,$this->{$method}());
        return $date;
    }

    public function toDateTime($campo,$formato='Y-m-d H:i:s'){
        $method = 'get'.$campo;
        $datetime = \DateTime::createFromFormat($formato,$this->{$method}());
        return $datetime;
    }

    public function checkLoggedUser($field='Login')
    {
        $sessao = new Container('autenticacao');
        return $sessao->autenticacao[$field];
    }

    public function hashifyPost($post,$returnHash=false){
        $hash = '';
        foreach($post as $item){
            $hash = $hash.$item;
        }
        $hash = $hash.date("Y-m-d H:i:s");
        $hash = hash('md5',$hash);

        $post['Hash'] = $hash;

        if($returnHash){
            return $hash;
        }

        return $post;
    }

    public function addPeriodToDateTime($formato,$date,$period){
        $date = \DateTime::createFromFormat($formato,$date);
        $date->modify($period);
        return $date->format($formato);
    }

    public function logRegister($obj,$tipo)
    {
        if(!$this->checkLoggedUser('Id')) return true;
        if($obj instanceof \Manager\Service\Log) return;
        //return true;
        $dados = json_encode((is_object($obj))?$obj->toArray():$obj);
        $log = new Log();
        $log->setType($tipo);
        $log->setTableName((!method_exists($obj,'getTable'))?'none':$obj->getTable()->table);
        $log->setFields($dados);
        $log->setIdUser($this->checkLoggedUser('Id'));
        $hostname = getHostName();
        if($hostname) $log->setHostname(getHostName());
        if(getHostByName($hostname)) $log->setIp(getHostByName($hostname));
        if(isset($_SERVER['HTTP_USER_AGENT'])) $log->setBrowser($_SERVER['HTTP_USER_AGENT']);

        $log->salvar(true);
    }

    /**
     * Default Method for Get Nome
     */
    public function getName(){
        return 'Registro #'.$this->getId();
    }

    /**
     * Generic Get of fields the object
     */
    public function get($field,$params=false){
        $method = 'get'.$field;
        if(method_exists($this,$method)){
            return $this->{$method}($params);
        }
    }

    public function buildObjToReturn(){

    }

    public function getForm($obj = true){
        $formString = str_replace('Service','Form',get_class($this));
        if($obj){
            $formObject = new $formString();
            return $formObject;
        }
        return $formString;
    }

    public function generateGUID() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    private function setDefaultValues($admin = false)
    {
        if ($admin) {
            if ($this->getId()) { /// Seta os valores padrões para update
                $this->setUpdatedBy($this->admin);
            } else { /// Seta os valores padrões para Inserir
                $this->setCreatedBy($this->admin);
                $this->setUpdatedBy($this->admin);
            }
        } else {
            $usuario = new Container('autenticacao');
            if ($this->getId()) { /// Seta os valores padrões para update
                $this->setUpdatedBy($usuario['autenticacao']['Id']);
            } else { /// Seta os valores padrões para Inserir
                $this->setCreatedBy($usuario['autenticacao']['Id']);
                $this->setUpdatedBy($usuario['autenticacao']['Id']);
            }
        }

        $this->setDtUpdated(date('Y-m-d H:i:s'));
        if (!$this->getId()) {
            $this->setDtCreated(date('Y-m-d H:i:s'));
            $this->setDeleted(0);
        }

    }

    public function getCacheObject(){
        $cache = new \Zend\Cache\Storage\Adapter\Filesystem();
        $cache->getOptions()->setTtl(5);
        return $cache;
    }

    public function getItemCache($key){
        $cache = $this->getCacheObject();
        $item = $cache->getItem($key);
        if(!$item) return false;

        return json_decode($item, true);
    }

    public function setItemCache($key,$data){
        $cache = $this->getCacheObject();
        $cache->setItem($key,json_encode($data));
    }

    public $registerDetails = false;
    public function getRegisterDetails(){
        if(!$this->registerDetails){
            $table = $this->getTable()->table;
            $sql = "SELECT 
                UPDATED.NAME AS UPDATED_BY,
                CREATED.NAME AS CREATED_BY,
                DATE_FORMAT(REL.DT_CREATED,'%d/%m/%Y %T') AS CREATED,
                DATE_FORMAT(REL.DT_UPDATED,'%d/%m/%Y %T') AS UPDATED
                FROM 
                {$table} REL
                INNER JOIN MANAGER_USER CREATED ON CREATED.ID = REL.CREATED_BY
                INNER JOIN MANAGER_USER UPDATED ON UPDATED.ID = REL.UPDATED_BY
                WHERE REL.ID = '{$this->getId()}'";
            $this->registerDetails = Conexao::lerSql($sql);
        }

        return $this->registerDetails;
    }

    public function getUserCreated(){
        $user = new User();
        $user->setId($this->getCreatedBy());
        return $user->load();
    }

    public function getUserUpdated(){
        $user = new User();
        $user->setId($this->getUpdatedBy());
        return $user->load();
    }

    public $offsetData = [];
    public function populateService($service, $data){
        $return = [];
        $campos = $service->getTable()->campos;
        $class  = get_class($service);
        foreach($data as $reg){
            $newService = new $class;
            foreach($reg as $key => $value){
                if(isset($campos[$key])){
                    $method = 'set'.$campos[$key];
                    $newService->{$method}($value);
                }else{
                    $newService->offsetData[$key] = $value;
                }
            }
            $return[] = $newService;
        }
        return $return;
    }

    public function deleteAll(){
        $sql = 'DELETE FROM '.$this->getTable()->table;
        return Conexao::listarSql($sql);
    }

}