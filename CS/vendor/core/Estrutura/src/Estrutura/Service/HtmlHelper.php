<?php

namespace Estrutura\Service;

class HtmlHelper{
    public static function botaoLink($url, $icone, $attributos=[]){
        $attributos = self::arrayToString($attributos);

        $botao = '<a href="'.$url.'" '.$attributos.'>
                     <button title="Alterar" type="button" class="btn btn-primary">
                       <span class="'.$icone.'"></span>
                     </button>
                  </a>';

        return $botao;
    }

    public static function botaoExcluir($url){
        $attributos = ['class'=>'btn-excluir'];
        return self::botaoLink($url, 'fa fa-minus', $attributos);
    }

    public static function botaoAlterar($url){
        $attributos = ['class'=>'btn-alterar'];
        return self::botaoLink($url, 'fa fa-edit', $attributos);
    }

    private static function arrayToString($array){
        $string = '';
        foreach($array as $chave => $item){
            $string .= "{$chave}='{$item}' ";
        }
        return $string;
    }
}