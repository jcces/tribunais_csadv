<?php

namespace Estrutura\Service;

class HighCharts {

    public $params;

    public $pie = '{
        "chart": {
            "plotBackgroundColor": null,
            "plotBorderWidth": null,
            "plotShadow": false,
            "type": "pie"
        },
        "credits": { "enabled": false },
        "title": {
            "text": ""
        },
        "tooltip": {
            "pointFormat": "{series.name}: <b>{point.percentage:.1f}%</b>"
        },
        "plotOptions": {
            "pie": {
                "allowPointSelect": true,
                "cursor": "pointer",
                "dataLabels": {
                    "enabled": true
                },
                "showInLegend": true
            }
        },
        "series": []
    }';

    /* EXAMPLE OF SERIES DATA
     * series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Microsoft Internet Explorer',
                y: 56.33
            }, {
                name: 'Chrome',
                y: 24.03,
                sliced: true,
                selected: true
            }, {
                name: 'Firefox',
                y: 10.38
            }, {
                name: 'Safari',
                y: 4.77
            }, {
                name: 'Opera',
                y: 0.91
            }, {
                name: 'Proprietary or Undetectable',
                y: 0.2
            }]
        }]
     * */

    public $column = '{
        "chart": {
            "plotBackgroundColor": null,
            "plotBorderWidth": null,
            "plotShadow": false,
            "type": "column"
        },
        "credits": { "enabled": false },
        "title": {
            "text": ""
        },
        "subtitle": {
            "text": ""
        },
        "xAxis": {
            "type": "category"
        },
        "yAxis": {
            "min": 0,
            "title": {
                "text": ""
            }
        },
        "legend": {
            "enabled": false
        },
        "plotOptions": {
            "column": {
                "pointPadding": 0.2,
                "borderWidth": 0
            },
            "series": {
                "colorByPoint": true,
                "borderWidth": 0,
                "dataLabels": {
                    "enabled": true
                }
            }
        },
        "series": []
    }';

    /* EXAMPLE OF SERIES DATA
     * series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Microsoft Internet Explorer',
            y: 56.33
        }, {
            name: 'Chrome',
            y: 24.03
        }, {
            name: 'Firefox',
            y: 10.38
        }]
    }],
     *
     * */

    public $line = '{
        "chart": {
            "plotBackgroundColor": null,
            "plotBorderWidth": null,
            "plotShadow": false,
            "type": "line"
        },
        "credits": { "enabled": false },
        "title": {
            "text": ""
        },
        "subtitle": {
            "text": ""
        },
        "yAxis": {
            "title": {
                "text": ""
            }
        },
        "xAxis": {
            "type": "category"
        },
        "plotOptions": {
            "series": {
                "point":[]
            }
        },
        "series": []
    }';

//"pointStart": 2010,

    /* EXAMPLE OF SERIES DATA
     * series: [{
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    }, {
        name: 'Manufacturing',
        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
    }, {
        name: 'Sales & Distribution',
        data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
    }, {
        name: 'Project Development',
        data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
    }, {
        name: 'Other',
        data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
    }]
     * */

    function __construct(){}

    function params($type){
        $this->params = json_decode($this->{$type},true);
    }

    function render($type,$params=[],$series=[]){

        $this->params($type);

        foreach ($params as $key=>$value) {
            $method = 'set'.ucfirst($key);
            $this->{$method}($value);
        }

        $this->setSerie($series);

        echo '<div class="highcharts" id="'.hash('md5',rand(1,1000000000)).'" data-params=\''.json_encode($this->params).'\'></div>';

    }

    /**
     * SETTERS
     */
    function setTitle($title) {
        $this->params['title']['text'] = ucwords($title);
    }
    function setSubTitle($subTitle) {
        $this->params['subtitle']['text'] = ucwords($subTitle);
    }
    function setColors($colors) {
        $this->params['colors'] = $colors;
    }
    function setSerie($data) {
        if(in_array($this->params['chart']['type'],['column','line'])){
            $this->params['series'] = $data;
        }else{
            $this->params['series'][] = $data;
        }
    }
    function setXcategories($label=[]){
        $this->params['xAxis']['categories'] = $label;
    }
    function setYtitle($label=[]){
        $this->params['yAxis']['title']['text'] = $label;
    }

    function setPoints($points=[]){
        $this->params['plotOptions']['series']['point'] = $points;
    }

} 