<?php

namespace Estrutura\Service;

use setasign\Fpdi;

class PdfService
{
    public $files = array();
    public $service;
    public $byContent = [];

    public function __construct(){

        require_once('./vendor/setasign/fpdi/src/autoload.php');
        require_once('./vendor/setasign/fpdi-2/src/autoload.php');
        $this->service = new Fpdi\Fpdi();
    }


    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function concat()
    {
        foreach($this->files AS $file) {
            $pageCount = $this->service->setSourceFile($file);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                $pageId = $this->service->ImportPage($pageNo);
                $s = $this->service->getTemplatesize($pageId);
                $this->service->AddPage($s['orientation'], $s);
                $this->service->useImportedPage($pageId);
            }
        }
    }

    public function setFilesByContent($array){

        foreach($array as $content){
            $temp = './data/pdf_temp/'.rand(0,999999999999999999).'_content.pdf';
            $this->byContent[] = $temp;
            file_put_contents($temp,$content);
        }
        $this->setFiles($this->byContent);
    }

    public function output($file=false){
        foreach($this->byContent as $old){
            @unlink($old);
        }
        if($file){
            return $this->service->output($file,'F');
        }else{
            $this->service->output();
            die;
        }
    }

    public function clearTemp()
    {
        foreach($this->byContent as $old){
            @unlink($old);
        }
        $this->byContent = [];
    }
}