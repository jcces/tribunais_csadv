<?php

namespace Estrutura\Service;


use Zend\View\Model\JsonModel;

class ApiModel
{
    protected $data;
    public function __construct($error,$data=[],$message='')
    {
        $this->data = new JsonModel(['error'=>$error,'data'=>$data,'datetime'=>date('Y-m-d H:i:s'),'message'=>$message]);
    }

    public function finish(){
        return $this->data;
    }
}