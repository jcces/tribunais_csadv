<?php

namespace Estrutura\Service;


class OpenLayers {
    function SimpleMap($latitude,$longitude,$zoom=12,$showMe=false,$icon='default'){
        echo '<div id="mapa-simples" data-latitude="'.$latitude.'" data-zoom="'.$zoom.'" data-longitude="'.$longitude.'" data-icon="'.$icon.'" data-show-me="'.$showMe.'"></div>';
    }
    public function LatLongMap($latitude=false,$longitude=false,$old=false){
        $fieldLatitude = ($latitude) ? 'data-latitude="'.$latitude.'"' : '';
        $fieldLongitude = ($longitude) ? 'data-longitude="'.$longitude.'"' : '';
        $fieldOld = ($old) ? 'data-old-latitude="'.$old[0].'" data-old-longitude="'.$old[1].'"' : '';

        echo '<div id="mapa-pin" '.$fieldLatitude.$fieldLongitude.$fieldOld.'></div>';
    }
    public function MarkersMap($markers,$showMe=false,$icon='default'){
        $jsonMarkers = json_encode($markers);
        echo '<div id="mapa-markers" data-markers=\''.$jsonMarkers.'\' data-icon="'.$icon.'" data-show-me="'.$showMe.'"></div>';
    }

    function getDistance($origem, $destino)
    {
        $lat1 = $origem[0];
        $long1 = $origem[1];
        $lat2 = $destino[0];
        $long2 = $destino[1];

        $earth = 6371; //km change accordingly
        $lat1 = deg2rad($lat1);
        $long1= deg2rad($long1);
        $lat2 = deg2rad($lat2);
        $long2= deg2rad($long2);
        $dlong=$long2-$long1;
        $dlat=$lat2-$lat1;
        $sinlat=sin($dlat/2);
        $sinlong=sin($dlong/2);
        $a=($sinlat*$sinlat)+cos($lat1)*cos($lat2)*($sinlong*$sinlong);
        $c=2*asin(min(1,sqrt($a)));
        $d=round($earth*$c);
        return $d;
    }

    public function getDistancePrecise($origem,$destino,$mode='carro'){
        $modos = ['bike'=>'bicycling','carro'=>'driving','pe'=>'walking'];
        $lat1 = $origem[0];
        $long1 = $origem[1];
        $lat2 = $destino[0];
        $long2 = $destino[1];
        $result = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=".$modos[$mode]."&language=pt-BR"),true);
        $dist = $result['rows'][0]['elements'][0]['distance']['text'];
        $time = $result['rows'][0]['elements'][0]['duration']['text'];

        return array('distance' => (float) str_replace([' km',','],['','.'],$dist), 'time' => $time);
    }

    public function getDetaisLocation($latitude,$longitude){
        $json = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude),true);
        $return = [];
        foreach($json['results'][0]['address_components'] as $address){
            if(in_array('administrative_area_level_4',$address['types'])){
                $return['bairro'] = $address['long_name'];
            }
            if(in_array('route',$address['types'])){
                $return['endereco'] = $address['long_name'];
            }
        }
        return $return;
    }
} 