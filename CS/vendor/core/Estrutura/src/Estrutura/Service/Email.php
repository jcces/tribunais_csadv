<?php

namespace Estrutura\Service;

use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
Use Zend\Mime;

class Email{

    const EMAIL = 'bruno.app2b@gmail.com';
    const PORTA = '587';
    const SENHA = 'brunorosasilva1';
    const HOST  = 'smtp.gmail.com';
    const SSL   = "tls";

    protected $mail;
    protected $button;

    function __construct(){
        $this->mail = new Mail\Message();
    }

    public function definePath($link)
    {
        return (isset($_SERVER['HTTPS']) ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].$link;
    }

    public function body($body){

        $body = $this->defineTemplate($body);

        $html = new \Zend\Mime\Part($body);
        $html->type = \Zend\Mime\Mime::TYPE_HTML;
        $html->disposition = \Zend\Mime\Mime::DISPOSITION_INLINE;
        $html->encoding = \Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE;
        $html->charset = 'UTF-8';

        $body = new \Zend\Mime\Message();

        $body->addPart($html);

        $this->mail->setBody($body);
    }

    public function para($para,$nome=null){

        $this->mail->addTo($para, $nome);
        if(APPLICATION_ENV == 'development'){
            $this->mail->addTo('contato@brunorosa.me', 'Bruno Rosa');
        }else{
            $this->mail->addTo($para, $nome);
        }
    }

    public function copia($para){
        $this->mail->addBcc($para);
    }

    public function assunto($assunto){
        $this->mail->setSubject($assunto);
    }

    public function linkButton($href,$text)
    {
        $this->button = '
        <tr>
            <td height="30"></td>
        </tr>
        <tr>
            <td align="center">
                <table class="textbutton" align="center" bgcolor="#3cb2d0" border="0"
                       cellspacing="0" cellpadding="0"
                       style=" border-radius:4px; box-shadow: 0px 2px 0px #dedfdf;">
                    <tbody>
                    <tr>
                        <td data-link-style="text-decoration:none; color:#ffffff;"
                            data-link-color="Button Link" data-size="Button" mc:edit="noti-2-5"
                            height="55" align="center"
                            style="font-family: \'Open Sans\', Arial, sans-serif; font-size:16px; color:#FFFFFF;font-weight: bold;padding-left: 25px;padding-right: 25px;">
                            <a href="[link-href]" style="text-decoration:none; color:#ffffff"
                               data-color="Button Link">
                                <singleline label="button">[link-text]</singleline>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="30"></td>
        </tr>';

        $this->button = str_replace('[link-href]',$this->definePath($href),$this->button);
        $this->button = str_replace('[link-text]',$text,$this->button);
    }

    public function defineTemplate($body)
    {
        if(!$this->mail->getSubject()) throw new \Exception('Defina o assunto antes do template.');
        $template = file_get_contents('./public/email/email.html');
        $template = str_replace('[logo]',$this->definePath('/assets/img/logo/Sentinel_Logo_Transparente_H100px.png'),$template);
        $template = str_replace('[title]',$this->mail->getSubject(),$template);
        $template = str_replace('[content]',$body,$template);
        if($this->button){
            $template = str_replace('[button]',$this->button,$template);
        }else{
            $template = str_replace('[button]','',$template);
        }

        return $template;
    }

    function enviar($email=NULL,$nome=NULL){
        try{
            $options = new SmtpOptions( array(
                "name" => "gmail",
                "host" => self::HOST,
                "port" => self::PORTA,
                "connection_class" => "login",
                "connection_config" => array(
                    "username" => self::EMAIL,
                    "password" => self::SENHA,
                    'ssl' => 'tls' )
            ) );

            if($email && $nome){
                $this->mail->setFrom($email, $nome);
            }else{
                $this->mail->setFrom(self::EMAIL, Config::getConfig('nomeProjeto')['long']);
            }

            $transport = new SmtpTransport();
            $transport->setOptions( $options );

            $transport->send($this->mail);

            return true;
        }catch (\Exception $e){
            debug($e->getMessage());
            return false;
        }
    }

    public function anexo($arquivo, $nomeArquivo='anexo')
    {
        $text = new Mime\Part($this->mail->getBody());
        $text->type = Mime\Mime::TYPE_TEXT;
        $text->charset = 'utf-8';

        $fileContents = fopen($arquivo, 'r');
        $attachment = new Mime\Part($fileContents);
        $attachment->type = Mime\Mime::TYPE_TEXT;
        $attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;
        $attachment->filename = $nomeArquivo;
        $attachment->charset = 'utf-8';

        $mimeMessage = new Mime\Message();
        $mimeMessage->setParts(array($text, $attachment));

        $this->mail->setBody($mimeMessage);
    }

    public function getConstEmail(){
        return self::EMAIL;
    }
}