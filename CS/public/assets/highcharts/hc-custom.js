/**
 * Created by bruno on 29/7/2017.
 */
$(function(){
    renderHighCharts();
});

function renderHighCharts() {
    if($('.highcharts').length){
        $.each($('.highcharts'),function () {
            var name = $(this).attr('id');
            var params = JSON.parse($(this).attr('data-params'));
            console.log(params);
            Highcharts.chart(name, params);
        })
    }

}