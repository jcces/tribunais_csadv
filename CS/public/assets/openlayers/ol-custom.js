$(function(){
    renderMap();
})

function renderMap() {
    if($("#mapa-simples").length && $('#mapa-simples').is(':empty')){
        var latitude = parseFloat($('#mapa-simples').attr('data-latitude'));
        var longitude = parseFloat($('#mapa-simples').attr('data-longitude'));
        var icon = $('#mapa-simples').attr('data-icon');
        var showMe = $('#mapa-simples').attr('data-show-me');
        var zoom = $('#mapa-simples').attr('data-zoom');

        var mapaSimples = new ol.Map({
            target: 'mapa-simples',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                }),
                getVectorLayer('SimplesLayer',latitude,longitude,icon,true)
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([longitude, latitude]),
                zoom:zoom
            })
        });

        if(showMe == 1){
            myPosition(function(position){
                mapaSimples.addLayer(getVectorLayer('MinhaPosicao',position.latitude,position.longitude,'professional',true));
            });
        }
    }
    
    if($("#mapa-markers").length && $('#mapa-markers').is(':empty')){
        var jsonMarkers = $('#mapa-markers').attr('data-markers');
        var markers = JSON.parse(jsonMarkers);
        var showMe = $('#mapa-markers').attr('data-show-me');
        var icon = $('#mapa-markers').attr('data-icon');

        var mapaMarkers = new ol.Map({
            target: 'mapa-markers',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([0, 0]),
                zoom: 12
            })
        });
        
        if(showMe == 1){
            myPosition(function(position){
                mapaMarkers.setView(new ol.View({
                    center: ol.proj.fromLonLat([position.longitude, position.latitude]),
                    zoom: 10
                }));
                mapaMarkers.addLayer(getVectorLayer('MinhaPosicao',position.latitude,position.longitude,'professional',true));
            })
        }else{
            var position = markers[0];
            mapaMarkers.setView(new ol.View({
                center: ol.proj.fromLonLat([parseFloat(position.longitude), parseFloat(position.latitude)]),
                zoom: 10
            }));
        }

        $.each(markers, function(i,e){
            if(e.latitude && e.longitude){
                var lat = parseFloat(e.latitude);
                var long = parseFloat(e.longitude);
                var finalIcon = (e.icon) ? e.icon : icon;
                mapaMarkers.addLayer(getVectorLayer('Marker'+i, lat, long,finalIcon,true));
            }
        })
    }
    
    if($("#mapa-pin").length && $('#mapa-pin').is(':empty')){
        var mapPin = new ol.Map({
            target: 'mapa-pin',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([-47.863826, -15.793288]),
                zoom: 8
            })
        });

        var oldLatitude = parseFloat($('#mapa-pin').attr('data-old-latitude'));
        var oldLongitude = parseFloat($('#mapa-pin').attr('data-old-longitude'));

        if(!oldLatitude || !oldLongitude){
            myPosition(function(position){
                mapPin.setView(new ol.View({
                    center: ol.proj.fromLonLat([position.longitude, position.latitude]),
                    zoom: 15
                }));
                mapPin.addLayer(getVectorLayer('meuLocal',position.latitude,position.longitude,'commercial-places',true));
            })
        }

        if(oldLatitude && oldLongitude){
            mapPin.setView(new ol.View({
                center: ol.proj.fromLonLat([oldLongitude,oldLatitude]),
                zoom: 15
            }));
            mapPin.addLayer(getVectorLayer('meuLocal',oldLatitude,oldLongitude,'commercial-places',true));
        }

        var clickLayer = false;

        mapPin.on('singleclick', function(evt) {
            if(clickLayer){
                mapPin.removeLayer(clickLayer);
            }
            clickLayer = getVectorLayer('clickLayer',evt.coordinate[1],evt.coordinate[0],'tutors',false);
            mapPin.addLayer(clickLayer);

            var campoLatitude = ($('#mapa-pin').attr('data-latitude')) ? '#'+$('#mapa-pin').attr('data-latitude') : '#Latitude';
            var campoLongitude = ($('#mapa-pin').attr('data-longitude')) ? '#'+$('#mapa-pin').attr('data-longitude') : '#Longitude';

            var coord = convertCoordinates(evt.coordinate[1],evt.coordinate[0]);
            $(campoLatitude).val(coord[0]);
            $(campoLongitude).val(coord[1]);
        });
    }
}

function myPosition(callback){
    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(function(position){
            callback(position.coords);
        });
    }
}

function getGeometry(latitude,longitude){
    return new ol.geom.Point(ol.proj.transform([longitude,latitude], 'EPSG:4326', 'EPSG:3857'));
}

function getVectorLayer(name,latitude,longitude,icon,convert){
    var geometry;
    if(convert){
        geometry = getGeometry(latitude,longitude);
    }else{
        geometry = new ol.geom.Point([longitude,latitude]);
    }

    var iconFeature = new ol.Feature({
        geometry: geometry
    });

    var iconSrc = '';
    if(typeof icon === 'object'){

        // var iconText = '<i class="fa '+icon.icon+'" style="color: '+icon.color+'"></i>';
        
        var iconStyle = new ol.style.Style({
            text: new ol.style.Text({
                font: 'normal normal normal 14px/1 FontAwesome',
                text: '\\'+icon.icon,
                fill: new ol.style.Fill({
                    color: icon.color
                })
            })
        });

        // iconStyle.getText().setText('\\'+icon.icon);

        iconFeature.setStyle(iconStyle);

    }else{

        iconSrc = '/assets/openlayers/icons/'+icon+'.png';

        iconFeature.setStyle(new ol.style.Style({
            image: new ol.style.Icon(({
                anchor: [0.5, 46],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                opacity: 0.75,
                src: iconSrc
            }))
        }));

    }

    return new ol.layer.Vector({
        name:name,
        source: new ol.source.Vector({
            features: [iconFeature]
        })
    });
}

function convertCoordinates(latitude,longitude){
    var coord = ol.proj.transform([longitude,latitude], 'EPSG:3857', 'EPSG:4326');
    return coord.reverse();
}
