<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Token");

date_default_timezone_set('America/Sao_Paulo');

chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

$env = (isset($_SERVER['APPLICATION_ENV'])) ? $_SERVER['APPLICATION_ENV'] : 'development';


if(!define('APPLICATION_ENV', $env));
//    define('APPLICATION_ENV', $env);

define('BASE_PATCH',str_replace(DIRECTORY_SEPARATOR.'public','',__DIR__));

defineBaseUrl();

if(APPLICATION_ENV != 'production'){
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

/// Função Debug
function debug( $mixExpression , $boolExit = TRUE , $boolFinish = NULL )
{
    static $arrMessages;
    if( ! $arrMessages )
    {
        $arrMessages = array();
    }

    if( $boolFinish )
    {
        return( implode( " <br/> " , $arrMessages ) );
    }

    $arrBacktrace = debug_backtrace();
    $strMessage = "";
    $strMessage .= "<fieldset><legend><font color=\"#007000\">debug</font></legend><pre>" ;
    foreach( $arrBacktrace[0] as $strAttribute => $mixValue )
    {
        if($strAttribute=='args') continue;
        $strMessage .= "<b>" . $strAttribute . "</b> ". $mixValue ."\n";
    }
    $strMessage .= "<hr />";

//     Abre o buffer, impedindo que seja impresso na tela alguma coisa
    ob_start();
    var_dump( $mixExpression );
//     Pega todo o buffer
    $strMessage .= ob_get_clean();

    $strMessage .= "</pre></fieldset>";


    foreach( $arrMessages as $messages )
    {
        print $messages;
        ob_flush();
        flush();
    }
    print $strMessage;
    print "<br /><font color=\"#700000\" size=\"4\"><b>D I E</b></font>";

    if( $boolExit )
    {
        exit();
    }
}

function defineBaseUrl(){
    if(isset($_SERVER['REQUEST_SCHEME'])){
        $type = $_SERVER['REQUEST_SCHEME'].'://';
        define('BASE_URL',$type.$_SERVER['HTTP_HOST']);
    }else{
        if(isset($_SERVER['HTTP_HOST'])) define('BASE_URL', $_SERVER['HTTP_HOST']);
    }
}