/**
 * Created by bruno.rosa on 07/06/2016.
 */

// if(typeof sessionStorage.sidebarCollapse !== 'undefined' && sessionStorage.sidebarCollapse !== null){
//     if(sessionStorage.sidebarCollapse == 'true'){
//         $('body').addClass('sidebar-collapse');
//     }else{
//         $('body').removeClass('sidebar-collapse');
//     }
// }

$(document).ready(function() {

    $('body').on('click','.sidebar-toggle',function(){
        setTimeout(function(){
            if($('body').hasClass('sidebar-collapse')){
                var toggle = 0;
                // sessionStorage.sidebarCollapse = true;
            }else{
                var toggle = 1;
                // sessionStorage.sidebarCollapse = false;
            }

            sideBarToggle(toggle);
        },500)
    })

});

function sideBarToggle(toggle) {
    $.ajax({
        url:'/user/side-bar-toggle',
        type:'POST',
        dataType:'json',
        data:{toggle:toggle},
        success:function(res){
            if(res.error){
                console.error(res);
            }
        },
        error:function(error){
            console.error(error);
        }
    })
}