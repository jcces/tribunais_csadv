$(function(){

    if($("#PageDetalhesRemessa").length){


        $('.ver-processo').click(function(){
            $('#modal-processo').modal('show');
            loadStats();

            intervalStats = setInterval(function(){
                loadStats();
            },10000);
        })

        $('.fechar-stats').click(function(){
            clearInterval(intervalStats);
        })

        function loadStats(){
            var id = $('.ver-processo').attr('data-id');
            $.ajax({
                url:'/robot-remessa/get-log/'+id,
                type:'GET',
                dataType:'html',
                success:function(html){
                    $('#modal-processo .modal-body').html(html);
                }
            })
        }
    }

    if($("#PageUploadSubsidio").length){
        $('body').on('click','.subsidio',function(){
            var id = $(this).attr('data-id');
            $('input[name="subsidio"]').val(id);

            $('#modal-subsidio').modal('show');
        })
    }

    if($("#PagePagamentoIndex").length || $("#PageReanaliseIndex").length || $("#PageManifestacaoIndex").length){
        $('input[name="Sequencial"]').change(function(){
            verificaSequencial();
        })

        $('input[name="Data"]').change(function(){
            verificaSequencial();
        })

        function verificaSequencial(){
            var sequencial = $('input[name="Sequencial"]').val();
            var data = $('input[name="Data"]').val();

            if(!sequencial || !data) return false;

            $.ajax({
                url:'/robot-remessa/verifica-sequencial',
                type:'POST',
                dataType:'json',
                data:{Data:data,Sequencial:sequencial},
                success:function(json){
                    if(!json.error){
                        $('.btn-sequencial').removeClass('hide');
                    }else{
                        $('input[name="Sequencial"]').val('');
                        $('.btn-sequencial').addClass('hide');
                    }
                }
            })
        }

        $('body').on('click','.selecionar-pagamento',function(){
            var id = $(this).attr('data-id');
            if(!$(this).hasClass('selecionado')){
                $('body').append('<input type="hidden" value="'+id+'" class="item-selecionado" data-id="'+id+'"/>')
            }else{
                $('.item-selecionado[value="'+id+'"]').remove();
            }
            //if($(this).hasClass(''))
            $(this).toggleClass('selecionado');

            atualizarValor();
        })

        function atualizarValor(){
            var valor = 0;
            $.each($('.selecionado'), function(i,e){
                var obj = $(e);
                var sub = parseFloat(obj.attr('data-valor'));
                valor += sub;
            })
            var money = $.number(valor,2);
            $('.total-valor').html('R$ '+money);
        }

        $('.abrir-modal').click(function(){
            var selecionados = [];
            $.each($('.item-selecionado'), function(i,e){
                var obj = $(e);
                selecionados.push(obj.attr('data-id'));
            })

            if(!selecionados.length){
                alert('Selecione pelo menos um pagamento!!');
                return false;
            }

            $('.total-pagamentos').html(selecionados.length);
            var pagamentos = selecionados.join(';');
            $('input[name="Pagamentos"]').val(pagamentos);
            $('#modal-pacote').modal('show');
        })
    }
    if($("#PageDetalheTribunais").length){
        $('.detalhe-processo').click(function(){

            var tribunal = $(this).attr('data-tribunal');
            var id = $(this).attr('data-id');

            $.ajax({
                url:'/tribunais/detalhes-processo',
                type:'POST',
                dataType:'html',
                data:{'tribunal':tribunal,'id':id},
                success:function(html){
                    $("#modal-processo .modal-body").html(html);
                    $('#modal-processo').modal('show');
                }
            })


        })
    }
    if($("#PageDashboardTriagem").length){
        $('.download').change(function(){
            if($(this).val()){
                $('.btn-relatorio').removeClass('hide');
                $('.btn-relatorio').attr('href','/robot-relacionamento/download/'+$(this).val());
            }else{
                $('.btn-relatorio').addClass('hide');
            }
        })
    }
    if($("#PageIgnorados").length){
        $('.remover-ignorado').click(function(){
            var id = $(this).attr('data-id');
            $.ajax({
                url:''+id,
                type:'GET',
                dataType:'json',
                success:function(json){
                    if(!json.error){
                        location.reload();
                    }
                }
            })
        })
    }
    if($("#PageTriagem").length){
        $(".buscar-processo").click(function(){
            consultarProcesso();
        })

        $("select[name='opcoes']").change(function(){
            var opt = $(this).val();
            $('input[name="ProcessoDestino"]').val(opt);
            consultarProcesso();
        })

        function consultarProcesso(){
            var processo = $('input[name="ProcessoDestino"]').val();
            if(!processo.length){
                addMessage('danger','Favor informe o processo');
                return false;
            }

            $.ajax({
                url:'/robot-processo/load-ajax',
                type:'POST',
                dataType:'JSON',
                data:{'processo':processo},
                success:function(json){
                    if(json.error){
                        $('#DadosProcesso').addClass('hide');
                        $('.salvar').addClass('hide');
                        addMessage('danger',json.message);
                        $('input[name="ProcessoDestino"]').val('');
                        $('.salvar').addClass('hide');
                    }else{
                        $('#DadosProcesso').removeClass('hide');
                        $('.NomeAutor').html(json.data.NomeAutor);
                        $('.Vara').html(json.data.Vara);
                        $('.NumeroCliente').html(json.data.NumeroCliente);

                        $('.salvar').removeClass('hide');
                        $("input[name='CodigoSistema']").val(json.data.CodigoSistema);
                    }
                }
            })
        }
    }
})