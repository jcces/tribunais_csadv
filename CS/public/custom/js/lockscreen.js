/**
 * Created by bruno.rosa on 10/12/15.
 */

LOCKSCREENTIMEOUT = '';
LOCKSCREENTIMER = 60000 * 30; // 30 MINUTOS
LOCKURL = '/user/lock-screen';

$(window).load(function() {

    startCheckInactivity();

    $('body,html').on('keypress',function(){
        startCheckInactivity();
    }).on('mousemove',function(){
        startCheckInactivity();
    });

});

function startCheckInactivity()
{
    return false;
    clearTimeout(LOCKSCREENTIMEOUT);
    LOCKSCREENTIMEOUT = setTimeout(function(){
        lockIt();
    },LOCKSCREENTIMER);
}

function lockIt()
{
    var hist = window.location.pathname;

    var lockForm = document.createElement("form");
    lockForm.method = "POST";
    lockForm.action = LOCKURL;

    var lockInput = document.createElement("input");
    lockInput.type = "text";
    lockInput.name = "historyUrl";
    lockInput.value = hist;
    lockForm.appendChild(lockInput);
    document.body.appendChild(lockForm);
    lockForm.submit();
}