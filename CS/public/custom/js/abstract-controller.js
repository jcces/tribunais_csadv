/**
 * Created by bruno.rosa on 01/06/2016.
 */

$(document).ready(function () {
    //TABLE RELATIONSHIP
    $('body').on('click', '[data-table-rel]', function () {

        if (!$(this).attr('data-table-rel')) {
            return false;
        }

        var service = $(this).attr('data-service');
        var table = $(this).attr('data-table');
        var id = $(this).attr('data-id');
        var parent = $(this).attr('data-parent');
        var callback = $(this).attr('data-callback');
        var title = $(this).attr('data-title');
        var dataRelField = $(this).attr('data-rel-field');
        $.ajax({
            url: '/abstract/form',
            type: 'POST',
            data: {
                'service': service,
                'table': table,
                'id': id,
                'parent': parent,
                'callback': callback,
                'dataRelField': dataRelField
            },
            success: function (json) {
                if (json.error) {
                    addMessage('danger', json.message);
                } else {

                    $('#input-insert-table').html(json);
                    $('#modal-insert-table .modal-title').text(title);
                    $('#modal-insert-table').modal('show');

                    updateMasks();

                    if (callback) {
                        window[callback]();
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }).on('submit', 'form#abstract-form', function (e) {
        e.preventDefault();

        var actionForm = $(this).attr('action');
        var data = $(this).serialize();
        var table = $(this).attr('data-table');
        var callback = $(table).attr('data-callback');

        $.ajax({
            url: actionForm,
            type: 'POST',
            data: data,
            success: function (json) {
                if (json.error) {
                    addMessage('danger', json.message);
                } else {
                    addMessage('success', json.message);
                    addRowTable(table, json.dados);
                    if (callback) {
                        window[callback]();
                    }
                    $('#modal-insert-table').modal('hide');
                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    }).on('click', '.excluir-resource', function () {

        var resource = $(this).attr('data-resource');
        var id = $(this).attr('data-id');
        var tableName = $('tr[data-id="' + id + '"]').parents('table').attr('id');
        var callback = $('#' + tableName).attr('data-callback');

        $.ajax({
            url: '/abstract/excluir',
            type: 'POST',
            data: {'service': resource, 'id': id},
            success: function (json) {
                if (json.error) {
                    addMessage('danger', json.message);
                } else {
                    if ($('table#' + tableName).hasClass('datatable')) {
                        var datatable = $('table#' + tableName).attr('data-table-obj');
                        table[datatable].row($('tr[data-id="' + json.dados['Id'] + '"]')).remove().draw();
                    } else {
                        $('table#' + tableName + ' tr[data-id="' + json.dados['Id'] + '"]').fadeTo("slow", 0.8, function () {
                            $(this).remove();
                        });
                    }

                    if (callback) {
                        window[callback]();
                    }
                    addMessage('success', json.message);
                    $('#modal-insert-table').modal('hide');
                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    });

});

function addRowTable(tableName, data) {

    var service = $('[data-table="' + tableName + '"]').attr('data-service');
    var callback = $('[data-table="' + tableName + '"]').attr('data-callback');
    var relField = $('[data-table="' + tableName + '"]').attr('data-rel-field');
    var dataParent = $('[data-table="' + tableName + '"]').attr('data-parent');

    var tr = '<tr data-table-rel="true" data-rel-field="' + relField + '" data-service="' + service + '" data-table="' + tableName + '" data-id="' + data['Id'] + '" data-parent="' + dataParent + '" data-callback="' + callback + '" data-title="Editando">';
    $('table' + tableName).find('thead tr.modelo th').each(function () {
        var field = $(this).attr('data-field');
        var mask = $(this).attr('data-mask');
        var value = '';

        if ($(this).hasClass('actions')) {
            tr += '<td></td>';
        } else {
            value = (mask) ? window[mask](data[field]) : data[field];
            tr += '<td>' + value + '</td>';
        }

    });

    tr += '</tr>';

    if ($('table' + tableName).hasClass('datatable')) {
        var datatable = $('table' + tableName).attr('data-table-obj');

        if ($('table' + tableName + ' tr[data-id="' + data['Id'] + '"]').length) {
            table[datatable].row($('tr[data-id="' + data['Id'] + '"]')).remove().draw();
        }

        table[datatable].row.add($(tr).flash()).draw();

    } else {
        if ($('tr.no-records').length) {
            $('tr.no-records').remove();
        }

        if ($('table' + tableName + ' tr[data-id="' + data['Id'] + '"]').length) {
            $('table' + tableName + ' tr[data-id="' + data['Id'] + '"]').replaceWith($(tr).flash());
        } else {
            $('table' + tableName + ' tbody').append($(tr).flash());
        }
    }

    if (callback) {
        window[callback]();
    }

}