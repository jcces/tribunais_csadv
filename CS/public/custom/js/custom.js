$(function () {
    success = new Audio('/assets/sound/success.mp3');
    info = new Audio('/assets/sound/info.mp3');
    error = new Audio('/assets/sound/error.mp3');

    //LOADING W/ AJAX
    $(document).ajaxStart(function() { Pace.restart(); });
    $(document).ajaxSuccess(function() { renderAllPlugins() });

    renderAllPlugins();

    //MENU FLOAT
    $(window).scroll(function(){
        var aTop = $('.main-header').height();
        var submit = ($('button[type=submit]:last').length > 0)?true:false;
        var aSubmit = $('button[type=submit]:last');
        var windowHeight = $(window).height();
        if($(this).scrollTop()>=aTop){
            $('.menu-float-container').not('.trigger-submit').show();
            if($('body').hasClass('sidebar-collapse')){
                $('.menu-float-container .menu-float').show();
            }else{
                $('.menu-float-container .menu-float').hide();
            }
            if(submit === true && ($(this).scrollTop() + windowHeight) <= (aSubmit.offset().top + aSubmit.height())){
                $('.menu-float-container .menu-float.trigger-submit').show();
            }else{
                $('.menu-float-container .menu-float.trigger-submit').hide();
            }
        }else{
            $('.menu-float-container').hide();
        }
    });

    $('body').on('click','.show-menu-lateral',function(){
        $('.sidebar-toggle').trigger('click');
        $(this).hide();
    }).on('click','.trigger-submit',function(){
        $('button[type=submit][data-submit="true"]').trigger('click');
    });

    //MODAL EVENT
    $('.modal').on('hidden.bs.modal', function (e) {
        if($(this).find('table.datatable')) $('div[id^="DataTables_"]',this).remove();
    }).on('show.bs.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $('body').on('click','tr[data-href] img, tr[data-href] a[href], tr[data-href] .btn',function(e){
        e.stopPropagation();
    }).on('click','tr[data-href]',function(e){
        var url = $(this).attr('data-href');
        if(!url) return true;
        if (e.ctrlKey || $(this).attr('data-blank') == 1){
            window.open(location.origin+url,'_blank');
        }else{
            window.location.href = location.origin+url;
        }
    }).on('click','img[data-action="img-link"]', function(){
        $('img[data-action="img-link"]').removeClass('selected');


        var src = $(this).attr('src');

        var img = new Image();
        img.src = src;
        var modalSize = 'modal-sm';
        if(img.naturalWidth > 300){
            modalSize = 'modal-md';
        }
        if(img.naturalWidth > 600){
            modalSize = 'modal-lg';
        }

        var id = $(this).attr('data-id');
        $(this).addClass('selected');

        $('#modal-imagem .modal-dialog').removeClass('modal-lg');
        $('#modal-imagem .modal-dialog').removeClass('modal-sm');
        $('#modal-imagem .modal-dialog').removeClass('modal-md');
        $('#modal-imagem .modal-dialog').addClass(modalSize);

        $('#modal-imagem img').attr('src',src);
        if(id.length){
            $('#modal-imagem .btn-danger').show()
        }else{
            $('#modal-imagem .btn-danger').hide()
        }
        $('#modal-imagem').modal('show');
    }).on('dblclick, click',function(){
        $('.container-alertas').remove();
    }).on('click','.excluir-imagem-link',function(){
        var id = $('img[data-action="img-link"].selected').attr('data-id');
        $.ajax({
            url:'/anexos/excluir-ajax/'+id,
            type:'GET',
            dataType:'json',
            success:function(res){
                if(!res.error){
                    $('img[data-action="img-link"].selected').remove();
                    $('#modal-imagem').modal('hide');
                }
            }
        })
    }).on('mouseleave','.container-alertas',function(){
        $('.container-alertas').remove();
    }).on('click','i[data-insert]',function(){
        var service = $(this).attr('data-service');
        var callbackField = $(this).attr('data-callback-field');
        var dataPadrao = $(this).attr('data-padrao');

        $.ajax({
            url:'/abstract/select-insert',
            type:'POST',
            dataType:'HTML',
            data:{service:service,dataPadrao:dataPadrao,'calbackField':callbackField},
            success:function(html){
                $('#modal-insert-select .modal-body').html(html);
                $('#modal-insert-select').modal('show');
            }
        })

    }).on('submit','form.form-select-insert',function(e){
        e.preventDefault();
        $.ajax({
            url:'/abstract/save-select-insert',
            type:'post',
            dataType:'json',
            data:$(this).serialize(),
            success:function(json){
                console.log(json.callback);
                var obj = $('#'+json.callback);
                var o = $("<option/>", {value: json.data.id, text: json.data.nome});
                $('select[data-service]').each(function(i,e){
                    if($(this).attr('data-service') == $('input[name="serviceSend"]').val()){
                        $(this).append(o);
                    }
                })
                console.log(json.data.id);
                obj.find('option[value="' + json.data.id + '"]').prop('selected',true);
                obj.trigger('change');

                addMessage('success','Registro salvo');
                $('#modal-insert-select').modal('hide');
            }
        })

    }).on('click','a.btn-confirma,a[data-confirma="true"],button.btn-confirma',function(e){
        if($(this).hasClass('no-backend')){
            e.preventDefault();
            $(this).parents('tr').remove();
            return true;
        }
        var href = $(this).attr('href');
        var text = ($(this).attr('data-confirma-label')) ? $(this).attr('data-confirma-label') : $(this).html();
        var classe = $(this).attr('class');

        var label = ($(this).attr('data-full-label')) ? $(this).attr('data-full-label') : 'Não será possível reverter a ação <strong>'+text+'</strong>, deseja continuar?';

        $('#modal-confirma .modal-body').html(label);
        $('.btn-acao-confirma').attr('href', href);
        $('.btn-acao-confirma').html(text);
        $('#modal-confirma').modal('show');
        return false;
    }).on('keydown','input[type="number"]',function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }).on('keydown','input[data-maxlength]',function (e) {
        var max = parseInt($(this).attr('data-maxlength'));
        var atual = $(this).val().length + 1;
        if(atual > max && e.keyCode != 8) return false;
    });

    //DROP SUBMENU
    $('body').on('click','.btn[data-submenu],tr[data-submenu]',function (e,i) {
        if($(this).attr('data-submenu')){
            e.preventDefault();
            $('#submenu').addClass($(this).attr('data-submenu-class'));
            if($(this)[0].localName == 'tr'){
                $(this).addClass('highlight');
            }
            var menuJson = JSON.parse($(this).attr('data-submenu'));
            $('#submenu ul li').remove();
            $.each(menuJson,function (menu) {
                var item = menuJson[menu];
                var url = (item.url)?item.url:'javascript:void(0)';
                var el = $('<a href="'+url+'" class="'+item.class+'">'+item.label+'</a>');
                if(item.attrName){
                    el.attr(item.attrName,item.attrValue);
                }
                if(item.title){
                    el.attr('title',item.title);
                }
                var li = $('<li></li>').prepend(el);
                $('#submenu ul').append(li);
            });
            var left = e.clientX;
            var top = e.clientY;
            if((left + $('#submenu').width()) > $(window).width()){
                left = left - $('#submenu').width();
                top = top - $('#submenu').height();
            }
            $('#submenu').css({ left: left, top: top });
            $('#submenu').removeClass('invisible');
        }
    }).on('mouseleave','#submenu',function () {
        if($('#submenu:visible')){
            $('#submenu').addClass('invisible');
            $('tr.highlight').removeClass('highlight');
        }
    });

});

$.fn.buildSelect2 = function() {
    $.each($(this).find('select'), function(){
        var tag = $(this).attr('data-tag');
        $(this).select2({
            tags:tag
        })
    })
};

$.fn.buildICheck = function() {
    $(this).iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass: 'iradio_minimal-green',
        increaseArea: '20%' // optional
    });

    if($(this).hasClass('selectAll')){
        $(this).on('ifChanged', function(event){
            if(event.currentTarget.checked){
                $(this).parents('table').find('td input[type=checkbox]').iCheck('check');
            }else{
                $(this).parents('table').find('td input[type=checkbox]').iCheck('uncheck');
            }

        });
    }

    if($(this).hasClass('selectAllChild')){
        $(this).on('ifChanged', function(event){
            var id = $(this).val();
            if(event.currentTarget.checked){
                $(this).parents('tbody').find('tr[data-idparent='+id+'] td input[type=checkbox]').iCheck('check');
            }else{
                $(this).parents('tbody').find('tr[data-idparent='+id+'] td input[type=checkbox]').iCheck('uncheck');
            }

        });
    }

};

$.fn.buildColorScale = function() {
    $(this).parent('div.form-group').addClass('colorScale');
};

function addMessage(type,message){
    if($('.container-alertas').length){
        $('.container-alertas').remove();
    }
    var titulo = { success : 'Sucesso!', info : 'Aviso!', danger : 'Erro!' };
    var msg = '<div class="container-alertas" ng-controller="MensagemCtrl"><div class="alert alert-dismissible alert-'+type+'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>'+titulo[type]+'</strong><ul><li>'+message+'</li></ul></div></div>';
    setTimeout(function () {
        $('body').append($(msg));
        msgSound();
        removeMessage();
    },1);
}

function removeMessage(){
    if($('.container-alertas').length){
        setTimeout(function(){
            $('.container-alertas').remove();
        },8000)
    }
}

function msgSound(){

    if($('.alert-success').length){
        success.play();
    }
    if($('.alert-info').length){
        info.play();
    }
    if($('.alert-danger').length){
        error.play();
    }

}

function money(value){
    var valueFormat = (typeof value == 'undefined')?0.00:value;
    var value = Number(valueFormat);
    $('#CampoMoneyAuxiliar').val(value.toFixed(2)).maskMoney('mask');
    var formatado = $('#CampoMoneyAuxiliar').val();
    return formatado;
}

function percentage(value){

    $('#CampoPorcentagemAuxiliar').val(value.toFixed(2));
    $("#CampoPorcentagemAuxiliar").maskMoney({thousands:'.', decimal:',', allowZero:true, allowNegative:false, suffix:' %'});
    $("#CampoPorcentagemAuxiliar").maskMoney('mask');

    return $('#CampoPorcentagemAuxiliar').val();
}

function updateMaskMoney(){
    $("input[data-mask='money']").maskMoney({thousands:'.', decimal:',', allowZero:true, allowNegative:true, prefix:'R$ '});
    $("input[data-mask='money']").maskMoney('mask');

    $("input[data-mask='peso']").maskMoney({thousands:'.', decimal:',', allowZero:true, allowNegative:false, suffix:' g'});
    $("input[data-mask='peso']").maskMoney('mask');

    $("input[data-mask='percentage']").maskMoney({thousands:'.', decimal:',', allowZero:true, allowNegative:false, suffix:' %'});
    $("input[data-mask='percentage']").maskMoney('mask');
}

function updateDatepicker(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        //startDate: '-1825d',
        language:'pt-BR',
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "top"
    });

    $('.datetimepicker').datetimepicker({
        locale: 'PT-BR'
    });
}

function updateMasks(){
    setTimeout(function(){
        updateMaskMoney();
        updateDatepicker();

        $('input[data-mask="cep"]').mask('99.999-999');
        $('input[data-mask="telefone"]').mask('(99) 9 9999-9999');
        $('input[data-mask="cpf"]').mask('999.999.999-99');
        $('input[data-mask="cnpj"]').mask('99.999.999/9999-99');
        $('input[data-mask="datepicker"]').mask('99/99/9999');
        $('input[data-mask="datetimepicker"]').mask('99/99/9999 H:i:s');
        $('input[data-mask="int"]').apenasNumeros();
    },1);
}

table = [];
$.fn.setDataTable = function(){

    if($('thead tr.search',this).length) $('thead tr.search',this).remove();

    var dataTableTranslate = {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    };

    var columnDefs = [];
    $.each($(this).find('th.icon, th.no-sort'), function(){
        columnDefs.push({"targets": $(this).index(),"orderable": false});
    });

    if($(this).attr('data-columnDefs')){
        var defs = $(this).attr('data-columnDefs');
        columnDefs.push(JSON.parse(defs));
    }

    var order = [];
    if($(this).attr('data-columnOrder')){
        var colOrder = $(this).attr('data-columnOrder').split(',');
        order.push([colOrder[0],colOrder[1]]);
    }

    var pageLength = 15;
    if ($(this).attr('data-page-length')) {
        var pageLength = $(this).attr('data-page-length');
    }

    var reorder = false;
    if($(this).hasClass('reorder')){
        reorder = true;
    }

    var objName = $(this).attr('data-table-obj');
    if(!objName){
        objName = 'table'+Math.floor(Math.random() * new Date().getTime());
        $(this).attr('data-table-obj',objName);
    }

    table[objName] = this.DataTable({
        "destroy": true,
        "language": dataTableTranslate,
        "paging": true,
        "lengthChange": false,
        "pageLength": pageLength,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "rowReorder": reorder,
        "order":order,
        "columnDefs":columnDefs,
        "dom": '<"clearSearchBtn">frtip'
    });

    if(reorder){
        table[objName].on( 'row-reordered', function ( e, diff, edit ) {
            reorderIt();
        });
    }

    $('table[data-table-obj='+objName+']').setSearchTable();

};

$.fn.setSearchTable = function () {
    $('thead tr',this).after("<tr class='search'></tr>");
    $(this).parents('div.dataTables_wrapper').find('.clearSearchBtn').append('<a href="javascript:void(0)" class="btn btn-default btn-xs clear-search-fields hide" data-title="Limpar Campos de Pesquisa">limpar filtro</a>');
    $(this).find('thead th').each( function (i) {
        var tableParent = $(this).parents('table');
        var content = '<th><div class="form-group table-search"><i class="fa fa-search"></i><input type="text" placeholder="" class="form-control tabela-field-search" data-coluna="'+i+'"/></div></th>';
        if($(this).hasClass('icon')) content = '<th class="no-search"></th>';
        $(tableParent).find('.search').append( content );
    });
    $( '.search input').on( 'keyup change', function () {
        var array = '';
        var i = $(this).attr('data-coluna');
        var objName = $(this).parents('table').attr('data-table-obj');
        if(objName){
            table[objName]
                .column( i )
                .search( $(this).val() )
                .draw();
            $(this).parents('div.dataTables_wrapper').find('.clear-search-fields').removeClass('hide');
        }else{
            table
                .column( i )
                .search( $(this).val() )
                .draw();
            $(this).parents('div.dataTables_wrapper').find('.clear-search-fields').removeClass('hide');
        }
    }).on( 'focus', function () {
        $(this).parents('.table-search').find('.fa').addClass('off');
    }).on( 'focusout', function () {
        if(!$(this).val().length){
            $(this).parents('.table-search').find('.fa').removeClass('off');
        }
    });

    $('.table-search .fa').on( 'click', function () {
        $(this).parents('.table-search').find('input').focus();
    });

    $( '.dataTables_filter input[type="search"]').on( 'keyup change', function () {
        $(this).parents('div.dataTables_wrapper').find('.clear-search-fields').removeClass('hide');
    });

    $('.clear-search-fields').on('click',function(){
        $(this).parents('div.dataTables_wrapper').find('input[type="text"].tabela-field-search, input[type="search"]').val('').keyup();
        $(this).parents('div.dataTables_wrapper').find('.dataTables_filter input[type="search"]').val('').keyup();
        $(this).parents('div.dataTables_wrapper').find('.table-search').find('.fa').removeClass('off');
        $(this).addClass('hide');
    });
};

function renderAllPlugins() {
    $.each($('.datatable'),function () {
        $(this).setDataTable();
    });

    $('input[data-mask="date"],input[data-mask="datepicker"]').datepicker({
        autoclose: true,
        format:'dd/mm/yyyy',
        language:'pt-BR'
    });

    $('.datetimepicker').datetimepicker({
        locale: 'PT-BR'
    });

    $.each($("input[data-slider=1]"), function(){
        $(this).bootstrapSlider();
        if($(this).attr('data-slider-range')){
        }else{
            var value = $(this).val();
            $(this).bootstrapSlider('setValue',value);
        }

    });

    //KNOB
    $(".dial").knob();


    //SELECT2
    $('body').buildSelect2();
    $.each($('input'),function () {
        $(this).buildICheck();
    });


    //COLOR SCALE
    $('input.colorScale').buildColorScale();

    //MAP
    renderMap();
}

//EFFECTS
jQuery.fn.flash = function () {
    this.addClass('flash');
    setTimeout(function(){
        $('.flash').removeClass('flash');
    },3000);
    return this;
};

function reorderIt() {
    $.each($('table.reorder tbody tr'),function () {
        $('input[type=hidden]',this).remove();
        $(this).append('<input type="hidden" name="reorder[]" value="'+$(this).attr('data-id')+'">')
    });
}