$(document).ready(function () {
    var hS = $(window).height();
    var header = $('.main-header').height();
    var boxHeader = $('.box-header').innerHeight();
    var footer = $('.main-footer').innerHeight();

    var totalH = header + boxHeader + footer + 90;

    var H = hS - totalH;

    $('#box-container .box .box-body').css('max-height', H + 'px');
});