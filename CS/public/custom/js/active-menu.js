/**
 * Created by bruno.rosa on 25/05/2017.
 */

$(function () {
    //ACTIVE MENU OF VIEWING PAGE
    activeMenu();
});

function activeMenu() {
    var path = location.pathname;
    var pathArr = path.split('/');
    if(pathArr.length > 3) {
        path = '/'+pathArr[1]+'/'+pathArr[2];
        if(!$('ul.sidebar-menu a[href="'+path+'"]').length){
            path = '/'+pathArr[1]+'/cadastro';
            if(!$('ul.sidebar-menu a[href="'+path+'"]').length){
                path = '/'+pathArr[1];
            }
        }
    }else if(pathArr.length > 2) {
        path = '/'+pathArr[1]+'/'+pathArr[2];
        if(!$('ul.sidebar-menu a[href="'+path+'"]').length){
            path = '/'+pathArr[1];
        }
    }else if(pathArr.length > 1) {
        path = '/'+pathArr[1];
        if(!$('ul.sidebar-menu a[href="'+path+'"]').length){
            path = '/';
        }
    }

    $('ul.sidebar-menu a[href="'+path+'"]').parents('li').addClass('active');

}