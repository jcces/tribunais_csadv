-- BACKUP INTEIRO DO BANCO DE DADOS
-- https://blog.winhost.com/using-mysqldump-to-backup-and-restore-your-mysql-databasetables/
-- mysqldump.exe –e –u[username] -p[password] -h[hostname] [database name] > C:\[filename].sql
-- mysqldump.exe –e –uroot -proot -h127.0.0.1 COSTA_SILVA_TRIBUNAIS > C:\www\APP2B\REPO\COSTA_SILVA_TRIBUNAIS\BACKUP-08142018.sql


--TOTAL PROCESSOS ALTERADOS

-- TOTAL POR UF
SELECT UF, COUNT(*) AS TOTAL FROM TRIBUNAL_JOB
WHERE ALTERADO = '1'
GROUP BY UF
ORDER BY TOTAL DESC

-- TOTAIS AGRUPADOR POR ORIGEM
SELECT SUBSTRING(ORIGEM, 1, 50) as o, COUNT(*) FROM TRIBUNAL_JOB
WHERE UF = 'RS'
AND ALTERADO = '1'
AND ORIGEM NOT LIKE '%selOrigem=RS%'
GROUP BY o;

-- TOTAIS
SELECT COUNT(*) FROM TRIBUNAL_JOB
WHERE ALTERADO = '0'
AND UF = 'MG'
-- AND OBS IS NOT NULL
-- AND ORIGEM LIKE '%/cposg5%'
-- AND NUM_PROC_JUST LIKE '5%8.13%0'

-- LISTA
SELECT * FROM TRIBUNAL_JOB
WHERE
ALTERADO = '1'
AND UF = 'RS'
-- AND NUM_PROC_JUST IS NOT NULL
-- AND ORIGEM IS NULL
-- AND OBS IS NOT NULL
-- AND JUIZO_VARA IS NULL
-- AND OBS LIKE '% DE % => %'

-- ALTERAÇÃO
UPDATE TRIBUNAL_JOB
SET
OBS='JSON',
ORIGEM='JSON'
WHERE
UF = 'GO'
AND ALTERADO = '1'
AND ORIGEM IS NULL
AND OBS IS NULL
AND JUIZO_VARA IS NOT NULL

UPDATE TRIBUNAL_JOB
SET
OBS='MANUAL',
ORIGEM='https://processual.trf1.jus.br/consultaProcessual/processo.php?proc=9100313220&secao=TRF1&pg=1&enviar=Pesquisar',
JUIZO_VARA='QUINTA TURMA',
ALTERADO='1'
WHERE ID = '25323240-3c90-4fcb-bb8d-ced76a8ea75d'
AND NUM_PROC_JUST = '91.00.31322-0'
AND UF = 'MG'


-- JUIZO VARA ERRADO!
SELECT * FROM TRIBUNAL_JOB
-- SELECT COUNT(*) AS TOTAL FROM TRIBUNAL_JOB
-- SELECT UF, COUNT(*) AS TOTAL FROM TRIBUNAL_JOB
WHERE
ALTERADO = '1'
AND
(
JUIZO_VARA LIKE '%Camara cível%' OR
JUIZO_VARA LIKE '%Camara de Direito%' OR
JUIZO_VARA LIKE '%Câmara do sexto grupo%' OR
JUIZO_VARA LIKE '%Câmara de Enfrentamento de acervos%' OR
JUIZO_VARA LIKE '%Câmara Especializada Cível%' OR
JUIZO_VARA LIKE '%Turma cível%' OR
JUIZO_VARA LIKE '%Turma de Direto%' OR
JUIZO_VARA LIKE '%Turma Julgadora%' OR
JUIZO_VARA LIKE '%Turma recursal%'
)
-- GROUP BY UF
-- ORDER BY TOTAL DESC
ORDER BY UF

--
SELECT * FROM TRIBUNAL_JOB
WHERE OBS LIKE '%(CS)%'
AND JUIZO_VARA IS NULL
ORDER BY UF

--
SELECT UF, COUNT(*) AS TOTAL FROM TRIBUNAL_JOB
WHERE ALTERADO = '0'
AND OBS LIKE '%CS%'
GROUP BY UF
ORDER BY TOTAL DESC