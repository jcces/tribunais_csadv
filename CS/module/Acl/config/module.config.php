<?php

return array(
    'router' => array(
        'routes' => array(
            'acl-home' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acl-action' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl-action[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl-action',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acl-module' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl-module[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl-module',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acl-profile' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl-profile[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl-profile',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acl-profile-service' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl-profile-service[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl-profile-service',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acl-service' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl-service[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl-service',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acl-service-action' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl-service-action[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl-service-action',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acl-user-profile' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/acl-user-profile[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'acl-user-profile',
                        'action'     => 'index',
                    ),
                ),
            ),

        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'acl' => 'Acl\Controller\AclController',
            'acl-action' => 'Acl\Controller\ActionController',
            'acl-module' => 'Acl\Controller\ModuleController',
            'acl-profile' => 'Acl\Controller\ProfileController',
            'acl-profile-service' => 'Acl\Controller\ProfileServiceController',
            'acl-service' => 'Acl\Controller\ServiceController',
            'acl-service-action' => 'Acl\Controller\ServiceActionController',
            'acl-user-profile' => 'Acl\Controller\UserProfileController',

        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    )
);
