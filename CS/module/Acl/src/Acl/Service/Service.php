<?php

namespace Acl\Service;

use \Acl\Entity\Service as Entity;

class Service extends Entity{

        public $optionsOwnerOnly = [""=>"Selecione...",'S'=>'Sim','N'=>'Não'];

        public function getLabelOwnerOnly(){
            $mapa = $this->optionsOwnerOnly;
            if(isset($mapa[$this->getOwnerOnly()])) return $mapa[$this->getOwnerOnly()];
        }

}