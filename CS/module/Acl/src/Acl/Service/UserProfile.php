<?php

namespace Acl\Service;

use \Acl\Entity\UserProfile as Entity;

class UserProfile extends Entity{

        public $classUser = '\Manager\Service\User';
        public $classProfile = '\Acl\Service\Profile';

        public function getObjUser(){
            $class = $this->classUser;
            $service = new $class;
            $service->setId($this->getIdUser());
            $service->load();
            return $service;
        }

        public function getObjProfile(){
            $class = $this->classProfile;
            $service = new $class;
            $service->setId($this->getIdProfile());
            $service->load();
            return $service;
        }

}