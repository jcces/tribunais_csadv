<?php

namespace Acl\Service;

use \Acl\Entity\Action as Entity;

class Action extends Entity{

        public $classModule = '\Acl\Service\Module';

        public function getObjModule(){
            $class = $this->classModule;
            $service = new $class;
            $service->setId($this->getIdModule());
            $service->load();
            return $service;
        }

}