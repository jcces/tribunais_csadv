<?php

namespace Acl\Service;

use \Acl\Entity\Profile as Entity;
use Estrutura\Service\Conexao;

class Profile extends Entity{
    public function getUsersNotIn(){
        $sql = "SELECT ID, NAME, EMAIL FROM 
                    MANAGER_USER 
                    WHERE ID NOT IN(SELECT ID_USER FROM ACL_USER_PROFILE WHERE ID_PROFILE = '{$this->getId()}' AND DELETED = 0 GROUP BY ID_USER) AND DELETED = 0";
        return Conexao::listarSql($sql);
    }

    public function getUsers(){
        $sql = "SELECT PROFILE.ID, USER.ID AS ID_USER, USER.NAME, USER.EMAIL FROM 
                MANAGER_USER USER
                INNER JOIN ACL_USER_PROFILE PROFILE ON PROFILE.ID_USER = USER.ID
                WHERE PROFILE.ID_PROFILE = '{$this->getId()}' AND PROFILE.DELETED = 0;";
        $lista = Conexao::listarSql($sql);
        $result = $this->populateService(new UserProfile(),$lista);
        return $result;
    }

    public function getServiceNotIn(){
        $sql = "SELECT ID, NAME, DESCRIPTION 
                FROM ACL_SERVICE
                WHERE ID NOT IN (SELECT ID_SERVICE FROM ACL_PROFILE_SERVICE WHERE ID_PROFILE = '{$this->getId()}' AND DELETED = 0)
                AND DELETED = 0;";
        return Conexao::listarSql($sql);
    }

    public function getServices(){
        $sql = "SELECT REL.ID, PROFILE.ID ID_PROFILE, SERVICE.ID ID_SERVICE, SERVICE.NAME SERVICE_NAME, PROFILE.NAME PROFILE_NAME 
                FROM ACL_PROFILE_SERVICE REL
                INNER JOIN ACL_PROFILE PROFILE ON REL.ID_PROFILE = PROFILE.ID AND PROFILE.DELETED = 0
                INNER JOIN ACL_SERVICE SERVICE ON SERVICE.ID = REL.ID_SERVICE AND PROFILE.DELETED = 0
                WHERE REL.ID_PROFILE = '{$this->getId()}'
                AND REL.DELETED = 0;";
        $lista = Conexao::listarSql($sql);
        $result = $this->populateService(new UserProfile(),$lista);
        return $result;
    }

}