<?php

namespace Acl\Service;

use \Acl\Entity\ProfileService as Entity;
use Estrutura\Service\Conexao;

class ProfileService extends Entity{

        public $classProfile = '\Acl\Service\Profile';
        public $classService = '\Acl\Service\Service';

        public function getObjProfile(){
            $class = $this->classProfile;
            $service = new $class;
            $service->setId($this->getIdProfile());
            $service->load();
            return $service;
        }

        public function getObjService(){
            $class = $this->classService;
            $service = new $class;
            $service->setId($this->getIdService());
            $service->load();
            return $service;
        }


}