<?php

namespace Acl\Controller;

use Estrutura\Controller\AbstractCrudController;

class ModuleController extends AbstractCrudController
{
    /**
     * @var \Acl\Service\Module
     */
    protected $service;

    /**
     * @var \Acl\Form\Module
     */
    protected $form;

    public function __construct(){
        parent::init();
    }

    public function indexAction()
    {
        return parent::index($this->service, $this->form);
    }

    public function gravarAction(){
        return parent::gravar($this->service, $this->form);
    }

    public function cadastroAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function excluirAction()
    {
        return parent::excluir($this->service, $this->form);
    }
}
