<?php

namespace Acl\Controller;

use Acl\Service\Profile;
use Acl\Service\ProfileService;
use Acl\Service\UserProfile;
use Estrutura\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;

class ProfileController extends AbstractCrudController
{
    /**
     * @var \Acl\Service\Profile
     */
    protected $service;

    /**
     * @var \Acl\Form\Profile
     */
    protected $form;

    public function __construct(){
        parent::init();
    }

    public function indexAction()
    {
        return parent::index($this->service, $this->form);
    }

    public function gravarAction(){
        return parent::gravar($this->service, $this->form);
    }

    public function cadastroAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function excluirAction()
    {
        return parent::excluir($this->service, $this->form);
    }

    public function usersAction(){
        $profile = new Profile();
        $profile->setId($this->params('id'));
        $lista = $profile->getUsersNotIn();
        $view = new ViewModel(['lista'=>$lista]);
        $view->setTerminal(true);
        return $view;
    }

    public function addUsersAction(){
        try{
            $request = $this->getRequest();
            $post = $request->getPost();

            $profile = new Profile();
            $profile->setId($post['IdProfile']);
            if(!$profile->load()) throw  new \Exception('Perfil não encontrado');

            $userProfile = new UserProfile();
            $userProfile->setIdProfile($post['IdProfile']);
            foreach($post['user'] as $user){
                $userProfile->setIdUser($user);
                $userProfile->salvar();
                $userProfile->setId('');
            }

            $this->addSuccessMessage('Usuários adicionados com sucesso');
            return $this->redirect()->toUrl('/acl-profile/cadastro/'.$post['IdProfile']);
        }catch (\Exception $e){
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/acl-profile');
        }
    }

    public function deleteUserAction(){
        try{
            $userProfile = new UserProfile();
            $userProfile->setId($this->params('id'));
            if(!$userProfile->load()) throw new \Exception('Perfil/Usuário não encontrado');
            $userProfile->excluir();

            $this->addSuccessMessage('Usuário Removido com Sucesso');
            return $this->redirect()->toUrl('/acl-profile/cadastro/'.$userProfile->getIdProfile());
        }catch (\Exception $e){
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/acl-profile');
        }
    }

    public function servicesAction(){
        $profile = new Profile();
        $profile->setId($this->params('id'));
        $lista = $profile->getServiceNotIn();
        $view = new ViewModel(['lista'=>$lista]);
        $view->setTerminal(true);
        return $view;
    }

    public function addServicesAction(){
        try{
            $request = $this->getRequest();
            $post = $request->getPost();

            $profile = new Profile();
            $profile->setId($post['IdProfile']);
            if(!$profile->load()) throw  new \Exception('Perfil não encontrado');

            $profileService = new ProfileService();
            $profileService->setIdProfile($post['IdProfile']);
            foreach($post['service'] as $service){
                $profileService->setIdService($service);
                $profileService->salvar();
                $profileService->setId('');
            }

            $this->addSuccessMessage('Serviços adicionados com sucesso');
            return $this->redirect()->toUrl('/acl-profile/cadastro/'.$post['IdProfile']);
        }catch (\Exception $e){
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/acl-profile');
        }
    }

    public function deleteServiceAction(){
        try{
            $profileService = new ProfileService();
            $profileService->setId($this->params('id'));
            if(!$profileService->load()) throw new \Exception('Perfil/Serviço não encontrado');
            $profileService->excluir();

            $this->addSuccessMessage('Serviço removido com sucesso');
            return $this->redirect()->toUrl('/acl-profile/cadastro/'.$profileService->getIdProfile());
        }catch (\Exception $e){
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/acl-profile');
        }
    }
}
