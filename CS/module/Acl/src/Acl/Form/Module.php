<?php

namespace Acl\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Module extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('module');

        $service = new \Acl\Service\Module();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('module',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("")->setAttribute('col','col-md-6');  
        $objForm->text("Name")->required(true)->label("Nome")->setAttribute('col','col-md-6');
        $objForm->text("Url")->required(true)->label("URL")->setAttribute('col','col-md-6');
        $objForm->textarea("Description")->required(true)->label("Descrição")->setAttribute('col','col-md-12');
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');

        $objForm->tableRel('actions','\Acl\Service\Action','IdModule',['Name'=>'Nome','Description'=>'Descrição'])->setAttribute('col','col-md-12');

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}