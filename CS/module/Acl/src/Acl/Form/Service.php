<?php

namespace Acl\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Service extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('service');

        $service = new \Acl\Service\Service();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('service',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("")->setAttribute('col','col-md-6');  
        $objForm->text("Name")->required(true)->label("Nome")->setAttribute('col','col-md-6');
        $objForm->select("OwnerOnly", $service->optionsOwnerOnly)->required(true)->label("Somente Proprietário")->setAttribute('col','col-md-6');
        $objForm->textarea("Description")->required(true)->label("Descrição")->setAttribute('col','col-md-12');
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}