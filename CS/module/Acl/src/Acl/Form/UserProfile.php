<?php

namespace Acl\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class UserProfile extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('userprofile');

        $service = new \Acl\Service\UserProfile();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('userprofile',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("")->setAttribute('col','col-md-6');  
        $objForm->combo("IdUser", $service->classUser)->required(true)->label("Usuário")->setAttribute('col','col-md-6');  
        $objForm->combo("IdProfile", $service->classProfile)->required(true)->label("Perfil")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}