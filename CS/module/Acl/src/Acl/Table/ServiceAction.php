<?php

namespace Acl\Table;

use Estrutura\Table\AbstractEstruturaTable;

class ServiceAction extends AbstractEstruturaTable{

    public $table = 'ACL_SERVICE_ACTION';
    public $campos = [
        'ID'=>'Id', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}