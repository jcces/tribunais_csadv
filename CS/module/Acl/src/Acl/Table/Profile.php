<?php

namespace Acl\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Profile extends AbstractEstruturaTable{

    public $table = 'ACL_PROFILE';
    public $campos = [
        'ID'=>'Id', 
        'NAME'=>'Name', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}