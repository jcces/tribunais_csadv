<?php

namespace Acl\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Service extends AbstractEstruturaTable{

    public $table = 'ACL_SERVICE';
    public $campos = [
        'ID'=>'Id', 
        'NAME'=>'Name', 
        'DESCRIPTION'=>'Description', 
        'OWNER_ONLY'=>'OwnerOnly', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}