<?php

namespace Acl\Table;

use Estrutura\Table\AbstractEstruturaTable;

class UserProfile extends AbstractEstruturaTable{

    public $table = 'ACL_USER_PROFILE';
    public $campos = [
        'ID'=>'Id', 
        'ID_USER'=>'IdUser', 
        'ID_PROFILE'=>'IdProfile', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}