<?php

namespace Acl\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Module extends AbstractEstruturaTable{

    public $table = 'ACL_MODULE';
    public $campos = [
        'ID'=>'Id', 
        'NAME'=>'Name', 
        'DESCRIPTION'=>'Description', 
        'URL'=>'Url', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}