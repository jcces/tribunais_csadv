<?php

namespace Acl\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Service extends AbstractEstruturaService{

        protected $Id; 
        protected $Name; 
        protected $Description; 
        protected $OwnerOnly; 
        protected $DtCreated; 
        protected $DtUpdated; 
        protected $UpdatedBy; 
        protected $CreatedBy; 
        protected $Deleted; 


        public function getId()
            {
                return $this->Id;
            } 
        public function setId($Id)
            {
                return $this->Id = $Id;
            } 
        public function getName()
            {
                return $this->Name;
            } 
        public function setName($Name)
            {
                return $this->Name = $Name;
            } 
        public function getDescription()
            {
                return $this->Description;
            } 
        public function setDescription($Description)
            {
                return $this->Description = $Description;
            } 
        public function getOwnerOnly()
            {
                return $this->OwnerOnly;
            } 
        public function setOwnerOnly($OwnerOnly)
            {
                return $this->OwnerOnly = $OwnerOnly;
            } 
        public function getDtCreated()
            {
                return $this->DtCreated;
            } 
        public function setDtCreated($DtCreated)
            {
                return $this->DtCreated = $DtCreated;
            } 
        public function getDtUpdated()
            {
                return $this->DtUpdated;
            } 
        public function setDtUpdated($DtUpdated)
            {
                return $this->DtUpdated = $DtUpdated;
            } 
        public function getUpdatedBy()
            {
                return $this->UpdatedBy;
            } 
        public function setUpdatedBy($UpdatedBy)
            {
                return $this->UpdatedBy = $UpdatedBy;
            } 
        public function getCreatedBy()
            {
                return $this->CreatedBy;
            } 
        public function setCreatedBy($CreatedBy)
            {
                return $this->CreatedBy = $CreatedBy;
            } 
        public function getDeleted()
            {
                return $this->Deleted;
            } 
        public function setDeleted($Deleted)
            {
                return $this->Deleted = $Deleted;
            } 

}