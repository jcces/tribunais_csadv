<?php

namespace Acl\Entity;

use Estrutura\Service\AbstractEstruturaService;

class ProfileService extends AbstractEstruturaService{

        protected $Id; 
        protected $IdProfile; 
        protected $IdService; 
        protected $DtCreated; 
        protected $DtUpdated; 
        protected $UpdatedBy; 
        protected $CreatedBy; 
        protected $Deleted; 


        public function getId()
            {
                return $this->Id;
            } 
        public function setId($Id)
            {
                return $this->Id = $Id;
            } 
        public function getIdProfile()
            {
                return $this->IdProfile;
            } 
        public function setIdProfile($IdProfile)
            {
                return $this->IdProfile = $IdProfile;
            } 
        public function getIdService()
            {
                return $this->IdService;
            } 
        public function setIdService($IdService)
            {
                return $this->IdService = $IdService;
            } 
        public function getDtCreated()
            {
                return $this->DtCreated;
            } 
        public function setDtCreated($DtCreated)
            {
                return $this->DtCreated = $DtCreated;
            } 
        public function getDtUpdated()
            {
                return $this->DtUpdated;
            } 
        public function setDtUpdated($DtUpdated)
            {
                return $this->DtUpdated = $DtUpdated;
            } 
        public function getUpdatedBy()
            {
                return $this->UpdatedBy;
            } 
        public function setUpdatedBy($UpdatedBy)
            {
                return $this->UpdatedBy = $UpdatedBy;
            } 
        public function getCreatedBy()
            {
                return $this->CreatedBy;
            } 
        public function setCreatedBy($CreatedBy)
            {
                return $this->CreatedBy = $CreatedBy;
            } 
        public function getDeleted()
            {
                return $this->Deleted;
            } 
        public function setDeleted($Deleted)
            {
                return $this->Deleted = $Deleted;
            } 

}