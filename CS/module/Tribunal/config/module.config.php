<?php

return array(
    'router' => array(
        'routes' => array(
            'tribunal-home' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/tribunal[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'tribunal',
                        'action'     => 'index',
                    ),
                ),
            ),
            'tribunal-processo' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/tribunal-processo[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'tribunal-processo',
                        'action'     => 'index',
                    ),
                ),
            ),
            'tribunal-pesquisa' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/tribunal-pesquisa[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'tribunal-pesquisa',
                        'action'     => 'index',
                    ),
                ),
            ),

        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'tribunal' => 'Tribunal\Controller\TribunalController',
            'tribunal-processo' => 'Tribunal\Controller\ProcessoController',
            'tribunal-pesquisa' => 'Tribunal\Controller\PesquisaController',
            'tribunal-robo' => 'Tribunal\Controller\RoboController',

        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'tribunal-routine' => array(
                    'options' => array(
                        'route'    => 'start',
                        'defaults' => array(
                            'controller' => 'tribunal-robo',
                            'action'     => 'start'
//                            'action'     => 'tjrs1instancia'
                        )
                    )
                ),
                'banner' => array(
                    'options' => array(
                        'route'    => 'banner',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'banner'
                        )
                    )
                ),
                'relatorioGeral' => array(
                    'options' => array(
                        'route'    => 'relatorio-geral',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'relatorio-geral'
                        )
                    )
                ),
                'import' => array(
                    'options' => array(
                        'route'    => 'import',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'import-spreadsheet'
                        )
                    )
                ),
                'fix' => array(
                    'options' => array(
                        'route'    => 'fix',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'fix'
                        )
                    )
                ),
                'job' => array(
                    'options' => array(
                        'route'    => 'job',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'job'
                        )
                    )
                ),
                'job-fix' => array(
                    'options' => array(
                        'route'    => 'job-fix',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'job-fix'
                        )
                    )
                ),
                'job-geral' => array(
                    'options' => array(
                        'route'    => 'job-geral',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'job-geral'
                        )
                    )
                ),
                'job-test' => array(
                    'options' => array(
                        'route'    => 'test',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'test'
                        )
                    )
                ),
                'job-rodrigo' => array(
                    'options' => array(
                        'route'    => 'job-rodrigo',
                        'defaults' => array(
                            'controller' => 'tribunal',
                            'action'     => 'job-rodrigo'
                        )
                    )
                ),
            ),
        ),
    ),
);
