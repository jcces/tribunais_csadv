<?php

namespace Tribunal\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Job extends AbstractEstruturaService{

        protected $Id; 
        protected $DtInclusao; 
        protected $NumIntProcesso; 
        protected $DesComarca; 
        protected $NomAutor; 
        protected $CpfAutor; 
        protected $NomReu; 
        protected $NomJuizo; 
        protected $Uf; 
        protected $NumProcJust; 
        protected $JuizoVara; 
        protected $Alterado; 
        protected $Validado; 
        protected $Obs; 
        protected $Origem; 
        protected $DtCreated; 
        protected $DtUpdated; 
        protected $UpdatedBy; 
        protected $CreatedBy; 
        protected $Deleted; 


        public function getId()
            {
                return $this->Id;
            } 
        public function setId($Id)
            {
                return $this->Id = $Id;
            } 
        public function getDtInclusao()
            {
                return $this->DtInclusao;
            } 
        public function setDtInclusao($DtInclusao)
            {
                return $this->DtInclusao = $DtInclusao;
            } 
        public function getNumIntProcesso()
            {
                return $this->NumIntProcesso;
            } 
        public function setNumIntProcesso($NumIntProcesso)
            {
                return $this->NumIntProcesso = $NumIntProcesso;
            } 
        public function getDesComarca()
            {
                return $this->DesComarca;
            } 
        public function setDesComarca($DesComarca)
            {
                return $this->DesComarca = $DesComarca;
            } 
        public function getNomAutor()
            {
                return $this->NomAutor;
            } 
        public function setNomAutor($NomAutor)
            {
                return $this->NomAutor = $NomAutor;
            } 
        public function getCpfAutor()
            {
                return $this->CpfAutor;
            } 
        public function setCpfAutor($CpfAutor)
            {
                return $this->CpfAutor = $CpfAutor;
            } 
        public function getNomReu()
            {
                return $this->NomReu;
            } 
        public function setNomReu($NomReu)
            {
                return $this->NomReu = $NomReu;
            } 
        public function getNomJuizo()
            {
                return $this->NomJuizo;
            } 
        public function setNomJuizo($NomJuizo)
            {
                return $this->NomJuizo = $NomJuizo;
            } 
        public function getUf()
            {
                return $this->Uf;
            } 
        public function setUf($Uf)
            {
                return $this->Uf = $Uf;
            } 
        public function getNumProcJust()
            {
                return $this->NumProcJust;
            } 
        public function setNumProcJust($NumProcJust)
            {
                return $this->NumProcJust = $NumProcJust;
            } 
        public function getJuizoVara()
            {
                return $this->JuizoVara;
            } 
        public function setJuizoVara($JuizoVara)
            {
                return $this->JuizoVara = $JuizoVara;
            } 
        public function getAlterado()
            {
                return $this->Alterado;
            } 
        public function setAlterado($Alterado)
            {
                return $this->Alterado = $Alterado;
            } 
        public function getValidado()
            {
                return $this->Validado;
            } 
        public function setValidado($Validado)
            {
                return $this->Validado = $Validado;
            } 
        public function getObs()
            {
                return $this->Obs;
            } 
        public function setObs($Obs)
            {
                return $this->Obs = $Obs;
            } 
        public function getOrigem()
            {
                return $this->Origem;
            } 
        public function setOrigem($Origem)
            {
                return $this->Origem = $Origem;
            } 
        public function getDtCreated()
            {
                return $this->DtCreated;
            } 
        public function setDtCreated($DtCreated)
            {
                return $this->DtCreated = $DtCreated;
            } 
        public function getDtUpdated()
            {
                return $this->DtUpdated;
            } 
        public function setDtUpdated($DtUpdated)
            {
                return $this->DtUpdated = $DtUpdated;
            } 
        public function getUpdatedBy()
            {
                return $this->UpdatedBy;
            } 
        public function setUpdatedBy($UpdatedBy)
            {
                return $this->UpdatedBy = $UpdatedBy;
            } 
        public function getCreatedBy()
            {
                return $this->CreatedBy;
            } 
        public function setCreatedBy($CreatedBy)
            {
                return $this->CreatedBy = $CreatedBy;
            } 
        public function getDeleted()
            {
                return $this->Deleted;
            } 
        public function setDeleted($Deleted)
            {
                return $this->Deleted = $Deleted;
            } 

}