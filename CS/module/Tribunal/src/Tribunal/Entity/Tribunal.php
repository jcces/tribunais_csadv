<?php

namespace Tribunal\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Tribunal extends AbstractEstruturaService
{

    protected $Id;
    protected $Name;
    protected $Uf;
    protected $Url;
    protected $UrlProcesso;
    protected $CallMethod;
    protected $Online;
    protected $Coletando;
    protected $DtUltimaColetaComecou;
    protected $DtUltimaColetaTerminou;
    protected $DtCreated;
    protected $DtUpdated;
    protected $UpdatedBy;
    protected $CreatedBy;
    protected $Deleted;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->Uf;
    }

    /**
     * @param mixed $Uf
     */
    public function setUf($Uf)
    {
        $this->Uf = $Uf;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->Url;
    }

    /**
     * @param mixed $Url
     */
    public function setUrl($Url)
    {
        $this->Url = $Url;
    }

    /**
     * @return mixed
     */
    public function getUrlProcesso()
    {
        return $this->UrlProcesso;
    }

    /**
     * @param mixed $UrlProcesso
     */
    public function setUrlProcesso($UrlProcesso)
    {
        $this->UrlProcesso = $UrlProcesso;
    }

    /**
     * @return mixed
     */
    public function getCallMethod()
    {
        return $this->CallMethod;
    }

    /**
     * @param mixed $CallMethod
     */
    public function setCallMethod($CallMethod)
    {
        $this->CallMethod = $CallMethod;
    }

    /**
     * @return mixed
     */
    public function getOnline()
    {
        return $this->Online;
    }

    /**
     * @param mixed $Online
     */
    public function setOnline($Online)
    {
        $this->Online = $Online;
    }

    /**
     * @return mixed
     */
    public function getColetando()
    {
        return $this->Coletando;
    }

    /**
     * @param mixed $Coletando
     */
    public function setColetando($Coletando)
    {
        $this->Coletando = $Coletando;
    }

    /**
     * @return mixed
     */
    public function getDtUltimaColetaComecou()
    {
        return $this->DtUltimaColetaComecou;
    }

    /**
     * @param mixed $DtUltimaColetaComecou
     */
    public function setDtUltimaColetaComecou($DtUltimaColetaComecou)
    {
        $this->DtUltimaColetaComecou = $DtUltimaColetaComecou;
    }

    /**
     * @return mixed
     */
    public function getDtUltimaColetaTerminou()
    {
        return $this->DtUltimaColetaTerminou;
    }

    /**
     * @param mixed $DtUltimaColetaTerminou
     */
    public function setDtUltimaColetaTerminou($DtUltimaColetaTerminou)
    {
        $this->DtUltimaColetaTerminou = $DtUltimaColetaTerminou;
    }

    /**
     * @return mixed
     */
    public function getDtCreated()
    {
        return $this->DtCreated;
    }

    /**
     * @param mixed $DtCreated
     */
    public function setDtCreated($DtCreated)
    {
        $this->DtCreated = $DtCreated;
    }

    /**
     * @return mixed
     */
    public function getDtUpdated()
    {
        return $this->DtUpdated;
    }

    /**
     * @param mixed $DtUpdated
     */
    public function setDtUpdated($DtUpdated)
    {
        $this->DtUpdated = $DtUpdated;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->UpdatedBy;
    }

    /**
     * @param mixed $UpdatedBy
     */
    public function setUpdatedBy($UpdatedBy)
    {
        $this->UpdatedBy = $UpdatedBy;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->CreatedBy;
    }

    /**
     * @param mixed $CreatedBy
     */
    public function setCreatedBy($CreatedBy)
    {
        $this->CreatedBy = $CreatedBy;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->Deleted;
    }

    /**
     * @param mixed $Deleted
     */
    public function setDeleted($Deleted)
    {
        $this->Deleted = $Deleted;
    }

}