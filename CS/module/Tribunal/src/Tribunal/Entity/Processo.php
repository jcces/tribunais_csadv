<?php

namespace Tribunal\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Processo extends AbstractEstruturaService
{

    protected $Id;
    protected $IdTribunal;
    protected $Processo;
    protected $ProcessoInterno;
    protected $Url;
    protected $Dados;
    protected $Hash;
    protected $Alteracao;
    protected $Processado;
    protected $DtCreated;
    protected $DtUpdated;
    protected $UpdatedBy;
    protected $CreatedBy;
    protected $Deleted;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getIdTribunal()
    {
        return $this->IdTribunal;
    }

    /**
     * @param mixed $IdTribunal
     */
    public function setIdTribunal($IdTribunal)
    {
        $this->IdTribunal = $IdTribunal;
    }

    /**
     * @return mixed
     */
    public function getProcesso()
    {
        return $this->Processo;
    }

    /**
     * @param mixed $Processo
     */
    public function setProcesso($Processo)
    {
        $this->Processo = $Processo;
    }

    /**
     * @return mixed
     */
    public function getProcessoInterno()
    {
        return $this->ProcessoInterno;
    }

    /**
     * @param mixed $ProcessoInterno
     */
    public function setProcessoInterno($ProcessoInterno)
    {
        $this->ProcessoInterno = $ProcessoInterno;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->Url;
    }

    /**
     * @param mixed $Url
     */
    public function setUrl($Url)
    {
        $this->Url = $Url;
    }

    /**
     * @return mixed
     */
    public function getDados()
    {
        return $this->Dados;
    }

    /**
     * @param mixed $Dados
     */
    public function setDados($Dados)
    {
        $this->Dados = $Dados;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->Hash;
    }

    /**
     * @param mixed $Hash
     */
    public function setHash($Hash)
    {
        $this->Hash = $Hash;
    }

    /**
     * @return mixed
     */
    public function getAlteracao()
    {
        return $this->Alteracao;
    }

    /**
     * @param mixed $Alteracao
     */
    public function setAlteracao($Alteracao)
    {
        $this->Alteracao = $Alteracao;
    }

    /**
     * @return mixed
     */
    public function getProcessado()
    {
        return $this->Processado;
    }

    /**
     * @param mixed $Processado
     */
    public function setProcessado($Processado)
    {
        $this->Processado = $Processado;
    }

    /**
     * @return mixed
     */
    public function getDtCreated()
    {
        return $this->DtCreated;
    }

    /**
     * @param mixed $DtCreated
     */
    public function setDtCreated($DtCreated)
    {
        $this->DtCreated = $DtCreated;
    }

    /**
     * @return mixed
     */
    public function getDtUpdated()
    {
        return $this->DtUpdated;
    }

    /**
     * @param mixed $DtUpdated
     */
    public function setDtUpdated($DtUpdated)
    {
        $this->DtUpdated = $DtUpdated;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->UpdatedBy;
    }

    /**
     * @param mixed $UpdatedBy
     */
    public function setUpdatedBy($UpdatedBy)
    {
        $this->UpdatedBy = $UpdatedBy;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->CreatedBy;
    }

    /**
     * @param mixed $CreatedBy
     */
    public function setCreatedBy($CreatedBy)
    {
        $this->CreatedBy = $CreatedBy;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->Deleted;
    }

    /**
     * @param mixed $Deleted
     */
    public function setDeleted($Deleted)
    {
        $this->Deleted = $Deleted;
    }

}