<?php

namespace Tribunal\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Pesquisa extends AbstractEstruturaService{

        protected $Id; 
        protected $Palavras; 
        protected $IdTribunal; 
        protected $PesquisarTodosTribunais; 
        protected $DtCreated; 
        protected $DtUpdated; 
        protected $UpdatedBy; 
        protected $CreatedBy; 
        protected $Deleted; 


        public function getId()
            {
                return $this->Id;
            } 
        public function setId($Id)
            {
                return $this->Id = $Id;
            } 
        public function getPalavras()
            {
                return $this->Palavras;
            } 
        public function setPalavras($Palavras)
            {
                return $this->Palavras = $Palavras;
            } 
        public function getIdTribunal()
            {
                return $this->IdTribunal;
            } 
        public function setIdTribunal($IdTribunal)
            {
                return $this->IdTribunal = $IdTribunal;
            } 
        public function getPesquisarTodosTribunais()
            {
                return $this->PesquisarTodosTribunais;
            } 
        public function setPesquisarTodosTribunais($PesquisarTodosTribunais)
            {
                return $this->PesquisarTodosTribunais = $PesquisarTodosTribunais;
            } 
        public function getDtCreated()
            {
                return $this->DtCreated;
            } 
        public function setDtCreated($DtCreated)
            {
                return $this->DtCreated = $DtCreated;
            } 
        public function getDtUpdated()
            {
                return $this->DtUpdated;
            } 
        public function setDtUpdated($DtUpdated)
            {
                return $this->DtUpdated = $DtUpdated;
            } 
        public function getUpdatedBy()
            {
                return $this->UpdatedBy;
            } 
        public function setUpdatedBy($UpdatedBy)
            {
                return $this->UpdatedBy = $UpdatedBy;
            } 
        public function getCreatedBy()
            {
                return $this->CreatedBy;
            } 
        public function setCreatedBy($CreatedBy)
            {
                return $this->CreatedBy = $CreatedBy;
            } 
        public function getDeleted()
            {
                return $this->Deleted;
            } 
        public function setDeleted($Deleted)
            {
                return $this->Deleted = $Deleted;
            } 

}