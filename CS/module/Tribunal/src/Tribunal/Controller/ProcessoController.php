<?php

namespace Tribunal\Controller;

use Estrutura\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;

class ProcessoController extends AbstractCrudController
{
    /**
     * @var \Tribunal\Service\Processo
     */
    protected $service;

    /**
     * @var \Tribunal\Form\Processo
     */
    protected $form;

    public function __construct()
    {
        parent::init();
    }

    public function indexAction()
    {
        $service = $this->service;
        $form = $this->form;

        $request = $this->getRequest();

        $lista = [];
        $total = 0;
        if ($request->isPost()) {
            $post = $request->getPost();
            $form->setData($post);
            $form->isValid();
            $service->setIdTribunal($post['IdTribunal']);
            if (isset($post['Processo']) && $post['Processo'] != '') $service->setProcesso($post['Processo']);
            $lista = $service->filtrarObjeto();
            $total = count($lista);
        }

        return new ViewModel(
            [
                'service' => $service,
                'form' => $form,
                'controller' => $this->params('controller'),
                'lista' => $lista,
                'total' => $total,
            ]);
    }

    public function gravarAction()
    {
        return parent::gravar($this->service, $this->form);
    }

    public function cadastroAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function excluirAction()
    {
        return parent::excluir($this->service, $this->form);
    }
}
