<?php

namespace Tribunal\Controller;

use Estrutura\Controller\AbstractCrudController;
use Estrutura\Service\AbstractEstruturaService;
use Estrutura\Service\Conexao;
use Estrutura\Service\ExcelIO;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Tribunal\Service\AsyncOperation;
use Tribunal\Service\Job;
use Tribunal\Service\JobRodrigo;
use Tribunal\Service\Processo;
use Tribunal\Service\Robo;
use Tribunal\Service\Tribunal;
use Tribunal\Service\TwoCaptcha;
use Zend\View\Model\ViewModel;
use Zend\XmlRpc\Generator\DomDocument;

class TribunalController extends AbstractCrudController
{
    /**
     * @var \Tribunal\Service\Pesquisa
     */
    protected $service;

    /**
     * @var \Tribunal\Form\Pesquisa
     */
    protected $form;

    public function __construct()
    {
        parent::init();
    }

    public function indexAction()
    {
//        exec("dir", $output);
//        debug($output);
//        $file = './data/captchas/1527606219.png';
//        $file = base64_encode($file);
//        $file = 'tjma.png';
//        $fileAlter = './data/test-ocr/captcha_es_ALT.png';
//        $image = imagecreatefrompng($file);
//        imagefilter($image, IMG_FILTER_GRAYSCALE);
//        imagefilter($image, IMG_FILTER_CONTRAST, -50);
//        imagepng($image, $fileAlter);
//
//        $tess = new TesseractOCR($file);
//        $tess->tessdataDir('/data/test-ocr');
//        echo $tess->run();
//        debug(1);

        $view = parent::index($this->service, $this->form);
        $view->setVariables(['tribunais' => []]);
        return $view;
    }

    public function cadastroAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function gravarAction()
    {
        return parent::gravar($this->service, $this->form);
    }

    public function excluirAction()
    {
        return parent::excluir($this->service, $this->form);
    }

    public function logsAction()
    {
        $dir = './data/tribunais-log/';
        $files = scandir($dir);
        $tratados = [];
        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) continue;

            $tratados[$file] = file_get_contents($dir . $file);
        }

        return new ViewModel(['files' => $tratados]);
    }

    public function tribunalAction()
    {
        return parent::index($this->service, $this->form);
    }

    public function tribunaisAction()
    {
        try {
            $tribunal = $this->params('id');
            $data = '';
            $fileNameFriendly = '';
            if ($tribunal) {
                $file = './data/tribunais-data/' . $tribunal;
                if (!file_exists($file)) throw new \Exception('Tribunal não está disponível.');

                $fileNameFriendly = ucfirst(str_replace('.json', '', $tribunal));
                $fileNameFriendly = str_replace(['inst', 'Inst', 'Tj'], '', $fileNameFriendly);
                $uf = substr($fileNameFriendly, 0, 2);
                $inst = substr($fileNameFriendly, 2, 1);
                $fileNameFriendly = strtoupper($uf) . ' ' . $inst . '° Instância';

                $data = file_get_contents($file);
            }

            $dir = './data/tribunais-data/';
            $files = scandir($dir);
            $tratados = [];
            foreach ($files as $file) {
                if (in_array($file, ['.', '..', 'tjrj1inst.json', 'tjrj2inst.json'])) continue;
                $tratados[] = $file;
            }

            $description = [
                'tjdf1inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjdf2inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjgo1inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjgo2inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjmg1inst.json' => [
                    'pesquisa' => 'CAIXA SEGURADORA',
                    'total' => '858 apenas em BH',
                    'obs' => '1 - O argumento de pesquisa CAIXA SEGUR não obteve resultado. 2 - Uma nova varredura será realizada para coletar todos os processos. Por agora temos uma amostra de alguns processos.',
                ],
                'tjmg2inst.json' => [
                    'pesquisa' => 'CAIXA SEGURADORA',
                    'total' => '2480',
                    'obs' => '1 - O argumento de pesquisa CAIXA SEGUR não obteve resultado. 2 - Uma nova varredura será realizada para coletar todos os processos. Por agora temos uma amostra de alguns processos.',
                ],
                'tjsp1inst.json' => [
                    'pesquisa' => 'CAIXA SEGUROS',
                    'obs' => 'Os argumentos de pesquisa CAIXA SEGUR e CAIXA SEGURADORA não obtiveram resultados.',
                ],
                'tjsp2inst.json' => [
                    'pesquisa' => 'CAIXA SEGUROS',
                    'obs' => 'Os argumentos de pesquisa CAIXA SEGUR e CAIXA SEGURADORA não obtiveram resultados.',
                ],
            ];

            $retorno = new ViewModel(['files' => $tratados, 'tribunal' => $fileNameFriendly, 'tribunalSelected' => $tribunal, 'data' => $data, 'description' => $description]);
        } catch (\Exception $e) {
            $retorno = new ViewModel(['error' => true, 'message' => $e->getMessage(), 'data' => []]);
        }
        return $retorno;
    }

    public function processosAction()
    {
        try {
            $tribunal = $this->params('id');
            if (!$tribunal) throw new \Exception('Favor selecionar um tribunal.');

            $file = './data/tribunais-data/' . $tribunal;
            if (!file_exists($file)) throw new \Exception('Tribunal não está disponível.');

            $fileNameFriendly = ucfirst(str_replace('.json', '', $tribunal));
            $fileNameFriendly = str_replace(['inst', 'Inst', 'Tj'], '', $fileNameFriendly);
            $uf = substr($fileNameFriendly, 0, 2);
            $inst = substr($fileNameFriendly, 2, 1);
            $fileNameFriendly = strtoupper($uf) . ' ' . $inst . '° Instância';

            $data = file_get_contents($file);

            $retorno = new ViewModel(['tribunal' => $fileNameFriendly, 'data' => $data]);
        } catch (\Exception $e) {
            $retorno = $this->backToOrigenUrl();
        }
        return $retorno;
    }

    public function deleteLogAction()
    {
        $logName = $this->params('id');
        $dir = './data/tribunais-log/';
        $ext = '.txt';
        if (file_exists($dir . $logName . $ext)) {
            file_put_contents($dir . $logName . $ext, '');
        }
        return $this->redirect()->toUrl('/logs');
    }

    public function startAction()
    {
        $tribunais = new Tribunal();
        $tribunais->start();
        echo 'Done!' . PHP_EOL;
        die;
    }

    public function importsqlAction()
    {
        $path = './data/database/data/';
        $files = scandir($path);
        $tratados = [];
        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) continue;
            $tratados[] = str_replace('.sql', '', $file);
        }
        $view = parent::index($this->service, $this->form);
        $view->setVariable('files', $tratados);
        return $view;
    }

    public function executesqlAction()
    {
        $path = './data/database/data/';
        $file = $this->params('id');

        $sql = file_get_contents($path . $file . '.sql');

        $data = Conexao::execSql($sql);
        debug($data);
    }

    public function bannerAction()
    {

        $robo = new Robo();
        $driver = $robo->startDriver();
        $driver->get('http://www.acessowifilivre.com.br/index2.php');

        $robo->log('ENTROU');

        $driver->findElement(WebDriverBy::xpath('//*[@id="bannerfull"]'))->click();
        $robo->log('CLICOU!');
        sleep(10);
        $driver->close();
        $driver->quit();
        $robo->log('FECHOU!');

        echo 'Done!' . PHP_EOL;
        die;
    }

    public function relatorioGeralAction()
    {
//        ini_set('memory_limit', '-1');
//        $excel = new ExcelIO();
//        echo 'LENDO EXCEL...'.PHP_EOL;
//        $arr = $excel->fileToArray('./data/relatorio-geral/RELATORIO_GERAL.xlsx');
//        echo 'CRIANDO JSON...'.PHP_EOL;
//        file_put_contents('./data/relatorio-geral/RELATORIO_GERAL.json',json_encode($arr));
//        echo 'DONE!'.PHP_EOL;

        $jsonArr = json_decode(file_get_contents('./data/relatorio-geral/RELATORIO_GERAL_TRATADOS.json'), true);
        $tratados = [];
        foreach ($jsonArr as $uf => $processos) {
            echo count($processos) . ' ' . $uf . PHP_EOL;
            continue;
//            foreach ($processos as $item) {
//                $tratados[$item['UF']][] = $item;
//            }
        }
        die;
        file_put_contents('./data/relatorio-geral/RELATORIO_GERAL_TRATADOS.json', json_encode($tratados));
        echo 'DONE!' . PHP_EOL;
        die;
    }

    public function importSpreadsheetAction()
    {
        $robo = new Robo();

//        $fileName = "PROCESSOS-NAO-ENCONTRADOS-14082018-ATUALIZADO-MARCIO";
        $fileName = "PROCESSOS-NAO-ENCONTRADOS-23082018";

        /*ini_set('memory_limit', '-1');
        $excel = new ExcelIO();
        echo 'LENDO EXCEL...'.PHP_EOL;
        $arr = $excel->fileToArray("./data/database/JOB/PROCESSOS RETORNADOS CLIENTE/{$fileName}.xlsx");
        echo 'CRIANDO JSON...'.PHP_EOL;
        file_put_contents("./data/database/JOB/PROCESSOS RETORNADOS CLIENTE/{$fileName}.json",json_encode($arr));
        die;*/

        $isDebug = false;
        $jsonArr = json_decode(file_get_contents("./data/database/JOB/PROCESSOS RETORNADOS CLIENTE/{$fileName}.json"), true);
        $qtd = 0;
        $duplicados = [];
        $problemas = [];
        $ufs = [];
        $index = 0;
        $total = count($jsonArr['DADOS']);
        $tratados = [];
        $totais = [];
        $totalRegistros = count($jsonArr['DADOS']);
        $robo->log("{$totalRegistros} REGISTROS");
        /** @var $jobs \Tribunal\Service\Job[] */
        foreach ($jsonArr['DADOS'] as $processos) {
            $index++;
//            $robo->log("{$robo->percentage($totalRegistros,$index)}");

            /**
             * IMPORTAR PLANILHA DO MÁRCIO
             * 17/08/2018
             */
            //PARTE 2
            if (false) {

                $processos['NUM_PROC_JUST'] = (string)$processos['NUM_PROC_JUST'];

                //PRIMEIRA CONDIÇÃO
                $campo = 'PROC_SCPJUD_CNJ';
                if ($processos[$campo] != '' && $processos[$campo] != '0' && $processos[$campo] != 0 && strlen($processos[$campo]) > strlen($processos['NUM_PROC_JUST'])) {
                    $robo->log("1° NUM_PROC_JUST => {$processos['NUM_PROC_JUST']} <> {$campo} => {$processos[$campo]}");
                    echo "Aceita a alteração?" . PHP_EOL;
                    $handle = fopen("php://stdin", "r");
                    $line = fgets($handle);
                    if (trim($line) == 'y') {
                        $robo->log('REGISTRO ALTERADO COM SUCESSO!!!');
                    }
                    fclose($handle);
                }
                //SEGUNDA CONDIÇÃO
                $campo = 'PROC_SCPJUD_ANTIGO';
                if ($processos[$campo] != '' && $processos[$campo] != '0' && $processos[$campo] != 0 && strlen($processos[$campo]) > strlen($processos['NUM_PROC_JUST'])) {
                    $robo->log("2° NUM_PROC_JUST => {$processos['NUM_PROC_JUST']} <> {$campo} => {$processos[$campo]}");
                    echo "Aceita a alteração?" . PHP_EOL;
                    $handle = fopen("php://stdin", "r");
                    $line = fgets($handle);
                    if (trim($line) == 'y') {
                        $robo->log('REGISTRO ALTERADO COM SUCESSO!!!');
                    }
                    fclose($handle);
                }
                //TERCEIRA CONDIÇÃO
                $campo = 'PROC_ARTERIA_CNJ';
                if ($processos[$campo] != '' && $processos[$campo] != '0' && $processos[$campo] != 0 && strlen($processos[$campo]) > strlen($processos['NUM_PROC_JUST'])) {
                    $robo->log("3° NUM_PROC_JUST => {$processos['NUM_PROC_JUST']} <> {$campo} => {$processos[$campo]}");
                    echo "Aceita a alteração?" . PHP_EOL;
                    $handle = fopen("php://stdin", "r");
                    $line = fgets($handle);
                    if (trim($line) == 'y') {
                        $robo->log('REGISTRO ALTERADO COM SUCESSO!!!');
                    }
                    fclose($handle);
                }
                //QUARTA CONDIÇÃO
                $campo = 'PROC_ARTERIA_ANTIGO';
                if ($processos[$campo] != '' && $processos[$campo] != '0' && $processos[$campo] != 0 && strlen($processos[$campo]) > strlen($processos['NUM_PROC_JUST'])) {
                    $robo->log("4° NUM_PROC_JUST => {$processos['NUM_PROC_JUST']} <> {$campo} => {$processos[$campo]}");
                    echo "Aceita a alteração?" . PHP_EOL;
                    $handle = fopen("php://stdin", "r");
                    $line = fgets($handle);
                    if (trim($line) == 'y') {
                        $robo->log('REGISTRO ALTERADO COM SUCESSO!!!');
                    }
                    fclose($handle);
                }

                /*$job = new Job();
                $job->setId($processos['ID']);
                $job->load();*/
            }
            //PARTE 1
            if (false) {
                $job = new Job();
                $job->setId($processos['ID']);
                $job->load();

                @$totais['TOTAL']++;

                $processos['NUM_PROC_JUST'] = (string)$processos['NUM_PROC_JUST'];
                if ($job->getNumProcJust() != $processos['NUM_PROC_JUST']) {
                    if (preg_match('/(CS)/', $job->getObs())) {
                        $robo->log("JÁ ATUALIZADO => {$job->getNumProcJust()}");
                        @$totais['JA_ATUALIZADO']++;
                    }
                    $robo->log("DE => {$processos['NUM_PROC_JUST']} PARA => {$job->getNumProcJust()}");
                    $processos['NUM_PROC_JUST'] = $job->getNumProcJust();
                }

                foreach ($processos as $keyname => $value) {
                    if (preg_match('/_VALIDA/', $keyname)) unset($processos[$keyname]);
                }

                $tratados['DADOS'][] = $processos;
            }

            /**
             * IMPORTAR PLANILHAS DA VALÉRIA
             */
            if (true) {
//                $nroNovoProcessoPlanilha = (string)$robo->cleanString($processos['NUMERO DO PROCESSO COMPLETO E CORRETO']);
                $nroNovoProcessoPlanilha = (string)$robo->cleanString($processos['NOVO_NRO']);
                $nroAntigoProcessoPlanilha = (string)$robo->cleanString($processos['NUM_PROC_JUST']);

                if (
                    $nroNovoProcessoPlanilha !== 0 &&
                    $nroNovoProcessoPlanilha !== '0' &&
                    $nroNovoProcessoPlanilha != '' &&
                    !is_null($nroNovoProcessoPlanilha) &&
                    !empty($nroNovoProcessoPlanilha)
                ) {

                    if ($nroNovoProcessoPlanilha != $nroAntigoProcessoPlanilha) {

                        if (preg_match("/[a-z]/i", $nroNovoProcessoPlanilha)) {
                            $robo->log("{$robo->percentage($total,$index)} => SKIPPING TRASH => {$nroNovoProcessoPlanilha}");
                            $problemas[] = $nroNovoProcessoPlanilha;
                            continue;
                        }

                        $job = new Job();
                        $job->setNumIntProcesso($processos['NUM_INT_PROCESSO']);
                        $job->setUf($processos['UF']);
                        $job->setAlterado('0');
                        $jobs = $job->filtrarObjeto();

                        if (count($jobs) > 1) {
                            $duplicados[] = [$processos['NUM_INT_PROCESSO'], $processos['NUM_PROC_JUST']];
                            $robo->log("{$robo->percentage($total,$index)} => DUPLICADO!");
                        } else {
                            foreach ($jobs as $item) {
                                if ($item->getNumProcJust() == $nroNovoProcessoPlanilha) {
                                    $robo->log("SKIPPING => {$processos['UF']} => {$nroNovoProcessoPlanilha}");
                                    continue;
                                }
                                $nroAntigo = $item->getNumProcJust();
                                $item->setNumProcJust($nroNovoProcessoPlanilha);
                                $item->setObs("NRO ATUALIZADO (CS.2308)|" . $item->getObs());
                                if (!$isDebug) $item->salvar(true);

                                if (!isset($ufs[$item->getUf()])) $ufs[$item->getUf()] = 0;
                                $ufs[$item->getUf()]++;

                                $nroAntigo = str_pad($nroAntigo, 26, ' ', STR_PAD_RIGHT);
                                $robo->log("{$robo->percentage($total,$index)} => {$item->getUf()} => DE: {$nroAntigo} PARA: {$item->getNumProcJust()}");
                                $qtd++;
                            }
                        }
                    }
                }
            }
        }

        debug([
            'PROBLEMAS' => $problemas,
            'DUPLICADOS' => $duplicados,
            'TOTAL POR UF' => $ufs,
            'TOTAL' => $qtd
        ]);

        debug($totais);
        file_put_contents("./data/database/JOB/PROCESSOS RETORNADOS CLIENTE/{$fileName}.json", json_encode($tratados));

        echo 'DONE!' . PHP_EOL;
        die;
    }

    public function jobAction()
    {
        $job = new Job();

        //SÃO PAULO - ALL DONE!!!
//        $job->start('SP','tjsp1instanciaesaj');//DONE
//        $job->start('SP', 'tjsp2instanciaesaj');//DONE
//        $job->start('SP', 'tjsprecursalesaj');//DONE
//        $job->start('SP', 'tjspjf');//DONE
//        $job->startDirect('SP', 'jeftrf3');//DONE
//        $job->startDirect('SP', 'jftrf3'); //@TODO - APENAS 2 PROCESSOS ENCONTRADO // pje2g.trf3.jus.br/pje/ConsultaPublica/listView.seam
//        $job->start('SP', 'tjsp1instanciatrf3pje');//DONE
//        $job->startDirect('SP', 'tjsp2instanciatrf3pje');//DONE

        //TRIBUNAIS MS - MATO GROSSO DO SUL - ALL DONE!!!
//        $job->startDirect('MS', 'tjmsjf');//DONE
//        $job->startDirect('MS', 'jeftrf3');//DONE
//        $job->startDirect('MS', 'jftrf3');//RODANDO - lento demais
//        $job->startDirect('MS', 'tjms1instanciaesaj');//DONE
//        $job->startDirect('MS', 'tjms2instanciaesaj');//DONE
//        TRF3 1° FEITA MANUALMENTE
//        TRF3 2° NÃO EXISTE MAIS PROCESSOS COM 5 NO BANCO DE DADOS


        //SANTA CATARINA - ALL DONE!!!
//        $job->startDirect('SC', 'tjsc1instanciaesaj');//DONE
//        $job->startDirect('SC', 'tjsc2instanciaesaj');//DONE
//        $job->startDirect('SC', 'tjscturmarecursalesaj');//DONE
//        $job->startDirect('SC', 'tjscjf');//ANTIGO
//        $job->startDirect('SC', 'trf4', ['uf' => 'TRF', 'codeTribunal' => '', 'puppet' => '03858136520034040000', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('SC', 'trf4', ['uf' => 'SC', 'codeTribunal' => '', 'puppet' => '00023116220084047104', 'tipoNro' => 'sem_mascara']);//DONE

        //RIO GRANDE DO SUL - ALL DONE!!!
//        $job->startDirect('RS', 'trf4', ['uf' => 'TRF', 'codeTribunal' => '', 'puppet' => '03858136520034040000', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('RS', 'trf4', ['uf' => 'RS', 'codeTribunal' => '', 'puppet' => '00023116220084047104', 'tipoNro' => 'sem_mascara']);//DONE
        //RS PESQUISA GERAL PARA 1° E 2° INSTÂNCIA, TURMA RECURSAL E JUIZADO ESPECIAL
//        $job->startDirect('RS', 'tjrspesquisageral',['fonte' => '', 'nroCnj' => false]);//DONE
//        $job->startDirect('RS', 'tjrspesquisageral',['fonte' => '', 'nroCnj' => true]);//DONE
        //http://www.tjrs.jus.br/pje/ConsultaPublica/listView.seam -> NÃO RESULTA PROCESSOS

        //MINAS GERAIS - ALL DONE!!!
//        $job->startDirect('MG', 'tjmgprojudi1instancia');//DONE
//        $job->startDirect('MG', 'tjmgprojudi2instancia');////NÃO HÁ PROCESSOS COM PREFIXO 9 (TERMINAR SE PRECISAR RODAR)
//        $job->startDirect('MG', 'tjmgpje');//DONE
//        $job->startDirect('MG', 'tjmg2instancia');//DONE
//        $job->startDirect('MG', 'tjmg1instancia');//DONE
//        $job->startDirect('MG', 'tjmgpjerecursal');//@TODO TERMINAR MÉTODO - SITE NÃO FUNCIONA - ENVIADO PARA DRA. VALÉRIA CHECAR.


        //RODRIGO - PR - PARANÁ - ALL DONE!!!
//        $job->startDirect('PR', 'trf4', ['uf' => 'TRF', 'codeTribunal' => '.4.04.', 'puppet' => '03858136520034040000', 'tipoNro' => 'cnj']);//DONE
//        $job->startDirect('PR', 'trf4', ['uf' => 'TRF', 'codeTribunal' => '', 'puppet' => '03858136520034040000', 'tipoNro' => 'cjf']);//DONE
//        $job->startDirect('PR', 'trf4', ['uf' => 'TRF', 'codeTribunal' => '', 'puppet' => '03858136520034040000', 'tipoNro' => 'sem_mascara']);//DONE
        //$job->startDirect('PR', 'trf4', ['uf' => 'PR', 'codeTribunal' => '.4.04.', 'puppet' => '50133796120164047000', 'tipoNro' => 'cnj']);//DONE
        //$job->startDirect('PR', 'trf4', ['uf' => 'PR', 'codeTribunal' => '', 'puppet' => '50133796120164047000', 'tipoNro' => 'cjf']);//DONE
//        $job->startDirect('PR', 'trf4', ['uf' => 'PR', 'codeTribunal' => '', 'puppet' => '50133796120164047000', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('PR', 'projuditjpr', ['instancia' => '2']);//DONE
//        $job->startDirect('PR', 'projuditjpr', ['instancia' => '1']);//DONE
//        $job->startDirect('PR','tjprsnu');//MUITO LENTO E COM ERROS DE CAPTCHA
//        $job->startDirect('PR','tjprfederal');//TRF4 SCRIPT SUBSTITUI - NÃO RODAR ESSE!
//        $job->startDirect('PR','justicaweb');//DONE
//        $job->startDirect('PR','tjprTurmasRecursais');//DONE
//        $job->startDirect('PR','portaltjprSegundaInstancia');//DONE
//        $job->startDirect('PR','portaltjprPrimeiraInstancia');//DONE
        /*Foi feito manualmente a extração da vara
        Foram encontrados dois processos
        O padrão começa com 5 e possui o identificador 8.16
       $job->startDirect('PR','tjpjetjpr');*/ //DONE

        //PERNAMBUCO - PE - ALL DONE!!!
//        $job->startDirect('PE', 'trf5', ['uf' => 'PE', 'codeTribunal' => '.4.05.', 'tipoNro' => 'cnj']);//DONE
//        $job->startDirect('PE', 'trf5', ['uf' => 'PE', 'codeTribunal' => '', 'tipoNro' => 'cjf']);//DONE
//        $job->startDirect('PE', 'trf5', ['uf' => 'PE', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('PE', 'jfpe', ['uf' => 'PE', 'codeTribunal' => '.4.05.', 'tipoNro' => 'cnj']);//DONE
//        $job->startDirect('PE', 'jfpe', ['uf' => 'PE', 'codeTribunal' => '', 'tipoNro' => 'cjf']);//DONE
//        $job->startDirect('PE', 'jfpe', ['uf' => 'PE', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('PE', 'tjpeprojudi');//@TODO A RODAR - LENTOOO
//        $job->startDirect('PE', 'tjpe1e2instfisico');//DONE
//        $job->startDirect('PE', 'tjpe2instfisicoantigo');//DONE
//        $job->startDirect('PE', 'tjpejuizadoespecial');//DONE
//        $job->startDirect('PE', 'tjpepje2inst');//DONE
//        $job->startDirect('PE', 'tjpepje1inst');//DONE
        //NOTA => 682 PROCESSOS NÃO FORAM ENCONTRADOS

        //GOIÁS - GO - ALL DONE!!!
//        $job->startDirect('GO', 'tjgoprojudi',['tipoNro' => 'cnj']);//DONE
//        $job->startDirect('GO', 'tjgoprojudi',['tipoNro' => 'antigo']);//DONE
//        $job->startDirect('GO', 'tjgopjd',['tipoNro' => 'cnj']);//DONE - ALGUNS PROCESSOS FORAM FEITOS A MÃO PQ ERAM POUCOS QUE ESTAVAM QUEBRANDO A APP PQ RESULTAVA MAIS DE UM REGISTRO COM O MESMO NRO
//        $job->startDirect('GO', 'tjgopjd',['tipoNro' => 'antigo']);//DONE - APENAS 1 PROCESSO COM SEGREDO DE JUSTIÇA
//        $job->startDirect('GO', 'tjgo2instfisico',['tipoNro' => 'cnj']);//DONE - NENHUM PROCESSO - PARECE SER UM SISTEMA ANTIGO DE CONSULTA
//        $job->startDirect('GO', 'tjgo2instfisico',['tipoNro' => 'antigo']);//DONE - PARECE SER UM SISTEMA ANTIGO DE CONSULTA
//        $job->startDirect('GO', 'tjgo1instfisico',['localidade'=>'interior','tipoNro' => 'cnj']);//DONE
//        $job->startDirect('GO', 'tjgo1instfisico',['localidade'=>'capital','tipoNro' => 'cnj']);//DONE
//        $job->startDirect('GO', 'tjgo1instfisico',['localidade'=>'interior','tipoNro' => 'antigo']);//DONE
//        $job->startDirect('GO', 'tjgo1instfisico',['localidade'=>'capital','tipoNro' => 'antigo']);//DONE

        //CEARÁ - CE - ALL DONE
//        $job->startDirect('CE', 'trf5', ['uf' => 'CE', 'codeTribunal' => '.4.05.', 'tipoNro' => 'cnj']);//DONE
//        $job->startDirect('CE', 'trf5', ['uf' => 'CE', 'codeTribunal' => '', 'tipoNro' => 'cjf']);//DONE
//        $job->startDirect('CE', 'trf5', ['uf' => 'CE', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('CE', 'jfce', ['uf' => 'CE', 'codeTribunal' => '.4.05.', 'tipoNro' => 'cnj']);//DONE
//        $job->startDirect('CE', 'jfce', ['uf' => 'CE', 'codeTribunal' => '', 'tipoNro' => 'cjf']);//DONE
//        $job->startDirect('CE', 'jfce', ['uf' => 'CE', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('CE', 'tjceesaj1grau');//DONE
//        $job->startDirect('CE', 'tjceesaj2grau');//DONE
//        $job->startDirect('CE', 'tjce1instanciafisico');//DONE
//        $job->startDirect('CE', 'tjceprojudi');//DONE
//        $job->startDirect('CE', 'tjcejuizadoespecial');//DESENVOLVENDO - SITE LENTO - ABORTADO!!!
//        $job->startDirect('CE', 'tjcepje1grau');//DONE
//        $job->startDirect('CE', 'tjcepje2grau');//DONE
        //TEM MAIS P CE?


        //TRIBUNAIS PA - PARÁ - ALL DONE
//        $job->startDirect('PA', 'tjpaporparte');//DONE
//        $job->startDirect('PA', 'tjpascript');//DONE - DEPOIS RODAR PEGANDO O JSON GERADO NO tjpaporparte
//        $job->startDirect('PA', 'tjpa');//DONE - NRO ANTIGO COMPLEXO PARA PESQUISA, HA APENAS 4 DO TIPO
//        $job->startDirect('PA', 'tjpaprojudiA');//DONE - SEM FILTRO USANDO GET URL
//        $job->startDirect('PA', 'tjpaprojudiB');//DONE - USANDO FORM COM NRO CNJ
//        $job->startDirect('PA', 'tjpapje1grau');//DONE
//        $job->startDirect('PA', 'tjpapje2grau');//DONE
//        $job->startDirect('PA', 'tjpawebservice');//DESENVOLVENDO - NÃO CONSEGUI CONSULTAR

        //TRIBUNAIS DF - DISTRITO FEDERAL - ALL DONE
//        $job->startDirect('DF', 'tjdf1instanciaget');//DONE
//        $job->startDirect('DF', 'tjdf1instancia');//DONE
//        $job->startDirect('DF', 'tjdf2instancia');//DONE
//        $job->startDirect('DF', 'tjdfprojudi');//DONE
//        $job->startDirect('DF', 'tjdfpje1grau');//DONE
//        $job->startDirect('DF', 'tjdfpje2grau');//DONE

        //TRIBUNAIS MT - MATO GROSSO ######### RODRIGO - ALL DONE
//        $job->startDirect('MT', 'tjmt1instanciafisico');//DONE
//        $job->startDirect('MT', 'tjmt2instanciafisico');//DONE
//        $job->startDirect('MT', 'tjmtturmarecursal');//DONE

        //TRIBUNAL AMAPÁ - AP - ALL DONE
//        $job->startDirect('AP', 'tjaptucujuris');//DONE

        //TRIBUNAL SERGIPE - SE - ALL DONE
//        $job->startDirect('SE', 'tjsejf');//DONE
//        $job->startDirect('SE', 'tjsejfpje');//DONE
//        $job->startDirect('SE', 'tjsejfcreta');//DONE
//        $job->startDirect('SE', 'tjsefisico');//DONE

        //TRIBUNAL RIO GRANDE DO NORTE - RN - ALL DONE
//        $job->startDirect('RN', 'tjrnesaj1grau');//DONE
//        $job->startDirect('RN', 'tjrnesaj2grau');//DONE
//        $job->startDirect('RN', 'tjrnesajjuizadoespecial');//DONE
//        $job->startDirect('RN', 'tjrnesajjuizadoespecialcreta');//DONE
//        $job->startDirect('RN', 'tjrnprojudi');//DONE
//        $job->startDirect('RN', 'tjrnjf');//DONE
//        $job->startDirect('RN', 'tjrnpje1grau');//DONE
//        $job->startDirect('RN', 'tjrnpje2grau');//DONE

        //TRIBUNAL ALAGOAS - AL - ALL DONE
//        $job->startDirect('AL', 'tjaljf');//DONE
//        $job->startDirect('AL', 'tjalesaj1grau');//DONE
//        $job->startDirect('AL', 'tjalesaj2grau');//DONE
//        $job->startDirect('AL', 'tjaljfpje');//DONE
//        $job->startDirect('AL', 'tjalprojudi');//DONE

        //TRIBUNAL AMAZONAS - AM - ALL DONE
//        $job->startDirect('AM', 'tjamesaj1grau');//DONE
//        $job->startDirect('AM', 'tjamesaj2grau');//DONE
//        $job->startDirect('AM', 'tjamprojudi');//DONE

        //TRIBUNAL RONDÔNIA/RO - ALL DONE
//        $job->startDirect('RO', 'tjroprojudi');//DONE
//        $job->startDirect('RO', 'tjrofisico1grau');//DONE
//        $job->startDirect('RO', 'tjrofisico2grau');//DONE
//        $job->startDirect('RO', 'tjropje1grau');//DONE
//        $job->startDirect('RO', 'tjropje2grau');//DONE

        //TRIBUNAL TOCANTINS/TO - ALL DONE
//        $job->startDirect('TO', 'tjtofisico');//DONE
//        $job->startDirect('TO', 'tjtoprojudi');//@TODO A RODAR (FORA DO AR)
//        $job->startDirect('TO', 'tjtoeproc1grau');//DONE
//        $job->startDirect('TO', 'tjtoeproc2grau');//DONE
//        $job->startDirect('TO', 'trf1', ['uf' => 'TO', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('TO', 'trf1', ['uf' => 'TRF1', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('TO', 'trf1secao', ['uf' => 'TO']);//DONE

        //TRIBUNAL ACRE/AC - ALL DONE
//        $job->startDirect('AC', 'tjacesaj1grau');//DONE
        //FALTARAM POUCOS QUE CHEQUEI EM OUTRAS ESTÂNCIAS, NÃO ENCONTRADO.

        //TRIBUNAL RORAIMA/RR - ALL DONE
//        $job->startDirect('RR', 'tjrr1e2grau');//DONE
//        $job->startDirect('RR', 'projuditjrr', ['instancia' => '2']);//DONE
//        $job->startDirect('RR', 'tjrrpje1grau');//DONE
//        $job->startDirect('RR', 'tjrrpje2grau');//DONE

        /**
         * PARAÍBA
         * rodrigo.silva
         */
//        $job->startDirect('PB','ejustjpbrecurso');//DONE


        /**
         * RODOU TRF1 GERAL PARA TODOS AS UFS ABAIXO
         */
        /*$ufsTrf1 = ['AC','AM','AP','DF','MA','MT','PA','RO','RR','TO','BA','GO','MG','PI'];
        $ufsTrf1 = ['AC','RO','RR'];
        foreach ($ufsTrf1 as $uf) {
            $job->startDirect($uf, 'trf1', ['uf' => $uf, 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);
            $job->startDirect($uf, 'trf1', ['uf' => 'TRF1', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);

            $time = '[' . date('Y-m-d H:i:s') . '] => ';
            $message = $time . $uf . PHP_EOL;
            echo $message;
            file_put_contents("./data/tribunais-log/trf1.txt", $message, FILE_APPEND | LOCK_EX);
        }*/
        /**
         * TRF1 SECAO
         */
        /*$ufsTrf1 = ['BA','MG','PI','GO','AC','AM','AP','DF','MA','MT','PA','RO','RR','TO'];//DONE
        $ufsTrf1 = ['AC','RO','RR'];
        foreach ($ufsTrf1 as $uf) {
            $time = '[' . date('Y-m-d H:i:s') . '] => STARTS - ';
            $message = $time . $uf . PHP_EOL;
            echo $message;
            file_put_contents("./data/tribunais-log/trf1secao{$uf}.txt", $message, FILE_APPEND | LOCK_EX);


            $job->startDirect($uf, 'trf1secao',['uf' => $uf]);

            $time = '[' . date('Y-m-d H:i:s') . '] => ENDS - ';
            $message = $time . $uf . PHP_EOL;
            echo $message;

            file_put_contents("./data/tribunais-log/trf1secao{$uf}.txt", $message, FILE_APPEND | LOCK_EX);
        }*/

        /**
         * TRF1 PJE
         */
        /*$trf1pje = ['AC','AM','AP','DF','MA','MT','PA','RO','RR','TO','BA','PI','GO','MG'];
        $trf1pje = ['AC','RO','RR'];
        foreach ($trf1pje as $uf) {
            $job->startDirect($uf, 'trf1pje1g',['uf'=>$uf]);
            $job->startDirect($uf, 'trf1pje2g',['uf'=>$uf]);

            $time = '[' . date('Y-m-d H:i:s') . '] => ';
            $message = $time . $uf . PHP_EOL;
            echo $message;
            file_put_contents("./data/tribunais-log/trf1pje.txt", $message, FILE_APPEND | LOCK_EX);
        }*/


        /**
         * RODAR TRF5 GERAL PARA TODOS AS UFS ABAIXO
         */
        /*$ufsTrf5 = ['AL','CE','PB','PE','RN','SE'];
        $ufsTrf5 = ['PB','PE'];
        foreach ($ufsTrf5 as $uf) {
//            $job->startDirect($uf, 'trf5', ['uf' => $uf, 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);
            $job->startDirect($uf, 'trf5pje');
            echo $uf.PHP_EOL;

            $time = '[' . date('Y-m-d H:i:s') . '] => ';
            $message = $time . $uf . PHP_EOL;
            echo $message;
//            file_put_contents("./data/tribunais-log/trf5.txt", $message, FILE_APPEND | LOCK_EX);
            file_put_contents("./data/tribunais-log/trf5pje.txt", $message, FILE_APPEND | LOCK_EX);
        }*/

        /**
         * JF PJE TRF5
         */
        /*$ufsTrf5 = ['AL','CE','PB','PE','RN','SE'];
        $ufsTrf5 = ['PB','PE'];
        foreach ($ufsTrf5 as $uf) {
            echo $uf.PHP_EOL;
            $job->startDirect($uf, 'jftrf5pje');

            $time = '[' . date('Y-m-d H:i:s') . '] => ';
            $message = $time . $uf . PHP_EOL;
            echo $message;
            file_put_contents("./data/tribunais-log/{$uf}jfpjetrf5.txt", $message, FILE_APPEND | LOCK_EX);
        }*/

        /**
         * CRETA - TRF5
         */
        /*$ufsTrf5Creta = [
//            'AL'=>'https://jef.jfal.jus.br/cretainternetal/',//DONE
//            'CE'=>'https://wwws.jfce.jus.br/cretainternetce/',//DONE
            'PB'=>'https://jefvirtual.jfpb.jus.br/cretainternetpb/',//DONE
            'PE'=>'https://creta.jfpe.jus.br/cretainternetpe',//DONE
//            'RN'=>'https://juizado.jfrn.jus.br/cretainternetrn/',//DONE
//            'SE'=>'https://wwws.jfse.jus.br/cretainternetse/',//DONE
        ];
        foreach ($ufsTrf5Creta as $uf => $link) {
            echo $uf.PHP_EOL;
            $job->startDirect($uf, 'trf5creta', ['link'=>$link]);

            $time = '[' . date('Y-m-d H:i:s') . '] => ';
            $message = $time . $uf . PHP_EOL;
            echo $message;
            file_put_contents("./data/tribunais-log/{$uf}trf5creta.txt", $message, FILE_APPEND | LOCK_EX);
        }*/


        //@TODO BA - ALL DONE
//        $job->startDirect('BA', 'tjbafisico');//DONE
//        $job->startDirect('BA', 'trf1secao', ['uf' => 'BA']);//DONE
//        $job->startDirect('BA', 'tjba2graupje');//DONE
//        $job->startDirect('BA', 'trf1', ['uf' => 'BA', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE
//        $job->startDirect('BA', 'trf1', ['uf' => 'TRF1', 'codeTribunal' => '', 'tipoNro' => 'sem_mascara']);//DONE


        //@TODO RJ - CHECKING
//        $job->startDirect('RJ','tjrjfisicos');//DONE
//        $job->startDirect('RJ','tjrjfisicosFixCamara');//DONE
//        $job->startDirect('RJ','trf2');//DONE

        //@TODO ES - ALL DONE
//        $job->startDirect('ES','trf2');//DONE


        //RODRIDO
        $job = new \Tribunal\Service\JobRodrigo();


        //@TODO ESAJ RODRIGO - PASSAR RETIRANDO O DIGITO VERIFICADOR EX.: "-1"

        //@TODO BA - ALL DONE
//        $job->startDirect('BA', 'tjba1grauEsaj');//DONE
//        $job->startDirect('BA', 'tjba2grauEsaj');//DONE
//        $job->startDirect('BA', 'tjba1instanciatrf1pje');//DONE
//        $job->startDirect('BA', 'tjbaprojudi');//DONE

        //@TODO RJ - ALL DONE
//        $job->startDirect('RJ', 'jfrj');//@DONE

        //@TODO PI - ALL DONE
//        $job->startDirect('PI','tjpijusetjpi');//DONE
//        $job->startDirect('PI','tjpijus');//DONE
//        $job->startDirect('PI','projuditjpi');//DONE
//        $job->startDirect('PI','tjpiRecursal');//DONE
//        $job->startDirect('PI','tjpipje');//DONE
//        $job->startDirect('PI','tjpipje2inst');//DONE

        //@TODO MT - ALL DONE
//        $job->startDirect('MT','servicostjmtPrimeiraInstancia');//DONE MEU CÓD
//        $job->startDirect('MT','servicostjmtSegundaInstancia');//DONE MEU CÓD
//        $job->startDirect('MT','servicostjmtRecursal');//DONE MEU CÓD
//        $job->startDirect('MT','projuditjmt');//@DONE
//        $job->startDirect('MT','pjetjmt');//DONE
//        $job->startDirect('MT','pje2tjmt');//DONE

        //@TODO PB - ALL DONE
//        $job->startDirect('PB','ejustjpb');//DONE
//        $job->startDirect('PB','pjetjpb');//DONE
//        $job->startDirect('PB','webjfpb');//DONE
        $job->startDirect('PB','apptjpb');//@todo rodando...

        //@TODO MA - ALL DONE
//        $job->startDirect('MA','jurisconsulttjmaPrimeiroGrau');//DONE
//        $job->startDirect('MA','jurisconsulttjmaSegundoGrau');//DONE
//        $job->startDirect('MA','jurisconsulttjmaJuizadoEspecial');//DONE
//        $job->startDirect('MA','jurisconsulttjmaRecursal');//DONE
//        $job->startDirect('MA','projuditjma');//DONE
//        $job->startDirect('MA','pjetjma');//DONE
//        $job->startDirect('MA','pje2tjma');//DONE

        //@TODO ES - ALL DONE
//        $job->startDirect('ES','jfesjus');//DONE
//        $job->startDirect('ES','projuditjes');//@TODO RODANDO... - LENTO
//        $job->startDirect('ES','aplicativostjesPrimeiraInstanciaJusticaComum');//DONE
//        $job->startDirect('ES','aplicativostjesSegundaInstancia');//DONE
//        $job->startDirect('ES','aplicativostjesPrimeiraInstanciaJusticaEspecial');//DONE
//        $job->startDirect('ES','pjetjes');//DONE
//        $job->startDirect('ES','pje2tjes');//DONE

        die;
    }

    public function jobGeralAction()
    {
        ini_set('memory_limit', '-1');

        $tribunal = new Tribunal();
        $tribunais = $tribunal->fetchAll();

        $ufs = [];
        $tribunais->buffer();
        foreach ($tribunais as $item) {
            if (in_array($item->getUf(), $ufs)) continue;
            $ufs[] = $item->getUf();
        }

        $robo = new Robo();

        $job = new Job();
        $job->setAlterado(0);
        $processos = $job->filtrarObjeto();
        $robo->log(count($processos) . ' PROCESSOS JOB');

        $jobs = [];
        /** @var $processos \Tribunal\Service\Job[] */
        foreach ($processos as $processo) {
            if (!in_array($processo->getUf(), $ufs)) continue;
            $nro = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $jobs[$processo->getUf()][$nro] = $processo;
            $robo->log("{$processo->getUf()} - {$nro}");
        }

        $processosCapturados = [];
        $totais = [];
        /** @var $tribunais \Tribunal\Service\Tribunal[] */
        /** @var $processoCapturado \Tribunal\Service\Processo */
        foreach ($tribunais as $item) {
            if (!$item->getUf()) continue;

            $proc = new Processo();
            $proc->setIdTribunal($item->getId());

            foreach ($proc->filtrarObjeto() as $processoCapturado) {
                $nro = $robo->cnjNumberFormatter($processoCapturado->getProcesso());
                if (isset($jobs[$item->getUf()][$nro])) {
                    if (!isset($totais[$item->getUf()])) $totais[$item->getUf()] = 0;
                    $totais[$item->getUf()]++;
                    @$processosCapturados[$item->getUf()][$nro] = json_decode($processoCapturado->getDados(), true);
                }
            }
        }

        $countUf = [];

        /** @var $jobProcesso \Tribunal\Service\Job */
        foreach ($processosCapturados as $uf => $items) {
            foreach ($items as $nro => $dados) {
                if (!isset($jobs[$uf][$nro])) continue;
                $jobProcesso = $jobs[$uf][$nro];
                $dadosProcesso = $dados;

                $vara = '';

                if ($uf == 'MT') {
                    if (isset($dadosProcesso['Dados Gerais']['Lotação']) && $dadosProcesso['Dados Gerais']['Lotação'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Lotação']);
                        if ($cleanData) $vara = $cleanData;
                    }
                    // MT 2° INSTÂNCIA FÍSICO
                    if (isset($dadosProcesso['Dados Gerais']['Câmara']) && $dadosProcesso['Dados Gerais']['Câmara'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Câmara']);
                        if ($cleanData) $vara = $cleanData;
                    }
                }

                if ($uf == 'CE') {
                    if (isset($dadosProcesso['Dados Gerais']['Localização']) && $dadosProcesso['Dados Gerais']['Localização'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Localização']);
                        $cleanData = str_replace(' Remetido em', '', $cleanData);
                        if ($cleanData) $vara = $cleanData;
                    }
                }

                if ($uf == 'GO') {
                    //GO 1° INSTANCIA FISICO
                    if (isset($dadosProcesso['Principal']['Comarca/Escrivania:']) && $dadosProcesso['Principal']['Comarca/Escrivania:'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Principal']['Comarca/Escrivania:']);
                        if ($cleanData) $vara = $cleanData;
                    }
                    // GO 2° INSTANCIA FISICO
                    // NAO HA PROCESSOS
                }

                if ($uf == 'SP') {
                    //SP 1° INSTANCIA
                    if (isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                        if (preg_match('/; /', $dadosProcesso['DadosProcesso']['Distribuição'])) {
                            $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                            $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                            $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                        } else {
                            //SP 2° INSTANCIA
                            $vara = $robo->cleanString($dadosProcesso['DadosProcesso']['Distribuição'], [';']);
                        }
                    }
                }

                if ($uf == 'DF') {
                    // DF 1° INSTÂNCIA FÍSICO
                    if (isset($dadosProcesso['detalhamento']['Vara']) && $dadosProcesso['detalhamento']['Vara'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['detalhamento']['Vara']);
                        if ($cleanData) $vara = $cleanData;
                    }
                    // DF 2° INSTÂNCIA FÍSICO
                    if (isset($dadosProcesso['detalhamento']['Orgão']) && $dadosProcesso['detalhamento']['Orgão'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['detalhamento']['Orgão']);
                        if ($cleanData) $vara = $cleanData;
                    }
                }

                if ($uf == 'MG') {
                    //MG 1° E 2° INSTANCIA
                    if (isset($dadosProcesso['Dados']['Local']) && $dadosProcesso['Dados']['Local'] != '') {
                        $vara = $robo->cleanString($dadosProcesso['Dados']['Local'], [';']);
                    }
                }

                if ($uf == 'MA') {
                    // MA 1° INSTÂNCIA FÍSICO
                    if (isset($dadosProcesso['Distribuição']['Vara']) && $dadosProcesso['Distribuição']['Vara'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Distribuição']['Vara']);
                        if ($cleanData) $vara = $cleanData;
                    }
                    // MA 2° INSTÂNCIA FÍSICO
                    if (isset($dadosProcesso['Distribuíção']['Câmara']) && $dadosProcesso['Distribuíção']['Câmara'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Distribuíção']['Câmara']);
                        if ($cleanData) $vara = $cleanData;
                    }
                }

                if ($uf == 'BA') {
                    //BA 1° E 2° INSTANCIA
                    if (isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                        $arr = explode(';', $dadosProcesso['DadosProcesso']['Distribuição']);
                        if (isset($arr[1])) $vara = $robo->cleanString($arr[1], [';']);
                    }
                }

                if ($uf == 'AL') {
                    // AL 1° INSTÂNCIA ESAJ
                    if (isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                        $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                        $varaAux = (isset($distribuicao[1]) && strlen($distribuicao[1] > 3)) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                        $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                    }
                }

                if ($uf == 'RJ') {
                    //RJ 1° INSTANCIA
                    if (isset($dadosProcesso['Dados Gerais']) && count($dadosProcesso['Dados Gerais'])) {
                        foreach ($dadosProcesso['Dados Gerais'] as $nomeKey => $valor) {
                            if (preg_match('/Comarca /', $nomeKey)) {
                                if (preg_match('/; /', $valor)) {
                                    $arr = explode('; ', $valor);
                                    if (isset($arr[1])) $vara = $robo->cleanString($arr[1], [';']);
                                } else {
                                    $vara = $robo->cleanString($valor);
                                }
                            }
                        }
                    }
                    //RJ 2° INSTANCIA
                    if ((isset($dadosProcesso['Dados Gerais']['Órgão Julgador']) && $dadosProcesso['Dados Gerais']['Órgão Julgador'] != '') ||
                        isset($dadosProcesso['Dados Gerais']['Local']) && $dadosProcesso['Dados Gerais']['Local'] != '' ||
                        isset($dadosProcesso['Dados Gerais']['Destino']) && $dadosProcesso['Dados Gerais']['Destino'] != ''
                    ) {

                        if (isset($dadosProcesso['Dados Gerais']['Órgão Julgador'])) {
                            $vara = $robo->cleanString($dadosProcesso['Dados Gerais']['Órgão Julgador'], [';']);
                        }
                        if ($vara == '' && isset($dadosProcesso['Dados Gerais']['Local'])) {
                            $vara = $robo->cleanString($dadosProcesso['Dados Gerais']['Local'], [';']);
                        }
                        if ($vara == '' && isset($dadosProcesso['Dados Gerais']['Destino'])) {
                            $vara = $robo->cleanString($dadosProcesso['Dados Gerais']['Destino'], [';']);
                        }

                        if ($vara == 'DIVERSOS') $vara = '';
                    }
                }

                if ($uf == 'MS') {
                    // MS 1° INSTÂNCIA ESAJ & 2° INSTÂNCIA ESAJ
                    if (isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                        $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                        $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                        $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                    }
                }

                if ($uf == 'ES') {
                    // ES 1° INSTÂNCIA FÍSICO
                    if (isset($dadosProcesso['Dados Gerais']['Vara']) && $dadosProcesso['Dados Gerais']['Vara'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Vara']);
                        if ($cleanData) $vara = $cleanData;
                    }
                    // ES 2° INSTÂNCIA FÍSICO
                    if (isset($dadosProcesso['Dados Gerais']['Órgão Julgador']) && $dadosProcesso['Dados Gerais']['Órgão Julgador'] != '') {
                        $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Órgão Julgador']);
                        if ($cleanData) $vara = $cleanData;
                    }
                }

                if ($uf == 'RS') {
                    //RS 1° E 2° INSTANCIA
                    if (isset($dadosProcesso['Dados Principais']['Órgão Julgador']) && $dadosProcesso['Dados Principais']['Órgão Julgador'] != '') {
                        $vara = $robo->cleanString($dadosProcesso['Dados Principais']['Órgão Julgador'], [';']);
                    }
                }

                if ($vara != '') {
                    if (!isset($countUf[$uf])) $countUf[$uf] = 0;
                    $countUf[$uf]++;

                    $jobProcesso->setAlterado(1);
                    $jobProcesso->setJuizoVara($vara);
                    $jobProcesso->salvar(true);
                    echo "SAVED => {$jobProcesso->getId()} - {$uf} => {$nro} => {$vara}" . PHP_EOL;
                }

            }
        }

        print_r($countUf);

        die;
    }

    public function jobFixAction()
    {
        if (true) {
            $robo = new Robo();

            $job = new Job();
            $job->setAlterado(0);
//            $job->setUf('SP');
//            $job->setUf('RS');
//            $job->setUf('MG');
//            $job->setUf('RJ');
//            $job->setUf('BA');
//            $job->setUf('GO');
//            $job->setUf('CE');
//            $job->setUf('DF');
//            $job->setUf('MA');
//            $job->setUf('PA');
//            $job->setUf('ES');
//            $job->setUf('AL');
//            $job->setUf('MS');
            $job->setUf('MT');
            $processos = $job->filtrarObjeto();
            $robo->log(count($processos) . ' PROCESSOS JOB');

            $tribunal = new Tribunal();
            $tribunal->setUf($job->getUf());
//            $tribunal->setCallMethod('tjsp1instanciaesaj');
//            $tribunal->setCallMethod('tjsp2instanciaesaj');
//            $tribunal->setCallMethod('tjrs1instancia');
//            $tribunal->setCallMethod('tjrs2instancia');
//            $tribunal->setCallMethod('tjmg1instancia');
//            $tribunal->setCallMethod('tjmg2instancia');
//            $tribunal->setCallMethod('tjrj1instancia');
//            $tribunal->setCallMethod('tjba1instanciaesaj');
//            $tribunal->setCallMethod('tjba2instanciaesaj');
//            $tribunal->setCallMethod('tjgo1instanciafisico');
//            $tribunal->setCallMethod('tjgo2instanciafisico');
//            $tribunal->setCallMethod('tjcefisico');
//            $tribunal->setCallMethod('tjdf1instanciafisico');
//            $tribunal->setCallMethod('tjdf2instanciafisico');
//            $tribunal->setCallMethod('tjma1instancia');
//            $tribunal->setCallMethod('tjma2instancia');
//            $tribunal->setCallMethod('tjpa1instanciafisico');
//            $tribunal->setCallMethod('tjpa2instanciafisico');
//            $tribunal->setCallMethod('tjmt2instanciafisico');
//            $tribunal->setCallMethod('tjes1instancia');
//            $tribunal->setCallMethod('tjes2instancia');
//            $tribunal->setCallMethod('tjal1instanciaesaj');
//            $tribunal->setCallMethod('tjal2instanciaesaj');
//            $tribunal->setCallMethod('tjms1instanciaesaj');
//            $tribunal->setCallMethod('tjms2instanciaesaj');
//            $tribunal->setCallMethod('tjmt1instanciafisico');
            $tribunal->setCallMethod('tjmt2instanciafisico');
            $robo->log($tribunal->getCallMethod());
            /** @var $tribunalFound \Tribunal\Service\Tribunal */
            $tribunalFound = $tribunal->filtrarObjeto()->current();

            /** @var $processos \Tribunal\Service\Job[] */
            $processosJob = [];
            foreach ($processos as $item) {
                $nroP = $robo->cnjNumberFormatter($item->getNumProcJust());

                if (in_array($tribunal->getCallMethod(), ['tjdf1instanciafisico', 'tjdf2instanciafisico'])) {
//                    $nroP = (string) str_replace(['.',',','/','-'],'',$nroP);

                    if (strlen($nroP) < 25) {
                        $nroP = str_pad($nroP, 25, '0', STR_PAD_LEFT);
                    }
                }

                $processosJob[$nroP][] = $item;
            }
            $robo->log(count($processosJob) . ' PROCESSOS JOB TRATADOS');

            $processoService = new Processo();
            $processoService->setIdTribunal($tribunalFound->getId());
            $processosJ = $processoService->filtrarObjeto();
            $processosJson = [];
            /** @var $processosJ \Tribunal\Service\Processo[] */
            foreach ($processosJ as $item) {
                $processosJson[$robo->cnjNumberFormatter($item->getProcesso())] = $item;
            }
            $robo->log(count($processosJson) . ' PROCESSOS JSON TRATADOS');

            /** @var $aux \Tribunal\Service\Job */
            $index = 0;
            foreach ($processosJson as $processoNro => $item) {

                //TRATAR PARA GO
                if (in_array($tribunal->getCallMethod(), ['tjgo1instanciafisico'])) {
                    if (strpos($processoNro, '(') > -1) {
                        $arr = explode('(', $processoNro);
                        if (isset($arr[1])) {
                            $processoNro = str_replace(')', '', $arr[1]);
                        }
                    }
                }
                //TRATAR PARA CE
                if (in_array($tribunal->getCallMethod(), ['tjcefisico'])) {
                    if (strpos($processoNro, '/') > -1) {
                        $arr = explode('/', $processoNro);
                        if (isset($arr[0])) {
                            $processoNro = $arr[0];
                            if (strlen($processoNro) < 25) {
                                $processoNro = str_pad($processoNro, 25, '0', STR_PAD_LEFT);
//                                echo $processoNro.PHP_EOL;
                            }
                        }
                    }
                }
                //TRATAR PARA DF 1° FISICO
                if (in_array($tribunal->getCallMethod(), ['tjdf1instanciafisico'])) {
                    $processoNro = str_replace(['.', ',', '/', '-'], '', $processoNro);
                }


                if (!isset($processosJob[$processoNro])) continue;

                $aux = $processosJob[$processoNro];
                $dadosProcesso = json_decode($item->getDados(), true);
                if (is_array($aux)) {
                    foreach ($aux as $itemB) {

                        $vara = '';

                        //SP 1° INSTANCIA
                        if ($tribunal->getCallMethod() == 'tjsp1instanciaesaj' && isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                            $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                            $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                            $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                        }
                        //SP 2° INSTANCIA
                        if ($tribunal->getCallMethod() == 'tjsp2instanciaesaj' && isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                            $vara = $robo->cleanString($dadosProcesso['DadosProcesso']['Distribuição'], [';']);
                        }

                        //MG 1° E 2° INSTANCIA
                        if (in_array($tribunal->getCallMethod(), ['tjmg1instancia', 'tjmg2instancia']) && isset($dadosProcesso['Dados']['Local']) && $dadosProcesso['Dados']['Local'] != '') {
                            $vara = $robo->cleanString($dadosProcesso['Dados']['Local'], [';']);
                        }

                        //RS 1° E 2° INSTANCIA
                        if (in_array($tribunal->getCallMethod(), ['tjrs1instancia', 'tjrs2instancia']) && isset($dadosProcesso['Dados Principais']['Órgão Julgador']) && $dadosProcesso['Dados Principais']['Órgão Julgador'] != '') {
                            $vara = $robo->cleanString($dadosProcesso['Dados Principais']['Órgão Julgador'], [';']);
                        }

                        //RJ 1° INSTANCIA
                        if ($tribunal->getCallMethod() == 'tjrj1instancia' && isset($dadosProcesso['Dados Gerais']['Comarca da Capital']) && $dadosProcesso['Dados Gerais']['Comarca da Capital'] != '') {
                            $vara = $robo->cleanString($dadosProcesso['Dados Gerais']['Comarca da Capital'], [';']);
                        }
                        //RJ 2° INSTANCIA
                        if ($tribunal->getCallMethod() == 'tjrj2instancia' && (isset($dadosProcesso['Dados Gerais']['Órgão Julgador']) && $dadosProcesso['Dados Gerais']['Órgão Julgador'] != '') ||
                            isset($dadosProcesso['Dados Gerais']['Local']) && $dadosProcesso['Dados Gerais']['Local'] != '' ||
                            isset($dadosProcesso['Dados Gerais']['Destino']) && $dadosProcesso['Dados Gerais']['Destino'] != ''
                        ) {

                            if (isset($dadosProcesso['Dados Gerais']['Órgão Julgador'])) {
                                $vara = $robo->cleanString($dadosProcesso['Dados Gerais']['Órgão Julgador'], [';']);
                            }
                            if ($vara == '' && isset($dadosProcesso['Dados Gerais']['Local'])) {
                                $vara = $robo->cleanString($dadosProcesso['Dados Gerais']['Local'], [';']);
                            }
                            if ($vara == '' && isset($dadosProcesso['Dados Gerais']['Destino'])) {
                                $vara = $robo->cleanString($dadosProcesso['Dados Gerais']['Destino'], [';']);
                            }

                            if ($vara == 'DIVERSOS') $vara = '';

                        }

                        //BA 1° E 2° INSTANCIA
                        if ($tribunal->getCallMethod() == 'tjba1instanciaesaj' && isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                            $arr = explode(';', $dadosProcesso['DadosProcesso']['Distribuição']);
                            if (isset($arr[1])) $vara = $robo->cleanString($arr[1], [';']);
                        }

                        //GO 1° INSTANCIA FISICO
                        if ($tribunal->getCallMethod() == 'tjgo1instanciafisico' && isset($dadosProcesso['Principal']['Comarca/Escrivania:']) && $dadosProcesso['Principal']['Comarca/Escrivania:'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Principal']['Comarca/Escrivania:']);
                            if ($cleanData) $vara = $cleanData;
                        }
                        // GO 2° INSTANCIA FISICO
                        // NAO HA PROCESSOS
                        if ($tribunal->getCallMethod() == 'tjgo2instanciafisico' && (isset($dadosProcesso['Principal']['Secretaria']) && $dadosProcesso['Principal']['Secretaria'] != '') && isset($dadosProcesso['Principal']['Local']) && $dadosProcesso['Principal']['Local'] != '') {
                        }

                        // CE 1° E 2° INSTANCIA FISICO
                        if ($tribunal->getCallMethod() == 'tjcefisico' && isset($dadosProcesso['Dados Gerais']['Localização']) && $dadosProcesso['Dados Gerais']['Localização'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Localização']);
                            $cleanData = str_replace(' Remetido em', '', $cleanData);
                            if ($cleanData) $vara = $cleanData;
                        }

                        // DF 1° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjdf1instanciafisico' && isset($dadosProcesso['detalhamento']['Vara']) && $dadosProcesso['detalhamento']['Vara'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['detalhamento']['Vara']);
                            if ($cleanData) $vara = $cleanData;
                        }
                        // DF 2° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjdf2instanciafisico' && isset($dadosProcesso['detalhamento']['Orgão']) && $dadosProcesso['detalhamento']['Orgão'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['detalhamento']['Orgão']);
                            if ($cleanData) $vara = $cleanData;
                        }

                        // MA 1° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjma1instancia' && isset($dadosProcesso['Distribuição']['Vara']) && $dadosProcesso['Distribuição']['Vara'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Distribuição']['Vara']);
                            if ($cleanData) $vara = $cleanData;
                        }
                        // MA 2° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjma2instancia' && isset($dadosProcesso['Distribuíção']['Câmara']) && $dadosProcesso['Distribuíção']['Câmara'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Distribuíção']['Câmara']);
                            if ($cleanData) $vara = $cleanData;
                        }

                        // PA 1° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjpa1instanciafisico' && isset($dadosProcesso['Distribuíção']['Câmara']) && $dadosProcesso['Distribuíção']['Câmara'] != '') {
                            //NÃO TENHO PROCESSO
                        }

                        // ES 1° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjes1instancia' && isset($dadosProcesso['Dados Gerais']['Vara']) && $dadosProcesso['Dados Gerais']['Vara'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Vara']);
                            if ($cleanData) $vara = $cleanData;
                        }
                        // ES 2° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjes2instancia' && isset($dadosProcesso['Dados Gerais']['Órgão Julgador']) && $dadosProcesso['Dados Gerais']['Órgão Julgador'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Órgão Julgador']);
                            if ($cleanData) $vara = $cleanData;
                        }

                        // AL 1° INSTÂNCIA ESAJ
                        if ($tribunal->getCallMethod() == 'tjal1instanciaesaj' && isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                            $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                            $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                            $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                        }
                        // AL 1° INSTÂNCIA ESAJ
                        if ($tribunal->getCallMethod() == 'tjal2instanciaesaj' && isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                            $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                            $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                            $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                        }

                        // MS 1° INSTÂNCIA ESAJ
                        if ($tribunal->getCallMethod() == 'tjms1instanciaesaj' && isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                            $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                            $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                            $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                        }
                        // MS 2° INSTÂNCIA ESAJ
                        if ($tribunal->getCallMethod() == 'tjms2instanciaesaj' && isset($dadosProcesso['DadosProcesso']['Distribuição']) && $dadosProcesso['DadosProcesso']['Distribuição'] != '') {
                            $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
                            $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
                            $vara = $robo->cleanString(str_replace(';', '', $varaAux), [';']);
                        }

                        // MT 1° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjmt1instanciafisico' && isset($dadosProcesso['Dados Gerais']['Lotação']) && $dadosProcesso['Dados Gerais']['Lotação'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Lotação']);
                            if ($cleanData) $vara = $cleanData;
                        }
                        // MT 2° INSTÂNCIA FÍSICO
                        if ($tribunal->getCallMethod() == 'tjmt2instanciafisico' && isset($dadosProcesso['Dados Gerais']['Câmara']) && $dadosProcesso['Dados Gerais']['Câmara'] != '') {
                            $cleanData = $robo->cleanString($dadosProcesso['Dados Gerais']['Câmara']);
                            if ($cleanData) $vara = $cleanData;
                        }

                        if (strlen($vara) >= 5) {
                            $index++;
                            $itemB->setJuizoVara($vara);
                            $itemB->setAlterado(1);
                            $itemB->salvar(true);
                            $robo->log("{$processoNro} => {$vara}");
                        }
                    }
                }
            }

            $robo->log("{$index} PROCESSOS");

            die;


            //REVERSE ARRAY
//            $processosTratados = [];
//            foreach ($processos as $processo) {
//                $processosTratados[] = $processo;
//            }
//            $processos = array_reverse($processosTratados);


            $total = count($processos);
            $robo->log(count($processos) . ' PROCESSOS');
            /** @var $processos \Tribunal\Service\Job[] */

            $tribunal = new Tribunal();
            $tribunal->setUf($job->getUf());
            $tribunal->setCallMethod('tjmg2instancia');
            $robo->log($tribunal->getCallMethod());
            /** @var $tribunalFound \Tribunal\Service\Tribunal */
            $tribunalFound = $tribunal->filtrarObjeto()->current();

            $index = 1;
            foreach ($processos as $processo) {

                $robo->log($robo->percentage($total, $index) . ' => ' . $processo->getNumProcJust());
                $index++;

                $nroProcesso = $robo->extractProcessNumber($processo->getNumProcJust(), false);

                $processoUnificado = true;
                if (!$nroProcesso) {
                    $processoUnificado = false;
                    $nroProcesso = $processo->getNumProcJust();
                }

                if (!$processoUnificado) continue;
//                if (!preg_match('/.8.26./', $nroProcesso)) continue;
                if (!preg_match('/.8.13./', $nroProcesso)) continue;

                $processoService = new Processo();
                $processoService->setIdTribunal($tribunalFound->getId());
                $processoService->setProcesso($processo->getNumProcJust());

                /** @var $processoFound \Tribunal\Service\Processo */
                $processoFound = $processoService->filtrarObjeto()->current();

                if (!$processoFound) continue;

                $dadosProcesso = json_decode($processoFound->getDados(), true);

                if (isset($dadosProcesso['Dados']['Local']) && $dadosProcesso['Dados']['Local'] != '') {

                    //SP 1° INSTANCIA
//                    $distribuicao = explode('; ', $dadosProcesso['DadosProcesso']['Distribuição']);
//                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['DadosProcesso']['Distribuição'];
//                    $vara = rtrim(str_replace(';', '', $varaAux));
                    //SP 2° INSTANCIA
//                    $vara = rtrim($dadosProcesso['DadosProcesso']['Distribuição']);

                    //RS 1° INSTANCIA
                    $vara = rtrim($dadosProcesso['Dados']['Local']);
                }

            }
        }

        if (false) {
            $sql = "SELECT UF, NUM_INT_PROCESSO, NUM_PROC_JUST, COUNT(*) AS TOTAL FROM TRIBUNAL_JOB
                    WHERE NUM_PROC_JUST IS NOT NULL
                    GROUP BY UF, NUM_PROC_JUST
                    ORDER BY TOTAL DESC, UF ASC";

            $con = new Conexao();
            $data = $con->listarSql($sql);

            echo "UF|NUM_PROC_JUST|TOTAL" . PHP_EOL;
            foreach ($data as $item) {
                if ($item->TOTAL < 2) continue;
                echo "{$item->UF}|{$item->NUM_PROC_JUST}|{$item->TOTAL}" . PHP_EOL;
            }
        }

        if (false) {
            $robo = new Robo();

//            $sql = "SELECT * FROM TRIBUNAL_JOB WHERE NUM_PROC_JUST IS NOT NULL";
            $sql = "SELECT UF, COUNT(*) AS TOTAL FROM TRIBUNAL_JOB
                    WHERE NUM_PROC_JUST IS NOT NULL
                    GROUP BY UF
                    ORDER BY TOTAL DESC";

            $con = new Conexao();
            $data = $con->listarSql($sql);

            $tratados = '';
            foreach ($data as $item) {
//                $processo = $robo->extractProcessNumber($item->NUM_INT_PROCESSO, false);
//                if (!$processo) {
//                    $tratados[] = $item->NUM_INT_PROCESSO;
//                }

                $tratados .= "$item->UF => $item->TOTAL, " . PHP_EOL;
            }

//            echo implode(', ', $tratados) . PHP_EOL;
            echo $tratados . PHP_EOL;
        }

        if (false) {
            $jsonArr = json_decode(file_get_contents('./data/relatorio-geral/RELATORIO_GERAL.json'), true);
            $total = count($jsonArr['Plan1']);
            $index = 1;
            $robo = new Robo();
            foreach ($jsonArr['Plan1'] as $processo) {
                echo "{$robo->percentage($total,$index)}% > {$index} DE {$total} => {$processo['NOM_AUTOR']} - {$processo['NUM_PROC_JUST']}" . PHP_EOL;
                $job = new Job();
                $job->setDtInclusao($processo['DTH_INCLUSAO']);
                $job->setNumIntProcesso($processo['NUM_INT_PROCESSO']);
                $job->setDesComarca($processo['DES_COMARCA']);
                $job->setNomAutor($processo['NOM_AUTOR']);
                if ($processo['CPF_AUTOR']) $job->setCpfAutor($processo['CPF_AUTOR']);
                $job->setNomReu($processo['NOM_REU']);
                $job->setNomJuizo($processo['NOM_JUIZO']);
                $job->setUf($processo['UF']);
                if ($processo['NUM_PROC_JUST']) $job->setNumProcJust($processo['NUM_PROC_JUST']);
                $job->salvar(true);
                $index++;
            }
        }

        echo 'DONE!' . PHP_EOL;
        die;
    }

    public function testAction()
    {

        if (false) {

            $fileJson = json_decode(file_get_contents('./data/relatorio-geral/RELATORIO_GERAL.json'), true);

            foreach ($fileJson['Plan1'] as $itemJ) {
                if (preg_match('/E/', $itemJ['NUM_PROC_JUST'])) {
                    echo $itemJ['NUM_PROC_JUST'] . PHP_EOL;
                }
            }

            die;

            $robo = new Robo();
            $job = new Job();
            $job->setAlterado(0);
            $tratados = [];
            /** @var $item \Tribunal\Service\Job */
            foreach ($job->filtrarObjeto() as $item) {
                if (preg_match('/E/', $item->getNumProcJust())) {
                    $num = sprintf('%f', $item->getNumProcJust());
                    $num = str_replace('.000000', '', $num);
                    if ($num == 0) continue;
                    $cnj = $robo->cnjNumberFormatter($num);
                    $item->setNumProcJust($cnj);
                    $item->setObs('NRO AJUSTADO VIA APLICAÇÃO');
                    $item->salvar(true);
                    $robo->log($cnj);
                }
            }

            die;
        }

        if (false) {
            /**
             * ELIMINA OS REGISTROS DUPLICADOS PARA O TRIBUNAL DETERMINADO
             */
            $service = new Processo();
            $service->setIdTribunal('692302e1-d78c-4273-ba06-2424419fc5fe');//Maranhão 1° Inst
            $service->setProcessado(1);
            /** @var $item \Tribunal\Service\Processo */
            $pool = [];
            $countRep = 0;
            $diff = 0;
            $deletes = 0;
            foreach ($service->filtrarObjeto() as $item) {
                if (in_array($item->getProcesso(), array_keys($pool))) {
                    $countRep++;
                    if (strlen($item->getDados()) > strlen($pool[$item->getProcesso()]->getDados())) {
                        $diff++;
                        $pool[$item->getProcesso()]->excluir(true);
                        $deletes++;
                        $pool[$item->getProcesso()] = $item;
                        continue;
                    }
                    $deletes++;
                    $item->excluir(true);
                    continue;
                }
                $pool[$item->getProcesso()] = $item;
            }
            debug([$countRep, $diff, $deletes]);
        }

        if (false) {
            /**
             * ARRUMA O NÚMERO DE PROCESSO INTERNO PARA O TRIBUNAL DETERMINADO
             */
            $service = new Processo();
            $service->setIdTribunal('692302e1-d78c-4273-ba06-2424419fc5fe');//Maranhão 1° Inst
            $service->setProcessado(1);
            /** @var $item \Tribunal\Service\Processo */
            foreach ($service->filtrarObjeto() as $item) {
                $dados = json_decode($item->getDados(), true);
                if (!isset($dados['Dados Gerais']['Número'])) continue;
                $item->setProcessoInterno(preg_replace('/[^0-9.]+/', '', $dados['Dados Gerais']['Número']));
                $item->salvar(true);
            }
        }

        die;
    }

    public function jobRodrigoAction()
    {
        $job = new \Tribunal\Service\JobRodrigo();
//        $job->startDirect('PR', 'tjprfederal'); // rodar
//        $job->startDirect('PR', 'projuditjpr'); // @TODO rodar apenas segunda instancia e processo começa com zero
//        $job->startDirect('PR', 'justicaweb');
//        $job->startDirect('PR', 'portaltjprSegundaInstancia'); // @TODO fazer pq está incompleto
//        $job->startDirect('PR','trf4'); // rodar
//        $job->startDirect('PR', 'tjprTurmasRecursais');
//        $job->startDirect('PR', 'tjprsnu');
//        $job->startDirect('RJ', 'trf2');
//        $job->startDirect('RJ', 'jfrj');
//        $job->startDirect('RJ', 'juizadoEspecialRj');

//        $job->startDirect('BA', 'trf1', ['uf' => 'TRF1', 'codeTribunal' => '.4.01.33', 'tipoNro' => 'cnj']); // 93 registros
//        $job->startDirect('BA', 'trf1', ['uf' => 'TRF1', 'codeTribunal' => '.33.', 'tipoNro' => 'cjf']); // 0 registros
//        $job->startDirect('BA', 'trf1', ['uf' => 'TRF1', 'codeTribunal' => '', 'tipoNro' => 'antigo']); // 0 registros
//        $job->startDirect('BA', 'trf1', ['uf' => 'TRF1', 'codeTribunal' => '40133', 'tipoNro' => 'sem_mascara']); // 68 registros
//        $job->startDirect('BA', 'trf1', ['uf' => 'BA', 'codeTribunal' => '.4.01.33', 'tipoNro' => 'cnj']); // 169 registros
//        $job->startDirect('BA', 'trf1', ['uf' => 'BA', 'codeTribunal' => '.33.', 'tipoNro' => 'cjf']); // 0 registros
//        $job->startDirect('BA', 'trf1', ['uf' => 'BA', 'codeTribunal' => '', 'tipoNro' => 'antigo']); // 0 registros
//        $job->startDirect('BA', 'trf1', ['uf' => 'BA', 'codeTribunal' => '40133', 'tipoNro' => 'sem_mascara']); // 122 registros
        $job->startDirect('BA', 'tjba1instanciatrf1pje'); // @TODO esta com problema no captcha
//        $job->startDirect('BA', 'tjba2grauEsaj');
//        $job->startDirect('BA', 'tjba1grauEsaj');
        die;
    }

    public function fixAction()
    {
        /**
         * FIX - REMOVER INFO DE DATA DO CAMPO JUIZO_VARA
         */
        $job = new Job();
        $robo = new Robo();

        $con = new Conexao();
        $sql = "SELECT * FROM TRIBUNAL_JOB WHERE JUIZO_VARA LIKE BINARY '% às %'";
        $data = $con->listarSql($sql);

        $abstract = new AbstractEstruturaService();
        $tratados = $abstract->populateService($job,$data);

        /** @var $tratados \Tribunal\Service\Job[] */
        foreach ($tratados as $item) {
            $aux = $item->getJuizoVara();
            // 1º Juizado Especial Cível e das Relações de Consumo da Capital - Turno Manhã - 07:00h às 13:00h
            $qtd = preg_match_all('( - Turno (.*) - [0-9]{2}[:][0-9]{2}[h] às [0-9]{2}[:][0-9]{2}[h])', $aux, $match);
            $vara = ($qtd > 0) ? str_replace($match[0], '', $aux) : $aux;
            $item->setJuizoVara($vara);
            $item->salvar(true);
            $robo->log($vara);
        }
        die;

        /**
         * FIX - CAMPO JUIZO_VARA COM VALORES ERRADOS - TRF1
         * * RELATOR *
         */
        $robo = new Robo();
        $job = new Job();
        $job->setAlterado(1);
        $processos = $job->filtrarObjeto();
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $processosTratados = [];
        foreach ($processos as $item) {
            //TRATAR => RELATOR TRF1
            /*if (preg_match('/relator/i',$item->getJuizoVara())) {
                $processosTratados[] = $item;
            }*/

            //TRATAR => TURMA TRF1
            /*if (preg_match('/TURMA/',$item->getJuizoVara()) && !preg_match('/trf1/',$item->getOrigem())) {
                $processosTratados[] = $item;
            }*/

            //TRATAR => TURMA DIFERENTE DA ORIGEM TRF1
//            if ($item->getUf() != 'RJ') continue; //DONE
//            if ($item->getUf() != 'SP') continue; //DONE
//            if ($item->getUf() != 'MS') continue; //DONE
//            if ($item->getUf() != 'PR') continue;//DONE
//            if ($item->getUf() != 'RN') continue;//DONE
//            if ($item->getUf() != 'SC') continue;//DONE
//            if ($item->getUf() != 'SE') continue;//DONE
//            if ($item->getUf() != 'GO') continue;//DONE
//            if ($item->getUf() != 'GO') continue;//DONE
//            if ($item->getUf() != 'PB') continue;//DONE
//            if ($item->getUf() != 'PE') continue;//DONE
            /*if (preg_match('/TURMA/i', $item->getJuizoVara()) && !preg_match('/processual.trf1.jus.br/', $item->getOrigem())) {
                $processosTratados[] = $item;
            }*/

            //TRATAR => CAMARA CIVEL
//            if ($item->getUf() != 'AL') continue; //DONE
//            if ($item->getUf() != 'BA') continue; //DONE
//            if ($item->getUf() != 'RJ') continue; //DONE
//            if ($item->getUf() != 'MA') continue; //DONE
//            if ($item->getUf() != 'MG') continue; //DONE
//            if ($item->getUf() != 'MS') continue; //DONE
//            if ($item->getUf() != 'MT') continue; //DONE
//            if ($item->getUf() != 'RN') continue;//DONE
//            if ($item->getUf() != 'SC') continue;//DONE
//            if ($item->getUf() != 'SE') continue;//DONE
//            if ($item->getUf() != 'GO') continue;//DONE
//            if ($item->getUf() != 'PB') continue;//DONE
//            if ($item->getUf() != 'PE') continue;//DONE
//            if ($item->getUf() != 'PI') continue;//DONE
//            if ($item->getUf() != 'PR') continue;//@TODO RODANDO...
//            if ($item->getUf() != 'SC') continue;//@TODO RODANDO...
            if ($item->getUf() != 'RS') continue;//@TODO RODANDO...
//            if ($item->getUf() != 'SP') continue;//DONE
            if (
                preg_match('/GAB./i', $item->getJuizoVara()) ||
                preg_match('/CÂMARA CRIMINAL/', $item->getJuizoVara()) ||
                preg_match('/CÂMARA ESPECIAL/', $item->getJuizoVara()) ||
                preg_match('/CÂMARA DE DIREITO/', $item->getJuizoVara()) ||
                preg_match('/Câmara Especializada/', $item->getJuizoVara()) ||
                preg_match('/Câmara de Direito/', $item->getJuizoVara()) ||
                preg_match('/Câmara de Enfrentamento/', $item->getJuizoVara()) ||
                preg_match('/Câmara Cível/', $item->getJuizoVara()) ||
                preg_match('/CÂMARA CÍVEL/i', $item->getJuizoVara()) ||
                preg_match('/CAMARA CIVEL/i', $item->getJuizoVara())
            ) {
                $processosTratados[] = $item;
            }
        }
        $total = count($processosTratados);

        $robo->log("$total PROCESSOS ENCONTRADOS");
        foreach ($processosTratados as $processosTratado) {
            $robo->log($processosTratado->getJuizoVara());
        }

        //AL
//        $job->tjalesaj1grau($processosTratados);

        //BA
//        $job = new JobRodrigo();
//        $job->tjba1grauEsaj()
        /*$driver = $robo->startDriver();
        foreach ($processosTratados as $processosTratado) {
            $driver->get($processosTratado->getOrigem());
            $rows = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[3]/tbody/tr'));
            $dadosProcesso = $robo->extractVerticalTable($rows, 2);
            if (!isset($dadosProcesso['Origem']) || $dadosProcesso['Origem'] == '') {
                $tableRows = '/html/body/table[4]/tbody/tr/td/table[2]/tbody/tr';
                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($tableRows));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);
            }
            $vara = '';
            if (isset($dadosProcesso['Origem']) && $dadosProcesso['Origem'] != '') {
                $arr = explode(' / ',$dadosProcesso['Origem']);
                $vara = $robo->cleanString(end($arr),['; ',';']);
                $vara .= ' - ' . $arr[1];
                $vara = $robo->cleanString($vara);
                $robo->addVara($processosTratado,$vara,false,$driver->getCurrentURL());
            }
        }*/

        //RJ
//        $job->tjrjfisicos($processosTratados);
//        $job->trf2($processosTratados);

        //PR
//        krsort($processosTratados);//RODAR INVERTIDO!
//        $job->projuditjpr($processosTratados, ['instancia' => '2']);
//        $job->portaltjprSegundaInstancia($processosTratados);
//        $job->trf4($processosTratados,['uf' => 'TRF', 'codeTribunal' => '404', 'puppet' => '50036817920174047005', 'tipoNro' => 'sem_mascara']);
//        $job->trf4($processosTratados,['uf' => 'PR', 'codeTribunal' => '404', 'puppet' => '50036817920174047005', 'tipoNro' => 'sem_mascara']);

        //RN
//        $job->jftrf5pje($processosTratados);//DONE
//        $job->tjrnpje1grau($processosTratados);//DONE

        //SC
//        $job->tjsc1instanciaesaj($processosTratados);
//        $job->tjsc2instanciaesaj($processosTratados);
//        $job->trf4($processosTratados, ['uf' => 'TRF', 'codeTribunal' => '', 'puppet' => '03858136520034040000', 'tipoNro' => 'sem_mascara']);
//        $job->trf4($processosTratados, ['uf' => 'SC', 'codeTribunal' => '', 'puppet' => '50000266320174047211', 'tipoNro' => 'sem_mascara']);

        //RIO GRANDE DO SUL - ALL DONE!!!
        $job->trf4($processosTratados, ['uf' => 'RS', 'codeTribunal' => '', 'puppet' => '50031109420164047118', 'tipoNro' => 'sem_mascara']);

        //GO
//        $job->tjgoprojudi($processosTratados,['tipoNro' => 'cnj']);

        //MA
//        $job = new JobRodrigo();
//        $job->jurisconsulttjmaPrimeiroGrau($processosTratados);

        //MG
        /*$driver = $robo->startDriver();
        foreach ($processosTratados as $processosTratado) {
            $nroProcesso = $robo->onlyDigits($robo->cnjNumberFormatter($processosTratado->getNumProcJust()));
            $driver->get(str_replace('[NRO_PROCESSO]',$nroProcesso,"http://www4.tjmg.jus.br/juridico/sf/proc_resultado2.jsp?lst_processos=&listaProcessos=[NRO_PROCESSO]"));
            $driver->findElement(WebDriverBy::xpath("//a[text()='Dados Completos']"))->click();
            $tables = $driver->findElements(WebDriverBy::cssSelector('table.corpo'));
            $vara = '';
            foreach ($tables as $table) {
                if (!preg_match('/Vara Origem/',$table->getText())) continue;
                $cells = $table->findElements(WebDriverBy::tagName('td'));
                foreach ($cells as $cell) {
                    if (!preg_match('/Vara Origem/',$cell->getText())) continue;
                    $vara = trim($robo->cleanString($cell->getText(),['Vara Origem:']));
                    break;
                }
                if ($vara) break;
            }
            $robo->addVara($processosTratado,$vara,false,$driver->getCurrentURL());
        }*/

        //MT
//        $job->tjmt1instanciafisico($processosTratados);

        //SE
//        $job->jftrf5pje($processosTratados);//DONE

        //PB
//        $job = new JobRodrigo();
//        $job->apptjpb($processosTratados);//DONE

        //PE
//        $job->jftrf5pje($processosTratados);
        /*$driver = $robo->startDriver();
        foreach ($processosTratados as $processosTratado) {
            $driver->get($processosTratado->getOrigem());
            $rowsElem = '/html/body/center/form/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td/table/tbody/tr';
            $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($rowsElem)));
            $rowsElems = $driver->findElements(WebDriverBy::xpath($rowsElem));
            $trash = array_shift($rowsElems);//RETIRA A PRIMEIRA LINHA
            $dados = $robo->extractVerticalTable($rowsElems, 2);
            $vara = '';
            if (isset($dados['Vara']) && $dados['Vara'] != '') {
                $vara = $robo->cleanString($dados['Vara'], ['; ', ';']);
            }
            $robo->addVara($processosTratado,$vara,false,$driver->getCurrentURL());
        }*/

        //PI
        /*$driver = $robo->startDriver();
        foreach ($processosTratados as $processosTratado) {
            $driver->get($processosTratado->getOrigem());
            $rowsElem = '//*[@id="relacionados"]/div/div/div[1]/dl/dd/ul/li/p/small';
            $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($rowsElem)));
            $varaAux = explode("\n",$driver->findElement(WebDriverBy::xpath($rowsElem))->getText());
            $robo->addVara($processosTratado,$varaAux[1],false,$driver->getCurrentURL());
        }*/

        //SP
//        $job->tjsp1instanciaesaj($processosTratados);
//        $job->tjsp2instanciaesaj($processosTratados);


        die;

        $index = 1;
        $driver = $robo->startDriver();
        $driver->get($robo->host);
        foreach ($processosTratados as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            /**
             * SÃO PAULO E MATO GROSSO DO SUL - FIX - DONE
             */
            /*$varaArr = explode(' - ',$processo->getJuizoVara());
            if (preg_match('/VARA/i',$varaArr[0])) {
                $processo->setJuizoVara($varaArr[0]);
                $processo->salvar(true);
                $robo->log($varaArr[0]);
            }*/

            /**
             * TRF1 FIX - DONE
             */
//            $base = 'https://processual.trf1.jus.br/consultaProcessual/processo.php?proc=[PROCESSO]&secao=[SESSAO]&mostrarBaixados=S';
//            $sessao = '';
//            if (strlen($processo->getNumProcJust()) > 20) {
//                $sessao = substr($processo->getNumProcJust(),-4,4);
//            } else {
//                $sessao = str_replace('.','',substr($processo->getNumProcJust(),5,5));
//            }
//            $robo->log(str_replace(['[PROCESSO]','[SESSAO]'],[$processo->getNumProcJust(),$sessao],$base));
//            $driver->navigate()->to(str_replace(['[PROCESSO]','[SESSAO]'],[$processo->getNumProcJust(),$sessao],$base));
//
//            //OLD WAY
////            $driver->navigate()->to($processo->getOrigem());
//
//            $pageOriginario = false;
//            inicio:
//
//            try {
//                $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="aba-processo"]/table/tbody/tr')));
//
//                $tableRows = $driver->findElements(WebDriverBy::xpath('//*[@id="aba-processo"]/table/tbody/tr'));
//
//                /**
//                 * CHECA SE O CAMPO VARA TEM VALOR DIFERENTE DO NÃO DESEJADO.
//                 */
//                $continue = false;
//                foreach ($tableRows as $row) {
//                    $key = $row->findElement(WebDriverBy::cssSelector("th"))->getText();
//                    if (preg_match('/Vara/', $key)) {
//                        $vara = $robo->cleanString($row->findElement(WebDriverBy::cssSelector("td"))->getText());
//                        if ($vara != '' && !preg_match('/relator/i',$vara) && !preg_match('/turma/i',$vara)) {
//                            $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
//                            $continue = true;
//                            break;
//                        }
//                    }
//                }
//                if ($continue) continue;
//
//                if (!$pageOriginario) {
//                    $wholeTable = $driver->findElement(WebDriverBy::xpath('//*[@id="aba-processo"]/table'))->getText();
//                    if (preg_match('/Processo Originário/',$wholeTable)) {
//                        foreach ($tableRows as $row) {
//                            $key = $row->findElement(WebDriverBy::tagName("th"))->getText();
//                            if (preg_match('/Processo Originário/', $key)) {
//                                $link = $row->findElement(WebDriverBy::tagName("a"))->getAttribute('href');
//                                $driver->navigate()->to($link);
//                                $pageOriginario = true;
//                                goto inicio;
//                                break;
//                            }
//                        }
//                    }
//                }
//
//                if ($pageOriginario) {
//                    $pageOriginario = false;
//                    $vara = '';
//                    foreach ($tableRows as $row) {
//                        $key = $row->findElement(WebDriverBy::cssSelector("th"))->getText();
//                        if (preg_match('/Vara/', $key)) {
//                            $vara = $robo->cleanString($row->findElement(WebDriverBy::cssSelector("td"))->getText());
//                            if ($vara != '') {
//                                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
//                                break;
//                            }
//                        }
//                        /*if (preg_match('/Órgão Julgador/', $key)) {
//                            $vara = $robo->cleanString($row->findElement(WebDriverBy::cssSelector("td"))->getText());
//                            if ($vara != '') {
//                                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
//                                break;
//                            }
//                        }*/
//                    }
//                }
//                $pageOriginario = false;
//
//            } catch (\Exception $e) {
//                $robo->log('PROCESSO NÃO ENCONTRADO');
//                $pageOriginario = false;
//            }
        }


        /**
         * FIX - CAMPO JUIZO_VARA COM VALORES ERRADOS
         * * 0, vazio /0 *
         */
//        $robo = new Robo();
//        $job = new Job();
//        $job->setAlterado(0);
//        $processos = $job->filtrarObjeto();
//        /** @var $processos \Tribunal\Service\Job[] */
//        /** @var $processosTratados \Tribunal\Service\Job[] */
//        $processosTratados = [];
//        foreach ($processos as $item) {
//            if ($item->getObs() == 'SEM_JUIZO_VARA' && $item->getUf() == 'RJ') {
//                $processosTratados[] = $item;
//            }
//        }
//        $total = count($processosTratados);
//
//        $robo->log("$total PROCESSOS ENCONTRADOS");
//
//        //@TODO - PR
////        $job->trf4($processosTratados,['uf' => 'PR', 'codeTribunal' => '', 'puppet' => '00023116220084047104', 'tipoNro' => 'sem_mascara']);//DONE
////        $job->trf4($processosTratados,['uf' => 'TRF', 'codeTribunal' => '', 'puppet' => '00023116220084047104', 'tipoNro' => 'sem_mascara']);//DONE
////        $job->projuditjpr($processosTratados,['instancia' => '1']);//DONE
////        $job->projuditjpr($processosTratados,['instancia' => '2']);//DONE
////        $job->startDirect('PR','tjprsnu');//MUITO LENTO E COM ERROS DE CAPTCHA
////        $job->justicaweb($processosTratados);//DONE
////        $job->tjprTurmasRecursais($processosTratados);//DONE
////        $job->portaltjprSegundaInstancia($processosTratados);//DONE
////        $job->portaltjprPrimeiraInstancia($processosTratados);//DONE
//        /*Foi feito manualmente a extração da vara
//        Foram encontrados dois processos
//        O padrão começa com 5 e possui o identificador 8.16
//       $job->startDirect('PR','tjpjetjpr');*/ //DONE
//
//        //@TODO - RJ
////        $job->tjrjfisicos($processosTratados);//DONE
////        $job->trf2($processosTratados);//DONE
//        $jobRodrigo = new \Tribunal\Service\JobRodrigo();
////        $jobRodrigo->jfrj($processosTratados);//DONE
////        $jobRodrigo->juizadoEspecialRj($processosTratados);//metodo substituido por tjrjfisicos.
//
//        die;
    }

}
