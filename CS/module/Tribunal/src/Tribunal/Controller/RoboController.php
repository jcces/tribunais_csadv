<?php

namespace Tribunal\Controller;

use Estrutura\Controller\AbstractCrudController;
use Tribunal\Service\Processo;
use Tribunal\Service\Robo;
use Tribunal\Service\Tribunal;
use Zend\View\Model\ViewModel;

class RoboController extends AbstractCrudController
{

    public function indexAction()
    {
//        $file = './data/test-ocr/captcha2.png';
//        $fileAlter = './data/test-ocr/captcha2CON.png';
//        $image = imagecreatefrompng($file);
//        imagefilter($image, IMG_FILTER_GRAYSCALE);
//        imagefilter($image, IMG_FILTER_CONTRAST, -50);
//        imagepng($image, $fileAlter);
//        imagedestroy($image);
//        echo (new TesseractOCR($fileAlter))
//            ->run();
//        die;
        return new ViewModel(['tribunais' => []]);
    }

    public function logsAction()
    {
        $dir = './data/tribunais-log/';
        $files = scandir($dir);
        $tratados = [];
        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) continue;

            $tratados[$file] = file_get_contents($dir . $file);
        }

        return new ViewModel(['files' => $tratados]);
    }

    public function tribunaisAction()
    {
        try {
            $tribunal = $this->params('id');
            $data = '';
            $fileNameFriendly = '';
            if ($tribunal) {
                $file = './data/tribunais-data/' . $tribunal;
                if (!file_exists($file)) throw new \Exception('Tribunal não está disponível.');

                $fileNameFriendly = ucfirst(str_replace('.json', '', $tribunal));
                $fileNameFriendly = str_replace(['inst', 'Inst', 'Tj'], '', $fileNameFriendly);
                $uf = substr($fileNameFriendly, 0, 2);
                $inst = substr($fileNameFriendly, 2, 1);
                $fileNameFriendly = strtoupper($uf) . ' ' . $inst . '° Instância';

                $data = file_get_contents($file);
            }

            $dir = './data/tribunais-data/';
            $files = scandir($dir);
            $tratados = [];
            foreach ($files as $file) {
                if (in_array($file, ['.', '..', 'tjrj1inst.json', 'tjrj2inst.json'])) continue;
                $tratados[] = $file;
            }

            $description = [
                'tjdf1inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjdf2inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjgo1inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjgo2inst.json' => [
                    'pesquisa' => 'CAIXA SEGU',
                ],
                'tjmg1inst.json' => [
                    'pesquisa' => 'CAIXA SEGURADORA',
                    'total' => '858 apenas em BH',
                    'obs' => '1 - O argumento de pesquisa CAIXA SEGUR não obteve resultado. 2 - Uma nova varredura será realizada para coletar todos os processos. Por agora temos uma amostra de alguns processos.',
                ],
                'tjmg2inst.json' => [
                    'pesquisa' => 'CAIXA SEGURADORA',
                    'total' => '2480',
                    'obs' => '1 - O argumento de pesquisa CAIXA SEGUR não obteve resultado. 2 - Uma nova varredura será realizada para coletar todos os processos. Por agora temos uma amostra de alguns processos.',
                ],
                'tjsp1inst.json' => [
                    'pesquisa' => 'CAIXA SEGUROS',
                    'obs' => 'Os argumentos de pesquisa CAIXA SEGUR e CAIXA SEGURADORA não obtiveram resultados.',
                ],
                'tjsp2inst.json' => [
                    'pesquisa' => 'CAIXA SEGUROS',
                    'obs' => 'Os argumentos de pesquisa CAIXA SEGUR e CAIXA SEGURADORA não obtiveram resultados.',
                ],
            ];

            $retorno = new ViewModel(['files' => $tratados, 'tribunal' => $fileNameFriendly, 'tribunalSelected' => $tribunal, 'data' => $data, 'description' => $description]);
        } catch (\Exception $e) {
            $retorno = new ViewModel(['error' => true, 'message' => $e->getMessage(), 'data' => []]);
        }
        return $retorno;
    }

    public function processosAction()
    {
        try {
            $tribunal = $this->params('id');
            if (!$tribunal) throw new \Exception('Favor selecionar um tribunal.');

            $file = './data/tribunais-data/' . $tribunal;
            if (!file_exists($file)) throw new \Exception('Tribunal não está disponível.');

            $fileNameFriendly = ucfirst(str_replace('.json', '', $tribunal));
            $fileNameFriendly = str_replace(['inst', 'Inst', 'Tj'], '', $fileNameFriendly);
            $uf = substr($fileNameFriendly, 0, 2);
            $inst = substr($fileNameFriendly, 2, 1);
            $fileNameFriendly = strtoupper($uf) . ' ' . $inst . '° Instância';

            $data = file_get_contents($file);

            $retorno = new ViewModel(['tribunal' => $fileNameFriendly, 'data' => $data]);
        } catch (\Exception $e) {
            $retorno = $this->backToOrigenUrl();
        }
        return $retorno;
    }

    public function deleteLogAction()
    {
        $logName = $this->params('id');
        $dir = './data/tribunais-log/';
        $ext = '.txt';
        if (file_exists($dir . $logName . $ext)) {
            file_put_contents($dir . $logName . $ext, '');
        }
        return $this->redirect()->toUrl('/logs');
    }

    public function startAction()
    {
        $tribunais = new Robo();
        $tribunais->start();
        echo 'Done!' . PHP_EOL;
        die;
    }

}
