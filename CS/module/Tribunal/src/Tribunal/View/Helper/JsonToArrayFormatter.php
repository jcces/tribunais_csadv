<?php

namespace Tribunal\View\Helper;

use Zend\View\Helper\AbstractHelper;

class JsonToArrayFormatter extends AbstractHelper
{
    /**
     * @param string $data
     * @param bool $formato
     * @return \DateTime
     */
    public function __invoke($json)
    {
        if (!$json) return '<ul><li>sem dados</li></ul>';
        $arr = json_decode($json, true);

        return '<div style="background-color: rgba(0,0,0,0.02); padding: 10px;">' . $this->formatter($arr) . '</div>';
    }

    public function formatter($arr)
    {
        $tratados = '';
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                if (count($value) == 0) {
                    $tratados .= '<li style="color: #555"><b>' . $key . ': </b>sem dados</li>';
                    continue;
                }
                $tratados .= '<li style="color: #555"><b>' . $key . ': </b>' . $this->formatter($value) . '</li>';
                continue;
            }
            $tratados .= '<li style="color: #555"><b>' . $key . ': </b>' . $value . '</li>';
        }
        return '<ul>' . $tratados . '</ul>';
    }
}