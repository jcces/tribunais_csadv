<?php

namespace Tribunal\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Processo extends AbstractEstruturaTable{

    public $table = 'TRIBUNAL_PROCESSO';
    public $campos = [
        'ID'=>'Id', 
        'ID_TRIBUNAL'=>'IdTribunal', 
        'PROCESSO'=>'Processo', 
        'PROCESSO_INTERNO'=>'ProcessoInterno',
        'URL'=>'Url',
        'DADOS'=>'Dados', 
        'HASH'=>'Hash', 
        'ALTERACAO'=>'Alteracao', 
        'PROCESSADO'=>'Processado',
        'DT_CREATED'=>'DtCreated',
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}