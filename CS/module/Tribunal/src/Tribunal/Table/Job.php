<?php

namespace Tribunal\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Job extends AbstractEstruturaTable{

    public $table = 'TRIBUNAL_JOB';
    public $campos = [
        'ID'=>'Id', 
        'DT_INCLUSAO'=>'DtInclusao', 
        'NUM_INT_PROCESSO'=>'NumIntProcesso', 
        'DES_COMARCA'=>'DesComarca', 
        'NOM_AUTOR'=>'NomAutor', 
        'CPF_AUTOR'=>'CpfAutor', 
        'NOM_REU'=>'NomReu', 
        'NOM_JUIZO'=>'NomJuizo', 
        'UF'=>'Uf', 
        'NUM_PROC_JUST'=>'NumProcJust', 
        'JUIZO_VARA'=>'JuizoVara', 
        'ALTERADO'=>'Alterado', 
        'VALIDADO'=>'Validado', 
        'OBS'=>'Obs', 
        'ORIGEM'=>'Origem', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}