<?php

namespace Tribunal\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Pesquisa extends AbstractEstruturaTable{

    public $table = 'TRIBUNAL_PESQUISA';
    public $campos = [
        'ID'=>'Id', 
        'PALAVRAS'=>'Palavras', 
        'ID_TRIBUNAL'=>'IdTribunal', 
        'PESQUISAR_TODOS_TRIBUNAIS'=>'PesquisarTodosTribunais', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}