<?php

namespace Tribunal\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Tribunal extends AbstractEstruturaTable{

    public $table = 'TRIBUNAL_TRIBUNAL';
    public $campos = [
        'ID'=>'Id', 
        'NAME'=>'Name', 
        'UF'=>'Uf',
        'URL'=>'Url',
        'URL_PROCESSO'=>'UrlProcesso',
        'CALL_METHOD'=>'CallMethod',
        'ONLINE'=>'Online', 
        'COLETANDO'=>'Coletando', 
        'DT_ULTIMA_COLETA_COMECOU'=>'DtUltimaColetaComecou',
        'DT_ULTIMA_COLETA_TERMINOU'=>'DtUltimaColetaTerminou',
        'DT_CREATED'=>'DtCreated',
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}