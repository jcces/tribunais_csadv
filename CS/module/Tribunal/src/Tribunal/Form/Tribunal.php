<?php

namespace Tribunal\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Tribunal extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('tribunal');

        $service = new \Tribunal\Service\Tribunal();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('tribunal',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("Id")->setAttribute('col','col-md-6');  
        $objForm->text("Name")->required(true)->label("Tribunal")->setAttribute('col','col-md-6');  
        $objForm->text("Uf")->required(false)->label("UF")->setAttribute('col','col-md-6');
        $objForm->text("Url")->required(true)->label("URL nome da parte")->setAttribute('col','col-md-6');
        $objForm->text("UrlProcesso")->required(false)->label("URL nro processo")->setAttribute('col','col-md-6');
        $objForm->text("CallMethod")->required(true)->label("Método a chamar")->setAttribute('col','col-md-6');
        $objForm->select("Online", $service->optionsOnline)->required(false)->label("Site online?")->setAttribute('col','col-md-6');  
        $objForm->select("Coletando", $service->optionsColetando)->required(false)->label("Coletando?")->setAttribute('col','col-md-6');  
        $objForm->datetime("DtUltimaColetaComecou")->required(false)->label("Data última coleta começou")->setAttribute('col','col-md-6');
        $objForm->datetime("DtUltimaColetaTerminou")->required(false)->label("Data última coleta terminou")->setAttribute('col','col-md-6');
        $objForm->hidden("DtCreated")->required(false)->label("Data de criação")->setAttribute('col','col-md-6');
        $objForm->hidden("DtUpdated")->required(false)->label("Data de atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}