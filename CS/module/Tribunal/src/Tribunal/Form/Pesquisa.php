<?php

namespace Tribunal\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Pesquisa extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('pesquisa');

        $service = new \Tribunal\Service\Pesquisa();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('pesquisa',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("Id")->setAttribute('col','col-md-6');  
        $objForm->text("Palavras")->required(true)->label("Palavras de pesquisa")->setAttribute('col','col-md-6');  
        $objForm->combo("IdTribunal", $service->classTribunal)->required(false)->label("Tribunal")->setAttribute('col','col-md-6');  
        $objForm->checkbox("PesquisarTodosTribunais", $service->optionsPesquisarTodosTribunais)->required(false)->label("Pesquisar em todos tribunais")->setAttribute('col','col-md-6');
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}