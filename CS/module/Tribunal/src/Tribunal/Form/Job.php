<?php

namespace Tribunal\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Job extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('job');

        $service = new \Tribunal\Service\Job();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('job',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("Id")->setAttribute('col','col-md-6');  
        $objForm->date("DtInclusao")->required(false)->label("Data inclusão")->setAttribute('col','col-md-6');  
        $objForm->text("NumIntProcesso")->required(false)->label("Nro Interno Processo")->setAttribute('col','col-md-6');  
        $objForm->text("DesComarca")->required(false)->label("Comarca")->setAttribute('col','col-md-6');  
        $objForm->text("NomAutor")->required(false)->label("Nome do Autor")->setAttribute('col','col-md-6');  
        $objForm->cpf("CpfAutor")->required(false)->label("Cpf do Autor")->setAttribute('col','col-md-6');  
        $objForm->text("NomReu")->required(false)->label("Nome do Reu")->setAttribute('col','col-md-6');  
        $objForm->text("NomJuizo")->required(false)->label("Nome do Juizo")->setAttribute('col','col-md-6');  
        $objForm->select("Uf", $service->optionsUf)->required(false)->label("UF")->setAttribute('col','col-md-6');  
        $objForm->text("NumProcJust")->required(false)->label("Nro Processo Justiça")->setAttribute('col','col-md-6');  
        $objForm->text("JuizoVara")->required(false)->label("Juízo e vara")->setAttribute('col','col-md-6');  
        $objForm->select("Alterado", $service->optionsAlterado)->required(true)->label("Registro alterado?")->setAttribute('col','col-md-6');  
        $objForm->select("Validado", $service->optionsValidado)->required(true)->label("Registro validado?")->setAttribute('col','col-md-6');  
        $objForm->text("Obs")->required(false)->label("Observações")->setAttribute('col','col-md-6');  
        $objForm->text("Origem")->required(false)->label("Origem da Extração")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}