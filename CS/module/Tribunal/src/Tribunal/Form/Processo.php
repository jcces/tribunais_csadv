<?php

namespace Tribunal\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Processo extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('processo');

        $service = new \Tribunal\Service\Processo();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('processo',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("Id")->setAttribute('col','col-md-6');  
        $objForm->combo("IdTribunal", $service->classTribunal)->required(true)->label("Tribunal")->setAttribute('col','col-md-12');
        $objForm->text("Processo")->required(true)->label("Nro Processo")->setAttribute('col','col-md-6');  
        $objForm->text("ProcessoInterno")->required(false)->label("Nro Processo Interno")->setAttribute('col','col-md-6');
        $objForm->text("Url")->required(true)->label("URL Processo")->setAttribute('col','col-md-12');
        $objForm->textarea("Dados")->required(false)->label("Dados JSON")->setAttribute('col','col-md-12');
        $objForm->hidden("Hash")->required(false)->label("Hash")->setAttribute('col','col-md-6');
        $objForm->checkbox("Alteracao", $service->optionsAlteracao)->required(false)->label("Registro Tem Alteração?")->setAttribute('col','col-md-12');
        $objForm->checkbox("Processado", $service->optionsProcessado)->required(false)->label("Registro processado hoje?")->setAttribute('col','col-md-12');
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}