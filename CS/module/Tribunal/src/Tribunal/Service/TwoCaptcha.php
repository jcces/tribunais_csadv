<?php
/**
 * Created by PhpStorm.
 * User: DELL VOSTRO
 * Date: 20/03/2018
 * Time: 15:35
 */

namespace Tribunal\Service;

use Facebook\WebDriver\WebDriverBy;

class TwoCaptcha
{
    public $key = '37ab7387ab23f79861892edf1ced224a';
    public $lastRequestId = '';
    //Normal Captcha Text/Image
    public $urlIn = 'http://2captcha.com/in.php';
    public $urlRes = 'http://2captcha.com/res.php';
    //ReCaptcha
    public $urlInReCaptcha = 'http://2captcha.com/in.php?key=';
    public $urlResReCaptcha = 'http://2captcha.com/res.php?key=';

    public function sendReCaptcha($googleKey, $pageUrl)
    {
        $url = $this->urlInReCaptcha . $this->key . '&method=userrecaptcha' . '&googlekey=' . $googleKey . '&json=1&pageurl=' . $pageUrl;
        $response = file_get_contents($url);
        if (is_array($response)) print_r($response);
        return $this->returnJson($response);
    }

    public function insertReCaptcha($drive, $hash)
    {
        /** @var $drive \Facebook\WebDriver\Remote\RemoteWebDriver */
        $drive->executeScript('document.getElementById("g-recaptcha-response").innerHTML="' . $hash . '";');
        $drive->findElement(WebDriverBy::cssSelector('input[type=submit]'))->click();

        return $drive;
    }

    public function sendImageCaptcha($imgUrl,$comment=null)
    {

        $base64 = base64_encode(file_get_contents($imgUrl));
        $time = time();
        file_put_contents("./data/captchas/{$time}.png", base64_decode($base64));
        $post = [
            'method' => 'base64',
            'key' => $this->key,
            'body' => $base64,
            'json' => 1
        ];

        if ($comment) $post['textinstructions'] = $comment;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urlIn);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return $this->returnJson($server_output);
    }

    public function returnJson($return)
    {
        if (preg_match('/ERROR/', $return)) {
            throw new \Exception($return);
        }
        $data = json_decode($return, true);
        if ($data['status'] === 1) {
            if (ctype_digit($data['request'])) {
                return $this->getResponseCaptcha($data['request']);
            }
            if (is_string($data['request'])) {
                return $data['request'];
            }
        }
        throw new \Exception($return);
    }

    public function getResponseCaptcha($id, $sleep = 3)
    {
        $this->lastRequestId = $id;
        $url = "http://2captcha.com/res.php?key={$this->key}&action=get&json=1&id={$id}";
        $retorno = '';
        sleep($sleep);
        $times = 1;
        while (true) {
            $return = file_get_contents($url);
            $this->log($return);
            if (preg_match('/ERROR/', $return)) {
                throw new \Exception($return);
            }
            if (preg_match('/CAPCHA_NOT_READY/', $return)) {
                sleep($sleep);
                continue;
            }
            $returnJson = json_decode($return, true);
            if ($returnJson['status'] === 1) {
                $retorno = $returnJson['request'];
                break;
            }
            $times++;
            if ($times > 15) {
                throw new \Exception($return);
            }
        }

        return $retorno;
    }

    public function getBalance()
    {
        $url = "http://2captcha.com/res.php?key={$this->key}&action=getbalance&json=1";
        $return = json_decode(file_get_contents($url),true);
        if ($return['status'] === 1) {
            return '2Captcha Balance: $' . number_format($return['request'],2);
        }
        return false;
    }

    public function complain($id)
    {
        if ($id == '') return false;
        $url = "http://2captcha.com/res.php?key={$this->key}&action=reportbad&id={$id}&json=1";
        $return = json_decode(file_get_contents($url),true);
        if (isset($return['status']) && $return['status'] === 1) return true;
        return false;
    }

    public function log($message)
    {
        $time = '[' . date('Y-m-d H:i:s') . '] => ';
        $message = $time . $message . PHP_EOL;
        echo $message;
    }

}