<?php

namespace Tribunal\Service;

use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Tribunal\Entity\Job as Entity;
use Facebook\WebDriver\Exception\NoAlertOpenException;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\UnexpectedAlertOpenException;
use Facebook\WebDriver\Exception\TimeOutException;

class JobRodrigo extends Entity
{

    public function startDirect($filtroUf, $method = false, $params = [])
    {
        $robo = new Robo();

        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registros = $job->filtrarObjeto();*/

        /**
         * AJUSTE DOS PROBLEMATICOS APONTADOS PELA VALÉRIA
         */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(1);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (
                empty($processo->getJuizoVara()) ||
                $processo->getJuizoVara() == '' ||
                $processo->getJuizoVara() == null ||
                $processo->getJuizoVara() == '0' ||
                $processo->getJuizoVara() == '(Processo não distribuído);'
            )
                $registros[] = $processo;
        }*/

        /**
         * ATUALIZAR PROCESSOS QUE OS OPERADORES ATUALIZARAM O NRO DE PROCESSO
         * 13/08/2018
         */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/NRO ATUALIZADO /', $processo->getObs())) {
                $registros[] = $processo;
            }
        }*/

        /**
         * ATUALIZAR PROCESSOS QUE OS OPERADORES ATUALIZARAM O NRO DE PROCESSO
         * 17/08/2018
         */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/CS.1708/',$processo->getObs())) {
                $registros[] = $processo;
            }
        }*/

        /**
         * ATUALIZAR PROCESSOS QUE OS OPERADORES ATUALIZARAM O NRO DE PROCESSO
         * 23/08/2018
         */
        $job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/CS.2308/',$processo->getObs())) {
                $registros[] = $processo;
            }
        }



        $qtdRegistros = count($registros);

        $robo->log("{$qtdRegistros} registros encontrados.");

        $start = new \DateTime();
        if (!method_exists($this, $method)) {
            $robo->log("MÉTODO {$method} NÃO EXISTE!");
            die;
        }
        if (count($params)) {
            $this->$method($registros, $params);
        } else {
            $this->$method($registros);
        }


        $end = new \DateTime();
        $interval = $start->diff($end);
        $robo->log('END => Running time: ' . $interval->format('%a days %h hours %i minutes %s seconds'));
        $robo->log('Done!');

        return true;
    }

    /*
     * RODRIGO SILVA
     * SEUS MÉTODOS AQUI
     */

    public function tjba1instanciatrf1pje($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();

        $linkGet = 'https://pje.tjba.jus.br/pje-web/ConsultaPublica/listView.seam';

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            if (strlen($nroProcesso) != 25) continue;
            if (!preg_match('/.8./', $nroProcesso)) continue;
//            if (substr($nroProcesso, 0, 1) != 8) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => {$processo->getNumProcJust()}");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->navigate()->to($linkGet);

//            $nroProcesso = '0000780-16.2011.8.05.0041';
//            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcessoDecoration:numProcesso"]'))->clear();
//            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcessoDecoration:numProcesso"]'))->sendKeys('0000780162011050041');
            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $robo->log($alertText);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:reCaptchaDiv"]/div/div/div/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();
            } catch (\Exception $e) {
            }

//            try {
//                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
//                $msg = $driver->findElement(WebDriverBy::className('rich-messages'))->getText();
//                $robo->log($msg);
//                if (!preg_match('/A verificação de captcha não está correta/', $msg)) continue;
//                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
//                $robo->log("LINE 440 => $captcha");
//                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
//                $driver->executeScript("executarPesquisa();", []);
//            } catch (\Exception $e) {
//            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }

        }

        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function tjba1grauEsaj($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $url = 'http://esaj.tjba.jus.br/cpopg/open.do';
        $driver->get($robo->host);
        $total = count($processos);
        $index = 1;

        foreach ($processos as $processo) {

            $nroProcesso = $processo->getNumProcJust();

            $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nroProcesso);
            $index++;

            if (is_null($nroProcesso)) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $vara = '';

            try {

                //ELIMINANDO DIGITO VERIFICADOR @TODO RODAR DEPOIS COM ISSO
//                if (substr($nroProcesso, -2, 1) == '-') {
//                    $robo->log($nroProcesso);
//                    $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
//                    $nroProcesso = rtrim($nroProcesso,'-');
//                    $robo->log($nroProcesso);
//                }

                $nroProcesso = $robo->cnjNumberFormatter($nroProcesso);
//                if (!preg_match('/8.05/', $nroProcesso)) continue;

                $driver->navigate()->to($url);

                $robo->saveCache(__FUNCTION__, $nroProcesso);

                // opção outros marcada
                $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();
                $nroProcesso = $robo->onlyDigits($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);

                // opção unificado marcada
//                $nroProcessoArr = explode('.8.05.', $nroProcesso);
//                if (count($nroProcessoArr) < 2) continue;
//                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();
//                $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();
//                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);
//                $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);

                $toContinue = false;
                while (true) {

                    $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

                    try {
                        $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                        $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                        if (!preg_match('/Digite o código aqui/', $msg)) {
                            $robo->log($msg);
                            $toContinue = true;
                            break;
                        }
                        $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="defaultCaptchaImage"]', '//*[@id="defaultCaptchaCampo"]', '//*[@id="pbEnviar"]', false);

                    } catch (\Exception $e) {
                        break;
                    }

                }

                if ($toContinue) continue;

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                    $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                    $processo->setObs("SEGREDO DE JUSTIÇA => {$driver->getCurrentURL()}");
                    $processo->salvar(true);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                    continue;
                } catch (\Exception $e) {
                }

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                    $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                    $processo->setObs("LISTA DE PROCESSOS => {$driver->getCurrentURL()}");
                    $processo->salvar(true);
                    continue;
                } catch (\Exception $e) {
                }

                $tableRows = '/html/body/table[4]/tbody/tr/td/table[3]/tbody/tr';
                try {
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($tableRows));
                    $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);
                    if (!isset($dadosProcesso['Distribuição']) || $dadosProcesso['Distribuição'] == '') {
                        $tableRows = '/html/body/table[4]/tbody/tr/td/table[2]/tbody/tr';
                        $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($tableRows));
                        $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);
                    }
                } catch (\Exception $e) {
                }
//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($tableRows)));

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace(';', '', $varaAux));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function tjba2grauEsaj($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            $nroProcesso = $processo->getNumProcJust();

            if (is_null($nroProcesso)) {
                continue;
            }

            $nroProcesso = $robo->cnjNumberFormatter($nroProcesso);

            //ELIMINANDO DIGITO VERIFICADOR @TODO RODAR DEPOIS COM ISSO
//            if (substr($nroProcesso, -2, 1) == '-') {
//                $robo->log($nroProcesso);
//                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
//                $nroProcesso = rtrim($nroProcesso, '-');
//                $robo->log($nroProcesso);
//            }

            // unificado
//            $url = 'http://esaj.tjba.jus.br/cpo/sg/search.do'
//                . '?paginaConsulta=1'
//                . '&cbPesquisa=NUMPROC'
//                . '&tipoNuProcesso=UNIFICADO'
//                . '&numeroDigitoAnoUnificado=' . substr($processo->getNumProcJust(), 0, 15)
//                . '&foroNumeroUnificado=' . substr($processo->getNumProcJust(), -4)
//                . '&dePesquisaNuUnificado=' . $processo->getNumProcJust()
//                . '&dePesquisa='
//                . '&pbEnviar=Pesquisar';

            // outros
            $url = 'http://esaj.tjba.jus.br/cpo/sg/search.do'
                . '?paginaConsulta=1'
                . '&cbPesquisa=NUMPROC'
                . '&tipoNuProcesso='
                . '&numeroDigitoAnoUnificado='
                . '&foroNumeroUnificado='
                . '&dePesquisaNuUnificado='
                . '&dePesquisa=' . $robo->onlyDigits($nroProcesso);

            $driver->get($url);
            $vara = '';

            try {

                $tableRows = '/html/body/table[4]/tbody/tr/td/table[2]/tbody/tr';
                $dadosProcesso = [];
                try {
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($tableRows));
                    $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                } catch (\Exception $e) {
                }
//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($tableRows)));

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $vara = $robo->cleanString($dadosProcesso['Distribuição'], ['; ', ';']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }
        }

        $driver->quit();
        return true;
    }

    public function trf2($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $code = $robo->onlyDigits($processo->getNumProcJust());

            $url = "http://portal.trf2.jus.br/portal/consulta/cons_procs.asp";
            $driver->get($url);
            $captchaSolved = false;
            $vara = '';
            try {
                $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($code);
                while (!$captchaSolved) {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[2]/td[2]/p/input'))->click();
                    sleep(1);
                    $solution = $this->solveTrf2Captcha($driver);
                    if (($solution == '') || ($solution == 0)) {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td/font/span/input[2]'))->click();
                    } else {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="captchacode"]'))->sendKeys($solution);
                    }

                    $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();
                    $text = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[2]/tbody/tr/td/font'))->getText();

                    if (strpos($text, 'Atenção: Processo não Localizado na base atual.') !== false) {
                        $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                    }

                    if ($text === 'Atenção: O valor digitado não corresponde a sequência de controle.') {
                        $captchaSolved = true;
                    }

                }
            } catch (NoSuchElementException $e) {
                // encontrei!
                $my_frame = $driver->findElement(WebDriverBy::xpath('//*[@id="container2"]/table[3]/tbody/tr[2]/td/table/tbody/tr/td[2]/p/font/span/iframe'));
                $driver->switchTo()->frame($my_frame);
                $text = $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="Resumo"]')))->getText();
                $lines = explode("\n", $text);

                foreach ($lines as $line) {
                    if (strpos($line, "Originário: ") !== false) {
                        $vara = substr($line, strpos($line, " - ") + 1);
                        $vara = substr($vara, 2);
                    }
                }

                if ($vara == '') {
                    $processo->setObs('campo vara não encontrado');
//                    throw new \Exception('VARA NÃO ENCONTRADA!');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }
        $driver->quit();
    }

    public function solveTrf2Captcha($driver)
    {
        $matches = [];
        $options = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td/font/span/b[2]'))->getText();
        $question = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td'))->getText();

        if (strpos($question, 'Quantos símbolos são números') !== false) {
            preg_match_all('/[0-9]/', $options, $matches);
            return count($matches[0]);
        }

        if (strpos($question, 'Quantos símbolos são consoantes') !== false) {
            preg_match_all('/[bcdfghjklmnpqrstvxzwy]/i', $options, $matches);
            return count($matches[0]);
        }

        if (strpos($question, 'Quantos símbolos são vogais') !== false) {
            preg_match_all('/[aeiou]/i', $options, $matches);
            return count($matches[0]);
        }

        if (strpos($question, 'Quais símbolos (informe os repetidos também, se houver) são números') !== false) {
            $answer = '';
            preg_match_all('!\d+!', $options, $matches);
            foreach ($matches[0] as $value) {
                $answer .= $value;
            }
            return $answer;
        }

        if (strpos($question, 'Quais símbolos (informe os repetidos também, se houver) são consoantes') !== false) {
            $answer = '';
            preg_match_all('/[bcdfghjklmnpqrstvxzwy]/i', $options, $matches);
            foreach ($matches[0] as $value) {
                $answer .= $value;
            }
            return $answer;
        }

        if (strpos($question, 'Quais símbolos (informe os repetidos também, se houver) são vogais') !== false) {
            $answer = '';
            preg_match_all('/[aeiou]/i', $options, $matches);
            foreach ($matches[0] as $value) {
                $answer .= $value;
            }
            return $answer;
        }

        throw new \Exception('Não consigo resolver esse captcha!');
    }

    public function juizadoEspecialRj($processos)
    {
        /**
         * ESSE METÓDO FOI SUBSTITUIDO POR "tjrjfisicos" DENTRO DO job.php
         */
        $robo = new Robo();
        $alterados = 0;
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if (!preg_match('/8.19/', $processo->getNumProcJust())) {
                continue;
            }

            $code = str_replace(
                ['.', '-', '/', '  ', ','], ['', ',', '', '', ''], $processo->getNumProcJust()
            );

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $url = "http://www4.tjrj.jus.br/ConsultaUnificada/consulta.do";
            $driver->get($url);
            $orgaoJulgador = '';

            try {
                $driver->findElement(WebDriverBy::xpath('//*[@id="parte1ProcCNJ"]'))->sendKeys(substr($code, 0, 13));
                $driver->findElement(WebDriverBy::xpath('//*[@id="parte2ProcCNJ"]'))->sendKeys(substr($code, -4));
                $driver->findElement(WebDriverBy::xpath('//*[@id="numeracaoUnica"]/input[3]'))->click();
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
            } catch (NoAlertOpenException $e) {

                //  encontrei  o  link.  clico  no  segundo
                if (count($driver->findElements(WebDriverBy::xpath('//*[@id="form"]/table/tbody/tr[2]/td/ul/li/a'))) !== 0) {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="form"]/table/tbody/tr[2]/td/ul/li/a'))->click();
                    sleep(15);
                }

                //  Tristeza  na  vida  um  programador...  o  site  não  tem  padrão...  :'-(
                if (count($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/span/table[3]/tbody/tr[5]/td[2]'))) !== 0) {
                    //  orgao  julgador
                    $orgaoJulgador = $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/span/table[3]/tbody/tr[5]/td[2]'))->getText();
                } elseif (count($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/span/table[2]/tbody/tr[5]/td[2]'))) !== 0) {
                    $orgaoJulgador = $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/span/table[2]/tbody/tr[5]/td[2]'))->getText();
                } elseif (count($driver->findElements(WebDriverBy::xpath('//*[@id="content"]/form/table/tbody/tr[contains(.,"Regional")]/td[2]'))) !== 0) {
                    //  query  magica  para  pegar  o  que  estiver  depois  da  comarca
                    $orgaoJulgador = $driver->findElement(
                        WebDriverBy::xpath('//*[@id="content"]/form/table/tbody/tr[contains(.,"Regional")]/td[2]')
                    )->getText();
                } elseif (count($driver->findElements(WebDriverBy::xpath('//*[@id="content"]/form/table/tbody/tr[contains(.,"Comarca")]/td[2]'))) !== 0) {
                    $orgaoJulgador = $driver->findElement(
                        WebDriverBy::xpath('//*[@id="content"]/form/table/tbody/tr[contains(.,"Comarca")]/td[2]')
                    )->getText();
                } else {
                    $processo->setObs('campo  vara  não  encontrado');
                    die('nenhum  ' . $processo->getNumProcJust());
                }

                $processo->setOrigem($driver->getCurrentURL());
                $processo->setAlterado(1);
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                $robo->log("ERRO  CATASTROFICO  {$url}");
                $robo->log($processo->getNumProcJust());
                die;
            }
            $robo->addVara($processo, $orgaoJulgador, false);
        }
        $robo->log("{$alterados}  PROCESSOS  COM  JUIZO|VARA");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function jfrj($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $encontrados = 0;
        $index = 1;
        $total = count($processos);
        foreach ($processos as $processo) {

            $robo->log($robo->percentage($total, $index) . " => " . $processo->getNumProcJust());
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

//            $code = $processo->getNumProcJust();
            $code = $robo->onlyDigits($processo->getNumProcJust());

            $url = "http://procweb.jfrj.jus.br/portal/consulta/cons_procs.asp";
            $driver->get($url);
            $captchaSolved = false;
            $vara = '';
            try {
                $driver->executeScript("document.getElementsByName('baixado')[0].checked = false;", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($code);
                while (!$captchaSolved) {
                    $driver->executeScript("document.getElementsByName('baixado')[0].checked = false;", []);
                    $solution = $this->solveTrf2Captcha($driver);
                    if (($solution == '') || ($solution == 0)) {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td/font/span/input[2]'))->click();
                    } else {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="captchacode"]'))->sendKeys($solution);
                    }

                    $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();
                    $text = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[2]/tbody/tr/td/font'))->getText();

                    $robo->log($text);
                    if ($text === 'Atenção:  O  valor  digitado  não  corresponde  a  sequência  de  controle.') {
                        $captchaSolved = true;
                    } else {
                        break;
                    }
                }
            } catch (NoSuchElementException $e) {
                //  encontrei!
                $my_frame = $driver->findElement(WebDriverBy::xpath('//*[@id="container2"]/table[3]/tbody/tr[2]/td/table/tbody/tr/td[2]/p/font/span/iframe'));
                $driver->switchTo()->frame($my_frame);
                $text = $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="Resumo"]')))->getText();
                $textareaArr = explode("\n", $text);

                $vara = '';
                foreach ($textareaArr as $textItem) {
                    if ((preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
                            preg_match('/a. VARA FEDERAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Vara Federal/i', $textItem) ||
                            preg_match('/ª Vara Federal/i', $textItem) ||
                            preg_match('/([0-9]{1})ª TURMA RECURSAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem) ||
                            preg_match('/ª Turma Recursal/i', $textItem) ||
                            preg_match('/([0-9]{1})° JUIZADO ESPECIAL/i', $textItem) ||
                            preg_match('/º Juizado Especial/i', $textItem) ||
                            preg_match('/([0-9]{1})° Juizado Especial/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                } else {
                    $robo->log("{$processo->getNumProcJust()} NÃO ENCONTRADO");
                    die;
                }

            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }
        $driver->quit();
        $robo->log(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjbaprojudi($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver(true, false);
        $robo->log(__FUNCTION__);
        $driver->get('https://projudi.tjba.jus.br/projudi/PaginaPrincipal.jsp');
        foreach ($processos as $processo) {

            if (preg_match('/Processo arquivado/',$processo->getObs())) continue;
            if (preg_match('/Segredo de Justiça/',$processo->getObs())) continue;

            $nroProcesso = $processo->getNumProcJust();

            if (is_null($nroProcesso)) {
                continue;
            }

//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            $nroProcesso = $robo->onlyDigits($nroProcesso);
//            if (strlen($nroProcesso) == 20) continue;
//            if (!preg_match('/8.05/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//            $nroProcesso = preg_replace('/\D/', '', '0003869-62.2013.8.05.0078');// PARA TESTE

            //ANTIGA FORMA
//            $url = 'https://projudi.tjba.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=' . $nroProcesso;
//            $driver->navigate()->to($url);

            $conteudo = '//*[@id="Partes"]/table';
            while (true) {
                $robo->log($nroProcesso);
                $driver->navigate()->to('https://projudi.tjba.jus.br/projudi/interno.jsp?endereco=/projudi/buscas/ProcessosParte');
                $driver->navigate()->to('https://projudi.tjba.jus.br/projudi/buscas/ProcessosParte');
//            $url = 'https://projudi.tjba.jus.br/projudi/buscas/ProcessosParte';

                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->sendKeys($nroProcesso);

                $driver->executeScript('import("https://code.jquery.com/jquery-3.3.1.slim.min.js");',[]);
                sleep(3);
                $driver->executeScript('$("#idCapcha").css({"position": "absolute","top": 0});',[]);

                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    '//*[@id="idImg"]',
                    '//*[@id="captcha"]',
                    '/html/body/form/p/table/tbody/tr[31]/td/input[1]',
                    false
                );

                try{
                    $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));
                    break;
                }catch(\Exception $e){
                }

                try{
                    $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/font/strong/p'))->getText());
                    $robo->log($msg);
                    if (preg_match('/AVISO: Processo sob Segredo de Justiça/',$msg) || preg_match('/AVISO: Processo arquivado/',$msg)) {
                        $processo->addObs($msg);
                        $processo->salvar(true);
                        break;
                    }
                    if (preg_match('/Caracteres da imagem inválidos/',$msg)) {
                        continue;
                    }
                    if (preg_match('/AVISO: Processo não encontrado/',$msg)) {
                        break;
                    }
                }catch(\Exception $e){
                    break;
                }

            }

            try {

                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                $robo->log(count($rows));
                foreach ($rows as $row) {
                    $robo->log($row->getText());
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }
        $driver->quit();
        $robo->log(__FUNCTION__);
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpiRecursal($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $code = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.18/', $code)) continue;
            $url = 'http://www.tjpi.jus.br/ThemisWebRecursal/modules/processo/ConsultaPublica.mtw';
            $driver->get($url);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/form/fieldset/div[5]/input')));
                sleep(1);
                $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/form/fieldset/div[5]/input'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/form/fieldset/div[2]/input'))->sendKeys($code);
                $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/form/fieldset/div[2]/button'))->click();
//                $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/form/fieldset/div[2]/input'))->sendKeys(WebDriverKeys::ENTER);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]')));
                    $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]'))->getText());
                    $robo->log($msg);
                    if (preg_match('/Sua busca não retornou resultados/', $msg)) continue;
                } catch (\Exception $e) {
                }

                die;

                $vara = '';
                $tables = $driver->findElements(WebDriverBy::cssSelector('table.detalhes'));
                foreach ($tables as $table) {
                    $tableContent = $table->getText();
                    if (preg_match('/Gabinete [/]{1} Secretaria/', $tableContent)) {
                        $rows = $table->findElements(WebDriverBy::tagName('tr'));
                        foreach ($rows as $row) {
                            $h = $row->findElement(WebDriverBy::tagName('th'));
                            if (preg_match('/Gabinete [/]{1} Secretaria/', $h->getText())) {
                                $vara = $robo->cleanString($row->findElement(WebDriverBy::tagName('td')));
                                break;
                            }
                        }
                        break;
                    }
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function projuditjpi($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $title = __FUNCTION__;
        $robo->log($title);
        $encontrados = 0;
        $total = count($processos);
        $index = 1;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/8.18/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;
//            $code = preg_replace('/\D/', '', $nroCnj);
            $code = preg_replace('/\D/', '', $processo->getNumProcJust());

            $url = 'https://projudi.tjpi.jus.br/projudi/publico/buscas/ProcessosParte?publico=true';
            $driver->get($url);

            try {

                $url = 'https://projudi.tjpi.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=' . $code;
                $driver->get($url);
                $robo->saveCache(__FUNCTION__, $code);

                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function tjpipje($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $encontrados = 0;
        $index = 1;
        $totalProcessos = count($processos);
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($totalProcessos,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => " . $processo->getNumProcJust());
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.18/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 2) != '08') continue;

            $vara = '';
            $domain = 'https://tjpi.pje.jus.br';
            $url = $domain . '/pje/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))
                    ->sendKeys($nroCnj);
                try {
                    //ALERT
                    $driver->wait(1)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $driver->switchTo()->alert()->getText();
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    continue;
                } catch (\Exception $e) {
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

                try {
                    //ALERT
                    $driver->wait(1)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $driver->switchTo()->alert()->getText();
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    continue;
                } catch (\Exception $e) {
                }

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
            }
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpipje2inst($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $encontrados = 0;
        $index = 1;
        $totalProcessos = count($processos);
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($totalProcessos,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => " . $processo->getNumProcJust());
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.18/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;

            $vara = '';
            $domain = 'https://tjpi.pje.jus.br';
            $url = $domain . '/pje2g/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroCnj}\";", []);

                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    '//*[@id="fPP:cDiv"]/div/div/span/div/p/img',
                    '//*[@id="fPP:cDiv"]/div/div/span/div/p[2]/input',
                    '//*[@id="fPP:searchProcessos"]',
                    false
                );

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                        break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
            }
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpijus($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $robo->log($processo->getNumProcJust());
            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            $nroCnj = '0023770-27.2014.8.18.0140'; //nro de teste
            $nroProcesso = preg_replace('/\D/', '', $nroCnj);

            $driver->get('http://www.tjpi.jus.br/themisconsulta/');

            $target = 'input-numero-unico';
            if (strlen($nroProcesso) != 20) {
                $driver->findElement(WebDriverBy::xpath('//*[@id="checkbox-numero-antigo"]'))->click();
                $target = 'input-numero-legado';
                $driver->findElement(WebDriverBy::xpath('//*[@id="select-comarca-numero"]'))->sendKeys($processo->getDesComarca());
            }

            try {

//                $driver->findElement(WebDriverBy::xpath($target))->sendKeys($nroProcesso);
                $driver->executeScript("document.getElementById(\"{$target}\").value=\"{$nroProcesso}\";", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="input-numero-unico"]'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="form-busca-numero"]/fieldset/div/div[4]/button'))->click();

                $fieldVara = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="container"]/div[2]/div[10]/table/tbody/tr[4]/th'))
                    ->getText();

                if ($fieldVara === 'Secretaria/Vara') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="container"]/div[2]/div[10]/table/tbody/tr[4]/td'))
                        ->getText();
                } else {
                    die('Nao encontrei a vara ' . $nroProcesso);
                }
                $vara = $robo->cleanString($vara);
                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function tjpijusetjpi($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $robo->log("TRIBUNAL PIAUÍ - e-tjpi");
        $total = count($processos);
        $index = 1;
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/.8.18./', $nroProcesso)) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.####.######-#', 15);
                if (!$nroProcesso) continue;
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($nroProcesso);
            $url = 'http://www.tjpi.jus.br/e-tjpi/home/consulta';
            $driver->get($url);

            try {

                $target = '//*[@id="num_processo"]';
                if (strlen($nroProcesso) > 20) {
                    $target = '//*[@id="NNNNNNN"]';
                }

                $driver
                    ->findElement(WebDriverBy::xpath($target))
                    ->sendKeys($nroProcesso)
                    ->sendKeys(WebDriverKeys::ENTER);
                sleep(1);

                $fieldVara = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="wrapper"]/div/div[3]/div/div/div[2]/div[3]/div[2]/dl[2]/dt[2]'))
                    ->getText();

                if ($fieldVara === 'Órgão Julgador') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="wrapper"]/div/div[3]/div/div/div[2]/div[3]/div[2]/dl[2]/dd[2]'))
                        ->getText();
                } else {
                    die('Nao encontrei a vara ' . $nroCnj);
                }
                $vara = $robo->cleanString($vara);

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $nroProcesso);
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $nroProcesso);
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    function apptjpb($processos)
    {
        $robo = new Robo();
        $robo->log(__FUNCTION__);
        $total = count($processos);
        $driver = $robo->startDriver();
        $url = 'https://app.tjpb.jus.br/consultaprocessual2/views/inicio.jsf';
        $driver->get($robo->host);
        $encontrados = 0;
        $index = 1;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => " . $processo->getNumProcJust());
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

//            $nroCnj = $robo->cnjNumberFormatter('3008446-70.2014.8.15.2001');
//            $nroCnj = $robo->cnjNumberFormatter('3006425-24.2014.815.2001');
            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/8.15/', $nroCnj)) continue;
            $code = preg_replace('/\D/', '', $nroCnj);
            $driver->get($url);
            $driver->executeScript("document.getElementById(\"formConsultaProcessual:blocoConsultaPadrao:cpSelectGrau1:0\").checked = true;", []);
            $driver->executeScript("document.getElementById(\"formConsultaProcessual:blocoConsultaPadrao:cpSelectGrau1:1\").checked = true;", []);
            $driver->executeScript("document.getElementById(\"formConsultaProcessual:blocoConsultaPadrao:cpSelectGrau1:2\").checked = true;", []);
            $driver->executeScript("document.getElementById(\"formConsultaProcessual:blocoConsultaPadrao:cpSelectGrau1:3\").checked = true;", []);

            $vara = '';

            try {

//                $driver
//                    ->findElement(WebDriverBy::xpath('//*[@id="formConsultaProcessual:blocoConsultaPadrao:botaoLimpar"]'))
//                    ->click();

                $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="formConsultaProcessual:blocoConsultaPadrao:textoCaptcha"]'))
                    ->clear();

                $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="formConsultaProcessual:blocoConsultaPadrao:textoPesquisa"]'))
                    ->sendKeys($code);

                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    '//*[@id="captcha"]/label/img',
                    '//*[@id="formConsultaProcessual:blocoConsultaPadrao:textoCaptcha"]',
                    '//*[@id="formConsultaProcessual:blocoConsultaPadrao:botaoConsultar"]',
                    false
                );

                $xpathVara = '//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[2]/table/tbody/tr[1]/td[1]/label';
                $xpathComarca = '//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[3]/table/tbody/tr[1]/td[1]/label';
                $xpathTurma = '//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[2]/table/tbody/tr[1]/td[1]/label';
                $xpathJuizo = '//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/label';

                $comarcaExists = count($driver->findElements(WebDriverBy::xpath($xpathComarca)));
                $turmaExists = count($driver->findElements(WebDriverBy::xpath($xpathTurma)));
                $juizoExists = count($driver->findElements(WebDriverBy::xpath($xpathJuizo)));

                if ($driver->findElement(WebDriverBy::xpath($xpathVara))->getText() === 'Vara:') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[2]/table/tbody/tr[1]/td[2]'))
                        ->getText();
                } elseif ($comarcaExists && ($driver
                            ->findElement(WebDriverBy::xpath($xpathComarca))->getText() === 'Comarca:')) {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[3]/table/tbody/tr[1]/td[2]'))
                        ->getText();
                } elseif ($turmaExists && ($driver
                            ->findElement(WebDriverBy::xpath($xpathTurma))->getText() === 'Vara de Origem:')) {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[2]/table/tbody/tr[1]/td[2]'))
                        ->getText();
                } elseif ($juizoExists && ($driver
                            ->findElement(WebDriverBy::xpath($xpathJuizo))->getText() === 'Juízo:')) {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="formConsultaProcessualNumero:painelGridGeral"]/tbody/tr/td[2]/table/tbody/tr[2]/td[2]'))
                        ->getText();
                } else {
                    $robo->log('Nao encontrei a vara ' . $nroCnj);
                    continue;
                }
//                die($vara);
                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                $encontrados++;

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $driver->quit();
        $robo->log(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    /**
     * Não foi encontrado nenhum processo.
     */
    public function ejustjpb($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $url = 'https://ejus.tjpb.jus.br/projudi/interno.jsp?endereco=/projudi/ConsultaPublica.jsp';
        $driver->get($url);

        $total = count($processos);
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $nroProcesso = $processo->getNumProcJust();
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroCnj = $robo->onlyDigits($processo->getNumProcJust());
//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/8.15/', $nroCnj)) continue;
            $code = preg_replace('/\D/', '', $nroCnj);

            try {
                $url = 'https://ejus.tjpb.jus.br/projudi/buscas/ProcessosParte';
                $driver->get($url);
                $driver->findElement(WebDriverBy::xpath('/html/body/form/p/table/tbody/tr[4]/td[2]/label[1]/input'))->click();//2° LEVA COM A OPÇÃO "Promovente"
                $driver->findElement(WebDriverBy::xpath('//*[@id="codStatusProcesso"]'))->sendKeys('Todos');
                $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))
                    ->sendKeys($nroCnj)
                    ->sendKeys(WebDriverKeys::ENTER);

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="tableProcessos"]/tbody/tr/td[2]/a')));
                $driver->findElement(WebDriverBy::xpath('//*[@id="tableProcessos"]/tbody/tr/td[2]/a'))->click();

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="processoPrincipal"]/tbody/tr')));
                $linhas = $driver->findElements(WebDriverBy::xpath('//*[@id="processoPrincipal"]/tbody/tr'));

                $dados = $robo->extractVerticalTable($linhas, 2);

                if (isset($dados['Juízo']) && $dados['Juízo'] != '') {
                    $vara = $robo->cleanString($dados['Juízo']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function pjetjpb($processos)
    {
        $robo = new Robo();
        $robo->log(__FUNCTION__);
        $driver = $robo->startDriver();
        $total = count($processos);
        $robo->log("$total PROCESSOS");
        $encontrados = 0;
        foreach ($processos as $index => $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.15/', $nroCnj)) continue;
            if (substr($nroCnj, 0, 1) != 0) continue;

//            $nroCnj = '0001042-47.2013.8.15.2003';

            $vara = '';
            $captcha = '';
            $domain = 'https://pje.tjpb.jus.br';
            $url = $domain . '/pje/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroCnj}\";", []);
//                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcessoDecoration:numProcesso"]'))->clear();
//                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcessoDecoration:numProcesso"]'))->sendKeys($nroCnj);
                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();


                //CAPTCHA
                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[4]/div[2]/iframe')));
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                    $driver->executeScript("onSubmit();", []);
                } catch (\Exception $e) {
                }

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }


                /*$varaField = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="pageBody"]/div[1]/div/div/div/table/tbody/tr[2]/td/table/tbody/tr/td/form/div/div[1]/div[3]/table/tbody/tr[2]/td/span/div/div/div'))
                    ->getText();

                if ($varaField === 'Órgão Julgador') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="pageBody"]/div[1]/div/div/div/table/tbody/tr[2]/td/table/tbody/tr/td/form/div/div[1]/div[3]/table/tbody/tr[2]/td/span/div/div/div[2]'))
                        ->getText();
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, true, $driver->getCurrentURL());*/

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
            }
        }

        $driver->quit();
        $robo->log(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function webjfpb($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/8.15/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;
            $nroCnj = preg_replace('/\D/', '', $processo->getNumProcJust());
            $vara = '';
            $url = 'http://web.jfpb.jus.br/consproc/cons_procs.asp';
            $driver->get($url);

            try {

                $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($nroCnj);

                $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ResConsProc"]/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td/table/tbody/tr/td[2]/p/font/span/iframe')));
                $iframe = $driver->findElement(WebDriverBy::xpath('//*[@id="ResConsProc"]/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td/table/tbody/tr/td[2]/p/font/span/iframe'));
                $driver->switchTo()->frame($iframe);
                $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="Resumo"]')));

                $textarea = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="Resumo"]'))
                    ->getText();

                $lines = explode("\n", $textarea);
                $vara = '';
                foreach ($lines as $textItem) {
                    if ((preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        $vara = trim(str_replace([' - Juiz Titular', '- Juiz Substituto'], ['', ''], $vara));
                        break;
                    }
                }

                if ($vara === '') {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    private function getSSLPage($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function jurisconsulttjmaJuizadoEspecial($processos)
    {
        $robo = new Robo();
        foreach ($processos as $processo) {
            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $url = "https://apijuris.tjma.jus.br/v1/juizado/processos?numero={$nroCnj}&bolunico=S&juizado=2436";
            $vara = '';
            try {
                $json = $this->getSSLPage($url);
                $result = json_decode($json, true);

                if (empty($result['response'])) {
                    $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                    continue;
                }

                $vara = $result['response']['processo'][0]['strVara'];

                if ($vara == '') {
                    var_dump('Nao achei a vara', $nroCnj, $result['response']['processo'][0]['strVara'], $url);
                    die();
                }

                $robo->addVara($processo, $vara, false, $url);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
            }

            // pg = primeiro grau https://apijuris.tjma.jus.br/v1/pg/processos?numero={$nroCnj}&bolunico=S&comarca=118
            // sg = segundo grau https://apijuris.tjma.jus.br/v1/sg/processos?numero=0004001-24.2015.8.10.0139&bolunico=S
            // juizado = juizado https://apijuris.tjma.jus.br/v1/juizado/processos?numero=0004001-24.2015.8.10.0139&bolunico=S&juizado=2436
            // turma = recursal https://apijuris.tjma.jus.br/v1/turma/processos?numero=0000453-61.2007.8.10.9001&bolunico=S&turma=3002
        }
    }

    public function jurisconsulttjmaPrimeiroGrau($processos)
    {
        $robo = new Robo();
        foreach ($processos as $processo) {
            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $url = "https://apijuris.tjma.jus.br/v1/pg/processos?numero={$nroCnj}&bolunico=S&comarca=118";
            $vara = '';
            try {
                $json = $this->getSSLPage($url);
                $result = json_decode($json, true);

//                var_dump('jurisconsulttjmaPrimeiroGrau', $nroCnj, $result['response']);
                sleep(2);
                if (empty($result['response'])) {
                    $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                    continue;
                }

                $vara = $result['response']['processo'][0]['strVara'];

                if ($vara == '') {
                    var_dump('Nao achei a vara', $nroCnj, $result['response']['processo'][0]['strVara'], $url);
                    die();
                }

                $robo->addVara($processo, $vara, false, $url);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
            }

            // pg = primeiro grau https://apijuris.tjma.jus.br/v1/pg/processos?numero={$nroCnj}&bolunico=S&comarca=118
            // sg = segundo grau https://apijuris.tjma.jus.br/v1/sg/processos?numero=0004001-24.2015.8.10.0139&bolunico=S
            // juizado = juizado https://apijuris.tjma.jus.br/v1/juizado/processos?numero=0004001-24.2015.8.10.0139&bolunico=S&juizado=2436
            // turma = recursal https://apijuris.tjma.jus.br/v1/turma/processos?numero=0000453-61.2007.8.10.9001&bolunico=S&turma=3002
        }
    }

    public function jurisconsulttjmaSegundoGrau($processos)
    {
        $robo = new Robo();
        foreach ($processos as $processo) {
            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $url = "https://apijuris.tjma.jus.br/v1/sg/processos?numero={$nroCnj}&bolunico=S";
            $vara = '';
            try {
                $json = $this->getSSLPage($url);
                $result = json_decode($json, true);

//                var_dump('jurisconsulttjmaSegundoGrau', $nroCnj, $result['response']);
                sleep(2);
                if (empty($result['response'])) {
                    $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                    continue;
                }

//                $vara = $result['response']['processo'][0]['strVara'];
                $vara = $result['response']['distribuicoes'][0]['txCamara'];

                if ($vara == '') {
                    var_dump('Nao achei a vara', $nroCnj, $result['response']['distribuicoes'], $url);
                    die();
                }

                $robo->addVara($processo, $vara, false, $url);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
            }

            // pg = primeiro grau https://apijuris.tjma.jus.br/v1/pg/processos?numero={$nroCnj}&bolunico=S&comarca=118
            // sg = segundo grau https://apijuris.tjma.jus.br/v1/sg/processos?numero=0004001-24.2015.8.10.0139&bolunico=S
            // juizado = juizado https://apijuris.tjma.jus.br/v1/juizado/processos?numero=0004001-24.2015.8.10.0139&bolunico=S&juizado=2436
            // turma = recursal https://apijuris.tjma.jus.br/v1/turma/processos?numero=0000453-61.2007.8.10.9001&bolunico=S&turma=3002
        }
    }

    public function jurisconsulttjmaRecursal($processos)
    {
        $robo = new Robo();
        foreach ($processos as $processo) {
            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $url = "https://apijuris.tjma.jus.br/v1/turma/processos?numero={$nroCnj}&bolunico=S&turma=3002";
            $vara = '';
            try {
                $json = $this->getSSLPage($url);
                $result = json_decode($json, true);

                var_dump('jurisconsulttjmaRecursal', $nroCnj, $result['response']);
                sleep(2);
                if (empty($result['response'])) {
                    $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                    continue;
                }

                $vara = $result['response']['processo'][0]['strVara'];

                if ($vara == '') {
                    var_dump('Nao achei a vara', $nroCnj, $result['response']['processo'][0]['strVara'], $url);
                    die();
                }

                $robo->addVara($processo, $vara, false, $url);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
            }

            // pg = primeiro grau https://apijuris.tjma.jus.br/v1/pg/processos?numero={$nroCnj}&bolunico=S&comarca=118
            // sg = segundo grau https://apijuris.tjma.jus.br/v1/sg/processos?numero=0004001-24.2015.8.10.0139&bolunico=S
            // juizado = juizado https://apijuris.tjma.jus.br/v1/juizado/processos?numero=0004001-24.2015.8.10.0139&bolunico=S&juizado=2436
            // turma = recursal https://apijuris.tjma.jus.br/v1/turma/processos?numero=0000453-61.2007.8.10.9001&bolunico=S&turma=3002

        }
    }

    public function projuditjma($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $url = 'https://projudi.tjma.jus.br/projudi/publico/buscas/ProcessosParte?publico=true';
        $driver->get($url);
        $encontrados = 0;
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroCnj = $processo->getNumProcJust();
            $code = preg_replace('/\D/', '', $nroCnj);

            try {
                $url = 'https://projudi.tjma.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=' . $code;
                $driver->get($url);

                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                /*$xpathJuizo = '//*[@id="Partes"]/table[2]/tbody/tr[8]/td[1]/div/strong';
                $xpathJuizo2 = '//*[@id="Partes"]/table[2]/tbody/tr[7]/td[1]/div/strong';

                $juizo = $driver->findElement(WebDriverBy::xpath($xpathJuizo))->getText();

                if ($juizo === 'Juízo:') {
                    $fullText = $driver->findElement(WebDriverBy::xpath('//*[@id="Partes"]/table[2]/tbody/tr[8]/td[2]'))->getText();
                } elseif ($driver->findElement(WebDriverBy::xpath($xpathJuizo2))->getText() === 'Juízo:') {
                    $fullText = $driver->findElement(WebDriverBy::xpath('//*[@id="Partes"]/table[2]/tbody/tr[7]/td[2]'))->getText();
                } else {
                    throw new \Exception('Não encontrei o juizo!');
                }

                $vara = explode(' Juiz Titular: ', $fullText);
                $robo->addVara($processo, $vara[0], false, $driver->getCurrentURL());*/

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die('erro catastrófico');
            }
        }

        $driver->quit();
        $robo->log(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function pjetjma($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.10/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;

            $vara = '';
            $domain = 'https://pje.tjma.jus.br';
            $url = $domain . '/pje/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {
                $driver->executeScript("document.getElementById(\"fPP:idElementoDecoration:idElemento\").value=\"{$nroCnj}\";", []);

                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();


                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("onSubmit();", []);

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);
                $varaField = $driver
                    ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td[3]/span/div/div'))
                    ->getText();

                if ($varaField === 'Órgão Julgador') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td[3]/span/div/div[2]'))
                        ->getText();
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die('erro catastrofico');
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function pje2tjma($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.10/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;

//            $nroCnj = '0801050-68.2016.8.10.0016';
            $vara = '';
            $domain = 'https://pje2.tjma.jus.br';
            $url = $domain . '/pje2g/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver->executeScript("document.getElementById(\"fPP:idElementoDecoration:idElemento\").value=\"{$nroCnj}\";", []);

                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();


                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("onSubmit();", []);

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);
                $varaField = $driver
                    ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td[2]/span/div/div[2]/div/div/b'))
                    ->getText();

                if ($varaField === 'Órgão Julgador Colegiado') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td[2]/span/div/div[2]/div/div'))
                        ->getText();
                    $vara = str_replace('Órgão Julgador Colegiado', '', $vara);
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die('erro catastrofico');
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function servicostjmtPrimeiraInstancia($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = preg_replace('/\D/', '', $processo->getNumProcJust());
            $vara = '';
            $url = 'http://servicos.tjmt.jus.br/processos/comarcas/dadosProcesso.aspx';
            $driver->get($url);

            try {

                $driver->findElement(WebDriverBy::xpath('//*[@id="processo"]'))
                    ->clear()
                    ->sendKeys($nroCnj)
                    ->sendKeys(WebDriverKeys::ENTER);

                $driver->findElement(WebDriverBy::xpath('//*[@id="buscarProcesso"]'))->click();

                $varaField = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/div[3]/table/tbody/tr[2]/td[1]/span'))
                    ->getText();

                if ($varaField === 'Lotação:') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/div[3]/table/tbody/tr[2]/td[2]/span'))
                        ->getText();
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function servicostjmtSegundaInstancia($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = preg_replace('/\D/', '', $processo->getNumProcJust());
//            $nroCnj = '0116290-72.2010.8.11.0000';
            $vara = '';
            $url = 'http://servicos.tjmt.jus.br/processos/tribunal/consulta.aspx';
            $driver->get($url);

            try {

                $selectElement = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="instancia"]'));
                $select = new \Facebook\WebDriver\WebDriverSelect($selectElement);
                $select->selectByValue('2');

                $driver->findElement(WebDriverBy::xpath('//*[@id="processo"]'))
                    ->clear()
                    ->sendKeys($nroCnj)
                    ->sendKeys(WebDriverKeys::ENTER);

                $driver->findElement(WebDriverBy::xpath('//*[@id="buscarProcesso"]'))->click();

                $varaField = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/div[2]/table/tbody/tr[2]/td[1]/span'))
                    ->getText();

                if ($varaField === 'Câmara:') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/div[2]/table/tbody/tr[2]/td[2]/span'))
                        ->getText();
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function servicostjmtRecursal($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = preg_replace('/\D/', '', $processo->getNumProcJust());
//            $nroCnj = '57/2007';
            $vara = '';
            $url = 'http://servicos.tjmt.jus.br/processos/recursal/consulta.aspx';
            $driver->get($url);

            try {

                $selectElement = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="instancia"]'));
                $select = new \Facebook\WebDriver\WebDriverSelect($selectElement);
                $select->selectByValue('3');

                $driver->findElement(WebDriverBy::xpath('//*[@id="processo"]'))
                    ->clear()
                    ->sendKeys($nroCnj)
                    ->sendKeys(WebDriverKeys::ENTER);

                $driver->findElement(WebDriverBy::xpath('//*[@id="buscarProcesso"]'))->click();

                $varaField = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/table[1]/tbody/tr[2]/td[1]/span'))
                    ->getText();

                if ($varaField === 'Câmara:') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/table[1]/tbody/tr[2]/td[2]/span'))
                        ->getText();
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function projuditjmt($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();

        $encontrados = 0;

        //OPEN SESSION
        $url = 'http://projudi.tjmt.jus.br/projudi/publico/buscas/ProcessosPartePublico';
        $driver->get($url);

        $driver->findElement(WebDriverBy::xpath('//*[@id="id_nome_parte"]'))->sendKeys('CAIXA SEGURADORA');

        $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="captchaFormBuscaProcessos"]/div/div/div/iframe')));
        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
        $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
        $driver->findElement(WebDriverBy::xpath('//*[@id="id_aba_buscar_processos"]/form/table/tbody/tr[28]/td/table/tbody/tr[2]/td[1]/a/input'))->click();

        $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="texto"]/table/tbody/tr[3]/td[1]/a[2]')));
        $driver->findElement(WebDriverBy::xpath('//*[@id="texto"]/table/tbody/tr[3]/td[1]/a[2]'))->click();

        $total = count($processos);
        $index = 1;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/8.11/', $nroProcesso)) continue;
            //if (substr($nroProcesso, 0, 1) != 0) continue;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $processo->getNumProcJust()");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            $vara = '';
            $urlProcesso = 'http://projudi.tjmt.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[NRO_PROCESSO]';
            $driver->get(str_replace('[NRO_PROCESSO]', $nroProcesso, $urlProcesso));

            try {

                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->clearCache(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    public function pjetjmt($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $total = count($processos);
        $robo->log("$total PROCESSOS");
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.11/', $nroCnj)) continue;
            if (!in_array(substr($nroCnj, 0, 1), ['1', '8'])) continue;

//            $nroCnj = '1000527-75.2016.8.11.0006';

            $vara = '';
            $captcha = '';
            $domain = 'http://pje.tjmt.jus.br';
            $url = $domain . '/pje/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroCnj}\";", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[4]/div[2]/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("onSubmit();", []);

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->clearCache(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    public function pje2tjmt($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $total = count($processos);
        $robo->log("$total PROCESSOS");
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.11/', $nroCnj)) continue;
            if (!in_array(substr($nroCnj, 0, 1), ['1', '8'])) continue;

//            $nroCnj = '1002358-79.2016.8.11.0000';

            $vara = '';
            $captcha = '';
            $domain = 'http://pje2.tjmt.jus.br';
            $url = $domain . '/pje2/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroCnj}\";", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:consultaSearchForm"]/p/button'))->click();

//                $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[4]/div[2]/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("onSubmit();", []);

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->clearCache(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    public function jfesjus($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $code = $robo->onlyDigits($processo->getNumProcJust());

            $url = "http://www2.jfes.jus.br/jfes/portal/consulta/cons_procs.asp";
            $driver->get($url);
            $captchaSolved = false;
            $vara = '';
            try {
                $driver->executeScript("document.getElementsByName('baixado')[0].checked = false;", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($code);
                while (!$captchaSolved) {
                    $driver->executeScript("document.getElementsByName('baixado')[0].checked = false;", []);
                    sleep(1);
                    $solution = $this->solveTrf2Captcha($driver);
                    if (($solution == '') || ($solution == 0)) {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td/font/span/input[2]'))->click();
                    } else {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="captchacode"]'))->sendKeys($solution);
                    }

                    $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();
                    $text = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[2]/tbody/tr/td/font'))->getText();

                    if (strpos($text, 'Atenção: Processo não Localizado na base atual.') !== false) {
                        $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                    }

                    if ($text === 'Atenção: O valor digitado não corresponde a sequência de controle.') {
                        $captchaSolved = true;
                    }

                }
            } catch (NoSuchElementException $e) {
                // encontrei!
                $my_frame = $driver->findElement(WebDriverBy::xpath('//*[@id="container2"]/table[3]/tbody/tr[2]/td/table/tbody/tr/td[2]/p/font/span/iframe'));
                $driver->switchTo()->frame($my_frame);
                $text = $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="Resumo"]')))->getText();
                $lines = explode("\n", $text);

                foreach ($lines as $textItem) {
                    if (preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
//                        preg_match('/([0-9]{1})ª VF Serra/i', $textItem) ||
                        preg_match('/([0-9]{1})ª VF /i', $textItem) ||
                        preg_match('/([0-9]{1})ª  Vara Federal Cível/i', $textItem) ||
                        preg_match('/([0-9]{1})ª Vara Federal Cível/i', $textItem) ||
                        preg_match('/([0-9]{1})ª  Vara Federal/i', $textItem) ||
                        preg_match('/([0-9]{1})º Juizado Especial/i', $textItem) ||
                        preg_match('/Juizado Especial Federal/i', $textItem) ||
                        preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        $vara = trim(str_replace([' - Juiz Titular', '- Juiz Substituto'], ['', ''], $vara));
                        break;
                    }
                }

                if ($vara == '') {
//                    $processo->setObs('campo vara não encontrado');
                    die($processo->getNumProcJust() . ' VARA NÃO ENCONTRADA!');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }
        $driver->quit();
    }

    public function projuditjes($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $url = 'https://sistemas.tjes.jus.br/projudi/publico/buscas/ProcessosParte?publico=true';
        $driver->get($url);
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $code = preg_replace('/\D/', '', $nroCnj);
//            $code = '446477320138100001';
//            $code = '00120110241070';

            try {
                $url = 'https://sistemas.tjes.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=' . $code;
                $driver->get($url);

                $xpathJuizo = '//*[@id="Partes"]/table[2]/tbody/tr[8]/td[1]/div/strong';
                $xpathJuizo2 = '//*[@id="Partes"]/table[2]/tbody/tr[7]/td[1]/div/strong';

                $juizo = $driver->findElement(WebDriverBy::xpath($xpathJuizo))->getText();

                if ($juizo === 'Juízo:') {
                    $fullText = $driver->findElement(WebDriverBy::xpath('//*[@id="Partes"]/table[2]/tbody/tr[8]/td[2]'))->getText();
                } elseif ($driver->findElement(WebDriverBy::xpath($xpathJuizo2))->getText() === 'Juízo:') {
                    $fullText = $driver->findElement(WebDriverBy::xpath('//*[@id="Partes"]/table[2]/tbody/tr[7]/td[2]'))->getText();
                } else {
                    throw new \Exception('Não encontrei o juizo!');
                }

                $vara = explode(' Juiz: ', $fullText);
                $robo->addVara($processo, $vara[0], false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die('erro catastrófico');
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function pjetjes($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.08/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;

            $vara = '';
            $domain = 'https://sistemas.tjes.jus.br';
            $url = $domain . '/pje/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroCnj}\";", []);

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath("//img[@id[contains(.,'captchaImg')]]")));
                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    "//img[@id[contains(.,'captchaImg')]]",
                    "//input[@id[contains(.,'verifyCaptcha')]]",
                    '//*[@id="fPP:searchProcessos"]',
                    false
                );

                $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);
                $varaField = $driver
                    ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td/table/tbody/tr/td/form/div/div/div[3]/table/tbody/tr[2]/td/span/div/div/div'))
                    ->getText();

                if ($varaField === 'Órgão Julgador') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td/table/tbody/tr/td/form/div/div/div[3]/table/tbody/tr[2]/td/span/div/div/div[2]'))
                        ->getText();
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function pje2tjes($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.08/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;

            $vara = '';
            $domain = 'https://sistemas.tjes.jus.br';
            $url = $domain . '/pje2g/ConsultaPublica/listView.seam';
            $driver->get($url);

            try {

                $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroCnj}\";", []);

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath("//img[@id[contains(.,'captchaImg')]]")));
                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    "//img[@id[contains(.,'captchaImg')]]",
                    "//input[@id[contains(.,'verifyCaptcha')]]",
                    '//*[@id="fPP:searchProcessos"]',
                    false
                );

                try {
                    $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                    $msg = $robo->cleanString($msg);
                    $robo->log($msg);
                    continue;
                } catch (\Exception $e) {
                }

                $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a')));
                $onclick = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable"]/tbody/tr/td/a'))->getAttribute('onclick');
                $popupLink = $domain . str_replace(["openPopUp('Consulta pública','", "')"], "", $onclick);

                $driver->get($popupLink);
                $varaField = $driver
                    ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td/table/tbody/tr/td/form/div/div/div[3]/table/tbody/tr[2]/td/span/div/div/div'))
                    ->getText();

                if ($varaField === 'Órgão Julgador Colegiado') {
                    $vara = $driver
                        ->findElement(WebDriverBy::xpath('//table/tbody/tr[2]/td/table/tbody/tr/td/form/div/div/div[3]/table/tbody/tr[2]/td/span/div/div/div[2]'))
                        ->getText();
                } else {
                    die('não consegui encontrar a vara');
                }

                $robo->addVara($processo, $vara, true, $driver->getCurrentURL());

            } catch (NoSuchElementException $e) {
                // não encontrei!
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (UnexpectedAlertOpenException $e) {
                // não encontrei!
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                continue;
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }
        }

        $robo->clearCache(__FUNCTION__);
        $driver->quit();
    }

    public function aplicativostjesPrimeiraInstanciaJusticaComum($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $total = count($processos);
        $robo->log("$total PROCESSOS");
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroCnj = $robo->onlyDigits($nroCnj);
            if (strlen($nroCnj) != 12 && strlen($nroCnj) != 20) continue;
//            if (!preg_match('/8.08/', $nroCnj)) continue;
//            if (!in_array(substr($nroCnj, 0, 1),['1','8'])) continue;
//            $nroCnj = '0036234-81.2013.8.08.0048';

            $vara = '';
            $captcha = '';
            $url = 'http://aplicativos.tjes.jus.br/sistemaspublicos/consulta_12_instancias/consulta_proces.cfm';
            inicio:
            $driver->get($url);

            try {

                if (count($driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/table/tbody/tr/td[2]/button')))) {
                    // as vezes acontece e o site exibir uma mensagem pedindo pra atualizar a página
                    $driver->navigate()->refresh();
                    goto inicio;
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="edNumProcesso"]'))->sendKeys($nroCnj);
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img')));
                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    '/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img',
                    '/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/input[1]',
                    '//*[@id="buPesquisar"]',
                    false
                );

                // mudo o foco da janela
                $window = $driver->getWindowHandles();
                $driver->switchTo()->window(end($window));

                if (count($driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img'))) ||
                    count($driver->findElements(WebDriverBy::xpath('//*[@id="estilo"]/div/img')))) {
                    // se o captcha for respondido incorretamente o site irá abrir uma nova com a pesquisa. site cagado!
                    // as vezes acontece tbm de ser exibida uma imagem quebrada mesmo respondendo o captcha corretamente.
                    $driver->executeScript('window.close()', []);
                    // volto o foco da janela
                    $window = $driver->getWindowHandles();
                    $driver->switchTo()->window(end($window));
                    $driver->navigate()->refresh();
                    goto inicio;
                }

                if (is_null($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))) ||
                    (!empty($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))) &&
                        $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))[0]->getText() == 'Nenhum PROCESSO encontrado com os parâmetros informados.')) {
                    $driver->executeScript('window.close()', []);
                    // volto o foco da janela
                    $window = $driver->getWindowHandles();
                    $driver->switchTo()->window(end($window));
                    $driver->navigate()->refresh();
                    throw new NoSuchElementException('Não achei o processo');
                }

                $element = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                $lines = $robo->extractVerticalTable($element, 2);

                $vara = '';
                foreach ($lines as $key => $line) {
                    if (preg_match('/Vara/i', $key)) {
                        $vara = $robo->cleanString($key,['Vara','Órgão Julgador','Órgão Atual',':']);
                        break;
                    }
                }

                if ($vara === '') {
                    die('Não foi possivel encontrar a vara ' . $nroCnj);
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

                $driver->executeScript('window.close()', []);
                // volto o foco da janela
                $window = $driver->getWindowHandles();
                $driver->switchTo()->window(end($window));
                $driver->navigate()->refresh();

            } catch (NoSuchElementException $e) {
                $robo->saveCache(__FUNCTION__, $nroCnj);
                $robo->log("PROCESSO {$nroCnj} não encontrado");
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }

        $robo->clearCache(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
    }

    public function aplicativostjesPrimeiraInstanciaJusticaEspecial($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $total = count($processos);
        $robo->log("$total PROCESSOS");
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroCnj = $robo->onlyDigits($nroCnj);
            if (strlen($nroCnj) != 12 && strlen($nroCnj) != 20) continue;
//            if (!preg_match('/8.08/', $nroCnj)) continue;
//            if (!in_array(substr($nroCnj, 0, 1),['1','8'])) continue;
//            $nroCnj = ' 0021357-88.2016.8.08.0030';

            $vara = '';
            $captcha = '';
            $url = 'http://aplicativos.tjes.jus.br/sistemaspublicos/consulta_12_instancias/consulta_proces.cfm';
            inicio:
            $driver->get($url);

            try {

                if (count($driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/table/tbody/tr/td[2]/button')))) {
                    // as vezes acontece e o site exibir uma mensagem pedindo pra atualizar a página
                    $driver->navigate()->refresh();
                    goto inicio;
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="juizo"]/table/tbody/tr/td[2]/input[2]'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="edNumProcesso"]'))->sendKeys($nroCnj);
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img')));
                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    '/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img',
                    '/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/input[1]',
                    '//*[@id="buPesquisar"]',
                    false
                );

                // mudo o foco da janela
                $window = $driver->getWindowHandles();
                $driver->switchTo()->window(end($window));

                if (count($driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img'))) ||
                    count($driver->findElements(WebDriverBy::xpath('//*[@id="estilo"]/div/img')))) {
                    // se o captcha for respondido incorretamente o site irá abrir uma nova com a pesquisa. site cagado!
                    // as vezes acontece tbm de ser exibida uma imagem quebrada mesmo respondendo o captcha corretamente.
                    $driver->executeScript('window.close()', []);
                    // volto o foco da janela
                    $window = $driver->getWindowHandles();
                    $driver->switchTo()->window(end($window));
                    $driver->navigate()->refresh();
                    goto inicio;
                }

                if (is_null($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))) ||
                    (!empty($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))) &&
                        $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))[0]->getText() == 'Nenhum PROCESSO encontrado com os parâmetros informados.')) {
                    $driver->executeScript('window.close()', []);
                    // volto o foco da janela
                    $window = $driver->getWindowHandles();
                    $driver->switchTo()->window(end($window));
                    $driver->navigate()->refresh();
                    throw new NoSuchElementException('Não achei o processo');
                }

                $element = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                $lines = $robo->extractVerticalTable($element, 2);

                $vara = '';
                foreach ($lines as $key => $line) {
                    if (preg_match('/Vara/i', $key)) {
                        $vara = $robo->cleanString($key,['Vara','Órgão Julgador','Órgão Atual',':']);
                        break;
                    }
                }

                if ($vara === '') {
                    die('Não foi possivel encontrar a vara ' . $nroCnj);
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

                $driver->executeScript('window.close()', []);
                // volto o foco da janela
                $window = $driver->getWindowHandles();
                $driver->switchTo()->window(end($window));
                $driver->navigate()->refresh();

            } catch (NoSuchElementException $e) {
                $robo->saveCache(__FUNCTION__, $nroCnj);
                $robo->log("PROCESSO {$nroCnj} não encontrado");
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }

        $robo->clearCache(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
    }

    public function aplicativostjesSegundaInstancia($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $total = count($processos);
        $robo->log("$total PROCESSOS");
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $robo->log("{$robo->percentage($total,$index)} => {$processo->getNumProcJust()}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroCnj = $robo->onlyDigits($nroCnj);
            if (strlen($nroCnj) != 12 && strlen($nroCnj) != 20) continue;
//            if (!preg_match('/8.08/', $nroCnj)) continue;
//            if (!in_array(substr($nroCnj, 0, 1),['1','8'])) continue;
//            $nroCnj = ' 0021357-88.2016.8.08.0030';

            $vara = '';
            $captcha = '';
            $url = 'http://aplicativos.tjes.jus.br/sistemaspublicos/consulta_12_instancias/consulta_proces.cfm';
            inicio:
            $driver->get($url);

            try {

                if (count($driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/table/tbody/tr/td[2]/button')))) {
                    // as vezes acontece e o site exibir uma mensagem pedindo pra atualizar a página
                    $driver->navigate()->refresh();
                    goto inicio;
                }

                $selectElement = $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="seInstancia"]'));
                $select = new \Facebook\WebDriver\WebDriverSelect($selectElement);
                $select->selectByValue('2');

                $driver->findElement(WebDriverBy::xpath('//*[@id="edNumProcesso"]'))->sendKeys($nroCnj);
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img')));
                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    '/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img',
                    '/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/input[1]',
                    '//*[@id="buPesquisar"]',
                    false
                );

                // mudo o foco da janela
                $window = $driver->getWindowHandles();
                $driver->switchTo()->window(end($window));

                if (count($driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/img'))) ||
                    count($driver->findElements(WebDriverBy::xpath('//*[@id="estilo"]/div/img')))) {
                    // se o captcha for respondido incorretamente o site irá abrir uma nova com a pesquisa. site cagado!
                    // as vezes acontece tbm de ser exibida uma imagem quebrada mesmo respondendo o captcha corretamente.
                    $driver->executeScript('window.close()', []);
                    // volto o foco da janela
                    $window = $driver->getWindowHandles();
                    $driver->switchTo()->window(end($window));
                    $driver->navigate()->refresh();
                    goto inicio;
                }

                if (is_null($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))) ||
                    (!empty($driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))) &&
                        $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[2]/td'))[0]->getText() == 'Nenhum PROCESSO encontrado com os parâmetros informados.')) {
                    $driver->executeScript('window.close()', []);
                    // volto o foco da janela
                    $window = $driver->getWindowHandles();
                    $driver->switchTo()->window(end($window));
                    $driver->navigate()->refresh();
                    throw new NoSuchElementException('Não achei o processo');
                }

                $element = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                $lines = $robo->extractVerticalTable($element, 2);

                $vara = '';
                foreach ($lines as $key => $line) {
                    if (preg_match('/Órgão Julgador/i', $key)) {
                        $vara = $robo->cleanString($key,['Órgão Julgador','Órgão Atual',':']);
                        break;
                    }
                }

                if ($vara === '') {
                    die('Não foi possivel encontrar a vara ' . $nroCnj);
                }

                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

                $driver->executeScript('window.close()', []);
                // volto o foco da janela
                $window = $driver->getWindowHandles();
                $driver->switchTo()->window(end($window));
                $driver->navigate()->refresh();

            } catch (NoSuchElementException $e) {
                $robo->saveCache(__FUNCTION__, $nroCnj);
                $robo->log("PROCESSO {$nroCnj} não encontrado");
            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }

        $robo->clearCache(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
    }

    public function addObs($obs)
    {
        if ($this->getObs() != '') {
            $this->setObs($obs . '|' .$this->getObs());
        } else {
            $this->setObs($obs);
        }
    }

}
