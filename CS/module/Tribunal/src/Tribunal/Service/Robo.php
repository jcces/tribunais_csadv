<?php
/**
 * Created by PhpStorm.
 * User: DELL VOSTRO
 * Date: 20/03/2018
 * Time: 15:35
 */

namespace Tribunal\Service;

use Estrutura\Service\Config;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\ElementNotSelectableException;
use Facebook\WebDriver\Exception\ElementNotVisibleException;
use Facebook\WebDriver\Exception\ExpectedException;
use Facebook\WebDriver\Exception\IMEEngineActivationFailedException;
use Facebook\WebDriver\Exception\IMENotAvailableException;
use Facebook\WebDriver\Exception\NoSuchDriverException;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\NoSuchWindowException;
use Facebook\WebDriver\Exception\TimeOutException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use thiagoalessio\TesseractOCR\TesseractOCR;

use \Tribunal\Entity\Tribunal as Entity;

class Robo extends Entity
{

    public $optionsOnline = ['0' => 'Offline', '1' => 'Online'];
    public $optionsColetando = ['0' => 'Não', '1' => 'Sim'];

    public $host = 'http://localhost:4444/wd/hub';
    public $logFileName = '';
    public $capabilities = [];
    public $agent = '';
    public $incognito = '';
    public $mobile = false;
    public $proxy = null; //IP:PORT

    public $lastRequestId = '';

    private $tmpDir = 'data' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;

    public function start()
    {
        $startCycle = new \DateTime();

        $tribunalService = new Tribunal();
        /** @var $tribunais \Tribunal\Service\Tribunal[] */
        $tribunais = $tribunalService->fetchAll();

        $qtdTribunais = count($tribunais);

        $this->log("{$qtdTribunais} tribunais encontrados.");

        foreach ($tribunais as $tribunal) {

//            if ($tribunal->getCallMethod() != 'tjmg1instancia') continue;
            if ($tribunal->getCallMethod() != 'tjrs1instancia') continue;

            $this->log('loading ' . $tribunal->getName());

            $tribunal->setDtUltimaColetaComecou(date('Y-m-d H:i:s'));
            $tribunal->setDtUltimaColetaTerminou(NULL);
            $tribunal->salvar(true);

            $this->logFileName = $tribunal->getCallMethod();
            $this->cleanLog($this->logFileName);
            $start = new \DateTime();
            if (!method_exists($this, $tribunal->getCallMethod())) {
                $this->log("MÉTODO {$tribunal->getCallMethod()} NÃO EXISTE!", $this->logFileName);
                continue;
            }
            $method = $tribunal->getCallMethod();
            $this->$method($tribunal);
            //VERIFICAR SE AINDA TEM PROCESSO SEM PROCESSAR
            $this->log('CHECK SE AINDA TEM PROCESSO SEM PROCESSAR...');
            $tryTimes = 1;
            // MELHORAR CHAMADA PARA QUE A FUNÇÃO APENAS LEVANTE OS PROCESSOS QUE NÃO FORAM PROCESSADOS NA PRIMEIRA LEVA.
            while ($tribunal->checkProcessosSemProcessar()) {
                $this->log("{$tryTimes}° TENTATIVA...");
                if ($tryTimes >= 3) break;
                $this->$method($tribunal, true);
                $tryTimes++;
            }

            $end = new \DateTime();
            $interval = $start->diff($end);
            $this->log('END => Running time: ' . $interval->format('%a days %h hours %i minutes %s seconds'), $this->logFileName);

            $tribunal->setDtUltimaColetaTerminou(date('Y-m-d H:i:s'));
            $tribunal->setColetando(0);
            $tribunal->salvar(true);
        }

        $this->log('Done!');
        $endCycle = new \DateTime();
        $intervalCycle = $startCycle->diff($endCycle);
        $this->cleanLog('cycle');
        $this->log("END => Running cycle time in {$qtdTribunais} scripts: " . $intervalCycle->format('%a days %h hours %i minutes %s seconds'), 'cycle');

        return true;
    }

    public function tjdf1instanciafisico($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();
        $driver->get($service->getUrl());

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        $firstTime = true;
        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            try {
                $driver->navigate()->to($link);
                if ($firstTime) {
                    $this->log("Up and running!");
                    $service->setOnline(1);
                    $service->setColetando(1);
                    $service->salvar(true);
                    $firstTime = false;
                }
            } catch (\Exception $e) {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::tagName('table')));

            $processosLinkElem = $driver->findElements(WebDriverBy::cssSelector('ul table tbody tr td:first-child a'));
            $processosLink = $this->extractLinks($processosLinkElem, true, false, $processosLink);
        }

        $processosTratados = [];
        foreach ($processosLink as $nroProcesso => $linkProcesso) {
            $processosTratados[$this->extractProcessNumber($nroProcesso, true, '/([0-9]{7}[-]?[0-9]{2}[.]?[0-9]{4}[.]?[0-9]{3}[.]?[0-9]{4})/')] = $linkProcesso;
        }

        $total = count($processosLink);
        $this->log($total . ' processo(s) encontrado(s)');

        $linksRegistrados = $service->registerProcessos($processosTratados);
        $totalLinksRegistrados = count($linksRegistrados);
        $this->log($totalLinksRegistrados . ' A PROCESSAR');

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
            $index++;

            $hashAnterior = $processo->getHash();

            try {

                $driver->navigate()->to($processo->getUrl());

                $fieldsMap = [
                    'i_codigoCircunscricao' => 'Código Circunscrição',
                    'i_nomeCircunscricao' => 'Circunscrição',
                    'i_numeroProcesso14' => 'Processo',
                    'i_dataDistribuicao' => 'Data Dist',
                    'i_numeroProcesso20' => 'Numeração Única do Processo(CNJ)',
                    'i_preferenciaTramitacao' => 'Preferência na Tramitação',
                    'i_codigoVara' => 'Código Vara',
                    'i_descricaoVara' => 'Vara',
                    'i_naturezaVara' => 'Natureza da Vara',
                    'i_enderecoVara' => 'Endereço da Vara',
                    'i_horarioVara' => 'Horário de Funcionamento da Vara',
                    'i_classeProcessual' => 'Classe',
                    'i_assuntoProcessual' => 'Assunto',
                    'i_feitoCodigo' => 'Código Feito',
                    'i_feitoDescricao' => 'Feito',
                    'i_valorCausa' => 'Valor da Causa',
                    'i_nomeAutor' => 'Requerente',
                    'i_advogadoAutor' => 'Advogado Autor',
                    'i_nomeReu' => 'Requerido',
                    'i_advogadoReu' => 'Advogado Reu',
                    'i_origem' => 'Origem',
                    'i_material' => 'Material',
                    'i_segredoJustica' => 'Seg. Justiça',
                    'i_processoApenso_1' => 'Apensado ao(s) processo(s)',
                ];

                $detalhamento = [];
                foreach ($fieldsMap as $field => $friendlyName) {
                    try {
                        $elements = $driver->findElement(WebDriverBy::id($field));
                        $detalhamento[$friendlyName] = $elements->getText();
                    } catch (NoSuchElementException $e) {
                        $detalhamento[$friendlyName] = '';
                    }
                }

                $fieldsTratados[$processo->getProcesso()]['detalhamento'] = $detalhamento;

                $linhasAndamentos = $driver->findElements(
                    WebDriverBy::cssSelector('table tbody tr')
                );

                $fieldsTratados[$processo->getProcesso()]['andamento'] = $this->extractTable($linhasAndamentos, 3, true);

                $links = $driver->findElements(
                    WebDriverBy::cssSelector('#i_competencia b a')
                );

                $linksTratados = [];
                foreach ($links as $link) {
                    if ($link->getText() == 'Receba gratuitamente os andamentos processuais, clicando aqui') break;
                    $linksTratados[$this->cleanString($link->getText())] = $link->getAttribute('href');
                }

                $aux = [];
                foreach ($linksTratados as $pageName => $link) {
                    $driver->navigate()->to($link);

                    if ($pageName == 'Consulta Advogados das Partes') {

                        $lineBreaks = explode("\n", $driver->findElement(WebDriverBy::xpath('/html/body'))->getText());
                        $autor = false;
                        $reu = false;
                        $text = '';
                        foreach ($lineBreaks as $lineBreak) {
                            $lineBreak = $this->cleanString($lineBreak);
                            if (in_array($lineBreak, ['', ' ', 'voltar'])) continue;
                            if (preg_match('/Circunscrição :|Processo :|Vara :|Acesso via INTERNET/', $lineBreak)) continue;
                            if (preg_match('/AUTOR :/', $lineBreak)) {
                                $autor = true;
                                $text = $lineBreak;
                                $reu = false;
                                continue;
                            }
                            if (preg_match('/REU :/', $lineBreak)) {
                                $reu = true;
                                $text = $lineBreak;
                                $autor = false;
                                continue;
                            }
                            if ($autor) $aux[$pageName][$text][] = $lineBreak;
                            if ($reu) $aux[$pageName][$text][] = $lineBreak;
                        }
                        $fieldsTratados[$processo->getProcesso()][$pageName] = $aux[$pageName];

                    }

                    if ($pageName == 'Consulta Inspeção') {

                        $lineBreaks = explode("\n", $driver->findElement(WebDriverBy::xpath('/html/body'))->getText());
                        foreach ($lineBreaks as $lineBreak) {
                            $lineBreak = $this->cleanString($lineBreak);
                            if (in_array($lineBreak, ['', ' ', 'voltar'])) continue;
                            if (preg_match('/Inspe|Circunscrição :|Processo :|Vara :|AUTOR :|REU :|Acesso via INTERNET/', $lineBreak)) continue;
                            $aux[$pageName][] = $lineBreak;
                        }
                        $odd = [];
                        $even = [];
                        foreach ($aux[$pageName] as $k => $v) {
                            if ($k % 2 == 0) {
                                $even[] = $v;
                            } else {
                                $odd[] = $v;
                            }
                        }
                        $aux[$pageName] = [];
                        foreach ($even as $i => $v) {
                            $aux[$pageName][] = $v . '. ' . $odd[$i];
                        }
                        $fieldsTratados[$processo->getProcesso()][$pageName] = $aux[$pageName];
                    }

                    if ($pageName == 'Consulta Pautas Publicadas') {

                        $linksPautasPublicadasAux = $driver->findElements(WebDriverBy::xpath('/html/body/font/font/a'));
                        $linksPautasPublicadas = [];
                        foreach ($linksPautasPublicadasAux as $link) {
                            $linksPautasPublicadas[] = $link->getAttribute('href');
                        }
                        foreach ($linksPautasPublicadas as $link) {
                            $driver->navigate()->to($link);
                            $lineBreaks = explode("\n", $driver->findElement(WebDriverBy::xpath('/html/body'))->getText());
                            $auxAux = [];
                            foreach ($lineBreaks as $lineBreak) {
                                $lineBreak = $this->cleanString($lineBreak);
                                if (in_array($lineBreak, ['', ' ', 'voltar'])) continue;
                                if (preg_match('/Circunscrição :|Processo :|Acesso via INTERNET/', $lineBreak)) continue;
                                $columnName = '';

                                $nameValue = explode(' : ', $lineBreak);
                                if (count($nameValue) < 2) continue;
                                $columnName = $nameValue[0];
                                $lineBreak = $nameValue[1];

                                if ($columnName) $auxAux[$columnName] = $lineBreak;
                            }
                            $aux[$pageName][] = $auxAux;
                        }

                        $fieldsTratados[$processo->getProcesso()][$pageName] = $aux[$pageName];
                    }

                    if ($pageName == 'Consulta Mandados via Oficial de Justiça') {

                        $lineBreaks = explode("\n", $driver->findElement(WebDriverBy::xpath('/html/body'))->getText());
                        $auxAux = [];
                        foreach ($lineBreaks as $lineBreak) {
                            $lineBreak = $this->cleanString($lineBreak);
                            if (in_array($lineBreak, [' ', 'voltar'])) continue;
                            if (preg_match('/Movimentação dos Mandados|Circunscrição :|Processo :|Vara :|Acesso via INTERNET/', $lineBreak)) continue;

                            if ($lineBreak == '' && count($auxAux)) {
                                $aux[$pageName][] = $auxAux;
                                $auxAux = [];
                            }

                            $columnName = '';
                            $nameValue = explode(' : ', $lineBreak);
                            if (count($nameValue) < 2) continue;
                            $columnName = $nameValue[0];
                            $lineBreak = $nameValue[1];

                            if ($columnName) $auxAux[$columnName] = $lineBreak;
                        }

                        $fieldsTratados[$processo->getProcesso()][$pageName] = $aux[$pageName];
                    }

                    if ($pageName == 'Lista de processos aptos para julgamento na vara') {
                    }

                    if ($pageName == 'Consulta Custas Iniciais' || $pageName == 'Consulta Custas Finais') {

                        $lineBreaks = explode("\n", $driver->findElement(WebDriverBy::xpath('/html/body'))->getText());
                        $auxAux = [];
                        foreach ($lineBreaks as $lineBreak) {
                            $lineBreak = $this->cleanString($lineBreak);
                            if (in_array($lineBreak, ['', ' ', 'voltar'])) continue;
                            if (preg_match('/Movimentação dos Mandados|Circunscrição :|Processo :|Vara :|Acesso via INTERNET/', $lineBreak)) continue;

                            $columnName = '';
                            $nameValue = explode(' : ', $lineBreak);
                            if (count($nameValue) < 2) continue;
                            $columnName = $nameValue[0];
                            $lineBreak = $nameValue[1];

                            if ($columnName) $auxAux[$columnName] = $lineBreak;
                        }

                        $aux[$pageName] = $auxAux;
                        $fieldsTratados[$processo->getProcesso()][$pageName] = $aux[$pageName];
                    }

                    if (preg_match('/Número do Agravo de Instrumento/', $pageName)) {

                        $tableGeral = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr'));
                        $aux = $this->extractVerticalTable($tableGeral, 2, true);
                        $fieldsTratados[$processo->getProcesso()][$pageName]['Andamentos Gerais']['Dados Gerais'] = $aux;
                        $tableAndamento = $driver->findElements(WebDriverBy::xpath('//*[@id="studio"]/tbody/tr'));
                        $aux = $this->extractTable($tableAndamento, 2, true);
                        $fieldsTratados[$processo->getProcesso()][$pageName]['Andamentos Gerais']['Andamentos'] = $aux;

                        $linkTaquigraficas = false;
                        try {
                            $linkTaquigraficas = $driver->findElement(WebDriverBy::xpath('/html/body/table/tbody/tr[14]/td/p/a'))->getAttribute('href');
                        } catch (\Exception $e) {
                        }

                        if ($linkTaquigraficas) {
                            $driver->navigate()->to($linkTaquigraficas);
                            $tableGeral = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr'));
                            $aux = $this->extractVerticalTable($tableGeral, 2, true);
                            $fieldsTratados[$processo->getProcesso()][$pageName]['Notas Taquigráficas']['Dados Gerais'] = $aux;
                            $tableAndamento = $driver->findElements(WebDriverBy::xpath('//*[@id="studio"]/tbody/tr'));
                            $aux = $this->extractTable($tableAndamento, 2, true);
                            $fieldsTratados[$processo->getProcesso()][$pageName]['Notas Taquigráficas']['Andamentos'] = $aux;
                        }
                    }
                }

            } catch (\Exception $e) {
                $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
            }

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjdf2instanciafisico($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        $firstTime = true;
        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            try {
                $driver->navigate()->to($link);
                if ($firstTime) {
                    $this->log("Up and running!");
                    $service->setOnline(1);
                    $service->setColetando(1);
                    $service->salvar(true);
                    $firstTime = false;
                }
            } catch (\Exception $e) {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::tagName('table')));

            $processosLinkElem = $driver->findElements(WebDriverBy::cssSelector('table tbody tr td:first-child a'));
            $processosLink = $this->extractLinks($processosLinkElem, true, false, $processosLink);
        }

        $processosTratados = [];
        foreach ($processosLink as $nroProcesso => $linkProcesso) {
            $processosTratados[$this->extractProcessNumber($nroProcesso, true, '/([0-9]{7}[-]?[0-9]{2}[.]?[0-9]{4}[.]?[0-9]{3}[.]?[0-9]{4})/')] = $linkProcesso;
        }

        $total = count($processosLink);
        $this->log($total . ' processo(s) encontrado(s)');

        $linksRegistrados = $service->registerProcessos($processosTratados);
        $totalLinksRegistrados = count($linksRegistrados);
        $this->log($totalLinksRegistrados . ' A PROCESSAR');

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
            $index++;

            $hashAnterior = $processo->getHash();

            try {

                $driver->navigate()->to($processo->getUrl());

                $tableDetalhamento = $driver->findElements(WebDriverBy::xpath("/html/body/table/tbody/tr"));
                $fieldsTratados = $this->extractVerticalTable($tableDetalhamento, 2, true);

                $linhasAndamentos = $driver->findElements(WebDriverBy::xpath('//*[@id="studio"]/tbody/tr'));
                $andamentoTratados = $this->extractTable($linhasAndamentos, 2, true);


                $linksAux = [];
                try {
                    $linksAux = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr/td[1]/p/a'));
                } catch (\Exception $e) {
                }

                $links = [];
                foreach ($linksAux as $link) {
                    if (preg_match('/javascript|#/', $link->getAttribute('href'))) continue;
                    $links[$link->getText()] = $link->getAttribute('href');
                }

                $decisao = [];
                $notas = [];
                foreach ($links as $textLink => $href) {

                    if ($textLink == 'Decisão') {
                        $driver->navigate()->to($href);
                        $decisaoGeral = [];
                        $decisaoDespacho = [];
                        try {
                            $tableA = $driver->findElements(WebDriverBy::xpath('/html/body/table[1]/tbody/tr'));
                            $decisaoGeral = $this->extractVerticalTable($tableA, 2);
                            $tableB = $driver->findElements(WebDriverBy::xpath('/html/body/table[2]/tbody/tr'));
                            $decisaoDespacho = $this->extractTable($tableB, 2);
                        } catch (NoSuchElementException $e) {
                        }

                        $decisao = [
                            'geral' => $decisaoGeral,
                            'despacho' => $decisaoDespacho,
                        ];
                        continue;
                    }

                    if ($textLink == 'Notas Taquigráficas') {
                        $driver->navigate()->to($href);
                        $notasGeral = [];
                        $notasAndamentos = [];
                        try {
                            $tableA = $driver->findElements(WebDriverBy::xpath("/html/body/table/tbody/tr"));
                            $notasGeral = $this->extractVerticalTable($tableA, 2, true);
                            $tableB = $driver->findElements(WebDriverBy::xpath('//*[@id="studio"]/tbody/tr'));
                            $notasAndamentos = $this->extractTable($tableB, 2, true);
                        } catch (NoSuchElementException $e) {
                        }

                        $notas = [
                            'geral' => $notasGeral,
                            'andamento' => $notasAndamentos,
                        ];
                        continue;
                    }

                }

                $fieldsTratados[$processo->getProcesso()] = [
                    'detalhamento' => $fieldsTratados,
                    'andamento' => $andamentoTratados,
                    'decisao' => $decisao,
                    'notas_taquigraficas' => $notas,
                ];

            } catch (\Exception $e) {
                $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
            }

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjgo1instanciafisico($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();
        $driver->get('http://sv-natweb-p00.tjgo.jus.br/');

//        $words = $service->getTodasPalavras(false);
//        $partesLinks = [];
//        $firstTime = true;
//        foreach ($words as $word) {
//            try {
//                $driver->navigate()->to($service->getUrl());
//                if ($firstTime) {
//                    $this->log("Up and running!");
//                    $service->setOnline(1);
//                    $service->setColetando(1);
//                    $service->salvar(true);
//                    $firstTime = false;
//                }
//            } catch (\Exception $e) {
//                $service->setOnline(0);
//                $service->setColetando(0);
//                $service->salvar(true);
//                continue;
//            }
//
//            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('input[name=nomepessoa]')));
//            $driver->findElement(WebDriverBy::cssSelector('input[name=nomepessoa]'))->sendKeys($word);
//            $driver->findElement(WebDriverBy::xpath('//input[@value="Consultar"]'))->click();
//
//            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr/td[1]/a')));
//            $partes = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr/td[1]/a'));
//            $partesLinks = $this->extractLinks($partes, false, false, $partesLinks);
//
//            while ($proximo = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p/a')) {
//
//                $driver->navigate()->to($proximo);
//
//                try {
//                    $driver->wait(4)->until(
//                        WebDriverExpectedCondition::alertIsPresent(),
//                        'Waiting for an alert'
//                    );
//                    $driver->switchTo()->alert()->accept();
//                    break;
//                } catch (\Exception $e) {
//                    $partes = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr/td[1]/a'));
//                    $partesLinks = $this->extractLinks($partes, false, false, $partesLinks);
//                }
//            }
//
//            //Interior de Goias
//            $driver->navigate()->to($service->getUrl());
//            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="container"]/form/div/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td[1]/input[2]')));
//            $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/form/div/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td[1]/input[2]'))->click();
//            $driver->findElement(WebDriverBy::cssSelector('input[name=nomepessoa]'))->sendKeys($word);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/form/div/table[2]/tbody/tr/td/input'))->click();
//
//            $partes = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr/td[1]/a'));
//            $partesLinks = $this->extractLinks($partes, false, false, $partesLinks);
//
//            while ($proximo = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p/a')) {
//
//                $driver->navigate()->to($proximo);
//
//                try {
//                    $driver->wait(4)->until(
//                        WebDriverExpectedCondition::alertIsPresent(),
//                        'Waiting for an alert'
//                    );
//                    $driver->switchTo()->alert()->accept();
//                    $proximos = false;
//                    break;
//                } catch (\Exception $e) {
//                    $partes = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr/td[1]/a'));
//                    $partesLinks = $this->extractLinks($partes, false, false, $partesLinks);
//                }
//            }
//
//        }
//
//        $processosLink = [];
//        foreach ($partesLinks as $parteLink) {
//            $driver->navigate()->to($parteLink);
//            try {
//                $driver->wait(3)->until(
//                    WebDriverExpectedCondition::alertIsPresent(),
//                    'Esperando alerta no link ' . $parteLink
//                );
//                $driver->switchTo()->alert()->accept();
//                continue;
//            } catch (\Exception $e) {
//                $linksProcessos = $driver->findElements(WebDriverBy::cssSelector("table tbody tr td.tabelas_conteudo:first-child a"));
//                $processosLink = $this->extractLinks($linksProcessos, true, false, $processosLink);
//                //proximos
//                while ($proximo = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p/a')) {
//
//                    $driver->navigate()->to($proximo);
//
//                    try {
//                        $driver->wait(4)->until(
//                            WebDriverExpectedCondition::alertIsPresent(),
//                            'Waiting for an alert'
//                        );
//                        $driver->switchTo()->alert()->accept();
//                        $proximos = false;
//                        break;
//                    } catch (\Exception $e) {
//                        $linksProcessos = $driver->findElements(WebDriverBy::cssSelector("table tbody tr td.tabelas_conteudo:first-child a"));
//                        $processosLink = $this->extractLinks($linksProcessos, true, false, $processosLink);
//                    }
//                }
//            }
//        }
//
//        $processosTratados = [];
//        foreach ($processosLink as $nroText => $processosLinkItem) {
////            $processosTratados[$this->extractProcessNumber($nroText,true,'\((.*?)\)')] = $processosLinkItem;
//            $processosTratados[$nroText] = $processosLinkItem;
//        }
//
//        $total = count($processosLink);
//        $this->log($total . ' processo(s) encontrado(s)');
//
//        $linksRegistrados = $service->registerProcessos($processosTratados);
//        $totalLinksRegistrados = count($linksRegistrados);
//        $this->log($totalLinksRegistrados . ' A PROCESSAR');

        $linksRegistrados = $service->getTodosProcessos(0); //TEMPORARIO
        $totalLinksRegistrados = count($linksRegistrados); //TEMPORARIO

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
            $index++;

            $hashAnterior = $processo->getHash();


            try {
                $driver->navigate()->to($processo->getUrl());

                $driver->wait(60)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.g-recaptcha'))
                );
                $dataSitekey = $driver->findElement(WebDriverBy::cssSelector('div.g-recaptcha'));
                $dataSitekeyHash = $dataSitekey->getAttribute('data-sitekey');

                if (!$dataSitekeyHash) throw new \Exception('Token não encontrato.');

                $captcha = new TwoCaptcha();
//                        $response = $captcha->sendReCaptcha($dataSitekeyHash, $itemATagHref);
                $response = $dataSitekeyHash; //Erro do site do TJGO. ReCaptcha bugado =)
                $driver = $captcha->insertReCaptcha($driver, $response);

                try {
                    $driver->wait(2)->until(
                        WebDriverExpectedCondition::alertIsPresent(),
                        'Waiting for an alert'
                    );
                    $alertText = $driver->switchTo()->alert()->getText();
                    $msg = ['Mensagem' => $alertText];
                    $processo->setDados(json_encode($msg));
                    $processo->setProcessado('1');
                    $processo->salvar(true);
                    $driver->switchTo()->alert()->accept();
                    continue;

//                    if (preg_match('/NAO DISPONIVEL/', $alertText)) {
//                        $driver->switchTo()->alert()->accept();
//                        continue;
//                    }
//                    if (preg_match('/PROCESSO BAIXADO/', $alertText)) {
//                        $driver->switchTo()->alert()->accept();
//                        continue;
//                    }
//                    if (preg_match('/ ARQUIVADO/', $alertText)) {
//                        $driver->switchTo()->alert()->accept();
//                        continue;
//                    }
//                    $driver->switchTo()->alert()->accept();
                } catch (\Exception $e) {
                }

                $driver->wait(60)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                );

                $menu = $driver->findElements(WebDriverBy::cssSelector("table tbody tr.destaque td a"));
                $menuTratado = $this->extractLinks($menu, true);

                $rowsPrincipal = $driver->findElements(WebDriverBy::xpath("//*[@id='container']/table/tbody/tr"));

                $header = array_shift($rowsPrincipal);
                $headerColunas = $header->findElements(WebDriverBy::cssSelector("td"));
                while (count($headerColunas) < 2) {
                    $headerB = array_shift($rowsPrincipal);
                    $headerColunas = $headerB->findElements(WebDriverBy::cssSelector("td"));
                }

                $auxPrincipal = [];
                foreach ($rowsPrincipal as $row) {
                    $colunas = $row->findElements(WebDriverBy::cssSelector("td"));
                    $auxPrincipal[$this->cleanString($colunas[0]->getText())] = $this->cleanString($colunas[1]->getText());
                }

                $fieldsTratados[$processo->getProcesso()]['Principal'] = $auxPrincipal;

                //ENTRANDO EM CADA ABA DO PROCESSO
                foreach ($menuTratado as $name => $href) {

                    try {
                        sleep(2);
                        $driver->navigate()->to($href);

                        $driver->wait(4)->until(
                            WebDriverExpectedCondition::alertIsPresent(),
                            'Esperando alerta na aba ' . $name
                        );
                        $driver->switchTo()->alert()->accept();
                        continue;
                    } catch (\Exception $e) {

                        //Partes tab
                        if (preg_match('/Partes/', $name)) {
                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Partes'] = [];

                            $driver->wait(30)->until(
                                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                            );

                            $linksElements = $driver->findElements(WebDriverBy::xpath("//*[@id=\"container\"]/table[2]/tbody/tr/td[1]/a"));
                            $linksPartes = $this->extractLinks($linksElements);

                            $partes = [];
                            foreach ($linksPartes as $link) {
                                $driver->navigate()->to($link);
                                $driver = $this->closeAlert($driver);

                                $rowsParte = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table[2]/tbody/tr'));
                                $partes[] = $this->extractVerticalTable($rowsParte, 2, true);
                            }

                            $fieldsTratados[$processo->getProcesso()]['Partes'] = $partes;
                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Partes']) . ' Partes');
                        }

                        //Interlocutorias tab
                        if (preg_match('/Interlocutorias/', $name)) {
                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Interlocutorias'] = [];
                            try {
                                $driver->wait(4)->until(
                                    WebDriverExpectedCondition::alertIsPresent(),
                                    'Esperando alerta na aba ' . $name
                                );
                                $driver->switchTo()->alert()->accept();
                                continue;
                            } catch (\Exception $e) {

                                $linksRows = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/p[1]/a'));
                                $links = $this->extractLinks($linksRows);

                                while ($proximo = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p[2]/a')) {

                                    $driver->navigate()->to($proximo);
                                    $driver = $this->closeAlert($driver);

                                    $linksRows = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/p[1]/a'));
                                    foreach ($linksRows as $link) {
                                        $links[] = $link->getAttribute('href');
                                    }
                                }

                                $aux = [];
                                foreach ($links as $link) {
                                    $driver->navigate()->to($link);
                                    $driver = $this->closeAlert($driver);

                                    $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table[2]/tbody/tr'));
                                    $aux[] = $this->extractVerticalTable($rows, 2, true);
                                }

                                $fieldsTratados[$processo->getProcesso()]['Interlocutorias'] = $aux;
                            }

                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Interlocutorias']) . ' Interlocutorias');
                        }

                        //Mandatos tab
                        if (preg_match('/Mandados/', $name)) {

                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Mandados'] = [];

                            $driver->wait(60)->until(
                                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                            );

                            $linksElements = $driver->findElements(WebDriverBy::xpath("//*[@id=\"container\"]/table[2]/tbody/tr/td[1]/a"));
                            $linksMandados = $this->extractLinks($linksElements);

                            $mandados = [];
                            foreach ($linksMandados as $linkMandado) {
                                $driver->navigate()->to($linkMandado);
                                $driver = $this->closeAlert($driver);

                                $mandadoRows = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table[2]/tbody/tr'));
                                $mandados[] = $this->extractVerticalTable($mandadoRows, 2, true);
                            }

                            $fieldsTratados[$processo->getProcesso()]['Mandados'] = $mandados;
                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Mandados']) . ' Mandatos');
                        }

                        //Histórico tab
                        if (preg_match('/Hist/', $name)) {

                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Histórico'] = [];

                            $textoHistorico = $driver->findElement(WebDriverBy::cssSelector('p.tabelas_conteudo'))->getText();
                            $textoHistorico = preg_replace('/\s+/', ' ', $textoHistorico);
                            $histArr = preg_split('([0-9]{2}[/]?[0-9]{2}[/]?[0-9]{4})', $textoHistorico, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                            preg_match_all('([0-9]{2}[/]?[0-9]{2}[/]?[0-9]{4})', $textoHistorico, $datas);

                            $aux = [];
                            foreach ($histArr as $histArrkey => $histArrItem) {
                                $aux[] = $datas[0][$histArrkey] . $histArrItem;
                            }

                            while ($linkNext = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p[2]/a')) {
                                $driver->navigate()->to($linkNext);
                                $driver = $this->closeAlert($driver);

                                $textoHistorico = $driver->findElement(WebDriverBy::cssSelector('p.tabelas_conteudo'))->getText();
                                $textoHistorico = preg_replace('/\s+/', ' ', $textoHistorico);
                                $histArr = preg_split('([0-9]{2}[/]?[0-9]{2}[/]?[0-9]{4})', $textoHistorico, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                                preg_match_all('([0-9]{2}[/]?[0-9]{2}[/]?[0-9]{4})', $textoHistorico, $datas);

                                foreach ($histArr as $histArrkey => $histArrItem) {
                                    $aux[] = $datas[0][$histArrkey] . $histArrItem;
                                }
                            }

                            foreach ($aux as $item) {
                                $fieldsTratados[$processo->getProcesso()]['Histórico'][] = $this->cleanString($item);
                            }

                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Histórico']) . ' Histórico');

                        }

                        //Sentenças tab
                        if (preg_match('/Senten/', $name)) {
                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Sentenças'] = [];

                            $linksSentencas = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table[2]/tbody/tr/td[1]/a'));
                            $links = $this->extractLinks($linksSentencas);

                            $sentencas = [];
                            foreach ($links as $link) {
                                $aux = [];
                                $driver->navigate()->to($link);
                                $driver = $this->closeAlert($driver);

                                $paragraphs = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/p'));
                                foreach ($paragraphs as $paragraph) {
                                    $hasLink = false;
                                    try {
                                        $hasLink = $paragraph->findElement(WebDriverBy::cssSelector('a'))->getAttribute('href');
                                    } catch (\Exception $e) {
                                        $hasLink = false;
                                    }
                                    $aux[$this->cleanString($paragraph->getAttribute('class'))] = ($hasLink) ? "{$this->cleanString($paragraph->getText())} [Link => {$hasLink}]" : $this->cleanString($paragraph->getText());
                                }
                                $sentencas[] = $aux;
                            }

                            $fieldsTratados[$processo->getProcesso()]['Sentenças'] = $sentencas;
                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Sentenças']) . ' Sentenças');
                        }

                        //Intimações tab
                        if (preg_match('/Intima/', $name)) {
                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Intimações'] = [];

                            $driver->wait(60)->until(
                                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                            );
                            $linksIntimacoes = $driver->findElements(WebDriverBy::xpath("//*[@id=\"container\"]/table[2]/tbody/tr/td/a"));
                            $links = $this->extractLinks($linksIntimacoes);

                            $aux = [];
                            foreach ($links as $link) {
                                $driver->navigate()->to($link);
                                $driver = $this->closeAlert($driver);

                                $rows = $driver->findElements(WebDriverBy::xpath("//*[@id=\"container\"]/table[2]/tbody/tr"));
                                $aux[] = $this->extractVerticalTable($rows);
                            }

                            $fieldsTratados[$processo->getProcesso()]['Intimações'] = $aux;
                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Intimações']) . ' Intimações');
                        }

                        //Ligações tab
                        if (preg_match('/Liga/', $name)) {
                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Ligações'] = [];

                            $driver->wait(60)->until(
                                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                            );
                            $rowsLigacoes = $driver->findElements(WebDriverBy::xpath("//*[@id=\"container\"]/table[2]/tbody/tr"));
                            $auxLigacoes = $this->extractVerticalTable($rowsLigacoes, 2, true);

                            $fieldsTratados[$processo->getProcesso()]['Ligações'] = $auxLigacoes;
                            $this->log('Tem Ligações');
                        }

                        //Redistribuições tab
                        if (preg_match('/Redistribui/', $name)) {
                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Redistribuições'] = [];

                            $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/ul/table/tbody/tr'));
                            $redistribuicao = $this->extractVerticalTable($rows, 2);
                            $fieldsTratados[$processo->getProcesso()]['Redistribuições'] = $redistribuicao;
                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Redistribuições']) . ' Redistribuições');
                        }

                    }

                }

            } catch (\Exception $e) {
                $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                continue;
            }


            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjgo2instanciafisico($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        $driver->get('http://sv-natweb-p00.tjgo.jus.br/');

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        $firstTime = true;
        foreach ($words as $word) {
            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            try {
                $driver->navigate()->to($link);
                if ($firstTime) {
                    $this->log("Up and running!");
                    $service->setOnline(1);
                    $service->setColetando(1);
                    $service->salvar(true);
                    $firstTime = false;
                }
            } catch (\Exception $e) {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $partesLinksElem = $driver->findElements(WebDriverBy::cssSelector('table tbody tr td:first-child a'));
            $partesLinks = $this->extractLinks($partesLinksElem);

            foreach ($partesLinks as $parteLink) {

                try {
                    $driver->navigate()->to($parteLink);
                    $driver->wait(3)->until(
                        WebDriverExpectedCondition::alertIsPresent(),
                        'Esperando alerta no link ' . $parteLink
                    );
                    $driver->switchTo()->alert()->accept();
                    continue;
                } catch (\Exception $e) {
                    $xpath = '//*[@id="container"]/font/table/tbody/tr/td[1]/a';
                    $processosElem = $driver->findElements(WebDriverBy::xpath($xpath));
                    $processosLink = $this->extractLinks($processosElem, true, false, $processosLink);

                    while ($proximo = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p/a')) {

                        $driver->navigate()->to($proximo);

                        try {
                            $driver->wait(4)->until(
                                WebDriverExpectedCondition::alertIsPresent(),
                                'Waiting for an alert'
                            );
                            $driver->switchTo()->alert()->accept();
                            break;
                        } catch (\Exception $e) {
                            $processosElem = $driver->findElements(WebDriverBy::xpath($xpath));
                            $processosLink = $this->extractLinks($processosElem, true, false, $processosLink);
                        }
                    }
                }
            }
        }

        $processosTratados = [];
        foreach ($processosLink as $nroText => $processosLinkItem) {
            $processosTratados[$this->extractProcessNumber($nroText, true, '\((.*?)\)')] = $processosLinkItem;
        }

        $total = count($processosLink);
        $this->log($total . ' processo(s) encontrado(s)');

        $linksRegistrados = $service->registerProcessos($processosTratados);
        $totalLinksRegistrados = count($linksRegistrados);
        $this->log($totalLinksRegistrados . ' A PROCESSAR');

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
            $index++;

            $hashAnterior = $processo->getHash();

            try {
                $driver->navigate()->to($processo->getUrl());

                $driver->wait(60)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.g-recaptcha'))
                );
                $dataSitekey = $driver->findElement(WebDriverBy::cssSelector('div.g-recaptcha'));
                $dataSitekeyHash = $dataSitekey->getAttribute('data-sitekey');

                if (!$dataSitekeyHash) throw new \Exception('Token não encontrato.');

                $captcha = new TwoCaptcha();
//                        $response = $captcha->sendReCaptcha($dataSitekeyHash, $itemATagHref);
                $response = $dataSitekeyHash; //Erro do site do TJGO. ReCaptcha bugado =)
                $driver = $captcha->insertReCaptcha($driver, $response);
            } catch (\Exception $e) {
                $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                continue;
            }

            try {

                try {
                    $driver->wait(2)->until(
                        WebDriverExpectedCondition::alertIsPresent(),
                        'Waiting for an alert'
                    );
                    if (preg_match('/NAO DISPONIVEL/', $driver->switchTo()->alert()->getText())) {
                        $driver->switchTo()->alert()->accept();
                        continue;
                    }
                    $driver->switchTo()->alert()->accept();
                } catch (\Exception $e) {
                }

                $driver->wait(60)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                );

                $menu = $driver->findElements(WebDriverBy::cssSelector("table tbody tr.destaque td a"));
                $menuTratado = $this->extractLinks($menu, true);

                $rowsPrincipal = $driver->findElements(WebDriverBy::cssSelector("table:first-of-type tbody tr"));

                $trash = array_shift($rowsPrincipal);
                $header = array_shift($rowsPrincipal);
                $headerColunas = $header->findElements(WebDriverBy::cssSelector("td"));
                preg_match('/([0-9]{12})/', $headerColunas[1]->getText(), $m);
                $processoNro = $m[1];

                $this->log($processo->getProcesso());

                $auxPrincipal = $this->extractVerticalTable($rowsPrincipal);
                $fieldsTratados[$processo->getProcesso()]['Principal'] = $auxPrincipal;

                //ENTRANDO EM CADA ABA DO PROCESSO
                foreach ($menuTratado as $name => $href) {

                    $driver->navigate()->to($href);

                    try {
                        $driver->wait(4)->until(
                            WebDriverExpectedCondition::alertIsPresent(),
                            'Esperando alerta na aba ' . $name
                        );
                        $driver->switchTo()->alert()->accept();
                    } catch (\Exception $e) {

                        //Histórico tab
                        if (preg_match('/Hist/', $name)) {

                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Histórico'] = [];
                            $tableHistoricoAuxLinks = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table[2]/tbody/tr/td/a'));

                            $tableHistoricoLinks = $this->extractLinks($tableHistoricoAuxLinks);

                            while ($next = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p/a')) {
                                $driver->navigate()->to($next);
                                $links = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table[2]/tbody/tr/td/a'));
                                $tableHistoricoLinks = $this->extractLinks($links, false, false, $tableHistoricoLinks);
                            }

                            $aux = [];
                            foreach ($tableHistoricoLinks as $link) {
                                $driver->navigate()->to($link);
                                $rows = $this->extractVerticalTable($driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr')));

                                $complemento = [];
                                try {
                                    $complementolink = $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/span/p/table/tbody/tr/td/a'))->getAttribute('href');
                                    $driver->navigate()->to($complementolink);
                                    $complementoRows = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr'));
                                    $complemento = $this->extractVerticalTable($complementoRows);

                                } catch (NoSuchElementException $e) {
                                }

                                $aux[] = array_merge($complemento, $rows);
                            }

                            $this->log(count($aux) . ' Histórico');
                            $fieldsTratados[$processo->getProcesso()]['Histórico'] = $aux;

                        }

                        //Distribuições tab
                        if (preg_match('/Distribui/', $name)) {

                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Distribuições'] = [];

                            $tableDistribuicao = [];
                            $tableDistribuicaoAux = $driver->findElements(WebDriverBy::cssSelector('table:nth-of-type(2) tbody tr'));
                            $trash = array_shift($tableDistribuicaoAux);
                            $tableDistribuicaoHeaderAux = array_shift($tableDistribuicaoAux);
                            $tableDistribuicaoHeader = $tableDistribuicaoHeaderAux->findElements(WebDriverBy::cssSelector("td"));
                            $aux = [];
                            foreach ($tableDistribuicaoAux as $indexKey => $item) {
                                $colunas = $item->findElements(WebDriverBy::cssSelector("td"));
                                $aux[$this->cleanString($tableDistribuicaoHeader[0]->getText())] = $this->cleanString($colunas[0]->getText());
                                $aux[$this->cleanString($tableDistribuicaoHeader[1]->getText())] = $this->cleanString($colunas[1]->getText());
                                $aux[$this->cleanString($tableDistribuicaoHeader[2]->getText())] = $this->cleanString($colunas[2]->getText());

                                $tableDistribuicao[] = $aux;
                            }

                            $this->log(count($tableDistribuicao) . ' Distribuições');
                            $fieldsTratados[$processo->getProcesso()]['Distribuições'] = $tableDistribuicao;
                        }

                        //Petições tab
                        if (preg_match('/Peti/', $name)) {

                            $fieldsTratados[$processo->getProcesso()]['Petições'] = [];

                            $peticoes = [];
                            $linksElem = $driver->findElements(WebDriverBy::cssSelector('table:nth-of-type(2) tbody tr td:nth-of-type(1) a'));
                            $links = $this->extractLinks($linksElem);

                            foreach ($links as $link) {
                                $driver->navigate()->to($link);

                                $tablePeticaoSelect = $driver->findElements(WebDriverBy::cssSelector('table tbody tr'));

                                $aux = $this->extractVerticalTable($tablePeticaoSelect);

                                $peticoes[] = $aux;
                            }

                            $this->log(count($peticoes) . ' Petições');
                            $fieldsTratados[$processo->getProcesso()]['Petições'] = $peticoes;
                        }

                        //Decisão tab
                        if (preg_match('/Decis/', $name)) {

                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Decisão'] = [];

                            $decisao = [];
                            $linksElem = $driver->findElements(WebDriverBy::cssSelector('table:nth-of-type(2) tbody tr td:first-child a'));
                            $links = $this->extractLinks($linksElem);

                            foreach ($links as $link) {
                                $driver->navigate()->to($link);

                                $tableDecisaoSelect = $driver->findElements(WebDriverBy::cssSelector('table:nth-of-type(1) tbody tr'));

                                $aux = $this->extractVerticalTable($tableDecisaoSelect);
//                                $aux = [];
//                                /** @var $rows \Facebook\WebDriver\Remote\RemoteWebDriver[] */
//                                foreach ($tableDecisaoSelect as $row) {
//                                    $colunas = $row->findElements(WebDriverBy::cssSelector("td"));
//                                    if (count($colunas) < 2) continue;
//                                    $link = '';
//                                    try {
//                                        $link = $colunas[1]->findElement(WebDriverBy::cssSelector('a'))->getAttribute('href');
//                                    } catch (NoSuchElementException $e) {
//                                    }
//
//                                    $text = str_replace('INTEIRO TEOR', '', $this->cleanString($colunas[1]->getText())) . ' ' . $link;
//                                    $aux[$this->cleanString($colunas[0]->getText())] = $text;
//                                }

                                try {
                                    $tableDecisaoVotantesLink = $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/p/table/tbody/tr/td/div/a'))->getAttribute('href');
                                    $driver->navigate()->to($tableDecisaoVotantesLink);
                                    $tableDecisaoVotantes = $driver->findElements(WebDriverBy::cssSelector('table tbody tr'));
                                    $aux = array_merge($aux, $this->extractVerticalTable($tableDecisaoVotantes));
                                } catch (NoSuchElementException $e) {
                                }

                                $decisao[] = $aux;
                            }

                            $this->log(count($decisao) . ' Decisão');
                            $fieldsTratados[$processo->getProcesso()]['Decisão'] = $decisao;
                        }

                        //Partes tab
                        if (preg_match('/Partes/', $name)) {

                            $partes = [];
                            $fieldsTratados[$processo->getProcesso()]['Partes'] = [];


                            $driver->wait(30)->until(
                                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                            );

                            $feito = $this->cleanString($driver->findElement(WebDriverBy::xpath("//*[@id=\"container\"]/table[1]/tbody/tr[2]/td[2]"))->getText());

                            $linksElem = $driver->findElements(WebDriverBy::cssSelector('table:nth-of-type(2) tbody tr td:first-child a'));
                            $links = $this->extractLinks($linksElem);

                            foreach ($links as $link) {
                                $driver->navigate()->to($link);

                                $tablePartesSelect = $driver->findElements(WebDriverBy::cssSelector('table tbody tr'));

                                $aux = $this->extractVerticalTable($tablePartesSelect);

                                $partes[$feito][] = $aux;
                            }

                            while ($next = $this->hasNext($driver, 'xpath', '//*[@id="container"]/p/a')) {
                                $driver->navigate()->to($next);

                                $feito = ($driver->findElement(WebDriverBy::xpath("//*[@id=\"container\"]/table[1]/tbody/tr[2]/td[2]"))->getText());

                                $linksElem = $driver->findElements(WebDriverBy::cssSelector('table:nth-of-type(2) tbody tr td:first-child a'));
                                $links = $this->extractLinks($linksElem);

                                foreach ($links as $link) {
                                    $driver->navigate()->to($link);

                                    $tablePartesSelect = $driver->findElements(WebDriverBy::cssSelector('table tbody tr'));

                                    $aux = $this->extractVerticalTable($tablePartesSelect);

                                    $partes[$feito][] = $aux;
                                }
                            }

                            $this->log(count($partes) . ' Partes');
                            $fieldsTratados[$processo->getProcesso()]['Partes'] = $partes;
                        }

                        //Mandatos tab
                        if (preg_match('/Mandados/', $name)) {
                            //SE ENCONTRAR TRATAR AQUI
                            $fieldsTratados[$processo->getProcesso()]['Mandados'] = [];

                            $driver->wait(60)->until(
                                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table'))
                            );
                            $linksElements = $driver->findElements(WebDriverBy::xpath("//*[@id=\"container\"]/table[2]/tbody/tr/td[1]/a"));
                            $linksMandados = $this->extractLinks($linksElements);

                            $mandados = [];
                            foreach ($linksMandados as $linkMandado) {
                                $driver->navigate()->to($linkMandado);
                                $mandadoRows = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table[2]/tbody/tr'));
                                $mandados[] = $this->extractVerticalTable($mandadoRows, 2, true);
                            }

                            $fieldsTratados[$processo->getProcesso()]['Mandados'] = $mandados;
                            $this->log(count($fieldsTratados[$processo->getProcesso()]['Mandados']) . ' Mandatos');
                        }

                    }

                }

            } catch (\Exception $e) {
                $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                continue;
            }

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjsp1instanciaesaj($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

//        $words = $service->getTodasPalavras(true);
//        $processosLink = [];
//        $foros = [];
//        foreach ($words as $word) {
//
//            $foros = [];
//            $foros['Todos os foros'] = '-1';
//
//            if (preg_match('/SEGURADORA/', $word)) {
//                //Foro
//                $driver->get($service->getUrl());
//
//                $selectForo = $driver->findElement(WebDriverBy::cssSelector('#id_Foro'));
//                $optionsForo = $selectForo->findElements(WebDriverBy::tagName('option'));
//
//                $foros = [];
//                foreach ($optionsForo as $option) {
//                    if ($option->getAttribute('value') == '' || $option->getAttribute('value') == '-1') continue;
//                    $foros[$option->getText()] = $option->getAttribute('value');
//                }
//            }
//
//            if (count($foros) > 1) $this->log(count($foros) . ' foros encontrados.');
//
//            $firstTime = true;
//            foreach ($foros as $foroNome => $foro) {
//
//                $this->log($foroNome);
//
//                $link = str_replace(['[PALAVRAS]', '[LOCAL]'], [$word, $foro], $service->getUrl());
//
//                try {
//                    $driver->get($link);
//                    if ($firstTime) {
//                        $this->log("Up and running!");
//                        $service->setOnline(1);
//                        $service->setColetando(1);
//                        $service->salvar(true);
//                        $firstTime = false;
//                    }
//                } catch (\Exception $e) {
//                    $service->setOnline(0);
//                    $service->setColetando(0);
//                    $service->salvar(true);
//                    continue;
//                }
//
//                try {
//                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
//                } catch (\Exception $e) {
//                    continue;
//                }
//
//                $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);
//
//                while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
//                    $driver->navigate()->to($next);
//                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
//                    $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);
//                }
//            }
//        }
//
//        $total = count($processosLink);
//        $this->log($total . ' processo(s) encontrado(s)');
//
//        $linksRegistrados = $service->registerProcessos($processosLink);
//        $totalLinksRegistrados = count($linksRegistrados);
//        $this->log($totalLinksRegistrados . ' A PROCESSAR');

        $linksRegistrados = $service->getTodosProcessos(0); //TEMPORARIO
        $totalLinksRegistrados = count($linksRegistrados); //TEMPORARIO

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
            $index++;

            $hashAnterior = $processo->getHash();

            try {
                $driver->navigate()->to($processo->getUrl());
            } catch (\Exception $e) {
                $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                continue;
            }

            $dadosProcesso = [];
            $partesProcesso = [];
            $movimentacoes = [];
            $peticoesDiversas = [];
            $incidentesAcoesRecursosSentencas = [];
            $apensosEntranhadosUnificados = [];
            $audiencia = [];
            $historico = [];

            try {
                //DADOS
                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
            } catch (\Exception $e) {
            }

            try {
                //PARTES
                $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                sleep(1);
                $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
            } catch (\Exception $e) {
                $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
            }

            try {
                //MOVIMENTACOES
                $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                sleep(1);
                $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tbody/tr'));
                $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
            } catch (\Exception $e) {
                $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tbody/tr'));
                $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
            }
            $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);

            try {
                //PETICOES
                $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[5]/tr'));
                $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, true);
            } catch (\Exception $e) {
            }

            try {
                //INCIDENTES
                $incidentesAcoesRecursosSentencasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[5]/tbody/tr'));
                $incidentesAcoesRecursosSentencas = $this->extractTable($incidentesAcoesRecursosSentencasAux, 2, true);
            } catch (\Exception $e) {
            }

            try {
                //APENSOS
                $apensosEntranhadosUnificadosAux = $driver->findElements(WebDriverBy::xpath('//*[@id="dadosApenso"]/tr'));
                $apensosEntranhadosUnificados = $this->extractTable($apensosEntranhadosUnificadosAux, 4, true);
            } catch (\Exception $e) {
            }

            try {
                //AUDIENCIAS
                $audienciaAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[7]/tbody/tr'));
                $audiencia = $this->extractTable($audienciaAux, 4, true);
            } catch (\Exception $e) {
            }

            try {
                //HISTORICO
                $historicoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[8]/tr'));
                $historico = $this->extractTable($historicoAux, 5, true);
            } catch (\Exception $e) {
            }

            $fieldsTratados[$processo->getProcesso()] = [
                'DadosProcesso' => $dadosProcesso,
                'PartesProcesso' => $partesProcesso,
                'Movimentacoes' => $movimentacoes,
                'PeticoesDiversas' => $peticoesDiversas,
                'IncidentesAcoesRecursosSentencas' => $incidentesAcoesRecursosSentencas,
                'ApensosEntranhadosUnificados' => $apensosEntranhadosUnificados,
                'Audiencia' => $audiencia,
                'Historico' => $historico,
            ];

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjsp2instanciaesaj($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            try {
                $driver->get($link);
                $this->log("Up and running!");
                $service->setOnline(1);
                $service->setColetando(1);
                $service->salvar(true);
            } catch (\Exception $e) {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            try {
                $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
            } catch (\Exception $e) {
                continue;
            }

            $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);

            while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
                $driver->navigate()->to($next);
                $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);
            }

        }

        $total = count($processosLink);
        $this->log($total . ' processo(s) encontrado(s)');

        $linksRegistrados = $service->registerProcessos($processosLink);
        $totalLinksRegistrados = count($linksRegistrados);
        $this->log($totalLinksRegistrados . ' A PROCESSAR');

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
            $index++;

            $hashAnterior = $processo->getHash();

            try {
                $driver->navigate()->to($processo->getUrl());
            } catch (\Exception $e) {
                $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                continue;
            }

            $dadosProcesso = [];
            $apensosVinculados = [];
            $nros1Instancia = [];
            $partesProcesso = [];
            $movimentacoes = [];
            $subprocessosRecursos = [];
            $peticoesDiversas = [];
            $composicaoJulgamento = [];
            $julgamentos = [];


            try {
                //DADOS DO PROCESSO
                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
            } catch (\Exception $e) {
            }

            try {
                //APENSOS E VINCULADOS
                $apensosVinculadosAux = $driver->findElement(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[4]/tbody/tr/td[contains(@colspan,"3")]'))->getText();
                if (!preg_match('/Não há processos apensos ou vinculados para este processo/', $apensosVinculadosAux)) throw new \Exception('TEM APENSOS E VINCULADOS!');
//                $apensosVinculados = $this->extractVerticalTable($apensosVinculadosAux, 2);
            } catch (\Exception $e) {
                $this->log($e->getMessage());
            }

            try {
                //NRO 1 INSTANCIA
                $nros1InstanciaAux = $driver->findElement(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[4]/tbody/tr/td[contains(@colspan,"3")]'))->getText();
                if (!preg_match('/Não há números de 1ª instância para este processo/', $nros1InstanciaAux)) throw new \Exception('TEM NRO 1 INSTANCIA!');
//                $nros1Instancia = $this->extractVerticalTable($nros1InstanciaAux, 2);
            } catch (\Exception $e) {
                $this->log($e->getMessage());
            }

            try {
                //PARTES
                $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                sleep(1);
                $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2, true);
            } catch (\Exception $e) {
                $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2, true);
            }

            try {
                //MOVIMENTACOES
                $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                sleep(1);
                $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tbody/tr'));
                $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
            } catch (\Exception $e) {
                $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tbody/tr'));
                $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
            }
            $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);

            try {
                //SUBPROCESSOS E RECURSOS
                $subprocessosRecursosAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[12]/tbody/tr'));
                $subprocessosRecursos = $this->extractTable($subprocessosRecursosAux, 2, true, ['Recebido em', 'Classe']);
            } catch (\Exception $e) {
            }

            try {
                //PETICOES DIVERSAS
                $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[14]/tbody/tr'));
                $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, true, ['Data', 'Tipo']);
            } catch (\Exception $e) {
            }

            try {
                //COMPOSIÇÃO DO JULGAMENTO
                $composicaoJulgamentoAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[16]/tbody/tr'));
                $composicaoJulgamento = $this->extractTable($composicaoJulgamentoAux, 2, true, ['Participação', 'Magistrado']);
            } catch (\Exception $e) {
            }

            try {
                //JULGAMENTOS
                $julgamentosAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[18]/tbody/tr'));
                $julgamentos = $this->extractTable($julgamentosAux, 3, true, ['Data', 'Situação do julgamento', 'Decisão']);
            } catch (\Exception $e) {
            }

            $fieldsTratados[$processo->getProcesso()] = [
                'DadosProcesso' => $dadosProcesso,
                'ApensosVinculados' => $apensosVinculados,
                'Numeros1Instancia' => $nros1Instancia,
                'PartesProcesso' => $partesProcesso,
                'Movimentacoes' => $movimentacoes,
                'SubprocessosRecursos' => $subprocessosRecursos,
                'PeticoesDiversas' => $peticoesDiversas,
                'ComposicaoJulgamento' => $composicaoJulgamento,
                'Julgamentos' => $julgamentos,
            ];

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjmg1instancia($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());

        //CAPTURA OS PROCESSOS
        try {
            $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
        } catch (\Exception $e) {
            $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
            return false;
        }

        $comarcaSelect = $driver->findElement(WebDriverBy::cssSelector('select[name="cbo_nome_comarca"]'));
        $optionsComarca = $comarcaSelect->findElements(WebDriverBy::tagName('option'));

        $comarcas = [];
        foreach ($optionsComarca as $option) {
            if ($option->getAttribute('value') == '') continue;
            $comarcas[$option->getText()] = $option->getAttribute('value');
        }

        $totalComarcas = count($comarcas);
        $this->log($totalComarcas . ' Comarcas encontradas.');

        $urlPadrao = "http://www4.tjmg.jus.br/juridico/sf/proc_resultado_nome.jsp?nomePessoa=[PALAVRAS]&cpfcnpj=&tipoPessoa=X&naturezaProcesso=0&situacaoParte=X&comrCodigo=[COMARCA]&numero=1";

        $words = $service->getTodasPalavras(true);
        $index = 1;
        $processosLink = [];
        foreach ($words as $word) {

            foreach ($comarcas as $comarcaNome => $comarcaId) {

                $urlComarca = str_replace(['[PALAVRAS]', '[COMARCA]'], [$word, $comarcaId], $urlPadrao);

                try {
                    $driver->get($urlComarca);

                    $this->log("{$index} de {$totalComarcas} => " . $comarcaNome);
                    $index++;
                } catch (\Exception $e) {
                    $this->log("Página indisponível link => {$urlComarca}", $this->logFileName);
                    continue;
                }

                try {
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                } catch (\Exception $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                }

                $processos = $driver->findElements(WebDriverBy::cssSelector('body > table > tbody > tr > td > a'));
                $processosLink = $this->extractLinks($processos, true, false, $processosLink, ['Ver Todos Processos com Paginação', 'Ver Todos Processos']);
            }
        }

        $processosLinkTratados = [];
        foreach ($processosLink as $nroProcesso => $item) {
            $processosLinkTratados[$this->extractProcessNumber($nroProcesso)] = $item;
        }

        $total = count($processosLinkTratados);
        $this->log($total . ' processo(s) encontrado(s)');

        $linksRegistrados = $service->registerProcessos($processosLinkTratados);
        $totalLinksRegistrados = count($linksRegistrados);
        $this->log($totalLinksRegistrados . ' A PROCESSAR');
        //FIM CAPTURA OS PROCESSOS
        $driver->quit();


        $linksRegistrados = $service->getTodosProcessos(0); //TEMPORARIO
        $totalLinksRegistrados = count($linksRegistrados); //TEMPORARIO

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            try {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                try {
                    $driver->navigate()->to($processo->getUrl());
                } catch (\Exception $e) {
                    $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                    continue;
                }

                $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');

                $fieldsTratados[$processo->getProcesso()] = [];

                $linksElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[7]/tbody/tr/td/b/a'));
                $linksGerais = $this->extractLinks($linksElem, true, false, [], [], ['Dados Completos', 'Todos Andamentos', 'Todas as Partes/Advogados']);

                //DADOS COMPLETOS
                $this->log('Dados Completos');
                $driver->navigate()->to($linksGerais['Dados Completos']);
                $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                $fieldsTratados[$processo->getProcesso()]['Dados'] = $this->explodeByPattern($driver->findElements(WebDriverBy::xpath('/html/body/table[1]/tbody/tr[1]/td')), ':');
                $fieldsTratados[$processo->getProcesso()]['Dados'] = $this->explodeByPattern($driver->findElements(WebDriverBy::xpath('/html/body/table[2]/tbody/tr/td')), ':', $fieldsTratados[$processo->getProcesso()]['Dados']);
                $fieldsTratados[$processo->getProcesso()]['Dados'] = $this->explodeByPattern($driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td')), ':', $fieldsTratados[$processo->getProcesso()]['Dados']);
                $fieldsTratados[$processo->getProcesso()]['Dados']['Local'] = $this->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/table[1]/tbody/tr[2]/td[1]'))->getText());
                $fieldsTratados[$processo->getProcesso()]['Dados']['Status'] = $this->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/table[1]/tbody/tr[2]/td[2]'))->getText());

                //TODOS ANDAMENTOS
                $this->log('Todos Andamentos');
                $driver->navigate()->to($linksGerais['Todos Andamentos']);
                $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                $tableElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[2]/tbody/tr'));
                $fieldsTratados[$processo->getProcesso()]['Andamentos'] = $this->extractTable($tableElem, 4, true, ['empty', 'Andamento', 'Obs', 'Data']);
                $fieldsTratados[$processo->getProcesso()]['Andamentos'] = $this->unsetKeyFromArray($fieldsTratados[$processo->getProcesso()]['Andamentos'], ['empty']);

                //TODAS AS PARTES/ADVOGADOS
                $this->log('Todas as Partes/Advogados');
                $driver->navigate()->to($linksGerais['Todas as Partes/Advogados']);
                $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                $tableRows = $driver->findElements(WebDriverBy::xpath('/html/body/table[2]/tbody/tr'));

                $partes = [];
                foreach ($tableRows as $tableRow) {
                    $aux = [];
                    $rowTextArr = preg_split('/\r\n|\r|\n/', $tableRow->getText());
                    foreach ($rowTextArr as $rowTextItem) {
                        if (preg_match('/([A-Z]{1}[\/]{1}[A-Z]{2})/', $rowTextItem)) {
                            $aux['Advogado(s)'][] = $this->cleanString($rowTextItem);
                            continue;
                        }
                        if (preg_match('/: /', $rowTextItem)) {
                            $auxArr = explode(':', $rowTextItem);
                            $aux[$this->cleanString($auxArr[0])] = $this->cleanString($auxArr[1]);
                            continue;
                        }
                        if (strpos($rowTextItem, '- ') == 0) {
                            $aux['Origem'] = str_replace('- ', '', $rowTextItem);
                            continue;
                        }

                    }
                    $partes[] = $aux;
                }

                $fieldsTratados[$processo->getProcesso()]['Partes'] = $partes;

            } catch (\Exception $e) {
                $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                continue;
            }

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);

        }

        $driver->quit();
        return true;
    }

    public function tjmg2instancia($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $words = $service->getTodasPalavras(true);
        foreach ($words as $word) {

            $driver = $this->startDriver();

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            if ($this->isDomainAvailable($link)) {
                $this->log("Up and running!");
                $service->setOnline(1);
                $service->setColetando(1);
                $service->salvar(true);
            } else {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $driver->get($link);
            try {
                $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
            } catch (\Exception $e) {
                $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                return false;
            }


            $processos = $driver->findElements(WebDriverBy::cssSelector('body > table > tbody > tr > td > a'));
            $processosLink = $this->extractLinks($processos, true, false, [], ['Ver Todos Processos com Paginação', 'Ver Todos Processos']);

            //TRATAR NRO PROCESSO
            $processosLinkTratados = [];
            foreach ($processosLink as $nroProcesso => $item) {
                $processosLinkTratados[$this->extractProcessNumber($nroProcesso)] = $item;
            }

            $total = count($processosLinkTratados);
            $this->log($total . ' processo(s) encontrado(s)');

            $linksRegistrados = $service->registerProcessos($processosLinkTratados);
            $totalLinksRegistrados = count($linksRegistrados);
            $this->log($totalLinksRegistrados . ' A PROCESSAR');

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */
            $index = 1;
            foreach ($linksRegistrados as $processo) {

                try {

                    $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                    $index++;

                    $hashAnterior = $processo->getHash();

                    try {
                        $driver->navigate()->to($processo->getUrl());
                    } catch (\Exception $e) {
                        $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                        continue;
                    }

                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');


                    $fieldsTratados[$processo->getProcesso()] = [];

                    $linksElem = $driver->findElements(WebDriverBy::xpath('//*[@id="proc1"]/table/tbody/tr/td/b/a'));
                    $linksGerais = $this->extractLinks($linksElem, true, false, [], [], ['Dados Completos', 'Todos Andamentos', 'Todas as Partes/Advogados', 'Expediente(s) Enviado(s) para Publicação']);

                    //DADOS COMPLETOS
                    $this->log('Dados Completos');
                    $driver->navigate()->to($linksGerais['Dados Completos']);
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                    $fieldsTratados[$processo->getProcesso()]['Dados'] = $this->explodeByPattern($driver->findElements(WebDriverBy::xpath('/html/body/table[1]/tbody/tr[1]/td')), ':');
                    $fieldsTratados[$processo->getProcesso()]['Dados'] = $this->explodeByPattern($driver->findElements(WebDriverBy::xpath('/html/body/table[2]/tbody/tr/td')), ':', $fieldsTratados[$processo->getProcesso()]['Dados']);
                    $fieldsTratados[$processo->getProcesso()]['Dados'] = $this->explodeByPattern($driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td')), ':', $fieldsTratados[$processo->getProcesso()]['Dados']);
                    $fieldsTratados[$processo->getProcesso()]['Dados']['Local'] = $this->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/table[1]/tbody/tr[2]/td[1]'))->getText());
                    $fieldsTratados[$processo->getProcesso()]['Dados']['Status'] = $this->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/table[1]/tbody/tr[2]/td[2]'))->getText());

                    //TODOS ANDAMENTOS
                    $this->log('Todos Andamentos');
                    $driver->navigate()->to($linksGerais['Todos Andamentos']);
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                    $tableElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[2]/tbody/tr'));
                    $fieldsTratados[$processo->getProcesso()]['Andamentos'] = $this->extractTable($tableElem, 4, true, ['empty', 'Andamento', 'Obs', 'Data']);
                    $fieldsTratados[$processo->getProcesso()]['Andamentos'] = $this->unsetKeyFromArray($fieldsTratados[$processo->getProcesso()]['Andamentos'], ['empty']);

                    //TODAS AS PARTES/ADVOGADOS
                    $this->log('Todas as Partes/Advogados');
                    $driver->navigate()->to($linksGerais['Todas as Partes/Advogados']);
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                    $tableRows = $driver->findElements(WebDriverBy::xpath('/html/body/table[2]/tbody/tr'));

                    $partes = [];
                    foreach ($tableRows as $tableRow) {
                        $aux = [];
                        $rowTextArr = preg_split('/\r\n|\r|\n/', $tableRow->getText());
                        foreach ($rowTextArr as $rowTextItem) {
                            if (preg_match('/([A-Z]{1}[\/]{1}[A-Z]{2})/', $rowTextItem)) {
                                $aux['Advogado(s)'][] = $this->cleanString($rowTextItem);
                                continue;
                            }
                            if (preg_match('/: /', $rowTextItem)) {
                                $auxArr = explode(':', $rowTextItem);
                                $aux[$this->cleanString($auxArr[0])] = $this->cleanString($auxArr[1]);
                                continue;
                            }
                            if (strpos($rowTextItem, '- ') == 0) {
                                $aux['Origem'] = str_replace('- ', '', $rowTextItem);
                                continue;
                            }

                        }
                        $partes[] = $aux;
                    }
                    $fieldsTratados[$processo->getProcesso()]['Partes'] = $partes;

                    //EXPEDIENTE(S) ENVIADO(S) PARA PUBLICAÇÃO
                    $publicacao = [];
                    if (isset($linksGerais['Expediente(s) Enviado(s) para Publicação'])) {
                        $this->log('Expediente(s) Enviado(s) para Publicação');
                        $driver->navigate()->to($linksGerais['Expediente(s) Enviado(s) para Publicação']);
                        $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');
                        $tablesCorpo = $driver->findElements(WebDriverBy::cssSelector('body table.corpo'));

                        foreach ($tablesCorpo as $tableCorpo) {
                            $data = $this->cleanString(str_replace('Data pauta: ', '', $tableCorpo->findElement(WebDriverBy::cssSelector('tbody tr td:first-child'))->getText()));
                            if (!$data) continue;
                            $infos = $driver->findElements(WebDriverBy::cssSelector('td[itemprop="texto' . $data . '"]'));
                            $infoTratado = '';
                            foreach ($infos as $info) {
                                $infoTratado .= $info->getText();
                            }
                            @$publicacao[$data] = $infoTratado;
                        }
                    }
                    $fieldsTratados[$processo->getProcesso()]['Publicação'] = $publicacao;

                } catch (TimeOutException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (NoSuchDriverException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (NoSuchWindowException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (NoSuchElementException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (ElementNotSelectableException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (ElementNotVisibleException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (IMENotAvailableException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (IMEEngineActivationFailedException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (ExpectedException $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                } catch (\Exception $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                }

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);

            }

        }

        $driver->quit();
        return true;
    }

    public function tjrj1instancia($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();
        $words = $service->getTodasPalavras();

        $processosLink = [];
        foreach ($words as $word) {

            if ($this->isDomainAvailable($service->getUrl())) {
                $this->log("Up and running!");
                $service->setOnline(1);
                $service->setColetando(1);
                $service->salvar(true);
            } else {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $driver->get($service->getUrl());

            $driver->wait(60)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('select[name=origem]'))
            );

            $driver->findElement(WebDriverBy::cssSelector('select[name=origem]'))->sendKeys('1');
            sleep(1);

            $competenciasElem = $driver->findElements(WebDriverBy::cssSelector('select[name=competencia] option'));
            $competencias = [];
            foreach ($competenciasElem as $comp) {
                if ($comp->getAttribute('value') == '' || $comp->getText() == '') continue;
                $competencias[$comp->getText()] = $comp->getAttribute('value');
            }

            $ano = date('Y') - 15;
            foreach ($competencias as $competenciasName => $competenciaCod) {

                $driver->navigate()->to($service->getUrl());

                $this->log($competenciasName);

                $driver->wait(60)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('select[name=origem]'))
                );

                $driver->findElement(WebDriverBy::cssSelector('select[name=origem]'))->sendKeys('1');
                sleep(1);
                $driver->findElement(WebDriverBy::cssSelector('select[name=comarca]'))->sendKeys('TODAS');
                $driver->executeScript("document.querySelector(\"select[name=competencia]\").value = '{$competenciaCod}'", []);
                $driver->findElement(WebDriverBy::cssSelector('input[name=anoInicio]'))->clear();

                $driver->findElement(WebDriverBy::cssSelector('input[name=anoInicio]'))->sendKeys($ano);
                $driver->findElement(WebDriverBy::cssSelector('input[name=nomeParte]'))->sendKeys($word);
                $driver->findElement(WebDriverBy::cssSelector('#pesquisa'))->click();

                $paginasElem = $driver->findElements(WebDriverBy::cssSelector('select[name="pagina"] option'));
                $qtdPaginas = count($paginasElem);

                $qtdPgaText = ($qtdPaginas) ? $qtdPaginas : 1;
                $this->log($qtdPgaText . ' pagina(s)');

                $processos = $driver->findElements(WebDriverBy::xpath('//*[@id="content"]/form/table[3]/tbody/tr/td/a'));
                $processosLink = $this->extractLinks($processos, true, false, $processosLink, ['voltar', 'avançar']);

                for ($i = 1; $i <= $qtdPaginas; $i++) {
                    $driver->findElement(WebDriverBy::xpath('//a[contains(text(),\'avançar\')]'))->click();
                    $processos = $driver->findElements(WebDriverBy::xpath('//*[@id="content"]/form/table[3]/tbody/tr/td/a'));
                    $processosLink = $this->extractLinks($processos, true, false, $processosLink, ['voltar', 'avançar']);
                }
            }

            //TRATAR NRO PROCESSO
            $processosLinkTratados = [];
            foreach ($processosLink as $nroProcesso => $item) {
                $processosLinkTratados[$this->extractProcessNumber($nroProcesso)] = $item;
            }

            $total = count($processosLinkTratados);
            $this->log($total . ' processo(s) encontrado(s)');

            $linksRegistrados = $service->registerProcessos($processosLinkTratados);
            $totalLinksRegistrados = count($linksRegistrados);
            $this->log($totalLinksRegistrados . ' A PROCESSAR');

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */
            $index = 1;
            foreach ($linksRegistrados as $processo) {

                try {

                    $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                    $index++;

                    $hashAnterior = $processo->getHash();

                    $driver->navigate()->to($processo->getUrl());
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="imgCaptcha"]', '//*[@id="captcha"]', '//*[@id="container_captcha"]/fieldset/table/tbody/tr[2]/td[2]/input');

                    $fieldsTratados[$processo->getProcesso()] = [];

                    $rowsElem = $driver->findElements(WebDriverBy::xpath('//*[@id="content"]/form/table/tbody/tr'));
                    $fieldsTratados[$processo->getProcesso()]['Dados Gerais'] = $this->extractVerticalTable($rowsElem, 2);

                    $allLinks = $driver->findElements(WebDriverBy::cssSelector('#content a'));

                    //ANDAMENTOS
                    $fieldsTratados[$processo->getProcesso()]['Andamentos'] = [];
                    try {
                        foreach ($allLinks as $linkParte) {
                            if (preg_match('/Visualização dos Históricos/', $linkParte->getText())) {
                                $linkParte->click();
                                $tableAndamentosAux = $driver->findElement(WebDriverBy::xpath('//*[@id="tbdados"]'));
                                $tableAndamentos = $this->extractRegularTable($tableAndamentosAux);
                                $fieldsTratados[$processo->getProcesso()]['Andamentos'] = $tableAndamentos;
                                $driver->findElement(WebDriverBy::cssSelector('a.fechar'))->click();
                                break;
                            }
                        }
                    } catch (\Exception $e) {
                    }

                    //PARTES
                    $fieldsTratados[$processo->getProcesso()]['Partes'] = [];
                    try {
                        foreach ($allLinks as $linkParte) {
                            if (preg_match('/Listar todos os personagens/', $linkParte->getText())) {
                                $linkParte->click();
                                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="listaPersonagens"]/table/tbody/tr'));
                                $tablePartes = $this->extractTable($rows, 2);
                                $fieldsTratados[$processo->getProcesso()]['Partes'] = $tablePartes;
                                break;
                            }
                        }
                    } catch (\Exception $e) {
                    }

                } catch (\Exception $e) {
                    $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                    continue;
                }

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);
            }
        }

        $driver->quit();
        return true;
    }

    public function tjrj2instancia($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();
        $words = $service->getTodasPalavras();
        $driver->get($service->getUrl());
        $processosLink = [];
        foreach ($words as $word) {

            if ($this->isDomainAvailable($service->getUrl())) {
                $this->log("Up and running!");
                $service->setOnline(1);
                $service->setColetando(1);
                $service->salvar(true);
            } else {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $driver->navigate()->to($service->getUrl());

            $driver->wait(60)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('select[name=origem]'))
            );

            $driver->executeScript("document.querySelector(\"select[name = origem]\").value = '2'", []);
            $driver->findElement(WebDriverBy::cssSelector('input[name=anoInicio]'))->clear();
            $ano = date('Y') - 15;
            $driver->findElement(WebDriverBy::cssSelector('input[name=anoInicio]'))->sendKeys($ano);
            $driver->findElement(WebDriverBy::cssSelector('input[name=nomeParte]'))->sendKeys($word);
            $driver->findElement(WebDriverBy::cssSelector('#pesquisa'))->click();

            $paginasElem = $driver->findElements(WebDriverBy::cssSelector('select[name="pagina"] option'));
            $qtdPaginas = count($paginasElem);

            $qtdPgaText = ($qtdPaginas) ? $qtdPaginas : 1;
            $this->log($qtdPgaText . ' pagina(s)');

            $processos = $driver->findElements(WebDriverBy::xpath('//*[@id="content"]/form/table[3]/tbody/tr/td/a'));
            $processosLink = $this->extractLinks($processos, true, false, $processosLink, ['voltar', 'avançar']);

            for ($i = 1; $i <= $qtdPaginas; $i++) {
                $driver->findElement(WebDriverBy::xpath('//a[contains(text(),\'avançar\')]'))->click();
                $processos = $driver->findElements(WebDriverBy::xpath('//*[@id="content"]/form/table[3]/tbody/tr/td/a'));
                $processosLink = $this->extractLinks($processos, true, false, $processosLink, ['voltar', 'avançar']);
            }
        }

        //TRATAR NRO PROCESSO
        $processosLinkTratados = [];
        foreach ($processosLink as $nroProcesso => $item) {
            $processosLinkTratados[$this->extractProcessNumber($nroProcesso)] = $item;
        }

        $total = count($processosLinkTratados);
        $this->log($total . ' processo(s) encontrado(s)');

        $linksRegistrados = $service->registerProcessos($processosLinkTratados);
        $totalLinksRegistrados = count($linksRegistrados);
        $this->log($totalLinksRegistrados . ' A PROCESSAR');

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            try {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                $driver->navigate()->to($processo->getUrl());

                $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="imgCaptcha"]', '//*[@id="captcha"]', '//*[@id="container_captcha"]/fieldset/table/tbody/tr[2]/td[2]/input');

                $fieldsTratados[$processo->getProcesso()] = [];

                //TABLES
                $driver->wait(5)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/span/table'))
                );

                $tables = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/span/table'));
                $dadosGerais = [];
                foreach ($tables as $tableItem) {
                    $dadosGerais = array_merge($dadosGerais, $this->extractVerticalTable($tableItem->findElements(WebDriverBy::cssSelector('tr')), 2, true));
                }

                $fieldsTratados[$processo->getProcesso()]['Dados Gerais'] = $dadosGerais;

                //TABLE 4
                try {
                    $inteiroTeor = [];
                    $tableInteiroTeorLinks = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/span/table[4]/tbody/tr/td/a'));
                    $linksInteiroTeor = $this->extractLinks($tableInteiroTeorLinks);
                    $tableInteriorTeor = $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/span/table[4]/tbody/tr/td'))->getText();
                    $textInteiroTeor = explode("\n", $tableInteriorTeor);

                    $textInteiroTeorTratado = [];
                    foreach ($textInteiroTeor as $itemTeor) {
                        $textTeor = $this->cleanString($itemTeor);
                        if ($textTeor == '' || $textTeor == ' ' || $textTeor == 'INTEIRO TEOR') continue;
                        $textInteiroTeorTratado[] = $textTeor;
                    }
                    foreach ($textInteiroTeorTratado as $idx => $itemTeor) {
                        $textTeor = $this->cleanString($itemTeor);
                        if ($textTeor == '' || $textTeor == ' ') continue;
                        $linkText = (isset($linksInteiroTeor[$idx])) ? ' [Link => ' . $linksInteiroTeor[$idx] . ']' : '';
                        $inteiroTeor[] = $textTeor . $linkText;
                    }

                } catch (\Exception $e) {

                }
                $fieldsTratados[$processo->getProcesso()]['Inteiro Teor'] = $inteiroTeor;

                $allLinks = $driver->findElements(WebDriverBy::cssSelector('#content a'));

                //PARTES
                $fieldsTratados[$processo->getProcesso()]['Partes'] = [];
                try {
                    foreach ($allLinks as $linkParte) {
                        if (preg_match('/Listar todos os personagens/', $linkParte->getText())) {
                            $linkParte->click();
                            $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="listaPersonagens"]/table/tbody/tr'));
                            $tablePartes = $this->extractTable($rows, 2);
                            $fieldsTratados[$processo->getProcesso()]['Partes'] = $tablePartes;
                            break;
                        }
                    }
                } catch (\Exception $e) {
                }

            } catch (\Exception $e) {
                $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
                continue;
            }

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjba1instanciaesaj($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $driver = $this->startDriver();
        $words = $service->getTodasPalavras(true);

        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            if ($this->isDomainAvailable($link)) {
                $this->log("Up and running!");
                $service->setOnline(1);
                $service->setColetando(1);
                $service->salvar(true);
            } else {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $driver->get($link);

            $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="defaultCaptchaImage"]', '//*[@id="defaultCaptchaCampo"]', '//*[@id="pbEnviar"]');

            $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));

            $links = [];
            foreach ($processosHref as $element) {
                $links[$element->getText()] = $element->getAttribute('href');
            }

            while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
                $driver->navigate()->to($next);
                $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                foreach ($processosHref as $element) {
                    $links[$element->getText()] = $element->getAttribute('href');
                }
            }

            $this->log(count($links) . ' processo(s) encontrado(s)');

            $linksRegistrados = $service->registerProcessos($links);
            $totalLinksRegistrados = count($linksRegistrados);
            $this->log($totalLinksRegistrados . ' processo(s) a processar');

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */
            $index = 1;
            foreach ($linksRegistrados as $processo) {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                $driver->navigate()->to($processo->getUrl());
                try {
                    try {
                        $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[3]/tbody/tr')));
                    } catch (\Exception $e) {
                        $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="defaultCaptchaImage"]', '//*[@id="defaultCaptchaCampo"]', '//*[@id="pbEnviar"]');
                    }
                } catch (\Exception $e) {
                    $this->log($e->getMessage(), $this->logFileName);
                    continue;
                }

                $dadosProcesso = [];
                $partesProcesso = [];
                $movimentacoes = [];
                $peticoesDiversas = [];
                $incidentesAcoesRecursosSentencas = [];
                $apensosEntranhadosUnificados = [];
                $audiencia = [];
                $historico = [];

                try {
                    //DADOS
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[3]/tbody/tr'));
                    $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
                } catch (\Exception $e) {
                }

                try {
                    //PARTES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                    sleep(1);
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                } catch (\Exception $e) {
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                }

                try {
                    //MOVIMENTACOES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                    sleep(1);
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tbody/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                } catch (\Exception $e) {
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tbody/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                }
                $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);

                try {
                    //PETICOES
                    $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[9]/tbody/tr'));
                    $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, true);
                } catch (\Exception $e) {
                }

                try {
                    //INCIDENTES
                    $incidentesAcoesRecursosSentencasAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[10]/tbody/tr'));
                    $incidentesAcoesRecursosSentencas = $this->extractTable($incidentesAcoesRecursosSentencasAux, 2, true);
                } catch (\Exception $e) {
                }

                try {
                    //APENSOS
                    $apensosEntranhadosUnificadosAux = $driver->findElements(WebDriverBy::xpath('//*[@id="dadosApenso"]/tr'));
                    $apensosEntranhadosUnificados = $this->extractTable($apensosEntranhadosUnificadosAux, 4, true);
                } catch (\Exception $e) {
                }

                try {
                    //AUDIENCIAS
                    $audienciaAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[13]/tbody/tr'));
                    $audiencia = $this->extractTable($audienciaAux, 4, true);
                } catch (\Exception $e) {
                }

                try {
                    //HISTORICO
                    $historicoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[8]/tr'));
                    $historico = $this->extractTable($historicoAux, 5, true);
                } catch (\Exception $e) {
                }

                $fieldsTratados[$processo->getProcesso()] = [
                    'DadosProcesso' => $dadosProcesso,
                    'PartesProcesso' => $partesProcesso,
                    'Movimentacoes' => $movimentacoes,
                    'PeticoesDiversas' => $peticoesDiversas,
                    'IncidentesAcoesRecursosSentencas' => $incidentesAcoesRecursosSentencas,
                    'ApensosEntranhadosUnificados' => $apensosEntranhadosUnificados,
                    'Audiencia' => $audiencia,
                    'Historico' => $historico,
                ];

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);
            }

        }

        $driver->quit();
        return true;
    }

    public function tjba2instanciaesaj($service, $processarApenasProcessos = false)
    {
        //REFAZER O $processarApenasProcessos
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $driver = $this->startDriver();
        $words = $service->getTodasPalavras(true);

        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            if ($this->isDomainAvailable($link)) {
                $this->log("Up and running!");
                $service->setOnline(1);
                $service->setColetando(1);
                $service->salvar(true);
            } else {
                $service->setOnline(0);
                $service->setColetando(0);
                $service->salvar(true);
                continue;
            }

            $driver->get($link);

            $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="defaultCaptchaImage"]', '//*[@id="defaultCaptchaCampo"]', '//*[@id="pbEnviar"]');

            $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));

            $links = [];
            foreach ($processosHref as $element) {
                $links[$element->getText()] = $element->getAttribute('href');
            }

            while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
                $driver->navigate()->to($next);
                $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                foreach ($processosHref as $element) {
                    $links[$element->getText()] = $element->getAttribute('href');
                }
            }

            $this->log(count($links) . ' processo(s) encontrado(s)');

            $linksRegistrados = $service->registerProcessos($links);
            $totalLinksRegistrados = count($linksRegistrados);
            $this->log($totalLinksRegistrados . ' processo(s) a processar');

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */

            $index = 1;
            foreach ($linksRegistrados as $processo) {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                $driver->navigate()->to($processo->getUrl());
                try {
                    try {
                        $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[3]/tbody/tr')));
                    } catch (\Exception $e) {
                        $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="defaultCaptchaImage"]', '//*[@id="defaultCaptchaCampo"]', '//*[@id="pbEnviar"]');
                    }
                } catch (\Exception $e) {
                    $this->log($e->getMessage(), $this->logFileName);
                    continue;
                }

                $dadosProcesso = [];
                $partesProcesso = [];
                $movimentacoes = [];
                $peticoesDiversas = [];
                $incidentesAcoesRecursosSentencas = [];
                $apensosEntranhadosUnificados = [];
                $audiencia = [];
                $historico = [];

                try {
                    //DADOS
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[3]/tbody/tr'));
                    $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
                } catch (\Exception $e) {
                }

                try {
                    //PARTES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                    sleep(1);
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                } catch (\Exception $e) {
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                }

                try {
                    //MOVIMENTACOES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                    sleep(1);
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tbody/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                } catch (\Exception $e) {
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tbody/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                }
                $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);

                try {
                    //PETICOES
                    $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[9]/tbody/tr'));
                    $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, true);
                } catch (\Exception $e) {
                }

                try {
                    //INCIDENTES
                    $incidentesAcoesRecursosSentencasAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[10]/tbody/tr'));
                    $incidentesAcoesRecursosSentencas = $this->extractTable($incidentesAcoesRecursosSentencasAux, 2, true);
                } catch (\Exception $e) {
                }

                try {
                    //APENSOS
                    $apensosEntranhadosUnificadosAux = $driver->findElements(WebDriverBy::xpath('//*[@id="dadosApenso"]/tr'));
                    $apensosEntranhadosUnificados = $this->extractTable($apensosEntranhadosUnificadosAux, 4, true);
                } catch (\Exception $e) {
                }

                try {
                    //AUDIENCIAS
                    $audienciaAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table[13]/tbody/tr'));
                    $audiencia = $this->extractTable($audienciaAux, 4, true);
                } catch (\Exception $e) {
                }

                try {
                    //HISTORICO
                    $historicoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[8]/tr'));
                    $historico = $this->extractTable($historicoAux, 5, true);
                } catch (\Exception $e) {
                }

                $fieldsTratados[$processo->getProcesso()] = [
                    'DadosProcesso' => $dadosProcesso,
                    'PartesProcesso' => $partesProcesso,
                    'Movimentacoes' => $movimentacoes,
                    'PeticoesDiversas' => $peticoesDiversas,
                    'IncidentesAcoesRecursosSentencas' => $incidentesAcoesRecursosSentencas,
                    'ApensosEntranhadosUnificados' => $apensosEntranhadosUnificados,
                    'Audiencia' => $audiencia,
                    'Historico' => $historico,
                ];

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);
            }

        }

        $driver->quit();
        return true;
    }

    public function tjrs1instancia($service, $processarApenasProcessos = true)
    {
        $processarApenasProcessos = false; //TEMPORARIO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
//        $service = new Tribunal();
        $driver = $this->startDriver2();
        $urlInitial = 'https://www.tjrs.jus.br/site_php/consulta/index.php';

        if ($this->isDomainAvailable($urlInitial)) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($urlInitial);

        if ($processarApenasProcessos) {


            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('#td_parte > a')));
            $driver->findElement(WebDriverBy::cssSelector('#td_parte > a'))->click();

            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('select[name=id_comarca3]')));

            $comarcasElem = $driver->findElements(WebDriverBy::cssSelector('select[name=id_comarca3] option'));
            $comarcas = [];
            foreach ($comarcasElem as $item) {
                $textSelect = $this->cleanString($item->getText());
                $valueSelect = $this->cleanString($item->getAttribute('value'));
                if (in_array($valueSelect, ['700', '710'])) continue; //Elimina 2° grau (instância)
                if ($textSelect == '' || $valueSelect == '') continue;
                $comarcas[$textSelect] = $valueSelect;
            }

            $situacao = '1';
            $words = $service->getTodasPalavras(true);

            $partes = [];
            $urlPadrao = $service->getUrl();
            $times = 1;
            foreach ($comarcas as $comarcaName => $comarca) {
                foreach ($words as $word) {
                    $this->log("{$times}: COMARCA => {$comarcaName}, PALAVRA => {$word}");
                    $link = str_replace(['[PALAVRAS]', '[COMARCA]', '[SITUACAO]', ' '], [$word, $comarca, $situacao, ''], $urlPadrao);
                    $driver->navigate()->to($link);

                    try {
                        $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('img[name=img_check]')));
                        $driver = $this->hasCaptcha($driver, 'xpath', '/html/body/form/table/tbody/tr[1]/td/div/img', '/html/body/form/table/tbody/tr[1]/td/div/input', '/html/body/form/table/tbody/tr[2]/td/input');
                    } catch (\Exception $e) {

                    }

                    try {
                        $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('span > erro')));
                        continue;
                    } catch (\Exception $e) {
                        $partesElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo > table > tbody > tr > td.texto_geral > a'));
                        $partes = $this->extractLinks($partesElem, false, false, $partes);
                    }
                    $times++;
                }
            }

            $processosLink = [];
            $index = 1;
            foreach ($partes as $parteLink) {
                try {
                    $this->log("Parte {$index} => {$parteLink}");
                    $driver->navigate()->to($parteLink);
                    $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('img[name=img_check]')));
                    $driver = $this->hasCaptcha($driver, 'xpath', '/html/body/form/table/tbody/tr[1]/td/div/img', '/html/body/form/table/tbody/tr[1]/td/div/input', '/html/body/form/table/tbody/tr[2]/td/input');
                } catch (\Exception $e) {
                }
                $processosElem = $driver->findElements(WebDriverBy::cssSelector('table tbody tr td:nth-child(1) a'));
                $processosLink = $this->extractLinks($processosElem, true, false, $processosLink);
                $this->log(' TOTAL DE PROCESSOS => ' . count($processosLink));
                $index++;
            }

            $processosLinkTratados = [];
            foreach ($processosLink as $nroProcesso => $processoLink) {
                if (in_array($nroProcesso, ['', ' '])) continue;
                $nroProcesso = $this->extractProcessNumber($nroProcesso);
                if (!preg_match('/-/', $nroProcesso)) continue;
                if (strlen($nroProcesso) < 25) continue;
                $processosLinkTratados[$nroProcesso] = $processoLink;
            }

            $total = count($processosLinkTratados);
            $this->log($total . ' processo(s) encontrado(s)');

            $linksRegistrados = $service->registerProcessos($processosLinkTratados);
            $totalLinksRegistrados = count($linksRegistrados);
            $this->log($totalLinksRegistrados . ' A PROCESSAR');

        } else {

            $linksRegistrados = $service->getTodosProcessos(0);
            $totalLinksRegistrados = count($linksRegistrados);

        }

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            try {
                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                $driver->navigate()->to($processo->getUrl());

                try {
                    try {
                        $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody/tr')));
                    } catch (\Exception $e) {
                        $driver = $this->hasCaptcha($driver, 'xpath', '/html/body/form/table/tbody/tr[1]/td/div/img', '/html/body/form/table/tbody/tr[1]/td/div/input', '/html/body/form/table/tbody/tr[2]/td/input');
                    }
                } catch (\Exception $e) {
                    $this->log($e->getMessage(), $this->logFileName);
                    continue;
                }

                $allLinksElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo a'));
                $allLinks = $this->extractLinks($allLinksElem, true, false, [], [], [
                    'Ver Processos',
                    'Ver todas as partes e advogados',
                    'Ver todas as movimentações',
                    'Ver Notas de Expediente',
                    'Ver Audiências',
                    'Ver Termos de Audiência',
                    'Ver Leilões',
                    'Ver Sentença',
                    'Ver Outras Informações',
                    'Ver Mandados Oficiais',
                    'Ver Depósitos Judiciais de 1º grau',
                    'Ver Alvarás Automatizados Expedidos',
                    'Ver Guias de Custas'
                ]);

                //Dados Principais
                $tableDadosPrincipaisElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody/tr'));
                $tableDadosPrincipais = $this->extractVerticalTable($tableDadosPrincipaisElem, 3, false, 1);

                $tableProcessos = [];
                $tablePartesAdvs = [];
                $tableMov = [];
                $tableNotasExp = [];
                $tableAudi = [];
                $tableTermosAudi = [];
                $tableLeiloes = [];
                $tableSentencas = [];
                $tableOutrasInfo = [];
                $tableMandadosOf = [];
                $tableDepositosJud1Grau = [];
                $tableAlvarasAutoExp = [];
                $tableGuiasCustas = [];
                foreach ($allLinks as $linkName => $linkUrl) {
                    $driver->navigate()->to($linkUrl);

                    if (preg_match('/Ver Processos/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr')));
                            $tableProcessosElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableProcessos[] = $this->extractTable($tableProcessosElem, 5);
                        } catch (\Exception $e) {
                            $tableProcessosText = $this->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody'))->getText());
                            $tableProcessos[] = $tableProcessosText;
                        }
                        continue;
                    }

                    if (preg_match('/Ver todas as partes e advogados/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tablePartesAdvsElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $colsHeader = [];
                            foreach ($tablePartesAdvsElem as $tablePartesAdvsElemitem) { //rows[]
                                $colunas = $tablePartesAdvsElemitem->findElements(WebDriverBy::tagName('td'));

                                $idx = 0;
                                $row = [];
                                foreach ($colunas as $coluna) {
                                    $textCol = str_replace(':', '', $this->cleanString($coluna->getText()));
                                    if ($textCol == '') continue;
                                    if ($coluna->getAttribute('class') == 'texto_geral_negrito') {
                                        if (count($colsHeader) >= 2) $colsHeader = [];
                                        $colsHeader[] = $textCol;
                                        continue;
                                    }
                                    if (isset($colsHeader[$idx])) {
                                        @$row[$colsHeader[$idx]] = $textCol;
                                        $idx++;
                                    }
                                }

                                if (count($row) > 0) $tablePartesAdvs[] = $row;
                            }

                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver todas as movimenta/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableMovElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableMov = $this->extractVerticalTable($tableMovElem, 3, true, 1);
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver Notas de Expediente/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableNotasExpElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableNotasExp = $this->extractTable($tableNotasExpElem, 3);
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver Audi/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableAudiText = $this->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody'))->getText());
                            $tableAudi[] = $tableAudiText;
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver Termos de Audi/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody')));
                            $tableAudiElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody/tr'));
                            foreach ($tableAudiElem as $tableAudiElemItem) {
                                try {
                                    $alinkElem = $tableAudiElemItem->findElements(WebDriverBy::cssSelector('a'));
                                    $tableTermosAudi = $this->extractLinks($alinkElem, true, false, $tableTermosAudi);
                                } catch (\Exception $e) {
                                    $tableTermosAudi = $tableAudiElemItem->getText();
                                }
                            }
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver Leil/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableLeiloesText = $this->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody'))->getText());
                            $tableLeiloes[] = $tableLeiloesText;
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver Senten/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableSentencasText = $this->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody'))->getText());
                            $tableSentencas['Sentença'] = $tableSentencasText;

                            $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[4]/tbody/tr')));
                            $tableSentencasDocsElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[4]/tbody/tr'));
                            $tableSentencas['Docs'] = $this->extractTable($tableSentencasDocsElem, 2, true, ['Data da liberação', 'Documentos']);


                            // Baixar arquivos das sentenças
                            $count_docs = count($tableSentencas['Docs']);
                            if($count_docs > 0){
                                for ($i=0; $i < $count_docs; $i++){
                                    $sentenca = explode('[', $tableSentencas['Docs'][$i]['Documentos']);
                                    $link_sentenca = trim($sentenca[0]);
                                    $driver->findElement(WebDriverBy::linkText($link_sentenca))->click();

                                }
                            }


                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver Outras Informa/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableOutrasInfoElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableOutrasInfo = $this->extractVerticalTable($tableOutrasInfoElem, 3, false, 1);
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver Mandados Oficiais/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr')));
                            $tableMandadosOfElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableMandadosOf = $this->extractTable($tableMandadosOfElem, 8);

                            if (!count($tableMandadosOf)) $tableMandadosOf[] = $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody'))->getText();
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/sitos Judiciais de 1/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableDepositosJud1GrauElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $aux = [];
                            foreach ($tableDepositosJud1GrauElem as $tableDepositosJud1GrauElemItem) {
                                $colunas = $tableDepositosJud1GrauElemItem->findElements(WebDriverBy::tagName('td'));
                                if (count($colunas) == 1) {
                                    if (count($aux)) {
                                        $tableDepositosJud1Grau[] = $aux;
                                    } else {
                                        if ($this->cleanString($colunas[0]->getText())) $tableDepositosJud1Grau[] = $this->cleanString($colunas[0]->getText());
                                    }
                                    $aux = [];
                                    continue;
                                }
                                $trash = array_shift($colunas); //elimina a primeira coluna vazia

                                $title = str_replace(':', '', $this->cleanString($colunas[0]->getText()));
                                $value = $this->cleanString($colunas[1]->getText());
                                if ($value == '') continue;
                                $aux[$title] = $value;
                            }
//                            if (count($aux)) $tableDepositosJud1Grau[] = $aux; //finaliza
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/s Automatizados Expedidos/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableAlvarasAutoExpElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $aux = [];
                            foreach ($tableAlvarasAutoExpElem as $tableAlvarasAutoExpElemItem) {
                                $colunas = $tableAlvarasAutoExpElemItem->findElements(WebDriverBy::tagName('td'));
                                if (count($colunas) == 1) {
                                    if (count($aux)) {
                                        $tableAlvarasAutoExp[] = $aux;
                                    } else {
                                        if ($this->cleanString($colunas[0]->getText())) $tableAlvarasAutoExp[] = $this->cleanString($colunas[0]->getText());
                                    }
                                    $aux = [];
                                    continue;
                                }
                                $trash = array_shift($colunas); //elimina a primeira coluna vazia

                                $title = str_replace(':', '', $this->cleanString($colunas[0]->getText()));
                                $value = $this->cleanString($colunas[1]->getText());
                                if ($value == '') continue;
                                $aux[$title] = $value;
                            }
//                            if (count($aux)) $tableAlvarasAutoExp[] = $aux; //finaliza
                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Guias de Custas/', $linkName)) {
                        try {
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableGuiasCustasElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $trash = array_shift($tableGuiasCustasElem);
                            foreach ($tableGuiasCustasElem as $tableGuiasCustasElemItem) {
                                $colunas = $tableGuiasCustasElemItem->findElements(WebDriverBy::tagName('td'));
                                $tableGuiasCustas[] = [
                                    'Data da Emissão' => $colunas[1]->getText(),
                                    'Número da Guia' => $colunas[2]->getText(),
                                    'Valor' => $colunas[3]->getText(),
                                    'Data do Pagamento' => $colunas[4]->getText()
                                ];
                            }
                        } catch (\Exception $e) {
                        }
                        continue;
                    }
                }


            } catch (\Exception $e) {
                $this->log($e->getMessage(), $this->logFileName);
                continue;
            }

//            $teste = substr_replace($teste[1],'', -2);
//            $teste = trim($teste);
//            $teste = $teste+"S";
//
//            $ttt = file_get_contents($teste);
//            file_put_contents('C:\Testesss\teste.odt', $ttt);
//            var_dump($teste);die;

            $fieldsTratados[$processo->getProcesso()] = [
                'Dados Principais' => $tableDadosPrincipais,
                'Processos Vinculados' => $tableProcessos,
                'Partes e Advogados' => $tablePartesAdvs,
                'Movimentações' => $tableMov,
                'Notas de Expediente' => $tableNotasExp,
                'Audiência' => $tableAudi,
                'Termos de Audiência' => $tableTermosAudi,
                'Leilões' => $tableLeiloes,
                'Sentença' => $tableSentencas,
                'Outras Informações' => $tableOutrasInfo,
                'Mandados Oficiais' => $tableMandadosOf,
                'Depósitos Judiciais de 1º grau' => $tableDepositosJud1Grau,
                'Alvarás Automatizados Expedidos' => $tableAlvarasAutoExp,
                'Guias de Custas' => $tableGuiasCustas,
            ];

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()],  JSON_UNESCAPED_UNICODE));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjrs2instancia($service, $processarApenasProcessos = false)
    {
        $processarApenasProcessos = true; //TEMPORARIO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();
        $urlInitial = 'https://www.tjrs.jus.br/site_php/consulta/index.php';

        if ($this->isDomainAvailable($urlInitial)) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($urlInitial);

        if (!$processarApenasProcessos) {

            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('#td_parte > a')));
            $driver->findElement(WebDriverBy::cssSelector('#td_parte > a'))->click();

            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('select[name=id_comarca3]')));

            $comarcasElem = $driver->findElements(WebDriverBy::cssSelector('select[name=id_comarca3] option'));
            $comarcas = [];
            foreach ($comarcasElem as $item) {
                $textSelect = $this->cleanString($item->getText());
                $valueSelect = $this->cleanString($item->getAttribute('value'));
                if (!in_array($valueSelect, ['700', '710'])) continue; //Apenas 2° grau (instância)
                if ($textSelect == '' || $valueSelect == '') continue;
                $comarcas[$textSelect] = $valueSelect;
            }

            $situacaoElem = $driver->findElements(WebDriverBy::cssSelector('select#N1_var2_2 option'));
            $situacoes = [];
            foreach ($situacaoElem as $item) {
                $textSelect = $this->cleanString($item->getText());
                $valueSelect = $this->cleanString($item->getAttribute('value'));
                if ($textSelect == '' || $valueSelect == '') continue;
                $situacoes[$textSelect] = $valueSelect;
            }

            $words = $service->getTodasPalavras(true);

            $partes = [];
            $urlPadrao = $service->getUrl();
            $times = 1;
            foreach ($comarcas as $comarcaName => $comarca) {
                foreach ($situacoes as $situacaoName => $situacao) {
                    foreach ($words as $word) {
                        $this->log("{$times}: COMARCA => {$comarcaName}, SITUACAO => {$situacaoName}, PALAVRA => {$word}");
                        $link = str_replace(['[PALAVRAS]', '[COMARCA]', '[SITUACAO]', ' '], [$word, $comarca, $situacao, ''], $urlPadrao);
                        $driver->navigate()->to($link);

                        try {
                            $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('img[name=img_check]')));
                            $driver = $this->hasCaptcha($driver, 'xpath', '/html/body/form/table/tbody/tr[1]/td/div/img', '/html/body/form/table/tbody/tr[1]/td/div/input', '/html/body/form/table/tbody/tr[2]/td/input');
                        } catch (\Exception $e) {

                        }

                        try {
                            $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('span > erro')));
                            continue;
                        } catch (\Exception $e) {
                            $partesElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo > table > tbody > tr > td.texto_geral > a'));
                            $partes = $this->extractLinks($partesElem, false, false, $partes);
                        }

                        $times++;
                    }
                }
            }

            $processosLink = [];
            $index = 1;
            foreach ($partes as $parteLink) {
                try {
                    $this->log("Parte {$index} => {$parteLink}");
                    $driver->navigate()->to($parteLink);
                    $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('img[name=img_check]')));
                    $driver = $this->hasCaptcha($driver, 'xpath', '/html/body/form/table/tbody/tr[1]/td/div/img', '/html/body/form/table/tbody/tr[1]/td/div/input', '/html/body/form/table/tbody/tr[2]/td/input');
                } catch (\Exception $e) {
                }
                $processosElem = $driver->findElements(WebDriverBy::cssSelector('table tbody tr td:nth-child(1) a'));
                $processosLink = $this->extractLinks($processosElem, true, false, $processosLink);
                $this->log(' TOTAL DE PROCESSOS => ' . count($processosLink));
                $index++;
            }

            $processosLinkTratados = [];
            foreach ($processosLink as $nroProcesso => $processoLink) {
                if (in_array($nroProcesso, ['', ' '])) continue;
                $nroProcesso = $this->extractProcessNumber($nroProcesso);
                if (!preg_match('/-/', $nroProcesso)) continue;
                if (strlen($nroProcesso) < 25) continue;
                $processosLinkTratados[$nroProcesso] = $processoLink;
            }

            $total = count($processosLinkTratados);
            $this->log($total . ' processo(s) encontrado(s)');

            $linksRegistrados = $service->registerProcessos($processosLinkTratados);
            $totalLinksRegistrados = count($linksRegistrados);
            $this->log($totalLinksRegistrados . ' A PROCESSAR');

        } else {

            $linksRegistrados = $service->getTodosProcessos(0);
            $totalLinksRegistrados = count($linksRegistrados);

        }

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            try {
                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                $driver->navigate()->to($processo->getUrl());

                try {
                    try {
                        $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody/tr')));
                    } catch (\Exception $e) {
                        $driver = $this->hasCaptcha($driver, 'xpath', '/html/body/form/table/tbody/tr[1]/td/div/img', '/html/body/form/table/tbody/tr[1]/td/div/input', '/html/body/form/table/tbody/tr[2]/td/input');
                    }
                } catch (\Exception $e) {
                    $this->log($e->getMessage(), $this->logFileName);
                    continue;
                }

                $allLinksElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo a'));
                $allLinks = $this->extractLinks($allLinksElem, true, false, [], [], [
                    'Ver Processos',
                    'Ver todas as partes e advogados',
                    'Ver todas as movimentações',
                    'Ver Acórdãos e Decisões Monocráticas',
                    'Ver Outras Decisões e Despachos',
                    'Ver Notas de Expediente',
                    'Ver Último Julgamento',
                    'Ver Depósitos Judiciais',
                ]);

                //Dados Principais
                $tableDadosPrincipaisElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody/tr'));
                $tableDadosPrincipais = $this->extractVerticalTable($tableDadosPrincipaisElem, 3, false, 1);

                $tableProcessos = [];
                $tablePartesAdvs = [];
                $tableMov = [];
                $tableAcordaoDecisoes = [];
                $tableOutrasDecisoesDespachos = [];
                $tableNotasExp = [];
                $tableUltimoJulgamento = [];
                $tableDepositosJud = [];
                foreach ($allLinks as $linkName => $linkUrl) {
                    $driver->navigate()->to($linkUrl);

                    if (preg_match('/Ver Processos/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr')));
                            $tableProcessosElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableProcessos[] = $this->extractTable($tableProcessosElem, 5);
                        } catch (\Exception $e) {
                            $tableProcessosText = $this->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody'))->getText());
                            $tableProcessos[] = $tableProcessosText;
                        }
                        continue;
                    }

                    if (preg_match('/Ver todas as partes e advogados/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tablePartesAdvsElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $colsHeader = [];
                            foreach ($tablePartesAdvsElem as $tablePartesAdvsElemitem) { //rows[]
                                $colunas = $tablePartesAdvsElemitem->findElements(WebDriverBy::tagName('td'));

                                $idx = 0;
                                $row = [];
                                foreach ($colunas as $coluna) {
                                    $textCol = str_replace(':', '', $this->cleanString($coluna->getText()));
                                    if ($textCol == '') continue;
                                    if ($coluna->getAttribute('class') == 'texto_geral_negrito') {
                                        if (count($colsHeader) >= 2) $colsHeader = [];
                                        $colsHeader[] = $textCol;
                                        continue;
                                    }
                                    if (isset($colsHeader[$idx])) {
                                        @$row[$colsHeader[$idx]] = $textCol;
                                        $idx++;
                                    }
                                }

                                if (count($row) > 0) $tablePartesAdvs[] = $row;
                            }

                        } catch (\Exception $e) {
                        }
                        continue;
                    }

                    if (preg_match('/Ver todas as movimenta/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr')));
                            $tableMovElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableMov = $this->extractVerticalTable($tableMovElem, 3, true, 1);
                        } catch (\Exception $e) {
                            $tableMov[] = 'NÃO HÁ MOVIMENTAÇÕES';
                        }
                        continue;
                    }

                    if (preg_match('/Ver Acórdãos e Decisões Monocráticas/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('#conteudo div.linhad a')));
                            $tableAcordaoDecisoesElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo div.linhad a'));
                            $tableAcordaoDecisoes[] = $this->extractLinks($tableAcordaoDecisoesElem, true);
//                            $tableAcordaoDecisoes = $this->extractTable($tableAcordaoDecisoesElem, 3);
                        } catch (\Exception $e) {
                            $tableAcordaoDecisoes[] = 'NÃO HÁ INTEIRO TEOR';
                        }
                        continue;
                    }

                    if (preg_match('/Ver Outras Decis/', $linkName)) { //rever
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody')));
                            $tableOutrasDecisoesDespachosElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableOutrasDecisoesDespachos = $this->extractTable($tableOutrasDecisoesDespachosElem, 3);
                        } catch (\Exception $e) {
                            $tableOutrasDecisoesDespachos[] = 'NÃO HÁ DECISÕES E DESPACHOS';
                        }
                        continue;
                    }

                    if (preg_match('/Ver Notas de Expediente/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr')));
                            $tableNotasExpElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $tableNotasExp = $this->extractTable($tableNotasExpElem, 3);
                        } catch (\Exception $e) {
                            $tableNotasExp[] = 'NÃO HÁ NOTAS DE EXPEDIENTE';
                        }
                        if (!count($tableNotasExp)) $tableNotasExp[] = 'NÃO HÁ NOTAS DE EXPEDIENTE';
                        continue;
                    }

                    if (preg_match('/ltimo Julgamento/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[1]/tbody/tr')));
                            $tableUltimoJulgamentoElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[1]/tbody/tr'));
                            $tableUltimoJulgamentoAux = $this->extractTable($tableUltimoJulgamentoElem, 3);

                            foreach ($tableUltimoJulgamentoAux as $tableUltimoJulgamentoAuxItem) {
                                $aux = [];
                                foreach ($tableUltimoJulgamentoAuxItem as $keyItem => $valueItem) {
                                    if ($keyItem == '' || $valueItem == '') continue;
                                    $aux[$keyItem] = $valueItem;
                                }
                                $tableUltimoJulgamento[] = $aux;
                            }
                        } catch (\Exception $e) {
                            $tableUltimoJulgamento[] = 'NÃO HÁ ÚLTIMO JULGAMENTO';
                        }
                        continue;
                    }

                    if (preg_match('/sitos Judiciais/', $linkName)) {
                        try {
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr')));
                            $tableDepositosJudElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]/tbody/tr'));
                            $aux = [];
                            foreach ($tableDepositosJudElem as $tableDepositosJudElemItem) {
                                $colunas = $tableDepositosJudElemItem->findElements(WebDriverBy::tagName('td'));
                                if (count($colunas) == 1) {
                                    if (count($aux)) {
                                        $tableDepositosJud[] = $aux;
                                    } else {
                                        if ($this->cleanString($colunas[0]->getText())) $tableDepositosJud[] = $this->cleanString($colunas[0]->getText());
                                    }
                                    $aux = [];
                                    continue;
                                }
                                $trash = array_shift($colunas); //elimina a primeira coluna vazia

                                $title = str_replace(':', '', $this->cleanString($colunas[0]->getText()));
                                $value = $this->cleanString($colunas[1]->getText());
                                if ($value == '') continue;
                                $aux[$title] = $value;
                            }
//                            if (count($aux)) $tableDepositosJud[] = $aux; //finaliza
                        } catch (\Exception $e) {
                            $tableDepositosJud[] = 'NÃO HÁ DEPÓSITOS JUDICIAIS';
                        }
                        continue;
                    }
                }


            } catch (\Exception $e) {
                $this->log($e->getMessage(), $this->logFileName);
                continue;
            }

            $fieldsTratados[$processo->getProcesso()] = [
                'Dados Principais' => $tableDadosPrincipais,
                'Processos Vinculados' => $tableProcessos,
                'Partes e Advogados' => $tablePartesAdvs,
                'Movimentações' => $tableMov,
                'Acórdãos e Decisões Monocráticas' => $tableAcordaoDecisoes,
                'Outras Decisões e Despachos' => $tableOutrasDecisoesDespachos,
                'Notas de Expediente' => $tableNotasExp,
                'Último Julgamento' => $tableUltimoJulgamento,
                'Depósitos Judiciais' => $tableDepositosJud,
            ];

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjcefisico($service, $processarApenasProcessos = false)
    {
        $processarApenasProcessos = true; //TEMPORARIO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());

        $linksRegistrados = [];
        if (!$processarApenasProcessos) {

            $processosLink = [];
            foreach ($service->getTodasPalavras() as $word) {

                $this->log("PESQUISANDO POR {$word}");

                $link = str_replace('[PALAVRAS]', $word, $service->getUrl());
                $driver->navigate()->to($link);

                $processosElem = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr > td > a:nth-child(2)'));
                $processosLink = $this->extractLinks($processosElem, true, false, $processosLink);

                $this->log("TOTAL PROCESSOS => " . count($processosLink));

            }

            $this->log("TRATANDO NRO DE PROCESSO");
            $processosLinkTratados = [];
            foreach ($processosLink as $processoNro => $procLink) {
                $aux = explode(' -', $processoNro);
                $nro = (count($aux) == 2) ? $aux[0] : $processoNro;
                $processosLinkTratados[$nro] = $procLink;
            }

            $this->log("TOTAL PROCESSOS => " . count($processosLinkTratados));
            $linksRegistrados = $service->registerProcessos($processosLinkTratados);

        } else {
            $linksRegistrados = $service->getTodosProcessos(0);
        }

        $totalLinksRegistrados = count($linksRegistrados);
        $this->log("TOTAL PROCESSOS REGISTRADOS => " . $totalLinksRegistrados);

        $fieldsTratados = [];
        /** @var $linksRegistrados \Tribunal\Service\Processo[] */
        $index = 1;
        foreach ($linksRegistrados as $processo) {

            $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
            $index++;
            $hashAnterior = $processo->getHash();

            try {
                $driver->navigate()->to($processo->getUrl());
                $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table[3]')));
            } catch (\Exception $e) {
                $this->log($e->getMessage(), $this->logFileName);
                continue;
            }

            $dadosGerais = [];
            $partes = [];
            $distribuicoes = [];
            $recursos = [];
            $peticoesAcompanhamento = [];
            $movimentacoes = [];


            //DADOS GERAIS
            try {
                $geraisElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[3]/tbody/tr/td'));
                foreach ($geraisElem as $geralItem) {
                    $texto = $this->cleanString($geralItem->getText());
                    if (preg_match('/Dados Gerais/', $texto)) continue;

                    if (preg_match('/: /', $texto)) {
                        $splitText = explode(': ', $texto);
                        if (count($splitText) < 2) continue;
                        $dadosGerais[$splitText[0]] = $splitText[1];
                    }
                }
            } catch (\Exception $e) {
                $dadosGerais[] = 'NÃO HÁ DADOS GERAIS';
                $this->log($e->getMessage(), $this->logFileName);
            }

            //PARTES
            try {
                $partesElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr/td/font'));
                $chavePartes = '';
                foreach ($partesElem as $partesElemItem) {
                    $texto = $this->cleanString($partesElemItem->getText());

                    if (preg_match('/Recorrente /', $texto)) {
                        $chavePartes = 'Requerente';
                    }
                    if (preg_match('/Recorrido /', $texto)) {
                        $chavePartes = 'Requerido';
                    }
                    if (preg_match('/Requerente /', $texto)) {
                        $chavePartes = 'Requerente';
                    }
                    if (preg_match('/Requerido /', $texto)) {
                        $chavePartes = 'Requerido';
                    }
                    if ($chavePartes) $partes[$chavePartes][] = $this->cleanString(str_replace(['Requerente :', 'Requerido :'], '', $texto));
                }
            } catch (\Exception $e) {
                $partes[] = 'NÃO HÁ PARTES';
                $this->log($e->getMessage(), $this->logFileName);
            }

            //DISTRIBUICOES
            try {
                $distribuicoesElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[5]/tbody/tr[2]/td'));
                foreach ($distribuicoesElem as $distribuicoesElemItem) {
                    $texto = $distribuicoesElemItem->getText();
                    $textoArr = explode("\n", $texto);
                    $distribuicoesRow = [];
                    foreach ($textoArr as $textoArrItem) {
                        if (preg_match('/: /', $textoArrItem)) {
                            $auxArrText = explode(': ', $textoArrItem);
                            if (count($auxArrText) < 2) continue;
                            $distribuicoesRow[$this->cleanString($auxArrText[0])] = $this->cleanString($auxArrText[1]);
                        }
                    }
                    if (count($distribuicoesRow)) $distribuicoes[] = $distribuicoesRow;
                }

            } catch (\Exception $e) {
                $distribuicoes[] = 'NÃO HÁ DISTRIBUIÇÕES';
                $this->log($e->getMessage(), $this->logFileName);
            }

            //RECURSOS
            try {
                $tableRecursosElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[6]/tbody/tr'));
                $trash = array_shift($tableRecursosElem);
                $recursos = $this->extractTable($tableRecursosElem, 5);
            } catch (\Exception $e) {
                $recursos[] = 'NÃO HÁ RECURSOS';
                $this->log($e->getMessage(), $this->logFileName);
            }

            //PETIÇÕES DE ACOMPANHAMENTO
            try {
                $peticoesAcompanhamentoElem = $driver->findElements(WebDriverBy::xpath('/html/body/table[7]/tbody/tr'));
                $trash = array_shift($peticoesAcompanhamentoElem);
                $peticoesAcompanhamento = $this->extractTable($peticoesAcompanhamentoElem, 4);
            } catch (\Exception $e) {
                $peticoesAcompanhamento[] = 'NÃO HÁ PETIÇÕES DE ACOMPANHAMENTO';
                $this->log($e->getMessage(), $this->logFileName);
            }

            //MOVIMENTACOES
            try {
                $movTableElem = $driver->findElements(WebDriverBy::xpath('//*[@id="LISTA"]/tbody/tr'));
                $trash = array_shift($movTableElem);
                $movimentacoes = $this->extractTable($movTableElem, 4);
            } catch (\Exception $e) {
                $movimentacoes[] = 'NÃO HÁ MOVIMENTAÇÕES';
                $this->log($e->getMessage(), $this->logFileName);
            }


            $fieldsTratados[$processo->getProcesso()] = [
                'Dados Gerais' => $dadosGerais,
                'Partes' => $partes,
                'Distribuições' => $distribuicoes,
                'Recursos' => $recursos,
                'Petições de Acompanhamento' => $peticoesAcompanhamento,
                'Movimentações' => $movimentacoes,
            ];

            $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
            $processo->setHash(md5($processo->getDados()));
            if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
            $processo->setProcessado(1);
            $processo->salvar(true);
        }

        $driver->quit();
        return true;
    }

    public function tjes1instancia($service, $processarApenasProcessos = false)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Processo[] */

        $processosProcessados = [];
        $processosNaoProcessados = [];
        $processos = $service->getTodosProcessos();

        foreach ($processos as $processo) {
            if ($processo->getProcessado() == '1') {
                $processosProcessados[$processo->getProcesso()] = $processo;
                continue;
            }
            $processosNaoProcessados[$processo->getProcesso()] = $processo;
        }
        $this->log(count($processosProcessados) . ' PROCESSADOS');
        $this->log(count($processosNaoProcessados) . ' NÃO PROCESSADOS');

        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());

        $words = $service->getTodasPalavras();
        $seJuizo = ['1', '2'];
        $seSituacao = ['1' => 'Processos ativos', '2' => 'Arquivados, Baixados e Destruídos', '3' => 'Tansitado em julgado'];

        $processosTratados = [];

        try {
            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('select#seInstancia')));
            $driver->findElement(WebDriverBy::cssSelector('select#sePesquisar'))->sendKeys('Nome da Parte');
            sleep(2);

            $processosEncontrados = 0;
            foreach ($words as $word) {
                foreach ($seSituacao as $situacao) {
                    foreach ($seJuizo as $juizo) {

                        //CHECK SE CAPTCHA RENDERIZOU
                        try {
                            while (preg_match('/Não foi possível carregar a imagem./', $driver->findElement(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/table/tbody/tr/td[1]'))->getText())) {
                                $this->log('CAPTCHA NÃO CARREGADO. ATUALIZANDO A PAGINA...');
                                $driver->navigate()->refresh();
                                sleep(1);
                            }
                        } catch (\Exception $e) {
                        }

                        $driver->findElement(WebDriverBy::cssSelector('input#edNome'))->clear();
                        $driver->findElement(WebDriverBy::cssSelector('input#edNome'))->sendKeys($word);
                        $driver->findElement(WebDriverBy::cssSelector('input[name=seJuizo][value="' . $juizo . '"]'))->click();
                        $driver->findElement(WebDriverBy::cssSelector('select#seSituacao'))->sendKeys($situacao);
                        $driver->findElement(WebDriverBy::cssSelector('select#seTipoParte'))->sendKeys('Requerido');
                        $anoInicial = date('Y') - 15;
                        $anoInicial = '01/01/' . $anoInicial;
                        $this->log($anoInicial);
                        $driver->findElement(WebDriverBy::cssSelector('input#edDataIni'))->clear();
                        $driver->findElement(WebDriverBy::cssSelector('input#edDataIni'))->sendKeys($anoInicial);

                        $driver = $this->hasCaptcha($driver, 'cssSelector', 'form > table > tbody > tr > td > img', 'input[name=userInput]', 'input#buPesquisar', false);
                        //CHECK IF STILL HAS CAPTCHA
                        try {
                            $img = $driver->findElement(WebDriverBy::cssSelector('form > table > tbody > tr > td > img'));
                            $driver->quit();
                            $this->log('REINICIANDO CAPTURA...');
                            return $this->tjes1instancia($service);
                        } catch (\Exception $e) {
                        }

                        $driver->findElement(WebDriverBy::cssSelector('input#buPesquisar'))->click();

                        $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table > tbody > tr > td:nth-child(1).label_cons')));
                        $processosElem = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr > td:nth-child(1).label_cons'));
                        $totalLinks = count($processosElem);
                        $this->log($totalLinks . ' linhas');
                        $processosEncontrados = 0;
                        foreach ($processosElem as $indexElem => $processosElemItem) {
                            try {
                                $nroUnico = $this->extractProcessNumber($processosElemItem->getText(), false);
                                if (!$nroUnico) continue;
                                $processosEncontrados++;

                                $this->log("({$this->percentage($totalLinks,$indexElem)}) {$indexElem} de {$totalLinks} => " . $nroUnico);

                                if (isset($processosProcessados[$nroUnico])) continue;

                                $processosTratados[$nroUnico] = [];
                                $processosElemItem->findElement(WebDriverBy::cssSelector('input.campo_ob_cons'))->click();
                                sleep(1);
                                @$driver->switchTo()->window(end($driver->getWindowHandles()));

                                //CHECK IF STILL HAS CAPTCHA
                                try {
                                    $img = $driver->findElement(WebDriverBy::cssSelector('form > table > tbody > tr > td > img'));
                                    $driver->quit();
                                    $this->log('REINICIANDO CAPTURA...');
                                    return $this->tjes1instancia($service);
                                } catch (\Exception $e) {
                                }

                                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('#conteudo table tbody tr:not(.andamentos)')));

                                //CAPTURANDO OS DADOS GERAIS
                                $this->log('CAPTURANDO OS DADOS GERAIS');
                                $linhasDadosElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo table tbody tr:not(.andamentos)'));
                                $dadosGeraisAux = [];
                                $dadosGeraisSubtitulo = '';
                                foreach ($linhasDadosElem as $linhasDadosElemItem) {
                                    if ($this->cleanString($linhasDadosElemItem->getText()) == '') continue;
                                    $colunas = $linhasDadosElemItem->findElements(WebDriverBy::tagName('td'));
                                    if (preg_match('/label_subtitulo/', $colunas[0]->getAttribute('class'))) {
                                        $dadosGeraisSubtitulo = $this->cleanString($colunas[0]->getText());
                                        continue;
                                    }
                                    if ($dadosGeraisSubtitulo) {
                                        $dadosGeraisAux[$dadosGeraisSubtitulo] = [];
                                        if ($dadosGeraisSubtitulo == 'Partes do Processo') {
                                            $textArr = explode("\n", $colunas[0]->getText());
                                            $tituloSelecionado = '';
                                            foreach ($textArr as $textArrItem) {
                                                $text = $this->cleanString($textArrItem);
                                                if ($text == '') continue;
                                                if (in_array($text, [
                                                    'AGVDO',
                                                    'AGVTE',
                                                    'APDO/APTE',
                                                    'APTE/APDO',
                                                    'APTE',
                                                    'APDO',
                                                    'Litisdenunciado',
                                                    'Litisconsorte Passivo',
                                                    'Assistente Simples Passivo',
                                                    'Requerente',
                                                    'Requerido',
                                                    'Embargado',
                                                    'Embargante',
                                                    'Litisdenunciado',
                                                    'Litisconsorte Passivo',
                                                    'Assistente Simples Passivo',
                                                    'Requerente',
                                                    'Requerido',
                                                    'Embargado',
                                                    'Embargante',
                                                    'Executado',
                                                    'Exequente'
                                                ])) {
                                                    $tituloSelecionado = $text;
                                                    continue;
                                                }
                                                $dadosGeraisAux[$dadosGeraisSubtitulo][$tituloSelecionado][] = $text;
                                            }
                                            $dadosGeraisSubtitulo = '';
                                            continue;
                                        }
                                        if (preg_match('/Distribui/', $dadosGeraisSubtitulo)) {
                                            foreach ($colunas as $coluna) {
                                                $text = $coluna->getText();
                                                $textArr = explode(': ', $text);
                                                $dadosGeraisAux[$dadosGeraisSubtitulo][$this->cleanString($textArr[0])] = $this->cleanString($textArr[1]);
                                            }
                                            $dadosGeraisSubtitulo = '';
                                            continue;
                                        }
                                    }
                                    foreach ($colunas as $coluna) {
                                        $text = $coluna->getText();
                                        if (!preg_match('/: /', $text)) continue;
                                        $textArr = explode(': ', $text);
                                        $dadosGeraisAux[$this->cleanString($textArr[0])] = $this->cleanString($textArr[1]);
                                    }
                                }
                                $processosTratados[$nroUnico]['Dados Gerais'] = $dadosGeraisAux;

                                //CAPTURANDO OS ANDAMENTOS
                                $this->log('CAPTURANDO OS ANDAMENTOS');
                                $linhasAndamentoElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo table tbody tr.andamentos'));
                                $andamentoAux = [];
                                foreach ($linhasAndamentoElem as $linhasAndamentoElemItem) {
                                    $andamentoAux = [];
                                    $spans = $linhasAndamentoElemItem->findElements(WebDriverBy::tagName('span'));

                                    $andamentoAux['Data'] = $this->cleanString($spans[0]->getText());
                                    $andamentoAux['Andamento'] = $this->cleanString($spans[1]->getText());
                                    $andamentoAux['Obs'] = $this->cleanString($spans[2]->getText());
                                    try {
                                        $formElem = $linhasAndamentoElemItem->findElement(WebDriverBy::cssSelector('form'));
                                        $action = $formElem->getAttribute('action');

                                        //AUDIÊNCIA
                                        if (preg_match('/ver_audiencia.cfm/', $action)) {
                                            $this->log('CAPTURANDO AUDIÊNCIA');

                                            $formElem->findElement(WebDriverBy::cssSelector('input[type="submit"]'))->click();
                                            sleep(1);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr')));
                                            $audienciaElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                                            $getNext = false;
                                            foreach ($audienciaElem as $audienciaElemItem) {
                                                $colunas = $audienciaElemItem->findElements(WebDriverBy::cssSelector('td'));

                                                if ($getNext) {
                                                    $textAllColunas = $this->cleanString($audienciaElemItem->getText());
                                                    if ($textAllColunas == '') continue;
                                                    @$andamentoAux['Audiência'] .= $textAllColunas;
                                                    continue;
                                                }
                                                if (preg_match('/Audiência/', $colunas[0]->getText())) {
                                                    $getNext = true;
                                                }
                                            }
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }

                                        //DECISÃO
                                        if (preg_match('/ver_decisao_new.cfm/', $action)) {
                                            $this->log('CAPTURANDO DECISÃO');

                                            $formElem->findElement(WebDriverBy::cssSelector('input[type="submit"]'))->click();
                                            sleep(1);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr')));
                                            $decisaoElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                                            $getNext = false;
                                            foreach ($decisaoElem as $decisaoElemItem) {
                                                $colunas = $decisaoElemItem->findElements(WebDriverBy::cssSelector('td'));

                                                if ($getNext) {
                                                    $textAllColunas = $this->cleanString($decisaoElemItem->getText());
                                                    if ($textAllColunas == '') continue;
                                                    @$andamentoAux['Decisão'] .= $textAllColunas;
                                                    continue;
                                                }
                                                if (preg_match('/Decisão/', $colunas[0]->getText())) {
                                                    $getNext = true;
                                                }
                                            }
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }

                                        //SENTENÇA
                                        if (preg_match('/ver_sentenca_new.cfm/', $action)) {
                                            $this->log('CAPTURANDO SENTENÇA');

                                            $formElem->findElement(WebDriverBy::cssSelector('input[type="submit"]'))->click();
                                            sleep(1);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[18]/td/table[3]')));
                                            $andamentoAux['Sentença'] = $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr[18]/td/table[3]'))->getText();
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }

                                        //DESPACHO
                                        if (preg_match('/ver_despacho_new.cfm/', $action)) {
                                            $this->log('CAPTURANDO DESPACHO');

                                            $formElem->findElement(WebDriverBy::cssSelector('input[type="submit"]'))->click();
                                            sleep(1);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr')));
                                            $despachosElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                                            $getNext = false;
                                            foreach ($despachosElem as $despachosElemItem) {
                                                $colunas = $despachosElemItem->findElements(WebDriverBy::cssSelector('td'));

                                                if ($getNext) {
                                                    $textAllColunas = $this->cleanString($despachosElemItem->getText());
                                                    if ($textAllColunas == '') continue;
                                                    @$andamentoAux['Despacho'] .= $textAllColunas;
                                                    continue;
                                                }
                                                if (preg_match('/Despacho/', $colunas[0]->getText())) {
                                                    $getNext = true;
                                                }
                                            }
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }

                                    } catch (\Exception $e) {
                                        if (count($driver->getWindowHandles()) > 2) {
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }
                                    }

                                    $processosTratados[$nroUnico]['Andamentos'][] = $andamentoAux;
                                }

                                //CUSTAS
                                $processosTratados[$nroUnico]['Situação das Custas'] = [];
                                try {
                                    try {
                                        $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('input.btnCustas')));
                                        $driver->findElement(WebDriverBy::cssSelector('input.btnCustas'))->click();
                                        sleep(1);
                                        @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        if (!preg_match('/NÃO EXISTEM CUSTAS CALCULADAS PARA ESTE PROCESSO/', $driver->findElement(WebDriverBy::xpath('/html/body/div/font'))->getText())) {
                                            throw new \Exception('TEM CUSTAS!');
                                        } else {
                                            $this->log('NÃO EXISTEM CUSTAS CALCULADAS PARA ESTE PROCESSO');
                                            $processosTratados[$nroUnico]['Situação das Custas'] = ['NÃO EXISTEM CUSTAS CALCULADAS PARA ESTE PROCESSO'];
                                        }
                                    } catch (\Exception $e) {
                                        $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr')));
                                        $tableRows = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr'));
                                        $trash = array_shift($tableRows);
                                        $processosTratados[$nroUnico]['Situação das Custas'] = $this->extractTable($tableRows, 7, true);
                                    }
                                    $driver->executeScript("window.close()", []);
                                    @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                } catch (\Exception $e) {
                                    $this->log($e->getMessage());
                                }

                                /** @var $processoService \Tribunal\Service\Processo */
                                $processoService = null;
                                if (isset($processosNaoProcessados[$nroUnico])) {
                                    $processoService = $processosNaoProcessados[$nroUnico];
                                } else {
                                    if (isset($processosProcessados[$nroUnico])) {
                                        $processoService = $processosProcessados[$nroUnico];
                                    } else {
                                        $processoService = new Processo();
                                        $processoService->setProcesso($nroUnico);
                                        $processoService->setIdTribunal($service->getId());
                                        $processoService->setUrl($service->getUrl());
                                        $processoService->setProcessado(0);
                                        $processoService->salvar(true);

                                        $processosProcessados[$nroUnico] = $processoService;
                                    }
                                }

                                $hashAnterior = $processoService->getHash();
                                $processoService->setDados(json_encode($processosTratados[$nroUnico]));
                                $processoService->setHash(md5($processoService->getDados()));
                                if ($hashAnterior != $processoService->getHash()) $processoService->setAlteracao(1);
                                $processoService->setProcessado(1);
                                $processoService->salvar(true);

                                $this->log("PROCESSO {$processoService->getProcesso()} PROCESSADO.");

                                $driver->executeScript("window.close()", []);
                                @$driver->switchTo()->window(reset($driver->getWindowHandles()));

                            } catch (\Exception $e) {
                                $this->log($e->getMessage());
                                $driver->executeScript("window.close()", []);
                                @$driver->switchTo()->window(reset($driver->getWindowHandles()));
                                continue;
                            }
                        }
                    }
                }
            }

            $this->log("{$processosEncontrados} PROCESSOS ENCONTRADOS.");
            $this->log(count($processosTratados) . ' PROCESSOS REGISTRADOS.');

        } catch (\Exception $e) {
            $this->log($e->getMessage(), $this->logFileName);
            return true;
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);

        die;//temporario
        return true;
    }

    public function tjes2instancia($service, $processarApenasProcessos = false)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Processo[] */

        $processosProcessados = [];
        $processosNaoProcessados = [];
        $processos = $service->getTodosProcessos();

        foreach ($processos as $processo) {
            if ($processo->getProcessado() == '1') {
                $processosProcessados[$processo->getProcesso()] = $processo;
                continue;
            }
            $processosNaoProcessados[$processo->getProcesso()] = $processo;
        }
        $this->log(count($processosProcessados) . ' PROCESSADOS');
        $this->log(count($processosNaoProcessados) . ' NÃO PROCESSADOS');

        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());

        $words = $service->getTodasPalavras();
        $seJuizo = ['1', '2'];
        $seSituacao = ['1' => 'Processos ativos', '2' => 'Arquivados, Baixados e Destruídos', '3' => 'Tansitado em julgado'];

        $processosTratados = [];

        try {
            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('select#seInstancia')));
            $driver->findElement(WebDriverBy::cssSelector('select#sePesquisar'))->sendKeys('Nome da Parte');
            sleep(2);

            $processosEncontrados = 0;
            foreach ($words as $word) {
                foreach ($seSituacao as $situacao) {
                    foreach ($seJuizo as $juizo) {

                        //CHECK SE CAPTCHA RENDERIZOU
                        try {
                            while (preg_match('/Não foi possível carregar a imagem./', $driver->findElement(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[7]/td[2]/table/tbody/tr/td[1]'))->getText())) {
                                $this->log('CAPTCHA NÃO CARREGADO. ATUALIZANDO A PAGINA...');
                                $driver->navigate()->refresh();
                                sleep(1);
                            }
                        } catch (\Exception $e) {
                        }

                        $driver->findElement(WebDriverBy::cssSelector('select#seInstancia'))->sendKeys('2ª Instância');
                        $driver->findElement(WebDriverBy::cssSelector('input#edNome'))->clear();
                        $driver->findElement(WebDriverBy::cssSelector('input#edNome'))->sendKeys($word);
                        $driver->findElement(WebDriverBy::cssSelector('select#seSituacao'))->sendKeys($situacao);
                        $driver->findElement(WebDriverBy::cssSelector('select#seTipoParte'))->sendKeys('Requerido');
                        $anoInicial = date('Y') - 15;
                        $anoInicial = '01/01/' . $anoInicial;
                        $this->log($anoInicial);
                        $driver->findElement(WebDriverBy::cssSelector('input#edDataIni'))->clear();
                        $driver->findElement(WebDriverBy::cssSelector('input#edDataIni'))->sendKeys($anoInicial);

                        $driver = $this->hasCaptcha($driver, 'cssSelector', 'form > table > tbody > tr > td > img', 'input[name=userInput]', 'input#buPesquisar', false);
                        try {
                            $img = $driver->findElement(WebDriverBy::cssSelector('form > table > tbody > tr > td > img'));
                            $driver->quit();
                            $this->log('REINICIANDO CAPTURA...');
                            return $this->tjes2instancia($service);
                        } catch (\Exception $e) {
                        }

                        $driver->findElement(WebDriverBy::cssSelector('input#buPesquisar'))->click();

                        $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table > tbody > tr > td:nth-child(1).label_cons')));
                        $processosElem = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr > td:nth-child(1).label_cons'));
                        $totalLinks = count($processosElem);
                        $this->log($totalLinks . ' linhas');
                        $processosEncontrados = 0;
                        foreach ($processosElem as $indexElem => $processosElemItem) {
                            try {
                                $nroUnico = $this->extractProcessNumber($processosElemItem->getText(), false);
                                if (!$nroUnico) continue;
                                $processosEncontrados++;

                                $this->log("({$this->percentage($totalLinks,$indexElem)}) {$indexElem} de {$totalLinks} => " . $nroUnico);

                                if (isset($processosProcessados[$nroUnico])) continue;

                                $processosTratados[$nroUnico] = [];
                                $processosElemItem->findElement(WebDriverBy::cssSelector('input.campo_ob_cons'))->click();
                                sleep(1);
                                @$driver->switchTo()->window(end($driver->getWindowHandles()));

                                //CHECK IF STILL HAS CAPTCHA
                                try {
                                    $img = $driver->findElement(WebDriverBy::cssSelector('form > table > tbody > tr > td > img'));
                                    $driver->quit();
                                    $this->log('REINICIANDO CAPTURA...');
                                    return $this->tjes2instancia($service);
                                } catch (\Exception $e) {
                                }

                                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('#conteudo table tbody tr:not(.andamentos)')));

                                //CAPTURANDO OS DADOS GERAIS
                                $this->log('CAPTURANDO OS DADOS GERAIS');
                                $linhasDadosElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo table tbody tr:not(.andamentos)'));
                                $dadosGeraisAux = [];
                                $dadosGeraisSubtitulo = '';
                                foreach ($linhasDadosElem as $linhasDadosElemItem) {
                                    if ($this->cleanString($linhasDadosElemItem->getText()) == '') continue;
                                    $colunas = $linhasDadosElemItem->findElements(WebDriverBy::tagName('td'));
                                    if (preg_match('/label_subtitulo/', $colunas[0]->getAttribute('class'))) {
                                        $dadosGeraisSubtitulo = $this->cleanString($colunas[0]->getText());
                                        continue;
                                    }
                                    if ($dadosGeraisSubtitulo) {
                                        $dadosGeraisAux[$dadosGeraisSubtitulo] = [];
                                        if ($dadosGeraisSubtitulo == 'Partes do Processo') {
                                            $textArr = explode("\n", $colunas[0]->getText());
                                            $tituloSelecionado = '';
                                            foreach ($textArr as $textArrItem) {
                                                $text = $this->cleanString($textArrItem);
                                                if ($text == '') continue;
                                                if (in_array($text, [
                                                    'AGVDO',
                                                    'AGVTE',
                                                    'APDO/APTE',
                                                    'APTE/APDO',
                                                    'APTE',
                                                    'APDO',
                                                    'Litisdenunciado',
                                                    'Litisconsorte Passivo',
                                                    'Assistente Simples Passivo',
                                                    'Requerente',
                                                    'Requerido',
                                                    'Embargado',
                                                    'Embargante',
                                                    'Litisdenunciado',
                                                    'Litisconsorte Passivo',
                                                    'Assistente Simples Passivo',
                                                    'Requerente',
                                                    'Requerido',
                                                    'Embargado',
                                                    'Embargante',
                                                    'Executado',
                                                    'Exequente'
                                                ])) {
                                                    $tituloSelecionado = $text;
                                                    continue;
                                                }
                                                $dadosGeraisAux[$dadosGeraisSubtitulo][$tituloSelecionado][] = $text;
                                            }
                                            $dadosGeraisSubtitulo = '';
                                            continue;
                                        }
                                        if (preg_match('/Distribui/', $dadosGeraisSubtitulo)) {
                                            foreach ($colunas as $coluna) {
                                                $text = $coluna->getText();
                                                $textArr = explode(': ', $text);
                                                $dadosGeraisAux[$dadosGeraisSubtitulo][$this->cleanString($textArr[0])] = $this->cleanString($textArr[1]);
                                            }
                                            $dadosGeraisSubtitulo = '';
                                            continue;
                                        }
                                    }
                                    foreach ($colunas as $coluna) {
                                        $text = $coluna->getText();
                                        if (!preg_match('/: /', $text)) continue;
                                        $textArr = explode(': ', $text);
                                        $dadosGeraisAux[$this->cleanString($textArr[0])] = $this->cleanString($textArr[1]);
                                    }
                                }
                                $processosTratados[$nroUnico]['Dados Gerais'] = $dadosGeraisAux;

                                //CAPTURANDO OS ANDAMENTOS
                                $this->log('CAPTURANDO OS ANDAMENTOS');
                                $linhasAndamentoElem = $driver->findElements(WebDriverBy::cssSelector('#conteudo table tbody tr.andamentos'));
                                $andamentoAux = [];
                                foreach ($linhasAndamentoElem as $linhasAndamentoElemItem) {
                                    $andamentoAux = [];
                                    $spans = $linhasAndamentoElemItem->findElements(WebDriverBy::tagName('span'));

                                    $andamentoAux['Data'] = $this->cleanString($spans[0]->getText());
                                    $andamentoAux['Andamento'] = $this->cleanString($spans[1]->getText());
                                    $andamentoAux['Obs'] = $this->cleanString($spans[2]->getText());
                                    try {
                                        $formElem = $linhasAndamentoElemItem->findElement(WebDriverBy::cssSelector('form'));
                                        $action = $formElem->getAttribute('action');

                                        //DECISÃO
                                        if (preg_match('/det_decinter.cfm/', $action)) {
                                            $this->log('CAPTURANDO DECISÃO');

                                            $formElem->findElement(WebDriverBy::cssSelector('input[type="submit"]'))->click();
                                            sleep(1);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr')));
                                            $decisaoElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                                            $getNext = false;
                                            foreach ($decisaoElem as $decisaoElemItem) {
                                                $colunas = $decisaoElemItem->findElements(WebDriverBy::cssSelector('td'));

                                                if ($getNext) {
                                                    $textAllColunas = $this->cleanString($decisaoElemItem->getText());
                                                    if ($textAllColunas == '') continue;
                                                    @$andamentoAux['Decisão'] .= $textAllColunas;
                                                    continue;
                                                }
                                                if (preg_match('/Decisão/', $colunas[0]->getText())) {
                                                    $getNext = true;
                                                }
                                            }
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }

                                        //ACORDÃO
                                        if (preg_match('/consulta_jurisprudencia/', $action)) {
                                            $this->log('CAPTURANDO ACORDÃO');
                                            $formElem->findElement(WebDriverBy::cssSelector('input[type="submit"]'))->click();
                                            sleep(1);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]')));
                                            @$andamentoAux['Acordão'] = $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]'))->getText();
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }

                                    } catch (\Exception $e) {
                                        if (count($driver->getWindowHandles()) > 2) {
                                            $driver->executeScript("window.close()", []);
                                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        }
                                    }

                                    $processosTratados[$nroUnico]['Andamentos'][] = $andamentoAux;
                                }

                                //CUSTAS
                                $processosTratados[$nroUnico]['Situação das Custas'] = [];
                                try {
                                    try {
                                        $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('input.btnCustas')));
                                        $driver->findElement(WebDriverBy::cssSelector('input.btnCustas'))->click();
                                        sleep(1);
                                        @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                        if (!preg_match('/NÃO EXISTEM CUSTAS CALCULADAS PARA ESTE PROCESSO/', $driver->findElement(WebDriverBy::xpath('/html/body/div/font'))->getText())) {
                                            throw new \Exception('TEM CUSTAS!');
                                        } else {
                                            $this->log('NÃO EXISTEM CUSTAS CALCULADAS PARA ESTE PROCESSO');
                                            $processosTratados[$nroUnico]['Situação das Custas'] = ['NÃO EXISTEM CUSTAS CALCULADAS PARA ESTE PROCESSO'];
                                        }
                                    } catch (\Exception $e) {
                                        $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr')));
                                        $tableRows = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr'));
                                        $trash = array_shift($tableRows);
                                        $processosTratados[$nroUnico]['Situação das Custas'] = $this->extractTable($tableRows, 7, true);
                                    }
                                    $driver->executeScript("window.close()", []);
                                    @$driver->switchTo()->window(end($driver->getWindowHandles()));
                                } catch (\Exception $e) {
                                    $this->log($e->getMessage());
                                }

                                /** @var $processoService \Tribunal\Service\Processo */
                                $processoService = null;
                                if (isset($processosNaoProcessados[$nroUnico])) {
                                    $processoService = $processosNaoProcessados[$nroUnico];
                                } else {
                                    if (isset($processosProcessados[$nroUnico])) {
                                        $processoService = $processosProcessados[$nroUnico];
                                    } else {
                                        $processoService = new Processo();
                                        $processoService->setProcesso($nroUnico);
                                        $processoService->setIdTribunal($service->getId());
                                        $processoService->setUrl($service->getUrl());
                                        $processoService->setProcessado(0);
                                        $processoService->salvar(true);

                                        $processosProcessados[$nroUnico] = $processoService;
                                    }
                                }

                                $hashAnterior = $processoService->getHash();
                                $processoService->setDados(json_encode($processosTratados[$nroUnico]));
                                $processoService->setHash(md5($processoService->getDados()));
                                if ($hashAnterior != $processoService->getHash()) $processoService->setAlteracao(1);
                                $processoService->setProcessado(1);
                                $processoService->salvar(true);

                                $this->log("PROCESSO {$processoService->getProcesso()} PROCESSADO.");

                                $driver->executeScript("window.close()", []);
                                @$driver->switchTo()->window(reset($driver->getWindowHandles()));

                            } catch (\Exception $e) {
                                $this->log($e->getMessage());
                                $driver->executeScript("window.close()", []);
                                @$driver->switchTo()->window(reset($driver->getWindowHandles()));
                                continue;
                            }
                        }
                    }
                }
            }

            $this->log("{$processosEncontrados} PROCESSOS ENCONTRADOS.");
            $this->log(count($processosTratados) . ' PROCESSOS REGISTRADOS.');

        } catch (\Exception $e) {
            $this->log($e->getMessage(), $this->logFileName);
            return true;
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);

        die;//temporario
        return true;
    }

    public function tjma1instancia($service, $processarApenasProcessos = false)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Processo[] */

        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());

        $processosProcessados = [];
        $processosNaoProcessados = [];
        $processos = $service->getTodosProcessos();

        foreach ($processos as $processo) {
            if ($processo->getProcessado() == '1') {
                $processosProcessados[$processo->getProcessoInterno()] = $processo;
                continue;
            }
            $processosNaoProcessados[$processo->getProcessoInterno()] = $processo;
        }
        $this->log(count($processosProcessados) . ' PROCESSADOS');
        $this->log(count($processosNaoProcessados) . ' NÃO PROCESSADOS');

        $words = $service->getTodasPalavras();
        foreach ($words as $word) {
            $driver->navigate()->to($service->getUrl());

            try {
                $driver->findElement(WebDriverBy::xpath('/html/body/div[4]/div[11]/div/button'))->click();
            } catch (\Exception $e) {
            }

            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="primeiroGrau"]')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="primeiroGrau"]'))->click();
            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="main-menu"]/li[2]/ul/li[1]/a')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="main-menu"]/li[2]/ul/li[1]/a'))->click();

            try {
                $driver->findElement(WebDriverBy::xpath('/html/body/div[4]/div[11]/div/button'))->click();
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="slcComarca"]'))->sendKeys('TODAS');
            $driver->findElement(WebDriverBy::xpath('//*[@id="slcTipo"]'))->sendKeys('Parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="txtChave"]'))->sendKeys($word);

            $driver = $this->ocr($driver, 'xpath', '//*[@id="formConsultaPublica"]/ul/li[5]/img', '//*[@id="txtCaptcha"]', '//*[@id="btnConsultar"]', true, false);
            while (true) {
                try {
                    $error = $driver->findElement(WebDriverBy::cssSelector('#content > div.message > div.error > p'))->getText();
                    $this->log($error . ' => OCR Captcha trying again...');
                    $driver->navigate()->refresh();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="slcComarca"]'))->sendKeys('TODAS');
                    $driver->findElement(WebDriverBy::xpath('//*[@id="slcTipo"]'))->sendKeys('Parte');
                    $driver->findElement(WebDriverBy::xpath('//*[@id="txtChave"]'))->sendKeys($word);
                    $driver = $this->ocr($driver, 'xpath', '//*[@id="formConsultaPublica"]/ul/li[5]/img', '//*[@id="txtCaptcha"]', '//*[@id="btnConsultar"]', true, false);
                } catch (\Exception $e) {
                    break;
                }
            }

            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::tagName('table')));

            $driver->findElement(WebDriverBy::xpath('//*[@id="linkTodos"]'))->click();
            $driver->wait(15)->until(WebDriverExpectedCondition::invisibilityOfElementLocated(WebDriverBy::xpath('//*[@id="pagination"]')));
            $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::tagName('table')));

            $totalCheckboxes = count($driver->findElements(WebDriverBy::cssSelector('#formConsultaPartes > table > tbody > tr > td > input[type="checkbox"]')));

            $totalCheckboxesAux = [];
            $range = 10;
            for ($iCheckbox = 0; $iCheckbox < $totalCheckboxes; $iCheckbox++) {
                $aux[] = $iCheckbox;
                if (count($aux) == $range) {
                    $totalCheckboxesAux[] = $aux;
                    $aux = [];
                } else {
                    if (($totalCheckboxes - 1) == $iCheckbox && count($aux) <= $range) {
                        $totalCheckboxesAux[] = $aux;
                        $aux = [];
                    }
                }

            }

            $partesProcessadas = [];
            foreach ($totalCheckboxesAux as $rangeCheckbox) {

                $partesProcessadas[] = array_merge($partesProcessadas, $rangeCheckbox);

                $this->log('PARTES => ' . $this->percentage($totalCheckboxes, count($partesProcessadas)));

                $driver->navigate()->to($service->getUrl());

                try {
                    $driver->wait(60)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="primeiroGrau"]')));
                    $driver->findElement(WebDriverBy::xpath('//*[@id="primeiroGrau"]'))->click();
                    $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="main-menu"]/li[2]/ul/li[1]/a')));
                    $driver->findElement(WebDriverBy::xpath('//*[@id="main-menu"]/li[2]/ul/li[1]/a'))->click();
                } catch (\Exception $e) {
                    $this->log("CHECKBOX LOOP NÃO CARREGOU. {$e->getMessage()}");
                    continue;
                }

                try {
                    $driver->findElement(WebDriverBy::xpath('/html/body/div[4]/div[11]/div/button'))->click();
                } catch (\Exception $e) {
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="slcComarca"]'))->sendKeys('TODAS');
                $driver->findElement(WebDriverBy::xpath('//*[@id="slcTipo"]'))->sendKeys('Parte');
                $driver->findElement(WebDriverBy::xpath('//*[@id="txtChave"]'))->sendKeys($word);

                $driver = $this->ocr($driver, 'xpath', '//*[@id="formConsultaPublica"]/ul/li[5]/img', '//*[@id="txtCaptcha"]', '//*[@id="btnConsultar"]', true, false);
                while (true) {
                    try {
                        $error = $driver->findElement(WebDriverBy::cssSelector('#content > div.message > div.error > p'))->getText();
                        $this->log($error . ' => OCR Captcha trying again...');
                        $driver->navigate()->refresh();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="slcComarca"]'))->sendKeys('TODAS');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="slcTipo"]'))->sendKeys('Parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="txtChave"]'))->sendKeys($word);
                        $driver = $this->ocr($driver, 'xpath', '//*[@id="formConsultaPublica"]/ul/li[5]/img', '//*[@id="txtCaptcha"]', '//*[@id="btnConsultar"]', true, false);
                    } catch (\Exception $e) {
                        break;
                    }
                }

                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::tagName('table')));

                $driver->findElement(WebDriverBy::xpath('//*[@id="linkTodos"]'))->click();
                $driver->wait(15)->until(WebDriverExpectedCondition::invisibilityOfElementLocated(WebDriverBy::xpath('//*[@id="pagination"]')));

                $checkboxesElem = $driver->findElements(WebDriverBy::xpath('//*[@id="formConsultaPartes"]/table/tbody/tr/td[1]/input[@type="checkbox"]'));
                foreach ($rangeCheckbox as $iCheckbox) {
                    $checkboxesElem[$iCheckbox]->click();
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="btnListar"]'))->click();
                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr')));

                //PROCESSOS
                $processosLinkTratados = [];
                $processosLink = $driver->findElements(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td[2]/a'));
                $processosLinkTratados = $this->extractLinks($processosLink, true, false, $processosLinkTratados);

                //PAGINACAO
                while (true) {
                    try {
                        $driver->findElement(WebDriverBy::cssSelector('#pagination > ul > li:last-child > a'))->click();
                        $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td/a')));
                        $processosLinkElem = $driver->findElements(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td[2]/a'));
                        $processosLinkTratados = $this->extractLinks($processosLinkElem, true, false, $processosLinkTratados);
                    } catch (\Exception $e) {
                        break;
                    }
                }

                $this->log(count($processosLinkTratados) . ' PROCESSOS ENCONTRADOS.');

                $processosTratados = [];

                foreach ($processosLinkTratados as $nroInterno => $processosLinkTratado) {

                    try {

                        $nroUnico = preg_replace('/[^0-9.]+/', '', $nroInterno);

                        if (isset($processosProcessados[$nroUnico])) {
                            $this->log("$nroUnico JÁ FOI PROCESSADO!");
                            continue;
                        }

                        $driver->navigate()->to($processosLinkTratado);

                        //DADOS GERAIS
                        $elems = $driver->findElements(WebDriverBy::xpath('//*[@id="dados_processo"]/table/tbody/tr'));
                        $dadosGerais = [];
                        $this->log('DADOS GERAIS');
                        foreach ($elems as $item) {
                            try {
                                $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                $valor = $this->cleanString($item->findElement(WebDriverBy::tagName('td'))->getText());
                                if ($label == '') continue;
                                $dadosGerais[$label] = $valor;
                            } catch (\Exception $e) {
                                continue;
                            }
                        }

                        if (!isset($dadosGerais['Numeração Única'])) continue;
                        $nroUnico = $dadosGerais['Numeração Única'];
                        $nroInterno = (isset($dadosGerais['Número'])) ? preg_replace('/[^0-9.]+/', '', $dadosGerais['Número']) : $nroUnico;

                        $this->log($nroUnico . ' - ' . $nroInterno);

                        $processosTratados[$nroUnico]['Dados Gerais'] = $dadosGerais;

                        //PARTES
                        try {
                            $processosTratados[$nroUnico]['Partes'] = [];
                            $driver->findElement(WebDriverBy::cssSelector('#ajust-tabbed-process > div.container_interna > ul > li > a[href="#partes_proc"]'))->click();
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="partes_proc"]/table/tbody/tr')));
                            $elems = $driver->findElements(WebDriverBy::xpath('//*[@id="partes_proc"]/table/tbody/tr'));
                            $partes = [];
                            $this->log('PARTES');
                            foreach ($elems as $item) {
                                try {
                                    $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                    $valor = $this->cleanString($item->findElement(WebDriverBy::tagName('td'))->getText());
                                    if ($label == '') continue;
                                    $partes[$label] = $valor;
                                } catch (\Exception $e) {
                                    continue;
                                }
                            }
                            $processosTratados[$nroUnico]['Partes'] = $partes;
                        } catch (\Exception $e) {
                        }


                        //DISTRIBUIÇÃO
                        try {
                            $processosTratados[$nroUnico]['Distribuição'] = [];
                            $driver->findElement(WebDriverBy::cssSelector('#ajust-tabbed-process > div.container_interna > ul > li > a[href="#distribuicao"]'))->click();
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="distribuicao"]/table/tbody/tr')));
                            $elems = $driver->findElements(WebDriverBy::xpath('//*[@id="distribuicao"]/table/tbody/tr'));
                            $distribuicao = [];
                            $this->log('DISTRIBUIÇÃO');
                            foreach ($elems as $item) {
                                try {
                                    $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                    $valor = $this->cleanString($item->findElement(WebDriverBy::tagName('td'))->getText());
                                    if ($label == '') continue;
                                    $distribuicao[$label] = $valor;
                                } catch (\Exception $e) {
                                    continue;
                                }
                            }
                            $processosTratados[$nroUnico]['Distribuição'] = $distribuicao;
                        } catch (\Exception $e) {

                        }


                        //MOVIMENTAÇÃO
                        try {
                            $processosTratados[$nroUnico]['Movimentação'] = [];
                            $driver->findElement(WebDriverBy::cssSelector('#ajust-tabbed-process > div.container_interna > ul > li > a[href="#todas_movime"]'))->click();
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="todas_movime"]/table/tbody/tr')));
                            $elems = $driver->findElements(WebDriverBy::xpath('//*[@id="todas_movime"]/table/tbody/tr'));
                            $movimentacao = [];
                            $this->log('MOVIMENTAÇÃO');
                            foreach ($elems as $item) {
                                try {
                                    $movimentacao[] = $item->getText();
                                } catch (\Exception $e) {
                                    continue;
                                }
                            }
                            $processosTratados[$nroUnico]['Movimentação'] = $movimentacao;
                        } catch (\Exception $e) {
                        }


                        //PETIÇÕES
                        try {
                            $processosTratados[$nroUnico]['Petições'] = [];
                            $driver->findElement(WebDriverBy::cssSelector('#ajust-tabbed-process > div.container_interna > ul > li > a[href="#peticoes"]'))->click();
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="peticoes"]/table/tbody/tr')));
                            $elems = $driver->findElements(WebDriverBy::xpath('//*[@id="peticoes"]/table/tbody/tr'));
                            $peticoes = [];
                            $auxPeticoes = [];
                            $this->log('PETIÇÕES');
                            foreach ($elems as $item) {
                                try {
                                    $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                    $valor = $this->cleanString($item->findElement(WebDriverBy::tagName('td'))->getText());
                                    $auxPeticoes[$label] = $valor;
                                } catch (\Exception $e) {
                                    $peticoes[] = $auxPeticoes;
                                    $auxPeticoes = [];
                                }
                            }
                            $processosTratados[$nroUnico]['Petições'] = $peticoes;
                        } catch (\Exception $e) {
                        }

                        //DOCUMENTOS
                        try {
                            $processosTratados[$nroUnico]['Documentos'] = [];
                            $driver->findElement(WebDriverBy::cssSelector('#ajust-tabbed-process > div.container_interna > ul > li > a[href="#doc_processo"]'))->click();
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="doc_processo"]/table/tbody/tr/td/a')));
                            $elems = $driver->findElements(WebDriverBy::xpath('//*[@id="doc_processo"]/table/tbody/tr/td/a'));
                            $processosTratados[$nroUnico]['Documentos'] = $this->extractLinks($elems, true);
                        } catch (\Exception $e) {
                        }


                        /** @var $processoService \Tribunal\Service\Processo */
                        $processoService = null;
                        if (isset($processosNaoProcessados[$nroInterno])) {
                            $processoService = $processosNaoProcessados[$nroInterno];
                        } else {
                            if (isset($processosProcessados[$nroInterno])) {
                                $processoService = $processosProcessados[$nroInterno];
                            } else {
                                $processoService = new Processo();
                                $processoService->setProcesso($nroUnico);
                                $processoService->setProcessoInterno($nroInterno);
                                $processoService->setIdTribunal($service->getId());
                                $processoService->setUrl($service->getUrl());
                                $processoService->setProcessado(0);
                                $processoService->salvar(true);

                                $processosProcessados[$nroInterno] = $processoService;
                            }
                        }

                        $hashAnterior = $processoService->getHash();
                        $processoService->setDados(json_encode($processosTratados[$nroUnico]));
                        $processoService->setHash(md5($processoService->getDados()));
                        if ($hashAnterior != $processoService->getHash()) $processoService->setAlteracao(1);
                        $processoService->setProcessado(1);
                        $processoService->setProcessoInterno($nroInterno);
                        $processoService->salvar(true);

                        $this->log("PROCESSO {$processoService->getProcesso()} - {$processoService->getProcessoInterno()} PROCESSADO.");

                    } catch (\Exception $e) {
                        $this->log($e->getMessage());
                        continue;
                    }

                }
            }

        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);

        die;//temporario
        return true;
    }

    public function tjma2instancia($service, $processarApenasProcessos = false)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Processo[] */

        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());

        $processosProcessados = [];
        $processosNaoProcessados = [];
        $processos = $service->getTodosProcessos();

        foreach ($processos as $processo) {
            if ($processo->getProcessado() == '1') {
                $processosProcessados[$processo->getProcessoInterno()] = $processo;
                continue;
            }
            $processosNaoProcessados[$processo->getProcessoInterno()] = $processo;
        }
        $this->log(count($processosProcessados) . ' PROCESSADOS');
        $this->log(count($processosNaoProcessados) . ' NÃO PROCESSADOS');

        $words = $service->getTodasPalavras();
        foreach ($words as $word) {
            $driver->navigate()->to($service->getUrl());

            $driver->findElement(WebDriverBy::xpath('//*[@id="segundoGrau"]'))->click();
            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="main-menu"]/li[3]/ul/li[1]/a')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="main-menu"]/li[3]/ul/li[1]/a'))->click();

            try {
                $driver->findElement(WebDriverBy::xpath('/html/body/div[4]/div[11]/div/button'))->click();
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="slcTipo"]'))->sendKeys('Parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="txtChave"]'))->sendKeys($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="btnConsultar"]'))->click();

            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr')));

            $linksPartesElem = $driver->findElements(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td[1]/a'));
            $linksPartes = $this->extractLinks($linksPartesElem);

            while (true) {
//                break; //RETIRAR APOS TESTE
                try {
                    $driver->findElement(WebDriverBy::cssSelector('#pagination > ul > li:last-child > a'))->click();
                    $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td/a')));
                    $linksPartesElem = $driver->findElements(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td[1]/a'));
                    $linksPartes = $this->extractLinks($linksPartesElem, false, false, $linksPartes);
                } catch (\Exception $e) {
                    break;
                }
            }

            $totalLinksPartes = count($linksPartes);
            $this->log($totalLinksPartes . ' PARTES ENCONTRADAS');

            $processosLinkTratados = [];
            foreach ($linksPartes as $indexParte => $linkParte) {
                try {

                    $this->log('PARTES => ' . $this->percentage($totalLinksPartes, $indexParte + 1));

                    $driver->navigate()->to($linkParte);
                    $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr')));
                    $processosLinkElem = $driver->findElements(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td[1]/a'));
                    $processosLinkTratados = $this->extractLinks($processosLinkElem, true, false, $processosLinkTratados);

//                    break; //RETIRAR APOS TESTE

                    while (true) {
                        try {
                            $driver->findElement(WebDriverBy::cssSelector('#pagination > ul > li:last-child > a'))->click();
                            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td/a')));
                            $processosLinkElem = $driver->findElements(WebDriverBy::xpath('//*[@id="ajust-table-result"]/table/tbody/tr/td[1]/a'));
                            $processosLinkTratados = $this->extractLinks($processosLinkElem, true, false, $processosLinkTratados);
                        } catch (\Exception $e) {
                            break;
                        }
                    }

                } catch (\Exception $e) {
                    $this->log($e->getMessage());
                    continue;
                }
            }

            $totalProcessosLinkTratados = count($processosLinkTratados);
            $this->log($totalProcessosLinkTratados . ' PROCESSOS ENCONTRADOS.');

            $processosTratados = [];

            $indexProc = 1;
            foreach ($processosLinkTratados as $nroInterno => $processosLinkTratado) {

                try {

                    $nroUnico = preg_replace('/[^0-9.]+/', '', $nroInterno);
                    $this->log($this->percentage($totalProcessosLinkTratados, $indexProc) . " PROCESSO INTERNO => " . $nroUnico);
                    $indexProc++;

                    if (isset($processosProcessados[$nroUnico])) {
                        $this->log("$nroUnico JÁ FOI PROCESSADO!");
                        continue;
                    }

                    $driver->navigate()->to($processosLinkTratado);

                    //DADOS GERAIS
                    $elems = $driver->findElements(WebDriverBy::xpath('//*[@id="ajust-table-process"]/table/tbody/tr'));
                    $dadosGerais = [];
                    $this->log('DADOS GERAIS');
                    foreach ($elems as $indexItem => $item) {
                        if ($indexItem > 3) continue;
                        try {
                            $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                            $valor = $this->cleanString($item->findElement(WebDriverBy::tagName('td'))->getText());
                            if ($label == '') continue;
                            $dadosGerais[$label] = $valor;
                        } catch (\Exception $e) {
                            continue;
                        }
                    }

                    if (!isset($dadosGerais['Numeração Única'])) continue;
                    $nroUnico = $dadosGerais['Numeração Única'];
                    $nroInterno = (isset($dadosGerais['Número'])) ? preg_replace('/[^0-9.]+/', '', $dadosGerais['Número']) : $nroUnico;
                    $this->log($nroUnico);

                    $processosTratados[$nroUnico]['Dados Gerais'] = $dadosGerais;

                    //PARTES
                    try {
                        $title = 'Partes';
                        $processosTratados[$nroUnico][$title] = [];
                        $titleAux = '';
                        $titleFound = false;
                        $partes = [];
                        $this->log('PARTES');
                        foreach ($elems as $item) {
                            try {
                                try {
                                    $titleAux = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td.title'))->getText());
                                } catch (\Exception $e) {
                                }

                                if (!$titleFound && $titleAux == $title) {
                                    $titleFound = true;
                                    continue;
                                }
                                if ($titleFound && $titleAux != $title) break;
                                if (!$titleFound && $titleAux != $title) continue;

                                $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                $valor = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td:not(.title)'))->getText());
                                if ($label == '') continue;
                                $partes[$label] = $valor;
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                        $processosTratados[$nroUnico][$title] = $partes;
                    } catch (\Exception $e) {
                    }


                    //DISTRIBUIÇÃO
                    try {
                        $title = 'Distribuíção';
                        $processosTratados[$nroUnico][$title] = [];
                        $titleAux = '';
                        $titleFound = false;
                        $distribuicao = [];
                        $this->log('DISTRIBUIÇÃO');
                        foreach ($elems as $item) {
                            try {
                                try {
                                    $titleAux = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td.title'))->getText());
                                } catch (\Exception $e) {
                                }

                                if (!$titleFound && $titleAux == $title) {
                                    $titleFound = true;
                                    continue;
                                }
                                if ($titleFound && $titleAux != $title) break;
                                if (!$titleFound && $titleAux != $title) continue;

                                $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                $valor = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td:not(.title)'))->getText());
                                if ($label == '') continue;
                                $distribuicao[$label] = $valor;
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                        $processosTratados[$nroUnico][$title] = $distribuicao;
                    } catch (\Exception $e) {
                    }

                    //MOVIMENTAÇÃO
                    try {
                        $title = 'Todas as Movimentações';
                        $processosTratados[$nroUnico][$title] = [];
                        $titleAux = '';
                        $titleFound = false;
                        $movimentacao = [];
                        $this->log('MOVIMENTAÇÃO');
                        foreach ($elems as $item) {
                            try {
                                try {
                                    $titleAux = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td.title'))->getText());;
                                } catch (\Exception $e) {
                                }

                                if (!$titleFound && $titleAux == $title) {
                                    $titleFound = true;
                                    continue;
                                }
                                if ($titleFound && $titleAux != $title) break;
                                if (!$titleFound && $titleAux != $title) continue;
                                $textCapt = $item->getText();
                                if (strlen($textCapt) > 5) $movimentacao[] = $textCapt;
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                        $processosTratados[$nroUnico][$title] = $movimentacao;
                    } catch (\Exception $e) {
                    }

                    //JULGAMENTO
                    try {
                        $title = 'Julgamento';
                        $processosTratados[$nroUnico][$title] = [];
                        $titleAux = '';
                        $titleFound = false;
                        $julgamento = [];
                        $this->log('JULGAMENTO');
                        foreach ($elems as $item) {
                            try {
                                try {
                                    $titleAux = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td.title'))->getText());
                                } catch (\Exception $e) {
                                }

                                if (!$titleFound && $titleAux == $title) {
                                    $titleFound = true;
                                    continue;
                                }
                                if ($titleFound && $titleAux != $title) break;
                                if (!$titleFound && $titleAux != $title) continue;

                                $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                $valor = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td:not(.title)'))->getText());
                                if ($label == '') continue;
                                $julgamento[$label] = $valor;
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                        $processosTratados[$nroUnico][$title] = $julgamento;
                    } catch (\Exception $e) {
                    }

                    //AGENDA DO JULGAMENTO
                    try {
                        $title = 'Agenda do Julgamento';
                        $processosTratados[$nroUnico][$title] = [];
                        $titleAux = '';
                        $titleFound = false;
                        $agendaJulgamento = [];
                        $this->log('AGENDA DO JULGAMENTO');
                        foreach ($elems as $item) {
                            try {
                                try {
                                    $titleAux = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td.title'))->getText());
                                } catch (\Exception $e) {
                                }

                                if (!$titleFound && $titleAux == $title) {
                                    $titleFound = true;
                                    continue;
                                }
                                if ($titleFound && $titleAux != $title) break;
                                if (!$titleFound && $titleAux != $title) continue;

                                $label = str_replace(':', '', $this->cleanString($item->findElement(WebDriverBy::tagName('th'))->getText()));
                                $valor = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td:not(.title)'))->getText());
                                if ($label == '') continue;
                                $agendaJulgamento[$label] = $valor;
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                        $processosTratados[$nroUnico][$title] = $agendaJulgamento;
                    } catch (\Exception $e) {
                    }

                    //EMENTA
                    try {
                        $title = 'Ementa';
                        $processosTratados[$nroUnico][$title] = [];
                        $titleAux = '';
                        $titleFound = false;
                        $ementa = [];
                        $this->log('EMENTA');
                        foreach ($elems as $item) {
                            try {
                                try {
                                    $titleAux = $this->cleanString($item->findElement(WebDriverBy::cssSelector('td.title'))->getText());
                                } catch (\Exception $e) {
                                }

                                if (!$titleFound && $titleAux == $title) {
                                    $titleFound = true;
                                    continue;
                                }
                                if ($titleFound && $titleAux != $title) break;
                                if (!$titleFound && $titleAux != $title) continue;
                                $textCapt = $item->getText();
                                if (strlen($textCapt) > 5) $ementa[] = $textCapt;
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                        $processosTratados[$nroUnico][$title] = $ementa;
                    } catch (\Exception $e) {
                    }


                    /** @var $processoService \Tribunal\Service\Processo */
                    $processoService = null;
                    if (isset($processosNaoProcessados[$nroInterno])) {
                        $processoService = $processosNaoProcessados[$nroInterno];
                    } else {
                        if (isset($processosProcessados[$nroInterno])) {
                            $processoService = $processosProcessados[$nroInterno];
                        } else {
                            $processoService = new Processo();
                            $processoService->setProcesso($nroUnico);
                            $processoService->setProcessoInterno($nroInterno);
                            $processoService->setIdTribunal($service->getId());
                            $processoService->setUrl($service->getUrl());
                            $processoService->setProcessado(0);
                            $processoService->salvar(true);

                            $processosProcessados[$nroInterno] = $processoService;
                        }
                    }

                    $hashAnterior = $processoService->getHash();
                    $processoService->setDados(json_encode($processosTratados[$nroUnico]));
                    $processoService->setHash(md5($processoService->getDados()));
                    if ($hashAnterior != $processoService->getHash()) $processoService->setAlteracao(1);
                    $processoService->setProcessado(1);
                    $processoService->salvar(true);

                    $this->log("PROCESSO {$processoService->getProcesso()} - {$processoService->getProcessoInterno()} PROCESSADO.");

                } catch (\Exception $e) {
                    $this->log($e->getMessage());
                    continue;
                }

            }
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);
        return true;
    }

    public function tjmt1instanciafisico($service, $processarApenasProcessos = false)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Processo[] */

        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());
        $driver->manage()->window()->maximize();

        $processosProcessados = [];
        $processosNaoProcessados = [];
        $processos = $service->getTodosProcessos();

        foreach ($processos as $processo) {
            if ($processo->getProcessado() == '1') {
                $processosProcessados[$processo->getProcesso()] = $processo;
                continue;
            }
            $processosNaoProcessados[$processo->getProcesso()] = $processo;
        }
        $this->log(count($processosProcessados) . ' PROCESSADOS');
        $this->log(count($processosNaoProcessados) . ' NÃO PROCESSADOS');

        $words = $service->getTodasPalavras();
        foreach ($words as $word) {
            $driver->navigate()->to($service->getUrl());
            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_ddlComarcas"]'))->sendKeys('PESQUISAR EM TODAS AS COMARCAS');
            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNomeParte"]'))->sendKeys($word);

            while (true) {
                try {
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_imgCaptcha"]', '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtCodSeguranca"]', null, false);
                    $valorCaptcha = $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtCodSeguranca"]'))->getAttribute('value');
                    if ($valorCaptcha == '') continue;

                    $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_btSubmit"]'))->click();

                    $driver->wait(3)->until(
                        WebDriverExpectedCondition::alertIsPresent(),
                        'Waiting for an alert'
                    );
                    $driver->switchTo()->alert()->accept();
                    $driver->navigate()->to($service->getUrl());
                    $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_ddlComarcas"]'))->sendKeys('PESQUISAR EM TODAS AS COMARCAS');
                    $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNomeParte"]'))->sendKeys($word);
                } catch (\Exception $e) {
                    break;
                }
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_ddlRegistrosPorPagina"]'))->sendKeys('100');

            //TODOS OS NOMES DA PARTE
            $verProcessosAction = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr > td > span.tamanho13 > a'));
            $totalProcessosAction = count($verProcessosAction);
            $this->log("TOTAL NOMES DA PARTE => {$totalProcessosAction}");
            foreach ($verProcessosAction as $idxAction => $processoAction) {
                $this->log('NOMES DA PARTE => ' . $this->percentage($totalProcessosAction, ($idxAction + 1)));

                //RECAPTURAR DEVIDO A PERDA DA REFERÊNCIA DO REGISTRO ANTERIOR AO LOOP
                $verProcessosAction = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr > td > span.tamanho13 > a'));
                $verProcessosAction[$idxAction]->click();

                $processosNumero = $driver->findElements(WebDriverBy::cssSelector('span.tamanho18.amarelo.alinha_dir.bold'));
                $processosElem = $driver->findElements(WebDriverBy::cssSelector('div.w100 > a'));
                $totalProcessos = count($processosElem);
                $this->log("TOTAL DE PROCESSOS ENCONTRADOS => {$totalProcessos}");
                foreach ($processosElem as $idx => $processoElem) {
                    //RECAPTURAR DEVIDO A PERDA DA REFERÊNCIA DO REGISTRO ANTERIOR AO LOOP
                    $processosNumero = $driver->findElements(WebDriverBy::cssSelector('span.tamanho18.amarelo.alinha_dir.bold'));
                    $processosElem = $driver->findElements(WebDriverBy::cssSelector('div.w100 > a'));

                    $nroProcesso = $this->cleanString($processosNumero[$idx]->getText());
                    $this->log("PROCESSO {$nroProcesso} => " . $this->percentage($totalProcessos, ($idx + 1)));
                    if (isset($processosProcessados[$nroProcesso])) {
                        $this->log("PROCESSO {$nroProcesso} JÁ PROCESSADO!");
                        continue;
                    }

                    $processosElem[$idx]->click();

                    //NEW WINDOW
                    $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso_LinkButton1"]'))->click();
                    @$driver->switchTo()->window(end($driver->getWindowHandles()));
                    $processoTratados[$nroProcesso] = [];
                    $processoTratados[$nroProcesso]['Dados Gerais'] = [];
                    $processoTratados[$nroProcesso]['Partes'] = [];
                    $processoTratados[$nroProcesso]['Andamentos'] = [];

                    $this->log('DADOS GERAIS');
                    $elems = $driver->findElements(WebDriverBy::cssSelector('div#dadosGerais > table > tbody > tr'));
                    foreach ($elems as $elemItem) {
                        $colunas = $elemItem->findElements(WebDriverBy::tagName('td'));
                        if (count($colunas) == 2) {
                            $processoTratados[$nroProcesso]['Dados Gerais'][$this->cleanString($colunas[0]->getText(), [':', '::'])] = $this->cleanString($colunas[1]->getText(), [':', '::']);
                            continue;
                        }
                        if (count($colunas) == 4) {
                            $processoTratados[$nroProcesso]['Dados Gerais'][$this->cleanString($colunas[0]->getText(), [':', '::'])] = $this->cleanString($colunas[1]->getText(), [':', '::']);
                            $processoTratados[$nroProcesso]['Dados Gerais'][$this->cleanString($colunas[2]->getText(), [':', '::'])] = $this->cleanString($colunas[3]->getText(), [':', '::']);
                            continue;
                        }
                    }

                    $this->log('PARTES');
                    $elems = $driver->findElements(WebDriverBy::cssSelector('div#listaPartes > table > tbody > tr'));
                    foreach ($elems as $elemItem) {
                        $colunas = $elemItem->findElements(WebDriverBy::tagName('td'));
                        $processoTratados[$nroProcesso]['Partes'][] = [$this->cleanString($colunas[0]->getText(), [':', '::']) => $this->cleanString($colunas[1]->getText(), [':', '::'])];
                    }

                    $this->log('ANDAMENTOS');
                    $elems = $driver->findElements(WebDriverBy::cssSelector('div#listaAndamento > table > tbody > tr'));
                    $even = [];
                    $odd = [];
                    foreach ($elems as $idxElem => $elemItem) {
                        if ($idxElem % 2 == 0) {
                            $even[] = $elemItem;
                        } else {
                            $odd[] = $elemItem;
                        }
                    }
                    foreach ($even as $i => $item) {
                        $data = $this->cleanString($item->getText());
                        $andamento = $odd[$i]->getText();
                        $processoTratados[$nroProcesso]['Andamentos'][] = [$data => $andamento];
                    }

                    /** @var $processoService \Tribunal\Service\Processo */
                    $processoService = null;
                    if (isset($processosNaoProcessados[$nroProcesso])) {
                        $processoService = $processosNaoProcessados[$nroProcesso];
                    } else {
                        if (isset($processosProcessados[$nroProcesso])) {
                            $processoService = $processosProcessados[$nroProcesso];
                        } else {
                            $processoService = new Processo();
                            $processoService->setProcesso($nroProcesso);
                            $processoService->setIdTribunal($service->getId());
                            $processoService->setUrl($service->getUrl());
                            $processoService->setProcessado(0);
                            $processoService->salvar(true);

                            $processosProcessados[$nroProcesso] = $processoService;
                        }
                    }

                    $hashAnterior = $processoService->getHash();
                    $processoService->setDados(json_encode($processoTratados[$nroProcesso]));
                    $processoService->setHash(md5($processoService->getDados()));
                    if ($hashAnterior != $processoService->getHash()) $processoService->setAlteracao(1);
                    $processoService->setProcessado(1);
                    $processoService->salvar(true);

                    $this->log("PROCESSO {$processoService->getProcesso()} PROCESSADO.");

                    $this->log('FECHANDO JANELA');
                    $driver->executeScript("window.close()", []);
                    @$driver->switchTo()->window(end($driver->getWindowHandles()));
                    $this->log('VOLTANDO JANELA');
                    $driver->navigate()->back();
                }
                $this->log('VOLTANDO JANELA');
                $driver->navigate()->back();
            }
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);
        return true;
    }

    public function tjmt2instanciafisico($service, $processarApenasProcessos = false)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Processo[] */

        $driver = $this->startDriver();

        if ($this->isDomainAvailable($service->getUrl())) {
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } else {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $driver->get($service->getUrl());
        $driver->manage()->window()->maximize();

        $processosProcessados = [];
        $processosNaoProcessados = [];
        $processos = $service->getTodosProcessos();

        foreach ($processos as $processo) {
            if ($processo->getProcessado() == '1') {
                $processosProcessados[$processo->getProcesso()] = $processo;
                continue;
            }
            $processosNaoProcessados[$processo->getProcesso()] = $processo;
        }
        $this->log(count($processosProcessados) . ' PROCESSADOS');
        $this->log(count($processosNaoProcessados) . ' NÃO PROCESSADOS');

        $words = $service->getTodasPalavras();
        foreach ($words as $word) {
            $driver->navigate()->to($service->getUrl());
            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNomeParte"]'))->sendKeys($word);

            while (true) {
                try {
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_imgCaptcha"]', '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtCodSeguranca"]', null, false);
                    $valorCaptcha = $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtCodSeguranca"]'))->getAttribute('value');
                    if ($valorCaptcha == '') continue;

                    $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_btSubmit"]'))->click();

                    $driver->wait(3)->until(
                        WebDriverExpectedCondition::alertIsPresent(),
                        'Waiting for an alert'
                    );
                    $driver->switchTo()->alert()->accept();
                    $driver->navigate()->to($service->getUrl());
                    $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNomeParte"]'))->sendKeys($word);
                } catch (\Exception $e) {
                    break;
                }
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_ddlRegistrosPorPagina"]'))->sendKeys('100');

            //TODOS OS NOMES DA PARTE
            $verProcessosAction = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr > td > span.tamanho13 > a'));
            $totalProcessosAction = count($verProcessosAction);
            $this->log("TOTAL NOMES DA PARTE => {$totalProcessosAction}");
            foreach ($verProcessosAction as $idxAction => $processoAction) {
                $this->log('NOMES DA PARTE => ' . $this->percentage($totalProcessosAction, ($idxAction + 1)));

                //RECAPTURAR DEVIDO A PERDA DA REFERÊNCIA DO REGISTRO NO LOOP
                $verProcessosAction = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr > td > span.tamanho13 > a'));
                $verProcessosAction[$idxAction]->click();

                $processosNumero = $driver->findElements(WebDriverBy::cssSelector('span.tamanho18.amarelo.alinha_dir.bold'));
                $processosElem = $driver->findElements(WebDriverBy::cssSelector('div.w100 > a'));
                $totalProcessos = count($processosElem);
                $this->log("TOTAL DE PROCESSOS ENCONTRADOS => {$totalProcessos}");
                foreach ($processosElem as $idx => $processoElem) {
                    //RECAPTURAR DEVIDO A PERDA DA REFERÊNCIA DO REGISTRO NO LOOP
                    $processosNumero = $driver->findElements(WebDriverBy::cssSelector('span.tamanho18.amarelo.alinha_dir.bold'));

                    $nroProcesso = $this->cleanString($processosNumero[$idx]->getText());
                    $this->log("PROCESSO {$nroProcesso} => " . $this->percentage($totalProcessos, ($idx + 1)));
                    if (isset($processosProcessados[$nroProcesso])) {
                        $this->log("PROCESSO {$nroProcesso} JÁ PROCESSADO!");
                        continue;
                    }

                    $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.w100 > a')));
                    $processosElem = $driver->findElements(WebDriverBy::cssSelector('div.w100 > a'));
//                    $processosElem[$idx]->click(); //DONT WORK
                    $href = $processosElem[$idx]->getAttribute('href');
                    $driver->executeScript($href, []);

                    //NEW WINDOW
                    try {
                        $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso_HyperLink3"]')));
                        $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso_HyperLink3"]'))->click();
                        @$driver->switchTo()->window(end($driver->getWindowHandles()));
                    } catch (\Exception $e) {
                        $this->log("PROCESSO => {$nroProcesso} DADO NÃO CAPTURADO", $this->logFileName);
                        $this->log('VOLTANDO JANELA');
                        $driver->navigate()->back();
                        continue;
                    }

                    $processoTratados[$nroProcesso] = [];
                    $processoTratados[$nroProcesso]['Dados Gerais'] = [];
                    $processoTratados[$nroProcesso]['Partes'] = [];
                    $processoTratados[$nroProcesso]['Andamentos'] = [];

                    $this->log('DADOS GERAIS');
                    $elems = $driver->findElements(WebDriverBy::cssSelector('div#dadosGerais > table > tbody > tr'));
                    foreach ($elems as $elemItem) {
                        $colunas = $elemItem->findElements(WebDriverBy::tagName('td'));
                        if (count($colunas) == 2) {
                            $processoTratados[$nroProcesso]['Dados Gerais'][$this->cleanString($colunas[0]->getText(), [':', '::'])] = $this->cleanString($colunas[1]->getText(), [':', '::']);
                            continue;
                        }
                        if (count($colunas) == 4) {
                            $processoTratados[$nroProcesso]['Dados Gerais'][$this->cleanString($colunas[0]->getText(), [':', '::'])] = $this->cleanString($colunas[1]->getText(), [':', '::']);
                            $processoTratados[$nroProcesso]['Dados Gerais'][$this->cleanString($colunas[2]->getText(), [':', '::'])] = $this->cleanString($colunas[3]->getText(), [':', '::']);
                            continue;
                        }
                    }

                    $this->log('PARTES');
                    $elems = $driver->findElements(WebDriverBy::cssSelector('div#listaPartes > table > tbody > tr'));
                    foreach ($elems as $elemItem) {
                        $colunas = $elemItem->findElements(WebDriverBy::tagName('td'));
                        $processoTratados[$nroProcesso]['Partes'][] = [$this->cleanString($colunas[0]->getText(), [':', '::']) => $this->cleanString($colunas[1]->getText(), [':', '::'])];
                    }

                    $this->log('ANDAMENTOS');
                    $elems = $driver->findElements(WebDriverBy::cssSelector('div#listaAndamento > table > tbody > tr'));
                    $even = [];
                    $odd = [];
                    foreach ($elems as $idxElem => $elemItem) {
                        if ($idxElem % 2 == 0) {
                            $even[] = $elemItem;
                        } else {
                            $odd[] = $elemItem;
                        }
                    }
                    foreach ($even as $i => $item) {
                        $data = $this->cleanString($item->getText());
                        $andamento = $odd[$i]->getText();
                        $processoTratados[$nroProcesso]['Andamentos'][] = [$data => $andamento];
                    }

                    /** @var $processoService \Tribunal\Service\Processo */
                    $processoService = null;
                    if (isset($processosNaoProcessados[$nroProcesso])) {
                        $processoService = $processosNaoProcessados[$nroProcesso];
                    } else {
                        if (isset($processosProcessados[$nroProcesso])) {
                            $processoService = $processosProcessados[$nroProcesso];
                        } else {
                            $processoService = new Processo();
                            $processoService->setProcesso($nroProcesso);
                            $processoService->setIdTribunal($service->getId());
                            $processoService->setUrl($service->getUrl());
                            $processoService->setProcessado(0);
                            $processoService->salvar(true);

                            $processosProcessados[$nroProcesso] = $processoService;
                        }
                    }

                    $hashAnterior = $processoService->getHash();
                    $processoService->setDados(json_encode($processoTratados[$nroProcesso]));
                    $processoService->setHash(md5($processoService->getDados()));
                    if ($hashAnterior != $processoService->getHash()) $processoService->setAlteracao(1);
                    $processoService->setProcessado(1);
                    $processoService->salvar(true);

                    $this->log("PROCESSO {$processoService->getProcesso()} PROCESSADO.");

                    $this->log('FECHANDO JANELA');
                    $driver->executeScript("window.close()", []);
                    @$driver->switchTo()->window(end($driver->getWindowHandles()));
                    $this->log('VOLTANDO JANELA');
                    $driver->navigate()->back();
                }
                $this->log('VOLTANDO JANELA');
                $driver->navigate()->back();
            }
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);
        return true;
    }

    public function tjms1instanciaesaj($service, $processarApenasProcessos = false)
    {
        $processarApenasProcessos = true;//TEMP
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        try {
            $driver->get($service->getUrl());
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } catch (\Exception $e) {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            $driver->navigate()->to($link);

            //SE O RESULTADO FOR APENAS 1 PROCESSO, ESSE PROCESSO APARECERÁ LOGO NO COMEÇO SEM A LISTA DE PROCESSOS
            try {
                $noProcessoInfo = false;
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]'));
                $nroProcesso = $this->extractProcessNumber($driver->findElement(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]'))->getText());
                $processosLink[$nroProcesso] = $driver->getCurrentURL();
            } catch (\Exception $e) {
                $noProcessoInfo = true;
            }

            if ($noProcessoInfo) {
                try {
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                } catch (\Exception $e) {
                    continue;
                }

                $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);

                while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
                    $driver->navigate()->to($next);
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                    $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);
                }
            }

            $total = count($processosLink);
            $this->log($total . ' processo(s) encontrado(s)');

            if ($processarApenasProcessos) {
                $linksRegistrados = $service->getTodosProcessos(0); //TEMPORARIO
                $totalLinksRegistrados = count($linksRegistrados); //TEMPORARIO
            } else {
                $linksRegistrados = $service->registerProcessos($processosLink);
                $totalLinksRegistrados = count($linksRegistrados);
                $this->log($totalLinksRegistrados . ' A PROCESSAR');
            }

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */
            $index = 1;
            foreach ($linksRegistrados as $processo) {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                try {
                    $driver->navigate()->to($processo->getUrl());
                } catch (\Exception $e) {
                    $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                    continue;
                }

                $dadosProcesso = [];
                $partesProcesso = [];
                $movimentacoes = [];
                $peticoesDiversas = [];
                $incidentesAcoesRecursosSentencas = [];
                $apensosEntranhadosUnificados = [];
                $audiencia = [];
                $historico = [];

                $pastaDigital = false;
                try {
                    $pastDigText = $driver->findElement(WebDriverBy::xpath('//*[@id="linkPasta"]'))->getText();
                    $this->log($pastDigText);
                    $pastaDigital = true;
                } catch (\Exception $e) {
                    $pastaDigital = false;
                }

                try {
                    //DADOS
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                    $this->log('DADOS GERAIS');
                    $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
                } catch (\Exception $e) {
                }

                $linkPartes = false;
                try {
                    //PARTES
                    $this->log('PARTES');

                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                    sleep(1);
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = true;
                } catch (\Exception $e) {
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = false;
                    $this->log('NÃO TEM LINK TODAS PARTES');
                }

                $movimentacoesAux = [];
                $linkMovimentacoes = false;
                try {
                    $this->log('MOVIMENTAÇÕES');
                    //MOVIMENTACOES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                    sleep(1);
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                    $linkMovimentacoes = true;
                } catch (\Exception $e) {
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                    $linkMovimentacoes = false;
                    $this->log('NÃO TEM LINK TODAS MOVIMENTAÇÕES');
                }
                $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);

                try {
                    //PETICOES DIVERSAS

                    //PROCESSO 0001476-33.2010.8.12.0028 TEM LINK PARTES E MOVIMENTAÇÕES
                    //PROCESSO 0005356-75.2014.8.12.0001 TEM INCIDENTES E APENSOS
                    $table = 4;
                    if (!$pastaDigital) $table = $table - 1;
                    if ($linkPartes) {
                        $table = 5;
                        if (!$pastaDigital) $table = $table - 1;
                    }
                    $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, false, ['Data', 'Tipo']);
                    if (count($peticoesDiversas)) $this->log('PETICOES DIVERSAS');
                } catch (\Exception $e) {
                }

                try {
                    //INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS
                    $table = 6;
                    if (!$pastaDigital) $table = $table - 1;
                    $incidentesAcoesRecursosSentencasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $trash = array_shift($incidentesAcoesRecursosSentencasAux);
                    $incidentesAcoesRecursosSentencas = $this->extractTable($incidentesAcoesRecursosSentencasAux, 2, true, ['Recebido em', 'Classe']);
                    if (count($incidentesAcoesRecursosSentencas)) $this->log('INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS');
                } catch (\Exception $e) {
                }

                try {
                    //APENSOS, ENTRANHADOS E UNIFICADOS
                    $apensosEntranhadosUnificadosAux = $driver->findElements(WebDriverBy::xpath('//*[@id="dadosApenso"]/tr'));
                    $apensosEntranhadosUnificados = $this->extractTable($apensosEntranhadosUnificadosAux, 4, true, ['Número', 'Classe', 'Apensamento', 'Motivo']);
                    if (count($apensosEntranhadosUnificados)) $this->log('APENSOS, ENTRANHADOS E UNIFICADOS');
                } catch (\Exception $e) {
                }

                try {
                    //AUDIÊNCIAS
                    $table = 7;
                    if (!$pastaDigital) $table = $table - 1;
                    if ($linkPartes) {
                        $table = 8;
                        if (!$pastaDigital) $table = $table - 1;
                    }
                    $audienciaAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $audiencia = $this->extractTable($audienciaAux, 4, true, ['Data', 'Audiência', 'Situação', 'Qtd. Pessoas']);
                    if (count($audiencia)) $this->log('AUDIÊNCIAS');
                } catch (\Exception $e) {
                }

                try {
                    //HISTORICO DE CLASSES
                    $table = 8;
                    if (!$pastaDigital) $table = $table - 1;
                    if ($linkPartes) {
                        $table = 9;
                        if (!$pastaDigital) $table = $table - 1;
                    }
                    $historicoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $historico = $this->extractTable($historicoAux, 5, true, ['Data', 'Tipo', 'Classe', 'Área', 'Motivo']);
                    if (count($historico)) $this->log('HISTORICO DE CLASSES');
                } catch (\Exception $e) {
                }

                $fieldsTratados[$processo->getProcesso()] = [
                    'DadosProcesso' => $dadosProcesso,
                    'PartesProcesso' => $partesProcesso,
                    'Movimentacoes' => $movimentacoes,
                    'PeticoesDiversas' => $peticoesDiversas,
                    'IncidentesAcoesRecursosSentencas' => $incidentesAcoesRecursosSentencas,
                    'ApensosEntranhadosUnificados' => $apensosEntranhadosUnificados,
                    'Audiencia' => $audiencia,
                    'Historico' => $historico,
                ];

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);
            }
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);
        return true;
    }

    public function tjms2instanciaesaj($service, $processarApenasProcessos = false)
    {
        $processarApenasProcessos = true;//TEMP
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        try {
            $driver->get($service->getUrl());
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } catch (\Exception $e) {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            $driver->navigate()->to($link);

            //SE O RESULTADO FOR APENAS 1 PROCESSO, ESSE PROCESSO APARECERÁ LOGO NO COMEÇO SEM A LISTA DE PROCESSOS
            try {
                $noProcessoInfo = false;
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]'));
                $nroProcesso = $this->extractProcessNumber($driver->findElement(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]'))->getText());
                $processosLink[$nroProcesso] = $driver->getCurrentURL();
            } catch (\Exception $e) {
                $noProcessoInfo = true;
            }

            if ($noProcessoInfo) {
                try {
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                } catch (\Exception $e) {
                    continue;
                }

                $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);

                while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
                    $driver->navigate()->to($next);
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                    $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);
                }
            }

            $total = count($processosLink);
            $this->log($total . ' processo(s) encontrado(s)');

            if ($processarApenasProcessos) {
                $linksRegistrados = $service->getTodosProcessos(0); //TEMPORARIO
                $totalLinksRegistrados = count($linksRegistrados); //TEMPORARIO
            } else {
                $linksRegistrados = $service->registerProcessos($processosLink);
                $totalLinksRegistrados = count($linksRegistrados);
                $this->log($totalLinksRegistrados . ' A PROCESSAR');
            }

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */
            $index = 1;
            foreach ($linksRegistrados as $processo) {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                try {
                    $driver->navigate()->to($processo->getUrl());
                } catch (\Exception $e) {
                    $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                    continue;
                }

                $dadosProcesso = [];
                $partesProcesso = [];
                $nros1Instancia = [];
                $movimentacoes = [];
                $peticoesDiversas = [];
                $incidentesAcoesRecursosSentencas = [];
                $apensosVinculados = [];
                $composicaoJulgamento = [];
                $julgamento = [];

                try {
                    $pastaDigital = false;
                    $pastDigText = $driver->findElement(WebDriverBy::cssSelector('a.linkPasta'))->getText();
                    $this->log($pastDigText);
                    $pastaDigital = true;
                } catch (\Exception $e) {
                }

                try {
                    //DADOS
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                    $this->log('DADOS GERAIS');
                    $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
                } catch (\Exception $e) {
                }

                try {
                    //APENSOS / VINCULADOS
                    $table = 1;
                    if ($pastaDigital) $table = $table + 1;
                    $apensosVinculadosAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $apensosVinculados = $this->extractTable($apensosVinculadosAux, 4, true, ['Número', 'Classe', 'Apensamento', 'Motivo']);
                    if (count($apensosVinculados)) $this->log('APENSOS / VINCULADOS');
                } catch (\Exception $e) {
                }

                try {
                    //NÚMEROS DE 1ª INSTÂNCIA
                    $hasNros1Instancia = false;
                    $nros1InstanciaAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[4]/tbody/tr'));
                    $nros1Instancia = $this->extractTable($nros1InstanciaAux, 5, true, ['Nº de 1ª instância', 'Foro', 'Vara', 'Juiz', 'Obs']);
                    if (count($nros1Instancia)) {
                        $this->log('NÚMEROS DE 1ª INSTÂNCIA');
                        $hasNros1Instancia = true;
                    }
                } catch (\Exception $e) {
                    $hasNros1Instancia = false;
                }

                $linkPartes = false;
                try {
                    //PARTES
                    $this->log('PARTES');

                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                    sleep(1);
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = true;
                } catch (\Exception $e) {
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = false;
                    $this->log('NÃO TEM LINK TODAS PARTES');
                }

                $movimentacoesAux = [];
                try {
                    $this->log('MOVIMENTAÇÕES');
                    //MOVIMENTACOES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                    sleep(1);
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                } catch (\Exception $e) {
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                    $this->log('NÃO TEM LINK TODAS MOVIMENTAÇÕES');
                }
                $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);


                try {
                    //INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS
                    $table = 6;
                    if ($pastaDigital) $table = $table + 1;
                    if ($hasNros1Instancia) $table = $table + 1;
                    if ($linkPartes) $table = $table + 1;
                    $incidentesAcoesRecursosSentencasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $trash = array_shift($incidentesAcoesRecursosSentencasAux);
                    $incidentesAcoesRecursosSentencas = $this->extractTable($incidentesAcoesRecursosSentencasAux, 2, true, ['Recebido em', 'Classe']);
                    if (count($incidentesAcoesRecursosSentencas)) $this->log('INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS');
                } catch (\Exception $e) {
                }


                try {
                    //PETICOES DIVERSAS
                    $table = 7;
                    if ($pastaDigital) $table = $table + 1;
                    if ($hasNros1Instancia) $table = $table + 1;
                    if ($linkPartes) $table = $table + 1;
                    $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, false, ['Data', 'Tipo']);
                    if (count($peticoesDiversas)) $this->log('PETICOES DIVERSAS');
                } catch (\Exception $e) {
                }

                try {
                    $hasCompJulg = false;
                    //COMPOSIÇÃO DO JULGAMENTOS
                    $composicaoJulgamentoAux = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr.itemComposicaoJulgamento'));
                    $composicaoJulgamento = $this->extractTable($composicaoJulgamentoAux, 2, true, ['Participação', 'Magistrado']);
                    $hasCompJulg = true;
                    if (count($composicaoJulgamento)) $this->log('COMPOSIÇÃO DO JULGAMENTOS');
                } catch (\Exception $e) {
                }

                try {
                    //JULGAMENTOS
                    $table = 8;
                    if ($pastaDigital) $table = $table + 1;
                    if ($hasNros1Instancia || $linkPartes) $table = $table + 1;
                    if ($hasCompJulg) $table = $table + 2;
                    $julgamentoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $julgamento = $this->extractTable($julgamentoAux, 3, true, ['Data', 'Situação do julgamento', 'Decisão']);
                    if (count($julgamento)) $this->log('JULGAMENTOS');
                } catch (\Exception $e) {
                }

                $fieldsTratados[$processo->getProcesso()] = [
                    'DadosProcesso' => $dadosProcesso,
                    'ApensosVinculados' => $apensosVinculados,
                    'Numeros1Instancia' => $nros1Instancia,
                    'PartesProcesso' => $partesProcesso,
                    'Movimentacoes' => $movimentacoes,
                    'IncidentesAcoesRecursosSentencas' => $incidentesAcoesRecursosSentencas,
                    'PeticoesDiversas' => $peticoesDiversas,
                    'ComposicaoJulgamento' => $composicaoJulgamento,
                    'Julgamento' => $julgamento,
                ];

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);
            }
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);
        return true;
    }

    public function tjal1instanciaesaj($service, $processarApenasProcessos = false)
    {
        $processarApenasProcessos = true;//TEMP
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        try {
            $driver->get($service->getUrl());
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } catch (\Exception $e) {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            $driver->navigate()->to($link);

            //SE O RESULTADO FOR APENAS 1 PROCESSO, ESSE PROCESSO APARECERÁ LOGO NO COMEÇO SEM A LISTA DE PROCESSOS
            try {
                $noProcessoInfo = false;
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]'));
                $nroProcesso = $this->extractProcessNumber($driver->findElement(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]'))->getText());
                $processosLink[$nroProcesso] = $driver->getCurrentURL();
            } catch (\Exception $e) {
                $noProcessoInfo = true;
            }

            if ($noProcessoInfo) {
                try {
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                } catch (\Exception $e) {
                    continue;
                }

                $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);

                while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
                    $driver->navigate()->to($next);
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                    $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);
                }
            }

            $total = count($processosLink);
            $this->log($total . ' processo(s) encontrado(s)');

            if ($processarApenasProcessos) {
                $linksRegistrados = $service->getTodosProcessos(0); //TEMPORARIO
                $totalLinksRegistrados = count($linksRegistrados); //TEMPORARIO
            } else {
                $linksRegistrados = $service->registerProcessos($processosLink);
                $totalLinksRegistrados = count($linksRegistrados);
                $this->log($totalLinksRegistrados . ' A PROCESSAR');
            }

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */
            $index = 1;
            foreach ($linksRegistrados as $processo) {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                try {
                    $driver->navigate()->to($processo->getUrl());
                } catch (\Exception $e) {
                    $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                    continue;
                }

                $dadosProcesso = [];
                $partesProcesso = [];
                $movimentacoes = [];
                $peticoesDiversas = [];
                $incidentesAcoesRecursosSentencas = [];
                $apensosEntranhadosUnificados = [];
                $audiencia = [];
                $historico = [];

                $pastaDigital = false;
                try {
                    $pastDigText = $driver->findElement(WebDriverBy::xpath('//*[@id="linkPasta"]'))->getText();
                    $this->log($pastDigText);
                    $pastaDigital = true;
                } catch (\Exception $e) {
                    $pastaDigital = false;
                }

                try {
                    //DADOS
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                    $this->log('DADOS GERAIS');
                    $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
                } catch (\Exception $e) {
                }

                $linkPartes = false;
                try {
                    //PARTES
                    $this->log('PARTES');

                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                    sleep(1);
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = true;
                } catch (\Exception $e) {
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = false;
                    $this->log('NÃO TEM LINK TODAS PARTES');
                }

                $movimentacoesAux = [];
                $linkMovimentacoes = false;
                try {
                    $this->log('MOVIMENTAÇÕES');
                    //MOVIMENTACOES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                    sleep(1);
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                    $linkMovimentacoes = true;
                } catch (\Exception $e) {
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                    $linkMovimentacoes = false;
                    $this->log('NÃO TEM LINK TODAS MOVIMENTAÇÕES');
                }
                $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);

                try {
                    //PETICOES DIVERSAS
                    $table = 4;
                    if (!$pastaDigital) $table = $table - 1;
                    if ($linkPartes) {
                        $table = 5;
                        if (!$pastaDigital) $table = $table - 1;
                    }
                    $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, false, ['Data', 'Tipo']);
                    if (count($peticoesDiversas)) $this->log('PETICOES DIVERSAS');
                } catch (\Exception $e) {
                }

                try {
                    //INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS
                    $table = 6;
                    if (!$pastaDigital) $table = $table - 1;
                    $incidentesAcoesRecursosSentencasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $trash = array_shift($incidentesAcoesRecursosSentencasAux);
                    $incidentesAcoesRecursosSentencas = $this->extractTable($incidentesAcoesRecursosSentencasAux, 2, true, ['Recebido em', 'Classe']);
                    if (count($incidentesAcoesRecursosSentencas)) $this->log('INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS');
                } catch (\Exception $e) {
                }

                try {
                    //APENSOS, ENTRANHADOS E UNIFICADOS
                    $apensosEntranhadosUnificadosAux = $driver->findElements(WebDriverBy::xpath('//*[@id="dadosApenso"]/tr'));
                    $apensosEntranhadosUnificados = $this->extractTable($apensosEntranhadosUnificadosAux, 4, true, ['Número', 'Classe', 'Apensamento', 'Motivo']);
                    if (count($apensosEntranhadosUnificados)) $this->log('APENSOS, ENTRANHADOS E UNIFICADOS');
                } catch (\Exception $e) {
                }

                try {
                    //AUDIÊNCIAS
                    $table = 7;
                    if (!$pastaDigital) $table = $table - 1;
                    if ($linkPartes) {
                        $table = 8;
                        if (!$pastaDigital) $table = $table - 1;
                    }
                    $audienciaAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $audiencia = $this->extractTable($audienciaAux, 4, true, ['Data', 'Audiência', 'Situação', 'Qtd. Pessoas']);
                    if (count($audiencia)) $this->log('AUDIÊNCIAS');
                } catch (\Exception $e) {
                }

                try {
                    //HISTORICO DE CLASSES
                    $table = 8;
                    if (!$pastaDigital) $table = $table - 1;
                    if ($linkPartes) {
                        $table = 9;
                        if (!$pastaDigital) $table = $table - 1;
                    }
                    $historicoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $historico = $this->extractTable($historicoAux, 5, true, ['Data', 'Tipo', 'Classe', 'Área', 'Motivo']);
                    if (count($historico)) $this->log('HISTORICO DE CLASSES');
                } catch (\Exception $e) {
                }

                $fieldsTratados[$processo->getProcesso()] = [
                    'DadosProcesso' => $dadosProcesso,
                    'PartesProcesso' => $partesProcesso,
                    'Movimentacoes' => $movimentacoes,
                    'PeticoesDiversas' => $peticoesDiversas,
                    'IncidentesAcoesRecursosSentencas' => $incidentesAcoesRecursosSentencas,
                    'ApensosEntranhadosUnificados' => $apensosEntranhadosUnificados,
                    'Audiencia' => $audiencia,
                    'Historico' => $historico,
                ];

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);
            }
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);
        return true;
    }

    public function tjal2instanciaesaj($service, $processarApenasProcessos = false)
    {
        $processarApenasProcessos = true;//TEMP
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver();

        try {
            $driver->get($service->getUrl());
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } catch (\Exception $e) {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $words = $service->getTodasPalavras(true);
        $processosLink = [];
        foreach ($words as $word) {

            $link = str_replace('[PALAVRAS]', $word, $service->getUrl());

            $driver->navigate()->to($link);

            //SE O RESULTADO FOR APENAS 1 PROCESSO, ESSE PROCESSO APARECERÁ LOGO NO COMEÇO SEM A LISTA DE PROCESSOS
            try {
                $noProcessoInfo = false;
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]'));
                $nroProcesso = $this->extractProcessNumber($driver->findElement(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]'))->getText());
                $processosLink[$nroProcesso] = $driver->getCurrentURL();
            } catch (\Exception $e) {
                $noProcessoInfo = true;
            }

            if ($noProcessoInfo) {
                try {
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                } catch (\Exception $e) {
                    continue;
                }

                $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);

                while ($next = $this->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {
                    $driver->navigate()->to($next);
                    $processosHref = $driver->findElements(WebDriverBy::cssSelector('div.nuProcesso > a'));
                    $processosLink = $this->extractLinks($processosHref, true, false, $processosLink);
                }
            }

            $total = count($processosLink);
            $this->log($total . ' processo(s) encontrado(s)');

            if ($processarApenasProcessos) {
                $linksRegistrados = $service->getTodosProcessos(0); //TEMPORARIO
                $totalLinksRegistrados = count($linksRegistrados); //TEMPORARIO
            } else {
                $linksRegistrados = $service->registerProcessos($processosLink);
                $totalLinksRegistrados = count($linksRegistrados);
                $this->log($totalLinksRegistrados . ' A PROCESSAR');
            }

            $fieldsTratados = [];
            /** @var $linksRegistrados \Tribunal\Service\Processo[] */
            $index = 1;
            foreach ($linksRegistrados as $processo) {

                $this->log("({$this->percentage($totalLinksRegistrados,$index)}) {$index} de {$totalLinksRegistrados} => " . $processo->getProcesso());
                $index++;

                $hashAnterior = $processo->getHash();

                try {
                    $driver->navigate()->to($processo->getUrl());
                } catch (\Exception $e) {
                    $this->log("Página indisponível link => {$processo->getUrl()}", $this->logFileName);
                    continue;
                }

                $dadosProcesso = [];
                $partesProcesso = [];
                $nros1Instancia = [];
                $movimentacoes = [];
                $peticoesDiversas = [];
                $incidentesAcoesRecursosSentencas = [];
                $apensosVinculados = [];
                $composicaoJulgamento = [];
                $julgamento = [];

                try {
                    $pastaDigital = false;
                    $pastDigText = $driver->findElement(WebDriverBy::cssSelector('a.linkPasta'))->getText();
                    $this->log($pastDigText);
                    $pastaDigital = true;
                } catch (\Exception $e) {
                }

                try {
                    //DADOS
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                    $this->log('DADOS GERAIS');
                    $dadosProcesso = $this->extractVerticalTable($dadosProcessoAux, 2);
                } catch (\Exception $e) {
                }

                try {
                    //APENSOS / VINCULADOS
                    $table = 1;
                    if ($pastaDigital) $table = $table + 1;
                    $apensosVinculadosAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $apensosVinculados = $this->extractTable($apensosVinculadosAux, 4, true, ['Número', 'Classe', 'Apensamento', 'Motivo']);
                    if (count($apensosVinculados)) $this->log('APENSOS / VINCULADOS');
                } catch (\Exception $e) {
                }

                try {
                    //NÚMEROS DE 1ª INSTÂNCIA
                    $hasNros1Instancia = false;
                    $nros1InstanciaAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[4]/tbody/tr'));
                    $nros1Instancia = $this->extractTable($nros1InstanciaAux, 5, true, ['Nº de 1ª instância', 'Foro', 'Vara', 'Juiz', 'Obs']);
                    if (count($nros1Instancia)) {
                        $this->log('NÚMEROS DE 1ª INSTÂNCIA');
                        $hasNros1Instancia = true;
                    }
                } catch (\Exception $e) {
                    $hasNros1Instancia = false;
                }

                $linkPartes = false;
                try {
                    //PARTES
                    $this->log('PARTES');

                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkpartes"]'))->click();
                    sleep(1);
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tableTodasPartes"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = true;
                } catch (\Exception $e) {
                    $partesProcessoAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tablePartesPrincipais"]/tbody/tr'));
                    $partesProcesso = $this->extractVerticalTable($partesProcessoAux, 2);
                    $linkPartes = false;
                    $this->log('NÃO TEM LINK TODAS PARTES');
                }

                $movimentacoesAux = [];
                try {
                    $this->log('MOVIMENTAÇÕES');
                    //MOVIMENTACOES
                    $driver->findElement(WebDriverBy::xpath('//*[@id="linkmovimentacoes"]'))->click();
                    sleep(1);
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaTodasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                } catch (\Exception $e) {
                    $movimentacoesAux = $driver->findElements(WebDriverBy::xpath('//*[@id="tabelaUltimasMovimentacoes"]/tr'));
                    $movimentacoes = $this->extractTable($movimentacoesAux, 3, true, ['Data', 'empty', 'Movimento']);
                    $this->log('NÃO TEM LINK TODAS MOVIMENTAÇÕES');
                }
                $movimentacoes = $this->unsetKeyFromArray($movimentacoes, ['empty']);


                try {
                    //INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS
                    $table = 6;
                    if ($pastaDigital) $table = $table + 1;
                    if ($hasNros1Instancia) $table = $table + 1;
                    if ($linkPartes) $table = $table + 1;
                    $incidentesAcoesRecursosSentencasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $trash = array_shift($incidentesAcoesRecursosSentencasAux);
                    $incidentesAcoesRecursosSentencas = $this->extractTable($incidentesAcoesRecursosSentencasAux, 2, true, ['Recebido em', 'Classe']);
                    if (count($incidentesAcoesRecursosSentencas)) $this->log('INCIDENTES, AÇÕES INCIDENTAIS, RECURSOS E EXECUÇÕES DE SENTENÇAS');
                } catch (\Exception $e) {
                }


                try {
                    //PETICOES DIVERSAS
                    $table = 7;
                    if ($pastaDigital) $table = $table + 1;
                    if ($hasNros1Instancia) $table = $table + 1;
                    if ($linkPartes) $table = $table + 1;
                    $peticoesDiversasAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $peticoesDiversas = $this->extractTable($peticoesDiversasAux, 2, false, ['Data', 'Tipo']);
                    if (count($peticoesDiversas)) $this->log('PETICOES DIVERSAS');
                } catch (\Exception $e) {
                }

                try {
                    $hasCompJulg = false;
                    //COMPOSIÇÃO DO JULGAMENTOS
                    $composicaoJulgamentoAux = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr.itemComposicaoJulgamento'));
                    $composicaoJulgamento = $this->extractTable($composicaoJulgamentoAux, 2, true, ['Participação', 'Magistrado']);
                    $hasCompJulg = true;
                    if (count($composicaoJulgamento)) $this->log('COMPOSIÇÃO DO JULGAMENTOS');
                } catch (\Exception $e) {
                }

                try {
                    //JULGAMENTOS
                    $table = 8;
                    if ($pastaDigital) $table = $table + 1;
                    if ($hasNros1Instancia || $linkPartes) $table = $table + 1;
                    if ($hasCompJulg) $table = $table + 2;
                    $julgamentoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/table[' . $table . ']/tbody/tr'));
                    $julgamento = $this->extractTable($julgamentoAux, 3, true, ['Data', 'Situação do julgamento', 'Decisão']);
                    if (count($julgamento)) $this->log('JULGAMENTOS');
                } catch (\Exception $e) {
                }

                $fieldsTratados[$processo->getProcesso()] = [
                    'DadosProcesso' => $dadosProcesso,
                    'ApensosVinculados' => $apensosVinculados,
                    'Numeros1Instancia' => $nros1Instancia,
                    'PartesProcesso' => $partesProcesso,
                    'Movimentacoes' => $movimentacoes,
                    'IncidentesAcoesRecursosSentencas' => $incidentesAcoesRecursosSentencas,
                    'PeticoesDiversas' => $peticoesDiversas,
                    'ComposicaoJulgamento' => $composicaoJulgamento,
                    'Julgamento' => $julgamento,
                ];

                $processo->setDados(json_encode($fieldsTratados[$processo->getProcesso()]));
                $processo->setHash(md5($processo->getDados()));
                if ($hashAnterior != $processo->getHash()) $processo->setAlteracao(1);
                $processo->setProcessado(1);
                $processo->salvar(true);
            }
        }

        $driver->quit();

        $service->setColetando(0);
        $service->salvar(true);
        return true;
    }

    public function tjpa1instanciafisico($service, $processarApenasProcessos = false)
    {
        $processarApenasProcessos = false;//TEMP
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        $driver = $this->startDriver(true);

        try {
            $driver->get($service->getUrl());
            $this->log("Up and running!");
            $service->setOnline(1);
            $service->setColetando(1);
            $service->salvar(true);
        } catch (\Exception $e) {
            $service->setOnline(0);
            $service->setColetando(0);
            $service->salvar(true);
            return true;
        }

        $words = $service->getTodasPalavras();
        $processosLink = [];
        foreach ($words as $word) {

            $driver->navigate()->to($service->getUrl());//aqui
            $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="menuConsultaDetalhada"]/a')));
            $linkOnClick = $driver->findElement(WebDriverBy::xpath('//*[@id="menuConsultaDetalhada"]/a'))->getAttribute('onclick');
            $driver->executeScript($linkOnClick, []);
            try {
                $text = $driver->findElement(WebDriverBy::cssSelector('div.blockUI'))->getText();
                $this->log($text);
                $driver->executeScript("$('div.blockUI').remove();", []);

            } catch (\Exception $e) {
            }
            $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="radioPorNomeParte"]')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="radioPorNomeParte"]'))->click();
            $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="inputTextPorNomeParte"]')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="inputTextPorNomeParte"]'))->sendKeys($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="btnCaptcha_1G"]'))->click();

            $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="img_1G"]', '//*[@id="textCaptcha"]', '//*[@id="buttonPesquisar"]', false);

            while (true) {
                try {
                    try {
                        $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="msgGeral"]')));
                        $text = $driver->findElement(WebDriverBy::xpath('//*[@id="msgGeral"]'))->getText();
                        $this->log($text);
                        $driver->executeScript('$("#msgGeral").dialog("close");', []);
                        $text = $driver->findElement(WebDriverBy::xpath('//*[@id="textCaptcha"]'))->getAttribute('value');
                        $this->log($text);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="textCaptcha"]'))->sendKeys(strtolower($text));
                        $driver->findElement(WebDriverBy::xpath('//*[@id="buttonPesquisar"]'))->click();
                        $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="msgGeral"]')));
                        $text = $driver->findElement(WebDriverBy::xpath('//*[@id="msgGeral"]'))->getText();
                        $this->log($text);
                        $driver->executeScript('$("#msgGeral").dialog("close");', []);
                    } catch (\Exception $e) {
                    }
                    $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="div-table-processos"]')));
                    break;
                } catch (\Exception $e) {
                    $driver = $this->hasCaptcha($driver, 'xpath', '//*[@id="img_1G"]', '//*[@id="textCaptcha"]', '//*[@id="buttonPesquisar"]', false);
                }
            }

            $rows = $driver->findElements(WebDriverBy::cssSelector('table > tbody > tr'));
            foreach ($rows as $row) {
                $click = $row->getAttribute('onclick');
                $driver->executeScript($click, []);
            }
            $this->log(count($rows));

            die;

        }

    }

    public
    function extractLinks($aTags, $labelKey = false, $acceptLinkEmptyLabel = false, $arrayToIncrement = [], $excludeText = [], $includeTextOnly = [])
    {
        $links = $arrayToIncrement;
        /** @var $aTag \Facebook\WebDriver\WebDriver */
        foreach ($aTags as $aTag) {
            $label = $this->cleanString($aTag->getText());
            if (!$acceptLinkEmptyLabel && $label == '') continue;
            if (in_array($label, $excludeText)) continue;
            if (count($includeTextOnly) && !in_array($label, $includeTextOnly)) continue;
            if ($labelKey) {
                $links[$label] = $aTag->getAttribute('href');
                continue;
            }
            $links[] = $aTag->getAttribute('href');
        }
        return $links;
    }

    public
    function extractProcessNumber($string, $returnSelfString = true, $regex = '([0-9]{7}[-][0-9]{2}[.][0-9]{4}[.][0-9]{1}[.][0-9]{2}[.][0-9]{4})')
    {
        preg_match_all($regex, $string, $nroProcessoAux);
        $nroProcesso = false;
        if ($returnSelfString) $nroProcesso = $string;
        if (isset($nroProcessoAux[0][0])) {
            $nroProcesso = $nroProcessoAux[0][0];
        }
        return $nroProcesso;
    }

    public function cnjNumberFormatter($processo, $mask = '#######-##.####.#.##.####', $min = 14, $max = 20)
    {
        $processoD = preg_replace('/\D/', '', $processo);
        if (strlen($processoD) < $min || strlen($processoD) > $max) return $processo;
        $processoNew = str_pad($processoD, $max, '0', STR_PAD_LEFT);

        $year = substr($processoNew, 9, 4);
        if ($year < 1970 || $year > date('Y')) return $processo;

        return $this->mask($processoNew, $mask);
    }

    public function processNumberFormatter($processo, $mask = '###/#.##.#######-#', $min = 14, $returnSelfIfError = false)
    {
        // ###/#.##.#######-# => THEMIS
        // #######-##.####.#.##.#### => CNJ
        $processoD = preg_replace('/\D/', '', $processo);
        $processoD = str_replace(' ', '', $processoD);
        if (strlen($processoD) <> $min) {
            if ($returnSelfIfError) return $processo;
            return false;
        }

        return $this->mask($processoD, $mask);
    }

    public function yearValidate($text, $start = 11, $length = 4, $yearMin = 1970, $yearMax = '')
    {
        if ($yearMax == '') $yearMax = date('Y');
        $year = substr($text, $start, $length);
        if ($year < $yearMin || $year > $yearMax) return false;
        return true;
    }

    public function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public function onlyDigits($text)
    {
        return preg_replace('/\D/', '', $text);
    }

    public
    function explodeByPattern($elements, $delimeter = ';', $arrayToIncrement = [])
    {
        $aux = $arrayToIncrement;
        /** @var $aTag \Facebook\WebDriver\WebDriver */
        foreach ($elements as $element) {
            $text = $this->cleanString($element->getText());
            $arr = explode($delimeter, $text);
            if (count($arr) < 2) continue;
            if ($arr[0] == '') continue;
            @$aux[$this->cleanString($arr[0])] = $this->cleanString($arr[1]);
        }
        return $aux;
    }

    public
    function extractRegularTable($table, $preserveLink = false, $headerStatic = [])
    {
        /** @var $table \Facebook\WebDriver\Remote\RemoteWebDriver */
        $header = $headerStatic;
        if (!count($header)) {
            $header = [];
            $columns = $table->findElements(WebDriverBy::xpath('//thead/tr[1]/th'));
            if (!count($columns)) {
                $columns = $table->findElements(WebDriverBy::xpath('//thead/tr[1]/td'));
            }
            foreach ($columns as $column) {
                $header[] = $this->cleanString($column->getText());
            }
        }

        $rows = $table->findElements(WebDriverBy::xpath('//tbody/tr'));
        return $this->extractTable($rows, count($header), $preserveLink, $header);
    }

    public
    function extractVerticalTable($rows, $columns = 2, $preserveLink = false, $headerIndex = 0)
    {
        $colValueIndex = $headerIndex + 1;
        $aux = [];
        $lastName = 'sem_titulo_' . date('YmdHis');
        /** @var $rows \Facebook\WebDriver\Remote\RemoteWebDriver[] */
        foreach ($rows as $row) {
            $colunas = $row->findElements(WebDriverBy::cssSelector("td"));
            if (count($colunas) < $columns) continue;
            $link = '';
            if ($preserveLink) {
                try {
                    $link = $colunas[$colValueIndex]->findElement(WebDriverBy::tagName('a'))->getAttribute('href');
                    if (preg_match('/javascript|#/', $link)) $link = '';
                } catch (\Exception $e) {
                }
            }
            $name = str_replace([' : ', ' :', ': ', ':', '.'], '', $this->cleanString($colunas[$headerIndex]->getText()));
            if ($name == '') $name = $lastName;
            @$aux[$name] .= ($link) ? "{$this->cleanString($colunas[$colValueIndex]->getText())} [Link => {$link}] " : $this->cleanString($colunas[$colValueIndex]->getText()) . '; ';
            $lastName = $name;
        }
        return $aux;
    }

    public
    function extractTable($rows, $columnsNumber = 4, $preserveLink = false, $headerStatic = [])
    {
        /** @var $rows \Facebook\WebDriver\Remote\RemoteWebDriver[] */
        $header = $headerStatic;
        if (!count($header)) {
            $header = [];
            $countIndex = 0;
            foreach ($rows as $index => $row) {
                $columns = $row->findElements(WebDriverBy::cssSelector("td"));
                if (count($columns) < $columnsNumber) continue;
                $colunaAux = '';
                $header = [];
                foreach ($columns as $column) {
                    $colunaAux .= $this->cleanString($column->getText());
                    $header[] = $column->getText();
                }
                if ($colunaAux == '') continue;

                $countIndex = $index;
                break;
            }
            for ($i = 0; $i <= $countIndex; $i++) {
                $trash = array_shift($rows);
            }
        }

        $aux = [];
        foreach ($rows as $row) {
            $columns = $row->findElements(WebDriverBy::cssSelector("td"));
            if (count($columns) < $columnsNumber) continue;
            $colunaAux = '';
            foreach ($columns as $column) {
                $colunaAux .= $this->cleanString($column->getText());
            }
            if ($colunaAux == '') continue;
            $auxRow = [];
            foreach ($columns as $columnIndex => $column) {
                $link = '';
                if ($preserveLink) {
                    try {
                        $link = $column->findElement(WebDriverBy::tagName('a'))->getAttribute('href');
                        if (preg_match('/javascript|#/', $link)) $link = '';
                    } catch (\Exception $e) {
                    }
                }
                if (!isset($header[$columnIndex])) continue;
                $auxRow[$header[$columnIndex]] = ($link) ? "{$this->cleanString($column->getText())} [Link => {$link}]" : $this->cleanString($column->getText());
            }
            $aux[] = $auxRow;
        }
        return $aux;
    }

    public
    function hasNext($driver, $webDriverBy, $webDriverByArg)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $next = false;
        try {
            $next = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg))->getAttribute('href');
        } catch (NoSuchElementException $e) {
            $next = false;
        }
        return $next;
    }

    public
    function ocrtest($driver, $webDriverBy, $webDriverByArg, $min = 4, $max = 4)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $captchaImg = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg));
        $file = $this->takeScreenshot($driver, $captchaImg);
        $tess = new TesseractOCR($file);
        $textRecognized = $tess->run();
        if (strlen($textRecognized) >= $min && $textRecognized <= $max) return $textRecognized;
        return false;
    }

    public
    function ocr($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton = null, $onlyDigits = true, $loop = true, $min = 4, $max = 4)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */

        $trial = 0;

        try {

            $trial++;

            $captchaImg = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg));
            $this->logCaptcha('OCR => HAS CAPTCHA!', $this->logFileName);
            $img = $this->takeScreenshot($driver, $captchaImg);
            while (filesize($img) < 1) {
                $this->log("OCR SLEEP");
                sleep(1);
            }
            $tess = new TesseractOCR($img);
            if ($onlyDigits) {
                $tess->whitelist(range(0, 9));
            } else {
                $tess->whitelist(range('a', 'z'), range(0, 9));
            }
            try {
                $textRecognized = $tess->run();
                $this->log("textRecognized => {$textRecognized}");
                if ($textRecognized == '') throw new \Exception('Empty');
            } catch (\Exception $e) {
                $this->log($e->getMessage());
                $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg))->getAttribute('src');
                return $this->ocr($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton, $onlyDigits, $loop, $min, $max);
            }

            if (strlen($textRecognized) >= $min && strlen($textRecognized) <= $max) {
                $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->sendKeys($textRecognized);
                if ($webDriverByArgClickButton) $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgClickButton))->click();
            } else {
                return $driver;
            }

            sleep(1);

            if ($loop) {
                $timesCaptcha = 1;
                while ($this->stillHasCaptcha($driver, $webDriverBy, $webDriverByArg)) {

                    sleep(1);
                    $captchaImg = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg));
                    $this->logCaptcha('HAS CAPTCHA!', $this->logFileName);
                    $img = $this->takeScreenshot($driver, $captchaImg);

                    while (filesize($img) < 1) {
                        sleep(1);
                    }

                    $tess = new TesseractOCR($img);
                    if ($onlyDigits) {
                        $tess->whitelist(range(0, 9));
                    } else {
                        $tess->whitelist(range('a', 'z'), range(0, 9));
                    }
                    $textRecognized = $tess->run();

                    if (strlen($textRecognized) >= $min && strlen($textRecognized) <= $max) {
                        $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->sendKeys($textRecognized);
                        if ($webDriverByArgClickButton) $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgClickButton))->click();
                    }

                    if ($timesCaptcha > 3) throw new \Exception("Captcha ERROR. Attempted {$timesCaptcha}x already.");

                    $timesCaptcha++;
                }
            }

        } catch (NoSuchElementException $e) {
            return $driver;
        } catch (\Exception $e) {
            if ($trial > 3) throw new \Exception($e->getMessage());
            $this->log($trial . ' => TENTATIVA CAPTCHA');
            return $this->ocr($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton, $onlyDigits, $loop, $min, $max);
        }

        return $driver;
    }

    public
    function ocrRefresh($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton = null, $webDriverByArgRefreshButton = null, $onlyDigits = true, $loop = true, $min = 4, $max = 4)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */

        $trial = 0;

        try {

            $trial++;

            $captchaImg = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg));
            $this->logCaptcha('OCR => HAS CAPTCHA!', $this->logFileName);
            $img = $this->takeScreenshot($driver, $captchaImg);
            while (filesize($img) < 1) {
                $this->log("OCR SLEEP");
                sleep(1);
            }
            $tess = new TesseractOCR($img);
            if ($onlyDigits) {
                $tess->whitelist(range(0, 9));
            } else {
                $tess->whitelist(range('a', 'z'), range(0, 9));
            }
            try {
                $textRecognized = $tess->run();
                $this->log("textRecognized => {$textRecognized}");
                if ($textRecognized == '') throw new \Exception('Empty');
            } catch (\Exception $e) {
                $this->log($e->getMessage());
                if ($webDriverByArgRefreshButton) $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgRefreshButton))->click();
                return $this->ocrRefresh($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton, $webDriverByArgRefreshButton, $onlyDigits, $loop, $min, $max);
            }

            if (strlen($textRecognized) >= $min && strlen($textRecognized) <= $max) {
                $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->sendKeys($textRecognized);
                if ($webDriverByArgClickButton) $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgClickButton))->click();
            } else {
                return $driver;
            }

            sleep(1);

            if ($loop) {
                $timesCaptcha = 1;
                while ($this->stillHasCaptcha($driver, $webDriverBy, $webDriverByArg)) {

                    sleep(1);
                    $captchaImg = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg));
                    $this->logCaptcha('HAS CAPTCHA!', $this->logFileName);
                    $img = $this->takeScreenshot($driver, $captchaImg);

                    while (filesize($img) < 1) {
                        sleep(1);
                    }

                    $tess = new TesseractOCR($img);
                    if ($onlyDigits) {
                        $tess->whitelist(range(0, 9));
                    } else {
                        $tess->whitelist(range('a', 'z'), range(0, 9));
                    }
                    $textRecognized = $tess->run();

                    if (strlen($textRecognized) >= $min && strlen($textRecognized) <= $max) {
                        $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->sendKeys($textRecognized);
                        if ($webDriverByArgClickButton) $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgClickButton))->click();
                    }

                    if ($timesCaptcha > 3) throw new \Exception("Captcha ERROR. Attempted {$timesCaptcha}x already.");

                    $timesCaptcha++;
                }
            }

        } catch (NoSuchElementException $e) {
            return $driver;
        } catch (\Exception $e) {
            if ($trial > 3) throw new \Exception($e->getMessage());
            $this->log($trial . ' => TENTATIVA CAPTCHA');
            return $this->ocr($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton, $onlyDigits, $loop, $min, $max);
        }

        return $driver;
    }

    public
    function hasCaptcha($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton = null, $loop = true, $refreshCaptcha = null, $lowercase = true, $comment = null)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */

        $trial = 0;

        try {

            $trial++;

            $captchaImg = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg));
            $this->logCaptcha('HAS CAPTCHA!', $this->logFileName);
            $img = $this->takeScreenshot($driver, $captchaImg);
            $captcha = new TwoCaptcha();
            $resCaptcha = $captcha->sendImageCaptcha($img,$comment);
            $this->lastRequestId = $captcha->lastRequestId;
            $this->log("CAPTCHA SOLVED => {$resCaptcha}");
            if ($lowercase) $resCaptcha = strtolower($resCaptcha);
            $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->clear();
            $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->sendKeys($resCaptcha);
            if ($webDriverByArgClickButton) $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgClickButton))->click();

            sleep(1);

            if ($loop) {
                $timesCaptcha = 1;
                while ($this->stillHasCaptcha($driver, $webDriverBy, $webDriverByArg)) {

                    sleep(1);
                    $captchaImg = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg));
                    $this->logCaptcha('HAS CAPTCHA!', $this->logFileName);
                    $img = $this->takeScreenshot($driver, $captchaImg);

                    $resCaptcha = $captcha->sendImageCaptcha($img,$comment);
                    if ($lowercase) $resCaptcha = strtolower($resCaptcha);
                    $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->clear();
                    $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgInputField))->sendKeys($resCaptcha);
                    if ($webDriverByArgClickButton) $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArgClickButton))->click();

                    if ($timesCaptcha > 3) throw new \Exception("Captcha ERROR. Attempted {$timesCaptcha}x already.");

                    $timesCaptcha++;
                }
            }

        } catch (NoSuchElementException $e) {
            return $driver;
        } catch (\Exception $e) {
            if (preg_match('/ERROR_TOO_BIG_CAPTCHA_FILESIZE/',$e->getMessage())) {
                throw new \Exception($e->getMessage());
            }
            if ($trial > 3) throw new \Exception($e->getMessage());
            $this->log($trial . ' => TENTATIVA CAPTCHA');
            if ($refreshCaptcha) {
                $driver->findElement(WebDriverBy::$webDriverBy($refreshCaptcha))->click();
                sleep(3);
            }
            return $this->hasCaptcha($driver, $webDriverBy, $webDriverByArg, $webDriverByArgInputField, $webDriverByArgClickButton, $loop, $refreshCaptcha, $lowercase, $comment);
        }

        return $driver;
    }

    public
    function stillHasCaptcha($driver, $webDriverBy, $webDriverByArg)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $has = false;
        try {
            $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::$webDriverBy($webDriverByArg)));
            $has = $driver->findElement(WebDriverBy::$webDriverBy($webDriverByArg))->getAttribute('src');
        } catch (NoSuchElementException $e) {
            $has = false;
        }
        return $has;
    }

    public function getUrlDomain($url)
    {
        $currentUrlArr = explode('//', $url);
        $secondPart = explode('/', $currentUrlArr[1]);
        if (count($secondPart) < 2) return $url;
        $currentUrl = $currentUrlArr[0] . '//' . $secondPart[0];
        return $currentUrl;
    }

    public function hasReCaptcha($driver, $keyElementClass = 'g-recaptcha', $webDriverBy = null, $webDriverByArg = null)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $currentUrl = $this->getUrlDomain($driver->getCurrentURL());

        $key = '';
        if ($keyElementClass != 'g-recaptcha' || substr($keyElementClass, 0, 1) == '=') {
            $key = str_replace('=', '', $keyElementClass);
        }

        if ($webDriverBy && $webDriverByArg) {
            $has = false;
            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::$webDriverBy($webDriverByArg)));
                $captcha = new TwoCaptcha();
                if (!$key) $key = $driver->findElement(WebDriverBy::className($keyElementClass))->getAttribute('data-sitekey');
                $res = $captcha->sendReCaptcha($key, $currentUrl);
                $this->lastRequestId = $captcha->lastRequestId;
                return $res;
            } catch (NoSuchElementException $e) {
                $this->log("NO ReCaptcha");
                return false;
            }
        }

        try {
            $captcha = new TwoCaptcha();
            if (!$key) $key = $driver->findElement(WebDriverBy::className($keyElementClass))->getAttribute('data-sitekey');
            $res = $captcha->sendReCaptcha($key, $currentUrl);
            $this->lastRequestId = $captcha->lastRequestId;
            return $res;
        } catch (NoSuchElementException $e) {
            $this->log("NO ReCaptcha!!!");
            return false;
        }
    }

    public function complainCaptcha()
    {
        $service = new TwoCaptcha();
        return $service->complain($this->lastRequestId);
    }

    public function navigate($driver, $url, $sleep = 5, $attempt = 3)
    {
        //IMPROVE IT - NOT WORK
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $count = 0;
        $ret = false;
        while (true) {
            try {
                $count++;
                $driver->navigate()->to($url);
                $ret = $driver;
                break;
            } catch (\Exception $e) {
                if ($count >= $attempt) break;
                $this->log("Try access {$url} again in {$sleep} sec");
                sleep($sleep);
            }
        }
        return $ret;
    }

    public function closeAlert($driver, $wait = 1)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        try {
            $driver->wait($wait)->until(
                WebDriverExpectedCondition::alertIsPresent(),
                'Waiting for an alert'
            );
            $driver->switchTo()->alert()->accept();
        } catch (\Exception $e) {
        }

        return $driver;
    }

    public function cleanString($string, $stripChars = [])
    {
        $aux = trim(preg_replace('/\s+/', ' ', $string));
        if (count($stripChars)) $aux = str_replace($stripChars, '', $aux);
        return $aux;
    }

    public function log($message, $file = false)
    {
        $time = '[' . date('Y-m-d H:i:s') . '] => ';
        $message = $time . $message . PHP_EOL;
        echo $message;
        if ($file) file_put_contents("./data/tribunais-log/{$file}.txt", $message, FILE_APPEND | LOCK_EX);
    }

    public function logCaptcha($message, $file = false)
    {
        $time = '[' . date('Y-m-d H:i:s') . '] => ';
        $message = $time . $message . PHP_EOL;
        echo $message;
        if ($file) file_put_contents("./data/tribunais-captcha/{$file}.txt", $message, FILE_APPEND | LOCK_EX);
    }

    public function cleanLog($file, $leaveLines = 20)
    {
        $file = "./data/tribunais-log/{$file}.txt";
        if (!file_exists($file)) file_put_contents($file, '');
        $lines_array = file($file);
        $lines = count($lines_array);
        $new_output = "";
        if ($lines <= $leaveLines) return true;
        for ($i = $lines - $leaveLines; $i < $lines; $i++) {
            $new_output .= $lines_array[$i];
        }
        file_put_contents($file, $new_output);
        return true;
    }

    public function unsetKeyFromArray($data, $key = [])
    {
        if (!count($key)) return $data;
        $tratados = [];
        foreach ($data as $item) {
            foreach ($key as $kItem) {
                if (isset($item[$kItem])) unset($item[$kItem]);
            }
            $tratados[] = $item;
        }
        return $tratados;
    }

    public function openSite($driver, $url)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        $driver->get('https://hide.me/pt/proxy');

        $urlField = $driver->findElement(WebDriverBy::xpath('//*[@id="u"]'));
        $urlField->sendKeys($url);
        $submit = $driver->findElement(WebDriverBy::xpath('//*[@id="hide_register_save"]'));
        $submit->click();

        return $driver;
    }

    public function isDomainAvailable($domain)
    {
        //check, if a valid url is provided
        if (!filter_var($domain, FILTER_VALIDATE_URL)) {
            return false;
        }

        //initialize curl
        $curlInit = curl_init($domain);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
        if (preg_match('/https:/', $domain)) curl_setopt($curlInit, CURLOPT_SSL_VERIFYPEER, false);

        //get answer
        $response = curl_exec($curlInit);

        curl_close($curlInit);

        if ($response) return true;

        return false;
    }

    public function takeScreenshot($driver, $element = null)
    {
        // Change the Path to your own settings
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $element \Facebook\WebDriver\WebDriverElement */
        $time = time();
        $dir = "./data/captchas/";
        $screenshot = "{$dir}{$time}.png";

        // Change the driver instance
        $driver->takeScreenshot($screenshot);
        if (!file_exists($screenshot)) {
            throw new \Exception('Could not save screenshot');
        }

        if (!(bool)$element) {
            return $screenshot;
        }

        $element_screenshot = $screenshot;

        $element_width = $element->getSize()->getWidth();
        $element_height = $element->getSize()->getHeight();

//        $element_src_x = $element->getLocation()->getX();
//        $element_src_y = $element->getLocation()->getY();

        $element_src_x = $element->getLocationOnScreenOnceScrolledIntoView()->getX();
        $element_src_y = $element->getLocationOnScreenOnceScrolledIntoView()->getY();

        // Create image instances
        $src = imagecreatefrompng($screenshot);
        $dest = imagecreatetruecolor($element_width, $element_height);

        // Copy
        imagecopy($dest, $src, 0, 0, $element_src_x, $element_src_y, $element_width, $element_height);

        imagepng($dest, $element_screenshot);

        // unlink($screenshot); // unlink function might be restricted in mac os x.

        if (!file_exists($element_screenshot)) {
            throw new \Exception('Could not save element screenshot');
        }

        return $element_screenshot;
    }

    public function percentage($total, $qtd)
    {
        $per = number_format((100 / $total) * $qtd, 2);
        return $per . '%';
    }

    public function startDriver($maximize = true, $proxy = false, $userDataActive = true)
    {
//        $this->capabilities = DesiredCapabilities::chrome();
        $this->capabilities = DesiredCapabilities::htmlUnitWithJS();

        $this->agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36';

        if ($userDataActive) {

            $userData = Config::getConfig('UserData');

            if (is_array($userData)) {
                var_dump('2');
                $options = new ChromeOptions();
                $options->addArguments($userData);

                $options->addArguments(array("disable-infobars"));


                $prefs = array(
                  'download.default_directory' => 'c:\\temporario',
                  'download.prompt_for_download' => false,
                  'download.directory_upgrade' => true,
                  'safebrowsing.enabled' => false
                );

                $options->setExperimentalOption('prefs', $prefs);

                $this->capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
            }
        }
        if ($this->mobile) {
            $options = new ChromeOptions();
            $options->setExperimentalOption("mobileEmulation", ["deviceName" => "Nexus 5"]);
            $options->addArguments(['--user-agent=Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19']);
            $this->capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        }
        if ($this->incognito) {
            $options = new ChromeOptions();
            $options->addArguments(['--incognito']);
            $this->capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        }
        if ($this->agent) {
            $options = new ChromeOptions();
            $options->addArguments(['--user-agent=' . $this->agent]);
            $this->capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        }

        if ($proxy) {
            if (!$this->proxy) {
                $this->proxy = $this->getBestProxy();
            }
            $proxy = "{$this->proxy}";

            $this->capabilities->setCapability(WebDriverCapabilityType::PROXY,
                [
                    'proxyType' => 'MANUAL',
                    'httpProxy' => $proxy,
                    'ftpProxy' => $proxy,
                    'sslProxy' => $proxy,
                    'noProxy' => '127.0.0.1, 192.168.0.0/24'
                ]
            );
        }

        try {
            $driver = RemoteWebDriver::create($this->host, $this->capabilities, 0, 0);
//            $driver = WebDriver::create($this->host, $this->capabilities, 0, 0);
            if ($maximize) $driver->manage()->window()->maximize();
            if (!$driver instanceof RemoteWebDriver) throw new \Exception('Cant create connection with selenium server.');
//            if (!$driver instanceof RemoteWebDriver) throw new \Exception('Cant create connection with selenium server.');
        } catch (\Exception $e) {
            $this->log('error on line ' . $e->getLine() . ' => ' . $e->getMessage() . ' => ' . $e->getCode(), $this->logFileName);
            return true;
        }

        return $driver;
    }

    public function startDriver2()
    {
        $options = new ChromeOptions();

        $options->addArguments(array("disable-infobars"));

        $prefs = array(
            'download.default_directory' => 'c:\\temporario',
            'download.prompt_for_download' => false,
            'download.directory_upgrade' => true,
            'safebrowsing.enabled' => false
        );
        
        $options->setExperimentalOption('prefs', $prefs);
        $this->capabilities = DesiredCapabilities::chrome();
        $this->capabilities->setCapability(ChromeOptions::CAPABILITY, $options);

        $driver = RemoteWebDriver::create($this->host, $this->capabilities, 5000);

        return $driver;
    }

    public function findBetween($string, $start, $end, $clean = true)
    {
        $startsAt = strpos($string, $start) + strlen($start);
        $endsAt = strpos($string, $end, $startsAt);
        $result = substr($string, $startsAt, $endsAt - $startsAt);
        if ($clean) $result = $this->cleanString($result);
        if ($result != '') return $result;
        return false;
    }

    public function getBestProxy()
    {
        $timeout = 1;
        $proxyLink = "https://www.proxydocker.com/en/proxylist/api?email=it.brunoms%40gmail.com&country=All&city=[city]&port=All&type=HTTP&anonymity=All&state=All&need=All&format=json";
        $cities = [
            'salvador',
            'fortaleza',
            'recife',
            'porto+alegre',
            'sao+paulo',
            'curitiba',
            'belo+horizonte',
            'rio+de+janeiro',
            'brasilia',
        ];

        shuffle($cities);

        $proxySelected = '';

        foreach ($cities as $city) {
            $auxProxy = json_decode(file_get_contents(str_replace('[city]', $city, $proxyLink)), true);
            $totalProxies = count($auxProxy['Proxies']);
            if ($totalProxies < 1) {
                $this->log("NO PROXY RESULT");
                continue;
            }
            //rand(0, $totalProxies - 1)
            $chooseOne = [];
            foreach ($auxProxy['Proxies'] as $itemP) {
                /*if ($con = @fsockopen($itemP['ip'], $itemP['port'], $eroare, $eroare_str, $timeout)) {
                    $chooseOne = $itemP;
                    break;
                } else {
                    $this->log($itemP['ip'] . ':' . $itemP['port'] . ' => NOT WORKING');
                }*/

                $url = 'http://dynupdate.no-ip.com/ip.php';
                $proxy = $itemP['ip'] . ':' . $itemP['port'];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
                curl_setopt($ch, CURLOPT_TIMEOUT, 3); //timeout in seconds
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                if ($curl_scraped_page) {
                    $chooseOne = $itemP;
                    break;
                } else {
                    $this->log($itemP['ip'] . ':' . $itemP['port'] . ' => SLOW');
                }
            }

            if (!count($chooseOne)) continue;
            $proxySelected = $chooseOne['ip'] . ':' . $chooseOne['port'];
            $this->log("$city => $proxySelected");
            break;
        }

        return $proxySelected;
    }

    public function decryptMd5($code, $answer)
    {
        $this->log($answer);

        $arr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

        $maxNum = 5;
        if (strlen($answer) > $maxNum) {
            return;
        }

        for ($i = 0; $i < count($arr); $i++) {
            $temp = $answer . $arr[$i];
            if (md5($temp) == $code) {
                return $temp;
            }

            $res = $this->decryptMd5($code, $temp);

            if (strlen($res) > 0) {
                return $res;
            }

        }
    }


    public function addVara($processo, $vara, $debug = false, $origem = null)
    {
        /** @var $processo \Tribunal\Service\Job */
        if ($vara == '' || is_null($vara) || empty($vara) || $vara == '0' || $vara == ' ' || (strlen($vara) < 5)) return true;
        if (!is_string($vara)) return true;

        /**
         * NOT BLOQUEADO PARA AJUSTE DOS PROBLEMATICOS APONTADOS PELA VALÉRIA
         */
//        $check = new Job();
//        $check->setId($processo->getId());
//        $check->load();
//        if ($check->getAlterado()) return true;
        /**
         * FIM BLOQUEIO
         */

        $processo->setJuizoVara($vara);
        $processo->setAlterado(1);
        if ($origem) $processo->setOrigem($origem);
        if (!$debug) $processo->salvar(true);
        $time = '[' . date('Y-m-d H:i:s') . '] => ';
        echo "{$time}{$processo->getNumProcJust()} => {$vara}" . PHP_EOL;
        return true;
    }

    public function saveCache($functionName, $numProc)
    {
        file_put_contents($this->tmpDir . $functionName, "$numProc\n", FILE_APPEND);
    }

    /**
     * Verifica se o arquivo de cache existe.
     * Se existir, verifica se $numProc existe dentro do arquivo.
     *
     * @param string $functionName
     * @param string $numProc
     * @return boolean true quando $numproc existir dentro do arquivo. false caso contrário.
     */
    public function cacheExists($functionName, $numProc)
    {
        if (!file_exists($this->tmpDir . $functionName)) {
            return false;
        }

        if (strpos(file_get_contents($this->tmpDir . $functionName), $numProc) !== false) {
            return true;
        }

        return false;
    }

    /**
     * Apaga o cache da função que está sendo utilizada.
     *
     * @param string $functionName nome da função
     */
    public function clearCache($functionName)
    {
        @unlink($this->tmpDir . $functionName);
    }

}