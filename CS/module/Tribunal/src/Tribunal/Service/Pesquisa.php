<?php

namespace Tribunal\Service;

use \Tribunal\Entity\Pesquisa as Entity;

class Pesquisa extends Entity
{

    public $classTribunal = '\Tribunal\Service\Tribunal';
    public $optionsPesquisarTodosTribunais = ["0" => "Não","1" => "Sim"];

    public function getObjTribunal()
    {
        $class = $this->classTribunal;
        $service = new $class;
        $service->setId($this->getIdTribunal());
        $service->load();
        return $service;
    }

    public function getLabelPesquisarTodosTribunais()
    {
        $mapa = $this->optionsPesquisarTodosTribunais;
        if (isset($mapa[$this->getPesquisarTodosTribunais()])) return $mapa[$this->getPesquisarTodosTribunais()];
    }

}