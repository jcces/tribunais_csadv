<?php

namespace Tribunal\Service;

use Estrutura\Service\Conexao;
use \Tribunal\Entity\Tribunal as Entity;

class Tribunal extends Entity
{

    public $optionsOnline = ["0" => "Não", "1" => "Sim"];
    public $optionsColetando = ["0" => "Não", "1" => "Sim"];

    public function getLabelOnline($label = false)
    {
        $mapa = $this->optionsOnline;
        if ($label) {
            if ($this->getOnline() == '0') return '<span class="label label-danger">' . $mapa[$this->getOnline()] . '</span>';
            if ($this->getOnline() == '1') return '<span class="label label-success">' . $mapa[$this->getOnline()] . '</span>';
        }
        if (isset($mapa[$this->getOnline()])) return $mapa[$this->getOnline()];
    }

    public function getLabelColetando($label = false)
    {
        $mapa = $this->optionsColetando;
        if ($label) {
            if ($this->getColetando() == '0') return '<span class="label label-default">' . $mapa[$this->getColetando()] . '</span>';
            if ($this->getColetando() == '1') return '<span class="label label-success">' . $mapa[$this->getColetando()] . '</span>';
        }
        if (isset($mapa[$this->getColetando()])) return $mapa[$this->getColetando()];
    }

    public function getTodasPalavras($tratar = false)
    {
        $sql = "SELECT * FROM TRIBUNAL_PESQUISA
                WHERE ID_TRIBUNAL = '{$this->getId()}' 
                OR PESQUISAR_TODOS_TRIBUNAIS = '1'";

        $data = Conexao::listarSql($sql);
        $dataObj = $this->populateService(new Pesquisa(), $data);

        $replace = ' ';
        if ($tratar) $replace = '+';

        $tratados = [];
        /** @var $dataObj \Tribunal\Service\Pesquisa[] */
        foreach ($dataObj as $item) {
            if (preg_match('/;/', $item->getPalavras())) {
                $aux = explode(';', $item->getPalavras());
                foreach ($aux as $auxItem) {
                    $tratados[] = trim(str_replace(' ', $replace, $auxItem));
                }
                continue;
            }
            $tratados[] = trim(str_replace(' ', $replace, $item->getPalavras()));
        }
        return $tratados;
    }

    public function buscarProcesso($nroProcesso)
    {
        $service = new Processo();
        $service->setIdTribunal($this->getId());
        $service->setProcesso($nroProcesso);
        $foundProcesso = $service->filtrarObjeto();
        if (count($foundProcesso)) return $foundProcesso[0];
        return $service;
    }

    public function registerProcessos($linksProcessos, $descartarProcessadosHoje = true)
    {
        $tratados = [];
        foreach ($linksProcessos as $nroProcesso => $link) {
            $service = new Processo();
            $service->setProcesso($nroProcesso);
            $service->setIdTribunal($this->getId());
            $found = $service->filtrarObjeto();
            if (count($found)) {
                $service = $found->current();
                if ($descartarProcessadosHoje) {
                    if ($service->getProcessado() == '1') continue;
//                    $data = explode(' ', $service->getDtUpdated());
//                    if ($service->getProcessado() == '1' && $data[0] == date('Y-m-d')) continue;
                }
            }
            $service->setUrl($link);
            $service->setProcessado(0);
            $service->salvar(true);

            $tratados[] = $service;
        }
        return $tratados;
    }

    public function checkProcessosSemProcessar()
    {
        $service = new Processo();
        $service->setIdTribunal($this->getId());
        $service->setProcessado(0);
        return (count($service->filtrarObjeto())) ? true : false;
    }

    public function getTodosProcessos($processado = null)
    {
        $processo = new Processo();
        $processo->setIdTribunal($this->getId());
        if ($processado !== null) $processo->setProcessado($processado);
        return $processo->filtrarObjeto();
    }

    public function getTotais()
    {
        $sql = "SELECT COUNT(P.ID) AS TOTAL, T.NAME FROM TRIBUNAL_PROCESSO AS P
                INNER JOIN TRIBUNAL_TRIBUNAL AS T ON T.ID = P.ID_TRIBUNAL
                GROUP BY P.ID_TRIBUNAL
                ORDER BY T.NAME";

        $data = Conexao::listarSql($sql);
        $tratados = [];
        foreach ($data as $item) {
            $tratados[$item->NAME] = $item->TOTAL;
        }

        return $tratados;
    }

    public function getTotaisProcessados($processado = 1)
    {
        $sql = "SELECT COUNT(P.ID) AS TOTAL, T.NAME FROM TRIBUNAL_PROCESSO AS P
                INNER JOIN TRIBUNAL_TRIBUNAL AS T ON T.ID = P.ID_TRIBUNAL
                WHERE P.PROCESSADO = '{$processado}'
                GROUP BY P.ID_TRIBUNAL
                ORDER BY T.NAME";

        $data = Conexao::listarSql($sql);
        $tratados = [];
        foreach ($data as $item) {
            $tratados[$item->NAME] = $item->TOTAL;
        }

        return $tratados;
    }

}