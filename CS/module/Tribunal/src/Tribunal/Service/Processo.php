<?php

namespace Tribunal\Service;

use \Tribunal\Entity\Processo as Entity;

class Processo extends Entity
{

    public $classTribunal = '\Tribunal\Service\Tribunal';
    public $optionsAlteracao = ["0" => "Não", "1" => "Sim"];
    public $optionsProcessado = ["0" => "Não", "1" => "Sim"];

    public function getObjTribunal()
    {
        $class = $this->classTribunal;
        $service = new $class;
        $service->setId($this->getIdTribunal());
        $service->load();
        return $service;
    }

    public function getLabelAlteracao($label = false)
    {
        $mapa = $this->optionsAlteracao;
        if ($label) {
            if ($this->getAlteracao() == '0') return '<span class="label label-default">'.$mapa[$this->getAlteracao()].'</span>';
            if ($this->getAlteracao() == '1') return '<span class="label label-success">'.$mapa[$this->getAlteracao()].'</span>';
        }
        if (isset($mapa[$this->getAlteracao()])) return $mapa[$this->getAlteracao()];
    }

    public function getLabelProcessado($label = false)
    {
        $mapa = $this->optionsProcessado;
        if ($label) {
            if ($this->getProcessado() == '0') return '<span class="label label-default">'.$mapa[$this->getProcessado()].'</span>';
            if ($this->getProcessado() == '1') return '<span class="label label-success">'.$mapa[$this->getProcessado()].'</span>';
        }
        if (isset($mapa[$this->getProcessado()])) return $mapa[$this->getProcessado()];
    }

}