<?php
/**
 * Created by PhpStorm.
 * User: DELL VOSTRO
 * Date: 17/06/2018
 * Time: 20:32
 */

namespace Tribunal\Service;

use Zend\Loader\StandardAutoloader;
use Zend\Feed\Reader\Reader;
use Zend\Debug\Debug;

class AsyncOperation extends \Thread {

    public $arg;

    public function __construct($arg) {
        $this->arg = $arg;
    }

    public function run() {
        if ($this->arg) {
            $sleep = mt_rand(1, 10);
            printf('%s: %s  -start -sleeps %d' . "\n", date("g:i:sa"), $this->arg, $sleep);
            sleep($sleep);
            printf('%s: %s  -finish' . "\n", date("g:i:sa"), $this->arg);
        }
    }
}
