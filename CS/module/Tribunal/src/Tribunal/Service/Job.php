<?php

namespace Tribunal\Service;

use Estrutura\Service\Conexao;
use Facebook\WebDriver\Interactions\Internal\WebDriverClickAndHoldAction;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverPoint;
use \Tribunal\Entity\Job as Entity;
use Facebook\WebDriver\Exception\NoAlertOpenException;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeOutException;

class Job extends Entity
{

    public function start($uf, $method = false)
    {
        $robo = new Robo();
        $startCycle = new \DateTime();]

//        var_dump($uf);die;

        /**
         * FILTRO NORMAL
         */
        /*$job = new Job();
        $job->setUf($uf);
        $job->setAlterado(0);
        $registros = $job->filtrarObjeto();*/

        /**
         * ATUALIZAR PROCESSOS QUE OS OPERADORES ATUALIZARAM O NRO DE PROCESSO
         * 17/08/2018
         */
        $job = new Job();
        $job->setUf($uf);
        $job->setAlterado(0);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/CS.1708/', $processo->getObs())) {
                $registros[] = $processo;
            }
        }

        $qtdRegistros = count($registros);

        $robo->log("{$qtdRegistros} registros encontrados.");

        $tribunalService = new Tribunal();
        $tribunalService->setUf($uf);
        if ($method) $tribunalService->setCallMethod($method);
        /** @var $tribunais \Tribunal\Service\Tribunal[] */
        $tribunais = $tribunalService->filtrarObjeto();

        foreach ($tribunais as $tribunal) {

            $robo->log('loading ' . $tribunal->getName());

            $tribunal->setDtUltimaColetaComecou(date('Y-m-d H:i:s'));
            $tribunal->setDtUltimaColetaTerminou(NULL);
            $tribunal->salvar(true);

            $robo->logFileName = $tribunal->getCallMethod();
            $robo->cleanLog($robo->logFileName);
            $start = new \DateTime();
            if (!method_exists($this, $tribunal->getCallMethod())) {
                $robo->log("MÉTODO {$tribunal->getCallMethod()} NÃO EXISTE!", $robo->logFileName);
                continue;
            }
            $method = $tribunal->getCallMethod();
            $this->$method($tribunal, $registros);

            $end = new \DateTime();
            $interval = $start->diff($end);
            $robo->log('END => Running time: ' . $interval->format('%a days %h hours %i minutes %s seconds'), $robo->logFileName);

            $tribunal->setDtUltimaColetaTerminou(date('Y-m-d H:i:s'));
            $tribunal->setColetando(0);
            $tribunal->salvar(true);
        }

        $robo->log('Done!');
        $endCycle = new \DateTime();
        $intervalCycle = $startCycle->diff($endCycle);
        $robo->cleanLog('cycle');
        $robo->log("END => Running cycle time in {$qtdRegistros} records: " . $intervalCycle->format('%a days %h hours %i minutes %s seconds'), 'cycle');

        return true;
    }

    public function startDirect($filtroUf, $method = false, $params = [])
    {
        $robo = new Robo();

        /**
         * FILTRO NORMAL
         */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registros = $job->filtrarObjeto();*/

        /**
         * AJUSTE DOS PROBLEMATICOS APONTADOS PELA VALÉRIA
         * 10/08/2018
         */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(1);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (
                empty($processo->getJuizoVara()) ||
                $processo->getJuizoVara() == '' ||
                $processo->getJuizoVara() == null ||
                $processo->getJuizoVara() == '0' ||
                $processo->getJuizoVara() == '(Processo não distribuído);'
            )
            $registros[] = $processo;
        }*/

        /**
         * ATUALIZAR PROCESSOS QUE OS OPERADORES ATUALIZARAM O NRO DE PROCESSO
         * 13/08/2018
         */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/NRO ATUALIZADO /',$processo->getObs())) {
                $registros[] = $processo;
            }
        }*/
        /**
         * ATUALIZAR PROCESSOS QUE OS OPERADORES ATUALIZARAM O NRO DE PROCESSO
         * 17/08/2018
         */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/CS.1708/', $processo->getObs())) {
                $registros[] = $processo;
            }
        }*/

        /**
         * ATUALIZAR PROCESSOS QUE OS OPERADORES ATUALIZARAM O NRO DE PROCESSO
         * 23/08/2018
         */
        $job = new Job();
        $job->setUf($filtroUf);
        $job->setAlterado(0);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/CS.2308/', $processo->getObs())) {
                $registros[] = $processo;
            }
        }

        /**
         * ATUALIZAR PROCESSOS QUE TRF1 COM VALOR ERRADO JUIZO/VARA (TURMA RECURSAL) E OUTROS TRIBUNAIS
         * filtrando processos que possuem valor TR - RELATOR ou CAMARA
         * 14/08/2018
         * @TODO - RODOU!!!
         */
        /** @var $registrosAux \Tribunal\Service\Job[] */
        /*$job = new Job();
        $job->setUf($filtroUf);
        $registrosAux = $job->filtrarObjeto();
        $registros = [];
        foreach ($registrosAux as $processo) {
            if (preg_match('/TR - RELATOR/',$processo->getJuizoVara()) || preg_match('/CAMARA/',$processo->getJuizoVara())) {
                $registros[] = $processo;
            }
        }*/


        $qtdRegistros = count($registros);

        $robo->log("{$qtdRegistros} registros encontrados.");

        $start = new \DateTime();
        if (!method_exists($this, $method)) {
            $robo->log("MÉTODO {$method} NÃO EXISTE!");
            die;
        }
        if (count($params)) {
            $this->$method($registros, $params);
        } else {
            $this->$method($registros);
        }


        $end = new \DateTime();
        $interval = $start->diff($end);
        $robo->log('END => Running time: ' . $interval->format('%a days %h hours %i minutes %s seconds'));
        $robo->log('Done!');

        return true;
    }

    public function tjsp1instanciaesaj($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $driver = $robo->startDriver();
        $driver->get('https://esaj.tjsp.jus.br/cpopg/open.do');

        $comarcaOptions = '<option value="91">ANTIGO Foro Distrital de Brás Cubas</option>
<option value="509">Araçatuba/DEECRIM UR2</option>
<option value="26">Bauru/DEECRIM UR3</option>
<option value="502">Campinas/DEECRIM UR4</option>
<option value="500">DEPRE</option>
<option value="53">Foro Central - Fazenda Pública/Acidentes</option>
<option value="100">Foro Central Cível</option>
<option value="52">Foro Central Criminal - Juri</option>
<option value="50">Foro Central Criminal Barra Funda</option>
<option value="16">Foro Central Juizados Especiais Cíveis</option>
<option value="800">Foro da Comissão Processante Permanente</option>
<option value="14">Foro das Execuções Fiscais Estaduais</option>
<option value="90">Foro das Execuções Fiscais Municipais</option>
<option value="81">Foro de Adamantina</option>
<option value="83">Foro de Aguaí</option>
<option value="35">Foro de Águas de Lindoia</option>
<option value="58">Foro de Agudos</option>
<option value="42">Foro de Altinópolis</option>
<option value="19">Foro de Americana</option>
<option value="40">Foro de Américo Brasiliense</option>
<option value="22">Foro de Amparo</option>
<option value="24">Foro de Andradina</option>
<option value="25">Foro de Angatuba</option>
<option value="28">Foro de Aparecida</option>
<option value="30">Foro de Apiaí</option>
<option value="32">Foro de Araçatuba</option>
<option value="37">Foro de Araraquara</option>
<option value="38">Foro de Araras</option>
<option value="666">Foro de Artur Nogueira</option>
<option value="45">Foro de Arujá</option>
<option value="47">Foro de Assis</option>
<option value="48">Foro de Atibaia</option>
<option value="60">Foro de Auriflama</option>
<option value="73">Foro de Avaré</option>
<option value="59">Foro de Bananal</option>
<option value="62">Foro de Bariri</option>
<option value="63">Foro de Barra Bonita</option>
<option value="66">Foro de Barretos</option>
<option value="68">Foro de Barueri</option>
<option value="69">Foro de Bastos</option>
<option value="70">Foro de Batatais</option>
<option value="71">Foro de Bauru</option>
<option value="72">Foro de Bebedouro</option>
<option value="75">Foro de Bertioga</option>
<option value="76">Foro de Bilac</option>
<option value="77">Foro de Birigui</option>
<option value="82">Foro de Boituva</option>
<option value="67">Foro de Borborema</option>
<option value="79">Foro de Botucatu</option>
<option value="99">Foro de Bragança Paulista</option>
<option value="94">Foro de Brodowski</option>
<option value="95">Foro de Brotas</option>
<option value="691">Foro de Buri</option>
<option value="97">Foro de Buritama</option>
<option value="80">Foro de Cabreúva</option>
<option value="101">Foro de Caçapava</option>
<option value="102">Foro de Cachoeira Paulista</option>
<option value="103">Foro de Caconde</option>
<option value="104">Foro de Cafelândia</option>
<option value="106">Foro de Caieiras</option>
<option value="108">Foro de Cajamar</option>
<option value="111">Foro de Cajuru</option>
<option value="114">Foro de Campinas</option>
<option value="115">Foro de Campo Limpo Paulista</option>
<option value="116">Foro de Campos do Jordão</option>
<option value="118">Foro de Cananéia</option>
<option value="120">Foro de Cândido Mota</option>
<option value="123">Foro de Capão Bonito</option>
<option value="125">Foro de Capivari</option>
<option value="126">Foro de Caraguatatuba</option>
<option value="127">Foro de Carapicuíba</option>
<option value="128">Foro de Cardoso</option>
<option value="129">Foro de Casa Branca</option>
<option value="132">Foro de Catanduva</option>
<option value="136">Foro de Cerqueira César</option>
<option value="137">Foro de Cerquilho</option>
<option value="140">Foro de Chavantes</option>
<option value="142">Foro de Colina</option>
<option value="144">Foro de Conchal</option>
<option value="145">Foro de Conchas</option>
<option value="146">Foro de Cordeirópolis</option>
<option value="150">Foro de Cosmópolis</option>
<option value="152">Foro de Cotia</option>
<option value="153">Foro de Cravinhos</option>
<option value="156">Foro de Cruzeiro</option>
<option value="157">Foro de Cubatão</option>
<option value="159">Foro de Cunha</option>
<option value="160">Foro de Descalvado</option>
<option value="161">Foro de Diadema</option>
<option value="165">Foro de Dois Córregos</option>
<option value="168">Foro de Dracena</option>
<option value="169">Foro de Duartina</option>
<option value="172">Foro de Eldorado Paulista</option>
<option value="176">Foro de Embu das Artes</option>
<option value="177">Foro de Embu-Guaçu</option>
<option value="180">Foro de Espírito Santo do Pinhal</option>
<option value="185">Foro de Estrela D\'Oeste</option>
<option value="187">Foro de Fartura</option>
<option value="189">Foro de Fernandópolis</option>
<option value="191">Foro de Ferraz de Vasconcelos</option>
<option value="673">Foro de Flórida Paulista</option>
<option value="196">Foro de Franca</option>
<option value="197">Foro de Francisco Morato</option>
<option value="198">Foro de Franco da Rocha</option>
<option value="200">Foro de Gália</option>
<option value="201">Foro de Garça</option>
<option value="204">Foro de General Salgado</option>
<option value="205">Foro de Getulina</option>
<option value="210">Foro de Guaíra</option>
<option value="213">Foro de Guará</option>
<option value="218">Foro de Guararapes</option>
<option value="219">Foro de Guararema</option>
<option value="220">Foro de Guaratinguetá</option>
<option value="222">Foro de Guariba</option>
<option value="223">Foro de Guarujá</option>
<option value="224">Foro de Guarulhos</option>
<option value="229">Foro de Hortolândia</option>
<option value="27">Foro de Iacanga</option>
<option value="233">Foro de Ibaté</option>
<option value="236">Foro de Ibitinga</option>
<option value="238">Foro de Ibiúna</option>
<option value="240">Foro de Iepê</option>
<option value="242">Foro de Igarapava</option>
<option value="244">Foro de Iguape</option>
<option value="246">Foro de Ilha Solteira</option>
<option value="247">Foro de Ilhabela</option>
<option value="248">Foro de Indaiatuba</option>
<option value="252">Foro de Ipauçu</option>
<option value="257">Foro de Ipuã</option>
<option value="262">Foro de Itaberá</option>
<option value="263">Foro de Itaí</option>
<option value="264">Foro de Itajobi</option>
<option value="266">Foro de Itanhaém</option>
<option value="268">Foro de Itapecerica da Serra</option>
<option value="269">Foro de Itapetininga</option>
<option value="270">Foro de Itapeva</option>
<option value="271">Foro de Itapevi</option>
<option value="272">Foro de Itapira</option>
<option value="274">Foro de Itápolis</option>
<option value="275">Foro de Itaporanga</option>
<option value="278">Foro de Itaquaquecetuba</option>
<option value="279">Foro de Itararé</option>
<option value="280">Foro de Itariri</option>
<option value="281">Foro de Itatiba</option>
<option value="282">Foro de Itatinga</option>
<option value="283">Foro de Itirapina</option>
<option value="286">Foro de Itu</option>
<option value="514">Foro de Itupeva</option>
<option value="288">Foro de Ituverava</option>
<option value="291">Foro de Jaboticabal</option>
<option value="292">Foro de Jacareí</option>
<option value="294">Foro de Jacupiranga</option>
<option value="296">Foro de Jaguariúna</option>
<option value="297">Foro de Jales</option>
<option value="299">Foro de Jandira</option>
<option value="300">Foro de Jardinópolis</option>
<option value="301">Foro de Jarinu</option>
<option value="302">Foro de Jaú</option>
<option value="306">Foro de José Bonifácio</option>
<option value="309">Foro de Jundiaí</option>
<option value="311">Foro de Junqueirópolis</option>
<option value="312">Foro de Juquiá</option>
<option value="315">Foro de Laranjal Paulista</option>
<option value="318">Foro de Leme</option>
<option value="319">Foro de Lençóis Paulista</option>
<option value="320">Foro de Limeira</option>
<option value="322">Foro de Lins</option>
<option value="323">Foro de Lorena</option>
<option value="681">Foro de Louveira</option>
<option value="326">Foro de Lucélia</option>
<option value="333">Foro de Macatuba</option>
<option value="334">Foro de Macaubal</option>
<option value="337">Foro de Mairinque</option>
<option value="338">Foro de Mairiporã</option>
<option value="341">Foro de Maracaí</option>
<option value="344">Foro de Marília</option>
<option value="346">Foro de Martinópolis</option>
<option value="347">Foro de Matão</option>
<option value="348">Foro de Mauá</option>
<option value="352">Foro de Miguelópolis</option>
<option value="355">Foro de Miracatu</option>
<option value="356">Foro de Mirandópolis</option>
<option value="357">Foro de Mirante do Paranapanema</option>
<option value="358">Foro de Mirassol</option>
<option value="360">Foro de Mococa</option>
<option value="361">Foro de Mogi das Cruzes</option>
<option value="362">Foro de Mogi Guaçu</option>
<option value="363">Foro de Mogi Mirim</option>
<option value="366">Foro de Mongaguá</option>
<option value="368">Foro de Monte Alto</option>
<option value="369">Foro de Monte Aprazível</option>
<option value="370">Foro de Monte Azul Paulista</option>
<option value="372">Foro de Monte Mor</option>
<option value="374">Foro de Morro Agudo</option>
<option value="695">Foro de Nazaré Paulista</option>
<option value="382">Foro de Neves Paulista</option>
<option value="383">Foro de Nhandeara</option>
<option value="390">Foro de Nova Granada</option>
<option value="394">Foro de Nova Odessa</option>
<option value="396">Foro de Novo Horizonte</option>
<option value="397">Foro de Nuporanga</option>
<option value="400">Foro de Olímpia</option>
<option value="404">Foro de Orlândia</option>
<option value="405">Foro de Osasco</option>
<option value="407">Foro de Osvaldo Cruz</option>
<option value="408">Foro de Ourinhos</option>
<option value="696">Foro de Ouroeste</option>
<option value="411">Foro de Pacaembu</option>
<option value="412">Foro de Palestina</option>
<option value="414">Foro de Palmeira D\'Oeste</option>
<option value="415">Foro de Palmital</option>
<option value="416">Foro de Panorama</option>
<option value="417">Foro de Paraguaçu Paulista</option>
<option value="418">Foro de Paraibuna</option>
<option value="420">Foro de Paranapanema</option>
<option value="424">Foro de Pariquera-Açu</option>
<option value="426">Foro de Patrocínio Paulista</option>
<option value="428">Foro de Paulínia</option>
<option value="430">Foro de Paulo de Faria</option>
<option value="431">Foro de Pederneiras</option>
<option value="434">Foro de Pedregulho</option>
<option value="435">Foro de Pedreira</option>
<option value="438">Foro de Penápolis</option>
<option value="439">Foro de Pereira Barreto</option>
<option value="441">Foro de Peruíbe</option>
<option value="443">Foro de Piedade</option>
<option value="444">Foro de Pilar do Sul</option>
<option value="445">Foro de Pindamonhangaba</option>
<option value="447">Foro de Pinhalzinho</option>
<option value="449">Foro de Piquete</option>
<option value="450">Foro de Piracaia</option>
<option value="451">Foro de Piracicaba</option>
<option value="452">Foro de Piraju</option>
<option value="453">Foro de Pirajuí</option>
<option value="698">Foro de Pirangi</option>
<option value="456">Foro de Pirapozinho</option>
<option value="457">Foro de Pirassununga</option>
<option value="458">Foro de Piratininga</option>
<option value="459">Foro de Pitangueiras</option>
<option value="462">Foro de Poá</option>
<option value="464">Foro de Pompéia</option>
<option value="466">Foro de Pontal</option>
<option value="470">Foro de Porangaba</option>
<option value="471">Foro de Porto Feliz</option>
<option value="472">Foro de Porto Ferreira</option>
<option value="474">Foro de Potirendaba</option>
<option value="477">Foro de Praia Grande</option>
<option value="480">Foro de Presidente Bernardes</option>
<option value="481">Foro de Presidente Epitácio</option>
<option value="482">Foro de Presidente Prudente</option>
<option value="483">Foro de Presidente Venceslau</option>
<option value="484">Foro de Promissão</option>
<option value="486">Foro de Quatá</option>
<option value="488">Foro de Queluz</option>
<option value="491">Foro de Rancharia</option>
<option value="493">Foro de Regente Feijó</option>
<option value="495">Foro de Registro</option>
<option value="498">Foro de Ribeirão Bonito</option>
<option value="505">Foro de Ribeirão Pires</option>
<option value="506">Foro de Ribeirão Preto</option>
<option value="510">Foro de Rio Claro</option>
<option value="511">Foro de Rio das Pedras</option>
<option value="512">Foro de Rio Grande da Serra</option>
<option value="515">Foro de Rosana</option>
<option value="516">Foro de Roseira</option>
<option value="523">Foro de Salesópolis</option>
<option value="526">Foro de Salto</option>
<option value="699">Foro de Salto de Pirapora</option>
<option value="531">Foro de Santa Adélia</option>
<option value="533">Foro de Santa Bárbara D\'Oeste</option>
<option value="534">Foro de Santa Branca</option>
<option value="538">Foro de Santa Cruz das Palmeiras</option>
<option value="539">Foro de Santa Cruz do Rio Pardo</option>
<option value="541">Foro de Santa Fé do Sul</option>
<option value="543">Foro de Santa Isabel</option>
<option value="547">Foro de Santa Rita do Passa Quatro</option>
<option value="549">Foro de Santa Rosa de Viterbo</option>
<option value="529">Foro de Santana de Parnaíba</option>
<option value="553">Foro de Santo Anastácio</option>
<option value="554">Foro de Santo André</option>
<option value="562">Foro de Santos</option>
<option value="563">Foro de São Bento do Sapucaí</option>
<option value="564">Foro de São Bernardo do Campo</option>
<option value="565">Foro de São Caetano do Sul</option>
<option value="566">Foro de São Carlos</option>
<option value="568">Foro de São João da Boa Vista</option>
<option value="572">Foro de São Joaquim da Barra</option>
<option value="575">Foro de São José do Rio Pardo</option>
<option value="576">Foro de São José do Rio Preto</option>
<option value="577">Foro de São José dos Campos</option>
<option value="579">Foro de São Luiz do Paraitinga</option>
<option value="581">Foro de São Manuel</option>
<option value="582">Foro de São Miguel Arcanjo</option>
<option value="584">Foro de São Pedro</option>
<option value="586">Foro de São Roque</option>
<option value="587">Foro de São Sebastião</option>
<option value="588">Foro de São Sebastião da Grama</option>
<option value="589">Foro de São Simão</option>
<option value="590">Foro de São Vicente</option>
<option value="595">Foro de Serra Negra</option>
<option value="596">Foro de Serrana</option>
<option value="597">Foro de Sertãozinho</option>
<option value="601">Foro de Socorro</option>
<option value="602">Foro de Sorocaba</option>
<option value="604">Foro de Sumaré</option>
<option value="606">Foro de Suzano</option>
<option value="607">Foro de Tabapuã</option>
<option value="609">Foro de Taboão da Serra</option>
<option value="614">Foro de Tambaú</option>
<option value="615">Foro de Tanabi</option>
<option value="619">Foro de Taquaritinga</option>
<option value="620">Foro de Taquarituba</option>
<option value="624">Foro de Tatuí</option>
<option value="625">Foro de Taubaté</option>
<option value="627">Foro de Teodoro Sampaio</option>
<option value="629">Foro de Tietê</option>
<option value="634">Foro de Tremembé</option>
<option value="637">Foro de Tupã</option>
<option value="638">Foro de Tupi Paulista</option>
<option value="642">Foro de Ubatuba</option>
<option value="646">Foro de Urânia</option>
<option value="648">Foro de Urupês</option>
<option value="650">Foro de Valinhos</option>
<option value="651">Foro de Valparaíso</option>
<option value="653">Foro de Vargem Grande do Sul</option>
<option value="654">Foro de Vargem Grande Paulista</option>
<option value="655">Foro de Várzea Paulista</option>
<option value="659">Foro de Vinhedo</option>
<option value="660">Foro de Viradouro</option>
<option value="663">Foro de Votorantim</option>
<option value="664">Foro de Votuporanga</option>
<option value="12">Foro Distrital de Parelheiros</option>
<option value="15">Foro Especial da Infância e Juventude</option>
<option value="635">Foro Plantão - 00ª CJ - Capital</option>
<option value="536">Foro Plantão - 01ª CJ - Santos</option>
<option value="537">Foro Plantão - 02ª CJ - São Be. Campo</option>
<option value="540">Foro Plantão - 03ª CJ - Santo André</option>
<option value="542">Foro Plantão - 04ª CJ - Osasco</option>
<option value="544">Foro Plantão - 05ª CJ - Jundiaí</option>
<option value="545">Foro Plantão - 06ª CJ - Brag. Paulista</option>
<option value="546">Foro Plantão - 07ª CJ - Mogi Mirim</option>
<option value="548">Foro Plantão - 08ª CJ - Campinas</option>
<option value="550">Foro Plantão - 09ª CJ - Rio Claro</option>
<option value="551">Foro Plantão - 10ª CJ - Limeira</option>
<option value="552">Foro Plantão - 11ª CJ - Pirassununga</option>
<option value="555">Foro Plantão - 12ª CJ - São Carlos</option>
<option value="556">Foro Plantão - 13ª CJ - Araraquara</option>
<option value="557">Foro Plantão - 14ª CJ - Barretos</option>
<option value="558">Foro Plantão - 15ª CJ - Catanduva</option>
<option value="559">Foro Plantão - 16ª CJ - S. J. Rio Preto</option>
<option value="560">Foro Plantão - 17ª CJ - Votuporanga</option>
<option value="561">Foro Plantão - 18ª CJ - Fernandópolis</option>
<option value="567">Foro Plantão - 19ª CJ - Sorocaba</option>
<option value="569">Foro Plantão - 20ª CJ - Itu</option>
<option value="570">Foro Plantão - 21ª CJ - Registro</option>
<option value="571">Foro Plantão - 22ª CJ - Itapetininga</option>
<option value="573">Foro Plantão - 23ª CJ - Botucatu</option>
<option value="574">Foro Plantão - 24ª CJ - Avaré</option>
<option value="578">Foro Plantão - 25ª CJ - Ourinhos</option>
<option value="580">Foro Plantão - 26ª CJ - Assis</option>
<option value="583">Foro Plantão - 27ª CJ - Pre. Prudente</option>
<option value="585">Foro Plantão - 28ª CJ - Pre. Venceslau</option>
<option value="591">Foro Plantão - 29ª CJ - Dracena</option>
<option value="592">Foro Plantão - 30ª CJ - Tupã</option>
<option value="593">Foro Plantão - 31ª CJ - Marília</option>
<option value="594">Foro Plantão - 32ª CJ - Bauru</option>
<option value="598">Foro Plantão - 33ª CJ - Jaú</option>
<option value="599">Foro Plantão - 34ª CJ - Piracicaba</option>
<option value="600">Foro Plantão - 35ª CJ - Lins</option>
<option value="603">Foro Plantão - 36ª CJ - Araçatuba</option>
<option value="605">Foro Plantão - 37ª CJ - Andradina</option>
<option value="608">Foro Plantão - 38ª CJ - Franca</option>
<option value="610">Foro Plantão - 39ª CJ - Batatais</option>
<option value="611">Foro Plantão - 40ª CJ - Ituverava</option>
<option value="530">Foro Plantão - 41ª CJ - Ribeirão Preto</option>
<option value="612">Foro Plantão - 42ª CJ - Jaboticabal</option>
<option value="613">Foro Plantão - 43ª CJ - Casa Branca</option>
<option value="535">Foro Plantão - 44ª CJ - Guarulhos</option>
<option value="616">Foro Plantão - 45ª CJ - Mogi das Cruzes</option>
<option value="617">Foro Plantão - 46ª CJ - S. J. dos Campos</option>
<option value="618">Foro Plantão - 47ª CJ - Taubaté</option>
<option value="621">Foro Plantão - 48ª CJ - Guaratinguetá</option>
<option value="622">Foro Plantão - 49ª CJ - Itapeva</option>
<option value="623">Foro Plantão - 50ª CJ - S. J. Boa Vista</option>
<option value="626">Foro Plantão - 51ª CJ - Caraguatatuba</option>
<option value="628">Foro Plantão - 52ª CJ - Itapec. da Serra</option>
<option value="630">Foro Plantão - 53ª CJ - Americana</option>
<option value="631">Foro Plantão - 54ª CJ - Amparo</option>
<option value="632">Foro Plantão - 55ª CJ - Jales</option>
<option value="633">Foro Plantão - 56ª CJ - Itanhaém</option>
<option value="84">Foro Regional de Vila Mimosa</option>
<option value="1">Foro Regional I - Santana</option>
<option value="2">Foro Regional II - Santo Amaro</option>
<option value="3">Foro Regional III - Jabaquara</option>
<option value="4">Foro Regional IV - Lapa</option>
<option value="9">Foro Regional IX - Vila Prudente</option>
<option value="5">Foro Regional V - São Miguel Paulista</option>
<option value="6">Foro Regional VI - Penha de França</option>
<option value="7">Foro Regional VII - Itaquera</option>
<option value="8">Foro Regional VIII - Tatuapé</option>
<option value="10">Foro Regional X - Ipiranga</option>
<option value="11">Foro Regional XI - Pinheiros</option>
<option value="20">Foro Regional XII - Nossa Senhora do Ó</option>
<option value="704">Foro Regional XV - Butantã</option>
<option value="996">Presidente Prudente/DEECRIM UR5</option>
<option value="496">Ribeirão Preto/DEECRIM UR6</option>
<option value="158">Santos/DEECRIM UR7</option>
<option value="154">São José do Rio Preto/DEECRIM UR8</option>
<option value="520">São José dos Campos/DEECRIM UR9</option>
<option value="41">São Paulo/DEECRIM UR1</option>
<option value="21">Setor de Cartas Precatórias Cíveis - Cap</option>
<option value="521">Sorocaba/DEECRIM UR10</option>';

        $doc = new \DOMDocument();
        $doc->loadHTML($comarcaOptions);
        $comarcas = [];
        foreach ($doc->getElementsByTagName('option') as $comarca) {
            $comarcas[] = str_pad($comarca->getAttribute('value'), 4, 0, STR_PAD_LEFT);
        }

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            //if (!preg_match('/.8.26./', $nroProcesso)) continue; // 1° TENTATIVA : PEGANDO APENAS NRO FORMATO CNJ
            //if (!in_array(substr($nroProcesso, -4, 4), $comarcas)) continue; //1° TENTATIVA

//            if (strlen($nroProcesso) == 25) continue; // 2° TENTATIVA : PEGANDO APENAS NRO FORMATO ANTIGO (NÃO CNJ)

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        foreach ($processosTratados as $processo) {

//            $nroProcesso = $processo->getNumProcJust();
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->navigate()->to('https://esaj.tjsp.jus.br/cpopg/open.do');

            $processoUnificado = false;
//            $processoUnificado = true;
//            if (!preg_match('/.8.26./', $nroProcesso)) {
//                $processoUnificado = false;
//            }

            if ($processoUnificado) {
                $nroProcessoArr = explode('.8.26.', $nroProcesso);
                if (count($nroProcessoArr) < 2) continue;
                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);
            } else {

                //ELIMINANDO DIGITO VERIFICADOR
//                if (substr($nroProcesso, -2, 1) == '-') {
//                    $robo->log($nroProcesso);
//                    $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
//                    $nroProcesso = rtrim($nroProcesso, '-');
//                    $robo->log($nroProcesso);
//                    $nroProcesso = $robo->onlyDigits($nroProcesso);
//                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$driver->getCurrentURL()}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$driver->getCurrentURL()}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace(';', '', $varaAux));
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        return true;
    }

    public function tjsp2instanciaesaj($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $driver = $robo->startDriver();
        $driver->get('https://esaj.tjsp.jus.br/cposg/open.do');

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            //if (!preg_match('/.8.26./', $nroProcesso)) continue; // 1° TENTATIVA : PEGANDO APENAS NRO FORMATO CNJ
            //if (!in_array(substr($nroProcesso, -4, 4), $comarcas)) continue; //1° TENTATIVA


//            if (strlen($nroProcesso) == 25) continue; // 2° TENTATIVA : PEGANDO APENAS NRO FORMATO ANTIGO (NÃO CNJ)

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            $nroProcesso = $processo->getNumProcJust();
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->navigate()->to('https://esaj.tjsp.jus.br/cposg/open.do');

            $processoUnificado = false;
//            $processoUnificado = true;
//            if (!preg_match('/.8.26./', $nroProcesso)) {
//                $processoUnificado = false;
//            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="id_Seção"]'))->sendKeys('Todas as seções');

            if ($processoUnificado) {
                $nroProcessoArr = explode('.8.26.', $nroProcesso);
                if (count($nroProcessoArr) < 2) continue;
                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);
            } else {

                //ELIMINANDO DIGITO VERIFICADOR
//                if (substr($nroProcesso, -2, 1) == '-') {
//                    $robo->log($nroProcesso);
//                    $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
//                    $robo->log($nroProcesso);
//                    $nroProcesso = $robo->onlyDigits($nroProcesso);
//                }

                $nroProcesso = $robo->onlyDigits($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            try {
                $vara = '';
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

//                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
//                    //salvar campo
//                    $vara = rtrim($dadosProcesso['Distribuição']);
//                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
//                }
                if (!isset($dadosProcesso['Origem'])) {
                    $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                    $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                }
                if (isset($dadosProcesso['Origem']) && $dadosProcesso['Origem'] != '') {
                    $arr = explode(' / ',$dadosProcesso['Origem']);
                    $vara = $robo->cleanString(end($arr),['; ',';']);
                    $vara .= ' - ' . $arr[1];
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo,$vara,false,$driver->getCurrentURL());
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        return true;
    }

    public function tjsprecursalesaj($service, $processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $driver = $robo->startDriver();
        $driver->get($service->getUrlProcesso());

        $comarcaOptions = '<option value="973">Colégio Recursal - Americana</option>
<option value="974">Colégio Recursal - Amparo</option>
<option value="957">Colégio Recursal - Andradina</option>
<option value="956">Colégio Recursal - Araçatuba</option>
<option value="933">Colégio Recursal - Araraquara</option>
<option value="946">Colégio Recursal - Assis</option>
<option value="944">Colégio Recursal - Avaré</option>
<option value="934">Colégio Recursal - Barretos</option>
<option value="959">Colégio Recursal - Batatais</option>
<option value="952">Colégio Recursal - Bauru</option>
<option value="943">Colégio Recursal - Botucatu</option>
<option value="926">Colégio Recursal - Bragança Paulista</option>
<option value="928">Colégio Recursal - Campinas</option>
<option value="971">Colégio Recursal - Caraguatatuba</option>
<option value="963">Colégio Recursal - Casa Branca</option>
<option value="935">Colégio Recursal - Catanduva</option>
<option value="949">Colégio Recursal - Dracena</option>
<option value="938">Colégio Recursal - Fernandópolis</option>
<option value="958">Colégio Recursal - Franca</option>
<option value="977">Colégio Recursal - Guaratinguetá</option>
<option value="964">Colégio Recursal - Guarulhos</option>
<option value="976">Colégio Recursal - Itanhaém</option>
<option value="972">Colégio Recursal - Itapecerica da Serra</option>
<option value="942">Colégio Recursal - Itapetininga</option>
<option value="969">Colégio Recursal - Itapeva</option>
<option value="940">Colégio Recursal - Itu</option>
<option value="960">Colégio Recursal - Ituverava</option>
<option value="962">Colégio Recursal - Jaboticabal</option>
<option value="975">Colégio Recursal - Jales</option>
<option value="953">Colégio Recursal - Jaú</option>
<option value="925">Colégio Recursal - Jundiaí</option>
<option value="986">Colégio Recursal - Lapa</option>
<option value="930">Colégio Recursal - Limeira</option>
<option value="955">Colégio Recursal - Lins</option>
<option value="951">Colégio Recursal - Marília</option>
<option value="965">Colégio Recursal - Mogi das Cruzes</option>
<option value="927">Colégio Recursal - Mogi Mirim</option>
<option value="924">Colégio Recursal - Osasco</option>
<option value="945">Colégio Recursal - Ourinhos</option>
<option value="985">Colégio Recursal - Penha de França</option>
<option value="954">Colégio Recursal - Piracicaba</option>
<option value="931">Colégio Recursal - Pirassununga</option>
<option value="947">Colégio Recursal - Presidente Prudente</option>
<option value="948">Colégio Recursal - Presidente Venceslau</option>
<option value="941">Colégio Recursal - Registro</option>
<option value="961">Colégio Recursal - Ribeirão Preto</option>
<option value="929">Colégio Recursal - Rio Claro</option>
<option value="988">Colégio Recursal - Santana</option>
<option value="987">Colégio Recursal - Santo Amaro</option>
<option value="923">Colégio Recursal - Santo André</option>
<option value="921">Colégio Recursal - Santos</option>
<option value="922">Colégio Recursal - São Bernardo do Campo</option>
<option value="932">Colégio Recursal - São Carlos</option>
<option value="970">Colégio Recursal - São João da Boa Vista</option>
<option value="936">Colégio Recursal - São José do Rio Preto</option>
<option value="966">Colégio Recursal - São José dos Campos</option>
<option value="939">Colégio Recursal - Sorocaba</option>
<option value="967">Colégio Recursal - Taubaté</option>
<option value="950">Colégio Recursal - Tupã</option>
<option value="937">Colégio Recursal - Votuporanga</option>
<option value="989">Colégio Recursal Central da Capital</option>
<option value="981">Corregedoria Geral da Justiça</option>
<option value="968">Turma Uniformização - Juizados Especiais</option>';

        $doc = new \DOMDocument();
        $doc->loadHTML($comarcaOptions);
        $comarcas = [];
        foreach ($doc->getElementsByTagName('option') as $comarca) {
            $comarcas[] = str_pad($comarca->getAttribute('value'), 4, 0, STR_PAD_LEFT);
        }

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            //if (!preg_match('/.8.26./', $nroProcesso)) continue; // 1° TENTATIVA : PEGANDO APENAS NRO FORMATO CNJ
            //if (!in_array(substr($nroProcesso, -4, 4), $comarcas)) continue; //1° TENTATIVA


            if (strlen($nroProcesso) == 25) continue; // 2° TENTATIVA : PEGANDO APENAS NRO FORMATO ANTIGO (NÃO CNJ)

            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        foreach ($processosTratados as $processo) {

            $nroProcesso = $processo->getNumProcJust();
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->navigate()->to($service->getUrlProcesso());

            $processoUnificado = false;
//            if (!preg_match('/.8.26./', $nroProcesso)) {
//                $processoUnificado = false;
//            }

            if ($processoUnificado) {
                $nroProcessoArr = explode('.8.26.', $nroProcesso);
                if (count($nroProcessoArr) < 2) continue;
                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);
            } else {
                $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();

                //ELIMINANDO DIGITO VERIFICADOR
                if (substr($nroProcesso, -2, 1) == '-') {
                    $robo->log($nroProcesso);
                    $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                    $robo->log($nroProcesso);
                    $nroProcesso = $robo->onlyDigits($nroProcesso);
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1]) && $distribuicao[1] != '') ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace(';', '', $varaAux));
                    //salvar campo
                    $robo->log("{$processo->getNumProcJust()} => {$vara}");
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());

                    $encontrados++;
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    public function tjspjf($service, $processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $driver = $robo->startDriver();
        $driver->get($service->getUrlProcesso());

        $total = count($processos);
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if (!$nroProcesso) continue;

            $driver->navigate()->to($service->getUrlProcesso());

            $driver->findElement(WebDriverBy::xpath('//*[@id="num_processo"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('/html/body/form/fieldset[1]/table/tbody/tr[3]/td[2]/p/input[4]'))->click();

            try {
                $driver->wait(1)->until(
                    WebDriverExpectedCondition::alertIsPresent());
                $driver->switchTo()->alert()->accept();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['SECRETARIA']) && $dadosProcesso['SECRETARIA'] != '') {
                    //salvar campo
                    $vara = rtrim($robo->cleanString($dadosProcesso['SECRETARIA'], ['; ', ';']));
                    $robo->log("{$nroProcesso} => {$vara}");
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                    $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");

        $driver->quit();
        return true;
    }

    public function tjsp1instanciatrf3pje($service, $processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();


        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            //            if ($nroProcesso != '5000871-64.2017.4.03.6110') continue;
            if (!$nroProcesso) continue;
            if (!preg_match('/.4.03./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) != 5) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($service->getUrlProcesso());

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE 429 => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $robo->log($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE 440 => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to('https://pje1g.trf3.jus.br' . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    public function tjsp2instanciatrf3pje($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();

        $link = 'https://pje2g.trf3.jus.br/pje/ConsultaPublica/listView.seam';

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            if (!preg_match('/.4.03./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) != 5) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($link);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE 429 => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $msg = $driver->findElement(WebDriverBy::className('rich-messages'))->getText();
                $robo->log($msg);
                if (!preg_match('/A verificação de captcha não está correta/', $msg)) continue;
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE 440 => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to('https://pje2g.trf3.jus.br' . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }


    public function tjsc1instanciaesaj($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("SANTA CATARINA 1° INST NCIA ESAJ");
        $driver = $robo->startDriver();
        $link = 'https://esaj.tjsc.jus.br/cpopg/open.do';
        $driver->get($link);

        $comarcaElem = $driver->findElements(WebDriverBy::xpath('//*[@id="id_Comarca"]/option'));
        $comarca = [];
        foreach ($comarcaElem as $item) {
            $comV = $item->getAttribute('value');
            if ($comV < 0) continue;
            $comarcas[] = str_pad($comV, 4, 0, STR_PAD_LEFT);
        }

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA E SEGUNDA LEVA
            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.24./', $nroProcesso)) continue;//PRIMEIRA E SEGUNDA LEVA
//            if (!in_array(substr($nroProcesso, -4, 4), $comarcas)) continue;//PRIMEIRA E SEGUNDA LEVA

            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';

        /**
         * PRIMEIRA TENTATIVA VARRENDO A LISTA DE PROCESSOS
         * APÓS A PESQUISA PELO NOME DA PARTE
         */
        /*
        $words = ['CAIXA SEGURO', 'CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA'];
        //$words = ['CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA', 'CAIXA CONSÓRCIO', 'CAIXA SEGURADORA'];

        $driver->navigate()->to($link);
        foreach ($words as $word) {
            $robo->log($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[12]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
            } catch (\Exception $e) {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msgError = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                $robo->log($msgError);

                if (preg_match('/Marque a opção Não sou um robô/', $msgError)) {
                    try {
                        $driver->navigate()->to($link);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                        $driver->navigate()->to($link);
                    }
                    $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                    $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[12]/td[2]/div/div/div/div/iframe');
                    if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();
                }
            }

            //VARRENDO OS PROCESSOS ENCONTRADOS
            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                //PRIMEIRA LEVA
                $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                foreach ($processosBloco as $bloco) {
                    $vara = '';
                    $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                    if ($nro == '') continue;
                    if (!isset($processosTratados[$nro])) continue;

                    $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                    $index++;

                    $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                    $linha = end($linhas);
                    $linhaArr = explode(' - ', $linha->getText());
                    $infoSelected = end($linhaArr);
                    if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                        $countAux = count($linhaArr);
                        $infoSelected = $linhaArr[$countAux - 2];
                    }
                    $vara = $robo->cleanString($infoSelected);

                    if ($vara != '') {
                        $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                    }
                }
                //SEGUNDA LEVA SE EXISTIR
                while ($next = $robo->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {

                    try {
                        $driver->navigate()->to($next);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                    }

                    $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                    foreach ($processosBloco as $bloco) {
                        $vara = '';
                        $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                        if ($nro == '') continue;
                        if (!isset($processosTratados[$nro])) continue;

                        $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                        $index++;

                        $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                        $linha = end($linhas);
                        $linhaArr = explode(' - ', $linha->getText());
                        $infoSelected = end($linhaArr);
                        if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                            $countAux = count($linhaArr);
                            $infoSelected = $linhaArr[$countAux - 2];
                        }
                        $vara = $robo->cleanString($infoSelected);

                        if ($vara != '') {
                            $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                        }
                    }
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }

        }

        $driver->quit();
        $robo->log("PRIMEIRA TENTATIVA - DONE!");
        die;
        */


        /**
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         * TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            //$nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $nroProcesso = $processo->getNumProcJust();
            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
//            if (strlen($nroProcesso) == 20) continue;
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            $nroProcessoArr = explode('.8.24.', $nroProcesso);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $nroProcesso = rtrim($nroProcesso, '-');
                $robo->log($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[12]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText();
                $robo->log($msg);
                if (preg_match('/Não existem informações disponíveis para os parâmetros informados./', $msg)) continue;
                $driver->navigate()->back();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                sleep(2);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace([';', ' - Unidade 100% Digital'], '', $varaAux));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjsc2instanciaesaj($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE

        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("SANTA CATARINA 2° INST NCIA ESAJ");
        $driver = $robo->startDriver();
        $link = 'https://esaj.tjsc.jus.br/cposgtj/open.do';
        $driver->get($link);

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $nroProcesso = $processo->getNumProcJust();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.24./', $nroProcesso)) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';

        /**
         * PRIMEIRA TENTATIVA VARRENDO A LISTA DE PROCESSOS
         * APÓS A PESQUISA PELO NOME DA PARTE
         */
        /*
        $words = ['CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA'];
        //$words = ['CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA', 'CAIXA CONSÓRCIO', 'CAIXA SEGURADORA'];

        $driver->navigate()->to($link);
        foreach ($words as $word) {
            $robo->log($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
            } catch (\Exception $e) {
                try {
                    $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                    $msgError = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                    $robo->log($msgError);

                    if (preg_match('/Marque a opção Não sou um robô/', $msgError)) {
                        try {
                            $driver->navigate()->to($link);
                        } catch (\Exception $e) {
                            $driver->navigate()->refresh();
                            $driver->navigate()->to($link);
                        }
                        $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
                        if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                    }
                } catch (\Exception $e) {
                    $robo->log("PROCESSOS NÃO ENCONTRADOS");
                    continue;
                }
            }

            //VARRENDO OS PROCESSOS ENCONTRADOS
            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                //PRIMEIRA LEVA
                $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                foreach ($processosBloco as $bloco) {
                    $vara = '';
                    $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                    if ($nro == '') continue;
                    if (!isset($processosTratados[$nro])) continue;

                    $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                    $index++;

                    $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                    $linha = end($linhas);
                    $linhaArr = explode(' - ', $linha->getText());
                    $infoSelected = end($linhaArr);
                    if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                        $countAux = count($linhaArr);
                        $infoSelected = $linhaArr[$countAux - 2];
                    }
                    $vara = $robo->cleanString($infoSelected);

                    if ($vara != '') {
                        $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                    }
                }
                //SEGUNDA LEVA SE EXISTIR
                while ($next = $robo->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {

                    try {
                        $driver->navigate()->to($next);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                    }

                    $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                    foreach ($processosBloco as $bloco) {
                        $vara = '';
                        $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                        if ($nro == '') continue;
                        if (!isset($processosTratados[$nro])) continue;

                        $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                        $index++;

                        $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                        $linha = end($linhas);
                        $linhaArr = explode(' - ', $linha->getText());
                        $infoSelected = end($linhaArr);
                        if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                            $countAux = count($linhaArr);
                            $infoSelected = $linhaArr[$countAux - 2];
                        }
                        $vara = $robo->cleanString($infoSelected);

                        if ($vara != '') {
                            $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                        }
                    }
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }

        }

        $driver->quit();
        $robo->log("PRIMEIRA TENTATIVA - DONE!");
        die;
        */

        /**
         * AJUSTAR PARA RECEBER OS PROCESSOS DE 2° INST NCIA ESAJ
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
//            $nroProcesso = $processo->getNumProcJust();

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

//            if (strlen($nroProcesso) == 20) continue;
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            $nroProcessoArr = explode('.8.24.', $nroProcesso);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Número do Processo');//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            /*if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $nroProcesso = rtrim($nroProcesso, '-');
                $robo->log($nroProcesso);
            }*/

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO


            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                $robo->log($msg);
                if (preg_match('/Não foi possível executar esta operação. Tente novamente mais tarde/', $msg)) {
                    $driver->navigate()->back();
                    try {
                        $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe')));
                        $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys('CAIXA SEGURADORA');
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
                        $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                    } catch (\Exception $e) {
                    }
                }
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                /*if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $vara = $robo->cleanString($dadosProcesso['Distribuição']);
                    if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }*/
                if (isset($dadosProcesso['Origem']) && $dadosProcesso['Origem'] != '') {
                    $arr = explode(' / ',$dadosProcesso['Origem']);
                    $vara = $robo->cleanString(end($arr),['; ',';']);
                    $vara .= ' - ' . $arr[1];
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo,$vara,false,$driver->getCurrentURL());
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjscturmarecursalesaj($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("SANTA CATARINA - TURMAS DE RECURSOS E DE UNIFORMIZAÇÃO");
        $driver = $robo->startDriver();
        $link = 'https://esaj.tjsc.jus.br/cposg5/open.do';
        $driver->get($link);

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $nroProcesso = $processo->getNumProcJust();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.24./', $nroProcesso)) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';

        /**
         * PRIMEIRA TENTATIVA VARRENDO A LISTA DE PROCESSOS
         * APÓS A PESQUISA PELO NOME DA PARTE
         */
        /*
        $words = ['CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA'];
        //$words = ['CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA', 'CAIXA CONSÓRCIO', 'CAIXA SEGURADORA'];

        $driver->navigate()->to($link);
        foreach ($words as $word) {
            $robo->log($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
            } catch (\Exception $e) {
                try {
                    $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                    $msgError = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                    $robo->log($msgError);

                    if (preg_match('/Marque a opção Não sou um robô/', $msgError)) {
                        try {
                            $driver->navigate()->to($link);
                        } catch (\Exception $e) {
                            $driver->navigate()->refresh();
                            $driver->navigate()->to($link);
                        }
                        $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
                        if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                    }
                } catch (\Exception $e) {
                    $robo->log("PROCESSOS NÃO ENCONTRADOS");
                    continue;
                }
            }

            //VARRENDO OS PROCESSOS ENCONTRADOS
            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                //PRIMEIRA LEVA
                $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                foreach ($processosBloco as $bloco) {
                    $vara = '';
                    $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                    if ($nro == '') continue;
                    if (!isset($processosTratados[$nro])) continue;

                    $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                    $index++;

                    $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                    $linha = end($linhas);
                    if (preg_match('/Processo não distribuído/', $linha->getText())) continue;
                    $linhaArr = explode(' - ', $linha->getText());
                    $infoSelected = end($linhaArr);
                    if (count($linhaArr) == 3) {
                        $infoSelected = $linhaArr[1];
                    }
                    if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                        $countAux = count($linhaArr);
                        $infoSelected = $linhaArr[$countAux - 2];
                    }
                    $vara = $robo->cleanString($infoSelected);

                    if ($vara != '') {
                        $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                    }
                }
                //SEGUNDA LEVA SE EXISTIR
                while ($next = $robo->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {

                    try {
                        $driver->navigate()->to($next);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                    }

                    $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                    foreach ($processosBloco as $bloco) {
                        $vara = '';
                        $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                        if ($nro == '') continue;
                        if (!isset($processosTratados[$nro])) continue;

                        $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                        $index++;

                        $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                        $linha = end($linhas);
                        if (preg_match('/Processo não distribuído/', $linha->getText())) continue;
                        $linhaArr = explode(' - ', $linha->getText());
                        $infoSelected = end($linhaArr);
                        if (count($linhaArr) == 3) {
                            $infoSelected = $linhaArr[1];
                        }
                        if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                            $countAux = count($linhaArr);
                            $infoSelected = $linhaArr[$countAux - 2];
                        }
                        $vara = $robo->cleanString($infoSelected);

                        if ($vara != '') {
                            $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                        }
                    }
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }

        }

        $driver->quit();
        $robo->log("PRIMEIRA TENTATIVA - DONE!");
        die;
        */


        /**
         * AJUSTAR PARA RECEBER OS PROCESSOS D TURMA RECURSAL ESAJ
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $nroProcesso = $processo->getNumProcJust();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            if (strlen($nroProcesso) == 20) continue;

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            $nroProcessoArr = explode('.8.24.', $nroProcesso);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Número do Processo');//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $nroProcesso = rtrim($nroProcesso, '-');
                $robo->log($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                $robo->log($msg);
                if (preg_match('/Não foi possível executar esta operação. Tente novamente mais tarde/', $msg)) {
                    $driver->navigate()->back();
                    try {
                        $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe')));
                        $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys('CAIXA SEGURADORA');
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
                        $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                    } catch (\Exception $e) {
                    }
                }
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $vara = $robo->cleanString($dadosProcesso['Distribuição'], ['; ', ';']);
                    if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjscjf($processos)
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("SANTA CATARINA - JUSTIÇA FEDERAL");
        $driver = $robo->startDriver();
        $link = 'https://www.jfsc.jus.br/novo_portal/home.php';
        $driver->get($link);

        $linkGet = 'https://www2.trf4.jus.br/trf4/controlador.php?acao=consulta_processual_resultado_pesquisa&txtValor=[NRO_PROCESSO]&selOrigem=SC&chkMostrarBaixados=S&selForma=NU&txtDataFase=01/01/1970&hdnRefId=5e05ca4334f39d339b1880a4cae65d86&txtPalavraGerada=';

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $nroProcesso = $processo->getNumProcJust();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            if (!in_array($nroProcesso, ['5005881-20.2012.4.04.7204', '5007829-12.2017.4.04.7207'])) continue;
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.24./', $nroProcesso)) continue;//PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            if (!in_array(strlen($nroProcesso), [10, 15, 20])) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            try {
                $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                $robo->log($driver->switchTo()->alert()->getText());
                $driver->switchTo()->alert()->accept();
                continue;
            } catch (\Exception $e) {

            }

            $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="divConteudo"]/form/div[1]/img', '//*[@id="divConteudo"]/form/div[2]/input', '//*[@id="divConteudo"]/form/input[3]', false, '//*[@id="divConteudo"]/form/input[2]');

            try {
                $driver->switchTo()->alert()->accept();
            } catch (NoAlertOpenException $e) {

                $vara = '';
                $array = [];

                $conteudoProcesso = $driver->findElement(WebDriverBy::xpath('//*[@id="divConteudo"]'))->getText();
                $textToFind = 'Órgão Atual: ';
                $array = explode($textToFind, $conteudoProcesso);
                if (count($array) < 2) {
                    $textToFind = 'Órgão Julgador: ';
                    $array = explode($textToFind, $conteudoProcesso);
                }
                $robo->log("FOUND AT $textToFind");
                $vara = substr($array[1], 0, strpos($array[1], "\n"));
                $vara = $robo->cleanString($vara, ['Juízo Substituto da ', 'Juízo Federal da ']);
                if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                $encontrados++;

                //REUSO DO LINK C/ CAPTCHA
                $linkGet = $driver->getCurrentURL();
                $linkGet = str_replace($nroProcesso, '[NRO_PROCESSO]', $linkGet);

            } catch (NoSuchElementException $e) {
                $robo->log("NAO FOI POSSIVEL ENCONTRAR O ELEMENTO {$nroProcesso}");
            } catch (TimeOutException $e) {
                $robo->log("TIMEOUT AO ACESSAR {$nroProcesso}");
            } catch (\Exception $e) {
                $robo->log("ERRO CATASTROFICO {$nroProcesso}");
            }


        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    //RIO GRANDE DO SUL
    public function tjrspesquisageral($processos, $params = ['fonte' => '', 'nroCnj' => true])
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ - IDENTIFICADOR 8.21 E INICIO COM 9
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO THEMIS EX.: 095/1.02.1234567-3
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $fonteMapa = [
            'Tribunal de Justiça' => 'JUIZADO ESPECIAL',
            'Turmas Recursais' => 'TURMAS RECURSAIS',
            '' => '1° E 2° INSTÂNCIA FÍSICO'
        ];
//        $fonte = 'Tribunal de Justiça'; //TEM NRO 9 COMO PREFIXO
//        $fonte = 'Turmas Recursais';//NRO TERMINA COM SUFIXO 9000
//        $fonte = '';//NRO DIREFENTE DE PREFIXO 9 E SUFIXO 9000

        $fonte = $params['fonte'];

        $func = __FUNCTION__ . $fonte;

        $robo = new Robo();
        $robo->log("RIO GRANDE DO SUL - {$fonteMapa[$fonte]}");
        $link = 'http://www.tjrs.jus.br/site_php/consulta/index.php';
        $driver = $robo->startDriver();
        $driver->get($link);
//        $driver->findElement(WebDriverBy::xpath('//*[@id="td_processo"]/a'))->click();
//
//        $comarcas = [];
//        foreach ($driver->findElements(WebDriverBy::xpath('//*[@id="div_processo"]/table/tbody/tr[2]/td/table[2]/tbody/tr/td[2]/select/option')) as  $comarca) {
//            $comarcas[$comarca->getText()] = $comarca->getAttribute('value');
//        }
//
//        debug($comarcas);


        $processosTratados = [];
        foreach ($processos as $processo) {

            if ($params['nroCnj']) {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA LEVA
                if (!preg_match('/8.21./', $nroProcesso)) continue;//PRIMEIRA LEVA
//                if (substr($nroProcesso, 0, 1) != 9) continue;//PRIMEIRA LEVA - JUSTIÇA ESPECIAL
//                if (substr($nroProcesso, -4, 4) != 9000) continue;//PRIMEIRA LEVA - TURMA RECURSAL

//                if (substr($nroProcesso, 0, 1) == 9) continue;//PRIMEIRA LEVA - ELIMINAR JUSTIÇA ESPECIAL
//                if (substr($nroProcesso, -4, 4) == 9000) continue;//PRIMEIRA LEVA - ELIMINAR TURMA RECURSAL
            } else {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###/#.##.#######-#', 14);//SEGUNDA LEVA
                if ($nroProcesso === false) continue;
            }
            $robo->log($nroProcesso);

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        foreach ($processosTratados as $processo) {

            if ($params['nroCnj']) {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA LEVA
                if (!preg_match('/8.21./', $nroProcesso)) continue;//PRIMEIRA LEVA
//                if (substr($nroProcesso, 0, 1) != 9) continue;//PRIMEIRA LEVA - JUSTIÇA ESPECIAL
//                if (substr($nroProcesso, -4, 4) != 9000) continue;//PRIMEIRA LEVA - TURMA RECURSAL

//                if (substr($nroProcesso, 0, 1) == 9) continue;//PRIMEIRA LEVA - ELIMINAR JUSTIÇA ESPECIAL
//                if (substr($nroProcesso, -4, 4) == 9000) continue;//PRIMEIRA LEVA - ELIMINAR TURMA RECURSAL
            } else {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###/#.##.#######-#', 14);//SEGUNDA LEVA
                if ($nroProcesso === false) continue;
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->saveCache($func, $nroProcesso);

//            $driver->findElement(WebDriverBy::xpath('//*[@id="td_processo"]/a'))->click();//PRIMEIRA LEVA
            if ($fonte) $driver->findElement(WebDriverBy::xpath('//*[@id="div_processo"]/table/tbody/tr[2]/td/table[2]/tbody/tr/td[2]/select'))->sendKeys($fonte);
            if ($params['nroCnj']) $driver->findElement(WebDriverBy::xpath('//*[@id="div_processo"]/table/tbody/tr[2]/td/table[2]/tbody/tr/td[4]/table/tbody/tr[2]/td/input[2]'))->click();
            $driver->findElement(WebDriverBy::xpath('//*[@id="num_processo_mask"]'))->sendKeys($nroProcesso);


            //CAPTCHA
            try {
                while (true) {
                    $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="humancheck"]/table/tbody/tr[1]/td/img', '//*[@id="code"]', '//*[@id="btnPesquisar"]', false, '//*[@id="humancheck"]/table/tbody/tr[1]/td/span/a[1]');
                    $body = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('body'))->getText());
                    if (preg_match('/Número de confirmação não confere/', $body)) {
                        $driver->navigate()->back();
                    } else {
                        break;
                    }
                }
            } catch (\Exception $e) {
            }

            try {
                $debugActive = false;

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody/tr')));
                $tableDadosPrincipaisElem = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table[3]/tbody/tr'));
                $tableDadosPrincipais = $robo->extractVerticalTable($tableDadosPrincipaisElem, 3, false, 1);

                //1° E 2° INST NCIA
                $instancia = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="titulo"]/span[1]'))->getText());

                $tableText = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/table[2]'))->getText());
                if (preg_match('/Procedimento do Juizado Especial Cível/', $tableText)) {
                    $instancia = 'Juizado Especial ' . $instancia;
                }
                if (preg_match('/RECURSO CIVEL/', $tableText)) {
                    $instancia = 'Turma Recursal ' . $instancia;
                }

                $robo->log($instancia);
                $processo->addObs($instancia);
                $instancia = str_replace('º', 'degree', $instancia);
                if (preg_match('/1degree Grau/', $instancia)) {
                    $vara = '';
                    $key = 'Órgão Julgador';
                    if (isset($tableDadosPrincipais[$key]) && $tableDadosPrincipais[$key] != '') {
                        $vara = $robo->cleanString($tableDadosPrincipais[$key], ['; ', ';']);
                        if ($vara != '') {
                            $robo->addVara($processo, $vara, $debugActive, $driver->getCurrentURL());
                            $encontrados++;
                        }
                    }
                }
                if (preg_match('/2degree Grau/', $instancia)) {
                    $vara = '';
                    $key = 'Local dos Autos';
                    if (isset($tableDadosPrincipais[$key]) && $tableDadosPrincipais[$key] != '') {
                        $vara = $robo->cleanString($tableDadosPrincipais[$key], ['; ', ';']);
                        if ($vara != '') {
                            $robo->addVara($processo, $vara, $debugActive, $driver->getCurrentURL());
                            $encontrados++;
                        }
                    }
                }

            } catch (\Exception $e) {
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/table[1]'))->getText());
                $robo->log($msg);
            }

            $driver->navigate()->to($link);
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }


    //MINAS GERAIS
    public function tjmgprojudi1instancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();

        $alterados = 0;

        $processosTratados = [];
        foreach ($processos as $item) {
            $nroProcesso = $robo->cnjNumberFormatter($item->getNumProcJust());
            if (strlen($nroProcesso) < 25) continue;
            if (!preg_match('/.8.13./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) != '9') continue;
            $arrAux = explode('.', $nroProcesso);
            $arrAuxStart = explode('-', $arrAux[0]);
            $projudiNro = $arrAux[1] . $arrAuxStart[0];
            $processosTratados[$projudiNro] = $item;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");

        $link = "https://projudi.tjmg.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[ANO+7INICIO_CNJ]";
        $driver = $robo->startDriver();
        $driver->get('https://projudi.tjmg.jus.br/projudi/interno.jsp?endereco=/projudi/consultapublica/CentroConsultaPublica');
        $index = 0;
        $alterados = 0;
        foreach ($processosTratados as $pesquisar => $processo) {

            $linkNew = str_replace('[ANO+7INICIO_CNJ]', $pesquisar, $link);

            $driver->navigate()->to($linkNew);

            $robo->log("{$robo->percentage($total,$index)} => {$pesquisar}");
            $index++;

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/form[2]/div[1]/table/tbody/tr[2]/td[1]')));
            } catch (\Exception $e) {
                $robo->log("NO TABLES");
                continue;
            }

            $vara = $robo->cleanString($driver->findElement(WebDriverBy::xpath("/html/body/form[2]/div[1]/table/tbody/tr[2]/td[1]"))->getText());
            if ($vara != '') {
                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                $alterados++;
            }
        }


        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $driver->quit();
        return true;
    }

    public function tjmgprojudi2instancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();

        $robo->log("TRIBUNAL MINAS GERAIS - PROJUDI 2° INSTÂNCIA");

        $processosTratados = [];
        foreach ($processos as $item) {
            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(), '#######-##.####.#.##.####', 20);
            if (!$nroProcesso) continue;
            if (!preg_match('/.8.13./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) != '9') continue;
            $processosTratados[preg_replace('/\D/', '', $nroProcesso)] = $item;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");

        $driver = $robo->startDriver();
        $link = 'https://projudi.tjmg.jus.br/projudi/frameCentro/ProcessosPartePublico';
        $driver->get('https://projudi.tjmg.jus.br/projudi/interno.jsp?endereco=/projudi/consultapublica/CentroConsultaPublica');
        $index = 0;
        $alterados = 0;
        $captcha = '';
        foreach ($processosTratados as $nroProcesso => $processo) {

            $driver->navigate()->to($link);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            try {
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabset1_tab_cont_2"]'))->click();
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="numeroRecurso"]')));
                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroRecurso"]'))->sendKeys($nroProcesso);
//                $driver->findElement(WebDriverBy::xpath('//*[@id="cont_2"]/div/table/tbody/tr[5]/td/div/input'))->click();

                //TERMINAR, PORÉM PARECE QUE NÃO HÁ PROCESSOS COM PREFIXO 9
                try {
                    $iframeSrc = $driver->findElement(WebDriverBy::xpath('//*[@id="RecaptchaField2"]/div/div/iframe'))->getAttribute('src');
                    //https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LdW4xwTAAAAACyzeaa4PH1LqFEPuQKHvq5yxkiF&co=aHR0cHM6Ly9wcm9qdWRpLnRqbWcuanVzLmJyOjQ0Mw..&hl=pt-BR&v=v1531117903872&size=normal&cb=4ztsltwu9ven
                    $googleKey = $robo->findBetween($iframeSrc, 'anchor?ar=1&k', '&hl=');//ADD = AO KEY PARA QUE O METODO RECONHEÇA QUE É A CHAVE AO INVES DO ELEMENT CSS

                    $captcha = $robo->hasReCaptcha(
                        $driver,
                        $googleKey,
                        'xpath',
                        '//*[@id="RecaptchaField2"]/div/div/iframe'
                    );
                } catch (\Exception $e) {
                    debug($e->getMessage());
                }

                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="cont_2"]/div/table/tbody/tr[5]/td/div/input'))->click();


                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/form[2]/div[1]/table/tbody/tr[2]/td[1]')));
            } catch (\Exception $e) {
                $robo->log("NO TABLES");
                continue;
            }

            $vara = $robo->cleanString($driver->findElement(WebDriverBy::xpath("/html/body/form[2]/div[1]/table/tbody/tr[2]/td[1]"))->getText());
            if ($vara != '') {
                $robo->addVara($processo, $vara, true, $driver->getCurrentURL());
                $alterados++;
            }
        }


        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $driver->quit();
        return true;
    }

    public function tjmgpje($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();

        $robo->log("TRIBUNAL MINAS GERAIS 2° INSTÂNCIA FÍSICO - " . __FUNCTION__);

        $alterados = 0;

        $processosTratados = [];
        $totalProcessos = 0;
        foreach ($processos as $item) {
            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(), '#######-##.####.#.##.####', 20);
            if (!$nroProcesso) continue;
            if (!preg_match('/.8.13./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) == 0) continue;
            $processosTratados[] = $item;
            $robo->log($nroProcesso);
            $totalProcessos++;
        }

        $robo->log("$totalProcessos PROCESSOS ON TARGET");

        $driver = $robo->startDriver();
        $link = 'https://pje.tjmg.jus.br/pje/ConsultaPublica/listView.seam';
        $driver->get($link);
        $index = 0;
        $alterados = 0;
        $captcha = '';
        $encontrados = 0;
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);

            $robo->log("{$robo->percentage($totalProcessos,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->setOrigem($driver->getCurrentURL());
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE " . __LINE__ . " => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $robo->log($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE " . __LINE__ . " => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to('https://pje.tjmg.jus.br' . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->log("{$encontrados} PROCESSOS COM JUIZO|VARA");
        $driver->quit();
        return true;
    }

    public function tjmg2instancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosArr \Tribunal\Service\Job[] */

        $comarcaOptions = '<option value="2">Abaeté</option><option value="3">Abre-Campo</option><option value="5">Açucena</option><option value="9">Águas Formosas</option><option value="11">Aimorés</option><option value="12">Aiuruoca</option><option value="15">Além Paraíba</option><option value="16">Alfenas</option><option value="17">Almenara</option><option value="19">Alpinópolis</option><option value="21">Alto Rio Doce</option><option value="23">Alvinópolis</option><option value="26">Andradas</option><option value="28">Andrelândia</option><option value="34">Araçuaí</option><option value="35">Araguari</option><option value="40">Araxá</option><option value="42">Arcos</option><option value="43">Areado</option><option value="778">Arinos</option><option value="49">Baependi</option><option value="51">Bambuí</option><option value="54">Barão de Cocais</option><option value="56">Barbacena</option><option value="59">Barroso</option><option value="24">Belo Horizonte</option>			<option value="64">Belo Vale</option><option value="27">Betim</option><option value="69">Bicas</option><option value="71">Boa Esperança</option><option value="73">Bocaiúva</option><option value="74">Bom Despacho</option><option value="80">Bom Sucesso</option><option value="81">Bonfim</option><option value="82">Bonfinópolis de Minas</option><option value="83">Borda da Mata</option><option value="84">Botelhos</option><option value="86">Brasília de Minas</option><option value="89">Brasópolis</option><option value="90">Brumadinho</option><option value="91">Bueno Brandão</option><option value="92">Buenópolis</option><option value="93">Buritis</option><option value="95">Cabo Verde</option><option value="97">Cachoeira de Minas</option><option value="45">Caeté</option><option value="103">Caldas</option><option value="878">Camanducaia</option><option value="106">Cambuí</option><option value="107">Cambuquira</option><option value="109">Campanha</option><option value="110">Campestre</option><option value="111">Campina Verde</option><option value="112">Campo Belo</option><option value="115">Campos Altos</option><option value="116">Campos Gerais</option><option value="118">Canápolis</option><option value="120">Candeias</option><option value="123">Capelinha</option><option value="126">Capinópolis</option><option value="132">Carandaí</option><option value="133">Carangola</option><option value="134">Caratinga</option><option value="137">Carlos Chagas</option><option value="140">Carmo da Mata</option><option value="141">Carmo de Minas</option><option value="142">Carmo do Cajuru</option><option value="143">Carmo do Paranaíba</option><option value="144">Carmo do Rio Claro</option><option value="879">Carmópolis de Minas</option><option value="151">Cássia</option><option value="153">Cataguases</option><option value="155">Caxambu</option><option value="166">Cláudio</option><option value="172">Conceição das Alagoas</option><option value="175">Conceição do Mato Dentro</option><option value="177">Conceição do Rio Verde</option><option value="180">Congonhas</option><option value="182">Conquista</option><option value="183">Conselheiro Lafaiete</option><option value="184">Conselheiro Pena</option><option value="79">Contagem</option><option value="775">Coração de Jesus</option><option value="191">Corinto</option><option value="193">Coromandel</option><option value="194">Coronel Fabriciano</option><option value="205">Cristina</option><option value="208">Cruzília</option><option value="209">Curvelo</option><option value="216">Diamantina</option><option value="220">Divino</option><option value="223">Divinópolis</option><option value="232">Dores do Indaiá</option><option value="236">Elói Mendes</option><option value="239">Entre-Rios de Minas</option><option value="240">Ervália</option><option value="241">Esmeraldas</option><option value="242">Espera Feliz</option><option value="243">Espinosa</option><option value="248">Estrela do Sul</option><option value="249">Eugenópolis</option><option value="251">Extrema</option><option value="259">Ferros</option><option value="261">Formiga</option><option value="267">Francisco Sá</option><option value="271">Frutal</option><option value="273">Galiléia</option><option value="105">Governador Valadares</option><option value="278">Grão Mogol</option><option value="280">Guanhães</option><option value="281">Guapé</option><option value="283">Guaranésia</option><option value="284">Guarani</option><option value="287">Guaxupé</option><option value="295">Ibiá</option><option value="297">Ibiraci</option><option value="114">Ibirité</option><option value="301">Igarapé</option><option value="303">Iguatama</option><option value="309">Inhapim</option><option value="312">Ipanema</option><option value="313">Ipatinga</option><option value="317">Itabira</option><option value="319">Itabirito</option><option value="322">Itaguara</option><option value="324">Itajubá</option><option value="325">Itamarandiba</option><option value="327">Itambacuri</option><option value="329">Itamoji</option><option value="330">Itamonte</option><option value="331">Itanhandu</option><option value="332">Itanhomi</option><option value="334">Itapajipe</option><option value="335">Itapecerica</option><option value="338">Itaúna</option><option value="342">Ituiutaba</option><option value="343">Itumirim</option><option value="344">Iturama</option><option value="346">Jabuticatubas</option><option value="347">Jacinto</option><option value="348">Jacuí</option><option value="349">Jacutinga</option><option value="351">Janaúba</option><option value="352">Januária</option><option value="355">Jequeri</option><option value="358">Jequitinhonha</option><option value="362">João Monlevade</option><option value="363">João Pinheiro</option><option value="145">Juiz de Fora</option><option value="372">Lagoa da Prata</option><option value="148">Lagoa Santa</option><option value="377">Lajinha</option><option value="378">Lambari</option><option value="382">Lavras</option><option value="384">Leopoldina</option><option value="386">Lima Duarte</option><option value="388">Luz</option><option style="background-color: #FF9999" value="-">Machado&nbsp;-&nbsp;(INDISPONÍVEL)</option><option value="392">Malacacheta</option><option value="393">Manga</option><option value="394">Manhuaçu</option><option value="395">Manhumirim</option><option value="396">Mantena</option><option value="398">Mar de Espanha</option><option value="400">Mariana</option><option value="405">Martinho Campos</option><option value="407">Mateus Leme</option><option value="408">Matias Barbosa</option><option value="411">Matozinhos</option><option style="background-color: #FF9999" value="-">Medina&nbsp;-&nbsp;(INDISPONÍVEL)</option><option value="416">Mercês</option><option value="417">Mesquita</option><option value="418">Minas Novas</option><option value="421">Miradouro</option><option value="422">Miraí</option><option value="427">Montalvânia</option><option value="428">Monte Alegre de Minas</option><option value="429">Monte Azul</option><option value="430">Monte Belo</option><option value="431">Monte Carmelo</option><option value="432">Monte Santo de Minas</option><option value="434">Monte Sião</option><option value="433">Montes Claros</option><option value="435">Morada Nova de Minas</option><option value="439">Muriaé</option><option value="440">Mutum</option><option value="441">Muzambinho</option><option value="443">Nanuque</option><option value="444">Natércia</option><option value="446">Nepomuceno</option><option value="447">Nova Era</option><option value="188">Nova Lima</option><option value="450">Nova Ponte</option><option value="451">Nova Resende</option><option value="452">Nova Serrana</option><option value="453">Novo Cruzeiro</option><option value="456">Oliveira</option><option value="459">Ouro Branco</option><option value="460">Ouro Fino</option><option value="461">Ouro Preto</option><option style="background-color: #FF9999" value="-">Palma&nbsp;-&nbsp;(INDISPONÍVEL)</option><option value="471">Pará de Minas</option><option value="470">Paracatu</option><option value="472">Paraguaçu</option><option value="473">Paraisópolis</option><option value="474">Paraopeba</option><option value="476">Passa-Quatro</option><option value="477">Passa-Tempo</option><option value="479">Passos</option><option value="480">Patos de Minas</option><option value="481">Patrocínio</option><option value="486">Peçanha</option><option value="487">Pedra Azul</option><option value="491">Pedralva</option><option value="210">Pedro Leopoldo</option><option value="498">Perdizes</option><option value="499">Perdões</option><option value="508">Piranga</option><option value="511">Pirapetinga</option><option value="512">Pirapora</option><option value="514">Pitangui</option><option value="515">Piumhi</option><option value="517">Poço Fundo</option><option value="518">Poços de Caldas</option><option value="520">Pompeu</option><option value="521">Ponte Nova</option><option value="522">Porteirinha</option><option value="525">Pouso Alegre</option><option value="527">Prados</option><option value="528">Prata</option><option value="529">Pratápolis</option><option value="534">Presidente Olegário</option><option value="540">Raul Soares</option><option value="542">Resende Costa</option><option value="543">Resplendor</option><option value="231">Ribeirão das Neves</option><option value="549">Rio Casca</option><option value="554">Rio Novo</option><option value="555">Rio Paranaíba</option><option value="556">Rio Pardo de Minas</option><option value="557">Rio Piracicaba</option><option value="558">Rio Pomba</option><option value="559">Rio Preto</option><option value="560">Rio Vermelho</option><option value="567">Sabará</option><option value="568">Sabinópolis</option><option value="569">Sacramento</option><option value="570">Salinas</option><option value="572">Santa Bárbara</option><option value="245">Santa Luzia</option><option value="582">Santa Maria do Suaçuí</option><option value="592">Santa Rita de Caldas</option><option value="596">Santa Rita do Sapucaí</option><option value="598">Santa Vitória</option><option value="604">Santo Antônio do Monte</option><option value="607">Santos Dumont</option><option value="610">São Domingos do Prata</option><option value="611">São Francisco</option><option value="620">São Gonçalo do Sapucaí</option><option value="621">São Gotardo</option><option value="624">São João da Ponte</option><option value="625">São João del-Rei</option><option value="627">São João do Paraíso</option><option value="628">São João Evangelista</option><option value="629">São João Nepomuceno</option><option value="637">São Lourenço</option><option value="642">São Romão</option><option value="643">São Roque de Minas</option><option value="647">São Sebastião do Paraíso</option><option value="657">Senador Firmino</option><option value="671">Serro</option><option value="672">Sete Lagoas</option><option value="674">Silvianópolis</option><option value="680">Taiobeiras</option><option value="684">Tarumirim</option><option value="685">Teixeiras</option><option value="686">Teófilo Otôni</option><option value="687">Timóteo</option><option value="689">Tiros</option><option value="692">Tombos</option><option value="693">Três Corações</option><option value="58">Três Marias</option><option value="694">Três Pontas</option><option value="696">Tupaciguara</option><option value="697">Turmalina</option><option value="699">Ubá</option><option value="701">Uberaba</option><option value="702">Uberlândia</option><option value="704">Unaí</option><option value="707">Varginha</option><option value="708">Várzea da Palma</option><option value="710">Vazante</option><option value="290">Vespasiano</option><option value="713">Viçosa</option><option value="718">Virginópolis</option><option value="720">Visconde do Rio Branco</option>';
        $doc = new \DOMDocument();
        $doc->loadHTML($comarcaOptions);
        $comarcas = [];
        foreach ($doc->getElementsByTagName('option') as $comarca) {
            $value = preg_replace('/\D/', '', $comarca->getAttribute('value'));
            if ($value == '' || $value < 1) continue;
            $comarcas[] = str_pad($value, 4, 0, STR_PAD_LEFT);
        }

        $robo = new Robo();

        $robo->log("TRIBUNAL MINAS GERAIS 2° INSTÂNCIA FÍSICO - " . __FUNCTION__);

        $alterados = 0;

        $processosTratados = [];
        $totalProcessos = 0;
        foreach ($processos as $item) {
            //PRIMEIRA LEVA C/ CNJ
//            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(),'#######-##.####.#.##.####',20);//PRIMEIRA LEVA C/ CNJ
//            if (!preg_match('/.8.13./', $nroProcesso)) continue;//PRIMEIRA LEVA C/ CNJ
//            $arrAux = explode('.8.13.', $nroProcesso);//PRIMEIRA LEVA C/ CNJ
//            if (!isset($arrAux[1])) continue;//PRIMEIRA LEVA C/ CNJ
//            $processosTratados[$arrAux[1]][] = $item;//PRIMEIRA LEVA C/ CNJ

            //SENGUNDA LEVA C/ OUTRO NRO
//            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(), '#.####.##.######-#/###', 17);//SENGUNDA LEVA C/ OUTRO NRO
//            if (!$nroProcesso) continue;//SENGUNDA LEVA C/ OUTRO NRO
////            if (substr($nroProcesso,7,2) > 18 || substr($nroProcesso,7,2) < 70) continue;
//            $comarca = substr($nroProcesso, 2, 4);//SENGUNDA LEVA C/ OUTRO NRO
//            if (!in_array($comarca, $comarcas)) continue;//SENGUNDA LEVA C/ OUTRO NRO
//            $processosTratados[$comarca][] = $item;//SENGUNDA LEVA C/ OUTRO NRO

//            $robo->log($comarca . " > " . $nroProcesso);

            //TERCEIRA LEVA SEM FILTRO E MASCARA
            $nroProcesso = preg_replace('/\D/', '', $item->getNumProcJust());
            if (!$nroProcesso) continue;
            $processosTratados['sem_comarca'][] = $item;

            $robo->log($nroProcesso);
            $totalProcessos++;
        }

        $robo->log("$totalProcessos PROCESSOS ON TARGET");

        $driver = $robo->startDriver();
        $link = 'http://www4.tjmg.jus.br/juridico/sf/proc_resultado2.jsp?lst_processos=&listaProcessos=[LISTA_PROCESSOS]';
        $driver->get($robo->host);
        foreach ($processosTratados as $comarca => $processosArr) {

//            $quebra = 20;
            $quebra = 1;//TERCEIRA LEVA
            $qtdProcComarca = count($processosArr);
            $robo->log("COMARCA $comarca => $qtdProcComarca PROCESSO(S)");
            $qtdLoop = $qtdProcComarca / $quebra;
            if (is_float($qtdLoop)) {
                if ($qtdLoop < 1) {
                    $qtdLoop = 1;
                } else {
                    if ($qtdLoop >= 1) $qtdLoop = (int)$qtdLoop + 1;
                }
            }

            $indexProc = 0;
            for ($i = 0; $i < $qtdLoop; $i++) {
                $processosToSearch = [];
                for ($x = 0; $x < $quebra; $x++) {
                    if (!isset($processosArr[$indexProc])) break;
//                    $processosToSearch[preg_replace('/\D/', '', $robo->cnjNumberFormatter($processosArr[$indexProc]->getNumProcJust()))] = $processosArr[$indexProc];//PRIMEIRA LEVA C/ CNJ
//                    $processosToSearch[preg_replace('/\D/', '', $robo->processNumberFormatter($processosArr[$indexProc]->getNumProcJust(), '#.####.##.######-#/###', 17))] = $processosArr[$indexProc];//SENGUNDA LEVA C/ OUTRO NRO
                    $processosToSearch[preg_replace('/\D/', '', $processosArr[$indexProc]->getNumProcJust())] = $processosArr[$indexProc];//TERCEIRA LEVA
                    $indexProc++;
                }

                $linkNew = str_replace('[LISTA_PROCESSOS]', implode(',', array_keys($processosToSearch)), $link);
                $driver->navigate()->to($linkNew);

                $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');

                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table.tabela_formulario')));
                } catch (\Exception $e) {
                    $robo->log("NO TABLES");
                    continue;
                }

                $tables = $driver->findElements(WebDriverBy::cssSelector("table.tabela_formulario"));
                $robo->log(count($tables) . " TABELAS ENCONTRADAS");
                foreach ($tables as $table) {

                    $wholeTableCheck = $table->getText();
                    if ($robo->extractProcessNumber($wholeTableCheck) == $wholeTableCheck) {
                        $robo->log("TABLE INVÁLIDA");
                        continue;
                    }

                    $processoSelecionado = '';
                    $vara = '';

                    $cells = $table->findElements(WebDriverBy::tagName('td'));

                    if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[0]->getText())) {
                        $processoSelecionado = $robo->cleanString($cells[0]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                    } else {
                        if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[1]->getText())) {
                            $processoSelecionado = $robo->cleanString($cells[1]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                        } else {
                            if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[2]->getText())) {
                                $processoSelecionado = $robo->cleanString($cells[2]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                            } else {
                                if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[3]->getText())) {
                                    $processoSelecionado = $robo->cleanString($cells[3]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                                }
                            }
                        }
                    }

                    $vara = $robo->cleanString($table->findElement(WebDriverBy::id("campoSecretaria"))->getText());

                    $processoClean = preg_replace('/\D/', '', $processoSelecionado);
                    if (isset($processosToSearch[$processoClean]) && $processoSelecionado != '' && $vara != '') {
                        $robo->addVara($processosToSearch[$processoClean], $vara, false, $driver->getCurrentURL());
                        $alterados++;
                    }
                }
            }
        }

        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $driver->quit();
        return true;
    }

    public function tjmg1instancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processosArr \Tribunal\Service\Job[] */
        $comarcaOptions = '<option value="2">Abaeté</option><option value="3">Abre-Campo</option><option value="5">Açucena</option><option value="9">Águas Formosas</option><option value="11">Aimorés</option><option value="12">Aiuruoca</option><option value="15">Além Paraíba</option><option value="16">Alfenas</option><option value="17">Almenara</option><option value="19">Alpinópolis</option><option value="21">Alto Rio Doce</option><option value="23">Alvinópolis</option><option value="26">Andradas</option><option value="28">Andrelândia</option><option value="34">Araçuaí</option><option value="35">Araguari</option><option value="40">Araxá</option><option value="42">Arcos</option><option value="43">Areado</option><option value="778">Arinos</option><option value="49">Baependi</option><option value="51">Bambuí</option><option value="54">Barão de Cocais</option><option value="56">Barbacena</option><option value="59">Barroso</option><option value="24">Belo Horizonte</option>			<option value="64">Belo Vale</option><option value="27">Betim</option><option value="69">Bicas</option><option value="71">Boa Esperança</option><option value="73">Bocaiúva</option><option value="74">Bom Despacho</option><option value="80">Bom Sucesso</option><option value="81">Bonfim</option><option value="82">Bonfinópolis de Minas</option><option value="83">Borda da Mata</option><option value="84">Botelhos</option><option value="86">Brasília de Minas</option><option value="89">Brasópolis</option><option value="90">Brumadinho</option><option value="91">Bueno Brandão</option><option value="92">Buenópolis</option><option value="93">Buritis</option><option value="95">Cabo Verde</option><option value="97">Cachoeira de Minas</option><option value="45">Caeté</option><option value="103">Caldas</option><option value="878">Camanducaia</option><option value="106">Cambuí</option><option value="107">Cambuquira</option><option value="109">Campanha</option><option value="110">Campestre</option><option value="111">Campina Verde</option><option value="112">Campo Belo</option><option value="115">Campos Altos</option><option value="116">Campos Gerais</option><option value="118">Canápolis</option><option value="120">Candeias</option><option value="123">Capelinha</option><option value="126">Capinópolis</option><option value="132">Carandaí</option><option value="133">Carangola</option><option value="134">Caratinga</option><option value="137">Carlos Chagas</option><option value="140">Carmo da Mata</option><option value="141">Carmo de Minas</option><option value="142">Carmo do Cajuru</option><option value="143">Carmo do Paranaíba</option><option value="144">Carmo do Rio Claro</option><option value="879">Carmópolis de Minas</option><option value="151">Cássia</option><option value="153">Cataguases</option><option value="155">Caxambu</option><option value="166">Cláudio</option><option value="172">Conceição das Alagoas</option><option value="175">Conceição do Mato Dentro</option><option value="177">Conceição do Rio Verde</option><option value="180">Congonhas</option><option value="182">Conquista</option><option value="183">Conselheiro Lafaiete</option><option value="184">Conselheiro Pena</option><option value="79">Contagem</option><option value="775">Coração de Jesus</option><option value="191">Corinto</option><option value="193">Coromandel</option><option value="194">Coronel Fabriciano</option><option value="205">Cristina</option><option value="208">Cruzília</option><option value="209">Curvelo</option><option value="216">Diamantina</option><option value="220">Divino</option><option value="223">Divinópolis</option><option value="232">Dores do Indaiá</option><option value="236">Elói Mendes</option><option value="239">Entre-Rios de Minas</option><option value="240">Ervália</option><option value="241">Esmeraldas</option><option value="242">Espera Feliz</option><option value="243">Espinosa</option><option value="248">Estrela do Sul</option><option value="249">Eugenópolis</option><option value="251">Extrema</option><option value="259">Ferros</option><option value="261">Formiga</option><option value="267">Francisco Sá</option><option value="271">Frutal</option><option value="273">Galiléia</option><option value="105">Governador Valadares</option><option value="278">Grão Mogol</option><option value="280">Guanhães</option><option value="281">Guapé</option><option value="283">Guaranésia</option><option value="284">Guarani</option><option value="287">Guaxupé</option><option value="295">Ibiá</option><option value="297">Ibiraci</option><option value="114">Ibirité</option><option value="301">Igarapé</option><option value="303">Iguatama</option><option value="309">Inhapim</option><option value="312">Ipanema</option><option value="313">Ipatinga</option><option value="317">Itabira</option><option value="319">Itabirito</option><option value="322">Itaguara</option><option value="324">Itajubá</option><option value="325">Itamarandiba</option><option value="327">Itambacuri</option><option value="329">Itamoji</option><option value="330">Itamonte</option><option value="331">Itanhandu</option><option value="332">Itanhomi</option><option value="334">Itapajipe</option><option value="335">Itapecerica</option><option value="338">Itaúna</option><option value="342">Ituiutaba</option><option value="343">Itumirim</option><option value="344">Iturama</option><option value="346">Jabuticatubas</option><option value="347">Jacinto</option><option value="348">Jacuí</option><option value="349">Jacutinga</option><option value="351">Janaúba</option><option value="352">Januária</option><option value="355">Jequeri</option><option value="358">Jequitinhonha</option><option value="362">João Monlevade</option><option value="363">João Pinheiro</option><option value="145">Juiz de Fora</option><option value="372">Lagoa da Prata</option><option value="148">Lagoa Santa</option><option value="377">Lajinha</option><option value="378">Lambari</option><option value="382">Lavras</option><option value="384">Leopoldina</option><option value="386">Lima Duarte</option><option value="388">Luz</option><option style="background-color: #FF9999" value="-">Machado&nbsp;-&nbsp;(INDISPONÍVEL)</option><option value="392">Malacacheta</option><option value="393">Manga</option><option value="394">Manhuaçu</option><option value="395">Manhumirim</option><option value="396">Mantena</option><option value="398">Mar de Espanha</option><option value="400">Mariana</option><option value="405">Martinho Campos</option><option value="407">Mateus Leme</option><option value="408">Matias Barbosa</option><option value="411">Matozinhos</option><option style="background-color: #FF9999" value="-">Medina&nbsp;-&nbsp;(INDISPONÍVEL)</option><option value="416">Mercês</option><option value="417">Mesquita</option><option value="418">Minas Novas</option><option value="421">Miradouro</option><option value="422">Miraí</option><option value="427">Montalvânia</option><option value="428">Monte Alegre de Minas</option><option value="429">Monte Azul</option><option value="430">Monte Belo</option><option value="431">Monte Carmelo</option><option value="432">Monte Santo de Minas</option><option value="434">Monte Sião</option><option value="433">Montes Claros</option><option value="435">Morada Nova de Minas</option><option value="439">Muriaé</option><option value="440">Mutum</option><option value="441">Muzambinho</option><option value="443">Nanuque</option><option value="444">Natércia</option><option value="446">Nepomuceno</option><option value="447">Nova Era</option><option value="188">Nova Lima</option><option value="450">Nova Ponte</option><option value="451">Nova Resende</option><option value="452">Nova Serrana</option><option value="453">Novo Cruzeiro</option><option value="456">Oliveira</option><option value="459">Ouro Branco</option><option value="460">Ouro Fino</option><option value="461">Ouro Preto</option><option style="background-color: #FF9999" value="-">Palma&nbsp;-&nbsp;(INDISPONÍVEL)</option><option value="471">Pará de Minas</option><option value="470">Paracatu</option><option value="472">Paraguaçu</option><option value="473">Paraisópolis</option><option value="474">Paraopeba</option><option value="476">Passa-Quatro</option><option value="477">Passa-Tempo</option><option value="479">Passos</option><option value="480">Patos de Minas</option><option value="481">Patrocínio</option><option value="486">Peçanha</option><option value="487">Pedra Azul</option><option value="491">Pedralva</option><option value="210">Pedro Leopoldo</option><option value="498">Perdizes</option><option value="499">Perdões</option><option value="508">Piranga</option><option value="511">Pirapetinga</option><option value="512">Pirapora</option><option value="514">Pitangui</option><option value="515">Piumhi</option><option value="517">Poço Fundo</option><option value="518">Poços de Caldas</option><option value="520">Pompeu</option><option value="521">Ponte Nova</option><option value="522">Porteirinha</option><option value="525">Pouso Alegre</option><option value="527">Prados</option><option value="528">Prata</option><option value="529">Pratápolis</option><option value="534">Presidente Olegário</option><option value="540">Raul Soares</option><option value="542">Resende Costa</option><option value="543">Resplendor</option><option value="231">Ribeirão das Neves</option><option value="549">Rio Casca</option><option value="554">Rio Novo</option><option value="555">Rio Paranaíba</option><option value="556">Rio Pardo de Minas</option><option value="557">Rio Piracicaba</option><option value="558">Rio Pomba</option><option value="559">Rio Preto</option><option value="560">Rio Vermelho</option><option value="567">Sabará</option><option value="568">Sabinópolis</option><option value="569">Sacramento</option><option value="570">Salinas</option><option value="572">Santa Bárbara</option><option value="245">Santa Luzia</option><option value="582">Santa Maria do Suaçuí</option><option value="592">Santa Rita de Caldas</option><option value="596">Santa Rita do Sapucaí</option><option value="598">Santa Vitória</option><option value="604">Santo Antônio do Monte</option><option value="607">Santos Dumont</option><option value="610">São Domingos do Prata</option><option value="611">São Francisco</option><option value="620">São Gonçalo do Sapucaí</option><option value="621">São Gotardo</option><option value="624">São João da Ponte</option><option value="625">São João del-Rei</option><option value="627">São João do Paraíso</option><option value="628">São João Evangelista</option><option value="629">São João Nepomuceno</option><option value="637">São Lourenço</option><option value="642">São Romão</option><option value="643">São Roque de Minas</option><option value="647">São Sebastião do Paraíso</option><option value="657">Senador Firmino</option><option value="671">Serro</option><option value="672">Sete Lagoas</option><option value="674">Silvianópolis</option><option value="680">Taiobeiras</option><option value="684">Tarumirim</option><option value="685">Teixeiras</option><option value="686">Teófilo Otôni</option><option value="687">Timóteo</option><option value="689">Tiros</option><option value="692">Tombos</option><option value="693">Três Corações</option><option value="58">Três Marias</option><option value="694">Três Pontas</option><option value="696">Tupaciguara</option><option value="697">Turmalina</option><option value="699">Ubá</option><option value="701">Uberaba</option><option value="702">Uberlândia</option><option value="704">Unaí</option><option value="707">Varginha</option><option value="708">Várzea da Palma</option><option value="710">Vazante</option><option value="290">Vespasiano</option><option value="713">Viçosa</option><option value="718">Virginópolis</option><option value="720">Visconde do Rio Branco</option>';
        $doc = new \DOMDocument();
        $doc->loadHTML($comarcaOptions);
        $comarcas = [];
        foreach ($doc->getElementsByTagName('option') as $comarca) {
            $value = preg_replace('/\D/', '', $comarca->getAttribute('value'));
            if ($value == '' || $value < 1) continue;
            $comarcas[] = str_pad($value, 4, 0, STR_PAD_LEFT);
        }

        $robo = new Robo();

        $robo->log("TRIBUNAL MINAS GERAIS 1° INSTÂNCIA FÍSICO - " . __FUNCTION__);

        $alterados = 0;
        $totalProcessos = 0;
        $processosTratados = [];
        foreach ($processos as $item) {
//            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(), '#######-##.####.#.##.####', 20);//PRIMEIRA LEVA C/ CNJ
//            if (!preg_match('/.8.13./', $nroProcesso)) continue;//PRIMEIRA LEVA C/ CNJ
//            $arrAux = explode('.8.13.', $nroProcesso);//PRIMEIRA LEVA C/ CNJ
//            if (!isset($arrAux[1])) continue;//PRIMEIRA LEVA C/ CNJ
//            $processosTratados[$arrAux[1]][] = $item;//PRIMEIRA LEVA C/ CNJ

//            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(), '#.####.##.######-#/###', 17);//SENGUNDA LEVA C/ OUTRO NRO
//            if (!$nroProcesso) continue;//SENGUNDA LEVA C/ OUTRO NRO
//            $comarca = substr($nroProcesso, 2, 4);//SENGUNDA LEVA C/ OUTRO NRO
//            if (!in_array($comarca, $comarcas)) continue;//SENGUNDA LEVA C/ OUTRO NRO
//            $robo->log($nroProcesso);
//            $robo->log($comarca);
//            $processosTratados[$comarca][] = $item;//SENGUNDA LEVA C/ OUTRO NRO

            //TERCEIRA LEVA SEM MASCARA C/COMARCA NA FRENTE
            $nroProcesso = $robo->onlyDigits($item->getNumProcJust());
            if (!$nroProcesso) continue;
            $comarca = substr($nroProcesso, 0, 4);
            if (!in_array($comarca, $comarcas)) continue;
            $robo->log($nroProcesso);
            $robo->log($comarca);
            $processosTratados[$comarca][] = $item;

            $robo->log($comarca . " > " . $nroProcesso);
            $totalProcessos++;
        }

        $robo->log("$totalProcessos PROCESSOS ON TARGET");

        $driver = $robo->startDriver();
        $link = 'http://www4.tjmg.jus.br/juridico/sf/proc_resultado.jsp?comrCodigo=[COMARCA]&numero=1&listaProcessos=[LISTA_PROCESSOS]&btn_pesquisar=Pesquisar';
        $driver->get($robo->host);
        foreach ($processosTratados as $comarca => $processosArr) {
            $comardaSemZero = ltrim($comarca, "0");

//            $quebra = 20;
            $quebra = 1;//TERCEIRA LEVA
            $qtdProcComarca = count($processosArr);
            $robo->log("COMARCA $comarca => $qtdProcComarca PROCESSO(S)");
            $qtdLoop = $qtdProcComarca / $quebra;
            if (is_float($qtdLoop)) {
                if ($qtdLoop < 1) {
                    $qtdLoop = 1;
                } else {
                    if ($qtdLoop >= 1) $qtdLoop = (int)$qtdLoop + 1;
                }
            }

            $indexProc = 0;
            for ($i = 0; $i < $qtdLoop; $i++) {
                $processosToSearch = [];
                for ($x = 0; $x < $quebra; $x++) {
                    if (!isset($processosArr[$indexProc])) break;
//                    $processosToSearch[preg_replace('/\D/', '', $robo->processNumberFormatter($processosArr[$indexProc]->getNumProcJust(),'#######-##.####.#.##.####', 20))] = $processosArr[$indexProc];//PRIMEIRA LEVA C/ CNJ
//                    $processosToSearch[preg_replace('/\D/', '', $robo->processNumberFormatter($processosArr[$indexProc]->getNumProcJust(), '#.####.##.######-#/###', 17))] = $processosArr[$indexProc];//SENGUNDA LEVA C/ OUTRO NRO
                    $processosToSearch[$robo->onlyDigits($processosArr[$indexProc]->getNumProcJust())] = $processosArr[$indexProc];//TERCEIRA LEVA SEM MASCARA C/COMARCA NA FRENTE

                    $indexProc++;
                }

                $linkComarca = str_replace('[COMARCA]', $comardaSemZero, $link);
                $linkComarca = str_replace('[LISTA_PROCESSOS]', implode(',', array_keys($processosToSearch)), $linkComarca);
                $driver->navigate()->to($linkComarca);


                $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="captcha_image"]', '//*[@id="captcha_text"]');

                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table.tabela_formulario')));
                } catch (\Exception $e) {
                    $robo->log("NO TABLES");
                    continue;
                }

                $tables = $driver->findElements(WebDriverBy::cssSelector("table.tabela_formulario"));
                $robo->log(count($tables) . " TABELAS ENCONTRADAS");
                foreach ($tables as $table) {

                    $wholeTableCheck = $table->getText();
                    if ($robo->extractProcessNumber($wholeTableCheck) == $wholeTableCheck) {
                        $robo->log("TABLE INVÁLIDA");
                        continue;
                    }

                    $processoSelecionado = '';
                    $vara = '';

                    $cells = $table->findElements(WebDriverBy::tagName('td'));

                    if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[0]->getText())) {
                        $processoSelecionado = $robo->cleanString($cells[0]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                        $vara = $robo->cleanString($cells[1]->getText());
                    } else {
                        if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[1]->getText())) {
                            $processoSelecionado = $robo->cleanString($cells[1]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                            $vara = $robo->cleanString($cells[2]->getText());
                        } else {
                            if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[2]->getText())) {
                                $processoSelecionado = $robo->cleanString($cells[2]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                                $vara = $robo->cleanString($cells[3]->getText());
                            } else {
                                if (preg_match('/NUMERAÇÃO ÚNICA:/', $cells[3]->getText())) {
                                    $processoSelecionado = $robo->cleanString($cells[3]->getText(), ['NUMERAÇÃO ÚNICA: ']);
                                    $vara = $robo->cleanString($cells[4]->getText());
                                }
                            }
                        }
                    }

                    $processoClean = preg_replace('/\D/', '', $processoSelecionado);
                    if (isset($processosToSearch[$processoClean]) && $processoSelecionado != '' && $vara != '') {
                        $robo->addVara($processosToSearch[$processoClean], $vara, false, $driver->getCurrentURL());
                        $alterados++;
                    }
                }
            }
        }


        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $driver->quit();
        return true;
    }

    public function tjmgpjerecursal($processos)
    {
        //TERMINAR MÉTODO - SITE NÃO FUNCIONA - ENVIADO PARA DRA. VALÉRIA CHECAR.
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();

        $robo->log("TRIBUNAL MINAS GERAIS 2° INSTÂNCIA FÍSICO - " . __FUNCTION__);

        $alterados = 0;

        $processosTratados = [];
        $totalProcessos = 0;
        foreach ($processos as $item) {
            $nroProcesso = $robo->cnjNumberFormatter($item->getNumProcJust());
            if (!$nroProcesso) continue;
            if (!preg_match('/.8.13./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) == 0) continue;
            $processosTratados[] = $item;
            $robo->log($nroProcesso);
            $totalProcessos++;
        }

        $robo->log("$totalProcessos PROCESSOS ON TARGET");

        $driver = $robo->startDriver();
        $link = 'https://pjerecursal.tjmg.jus.br/pje/ConsultaPublica/listView.seam';
        $driver->get($link);
        $index = 0;
        $alterados = 0;
        $captcha = '';
        $encontrados = 0;
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);

            $robo->log("{$robo->percentage($totalProcessos,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->setOrigem($driver->getCurrentURL());
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE " . __LINE__ . " => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $robo->log($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE " . __LINE__ . " => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to('https://pje.tjmg.jus.br' . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->log("{$encontrados} PROCESSOS COM JUIZO|VARA");
        $driver->quit();
        return true;
    }


    public function jfpe($processos, $params = ['uf' => 'TRF', 'codeTribunal' => '.4.05.', 'tipoNro' => 'cnj'])
    {
        if (!count($params)) return false;
        /**
         * $params = ['uf'=>string,'codeTribunal'=>string,'tipoNro'=>string]
         */
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $uf = $params['uf'];
        $codeT = $params['codeTribunal'];
        $title = "{$uf} {$codeT} - JUSTIÇA FEDERAL PE";
        $robo->log($title);
        print_r($params);
        $func = __FUNCTION__ . implode('', $params);
        $driver = $robo->startDriver();

        $linkGet = "http://tebas.jfpe.jus.br/consultaProcessos/cons_procs.asp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if (!$nroProcesso) continue;
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
                if (substr($nroProcesso, 0, 4) < 1970 || substr($nroProcesso, 0, 4) > date('Y')) continue;
            }
            if ($params['tipoNro'] == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;
            if ($codeT) if (!preg_match("/$codeT/", $nroProcesso)) continue;

            //if (!in_array(strlen(preg_replace('/\D/', '', $nroProcesso)), [10, 15, 20])) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
            }

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($linkGet);

            $robo->saveCache($func, $processo->getNumProcJust());

//            $nroProcesso = '0000252-93.2013.4.05.8302';//teste

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();

            try {
                $iframe = '//*[@id="ResConsProc"]/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td/table/tbody/tr/td[2]/p/font/span/iframe';
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($iframe)));

                $iframe = $driver->findElement(WebDriverBy::xpath($iframe));
                $driver->switchTo()->frame($iframe);

                //FORMA ERRADA QUE VALÉRIA PEDIU PARA EXTRAIR CONF. PRINT ENVIADO P PROVA DE CAMPO
//                $row = '//*[@id="ResInfoProc"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]';
//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($row)));
//                $conteudoVara = $driver->findElement(WebDriverBy::xpath($row))->getText();
//                $textareaArr = explode("\n", $text);

                $text = $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="Resumo"]')))->getText();
                $textareaArr = explode("\n", $text);

                $vara = '';
                foreach ($textareaArr as $textItem) {
                    if ((preg_match('/a. Vara Federal/i', $textItem) ||
                            preg_match('/a. VARA FEDERAL/i', $textItem) ||
                            preg_match('/ª TURMA RECURSAL/i', $textItem) ||
                            preg_match('/ª Turma Recursal/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        break;
                    }
                }
                if (!$vara) {
                    foreach ($textareaArr as $textItem) {
                        if (preg_match('/a. Vara Federal/i', $textItem) ||
                            preg_match('/ª VF /i', $textItem) ||
                            preg_match('/ª Vara Federal Cível/i', $textItem) ||
                            preg_match('/ª Vara Federal Cível/i', $textItem) ||
                            preg_match('/ª Vara Federal/i', $textItem) ||
                            preg_match('/º Juizado Especial/i', $textItem) ||
                            preg_match('/Juizado Especial Federal/i', $textItem) ||
                            preg_match('/ª Turma Recursal/i', $textItem) ||
                            preg_match('/ÓRGÃO RESP/i', $textItem)) {
                            $vara = $robo->cleanString($textItem, [' - Juiz Titular', '- Juiz Substituto', 'ÓRGÃO RESP : ']);
                            break;
                        }
                    }
                }

                //FORMA ERRADA QUE VALÉRIA PEDIU PARA EXTRAIR CONF. PRINT ENVIADO P PROVA DE CAMPO
//                $textToFind = 'Localização ';
//                if (!preg_match("/$textToFind/", $conteudoVara)) continue;
//                $vara = $robo->cleanString($conteudoVara, [$textToFind]);

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function tjpeprojudi($processos)
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = __FUNCTION__;
        $robo->log($title);
        $driver = $robo->startDriver();

//        $linkGet = "https://www.tjpe.jus.br/projudi/publico/buscas/ProcessosParte";
//        $linkGet = "https://www.tjpe.jus.br/projudi/";
//        $linkGet = "https://www.tjpe.jus.br/projudi/interno.jsp?endereco=/projudi/publico/buscas/ProcessosParte";
        $linkGet = "https://www.tjpe.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[NRO_PROCESSO_SEM_MASCARA]";
        $session = "https://www.tjpe.jus.br/projudi/interno.jsp?endereco=/projudi/publico/buscas/ProcessosParte";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $processo->getNumProcJust();
            if (!$processo->getNumProcJust()) continue;
            //$nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
            if (!$nroProcesso) {
                continue;
//                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if (!$processo->getNumProcJust()) continue;
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) {
                continue;
//                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
//                if (!$nroProcesso) {
//                    $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//                }
            }
            if (!$nroProcesso) continue;

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($session);
            $nroProcesso = ltrim($nroProcesso, '0');
            $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
            $driver->navigate()->to($link);

            $robo->saveCache($title, $nroProcesso);


            try {
                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function tjpe1e2instfisico($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL PERNAMBUCO 1° e 2° INSTÂNCIA FÍSICO';
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://srv01.tjpe.jus.br/consultaprocessualunificada/processo/[NRO_PROCESSO_SEM_MASK]";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            /*$nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            $year = substr($nroProcesso, 11, 4);
            if ($year < 1970 || $year > date('Y')) $nroProcesso = false;
            if (!$nroProcesso) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.#######-#/##', 17);
                $year = substr($nroProcesso, 4, 4);
                if ($year < 1970 || $year > date('Y')) $nroProcesso = false;
            } else {
                if (!preg_match('/.8.17./', $nroProcesso)) continue;
            }
            if (!$nroProcesso) continue;*/

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $processo->getNumProcJust();
            /*$nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            $year = substr($nroProcesso, 11, 4);
            if ($year < 1970 || $year > date('Y')) $nroProcesso = false;
            if (!$nroProcesso) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.#######-#/##', 17);
                $year = substr($nroProcesso, 4, 4);
                if ($year < 1970 || $year > date('Y')) $nroProcesso = false;
            } else {
                if (!preg_match('/.8.17./', $nroProcesso)) continue;
            }*/

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $nroProcesso);
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->get(str_replace('[NRO_PROCESSO_SEM_MASK]', $nroProcesso, $linkGet));

            try {
                $alertErro = 'div.alert-info';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector($alertErro)));
                $text = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector($alertErro))->getText());
                $robo->log($text);
                if (!preg_match('/a imagem de confirmação inválido ou expirado/', $text)) continue;
                $driver->manage()->timeouts()->implicitlyWait(3);
                $driver = $robo->hasCaptcha($driver, 'xpath', '/html/body/div/div[2]/ui-view/section[2]/ng-include/div/div/div/div/form/div[2]/div[2]/img', '//*[@id="captcha"]', '/html/body/div/div[2]/ui-view/section[2]/ng-include/div/div/div/div/form/div[3]/div/button', false, '/html/body/div/div[2]/ui-view/section[2]/ng-include/div/div/div/div/form/div[2]/div[2]/span');

                $driver->manage()->timeouts()->implicitlyWait(3);

            } catch (\Exception $e) {
            }

            try {

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div[id^=accordiongroup-]')));

                $elems = $driver->findElements(WebDriverBy::cssSelector('div[id^=accordiongroup-] > div > div'));
                $vara = '';
                foreach ($elems as $elem) {
                    $text = $elem->getText();
                    if (preg_match('/Orgão Julgador/', $text)) {
                        $vara = $robo->cleanString($text, ['Orgão Julgador']);
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpepje2inst($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();

        $title = "TRIBUNAL PERNAMBUCO 2° INSTÂNCIA PJE - ";
        $robo->log($title);

        $alterados = 0;

        $processosTratados = [];
        $totalProcessos = 0;
        foreach ($processos as $item) {
            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(), '#######-##.####.#.##.####', 20);
            $nroProcesso = $robo->cnjNumberFormatter($nroProcesso);
            if (!preg_match('/.8.17./', $nroProcesso)) continue;
            $processosTratados[] = $item;
            $robo->log($nroProcesso);
            $totalProcessos++;
        }

        $robo->log("$totalProcessos PROCESSOS ON TARGET");

        $driver = $robo->startDriver();
        $link = 'https://pje.tjpe.jus.br/2g/ConsultaPublica/listView.seam';
        $driver->get($link);
        $index = 0;
        $alterados = 0;
        $captcha = '';
        $encontrados = 0;
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);

            $robo->log("{$robo->percentage($totalProcessos,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $nroProcesso);

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE " . __LINE__ . " => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $erroMsg = $robo->cleanString($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $robo->log($erroMsg);
                if (!preg_match('/captcha/', $erroMsg)) continue;
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE " . __LINE__ . " => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($driver->getCurrentURL()) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpepje1inst($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();

        $title = "TRIBUNAL PERNAMBUCO 1° INSTÂNCIA PJE - ";
        $robo->log($title);

        $processosTratados = [];
        $totalProcessos = 0;
        foreach ($processos as $item) {
            $nroProcesso = $robo->processNumberFormatter($item->getNumProcJust(), '#######-##.####.#.##.####', 20);
            $nroProcesso = $robo->cnjNumberFormatter($nroProcesso);
            if (!preg_match('/.8.17./', $nroProcesso)) continue;
            $processosTratados[] = $item;
            $robo->log($nroProcesso);
            $totalProcessos++;
        }

        $robo->log("$totalProcessos PROCESSOS ON TARGET");

        $driver = $robo->startDriver();
        $link = 'https://pje.tjpe.jus.br/1g/ConsultaPublica/listView.seam';
        $driver->get($link);
        $index = 0;
        $captcha = '';
        $encontrados = 0;
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);

            $robo->log("{$robo->percentage($totalProcessos,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $nroProcesso);

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE " . __LINE__ . " => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $erroMsg = $robo->cleanString($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $robo->log($erroMsg);
                if (!preg_match('/captcha/', $erroMsg)) continue;
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE " . __LINE__ . " => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($driver->getCurrentURL()) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpe2instfisicoantigo($processos)
    {
        $comarcaOptions = '<option value="0000">Tribunal de Justiça - 0000</option><option value="0001">Recife - 0001</option><option value="0100">Abreu e Lima - 0100</option><option value="0110">Afogados da Ingazeira - 0110</option><option value="0120">Afrânio - 0120</option><option value="0130">Agrestina - 0130</option><option value="0140">Água Preta - 0140</option><option value="0150">Águas Belas - 0150</option><option value="0160">Alagoinha - 0160</option><option value="0170">Aliança - 0170</option><option value="0180">Altinho - 0180</option><option value="0190">Amaraji - 0190</option><option value="0200">Angelim - 0200</option><option value="0210">Araripina - 0210</option><option value="0220">Arcoverde - 0220</option><option value="0230">Barreiros - 0230</option><option value="0240">Belém de Maria - 0240</option><option value="0250">Belém do São Francisco - 0250</option><option value="0260">Belo Jardim - 0260</option><option value="0270">Betânia - 0270</option><option value="0280">Bezerros - 0280</option><option value="0290">Bodocó - 0290</option><option value="0300">Bom Conselho - 0300</option><option value="0310">Bom Jardim - 0310</option><option value="0320">Bonito - 0320</option><option value="0330">Brejão - 0330</option><option value="0340">Brejo da Madre de Deus - 0340</option><option value="0350">Buenos Aires - 0350</option><option value="0360">Buíque - 0360</option><option value="0370">Cabo de Santo Agostinho - 0370</option><option value="0380">Cabrobó - 0380</option><option value="0390">Cachoeirinha - 0390</option><option value="0400">Caetés - 0400</option><option value="0410">Calçado - 0410</option><option value="0420">Camaragibe - 0420</option><option value="0430">Camocim de São Félix - 0430</option><option value="0440">Canhotinho - 0440</option><option value="0450">Capoeiras - 0450</option><option value="0460">Carnaíba - 0460</option><option value="0470">Carpina - 0470</option><option value="0480">Caruaru - 0480</option><option value="0490">Catende - 0490</option><option value="0500">Chã Grande - 0500</option><option value="0510">Condado - 0510</option><option value="0520">Correntes - 0520</option><option value="0530">Cortês - 0530</option><option value="0540">Cumaru - 0540</option><option value="0550">Cupira - 0550</option><option value="0560">Custódia - 0560</option><option value="0570">Escada - 0570</option><option value="0580">Exu - 0580</option><option value="0590">Feira Nova - 0590</option><option value="0600">Ferreiros - 0600</option><option value="0610">Flores - 0610</option><option value="0620">Floresta - 0620</option><option value="0630">Gameleira - 0630</option><option value="0640">Garanhuns - 0640</option><option value="0650">Glória do Goitá - 0650</option><option value="0660">Goiana - 0660</option><option value="0670">Gravatá - 0670</option><option value="0680">Iati - 0680</option><option value="0690">Ibimirim - 0690</option><option value="0700">Ibirajuba - 0700</option><option value="0710">Igarassu - 0710</option><option value="0720">Inajá - 0720</option><option value="0730">Ipojuca - 0730</option><option value="0740">Ipubi - 0740</option><option value="0750">Itaiba - 0750</option><option value="0760">Itamaracá - 0760</option><option value="0770">Itambé - 0770</option><option value="0780">Itapetim - 0780</option><option value="0790">Itapissuma - 0790</option><option value="0800">Itaquitinga - 0800</option><option value="0810">Jaboatão dos Guararapes - 0810</option><option value="0820">Jataúba - 0820</option><option value="0830">João Alfredo - 0830</option><option value="0840">Joaquim Nabuco - 0840</option><option value="0850">Jupi - 0850</option><option value="0860">Jurema - 0860</option><option value="0870">Lagoa do Itaenga - 0870</option><option value="0880">Lagoa do Ouro - 0880</option><option value="0890">Lagoa dos Gatos - 0890</option><option value="0900">Lagoa Grande - 0900</option><option value="0910">Lajedo - 0910</option><option value="0920">Limoeiro - 0920</option><option value="0930">Macaparana - 0930</option><option value="0940">Maraial - 0940</option><option value="0950">Mirandiba - 0950</option><option value="0960">Moreilandia - 0960</option><option value="0970">Moreno - 0970</option><option value="0980">Nazaré da Mata - 0980</option><option value="0990">Olinda - 0990</option><option value="1000">Orobó - 1000</option><option value="1010">Orocó - 1010</option><option value="1020">Ouricuri - 1020</option><option value="1030">Palmares - 1030</option><option value="1040">Palmeirina - 1040</option><option value="1050">Panelas - 1050</option><option value="1060">Parnamirim - 1060</option><option value="1070">Passira - 1070</option><option value="1080">Paudalho - 1080</option><option value="1090">Paulista - 1090</option><option value="1100">Pedra - 1100</option><option value="1110">Pesqueira - 1110</option><option value="1120">Petrolândia - 1120</option><option value="1130">Petrolina - 1130</option><option value="1140">Poção - 1140</option><option value="1150">Pombos - 1150</option><option value="1160">Primavera - 1160</option><option value="1170">Quipapá - 1170</option><option value="1180">Riacho das Almas - 1180</option><option value="1190">Ribeirão - 1190</option><option value="1200">Rio Formoso - 1200</option><option value="1210">Sairé - 1210</option><option value="1220">Salgueiro - 1220</option><option value="1230">Saloá - 1230</option><option value="1240">Sanharó - 1240</option><option value="1250">Santa Cruz do Capibaribe - 1250</option><option value="1260">Santa Maria da Boa Vista - 1260</option><option value="1270">Santa Maria do Cambucá - 1270</option><option value="1280">São Bento do Una - 1280</option><option value="1290">São Caetano - 1290</option><option value="1300">São João - 1300</option><option value="1310">São Joaquim do Monte - 1310</option><option value="1320">São José da Coroa Grande - 1320</option><option value="1330">São José do Belmonte - 1330</option><option value="1340">São José do Egito - 1340</option><option value="1350">São Lourenço da Mata - 1350</option><option value="1360">São Vicente Férrer - 1360</option><option value="1370">Serra Talhada - 1370</option><option value="1380">Serrita - 1380</option><option value="1390">Sertânia - 1390</option><option value="1400">Sirinhaém - 1400</option><option value="1410">Surubim - 1410</option><option value="1420">Tabira - 1420</option><option value="1430">Tacaimbó - 1430</option><option value="1440">Tacaratu - 1440</option><option value="1450">Tamandaré - 1450</option><option value="1460">Taquaritinga do Norte - 1460</option><option value="1470">Terra Nova - 1470</option><option value="1480">Timbaúba - 1480</option><option value="1490">Toritama - 1490</option><option value="1500">Tracunhaém - 1500</option><option value="1510">Trindade - 1510</option><option value="1520">Triunfo - 1520</option><option value="1540">Tuparetama - 1540</option><option value="1550">Venturosa - 1550</option><option value="1560">Verdejante - 1560</option><option value="1570">Vertentes - 1570</option><option value="1580">Vicência - 1580</option><option value="1590">Vitória de Santo Antão - 1590</option>';
        $doc = new \DOMDocument();
        $doc->loadHTML($comarcaOptions);
        $comarcas = [];
        foreach ($doc->getElementsByTagName('option') as $comarca) {
            $code = str_pad($comarca->getAttribute('value'), 4, 0, STR_PAD_LEFT);
            $comarcas[$code] = $comarca->nodeValue;
        }
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL PERNAMBUCO 2° INSTÂNCIA FÍSICO';
        $robo->log($title);
        $driver = $robo->startDriver();

//        $linkGet = "http://www.tjpe.jus.br/processos/consulta2grau/ole_busca_processos2.asp";
        $linkGet = "http://www.tjpe.jus.br/processos/consulta2grau/ole_busca_processos_numero2.asp?nume=[NRO_PROCESSO_SEM_MASK]";
        $driver->get($robo->host);
        $driver->get("http://www.tjpe.jus.br/processos/consulta2grau/ole_busca_processos2.asp");

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            if (!$nroProcesso) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#####.#/##', 8);
            } else {
                if (!preg_match('/.8.17./', $nroProcesso)) continue;
            }
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if (!$processo->getNumProcJust()) continue;
            $tipoNro = 'cnj';
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######.##.####.#.##.####', 20);
            if (!$nroProcesso) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#####.#.##', 8);
                $tipoNro = 'antigo';
            }
            $number = explode('.', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $nroProcesso);
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->get(str_replace('[NRO_PROCESSO_SEM_MASK]', $nroProcesso, $linkGet));


            try {
                $rowsElem = '/html/body/center/form/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td/table/tbody/tr';
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($rowsElem)));
                $rowsElems = $driver->findElements(WebDriverBy::xpath($rowsElem));
                $trash = array_shift($rowsElems);//RETIRA A PRIMEIRA LINHA
                $dados = $robo->extractVerticalTable($rowsElems, 2);
                $vara = '';
                if (isset($dados['OrgaoJulgador']) && $dados['OrgaoJulgador'] != '') {
                    $vara = $robo->cleanString($dados['OrgaoJulgador'], ['; ', ';']);
                } else {
                    if (isset($dados['Vara']) && $dados['Vara'] != '') {
                        $vara = $robo->cleanString($dados['Vara'], ['; ', ';']);
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpejuizadoespecial($processos)
    {
        //http://www.tjpe.jus.br/juizado/busca_processos_numero.asp?codg_juizado=8112;;12%C2%BA%20Juizado%20Esp.C%C3%ADvel%20e%20das%20Rel.Consumo%20da%20Capital&codg_proc=003886&codg_sub_proc=00&ano=2004
        //http://www.tjpe.jus.br/juizado/busca_processos_numero2.asp?codg_juizado=8111&codg_proc=0038862004
        //http://www.tjpe.jus.br/juizado/consulta.asp

        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL PERNAMBUCO - JUIZADO ESPECIAL';
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://www.tjpe.jus.br/juizado/consulta.asp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;

            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);

            if (!$nroProcesso) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '######/####-##', 12);
                if ($nroProcesso) {
                    if (!$robo->yearValidate($nroProcesso, 7)) continue;
                }
            } else {
                if (!preg_match('/.8.17./', $nroProcesso)) continue;
                if (!$robo->yearValidate($nroProcesso)) continue;
            }
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if (!$processo->getNumProcJust()) continue;
            $tipoNro = 'cnj';
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######.##.####.###.####', 20);
            if (!$nroProcesso) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '######.####.##', 12);
                $tipoNro = 'antigo';
            }

            $number = explode('.', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $nroProcesso);

            $driver->get($linkGet);

            try {

                if ($tipoNro == 'cnj') {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="tx1"]'))->sendKeys($number[0]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="tx2"]'))->sendKeys($number[1]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="tx3"]'))->sendKeys($number[2]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="tx6"]'))->sendKeys($number[4]);

                    while (true) {
                        try {
                            $driver = $robo->hasCaptcha($driver, 'xpath',
                                '//*[@id="trBusca1"]/table[2]/tbody/tr[7]/td/div/img',
                                '//*[@id="trBusca1"]/table[2]/tbody/tr[7]/td/div/input', '//*[@id="trBusca1"]/table[2]/tbody/tr[4]/td/input[1]', false);

                            $msg = $robo->cleanString($driver->findElement(WebDriverBy::className('msgerro'))->getText());
                            $robo->log($msg);
                            if (preg_match('/Código inválido. Tente novamente./', $msg)) continue;
                        } catch (\Exception $e) {
                            break;
                        }
                    }
                } else {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumero"]'))->click();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="codg_proc"]'))->sendKeys($number[0]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="ano"]'))->sendKeys($number[1]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="codg_sub_proc"]'))->sendKeys($number[2]);

                    while (true) {
                        try {
                            $driver = $robo->hasCaptcha($driver, 'xpath',
                                '//*[@id="trBusca2"]/table[2]/tbody/tr[5]/td/div/img',
                                '//*[@id="trBusca2"]/table[2]/tbody/tr[5]/td/div/input', '//*[@id="trBusca2"]/table[2]/tbody/tr[3]/td[7]/input[1]', false);

                            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumero"]'))->click();

                            $msg = $robo->cleanString($driver->findElement(WebDriverBy::className('msgerro'))->getText());
                            $robo->log($msg);
                            if (preg_match('/Código inválido. Tente novamente./', $msg)) continue;
                        } catch (\Exception $e) {
                            break;
                        }
                    }
                }

                //CAPTURAR DADO
                $vara = '';
                $linhasElemText = '/html/body/table/tbody/tr/td/table/tbody/tr[7]/td/table/tbody/tr/td[1]/table[2]/tbody/tr';
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($linhasElemText)));

                $linhasElems = $driver->findElements(WebDriverBy::xpath($linhasElemText));
                $dados = $robo->extractVerticalTable($linhasElems);

                if (isset($dados['Turma']) && $dados['Turma'] != '') {
                    $vara = $robo->cleanString($dados['Turma'], ['; ', ';']);
                    if ($vara != '') {
                        $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                        $encontrados++;
                    }
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    /*GOIÁS - GO*/
    public function tjgoprojudi($processos, $params = ['tipoNro' => 'cnj'])
    {
        if (!count($params)) return false;
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL GOIÁS - PROJUDI';

        $tipoNro = $params['tipoNro'];
        $func = __FUNCTION__ . $tipoNro;
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://projudi.tjgo.jus.br/BuscaProcessoPublica?PaginaAtual=4&TipoConsulta=4";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
                if (!preg_match('/8.09/',$nroProcesso)) continue;
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
                if ($nroProcesso) {
                    if (!$robo->yearValidate($nroProcesso, 4)) continue;
                }
            }

            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######.##|####.#.##.####');
                $nroProcessoArr = explode('|', $nroProcesso);
                $nroProcesso = $nroProcessoArr[0];
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####|######|#', 14);
                $nroProcessoArr = explode('|', $nroProcesso);
                $nroProcesso = $nroProcessoArr[1];
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => {$processo->getNumProcJust()}");
                continue;
            }
            $robo->saveCache($func, $processo->getNumProcJust());

            try {

                //CAPTCHA
                try {
                    $toContinue = false;
                    while (true) {
                        $driver->get($linkGet);
                        $driver->executeScript('$("body").css("font-size","0px");$("#divEditar > fieldset > p:nth-child(4)").remove();$("#divCaptcha > img").css("width","150px");', []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="ProcessoNumero"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="ProcessoNumero"]'))->sendKeys($nroProcesso);

                        $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="divCaptcha"]/img', '//*[@id="textoDigitado"]', '//*[@id="divBotoesCentralizados"]/input[1]', false);

                        $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.ui-dialog')));
                        $msg = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('div.ui-dialog'))->getText());
                        $robo->log($msg);
                        if (!preg_match('/Código digitado não confere/', $msg)) {
                            $toContinue = true;
                            break;
                        }
                    }
                    if ($toContinue) continue;
                } catch (\Exception $e) {
                }


                $conteudoElem = '.VisualizaDados > .VisualizaDados > .VisualizaDados';
                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector($conteudoElem)));
                } catch (\Exception $e) {
                    $conteudoElem = '.VisualizaDados';
                    try {
                        $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector($conteudoElem)));
                    } catch (\Exception $e) {
                        $robo->log("PROCESSO NÃO ENCONTRADO" . __LINE__);
                        continue;
                    }
                }

                $driver->executeScript('$("#Formulario > img").remove();', []);

                $fieldsets = $driver->findElements(WebDriverBy::cssSelector($conteudoElem));

                $robo->log(count($fieldsets) . ' .VisualizaDados ENCONTRADOS!!!');

                $vara = '';
                foreach ($fieldsets as $fieldset) {
                    $divs = $fieldset->findElements(WebDriverBy::tagName('div'));
                    $spans = $fieldset->findElements(WebDriverBy::tagName('span'));

//                    if (!preg_match('/OUTRAS INFORMAÇÕES/i', $fieldset->getText())) continue;
                    if (!preg_match('/Serventia Origem/', $fieldset->getText())) continue;

                    $indexFound = 0;
                    foreach ($divs as $div) {
                        $textToFind = 'Serventia Origem';
                        $robo->log($div->getText());
                        if (preg_match("/$textToFind/i", $div->getText())) {
                            break;
                        } else {
                            $textToFind = 'Serventia';
                            if (preg_match("/$textToFind/i", $div->getText())) {
                                break;
                            }
                        }
                        $indexFound++;
                    }
                    if (isset($spans[$indexFound])) {
                        $vara = $robo->cleanString($spans[$indexFound]->getText());
                    }
                    break;
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log($e->getMessage());
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function tjgopjd($processos, $params = ['tipoNro' => 'cnj'])
    {
        if (!count($params)) return false;
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL GOIÁS - PJD';

        $tipoNro = $params['tipoNro'];
        $func = __FUNCTION__ . $tipoNro;
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pjd.tjgo.jus.br/";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
                if (strlen($nroProcesso) != 25) continue;
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
                if ($nroProcesso) {
                    if (!$robo->yearValidate($nroProcesso, 4)) continue;
                }
            }

            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
                $nroProcesso = $robo->processNumberFormatter($nroProcesso, '#######.##|####.#.##.####', 20);
                $nroProcessoArr = explode('|', $nroProcesso);
                $nroProcesso = $nroProcessoArr[0];
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####|######|#', 14);
                $nroProcessoArr = explode('|', $nroProcesso);
                $nroProcesso = $nroProcessoArr[1];
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => {$processo->getNumProcJust()}");
                continue;
            }
            $robo->saveCache($func, $processo->getNumProcJust());

            $driver->get($linkGet);

            try {

                $driver->executeScript("javascript:abrirMenu('./menuPublico/consultaProcessualPublica.jsp','conteudo', setarFocoNoMenuItens);", []);
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ProcessoNumero"]')));

                if ($tipoNro == 'antigo') {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="opNumeracao2"]'))->click();
                }
                $driver->findElement(WebDriverBy::xpath('//*[@id="ProcessoNumero"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="ProcessoNumero"]'))->sendKeys($nroProcesso);
                $driver->executeScript('$("body").scrollTop(1000);', []);
//                $driver->executeScript('$("form").submit();',[]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="btnBuscarProcPublico"]'))->click();

                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="mensagem-conteudo"]')));
                    $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="mensagem-conteudo"]'))->getText());
                    if (preg_match('/Nenhum Processo foi localizado para os parâmetros informado/', $msg)) {
                        $robo->log($msg);
                        continue;
                    }
                } catch (\Exception $e) {
                }

                //CAPTCHA
                try {
                    $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="FormCaptcha"]/div[2]/div/div/div/iframe')));
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                    $driver->executeScript("confirmaConsulta();", []);
                } catch (\Exception $e) {
                }


                $conteudoElem = 'infoProcesso';
                $vara = '';
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className($conteudoElem)));

                $conteudo = $driver->findElement(WebDriverBy::className($conteudoElem));
                $dts = $conteudo->findElements(WebDriverBy::tagName('dt'));
                $dds = $conteudo->findElements(WebDriverBy::tagName('dd'));

                foreach ($dts as $i => $dt) {
                    if (preg_match('/Serventia/', $dt->getText())) {
                        $vara = $robo->cleanString($dds[$i]->getText());
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }


            } catch (\Exception $e) {
                $robo->log($e->getMessage());
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function tjgo2instfisico($processos, $params = ['tipoNro' => 'cnj'])
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL GOIÁS - 2° INSTÂNCIA FÍSICO';

        $tipoNro = $params['tipoNro'];
        $func = __FUNCTION__ . $tipoNro;
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://sv-natweb-p00.tjgo.jus.br/ssg/Consulta_Proc.php";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if ($nroProcesso) {
                    if (!$robo->yearValidate($nroProcesso, 11)) continue;
                }
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
                if ($nroProcesso) {
                    if (!$robo->yearValidate($nroProcesso, 0)) continue;
                }
            }

            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######.##.####|#.##.####', 20);
                $nroProcessoArr = explode('|', $nroProcesso);
                $nroProcesso = $nroProcessoArr[0];
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => {$processo->getNumProcJust()}");
                continue;
            }
            $robo->saveCache($func, $processo->getNumProcJust());

            $driver->get($linkGet);

            try {

                $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/form/div/table[1]/tbody/tr[2]/td/table/tbody/tr/td[2]/input[1]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/form/div/table[2]/tbody/tr/td/input'))->click();

                $googlekey = $driver->findElement(WebDriverBy::cssSelector('div.g-recaptcha'))->getAttribute('data-sitekey');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$googlekey}\";", []);
                $driver->findElement(WebDriverBy::xpath('/html/body/form/input[6]'))->click();


                try {
                    $driver->wait(2)->until(
                        WebDriverExpectedCondition::alertIsPresent(),
                        'Waiting for an alert'
                    );
                    if (preg_match('/NAO DISPONIVEL/', $driver->switchTo()->alert()->getText())) {
                        $driver->switchTo()->alert()->accept();
                        continue;
                    }
                    $driver->switchTo()->alert()->accept();
                } catch (\Exception $e) {
                }

                $driver->wait(2)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table tbody tr.destaque td a'))
                );

                $rowsPrincipal = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr'));

                $trash = array_shift($rowsPrincipal);
                $header = array_shift($rowsPrincipal);

                $auxPrincipal = $robo->extractVerticalTable($rowsPrincipal);

                debug($auxPrincipal);


                $vara = '';
                if ($vara != '') {
                    $robo->addVara($processo, $vara, true, $driver->getCurrentURL());
                    $encontrados++;
                }


            } catch (\Exception $e) {
                $robo->log($e->getMessage());
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function tjgo1instfisico($processos, $params = ['tipoNro' => 'cnj'])
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL GOIÁS - 1° INSTÂNCIA FÍSICO';

        $tipoNro = $params['tipoNro'];
        $localidade = $params['localidade'];
        $func = __FUNCTION__ . $tipoNro;
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://sv-natweb-p00.tjgo.jus.br/spg/Consulta_Proc.php";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if ($nroProcesso) {
                    if (!$robo->yearValidate($nroProcesso, 11)) continue;
                }
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
                if ($nroProcesso) {
                    if (!$robo->yearValidate($nroProcesso, 0)) continue;
                }
            }

            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######.##.####|#.##.####', 20);
                $nroProcessoArr = explode('|', $nroProcesso);
                $nroProcesso = $nroProcessoArr[0];
            }

            if ($tipoNro == 'antigo') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => {$processo->getNumProcJust()}");
                continue;
            }
            $robo->saveCache($func, $processo->getNumProcJust());

            $driver->get($linkGet);

            try {
                if (preg_match('/Serviço indisponível ou sobrecarregado/', $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="faixa"]'))->getText()))) {
                    $driver->quit();
                    $driver = $robo->startDriver(false, true);
                    $driver->get($linkGet);
                }
            } catch (\Exception $e) {
            }

            try {

                if ($localidade == 'interior') {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/form/div/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td[1]/input[2]'))->click();
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/form/div/table[1]/tbody/tr[1]/td/table/tbody/tr[3]/td[2]/input[1]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="container"]/form/div/table[2]/tbody/tr/td/input'))->click();

                $googlekey = $driver->findElement(WebDriverBy::cssSelector('div.g-recaptcha'))->getAttribute('data-sitekey');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$googlekey}\";", []);
                $driver->findElement(WebDriverBy::xpath('/html/body/form/input[6]'))->click();

                try {
                    $driver->wait(2)->until(
                        WebDriverExpectedCondition::alertIsPresent(),
                        'Waiting for an alert'
                    );
                    if (preg_match('/NAO DISPONIVEL/', $driver->switchTo()->alert()->getText())) {
                        $driver->switchTo()->alert()->accept();
                        continue;
                    }
                    $driver->switchTo()->alert()->accept();
                } catch (\Exception $e) {
                }

                $driver->wait(2)->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table tbody tr.destaque td a'))
                );

                $rowsPrincipal = $driver->findElements(WebDriverBy::xpath('//*[@id="container"]/table/tbody/tr'));

                $trash = array_shift($rowsPrincipal);
                $header = array_shift($rowsPrincipal);

                $auxPrincipal = $robo->extractVerticalTable($rowsPrincipal);

                if (isset($auxPrincipal['Comarca/Escrivania']) && $auxPrincipal['Comarca/Escrivania'] != '') {
                    $vara = $robo->cleanString($auxPrincipal['Comarca/Escrivania'], ['; ', ';']);
                    if ($vara != '') {
                        $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                        $encontrados++;
                    }
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }


    public function tjprsnu($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("TRIBUNAL PARANÁ - SISTEMA DE NUMERAÇÃO ÚNICA");
//        $robo->proxy = $robo->getBestProxy();
//        $driver = $robo->startDriver(false, true);
        $driver = $robo->startDriver(false, false, false);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.16/', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) != '0') continue;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);

        $robo->log("$total PROCESSOS ON TARGET");

        $linkGet = 'https://portal.tjpr.jus.br/snu';
        $driver->get($robo->host);

        $index = 0;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {
            $driver->get($linkGet);
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroUnico"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroUnico"]'))->sendKeys($nroProcesso);

            try {
                $driver->findElement(WebDriverBy::xpath('//*[@id="internetForm"]/table[4]/tbody/tr/td[2]/button'))->click();
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[2]/div[2]/iframe')));
                $robo->log("CAPTCHA!!!");
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("onSubmit();", []);
//                $driver->executeScript("document.getElementsByName(\"internetForm\")[0].submit();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="errorMessages"]')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]'))->getText());
                $robo->log($msg);
                if (!preg_match('/Verificação do Captcha inválida/', $msg)) {
                    $robo->saveCache(__FUNCTION__, $nroProcesso);
                    continue;
                }

                $driver->findElement(WebDriverBy::xpath('//*[@id="internetForm"]/table[4]/tbody/tr/td[2]/button'))->click();
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[2]/div[2]/iframe')));
                $robo->log("CAPTCHA!!!");
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("onSubmit();", []);
//                $driver->executeScript("document.getElementsByName(\"internetForm\")[0].submit();", []);

                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="errorMessages"]')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]'))->getText());
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            //TRATANDO ERRO DE PESQUISA
            try {
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="content"]'))->getText());
                if (preg_match('/Não foi possível realizar a operação devido ao seguinte erro/', $msg)) {
                    $robo->log($msg);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="content"]/div[2]/input'))->click();
                    continue;
                }
            } catch (\Exception $e) {
            }

            $vara = '';
            $array = [];
            $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="processoForm"]/fieldset/table[2]')));
            $conteudoProcesso = $driver->findElement(WebDriverBy::xpath('//*[@id="processoForm"]/fieldset/table[2]'))->getText();
            $textToFind = 'Órgão Julgador:';
            $array = explode($textToFind, $conteudoProcesso);
            if (count($array) < 2) {
                $textToFind = 'Juízo:';
                $array = explode($textToFind, $conteudoProcesso);
                if (count($array) < 2) {
                    $robo->log("PROCESSO NÃO ENCONTRADO");
                    continue;
                }
            } else {
                $robo->log('##############################');
                $robo->log(substr($array[1], 0, strpos($array[1], "\n")));
                $robo->log('##############################');
                continue;
            }

            $robo->log("FOUND AT $textToFind");
            $vara = substr($array[1], 0, strpos($array[1], "\n"));
            $vara = $robo->cleanString($vara, [': ', ':']);
            if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
            $encontrados++;

            //$driver->findElement(WebDriverBy::xpath('//*[@id="backButton"]'))->click();
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    /**
     * Esse site para de solicitar o preenchimento do captcha às 20:05
     *
     * @param type $processos
     * @return boolean
     */
    public function tjprfederal($processos)
    {
        $robo = new Robo();
        $alterados = 0;
        $driver = $robo->startDriver();
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            $code = str_replace(
                ['.', '-', '/', ' ', ','],
                ['', ',', '', '', ''],
                $processo->getNumProcJust()
            );

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $url = "https://www2.trf4.jus.br/trf4/controlador.php?"
                . "acao=consulta_processual_resultado_pesquisa&"
                . "txtValor={$code}&"
                . "selOrigem=PR&"
                . "chkMostrarBaixados=S&"
                . "selForma=NU&"
                . "txtDataFase=01/01/1970&"
                . "hdnRefId=&"
                . "txtPalavraGerada=";

            $driver->get($url);

            try {
                $driver->switchTo()->alert()->accept();
                $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
            } catch (NoAlertOpenException $e) {
                $array = explode(
                    'Órgão Julgador: ',
                    $driver->findElement(WebDriverBy::xpath('//*[@id="divConteudo"]'))->getText()
                );
                $orgaoJulgador = substr($array[1], 0, strpos($array[1], "\n"));
                $robo->log($orgaoJulgador);
                $processo->setOrigem($driver->getCurrentURL());
                $robo->addVara($processo, $orgaoJulgador, false, $driver->getCurrentURL());
                $alterados++;
            } catch (NoSuchElementException $e) {
                $robo->log("NAO FOI POSSIVEL ENCONTRAR O ELEMENTO {$url}");
            } catch (TimeOutException $e) {
                $robo->log("Timeout ao acessar {$url}");
            } catch (\Exception $e) {
                $robo->log("ERRO CATASTROFICO {$url}");
            }
            sleep(5);
//            $driver->quit();
        }
        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    public function projuditjpr($processos, $params = [])
    {
        if (!count($params)) return false;
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("TRIBUNAL PARANÁ - PROJUDI 1° E 2° INSTÂNCIA");
        $driver = $robo->startDriver();

        $instancia = $params['instancia'];
        $func = __FUNCTION__ . $instancia . 'instancia';

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            if ($nroProcesso) {
                if (!preg_match('/.8.16/', $nroProcesso)) continue;
                if (substr($nroProcesso, 0, 1) != 0) continue;
            } else {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
                if (!$nroProcesso) continue;
                $year = substr($nroProcesso, 4, 4);
                if ($year < 1970 || $year > date('Y')) continue;
            }

            if ($robo->cacheExists($func, $nroProcesso)) continue;
            $processosTratados[] = $processo;

        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 0;
        $encontrados = 0;
        $linkGet = 'https://projudi.tjpr.jus.br/projudi_consulta/processo/consultaPublica.do?actionType=iniciar';
        foreach ($processosTratados as $processo) {
            try {

                $cnj = true;
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if (!$nroProcesso) {
                    $cnj = false;
                    $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
                }

                $robo->log("{$robo->percentage($total, $index)} => {$nroProcesso}");
                $index++;

                if ($robo->cacheExists($func, $nroProcesso)) continue;

                while (true) {
                    $driver->get($linkGet);

                    //1° INSTÂNCIA
                    if ($instancia == '1') {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/fieldset/table/tbody/tr[3]/td[2]/table/tbody/tr/td[1]/input'))->click();
                    } else {
                        //2° INSTÂNCIA
                        $driver->findElement(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/fieldset/table/tbody/tr[3]/td[2]/table/tbody/tr/td[2]/input'))->click();
                    }

                    if ($cnj) {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="divFiltroNumeroProcesso"]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[1]/input'))->click();
                    } else {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="divFiltroNumeroProcesso"]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/input'))->click();
                    }

                    $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->sendKeys($nroProcesso);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/table[1]/tbody/tr/td[2]/button[1]'))->click();

                    try {
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '/html/body/div[2]/div[2]/iframe');
                        if (!$captcha) {
                            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                        }
                        $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->executeScript("onSubmit();", []);

                        $notFound = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]/div[3]/div/ul/li'))->getText());
                        $robo->log($notFound);
                        if (!preg_match('/Verificação do Captcha inválida/', $notFound)) break;
                    } catch (\Exception $e) {
                        break;
                    }
                    $robo->log("TRYING AGAIN");
                }

                $robo->saveCache($func, $processo->getNumProcJust());

                try {
                    $notFound = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]/div[3]/div/ul/li'))->getText());
                    continue;
                } catch (\Exception $e) {
                }

                try {
                    //SE APARECER MAIS DE UM PROCESSO
                    $linksProcesso = $driver->findElements(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/table[2]/tbody/tr/td[1]/a'));
                    if (is_array($linksProcesso) && count($linksProcesso) > 0) {
                        $lastElem = end($linksProcesso);
                        $lastElem->click();
                    }
                } catch (\Exception $e) {
                }

                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="tabItemprefix0"]')));
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabItemprefix0"]'))->click();

                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="tabprefix0"]/fieldset/table/tbody/tr')));

                $vara = '';
                $delimiter = 'Órgão Julgador';
                if ($instancia == '1') {
                    $delimiter = 'Juízo';
                }
                $linhas = $driver->findElements(WebDriverBy::xpath('//*[@id="tabprefix0"]/fieldset/table/tbody/tr'));
                $linhasencontradas = count($linhas);
                $varaEncontrada = false;
                foreach ($linhas as $linha) {
                    $cells = $linha->findElements(WebDriverBy::tagName('td'));
                    foreach ($cells as $cell) {
                        $cellText = $cell->getText();
                        if ($varaEncontrada) {
                            $vara = $cellText;
                            break;
                        }
                        if (preg_match("/{$delimiter}/", $cellText)) {
                            $varaEncontrada = true;
                        }
                    }
                    if ($varaEncontrada) break;
                }

                /**
                 * OLD WAY
                 */
                /*$array = explode($delimiter, $driver->findElement(WebDriverBy::xpath('//*[@id="tabprefix0"]'))->getText());
                if (count($array) < 1) continue;
                $vara = substr($array[1], 0, strpos($array[1], "\n"));*/

                $vara = $robo->cleanString($vara);
                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                $encontrados++;

            } catch (\Exception $e) {
                $robo->log($e->getMessage());
            }
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
//        $robo->clearCache($func);
        return true;
    }

    public function tjpjetjpr($processos)
    {
        //https://pje.tjpr.jus.br/pje/ConsultaPublica/listView.seam
        // Foi feito manualmente a extração da vara
        // Foram encontrados dois processos
        // O padrão começa com 5 e possui o identificador 8.16
    }

    public function justicaweb($processos)
    {
        $robo = new Robo();
        $alterados = 0;
        $driver = $robo->startDriver();

        $total = count($processos);
        $index = 1;
        foreach ($processos as $processo) {

            if (is_null($processo->getNumProcJust())) continue;
            $code = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$code}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $code)) continue;

            $livros = ['1', '2', '3', '4', '5', 'NOSEL'];
            foreach ($livros as $livro) {
                $url = "http://187.18.102.108/consultx.exe?"
                    . "LIVRO={$livro}&"
                    . "Processo={$code}&"
                    . "Campo=1";

                $driver->get($url);

                $vara = '';
                try {
                    $notFound = $driver->findElement(WebDriverBy::xpath('/html/body'))->getText();
                    if ($notFound != "Processo Não Encontrado") {
                        throw new NoSuchElementException('Encontrei um processo');
                    }
                    $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                } catch (NoSuchElementException $e) {
                    $vara = $robo->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td[2]/table[2]/tbody/tr[1]/td[2]/span/b'))->getText());
                    if ($vara != '') {
                        $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                        $alterados++;
                        break;
                    }
                } catch (TimeOutException $e) {
                    $robo->log("Timeout ao acessar {$url}");
                } catch (\Exception $e) {
                    $robo->log("ERRO CATASTROFICO {$url}");
                }
            }


        }

        $driver->quit();
        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function portaltjprSegundaInstancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log('TRIBUNAL PARANÁ - 2° INSTÂNCIA FÍSICO');
        $driver = $robo->startDriver();
        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            if (strlen($nroProcesso) == 25) {
                if (!preg_match('/.8.16/', $nroProcesso)) continue;
            } else {
                $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
                if (!in_array(strlen($nroProcesso), [7, 8, 9, 10])) continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) continue;

            $processosTratados[] = $processo;
        }

        krsort($processosTratados);//@TODO INVERTENDO O ARRAY PARA RODAR 2 ROTINAS

        $total = count($processosTratados);

        $robo->log("$total PROCESSOS ON TARGET");

        $url = "https://portal.tjpr.jus.br/consulta-processual/publico/b2grau/consultaPublica.do?tjpr.url.crypto=8a6c53f8698c7ff7d88bd1d17bac0727df8ab941b113325a";
        $index = 1;
        $alterados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $tipoNro = 'Número Único';
            $nomeCampo = '//*[@id="numeracaoProcessual"]';
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            if (strlen($nroProcesso) == 25) {
                if (!preg_match('/.8.16/', $nroProcesso)) continue;
            } else {
                $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
                if (!in_array(strlen($nroProcesso), [7, 8, 9, 10])) continue;
                if (strlen($nroProcesso) == 7) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '######-#', 7);
                    if (!$nroProcesso) continue;
                }
                if (strlen($nroProcesso) == 8) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '#######-#', 8);
                    if (!$nroProcesso) continue;
                }
                if (strlen($nroProcesso) == 9) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '######-#/##', 9);
                    if (!$nroProcesso) continue;
                }
                if (strlen($nroProcesso) == 10) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '#######-#/##', 10);
                    if (!$nroProcesso) continue;
                }
                $tipoNro = 'Número Original';
                $nomeCampo = '//*[@id="numeroAntigo"]';
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) continue;
            $nroProcessoTest = $robo->onlyDigits($nroProcesso);
            if ($robo->cacheExists(__FUNCTION__, $nroProcessoTest)) continue;

            while (true) {
                $driver->get($url);
                $driver->findElement(WebDriverBy::xpath('//*[@id="tipoConsulta"]'))->sendKeys($tipoNro);
                $driver->findElement(WebDriverBy::xpath($nomeCampo))->clear();
                $driver->findElement(WebDriverBy::xpath($nomeCampo))->sendKeys($nroProcesso);
                try {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                } catch (\Exception $e) {
                    $robo->log($e->getMessage());
                    continue;
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
//                $driver->findElement(WebDriverBy::xpath('//*[@id="pesquisar"]'))->click();
                $driver->executeScript("document.getElementById('pesquisar').click()", []);

                try {
                    $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="errorMessages"]')));
                    $errorMsg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]'))->getText());
                    $robo->log($errorMsg);
                    if (!preg_match('/Verificação do Captcha inválida/', $errorMsg)) break;
                } catch (\Exception $e) {
                    break;
                }
                $robo->log('TRYING CAPTCHA AGAIN!!!');
            }

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $errorMsg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]'))->getText());
                continue;
            } catch (\Exception $e) {
            }

            try {
                $vara = '';

                $driver->findElement(WebDriverBy::xpath('//*[@id="consulta2GrauForm"]/table/tbody/tr/td[2]/a'))->click();
                // segunda pagina
                $linkProcessos = $driver->findElements(WebDriverBy::xpath('//*[@id="consulta2GrauForm"]/table[2]/tbody/tr/td[1]/a'));
                $linkSelected = end($linkProcessos);
                $linkSelected->click();

                $tableRows = $driver->findElements(WebDriverBy::xpath('//*[@id="consulta2GrauForm"]/fieldset/table/tbody/tr'));
                $data = [];
                foreach ($tableRows as $row) {
                    $cells = $row->findElements(WebDriverBy::tagName('td'));
                    $data[$robo->cleanString($cells[0]->getText(), [':'])] = $robo->cleanString($cells[1]->getText());
                    $data[$robo->cleanString($cells[2]->getText(), [':'])] = $robo->cleanString($cells[3]->getText());
                }
//                if (isset($data['Órgão Julgador'])) $vara = $data['Órgão Julgador'];
                if (isset($data['Vara'])) $vara = $data['Vara'];
                if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                $alterados++;
            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }
        }

        $driver->quit();
        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function portaltjprPrimeiraInstancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL PARANÁ - 1° INSTÂNCIA FÍSICO';
        $robo->log($title);
        $driver = $robo->startDriver();
        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (strlen($nroProcesso) == 25) {
                if (!preg_match('/.8.16/', $nroProcesso)) continue;
            } else {
                $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
                if (!in_array(strlen($nroProcesso), [7, 8, 9, 10])) continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);

        $robo->log("$total PROCESSOS ON TARGET");

        $url = "https://portal.tjpr.jus.br/civel/publico/consulta/processo.do?actionType=iniciar";
        $index = 1;
        $alterados = 0;
        $captcha = '';

        $solta = false;

        foreach ($processosTratados as $processo) {

            $nomeCampo = '//*[@id="numeroUnico"]';
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (strlen($nroProcesso) == 25) {
                if (!preg_match('/.8.16/', $nroProcesso)) continue;
            } else {
                $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
                if (!in_array(strlen($nroProcesso), [7, 8, 9, 10])) continue;
                if (strlen($nroProcesso) == 7) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '######-#', 7);
                    if (!$nroProcesso) continue;
                }
                if (strlen($nroProcesso) == 8) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '#######-#', 8);
                    if (!$nroProcesso) continue;
                }
                if (strlen($nroProcesso) == 9) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '######-#/##', 9);
                    if (!$nroProcesso) continue;
                }
                if (strlen($nroProcesso) == 10) {
                    $nroProcesso = $robo->processNumberFormatter($nroProcesso, '#######-#/##', 10);
                    if (!$nroProcesso) continue;
                }
                $nomeCampo = '//*[@id="numeroAntigo"]';
            }

            $nroProcesso = $robo->onlyDigits($nroProcesso);
            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) continue;

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            if ($nroProcesso == '00043554420108160014') $solta = true;
//            if (!$solta) continue;

            while (true) {
                $driver->get($url);
                $driver->findElement(WebDriverBy::xpath($nomeCampo))->clear();
                $driver->findElement(WebDriverBy::xpath($nomeCampo))->sendKeys($nroProcesso);
                try {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                } catch (\Exception $e) {
                    $robo->log($e->getMessage());
                    continue;
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("enviarConsulta()", []);

                try {
                    $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="errorMessages"]')));
                    $errorMsg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]'))->getText());
                    $robo->log($errorMsg);
                    if (!preg_match('/Verificação do Captcha inválida/', $errorMsg)) break;
                } catch (\Exception $e) {
                    break;
                }
                $robo->log('TRYING CAPTCHA AGAIN!!!');
            }

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $errorMsg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]'))->getText());
                continue;
            } catch (\Exception $e) {
            }

            try {
                $vara = '';

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="processoConsultaPublicaForm"]/table[2]/tbody/tr/td[1]/a')));

                $tableRowsElem = $driver->findElements(WebDriverBy::xpath('//*[@id="processoConsultaPublicaForm"]/table[2]/tbody/tr'));
                $rowSelect = end($tableRowsElem);
                $cellsElem = $rowSelect->findElements(WebDriverBy::tagName('td'));
                $cellSelect = end($cellsElem);
                $vara = $robo->cleanString($cellSelect->getText());
                if ($vara != '') $robo->addVara($processo, $vara, false, $title . ' => ' . $driver->getCurrentURL());
                $alterados++;
            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjprTurmasRecursais($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL PARANÁ - TURMA RECURSAL';
        $robo->log($title);
        $driver = $robo->startDriver();
        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/.8.16/', $nroProcesso)) {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.#######-#', 12);
                if (!$nroProcesso) continue;
                $year = substr($nroProcesso, 0, 4);
                if ($year < 1970 || $year > date('Y')) continue;
            }

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);

        $robo->log("$total PROCESSOS ON TARGET");

        $url = "https://www.tjpr.jus.br/turmas-recursais";
        $index = 1;
        $alterados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $driver->get($url);

            $tabText = 'Número Único';
            $field = '_turmarecursal_WAR_TurmaRecursal100_numeroUnico';
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/.8.16/', $nroProcesso)) {
                $nroProcesso = $processo->getNumProcJust();
                if (!$nroProcesso) continue;
                $tabText = 'Número Original';
                $field = '_turmarecursal_WAR_TurmaRecursal100_numeroOriginal';
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) continue;
            $robo->saveCache(__FUNCTION__, $nroProcesso);

            $tabs = $driver->findElements(WebDriverBy::cssSelector('ul.nav.nav-tabs > li.tab'));
            foreach ($tabs as $tab) {
                if ($robo->cleanString($tab->getText()) == $tabText) {
                    $tab->click();
                    break;
                }
            }

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $driver->findElement(WebDriverBy::cssSelector('input[name="' . $field . '"]'))->sendKeys($nroProcesso);
            if ($tabText == 'Número Único') {
                $driver->executeScript('listProcessos("numeroUnico");', []);
            } else {
                $driver->executeScript('listProcessos("numeroOriginal");', []);
            }
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->executeScript('loadProcesso(' . $nroProcesso . ');', []);

            try {
                $driver->wait(6)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="processoDadosBasicos"]/tbody/tr')));
                $rowsElem = $driver->findElements(WebDriverBy::xpath('//*[@id="processoDadosBasicos"]/tbody/tr'));
                $dados = $robo->extractVerticalTable($rowsElem, 2);
                print_r($dados);
            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }
        }


        $driver->quit();
        $robo->log("{$alterados} PROCESSOS COM JUIZO|VARA");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    //CEARÁ - CE
    public function jfce($processos, $params = ['uf' => 'TRF', 'codeTribunal' => '.4.05.', 'tipoNro' => 'cnj'])
    {
        if (!count($params)) return false;
        /**
         * $params = ['uf'=>string,'codeTribunal'=>string,'tipoNro'=>string]
         */
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $uf = $params['uf'];
        $codeT = $params['codeTribunal'];
        $title = "{$uf} {$codeT} - JUSTIÇA FEDERAL";
        $robo->log($title);
        print_r($params);
        $func = __FUNCTION__ . implode('', $params);
        $driver = $robo->startDriver();

        $linkGet = "http://www.jfce.jus.br/consultaProcessual/cons_procs.asp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if (!$nroProcesso) continue;
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
                if (substr($nroProcesso, 0, 4) < 1970 || substr($nroProcesso, 0, 4) > date('Y')) continue;
            }
            if ($params['tipoNro'] == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;
            if ($codeT) if (!preg_match("/$codeT/", $nroProcesso)) continue;

            //if (!in_array(strlen(preg_replace('/\D/', '', $nroProcesso)), [10, 15, 20])) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
            }

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($linkGet);

            $robo->saveCache($func, $processo->getNumProcJust());

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();

            try {
                $iframe = '//*[@id="ResConsProc"]/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td/table/tbody/tr/td[2]/p/font/span/iframe';

                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($iframe)));

                $iframe = $driver->findElement(WebDriverBy::xpath($iframe));
                $driver->switchTo()->frame($iframe);

                $textarea = $driver->findElement(WebDriverBy::xpath('//*[@id="Resumo"]'))->getText();
                $textareaArr = explode("\n", $textarea);
                $vara = '';
                foreach ($textareaArr as $textItem) {
                    if ((preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
                            preg_match('/([0-9]{1}) a. VARA FEDERAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª TURMA RECURSAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        break;
                    }
                }

//                $row = '//*[@id="ResInfoProc"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]';
//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($row)));
//                $conteudoVara = $driver->findElement(WebDriverBy::xpath($row))->getText();
//
//                $textToFind = 'Localização ';
//                if (!preg_match("/$textToFind/", $conteudoVara)) continue;
//                $vara = $robo->cleanString($conteudoVara, [$textToFind]);

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function tjceesaj1grau($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "CEARÁ 1° INSTÂNCIA ESAJ";
        $robo->log($title);
        $driver = $robo->startDriver();
        $link = 'http://esaj.tjce.jus.br/cpopg/open.do';
        $driver->get($link);

        $comarcaElem = $driver->findElements(WebDriverBy::xpath('//*[@id="id_Foro"]/option'));
        $comarcas = [];
        foreach ($comarcaElem as $item) {
            $comV = $item->getAttribute('value');
            if ($comV <= 0) continue;
            $comarcas[] = str_pad($comV, 4, 0, STR_PAD_LEFT);
        }

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA E SEGUNDA LEVA
            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.06./', $nroProcesso)) continue;//PRIMEIRA E SEGUNDA LEVA
//            if (!in_array(substr($nroProcesso, -4, 4), $comarcas)) continue;//PRIMEIRA E SEGUNDA LEVA
//            $processosTratados[$nroProcesso] = $processo;//PRIMEIRA E SEGUNDA LEVA

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';
        $encontrados = 0;
        $debug = false;

        /**
         * PRIMEIRA TENTATIVA VARRENDO A LISTA DE PROCESSOS
         * APÓS A PESQUISA PELO NOME DA PARTE
         */

        /*$words = ['CAIXA SEGURO', 'CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA'];

        $driver->navigate()->to($link);
        
        foreach ($words as $word) {
            $robo->log($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);

            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'cssSelector', 'div.g-recaptcha iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
            } catch (\Exception $e) {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msgError = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                $robo->log($msgError);

                if (preg_match('/Marque a opção Não sou um robô/', $msgError)) {
                    try {
                        $driver->navigate()->to($link);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                        $driver->navigate()->to($link);
                    }
                    $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                    $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'cssSelector', 'div.g-recaptcha iframe');
                    if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();
                }
            }


            //VARRENDO OS PROCESSOS ENCONTRADOS
            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                //PRIMEIRA LEVA
                $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                foreach ($processosBloco as $bloco) {
                    $vara = '';
                    $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                    if ($nro == '') continue;
                    if (!isset($processosTratados[$nro])) continue;

                    $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                    $index++;

                    $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                    $linha = end($linhas);
                    $linhaArr = explode(' - ', $linha->getText());
                    $infoSelected = end($linhaArr);
                    if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                        $countAux = count($linhaArr);
                        $infoSelected = $linhaArr[$countAux - 2];
                    }
                    $vara = $robo->cleanString($infoSelected);

                    if ($vara != '') {
                        $robo->addVara($processosTratados[$nro], $vara, $debug, $driver->getCurrentURL());
                        $encontrados++;
                    }
                }
                //SEGUNDA LEVA SE EXISTIR
                while ($next = $robo->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {

                    try {
                        $driver->navigate()->to($next);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                    }

                    $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                    foreach ($processosBloco as $bloco) {
                        $vara = '';
                        $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                        if ($nro == '') continue;
                        if (!isset($processosTratados[$nro])) continue;

                        $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                        $index++;

                        $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                        $linha = end($linhas);
                        $linhaArr = explode(' - ', $linha->getText());
                        $infoSelected = end($linhaArr);
                        if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                            $countAux = count($linhaArr);
                            $infoSelected = $linhaArr[$countAux - 2];
                        }
                        $vara = $robo->cleanString($infoSelected);

                        if ($vara != '') {
                            $robo->addVara($processosTratados[$nro], $vara, $debug, $driver->getCurrentURL());
                            $encontrados++;
                        }
                    }
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }

        }*/


        /**
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         * TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
//            $nroProcesso = $processo->getNumProcJust();

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            $nroProcessoArr = explode('8.06', $nroProcesso);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $nroProcesso = rtrim($nroProcesso, '-');
                $robo->log($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'cssSelector', 'div.g-recaptcha iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText();
                $robo->log($msg);
                if (preg_match('/Não existem informações disponíveis para os parâmetros informados./', $msg)) continue;
//                $driver->navigate()->to($link);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->setOrigem($driver->getCurrentURL());
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                sleep(2);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->setOrigem($driver->getCurrentURL());
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace([';', ' - Unidade 100% Digital'], '', $varaAux));
                    if ($vara != '') $robo->addVara($processo, $vara, $debug, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->log("PRIMEIRA TENTATIVA - DONE!");
        $robo->clearCache(__FUNCTION__);
        die;
    }

    public function tjceesaj2grau($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE

        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "CEARÁ 2° INSTÂNCIA ESAJ";
        $robo->log($title);
        $driver = $robo->startDriver();
        $link = 'http://esaj.tjce.jus.br/cposg5/open.do';
        $driver->get($link);

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $nroProcesso = $processo->getNumProcJust();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.06./', $nroProcesso)) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';
        $alterados = 0;
        $debug = false;

        /**
         * PRIMEIRA TENTATIVA VARRENDO A LISTA DE PROCESSOS
         * APÓS A PESQUISA PELO NOME DA PARTE
         */
        /*$words = ['CAIXA SEGURO', 'CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA'];
        //$words = ['CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA', 'CAIXA CONSÓRCIO', 'CAIXA SEGURADORA'];

        $driver->navigate()->to($link);
        foreach ($words as $word) {
            $robo->log($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'cssSelector', 'div.g-recaptcha iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
            } catch (\Exception $e) {
                try {
                    $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                    $msgError = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                    $robo->log($msgError);

                    if (preg_match('/Marque a opção Não sou um robô/', $msgError)) {
                        try {
                            $driver->navigate()->to($link);
                        } catch (\Exception $e) {
                            $driver->navigate()->refresh();
                            $driver->navigate()->to($link);
                        }
                        $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'cssSelector', 'div.g-recaptcha iframe');
                        if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                    }
                } catch (\Exception $e) {
                    $robo->log("PROCESSOS NÃO ENCONTRADOS");
                    continue;
                }
            }

            //VARRENDO OS PROCESSOS ENCONTRADOS
            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                //PRIMEIRA LEVA
                $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                foreach ($processosBloco as $bloco) {
                    $vara = '';
                    $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                    if ($nro == '') continue;
                    if (!isset($processosTratados[$nro])) continue;

                    $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                    $index++;

                    $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                    $linha = end($linhas);
                    $linhaArr = explode(' - ', $linha->getText());
                    $infoSelected = end($linhaArr);
                    if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                        $countAux = count($linhaArr);
                        $infoSelected = $linhaArr[$countAux - 2];
                    }
                    $vara = $robo->cleanString($infoSelected);

                    if ($vara != '') {
                        $robo->addVara($processosTratados[$nro], $vara, $debug, $driver->getCurrentURL());
                        $alterados++;
                    }
                }
                //SEGUNDA LEVA SE EXISTIR
                while ($next = $robo->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {

                    try {
                        $driver->navigate()->to($next);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                    }

                    $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                    foreach ($processosBloco as $bloco) {
                        $vara = '';
                        $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                        if ($nro == '') continue;
                        if (!isset($processosTratados[$nro])) continue;

                        $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                        $index++;

                        $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                        $linha = end($linhas);
                        $linhaArr = explode(' - ', $linha->getText());
                        $infoSelected = end($linhaArr);
                        if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                            $countAux = count($linhaArr);
                            $infoSelected = $linhaArr[$countAux - 2];
                        }
                        $vara = $robo->cleanString($infoSelected);

                        if ($vara != '') {
                            $robo->addVara($processosTratados[$nro], $vara, $debug, $driver->getCurrentURL());
                            $alterados++;
                        }
                    }
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }

        }*/


        /**
         * AJUSTAR PARA RECEBER OS PROCESSOS DE 2° INST NCIA ESAJ
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
//            $nroProcesso = $processo->getNumProcJust();

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

//            $nroProcessoArr = explode('.8.06.', $nroProcesso);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Número do Processo');//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $nroProcesso = rtrim($nroProcesso, '-');
                $robo->log($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO


            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'cssSelector', 'div.g-recaptcha iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                $robo->log($msg);
                if (preg_match('/Não foi possível executar esta operação. Tente novamente mais tarde/', $msg)) {
                    $driver->navigate()->back();
                    try {
                        $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.g-recaptcha iframe')));
                        $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys('CAIXA SEGURADORA');
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'cssSelector', 'div.g-recaptcha iframe');
                        $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                    } catch (\Exception $e) {
                    }
                }
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->setOrigem($driver->getCurrentURL());
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->setOrigem($driver->getCurrentURL());
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $vara = $robo->cleanString($dadosProcesso['Distribuição']);
                    if ($vara != '') $robo->addVara($processo, $vara, $debug, $driver->getCurrentURL());
                    $alterados++;
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$alterados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        die;
    }

    public function tjce1instanciafisico($processos)
    {

        //PRIMEIRO - CNJ
        //SEGUNDO - NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA CEARÁ - 1° INSTÂNCIA FÍSICO";
        $robo->log($title);
        $driver = $robo->startDriver();

        //$linkGet = "http://www4.tjce.jus.br/sproc2/paginas/ConProcNum.asp";
        //$linkGet = "http://www4.tjce.jus.br/sproc2/paginas/ResConProc01.asp?TXT_PARAM1=[NRO_PROCESSO]&TXT_PARAM5=&TXT_PARAM2=&CMB_NUMMOV=99&TXT_SOURCE=ResConProcNum&CHK_PARTE=";
        //$linkGet = "http://www4.tjce.jus.br/sproc2/paginas/ResConProc02.asp?TXT_PARAM1=[NRO_PROCESSO]&TXT_PARAM2=0&CMB_NUMMOV=99&TXT_SOURCE=ResConProcNum&CHK_PARTE=";
        $linkGet = "http://www4.tjce.jus.br/sproc2/paginas/ResConProcNum.asp?TXT_NUMERO=&TXT_RECURSO=&TXT_NUMERO_sproc=&TXT_RECURSO_sproc=&TXT_NANT=[NRO_PROCESSO]&CMB_NUMMOV=99&submit=Consulta";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20); //PRIMEIRO
            if ($nroProcesso) continue;
            $nroProcesso = $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$robo->yearValidate($nroProcesso, 0, 4)) continue;
            $robo->log($nroProcesso);
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            //$nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20); //PRIMEIRO
            $nroProcesso = $processo->getNumProcJust();//SEGUNDO
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {
                $elem = '/html/body/table[3]/tbody/tr';
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($elem)));
                $rows = $driver->findElements(WebDriverBy::xpath($elem));

                $vara = '';
                foreach ($rows as $row) {
                    if (preg_match('/Localização: /', $row->getText())) {
                        $vara = $robo->cleanString($row->getText());
                    }
                }

                if ($vara) {
                    $varaArr = explode('Remetido em', $vara);
                    $vara = $robo->cleanString(str_replace('Localização: ', '', $varaArr[0]));

                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjceprojudi($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA CEARÁ - PROJUDI";
        $robo->log($title);
        $driver = $robo->startDriver();

        $sessao = "https://projudi.tjce.jus.br/projudi/interno.jsp?endereco=/projudi/publico/buscas/ProcessosParte";
        $linkGet = "https://projudi.tjce.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[NRO_PROCESSO]";
        $driver->get($robo->host);
        $driver->get($sessao);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $processo->getNumProcJust();
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {

                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjcejuizadoespecial($processos)
    {
        //SITE LENTO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA CEARÁ - JUIZADO ESPECIAL";
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://www3.tjce.jus.br/Capital/base_juizados_especiais.htm";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $processo->getNumProcJust();
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {

                $driver->findElement(WebDriverBy::xpath('/html/body/form/p[2]/font/input[2]'))->click();
                $driver->findElement(WebDriverBy::xpath('/html/body/form/p[3]/input[1]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('/html/body/form/p[3]/input[2]'))->click();

                sleep(3);
                continue;


                $vara = '';
                if ($vara != '') {
                    $robo->addVara($processo, $vara, true, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjcepje1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA CEARÁ - PJE 1° GRAU";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje.tjce.jus.br/pje1grau/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.06./', $nroProcesso)) continue;
            //if (!$robo->yearValidate($nroProcesso, 11)) continue;
            //if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($title, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="fPP:j_id140:captchaImg"]',
                    '//*[@id="fPP:j_id140:verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function tjcepje2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA CEARÁ - PJE 2° GRAU";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje.tjce.jus.br/pje2grau/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.06./', $nroProcesso)) continue;
            //if (!$robo->yearValidate($nroProcesso, 11)) continue;
            //if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($title, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="fPP:j_id140:captchaImg"]',
                    '//*[@id="fPP:j_id140:verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }


    //PARÁ - PA
    public function tjpawebservice($processos)
    {
        $url = 'https://consultas.tjpa.jus.br/WSConsultaProcessualTJ/ConsultaProcessualService?wsdl';

        $client = new \SoapClient($url);

//        debug($client->__getTypes());
//        debug($client->__getFunctions());

        $function = 'dadosProcessoCNJ';
        $arguments = [
            'dadosProcessoCNJ' => [
                'numeroProcessoCNJ' => '0006823-08.2014.8.14.0301',
                'tipoConsulta' => 'primeiro grau',
            ]
        ];


        $result = $client->__soapCall($function, $arguments);

        debug($result);
    }

    public function tjpa($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA PARÁ - 1° INSTÂNCIA";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://consultas.tjpa.jus.br/consultaprocessoportal/consulta/principal";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.#.######-#');
            if (!$nroProcesso) continue;
            if (!$robo->yearValidate($nroProcesso, 0)) continue;
            //CHECK CODE TRIBUNAL
//            if (!preg_match('/.8.14./', $nroProcesso)) continue;
//            if (substr($nroProcesso, 0, 1) != '0') continue;

//            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
//            if (strlen($nroProcesso) <> 20) continue;
            $robo->log($nroProcesso);
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        die;
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);
//
//            $nroProcesso = '0005918-86.2012.8.14.0005'; // 1° GRAU
//            $nroProcesso = '0000258-91.2005.8.14.0031'; // 2° GRAU
//            $nroProcesso = '0005918-86.2012.8.14.0005'; //JUIZADO ESPECIAL
//            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);


//            $nroProcesso = '0000031-25.2011.8.14.0097';
//            $nroProcesso = '0004625-69.2017.8.14.0017'; //JUIZADO ESPECIAL
//            $nroProcesso = '0011546-29.2005.8.14.0301'; // com 2 botões

            $processoNotFound = false;
            //NRO CNJ
            /*while (true) {
                try {
                    $driver->executeScript("document.getElementById(\"textProcesso\").value=\"{$nroProcesso}\";", []);
                    $element = $driver->findElement(WebDriverBy::xpath('//*[@id="imgPorNumeroProcesso"]'));
                    $driver->getMouse()->mouseMove($element->getCoordinates());

                    $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="imgPorNumeroProcesso"]', '//*[@id="textCaptcha"]', '//*[@id="btnPesquisarPorProcesso"]', false);

                    $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="msgGeral"]'))->getText());
                    $robo->log($msg);
                    $driver->executeScript('$("#msgGeral").dialog("close");', []);
                    if (strlen($msg) < 10) break;
                    if (preg_match('/Processo não encontrado/', $msg)) {
                        $processoNotFound = true;
                        break;
                    }
                    $robo->log($msg);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="btnCaptcha"]'))->click();
                } catch (\Exception $e) {
                    break;
                }
            }*/


            //NRO ANTIGO - CANCELADO PQ TEM MUITAS OPÇÕES DE SELEÇÃO
            /*while (true) {
                try {
                    $driver->executeScript("onClickConsultaDetalhada();", []);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="radioPorNumProcessoAntigo"]'))->click();
                    $driver->executeScript("document.getElementById(\"inputTextPorNumProcessoAntigo\").value=\"{$nroProcesso}\";", []);
                    $element = $driver->findElement(WebDriverBy::xpath('//*[@id="imgPorNumeroProcesso"]'));
                    $driver->getMouse()->mouseMove($element->getCoordinates());

                    $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="imgPorNumeroProcesso"]', '//*[@id="textCaptcha"]', '//*[@id="btnPesquisarPorProcesso"]', false);

                    $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="msgGeral"]'))->getText());
                    $robo->log($msg);
                    $driver->executeScript('$("#msgGeral").dialog("close");', []);
                    if (strlen($msg) < 10) break;
                    if (preg_match('/Processo não encontrado/', $msg)) {
                        $processoNotFound = true;
                        break;
                    }
                    $robo->log($msg);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="btnCaptcha"]'))->click();
                } catch (\Exception $e) {
                    break;
                }
            }*/

            if ($processoNotFound) continue;

            try {
                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('#divSelecaoInstancia a#unicaInstancia')));
                } catch (\Exception $e) {
                    $btns = $driver->findElements(WebDriverBy::cssSelector('#divSelecaoInstancia a.tj-btn'));
                    $chooseOne = end($btns);
                    $chooseOne->click();
                }

                $vara = '';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="divDadosProcesso"]/table[1]/tbody/tr')));
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="divDadosProcesso"]/table[1]/tbody/tr'));
                $dados = $robo->extractVerticalTable($rows);

                if (preg_match('/1º GRAU/', $dados['Instância']) || preg_match('/JUIZADO ESPECIAL/', $dados['Instância'])) {
                    $vara = $robo->cleanString($dados['Vara / Câmara'], ['; ', ';']);
                }
                if (preg_match('/2º GRAU/', $dados['Instância'])) {
                    if (isset($dados['Vara / Câmara']) && strlen($dados['Vara / Câmara']) > 5) {
                        $vara = $robo->cleanString($dados['Vara / Câmara'], ['; ', ';']);
                    } else {
                        $vara = $robo->cleanString($dados['Secretaria'], ['; ', ';']);
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;

    }

    public function tjpaporparte($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA PARÁ - 1° e 2° INSTÂNCIA + TURMA RECURSAL";
        $robo->log($title);
        $driver = $robo->startDriver(true);

        $linkGet = "https://consultas.tjpa.jus.br/consultaprocessoportal/consulta/principal";
        $driver->get($robo->host);

        //'CAIXA SEGURO','CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO',
        $partes = ['CAIXA VIDA', 'SULAMERICA'];

        $processosCap = [];
        foreach ($partes as $word) {

            $robo->log($word);

            $driver->navigate()->to($linkGet);
//            $driver->executeScript("$('body').css('position','fixed');", []);
            $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="menuConsultaDetalhada"]/a')));
            $driver->executeScript("$('div.blockUI').remove();", []);
            $linkOnClick = $driver->findElement(WebDriverBy::xpath('//*[@id="menuConsultaDetalhada"]/a'))->getAttribute('onclick');
            $driver->executeScript($linkOnClick, []);
            try {
                $text = $driver->findElement(WebDriverBy::cssSelector('div.blockUI'))->getText();
                $robo->log($text);
                $driver->executeScript("$('div.blockUI').remove();", []);

            } catch (\Exception $e) {
            }
            $driver->executeScript("$('div.blockUI').remove();", []);
            $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="radioPorNomeParte"]')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="radioPorNomeParte"]'))->click();
//            $driver->executeScript("$('#img_1G').css({'position':'absolute','top':'0','left':'0'});", []);
            $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="inputTextPorNomeParte"]')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="inputTextPorNomeParte"]'))->sendKeys($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="btnCaptcha_1G"]'))->click();

            sleep(2);

            $element = $driver->findElement(WebDriverBy::xpath('//*[@id="img_1G"]'));
            $driver->getMouse()->mouseMove($element->getCoordinates());

            $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="img_1G"]', '//*[@id="textCaptcha"]', '//*[@id="buttonPesquisar"]', false);

            while (true) {
                try {
                    try {
                        $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="msgGeral"]')));
                        $text = $driver->findElement(WebDriverBy::xpath('//*[@id="msgGeral"]'))->getText();
                        $robo->log($text);
                        $driver->executeScript('$("#msgGeral").dialog("close");', []);
                        $text = $driver->findElement(WebDriverBy::xpath('//*[@id="textCaptcha"]'))->getAttribute('value');
                        $robo->log($text);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="textCaptcha"]'))->sendKeys(strtolower($text));
                        $driver->findElement(WebDriverBy::xpath('//*[@id="buttonPesquisar"]'))->click();
                        $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="msgGeral"]')));
                        $text = $driver->findElement(WebDriverBy::xpath('//*[@id="msgGeral"]'))->getText();
                        $robo->log($text);
                        $driver->executeScript('$("#msgGeral").dialog("close");', []);
                    } catch (\Exception $e) {
                    }
                    $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="div-table-processos"]')));
                    break;
                } catch (\Exception $e) {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="btnCaptcha_1G"]'))->click();
                    $element = $driver->findElement(WebDriverBy::xpath('//*[@id="img_1G"]'));
                    $driver->getMouse()->mouseMove($element->getCoordinates());
//                    $driver->executeScript("$('#img_1G').css({'position':'absolute','top':'0','left':'0'});", []);
                    sleep(2);
                    $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="img_1G"]', '//*[@id="textCaptcha"]', '//*[@id="buttonPesquisar"]', false);
                }
            }

            $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table tbody tr')));
            $table = $driver->findElement(WebDriverBy::cssSelector('table'));

            $auxTable = $robo->extractRegularTable($table);
            $dados = [];
            foreach ($auxTable as $row) {
                $nrocnj = preg_replace('/\D/', '', $row['Nº Processo']);
                $dados[$nrocnj . '|' . $row['Documento']] = $row['Vara'];
            }
            $processosCap = array_merge($processosCap, $dados);

            while (true) {
                $next = $driver->findElement(WebDriverBy::xpath('//*[@id="div-paginacao-libra"]/div/a[3]'));
                if ($next->getAttribute('class') == 'next disabled') break;
                $next->click();

                sleep(5);

                $table = $driver->findElement(WebDriverBy::cssSelector('table'));

                $auxTable = $robo->extractRegularTable($table);
                $dados = [];
                foreach ($auxTable as $row) {
                    $nrocnj = preg_replace('/\D/', '', $row['Nº Processo']);
                    $dados[$nrocnj . '|' . $row['Documento']] = $row['Vara'];
                }
                $processosCap = array_merge($processosCap, $dados);

            }

            $totalWord = count($processosCap);
            $robo->log("$totalWord PROCESSOS PARA A PARTE PARTE PESQUISADA");
            $file = './data/temp/tjpa1inst.json';
            $before = json_decode(file_get_contents($file), true);
            file_put_contents($file, json_encode(array_merge($before, $processosCap)));


        }

    }

    public function tjpascript($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "JUSTIÇA PARÁ - 1° INSTÂNCIA";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://consultas.tjpa.jus.br/consultaprocessoportal/consulta/principal";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!$nroProcesso) continue;
            if (!$processo->getNumProcJust()) continue;
            //CHECK CODE TRIBUNAL
//            if (!preg_match('/8.14./', $nroProcesso)) continue;
//            if (substr($nroProcesso, 0, 1) != '0') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        $file = './data/temp/tjpa1inst.json';
        $processosCapturados = json_decode(file_get_contents($file), true);

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            $nroProcesso = $processo->getNumProcJust();

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

//            $driver->navigate()->to($linkGet);
//
//            $nroProcesso = '0005918-86.2012.8.14.0005'; // 1° GRAU
//            $nroProcesso = '0000258-91.2005.8.14.0031'; // 2° GRAU
//            $nroProcesso = '0005918-86.2012.8.14.0005'; //JUIZADO ESPECIAL
//
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
//
//            $driver->findElement(WebDriverBy::xpath('//*[@id="textProcesso"]'))->sendKeys($nroProcesso);


            try {
//                $driver->findElement(WebDriverBy::cssSelector('#divSelecaoInstancia a#unicaInstancia'));
//                $btns = $driver->findElements(WebDriverBy::cssSelector('#divSelecaoInstancia a.tj-btn'));

                $vara = '';
                foreach ($processosCapturados as $nro => $nomeVara) {
                    if (preg_match("/$nroProcesso/", $nro)) {
                        $vara = $nomeVara;
                        $robo->log("$nroProcesso => $nro ==>> $nomeVara");
                        break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;

    }

    public function tjpaprojudiA($processos)
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL PARÁ - PROJUDI";
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://projudi.tjpa.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[NRO_PROCESSO_SEM_MASCARA]";
        $session = "https://projudi.tjpa.jus.br/projudi/interno.jsp?endereco=/projudi/buscas/ProcessosParte";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $processo->getNumProcJust();
            if (!$processo->getNumProcJust()) continue;
            //$nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
            if (!$nroProcesso) {
                continue;
//                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            if (!$processo->getNumProcJust()) continue;
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) {
                continue;
//                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
//                if (!$nroProcesso) {
//                    $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//                }
            }
            if (!$nroProcesso) continue;

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($session);
            $nroProcesso = ltrim($nroProcesso, '0');
            $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
            $driver->navigate()->to($link);

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {
                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpaprojudiB($processos)
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL PARÁ - PROJUDI B";
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://projudi.tjpa.jus.br/projudi/interno.jsp?endereco=/projudi/buscas/ProcessosParte";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            if (strlen($nroProcesso) != 20) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($linkGet);

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {

                $iframe = $driver->findElement(WebDriverBy::tagName('iframe'));
                $driver->switchTo()->frame($iframe);
                $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('/html/body/div[1]/form/p[2]/table/tbody/tr[19]/td/input'))->click();
                $driver->switchTo()->defaultContent();

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::tagName('iframe')));
                $iframe = $driver->findElement(WebDriverBy::tagName('iframe'));
                $driver->switchTo()->frame($iframe);

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/form[2]/table/tbody/tr[4]/td[2]/a')));
                $driver->findElement(WebDriverBy::xpath('/html/body/div/form[2]/table/tbody/tr[4]/td[2]/a'))->click();

                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpapje1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL PARÁ - PJE 1° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://pje.tjpa.jus.br/pje/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.14./', $nroProcesso)) continue;
//            if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="fPP:j_id140:captchaImg"]',
                    '//*[@id="fPP:j_id140:verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjpapje2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL PARÁ - PJE 2° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://pje.tjpa.jus.br/pje-2g/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.14./', $nroProcesso)) continue;
//            if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="fPP:j_id140:captchaImg"]',
                    '//*[@id="fPP:j_id140:verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    //TRIBUNAL MATO GROSSO DO SUL
    public function tjmsjf($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $link = 'http://www.jfsp.jus.br/csp/consulta/consinternet.csp';

        $total = count($processos);
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if (!$nroProcesso) continue;

            $driver->navigate()->to($link);

            $driver->findElement(WebDriverBy::xpath('/html/body/form/fieldset[1]/table/tbody/tr[3]/td[2]/p/input[2]'))->click();
            $driver->findElement(WebDriverBy::xpath('//*[@id="num_processo"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('/html/body/form/fieldset[1]/table/tbody/tr[3]/td[2]/p/input[4]'))->click();

            try {
                $driver->wait(1)->until(
                    WebDriverExpectedCondition::alertIsPresent());
                $driver->switchTo()->alert()->accept();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['SECRETARIA']) && $dadosProcesso['SECRETARIA'] != '') {
                    //salvar campo
                    $vara = rtrim($robo->cleanString($dadosProcesso['SECRETARIA'], ['; ', ';']));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                    $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    public function tjms1instanciaesaj($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "MATO GROSSO DO SUL 1° INSTÂNCIA ESAJ";
        $robo->log($title);
        $driver = $robo->startDriver();
        $link = 'https://www.tjms.jus.br/cpopg5/open.do';
        $driver->get($link);

        $comarcaElem = $driver->findElements(WebDriverBy::xpath('//*[@id="id_Comarca"]/option'));
        $comarca = [];
        foreach ($comarcaElem as $item) {
            $comV = $item->getAttribute('value');
            if ($comV < 0) continue;
            $comarcas[] = str_pad($comV, 4, 0, STR_PAD_LEFT);
        }

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA E SEGUNDA LEVA
            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.12./', $nroProcesso)) continue;//PRIMEIRA E SEGUNDA LEVA
//            if (!in_array(substr($nroProcesso, -4, 4), $comarcas)) continue;//PRIMEIRA E SEGUNDA LEVA

            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';
        $encontrados = 0;

        /**
         * PRIMEIRA TENTATIVA VARRENDO A LISTA DE PROCESSOS
         * APÓS A PESQUISA PELO NOME DA PARTE
         */

        /*$words = ['CAIXA SEGURO', 'CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA'];
        //$words = ['CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA', 'CAIXA CONSÓRCIO', 'CAIXA SEGURADORA'];

        $driver->navigate()->to($link);
        foreach ($words as $word) {
            $robo->log($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
            } catch (\Exception $e) {
                try{
                    $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                    $msgError = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                    $robo->log($msgError);
                }catch(\Exception $e){
                }
                continue;
            }

            //VARRENDO OS PROCESSOS ENCONTRADOS
            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                //PRIMEIRA LEVA
                $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                foreach ($processosBloco as $bloco) {
                    $vara = '';
                    $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                    if ($nro == '') continue;
                    if (!isset($processosTratados[$nro])) continue;

                    $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                    $index++;

                    $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                    $linha = end($linhas);
                    $linhaArr = explode(' - ', $linha->getText());
                    $infoSelected = end($linhaArr);
                    if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                        $countAux = count($linhaArr);
                        $infoSelected = $linhaArr[$countAux - 2];
                    }
                    $vara = $robo->cleanString($infoSelected);

                    if ($vara != '') {
                        $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                    }
                }
                //SEGUNDA LEVA SE EXISTIR
                while ($next = $robo->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {

                    try {
                        $driver->navigate()->to($next);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                    }

                    $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                    foreach ($processosBloco as $bloco) {
                        $vara = '';
                        $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                        if ($nro == '') continue;
                        if (!isset($processosTratados[$nro])) continue;

                        $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                        $index++;

                        $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                        $linha = end($linhas);
                        $linhaArr = explode(' - ', $linha->getText());
                        $infoSelected = end($linhaArr);
                        if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                            $countAux = count($linhaArr);
                            $infoSelected = $linhaArr[$countAux - 2];
                        }
                        $vara = $robo->cleanString($infoSelected);

                        if ($vara != '') {
                            $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                            $encontrados++;
                        }
                    }
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("PRIMEIRA TENTATIVA - DONE!");
        $robo->log("$encontrados JUIZO/VARA ENCONTRADOS");
        die;*/


        /**
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         * TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
//            if (strlen($nroProcesso) == 20) continue;
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            $nroProcessoArr = explode('.8.12.', $nroProcesso);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $robo->log($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO


            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText();
                $robo->log($msg);
                if (preg_match('/Não existem informações disponíveis para os parâmetros informados./', $msg)) continue;
                $driver->navigate()->back();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                sleep(2);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace([';', ' - Unidade 100% Digital'], '', $varaAux));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        $robo->log($title);
        $robo->log("$encontrados JUIZO/VARA ENCONTRADOS");
        die;
    }

    public function tjms2instanciaesaj($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE

        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "MATO GROSSO DO SUL 2° INSTÂNCIA ESAJ";
        $robo->log($title);
        $driver = $robo->startDriver();
        $link = 'https://www.tjms.jus.br/cposg5/open.do';
        $driver->get($link);

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $nroProcesso = $processo->getNumProcJust();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.12./', $nroProcesso)) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';
        $encontrados = 0;

        /**
         * AJUSTAR PARA RECEBER OS PROCESSOS DE 2° INST NCIA ESAJ
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $nroProcesso = $processo->getNumProcJust();

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

//            if (strlen($nroProcesso) == 20) continue;
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            $nroProcessoArr = explode('.8.12.', $nroProcesso);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Número do Processo');//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $robo->log($nroProcesso);
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                $robo->log($msg);
                if (preg_match('/Não foi possível executar esta operação. Tente novamente mais tarde/', $msg)) {
                    $driver->navigate()->back();
                    try {
                        $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe')));
                        $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                        $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys('CAIXA SEGURADORA');
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[8]/td[2]/div/div/div/div/iframe');
                        $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                    } catch (\Exception $e) {
                    }
                }
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[2]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $vara = $robo->cleanString($dadosProcesso['Distribuição']);
                    if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        $robo->log($title);
        $robo->log("$encontrados JUIZO/VARA ENCONTRADOS");
        return true;
    }


    //TRIBUNAIS DF - DISTRITO FEDERAL
    public function tjdf1instanciaget($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "DISTRITO FEDERAL 1° INSTÂNCIA - GET MODE";
        $robo->log($title);
        $driver = $robo->startDriver();
//        $link = 'http://cache-internet.tjdft.jus.br/cgi-bin/tjcgi1?NXTPGM=tjhtml105&SELECAO=1&ORIGEM=INTER&CIRCUN=1&CDNUPROC=[NRO_PROCESSO_ANTIGO]';
        $link = 'http://cache-internet.tjdft.jus.br/cgi-bin/tjcgi1?NXTPGM=tjhtml101&submit=ok&SELECAO=1&CHAVE=[NRO_PROCESSO_ANTIGO]&CIRC=ZZ&CHAVE1=&ORIGEM=INTER';
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.#.######-#', 14);
//            $robo->log($nroProcesso);
//            if (!$nroProcesso) continue;
//            if (!$robo->yearValidate($nroProcesso, 0)) continue;
//            if (!preg_match('/.8.07./', $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';
        $encontrados = 0;

        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->get(str_replace('[NRO_PROCESSO_ANTIGO]', $nroProcesso, $link));

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {

//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('detalhamentoDeProcesso')));

                $driver->findElement(WebDriverBy::id('detalhamentoDeProcesso'));

                $fieldsMap = [
                    'i_codigoCircunscricao' => 'Código Circunscrição',
                    'i_nomeCircunscricao' => 'Circunscrição',
                    'i_numeroProcesso14' => 'Processo',
                    'i_dataDistribuicao' => 'Data Dist',
                    'i_numeroProcesso20' => 'Numeração Única do Processo(CNJ)',
                    'i_preferenciaTramitacao' => 'Preferência na Tramitação',
                    'i_codigoVara' => 'Código Vara',
                    'i_descricaoVara' => 'Vara',
                    'i_naturezaVara' => 'Natureza da Vara',
                    'i_enderecoVara' => 'Endereço da Vara',
                    'i_horarioVara' => 'Horário de Funcionamento da Vara',
                    'i_classeProcessual' => 'Classe',
                    'i_assuntoProcessual' => 'Assunto',
                    'i_feitoCodigo' => 'Código Feito',
                    'i_feitoDescricao' => 'Feito',
                    'i_valorCausa' => 'Valor da Causa',
                    'i_nomeAutor' => 'Requerente',
                    'i_advogadoAutor' => 'Advogado Autor',
                    'i_nomeReu' => 'Requerido',
                    'i_advogadoReu' => 'Advogado Reu',
                    'i_origem' => 'Origem',
                    'i_material' => 'Material',
                    'i_segredoJustica' => 'Seg. Justiça',
                    'i_processoApenso_1' => 'Apensado ao(s) processo(s)',
                ];

                $detalhamento = [];
                foreach ($fieldsMap as $field => $friendlyName) {
                    try {
                        $elements = $driver->findElement(WebDriverBy::id($field));
                        $detalhamento[$friendlyName] = $elements->getText();
                    } catch (NoSuchElementException $e) {
                        $detalhamento[$friendlyName] = '';
                    }
                }

                if (isset($detalhamento['Vara']) && $detalhamento['Vara'] != '') {
                    $vara = $robo->cleanString($detalhamento['Vara']);
                    if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }

        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        $robo->log($title);
        $robo->log("$encontrados JUIZO/VARA ENCONTRADOS");
        return true;
    }

    public function tjdf1instancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "DISTRITO FEDERAL 1° INSTÂNCIA";
        $robo->log($title);
        $driver = $robo->startDriver();
        $link = 'http://www.tjdft.jus.br/';
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/.8.07./', $nroProcesso)) continue;
            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';
        $encontrados = 0;

        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->get($link);

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {

                $driver->findElement(WebDriverBy::xpath('//*[@id="SELECAO"]'))->sendKeys('número do processo');
                $driver->findElement(WebDriverBy::xpath('//*[@id="CIRC"]'))->sendKeys('todas');
                $driver->findElement(WebDriverBy::xpath('//*[@id="src1inst"]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="portal-column-one"]/form[1]/input[6]'))->click();

                @$driver->switchTo()->window(end($driver->getWindowHandles()));

                $driver->findElement(WebDriverBy::id('detalhamentoDeProcesso'));

                $fieldsMap = [
                    'i_codigoCircunscricao' => 'Código Circunscrição',
                    'i_nomeCircunscricao' => 'Circunscrição',
                    'i_numeroProcesso14' => 'Processo',
                    'i_dataDistribuicao' => 'Data Dist',
                    'i_numeroProcesso20' => 'Numeração Única do Processo(CNJ)',
                    'i_preferenciaTramitacao' => 'Preferência na Tramitação',
                    'i_codigoVara' => 'Código Vara',
                    'i_descricaoVara' => 'Vara',
                    'i_naturezaVara' => 'Natureza da Vara',
                    'i_enderecoVara' => 'Endereço da Vara',
                    'i_horarioVara' => 'Horário de Funcionamento da Vara',
                    'i_classeProcessual' => 'Classe',
                    'i_assuntoProcessual' => 'Assunto',
                    'i_feitoCodigo' => 'Código Feito',
                    'i_feitoDescricao' => 'Feito',
                    'i_valorCausa' => 'Valor da Causa',
                    'i_nomeAutor' => 'Requerente',
                    'i_advogadoAutor' => 'Advogado Autor',
                    'i_nomeReu' => 'Requerido',
                    'i_advogadoReu' => 'Advogado Reu',
                    'i_origem' => 'Origem',
                    'i_material' => 'Material',
                    'i_segredoJustica' => 'Seg. Justiça',
                    'i_processoApenso_1' => 'Apensado ao(s) processo(s)',
                ];

                $detalhamento = [];
                foreach ($fieldsMap as $field => $friendlyName) {
                    try {
                        $elements = $driver->findElement(WebDriverBy::id($field));
                        $detalhamento[$friendlyName] = $elements->getText();
                    } catch (NoSuchElementException $e) {
                        $detalhamento[$friendlyName] = '';
                    }
                }

                if (isset($detalhamento['Vara']) && $detalhamento['Vara'] != '') {
                    $vara = $robo->cleanString($detalhamento['Vara']);
                    if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }

            $driver->executeScript("window.close()", []);
            @$driver->switchTo()->window(end($driver->getWindowHandles()));

        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        $robo->log($title);
        $robo->log("$encontrados JUIZO/VARA ENCONTRADOS");
        return true;
    }

    public function tjdf2instancia($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "DISTRITO FEDERAL 2° INSTÂNCIA";
        $robo->log($title);
        $driver = $robo->startDriver();
        $link = 'http://www.tjdft.jus.br/';
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;
            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';
        $encontrados = 0;

        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->get($link);

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {

                $driver->findElement(WebDriverBy::xpath('//*[@id="tipo"]'))->sendKeys('número do processo');
                $driver->findElement(WebDriverBy::xpath('//*[@id="src2inst"]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="portal-column-one"]/form[4]/input[6]'))->click();

                @$driver->switchTo()->window(end($driver->getWindowHandles()));

                try {
                    $linkProcessos = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr/td[1]/li/a'));
                    $linkChosen = null;
                    foreach ($linkProcessos as $linkProc) {
                        $text = $robo->cleanString($linkProc->getText());
                        if (preg_match('/APC/', $text)) {
                            $linkChosen = $linkProc->getAttribute('href');
                            break;
                        }
                    }
                    if ($linkChosen) $driver->navigate()->to($linkChosen);

                    $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr')));

                } catch (\Exception $e) {
                }

                $linhas = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr'));

                $detalhamento = $robo->extractVerticalTable($linhas);

                if (isset($detalhamento['Orgão']) && $detalhamento['Orgão'] != '') {
                    $vara = $robo->cleanString($detalhamento['Orgão'], ['; ', ';']);
                    if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }

            $driver->executeScript("window.close()", []);
            @$driver->switchTo()->window(end($driver->getWindowHandles()));

        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        $robo->log($title);
        $robo->log("$encontrados JUIZO/VARA ENCONTRADOS");
        return true;
    }

    public function tjdfprojudi($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "DISTRITO FEDERAL - PROJUDI";
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://projudi.tjdft.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[NRO_PROCESSO_SEM_MASCARA]";
        $session = "https://projudi.tjdft.jus.br/projudi/publico/buscas/ProcessosParte?publico=true";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($session);
            $nroProcesso = ltrim($nroProcesso, '0');
            $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
            $driver->navigate()->to($link);

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {
                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjdfpje1grau($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();
        $title = 'DISTRITO FEDERAL PJE 1° GRAU';
        $robo->log($title);

        $linkGet = 'https://pje.tjdft.jus.br/consultapublica/ConsultaPublica/listView.seam';

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            if (!preg_match('/.8.07./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 2) != '07') continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $driver->get($linkGet);

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE 429 => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $robo->log($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE 440 => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function tjdfpje2grau($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();
        $title = 'DISTRITO FEDERAL PJE 2° GRAU';
        $robo->log($title);

        $linkGet = 'https://pje2i.tjdft.jus.br/pje/ConsultaPublica/listView.seam';

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            if (!preg_match('/.8.07./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 2) != '07') continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $driver->get($linkGet);

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                if ($captcha == '') {
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                    $robo->log("LINE 429 => $captcha");
                }
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $robo->log($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE 440 => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    //TRIBUNAIS MATO GROSSO - MT
    public function tjmt1instanciafisico($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();
        $title = 'MATO GROSSO 1° INSTÂNCIA';
        $robo->log($title);

        $linkGet = 'http://www.tjmt.jus.br/ConsultaProcessual/7efd62d070c446e5819002bf3b83df66';

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
            if (!preg_match('/.8.11./', $nroProcesso)) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######-##.####.###.####');
            //            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//            $nroProcesso = $processo->getNumProcJust();
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $nroProcessoArr = explode('.', $nroProcesso);

            $driver->get($linkGet);

//            $driver->findElement(WebDriverBy::xpath('//*[@id="abaNumeracaoUnica"]'))->click();
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNumeroUnico"]'))->sendKeys($nroProcessoArr[0]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtDigito"]'))->sendKeys($nroProcessoArr[1]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNumeroUnicoAno"]'))->sendKeys($nroProcessoArr[3]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtOrigem"]'))->sendKeys($nroProcessoArr[5]);

            $driver->findElement(WebDriverBy::xpath('//*[@id="processo"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="buscarProcesso"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {
                $linhas = $driver->findElements(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/div[3]/table/tbody/tr'));

                $vara = '';
                foreach ($linhas as $linha) {
                    $cells = $linha->findElements(WebDriverBy::tagName('td'));
                    if ($cells[0]->getText() == 'Lotação:') {
                        $vara = $robo->cleanString($cells[1]->getText());
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }

            /*while (true) {
                try{
                    $driver = $robo->hasCaptcha(
                        $driver,
                        'xpath',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_imgCaptcha"]',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtCodSeguranca"]',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_btSubmit"]',
                        false);

                    $driver->wait(1)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $robo->cleanString($driver->switchTo()->alert()->getText());
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    if (!preg_match('/O código de segurança digitado não está de acordo com a imagem/',$alertText)) break;
                }catch(\Exception $e){
                    break;
                }
            }*/

        }

        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function tjmt2instanciafisico($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();
        $title = 'MATO GROSSO 2° INSTÂNCIA';
        $robo->log($title);

        $linkGet = 'http://www.tjmt.jus.br/ConsultaProcessual/7efd62d070c446e5819002bf3b83df66';

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.11./', $nroProcesso)) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######-##.####.###.####');
            //            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $nroProcesso = $processo->getNumProcJust();
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $nroProcessoArr = explode('.', $nroProcesso);

            $driver->get($linkGet);

//            $driver->findElement(WebDriverBy::xpath('//*[@id="abaNumeracaoUnica"]'))->click();
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNumeroUnico"]'))->sendKeys($nroProcessoArr[0]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtDigito"]'))->sendKeys($nroProcessoArr[1]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNumeroUnicoAno"]'))->sendKeys($nroProcessoArr[3]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtOrigem"]'))->sendKeys($nroProcessoArr[5]);

            $driver->findElement(WebDriverBy::xpath('//*[@id="instancia"]'))->sendKeys('2ª Instância');
            $driver->findElement(WebDriverBy::xpath('//*[@id="processo"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="buscarProcesso"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {
                $linhas = $driver->findElements(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/div[2]/table/tbody/tr'));

                $vara = '';
                foreach ($linhas as $linha) {
                    $cells = $linha->findElements(WebDriverBy::tagName('td'));
                    if ($cells[0]->getText() == 'Câmara:') {
                        $vara = $robo->cleanString($cells[1]->getText());
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }

            /*while (true) {
                try{
                    $driver = $robo->hasCaptcha(
                        $driver,
                        'xpath',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_imgCaptcha"]',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtCodSeguranca"]',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_btSubmit"]',
                        false);

                    $driver->wait(1)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $robo->cleanString($driver->switchTo()->alert()->getText());
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    if (!preg_match('/O código de segurança digitado não está de acordo com a imagem/',$alertText)) break;
                }catch(\Exception $e){
                    break;
                }
            }*/

        }

        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function tjmtturmarecursal($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();
        $title = 'MATO GROSSO - TURMA RECURSAL';
        $robo->log($title);

        $linkGet = 'http://www.tjmt.jus.br/ConsultaProcessual/7efd62d070c446e5819002bf3b83df66';

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.11./', $nroProcesso)) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######-##.####.###.####');
//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $nroProcesso = $processo->getNumProcJust();
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $nroProcessoArr = explode('.', $nroProcesso);

            $driver->get($linkGet);

//            $driver->findElement(WebDriverBy::xpath('//*[@id="abaNumeracaoUnica"]'))->click();
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNumeroUnico"]'))->sendKeys($nroProcessoArr[0]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtDigito"]'))->sendKeys($nroProcessoArr[1]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtNumeroUnicoAno"]'))->sendKeys($nroProcessoArr[3]);
//            $driver->findElement(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtOrigem"]'))->sendKeys($nroProcessoArr[5]);

            $driver->findElement(WebDriverBy::xpath('//*[@id="instancia"]'))->sendKeys('Turma Recursal');
            $driver->findElement(WebDriverBy::xpath('//*[@id="processo"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="buscarProcesso"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {
                $linhas = $driver->findElements(WebDriverBy::xpath('//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_fvProcesso"]/tbody/tr/td/div[2]/table/tbody/tr'));

                $vara = '';
                foreach ($linhas as $linha) {
                    $cells = $linha->findElements(WebDriverBy::tagName('td'));
                    if ($cells[0]->getText() == 'Câmara:') {
                        $vara = $robo->cleanString($cells[1]->getText());
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }

            /*while (true) {
                try{
                    $driver = $robo->hasCaptcha(
                        $driver,
                        'xpath',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_imgCaptcha"]',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_txtCodSeguranca"]',
                        '//*[@id="phMiolo_ContentPrincipal_cphConsultaProcessoPrincipal_Principal_btSubmit"]',
                        false);

                    $driver->wait(1)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $robo->cleanString($driver->switchTo()->alert()->getText());
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    if (!preg_match('/O código de segurança digitado não está de acordo com a imagem/',$alertText)) break;
                }catch(\Exception $e){
                    break;
                }
            }*/

        }

        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    //TRIBUNAL AMAPÁ - AP
    public function tjaptucujuris($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();
        $title = 'TRIBUNAL AMAPÁ/AP - TUCUJURIS';
        $robo->log($title);

        $linkGet = 'http://tucujuris.tjap.jus.br/tucujuris/pages/consultar-processo/consultar-processo.html';

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            if (!preg_match('/.8.03./', $nroProcesso)) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($robo->host);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######-##.####.###.####');
//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $nroProcessoArr = explode('.', $nroProcesso);

            try {

                $driver->get($linkGet);

                $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="form-consulta"]/div/form/div[5]/div/label[4]/input')));

                sleep(5);


                $driver->findElement(WebDriverBy::xpath('//*[@id="form-consulta"]/div/form/div[1]/div/div/div/input'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="form-consulta"]/div/form/div[5]/div/label[4]/input'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="btnConsultar"]'))->click();

                $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('span[data-bind="text: processo.lotacao"]')));

                sleep(5);

                $vara = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('span[data-bind="text: processo.lotacao"]'))->getText());

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log('PROCESSO NÃO ENCONTRADO');
            }

        }

        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    //TRIBUNAL SERGIPE - SE
    public function tjsejf($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL SERGIPE - JF";
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://consulta2.jfse.jus.br/ConsultaTebas/cons_procs.asp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            /*$nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if (!$nroProcesso) continue;
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
                if (substr($nroProcesso, 0, 4) < 1970 || substr($nroProcesso, 0, 4) > date('Y')) continue;
            }
            if ($params['tipoNro'] == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;
            if ($codeT) if (!preg_match("/$codeT/", $nroProcesso)) continue;*/

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            /*if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
            }

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);*/

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($linkGet);

            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();

            try {
                $iframe = '//*[@id="ResConsProc"]/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td/table/tbody/tr/td[2]/p/font/span/iframe';

                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($iframe)));

                $iframe = $driver->findElement(WebDriverBy::xpath($iframe));
                $driver->switchTo()->frame($iframe);

                $textarea = $driver->findElement(WebDriverBy::xpath('//*[@id="Resumo"]'))->getText();
                $textareaArr = explode("\n", $textarea);
                $vara = '';

                foreach ($textareaArr as $textItem) {
                    if ((preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
                            preg_match('/([0-9]{1}) a. VARA FEDERAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª TURMA RECURSAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        break;
                    }
                }

//                $row = '//*[@id="ResInfoProc"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]';
//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($row)));
//                $conteudoVara = $driver->findElement(WebDriverBy::xpath($row))->getText();
//
//                $textToFind = 'Localização ';
//                if (!preg_match("/$textToFind/", $conteudoVara)) continue;
//                $vara = $robo->cleanString($conteudoVara, [$textToFind]);

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjsejfcreta($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL SERGIPE JF CRETA";

        $robo->log($title);
        $driver = $robo->startDriver(false, true);

        $linkGet = "https://wwws.jfse.jus.br/cretainternetse/";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (strlen($nroProcesso) < 15) continue;
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {


            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($title, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[3]/td/form/table[3]/tbody/tr/td[2]/input')));

            $driver->findElement(WebDriverBy::xpath('/html/body/table/tbody/tr[3]/td/form/table[3]/tbody/tr/td[2]/input'))->sendKeys($nroProcesso);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '/html/body/table/tbody/tr[3]/td/form/table[6]/tbody/tr/td[2]/a[1]/img',
                    '/html/body/table/tbody/tr[3]/td/form/table[6]/tbody/tr/td[2]/input',
                    '//*[@id="btn_pesquisar"]', false);

                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $robo->cleanString($driver->switchTo()->alert()->getText());
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    if (preg_match('/Código da imagem de segurança inválido/', $alertText)) continue;
                    if (preg_match('/O código de segurança deve ser preenchido/', $alertText)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $result = $robo->cleanString($driver->findElement(WebDriverBy::className('grid_center'))->getText());
                $robo->log($result);
                if ($result == 'Nenhum processo encontrado.') continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('grid')));

                $vara = '';
                $cells = $driver->findElements(WebDriverBy::cssSelector('td.grid'));
                foreach ($cells as $cell) {
                    $cellText = $cell->getText();
                    if (preg_match('/Juizado: /', $cellText)) {
                        $vara = $robo->cleanString($cellText, ['Juizado: ']);
                        break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function tjsejfpje($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL SERGIPE JF PJE";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje.jfse.jus.br/pje/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/4.05/', $nroProcesso)) continue;
            //if (!$robo->yearValidate($nroProcesso, 11)) continue;
            //if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($title, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

//            $nroProcesso = '0806154-40.2017.4.05.8500';

            $driver->executeScript("document.getElementById(\"consultaPublicaForm:Processo:ProcessoDecoration:Processo\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="consultaPublicaForm:captcha:captchaImg"]',
                    '//*[@id="consultaPublicaForm:captcha:j_id224:verifyCaptcha"]',
                    '//*[@id="consultaPublicaForm:pesq"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a')));

                $linkAux = $driver->findElement(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a'))->getAttribute('onclick');
                $piece = '/pje/ConsultaPublica/DetalheProcessoConsultaPublica/listView.seam?signedIdProcessoTrf=';
                $token = $robo->findBetween($linkAux, $piece, "');;A4J.AJAX.Submit(");
                $link = $piece . $token;
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::className('propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    try {
                        if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                            $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                            break;
                        }
                    } catch (\Exception $e) {
                    }
                }
                if ($vara == '') {
                    foreach ($cells as $cell) {
                        try {
                            if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                                $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                                break;
                            }
                        } catch (\Exception $e) {
                        }
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function tjsefisico($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL SERGIPE PROCESSOS FÍSICOS";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://www.tjse.jus.br/portal/aplicacoes/consultaprocessual/#/";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.25/', $nroProcesso)) continue;
//            if (strlen($nroProcesso) < 10) continue;
//            if (!$robo->yearValidate($nroProcesso,0)) continue;
//            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######.##.####.###.####');

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            while (true) {

                $driver->navigate()->to($linkGet);
                $driver->findElement(WebDriverBy::xpath('//*[@id="consultaForm"]/section/div[1]/ul/li[4]'))->click();
                $driver->executeScript("document.getElementById('consultaForm').reset();", []);


                //NRO ÚNICO
                $pedacosProcesso = explode('.', $nroProcesso);
                if (count($pedacosProcesso) != 5) continue;
                sleep(1);
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="sequencial"]')));
                $driver->findElement(WebDriverBy::xpath('//*[@id="sequencial"]'))->sendKeys($pedacosProcesso[0]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="digito"]'))->sendKeys($pedacosProcesso[1]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="ano"]'))->sendKeys($pedacosProcesso[2]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="origem"]'))->sendKeys($pedacosProcesso[4]);

                //OUTROS NROS
//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="numero"]')));
//                $driver->findElement(WebDriverBy::xpath('//*[@id="numero"]'))->sendKeys($nroProcesso);

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="captcha"]',
                    '//*[@id="token"]',
                    '//*[@id="destino"]', false);

                try {
                    $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="vazioTitle"]'))->getText());
                    if (preg_match('/Código de Segurança errado/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }


            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table#tbl_lista_processos tbody tr td a.numprocesso')));
                $driver->findElement(WebDriverBy::cssSelector('table#tbl_lista_processos tbody tr td a.numprocesso'))->click();

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="dados"]/table/tbody/tr')));
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="dados"]/table/tbody/tr'));

                $dados = [];
                $head = [];
                $body = [];

                foreach ($rows as $row) {
                    $headAux = $row->findElements(WebDriverBy::tagName('th'));
                    foreach ($headAux as $h) {
                        $head[] = $robo->cleanString($h->getText());
                    }
                    $bodyAux = $row->findElements(WebDriverBy::tagName('td'));
                    foreach ($bodyAux as $b) {
                        $body[] = $robo->cleanString($b->getText());
                    }
                }

                foreach ($head as $index => $title) {
                    $dados[$title] = $body[$index];
                }

                $vara = '';
                if (preg_match('/Juizado Especial/', $dados['Classe'])) {
                    $vara = $dados['Classe'];
                } else {
                    if (preg_match('/Recurso/', $dados['Classe'])) {
                        $vara = $dados['Competência'];
                    } else {
                        if (preg_match('/Procedimento Comum/', $dados['Classe'])) {
                            $vara = $dados['Competência'];
                        } else {
                            if (preg_match('/Agravo/', $dados['Classe'])) {
                                $vara = $dados['Procedência'];
                            }
                        }
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }


    /**
     * BA - RODANDO SITE QUE NÃO FOI PROGRAMADO PQ PARECIA
     * NÃO FUNCIONAR OU FOI MIGRADO
     */

    public function tjbafisico($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL BAHIA PROCESSOS FÍSICOS";

        $robo->log($title);
        $driver = $robo->startDriver();

        //http://www2.tjba.jus.br/consultaprocessual/primeirograu/numero.wsp?parametro=3805994-0/2011&wi.page.prev=primeirograu/processoparte
        $linkGet = "http://www2.tjba.jus.br/consultaprocessual/juizadocivel/numero.wsp?parametro=[NRO_PROCESSO]&wi.page.prev=juizadocivel/processoparte";
//        $linkGet = "http://www2.tjba.jus.br/consultaprocessual/primeirograu/numero.wsp?parametro=[NRO_PROCESSO]&wi.page.prev=primeirograu/processoparte";
        $session = "http://www2.tjba.jus.br/consultaprocessual/index.wsp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//            if (strlen($nroProcesso) < 10) continue;
//            if (!$robo->yearValidate($nroProcesso,0)) continue;
//            if (!$nroProcesso) continue;
            if (!$processo->getNumProcJust()) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $nroProcesso = $processo->getNumProcJust();

            $driver->navigate()->to($session);
            $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($title, $processo->getNumProcJust());

            try {
                $vara = '';

                //primeirograu
//                $vara = $driver->findElement(WebDriverBy::xpath('/html/body/table[3]/tbody/tr/td[4]'))->getText();
                //juizadocivel
                $vara = $driver->findElement(WebDriverBy::xpath('/html/body/table[3]/tbody/tr/td[2]'))->getText();

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function tjba2graupje($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();

        //8000866-93.2015.8.05.0109
        $link = 'https://pje2g.tjba.jus.br/pje-web/ConsultaPublica/listView.seam';

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            if (!preg_match('/.8./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) != 8) continue;
            $processosTratados[] = $processo;

        }

        $driver = $robo->startDriver();
        $driver->get($link);

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;
        $captcha = '';
        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcessoDecoration:numProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcessoDecoration:numProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();
//            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $msg = $driver->findElement(WebDriverBy::className('rich-messages'))->getText();
                $robo->log($msg);
                if (!preg_match('/A verificação de captcha não está correta/', $msg)) continue;
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE 440 => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($link) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO!");
                continue;
            }
        }

        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $driver->quit();
        return true;
    }

    //TRIBUNAL RIO GRANDE DO NORTE - RN

    public function tjrnesaj1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RIO GRANDE DO NORTE - RN 1° GRAU ESAJ";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://esaj.tjrn.jus.br/cpo/pg/open.do";
        $driver->get($robo->host);

        $cnj = false;
        $robo->log("TIPO NRO, É CNJ => {$cnj}");

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if ($cnj) {
                if (!preg_match('/8.20/', $nroProcesso)) continue;
            }
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $driver->navigate()->to($linkGet);
//            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());


            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {

                if ($cnj) {
                    $nroProcessoArr = explode('.8.20.', $nroProcesso);
                    if (count($nroProcessoArr) < 2) continue;
                    $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);
                } else {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//2° LEVA
                    $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesaquisar"]'))->click();
                }


                $target = '/html/body/table[4]/tbody/tr/td/table[2]/tbody/tr';
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($target));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace(';', '', $varaAux));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrnesaj2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RIO GRANDE DO NORTE - RN 2° GRAU ESAJ";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://esaj.tjrn.jus.br/cposg/";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $driver->navigate()->to($linkGet);
            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {

                //OUTRO NRO PROCESSO - SEGUNDA LEVA
//                $driver->findElement(WebDriverBy::xpath('//*[@id="comboTipoPesquisa"]'))->sendKeys('Outro Nº do Processo');

                $driver->findElement(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[2]/td[3]/input[2]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/table/tbody/tr/td/div/form/table/tbody/tr/td/table/tbody/tr[3]/td[3]/input'))->click();

                try {
                    $alertText = $driver->switchTo()->alert()->getText();
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    continue;
                } catch (\Exception $e) {
                }

                $target = '/html/body/table[4]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/table[3]/tbody/tr';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($target));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                $campo = 'Órgão Julgador';
                if (isset($dadosProcesso[$campo]) && $dadosProcesso[$campo] != '') {
                    $vara = $robo->cleanString($dadosProcesso[$campo], ['; ', ';']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrnesajjuizadoespecial($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RIO GRANDE DO NORTE - RN JUIZADO ESPECIAL ESAJ";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://esaj.tjrn.jus.br/cje/pesquisaProcessosWeb.do";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//            if (strlen($nroProcesso) < 10) continue;
//            if (!$robo->yearValidate($nroProcesso,0)) continue;
//            if (!$nroProcesso) continue;
            if (!$processo->getNumProcJust()) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->get($linkGet);

//            $nroProcesso = '011080030772';

//            $driver->findElement(WebDriverBy::cssSelector('select[name=deTipoPesquisa]'))->sendKeys('Outro Nº do Processo');//2° LEVA
            $driver->findElement(WebDriverBy::cssSelector('input[name=flAtivos]'))->click();
            $driver->findElement(WebDriverBy::cssSelector('input[name=dePesquisa]'))->clear();
            $driver->findElement(WebDriverBy::cssSelector('input[name=dePesquisa]'))->sendKeys($nroProcesso);

            $driver->findElement(WebDriverBy::xpath('/html/body/table[4]/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[2]/td[5]/input'))->click();
            try {
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('td.errors'))->getText());
                $robo->log($msg);

                if (preg_match('/O código de autenticação está incorreto ou não foi informado/', $msg)) {
//                    $driver->findElement(WebDriverBy::cssSelector('select[name=deTipoPesquisa]'))->sendKeys('Outro Nº do Processo');//2° LEVA
                    $driver->findElement(WebDriverBy::cssSelector('input[name=flAtivos]'))->click();
                    $driver->findElement(WebDriverBy::cssSelector('input[name=dePesquisa]'))->clear();
                    $driver->findElement(WebDriverBy::cssSelector('input[name=dePesquisa]'))->sendKeys($nroProcesso);
                    $driver = $robo->hasCaptcha(
                        $driver,
                        'xpath',
                        '/html/body/table[4]/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[1]/td[4]/img',
                        '/html/body/table[4]/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[2]/td[4]/input',
                        '/html/body/table[4]/tbody/tr/td/form/table/tbody/tr/td/table/tbody/tr[2]/td[5]/input',
                        false
                    );
                } else {
                    continue;
                }
            } catch (\Exception $e) {
            }

            try {
                $vara = '';

                $target = '/html/body/table[4]/tbody/tr/td/table[3]/tbody/tr';
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($target));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $qtd = preg_match_all('(Sorteio - [0-9]{2}[/][0-9]{2}[/][0-9]{4} às [0-9]{2}[:][0-9]{2})', $dadosProcesso['Distribuição'], $match);
                    $vara = ($qtd > 0) ? str_replace($match[0], '', $dadosProcesso['Distribuição']) : $dadosProcesso['Distribuição'];
                    $vara = $robo->cleanString($vara, ['; ', ';']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrnesajjuizadoespecialcreta($processos)
    {
        //https://juizado.jfrn.jus.br/cretainternetrn/consulta/processo/pesquisar2.wsp
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RIO GRANDE DO NORTE/RN JF CRETA";

        $robo->log($title);
        $driver = $robo->startDriver(false, false);

        $linkGet = "https://juizado.jfrn.jus.br/cretainternetrn/";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
            if (strlen($nroProcesso) < 15) continue;
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {


            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[3]/td/form/table[3]/tbody/tr/td[2]/input')));

            $driver->findElement(WebDriverBy::xpath('/html/body/table/tbody/tr[3]/td/form/table[3]/tbody/tr/td[2]/input'))->sendKeys($nroProcesso);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '/html/body/table/tbody/tr[3]/td/form/table[6]/tbody/tr/td[2]/a[1]/img',
                    '/html/body/table/tbody/tr[3]/td/form/table[6]/tbody/tr/td[2]/input',
                    '//*[@id="btn_pesquisar"]', false);

                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $robo->cleanString($driver->switchTo()->alert()->getText());
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    if (preg_match('/Código da imagem de segurança inválido/', $alertText)) continue;
                    if (preg_match('/O código de segurança deve ser preenchido/', $alertText)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $result = $robo->cleanString($driver->findElement(WebDriverBy::className('grid_center'))->getText());
                $robo->log($result);
                if ($result == 'Nenhum processo encontrado.') continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('grid')));

                $vara = '';
                $cells = $driver->findElements(WebDriverBy::cssSelector('td.grid'));
                foreach ($cells as $cell) {
                    $cellText = $cell->getText();
                    if (preg_match('/Juizado: /', $cellText)) {
                        $vara = $robo->cleanString($cellText, ['Juizado: ']);
                        break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrnprojudi($processos)
    {
        //https://projudi.tjrn.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=00100361620188200139
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = __FUNCTION__;
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://projudi.tjrn.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[NRO_PROCESSO_SEM_MASCARA]";
        $session = "https://projudi.tjrn.jus.br/projudi/publico/buscas/ProcessosParte?publico=true";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($session);
            $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
            $driver->navigate()->to($link);

            $robo->saveCache(__FUNCTION__, $nroProcesso);


            try {
                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrnjf($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RIO GRANDE DO NORTE/RN - JF";
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://consulta.jfrn.jus.br/consultatebas/cons_procs.asp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            /*$nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if (!$nroProcesso) continue;
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
                if (substr($nroProcesso, 0, 4) < 1970 || substr($nroProcesso, 0, 4) > date('Y')) continue;
            }
            if ($params['tipoNro'] == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;
            if ($codeT) if (!preg_match("/$codeT/", $nroProcesso)) continue;*/

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            /*if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
            }

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);*/

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($linkGet);

            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

//            $nroProcesso = '0006067-39.2011.4.05.8400';
//            $nroProcesso = '0005947-59.2012.4.05.8400';
            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();

            try {
                $iframe = '//*[@id="ResConsProc"]/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td/table/tbody/tr/td[2]/p/font/span/iframe';
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($iframe)));
                $iframe = $driver->findElement(WebDriverBy::xpath($iframe));
                $driver->switchTo()->frame($iframe);

                $textarea = $driver->findElement(WebDriverBy::xpath('//*[@id="Resumo"]'))->getText();
                $textareaArr = explode("\n", $textarea);
                $vara = '';
                foreach ($textareaArr as $textItem) {
                    if ((preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
                            preg_match('/([0-9]{1}) a. VARA FEDERAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª TURMA RECURSAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        break;
                    }
                }

//                $row = '//*[@id="ResInfoProc"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]';
//                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($row)));
//                $conteudoVara = $driver->findElement(WebDriverBy::xpath($row))->getText();
//
//                $textToFind = 'Localização ';
//                if (!preg_match("/$textToFind/", $conteudoVara)) continue;
//                $vara = $robo->cleanString($conteudoVara, [$textToFind]);

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                } else {
                    $robo->log("############## => NÃO LOCALIZOU O CAMPO CORRETO!!! <= ##############");
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrnpje1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL RIO GRANDE DO NORTE/RN PJE 1° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje.tjrn.jus.br/consulta1grau/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.20./', $nroProcesso)) continue;
//            if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:dnp:nomeParte"]'))->click();

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="fPP:j_id141:captchaImg"]',
                    '//*[@id="fPP:j_id141:verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrnpje2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL RIO GRANDE DO NORTE/RN PJE 2° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje.tjrn.jus.br/consulta2grau/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.20./', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 2) != '08') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="fPP:j_id141:captchaImg"]',
                    '//*[@id="fPP:j_id141:verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    //TRIBUNAL ALAGOAS - AL
    public function tjaljf($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL ALAGOAS/AL - JF";
        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://tebas.jfal.jus.br/consulta/cons_procs.asp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($linkGet);

            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();

            try {
                $iframe = '//*[@id="ResConsProc"]/table/tbody/tr[1]/td/table[2]/tbody/tr[3]/td/table/tbody/tr/td[2]/p/font/span/iframe';

                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($iframe)));

                $iframe = $driver->findElement(WebDriverBy::xpath($iframe));
                $driver->switchTo()->frame($iframe);

                $textarea = $driver->findElement(WebDriverBy::xpath('//*[@id="Resumo"]'))->getText();
                $textareaArr = explode("\n", $textarea);
                $vara = '';

                foreach ($textareaArr as $textItem) {
                    if ((preg_match('/a. Vara Federal/i', $textItem) ||
                            preg_match('/a. VARA FEDERAL/i', $textItem) ||
                            preg_match('/ª TURMA RECURSAL/i', $textItem) ||
                            preg_match('/ª Turma Recursal/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjalesaj1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL ALAGOAS/AL 1° GRAU ESAJ";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://www2.tjal.jus.br/cpopg/open.do";
        $driver->get($robo->host);

        $cnj = false;

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (strlen($nroProcesso) == 25 && !preg_match('/8.02/', $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $driver->navigate()->to($linkGet);
//            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {

                if ($cnj) {
                    $nroProcessoArr = explode('.8.02.', $nroProcesso);
                    if (count($nroProcessoArr) < 2) continue;
                    $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);
                } else {
                    $nroProcesso = $robo->onlyDigits($nroProcesso);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);
                }
                $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

                $target = '/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr';
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($target));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace(';', '', $varaAux));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjalesaj2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL ALAGOAS/AL 2° GRAU ESAJ";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://www2.tjal.jus.br/cposg5/open.do";
        $driver->get($robo->host);

        $cnj = false;

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (strlen($nroProcesso) == 25 && !preg_match('/8.02/', $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $driver->navigate()->to($linkGet);
//            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {

                if ($cnj) {
                    $nroProcessoArr = explode('.8.02.', $nroProcesso);
                    if (count($nroProcessoArr) < 2) continue;
                    $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);
                } else {
                    $nroProcesso = $robo->onlyDigits($nroProcesso);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                }


                try {
                    $alertText = $driver->switchTo()->alert()->getText();
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    continue;
                } catch (\Exception $e) {
                }

                $target = '/html/body/div[1]/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($target));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                $campo = 'Distribuição';
                if (isset($dadosProcesso[$campo]) && $dadosProcesso[$campo] != '') {
                    $vara = $robo->cleanString($dadosProcesso[$campo], ['; ', ';']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjaljfpje($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL ALAGOAS/AL JF PJE";

        $robo->log($title);
        $driver = $robo->startDriver(false, false);

        $linkGet = "https://pje.jfal.jus.br/pje/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/4.05/', $nroProcesso)) continue;
            //if (!$robo->yearValidate($nroProcesso, 11)) continue;
            //if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

//            $nroProcesso = '0811513-16.2017.4.05.8000';

            $driver->executeScript("document.getElementById(\"consultaPublicaForm:Processo:ProcessoDecoration:Processo\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="consultaPublicaForm:captcha:captchaImg"]',
                    '//*[@id="consultaPublicaForm:captcha:j_id224:verifyCaptcha"]',
                    '//*[@id="consultaPublicaForm:pesq"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a')));

                $linkAux = $driver->findElement(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a'))->getAttribute('onclick');
                $piece = '/pje/ConsultaPublica/DetalheProcessoConsultaPublica/listView.seam?signedIdProcessoTrf=';
                $token = $robo->findBetween($linkAux, $piece, "');;A4J.AJAX.Submit(");
                $link = $piece . $token;
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::className('propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    try {
                        if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                            $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                            break;
                        }
                    } catch (\Exception $e) {
                    }
                }
                if ($vara == '') {
                    foreach ($cells as $cell) {
                        try {
                            if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                                $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                                break;
                            }
                        } catch (\Exception $e) {
                        }
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjalprojudi($processos)
    {
        //https://www5.tjal.jus.br/projudi/processo/consultaPublica.do?_tj=8a6c53f8698c7ff7826b4c776d71316d3a6b758b3d398283
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL ALAGOAS PROJUDI";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://www5.tjal.jus.br/projudi/processo.do?actionType=visualizar&id=[NRO_PROCESSO]";
        $session = "https://www5.tjal.jus.br/projudi/processo/consultaPublica.do?actionType=iniciar";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $driver->navigate()->to($session);
            $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {
                $vara = '';

                $target = '//*[@id="includeContent"]/fieldset/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));
                $conteudoProcesso = $driver->findElement(WebDriverBy::xpath($target))->getText();
                $textToFind = 'Juízo:';
                $array = explode($textToFind, $conteudoProcesso);
                if (count($array) < 2) {
                    $robo->log("PROCESSO NÃO ENCONTRADO");
                    continue;
                }

                $vara = substr($array[1], 0, strpos($array[1], "\n"));
                $vara = $robo->cleanString($vara, [': ', ':']);

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    //TRIBUNAL AMAZONAS - AM
    public function tjamesaj1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL AMAZONAS/AM 1° GRAU ESAJ";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://consultasaj.tjam.jus.br/cpopg/open.do";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        $colorsCaptcha = [
            'VERMELHO' => 'RED',
            'ROXO' => 'PURPLE',
            'PRETO' => 'BLACK',
            'LARANJA' => 'ORANGE',
            'VERDE' => 'GREEN',
            'ROSA' => 'PINK',
            'AZUL' => 'BLUE',
        ];

        $driver->navigate()->to($linkGet);

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {

                $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);

                try {
                    $labelComment = $driver->findElement(WebDriverBy::xpath('//*[@id="labelValorCaptcha"]'))->getText();
                    //CAPTCHA
                    while (true) {

                        $labelComment = $driver->findElement(WebDriverBy::xpath('//*[@id="labelValorCaptcha"]'))->getText();
                        $colorPicked = '';
                        foreach ($colorsCaptcha as $colorPortuguese => $colorEnglish) {
                            if (preg_match("/$colorPortuguese/", $labelComment)) {
                                $colorPicked = $colorEnglish;
                                break;
                            }
                        }
                        if ($colorPicked == '') {
                            $driver->findElement(WebDriverBy::xpath('//*[@id="captchaInfo"]/ul/li[2]/a'))->click();
                            sleep(2);
                            continue;
                        }

                        $textComment = "type {$colorPicked} symbols only";

                        $driver = $robo->hasCaptcha(
                            $driver,
                            'xpath',
                            '//*[@id="captchaCodigo"]',
                            '//*[@id="valorCaptcha"]',
                            '//*[@id="pbEnviar"]',
                            false,
                            null,
                            true,
                            $textComment
                        );

                        try {
                            $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                            $msg = $driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText();
                            $robo->log($msg);
                            if (preg_match('/Digite aqui todas as letras/', $msg)) {
                                $driver->findElement(WebDriverBy::xpath('//*[@id="captchaInfo"]/ul/li[2]/a'))->click();
                                sleep(2);
                                continue;
                            }
                            break;
                        } catch (\Exception $e) {
                            break;
                        }

                    }
                } catch (\Exception $e) {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();
                }

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                    $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                    $processo->addObs("SEGREDO DE JUSTIÇA => {$driver->getCurrentURL()}");
                    $processo->salvar(true);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                    sleep(1);
                    continue;
                } catch (\Exception $e) {
                }

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                    $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                    $processo->addObs("LISTA DE PROCESSOS => {$driver->getCurrentURL()}");
                    $processo->salvar(true);
                    continue;
                } catch (\Exception $e) {
                }

                $target = '/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr';
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($target));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace(';', '', $varaAux));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjamesaj2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL AMAZONAS/AM 2° GRAU ESAJ";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://consultasaj.tjam.jus.br/cposgcr/open.do";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        $colorsCaptcha = [
            'VERMELHO' => 'RED',
            'ROXO' => 'PURPLE',
            'PRETO' => 'BLACK',
            'LARANJA' => 'ORANGE',
            'VERDE' => 'GREEN',
            'ROSA' => 'PINK',
            'AZUL' => 'BLUE',
        ];

        foreach ($processosTratados as $processo) {

            $driver->navigate()->to($linkGet);

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {

                $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();
                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);

                try {
                    $labelComment = $driver->findElement(WebDriverBy::xpath('//*[@id="labelValorCaptcha"]'))->getText();
                    //CAPTCHA
                    while (true) {

                        $labelComment = $driver->findElement(WebDriverBy::xpath('//*[@id="labelValorCaptcha"]'))->getText();
                        $colorPicked = '';
                        foreach ($colorsCaptcha as $colorPortuguese => $colorEnglish) {
                            if (preg_match("/$colorPortuguese/", $labelComment)) {
                                $colorPicked = $colorEnglish;
                                break;
                            }
                        }
                        if ($colorPicked == '') {
                            $driver->findElement(WebDriverBy::xpath('//*[@id="captchaInfo"]/ul/li[2]/a'))->click();
                            sleep(2);
                            continue;
                        }

                        $textComment = "type {$colorPicked} symbols only";

                        $driver = $robo->hasCaptcha(
                            $driver,
                            'xpath',
                            '//*[@id="captchaCodigo"]',
                            '//*[@id="valorCaptcha"]',
                            '//*[@id="botaoPesquisar"]',
                            false,
                            null,
                            true,
                            $textComment
                        );

                        try {
                            $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                            $msg = $driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText();
                            $robo->log($msg);
                            if (preg_match('/Digite aqui todas as letras/', $msg)) {
                                $driver->findElement(WebDriverBy::xpath('//*[@id="captchaInfo"]/ul/li[2]/a'))->click();
                                sleep(2);
                                continue;
                            }
                            break;
                        } catch (\Exception $e) {
                            break;
                        }

                    }
                } catch (\Exception $e) {
                    $driver->findElement(WebDriverBy::xpath('//*[@id="botaoPesquisar"]'))->click();
                }

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                    $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                    $processo->addObs("SEGREDO DE JUSTIÇA => {$driver->getCurrentURL()}");
                    $processo->salvar(true);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                    sleep(1);
                    continue;
                } catch (\Exception $e) {
                }

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                    $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                    $processo->addObs("LISTA DE PROCESSOS => {$driver->getCurrentURL()}");
                    $processo->salvar(true);
                    continue;
                } catch (\Exception $e) {
                }

                $target = '/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr';
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath($target));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $vara = $robo->cleanString($dadosProcesso['Distribuição'], ['; ', ';']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjamprojudi($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL AMAZONAS/AM PROJUDI";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://projudi.tjam.jus.br:8082/projudi/processo/consultaPublicaNova.do?actionType=iniciar";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $processo->getNumProcJust();//NRO ANTIGO
            if (!$nroProcesso) continue;
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (preg_match('/8.04/', $nroProcesso)) continue;//NRO ANTIGO

            //NRO ANTIGO

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());//NRO ANTIGO

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->get($linkGet);

            //$nroProcesso = '0000002-32.2014.8.04.5000';

            $driver->findElement(WebDriverBy::xpath('//*[@id="secaoFormConsulta"]/tbody/tr[3]/td[2]/table/tbody/tr/td/input[3]'))->click();//NRO ANTIGO
            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->sendKeys($nroProcesso);

            //CAPTCHA
            try {
                $iframeSrc = $driver->findElement(WebDriverBy::xpath('//*[@id="recaptcha"]/div/div/iframe'))->getAttribute('src');
                $googleKey = $robo->findBetween($iframeSrc, 'anchor?ar=1&k', '&hl=');//ADD = AO KEY PARA QUE O METODO RECONHEÇA QUE É A CHAVE AO INVES DO ELEMENT CSS
                $robo->log($googleKey);
                $captcha = $robo->hasReCaptcha($driver, $googleKey);
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="pesquisar"]'))->click();
            } catch (\Exception $e) {
                die;
            }

            try {

                $target = '//*[@id="includeContent"]/fieldset/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));
                $conteudoProcesso = $driver->findElement(WebDriverBy::xpath($target))->getText();
                $textToFind = 'Juízo:';
                $array = explode($textToFind, $conteudoProcesso);
                if (count($array) < 2) {
                    $robo->log("PROCESSO NÃO ENCONTRADO");
                    continue;
                }

                $vara = substr($array[1], 0, strpos($array[1], "\n"));
                $vara = $robo->cleanString($vara, [': ', ':']);

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    //TRIBUNAL RONDÔNIA/RO
    public function tjroprojudi($processos)
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RONDÔNIA/RO - PROJUDI";
        $robo->log($title);
        $driver = $robo->startDriver();

        $session = "http://projudi.tjro.jus.br/projudi/interno.jsp?endereco=/projudi/buscas/ProcessosParte";
        $linkGet = "http://projudi.tjro.jus.br/projudi/buscas/ProcessosParte";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $cnj = false;


            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

//            $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
            $driver->navigate()->to($session);
            $driver->navigate()->to($linkGet);

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            $targetInput = '//*[@id="numeroProcesso"]';

            if ($cnj) $targetInput = '//*[@id="numeroProcesso"]';

            $driver->findElement(WebDriverBy::xpath($targetInput))->clear();
            $driver->findElement(WebDriverBy::xpath($targetInput))->sendKeys($nroProcesso);
            $driver->findElement(WebDriverBy::xpath('/html/body/form/p/table/tbody/tr[4]/td[2]/input[2]'))->click();//PROMOVIDO
            sleep(1);
            $driver->findElement(WebDriverBy::xpath('/html/body/form/p/table/tbody/tr[19]/td/input'))->click();


            try {
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/div/div[1]/p'))->getText());
                if (!preg_match('/Nenhum resultado encontrado/', $msg)) {
                    die;
                }
            } catch (\Exception $e) {
            }
            continue;


            try {
                $vara = '';

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrofisico1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RONDÔNIA/RO - FÍSICO 1° GRAU";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://www.tjro.jus.br/appg/pages/index.xhtml";
        $driver->get($robo->host);

        $cnj = true;

        $processosTratados = [];
        foreach ($processos as $processo) {

            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.22/', $nroProcesso)) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######.##.####.###.####');
//            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

//            $nroProcesso = '0000971.71.2015.822.0001';

            $driver->get($linkGet);

            if ($cnj) {
                $pieceProcesso = explode('.', $nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrproccnj"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocdig"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocano"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocorigem"]'))->clear();

                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrproccnj"]'))->sendKeys($pieceProcesso[0]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocdig"]'))->sendKeys($pieceProcesso[1]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocano"]'))->sendKeys($pieceProcesso[2]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocorigem"]'))->sendKeys($pieceProcesso[4]);

                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:cb4"]'))->click();
            } else {
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc"]/h3[2]'))->click();
                sleep(2);

                $comarcasOptions = '<option value="17">Alta Floresta</option><option value="11">Alvorada do Oeste</option><option value="2">Ariquemes</option><option value="21">Buritis</option><option value="7">Cacoal</option><option value="13">Cerejeiras</option><option value="12">Colorado do Oeste</option><option value="16">Costa Marques</option><option value="8">Espigão do Oeste</option><option value="15">Guajará&nbsp;-Mirim </option><option value="3">Jaru</option><option value="5">Ji-Paraná</option><option value="19">Machadinho do Oeste</option><option value="20">Nova Brasilândia do Oeste</option><option value="4">Ouro Preto do Oeste</option><option value="9">Pimenta Bueno</option><option value="1" selected="selected">Porto Velho</option><option value="6">Presidente Médici</option><option value="10">Rolim de Moura</option><option value="18">Santa Luzia do Oeste</option><option value="22">São Miguel do Guaporé</option><option value="23">São Francisco do Guaporé</option><option value="14">Vilhena</option>';

                $codeComarca = '';
                $doc = new \DOMDocument();
                $doc->loadHTML($comarcasOptions);
                $comarcas = [];
                foreach ($doc->getElementsByTagName('option') as $comarca) {
                    $codeComarca = $comarca->getAttribute('value');
                    similar_text(strtoupper($comarca->nodeValue), $processo->getDesComarca(), $percent);
                    $comarcas[$percent] = $codeComarca;
                }
                ksort($comarcas);
                $comarcaCod = end($comarcas);

                $inputComarca = '<input type="hidden" name="formNovo:idarc:strCodigoComarca_input" value="' . $comarcaCod . '"/>';
                $inputTipoPesq = '<input type="hidden" name="formNovo:idarc:strTpConsulta_input" value="2"/>';

                $append = $inputComarca . $inputTipoPesq;
                $append = "$('#formNovo').prepend('{$append}');";

                $driver->executeScript($append, []);

                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:parametroConsulta"]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:cb1"]'))->click();
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.ui-messages.ui-widget')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('.ui-messages.ui-widget'))->getText());
                $robo->log($msg);
                if (preg_match('/O Processo informado não existe/', $msg)) continue;
                if (preg_match('/O Número do Processo informado é inválido/', $msg)) continue;
            } catch (\Exception $e) {
            }

            try {

                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('frm:j_idt63')));

                $rows = $driver->findElements(WebDriverBy::className('ui-grid-row'));
                $vara = '';
                foreach ($rows as $row) {
                    $text = $robo->cleanString($row->getText());
                    if (preg_match('/Vara:/', $text)) {
                        $vara = $robo->cleanString($text, ['Vara:', ':']);
                        break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrofisico2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RONDÔNIA/RO - FÍSICO 2° GRAU";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://www.tjro.jus.br/apsg/pages/index.xhtml";
        $driver->get($robo->host);

        $cnj = true;

        $processosTratados = [];
        foreach ($processos as $processo) {

            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!preg_match('/8.22/', $nroProcesso)) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

//            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust(), '#######.##.####.###.####');

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

//            $nroProcesso = '0007928.88.2015.822.0001';

            $driver->get($linkGet);

            if ($cnj) {
                $pieceProcesso = explode('.', $nroProcesso);

                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrproccnj"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocdig"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocano"]'))->clear();
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocorigem"]'))->clear();

                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrproccnj"]'))->sendKeys($pieceProcesso[0]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocdig"]'))->sendKeys($pieceProcesso[1]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocano"]'))->sendKeys($pieceProcesso[2]);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:nrprocorigem"]'))->sendKeys($pieceProcesso[4]);

                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:cb4"]'))->click();
            } else {
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc"]/h3[2]'))->click();
                sleep(2);

                $inputTipoPesq = '<input type="hidden" name="formNovo:idarc:strTpConsulta_input" value="0"/>';

                $append = "$('#formNovo').prepend('{$inputTipoPesq}');";
                $driver->executeScript($append, []);

                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:parametroConsulta"]'))->sendKeys($nroProcesso);
                $driver->findElement(WebDriverBy::xpath('//*[@id="formNovo:idarc:cb1"]'))->click();
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.ui-messages.ui-widget')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('.ui-messages.ui-widget'))->getText());
                $robo->log($msg);
                if (preg_match('/O Processo informado não existe/', $msg)) continue;
                if (preg_match('/O Processo informado não foi encontrado/', $msg)) continue;
                if (preg_match('/Parâmetro informado não foi encontrado/', $msg)) continue;
            } catch (\Exception $e) {
            }

            try {

                $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('frm:j_idt64')));

                $rows = $driver->findElements(WebDriverBy::className('ui-grid-row'));
                $vara = '';
                foreach ($rows as $row) {
                    $text = $robo->cleanString($row->getText());
                    if (preg_match('/Órgão Julgador:/', $text)) {
                        $vara = $robo->cleanString($text, ['Órgão Julgador:', ':']);
                        break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjropje1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL RONDÔNIA/RO PJE 1° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://pje.tjro.jus.br/pg/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.22./', $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);
            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $idDinamico = $driver->findElement(WebDriverBy::cssSelector('img[id$="captchaImg"]'))->getAttribute('id');
                $idDinamico = str_replace('captchaImg', '', $idDinamico);

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="' . $idDinamico . 'captchaImg"]',
                    '//*[@id="' . $idDinamico . 'verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjropje2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL RONDÔNIA/RO PJE 2° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://pje.tjro.jus.br/sg/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.22./', $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $idDinamico = $driver->findElement(WebDriverBy::cssSelector('img[id$="captchaImg"]'))->getAttribute('id');
                $idDinamico = str_replace('captchaImg', '', $idDinamico);

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="' . $idDinamico . 'captchaImg"]',
                    '//*[@id="' . $idDinamico . 'verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    /**
     * PARAÍBA
     * rodrigo.silva
     */
    public function ejustjpbrecurso($processos)
    {
        $robo = new Robo();
        $robo->log(__FUNCTION__);
        $driver = $robo->startDriver();
        $url = 'https://ejus.tjpb.jus.br/projudi/interno.jsp?endereco=/projudi/ConsultaPublica.jsp';
        $driver->get($robo->host);

        $total = count($processos);
        $index = 1;
        $encontrados = 0;
        foreach ($processos as $processo) {

            $nroProcesso = $processo->getNumProcJust();
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $nroCnj = $robo->onlyDigits($processo->getNumProcJust());
//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/8.15/', $nroCnj)) continue;

            try {
                $url = 'https://ejus.tjpb.jus.br/projudi/ConsultaPublica.jsp';
                $driver->get($url);
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabs"]/a[2]'))->click();
                $iframe = $driver->findElement(WebDriverBy::tagName('iframe'));
                $driver->switchTo()->frame($iframe);

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="numeroRecurso"]')));
                $driver
                    ->findElement(WebDriverBy::xpath('//*[@id="numeroRecurso"]'))
                    ->sendKeys($nroCnj)
                    ->sendKeys(WebDriverKeys::ENTER);

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="item"]/tbody/tr[1]/td[1]/a')));
                $driver->findElement(WebDriverBy::xpath('//*[@id="item"]/tbody/tr[1]/td[1]/a'))->click();

                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="recursoPrincipal"]/tbody/tr')));
                $linhas = $driver->findElements(WebDriverBy::xpath('//*[@id="recursoPrincipal"]/tbody/tr'));

                $dados = $robo->extractVerticalTable($linhas, 2);

                debug($dados);

                if (isset($dados['Turma:']) && $dados['Turma:'] != '') {
                    $vara = $robo->cleanString($dados['Turma:']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    /**
     * RIO DE JANEIRO
     * rodrigo.silva
     */
    public function tjrjfisicos($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log('##### RODAR A NOITE NÃO TEM CAPTCHA #####');
        $title = "TRIBUNAL RIO DE JANEIRO";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = [
            '//*[@id="content"]/form/table/tbody/tr' => "http://www4.tjrj.jus.br/consultaProcessoWebV2/consultaProc.do?v=2&numProcesso=[NRO_PROCESSO]&FLAGNOME=S&tipoConsulta=publica&back=1&PORTAL=1&v=2",
            '//*[@id="conteudo"]/span/table[3]/tbody/tr' => "http://www4.tjrj.jus.br/ejud/ConsultaProcesso.aspx?N=[NRO_PROCESSO]&back=1&PORTAL=1&v=2"
        ];

        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {


//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//            if (strlen($nroProcesso) < 10) continue;
//            if (!$robo->yearValidate($nroProcesso,0)) continue;
//            if (!$nroProcesso) continue;
            if (!$processo->getNumProcJust()) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $nroProcesso = $processo->getNumProcJust();

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            foreach ($linkGet as $target => $link) {

                $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $link));

                try {
                    while (true) {
                        $robo->hasCaptcha(
                            $driver,
                            'xpath',
                            '//*[@id="imgCaptcha"]',
                            '//*[@id="captcha"]',
                            '//*[@id="container_captcha"]/fieldset/table/tbody/tr[2]/td[2]/input',
                            false);
                        $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="container_captcha"]/fieldset/table/tbody/tr[1]/td[2]/font/b'))->getText());
                        if (preg_match('/caracteres informados estão inválidos/', $msg)) continue;
                        $alertText = $driver->switchTo()->alert()->getText();
                        if (preg_match('/O Código de Segurança deve ser informado/', $alertText)) {
                            $driver->switchTo()->alert()->accept();
                            continue;
                        }
                        break;
                    }
                } catch (\Exception $e) {
                }

                try {
                    $rows = [];
                    $vara = '';

                    $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                    try {
                        //VERIFICANDO OCORRENCIA DO NRO ORIGINARIO
                        $targetOriginario = '//*[@id="conteudo"]/span/table[3]/tbody/tr';
                        $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($targetOriginario)));
                        $rows = $driver->findElements(WebDriverBy::xpath($targetOriginario));
                        $linkOriginario = null;
                        foreach ($rows as $row) {
                            if (preg_match('/Processo originário/', $row->getText())) {
                                $linkOriginario = $row->findElement(WebDriverBy::tagName('a'))->getAttribute('href');
                            }
                        }
                        if ($linkOriginario) {
                            $targetOriginario = '//*[@id="content"]/form/table/tbody/tr';
                            $driver->navigate()->to($linkOriginario);
                            $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($targetOriginario)));
                            $rows = $driver->findElements(WebDriverBy::xpath($targetOriginario));

                            $dados = $robo->extractVerticalTable($rows, 2);

                            foreach ($dados as $key => $val) {
                                if (preg_match('/Comarca /', $key)) {
                                    $vara = $robo->cleanString($val);
                                    break;
                                }

                                if (preg_match('/Regional d/', $key)) {
                                    $varaAux = explode(';', $val);
                                    $vara = $robo->cleanString($varaAux[0], ['; ', ';']);
                                    break;
                                }
                            }

                            if ($vara != '') {
                                $vara = $robo->cleanString($vara);
                                $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                                $encontrados++;
                                break;
                            }
                        }
                    } catch (\Exception $e) {
                    }

                    $rows = $driver->findElements(WebDriverBy::xpath($target));

                    $dados = $robo->extractVerticalTable($rows, 2);

                    foreach ($dados as $key => $val) {
                        if (preg_match('/Comarca /', $key)) {
                            $vara = $robo->cleanString($val);
                            break;
                        }

                        if (preg_match('/Regional d/', $key)) {
                            $vara = $robo->cleanString($val);
                            break;
                        }
                    }

                    if ($vara != '') {
                        $vara = $robo->cleanString($vara);
                        $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                        $encontrados++;
                        break;
                    }

                } catch (\Exception $e) {
                    $robo->log("PROCESSO NÃO ENCONTRADO!");
                    continue;
                }
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrjfisicosFixCamara($processos)
    {
        /**
         * ATUALIZAR PROCESSOS QUE TRF1 COM VALOR ERRADO JUIZO/VARA (TURMA RECURSAL) E OUTROS TRIBUNAIS
         * filtrando processos que possuem valor TR - RELATOR ou CAMARA
         * 14/08/2018
         */

        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RIO DE JANEIRO - FIX CAMARA";

        $robo->log($title);
        $driver = $robo->startDriver();
        $driver->get($robo->host);

        foreach ($processos as $processo) {

            if ($processo->getOrigem() == 'JSON') {
                $robo->log("NO LINK");
                continue;
            }
            $driver->navigate()->to($processo->getOrigem());

            try {
                $rows = [];
                $vara = '';

                $targetOriginario = '//*[@id="conteudo"]/span/table[3]/tbody/tr';
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($targetOriginario)));
                $rows = $driver->findElements(WebDriverBy::xpath($targetOriginario));
                $linkOriginario = null;
                foreach ($rows as $row) {
                    if (preg_match('/Processo originário/', $row->getText())) {
                        $linkOriginario = $row->findElement(WebDriverBy::tagName('a'))->getAttribute('href');
                    }
                }
                if ($linkOriginario) {
                    $target = '//*[@id="content"]/form/table/tbody/tr';
                    $driver->navigate()->to($linkOriginario);
                    $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));
                    $rows = $driver->findElements(WebDriverBy::xpath($target));

                    $dados = $robo->extractVerticalTable($rows, 2);

                    foreach ($dados as $key => $val) {
                        if (preg_match('/Comarca /', $key)) {
                            $vara = $robo->cleanString($val);
                            break;
                        }

                        if (preg_match('/Regional d/', $key)) {
                            $varaAux = explode(';', $val);
                            $vara = $robo->cleanString($varaAux[0], ['; ', ';']);
                            break;
                        }
                    }

                    if ($vara != '') {
                        $vara = $robo->cleanString($vara);
                        $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    }
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }
        }

        $driver->quit();
        $robo->log($title);
        return true;
    }

    //TRIBUNAL TOCANTINS/TO
    public function tjtofisico($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL TOCANTINS/TO - FÍSICOS";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://sproc.tjto.jus.br/sprocnewconsultas/consultasnet/Juridico/ResConProc02.asp?TXT_PARAM1=[NRO_PROCESSO]&TXT_PARAM2=0&CMB_NUMMOV=99&TXT_SOURCE=ResConProcNum&CHK_PARTE=";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('/html/body/table'))->getText());
                if (preg_match('/Nenhum Registro encontrado/', $msg)) {
                    $robo->log($msg);
                    continue;
                }
            } catch (\Exception $e) {
            }

            try {
                $vara = '';

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table[1]/tbody/tr')));

                $rows = $driver->findElements(WebDriverBy::xpath('/html/body/table[1]/tbody/tr'));
                $robo->log(count($rows));
                foreach ($rows as $row) {
                    $text = $robo->cleanString($row->getText());
                    if (preg_match('/Localização: /', $text)) {
                        $text = $robo->cleanString($text, ['Localização: ']);
                        $textArr = explode(' - ', $text);
                        $vara = $textArr[0];
                        break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                } else {
                    debug('NÃO ENCONTROU?');
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjtoprojudi($processos)
    {
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL TOCANTINS/TO - FÍSICOS";
        $robo->log($title);
        $driver = $robo->startDriver();

//        $linkGet = "https://www.tjto.jus.br/projudi/publico/buscas/ProcessosParte";
//        $linkGet = "https://www.tjto.jus.br/projudi/";
//        $linkGet = "https://www.tjto.jus.br/projudi/interno.jsp?endereco=/projudi/publico/buscas/ProcessosParte";
        $linkGet = "https://www.tjto.jus.br/projudi/listagens/DadosProcesso?numeroProcesso=[NRO_PROCESSO_SEM_MASCARA]";
        $session = "https://www.tjto.jus.br/projudi/interno.jsp?endereco=/projudi/publico/buscas/ProcessosParte";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->get($session);
//            $nroProcesso = ltrim($nroProcesso, '0');
            $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
            $driver->navigate()->to($link);

            $robo->saveCache($title, $nroProcesso);


            try {
                $conteudo = '//*[@id="Partes"]/table';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($conteudo)));

                $textToFind = 'Juízo:';
                $vara = '';
                $rows = $driver->findElements(WebDriverBy::xpath('//*[@id="Partes"]/table/tbody/tr'));
                foreach ($rows as $row) {
                    if (preg_match("/$textToFind/", $row->getText())) {
                        $vara = $robo->cleanString($row->getText(), [$textToFind]);
                        if (preg_match('/Juiz: /', $vara)) {
                            $arr = explode('Juiz: ', $vara);
                            $vara = $arr[0];
                        }
                        break;
                    }
                }

                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function tjtoeproc1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL TOCANTINS/TO - EPROC 1° GRAU";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://consultaeproc.tjto.jus.br/eprocV2_prod_1grau/externo_controlador.php?acao=processo_consulta_publica";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
            $nroProcesso = $robo->cnjNumberFormatter($nroProcesso);
            $nroProcesso = $robo->processNumberFormatter($nroProcesso, '#######-##.####.#.##.####', 20);
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        $driver->navigate()->to($linkGet);

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = $robo->onlyDigits($nroProcesso);
            if (!$nroProcesso) continue;

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            //CAPTCHA
            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="formPrincipal"]/div/div/div/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("recaptchaCallback('" . $captcha . "');", []);
                $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="txtNumProcesso"]')));
            } catch (\Exception $e) {
            }

            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="txtNumProcesso"]')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="txtNumProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="txtNumProcesso"]'))->sendKeys($nroProcesso);
            sleep(1);
            $driver->findElement(WebDriverBy::xpath('//*[@id="sbmNovo"]'))->click();

            try {
                $vara = '';
                $target = '//*[@id="txtOrgaoJulgador"]';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $vara = $robo->cleanString($driver->findElement(WebDriverBy::xpath($target))->getText());

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                    $driver->findElement(WebDriverBy::xpath('//*[@id="btnVoltar"]'))->click();
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjtoeproc2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL TOCANTINS/TO - EPROC 2° GRAU";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://eproc2.tjto.jus.br/eprocV2_prod_2grau/externo_controlador.php?acao=processo_consulta_publica";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

            $nroProcesso = $processo->getNumProcJust();
            $nroProcesso = $robo->cnjNumberFormatter($nroProcesso);
            $nroProcesso = $robo->processNumberFormatter($nroProcesso, '#######-##.####.#.##.####', 20);
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        $driver->navigate()->to($linkGet);

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $nroProcesso = $robo->onlyDigits($nroProcesso);
            if (!$nroProcesso) continue;

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            //CAPTCHA
            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="formPrincipal"]/div/div/div/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("recaptchaCallback('" . $captcha . "');", []);
                $driver->executeScript("document.getElementById('formPrincipal').submit();", []);
                $driver->navigate()->to($linkGet);
                $driver->wait(10)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="txtNumProcesso"]')));
            } catch (\Exception $e) {
            }

            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="txtNumProcesso"]')));
            $driver->findElement(WebDriverBy::xpath('//*[@id="txtNumProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="txtNumProcesso"]'))->sendKeys($nroProcesso);
            sleep(1);
            $driver->findElement(WebDriverBy::xpath('//*[@id="sbmNovo"]'))->click();

            try {
                $vara = '';
                $target = '//*[@id="txtOrgaoColegiado"]';
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($target)));

                $vara = $robo->cleanString($driver->findElement(WebDriverBy::xpath($target))->getText());

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                    $driver->findElement(WebDriverBy::xpath('//*[@id="btnVoltar"]'))->click();
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    //TRIBUNAL ACRE/AC
    public function tjacesaj1grau($processos)
    {
        //PRIMEIRA LEVA => LISTO OS PROCESSOS PELO NOME DA PARTE E DEPOIS TENTO ENCONTRAR OS PROCESSOS QUE PRECISO.
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        //QUARTA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO COM FORMATAÇÃO DEFINIDO PELO CLIENTE
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("ACRE/AC 1° GRAU ESAJ");
        $driver = $robo->startDriver();
        $link = 'https://esaj.tjac.jus.br/cpopg/open.do';
        $driver->get($link);

        $comarcaElem = $driver->findElements(WebDriverBy::xpath('//*[@id="id_Comarca"]/option'));
        $comarca = [];
        foreach ($comarcaElem as $item) {
            $comV = $item->getAttribute('value');
            if ($comV < 0) continue;
            $comarcas[] = str_pad($comV, 4, 0, STR_PAD_LEFT);
        }

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//PRIMEIRA E SEGUNDA LEVA
            $nroProcesso = $processo->getNumProcJust();
            if (!$nroProcesso) continue;
//            if (!preg_match('/.8.01./', $nroProcesso)) continue;//PRIMEIRA E SEGUNDA LEVA
//            if (!in_array(substr($nroProcesso, -4, 4), $comarcas)) continue;//PRIMEIRA E SEGUNDA LEVA

            $processosTratados[$nroProcesso] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $captcha = '';

        /**
         * PRIMEIRA TENTATIVA VARRENDO A LISTA DE PROCESSOS
         * APÓS A PESQUISA PELO NOME DA PARTE
         */
        /*
        $words = ['CAIXA SEGURO', 'CAIXA SEGUROS', 'CAIXA SEGURADORA', 'CAIXA CONSÓRCIO', 'CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA'];
        //$words = ['CAIXA CAPITALIZAÇÃO', 'CAIXA VIDA', 'SULAMERICA', 'CAIXA CONSÓRCIO', 'CAIXA SEGURADORA'];

        $driver->navigate()->to($link);
        foreach ($words as $word) {
            $robo->log($word);
            $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[12]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
            } catch (\Exception $e) {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msgError = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText());
                $robo->log($msgError);

                if (preg_match('/Marque a opção Não sou um robô/', $msgError)) {
                    try {
                        $driver->navigate()->to($link);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                        $driver->navigate()->to($link);
                    }
                    $driver->findElement(WebDriverBy::xpath('//*[@id="cbPesquisa"]'))->sendKeys('Nome da parte');
                    $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="campo_NMPARTE"]'))->sendKeys($word);
                    $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[12]/td[2]/div/div/div/div/iframe');
                    if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();
                }
            }

            //VARRENDO OS PROCESSOS ENCONTRADOS
            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                //PRIMEIRA LEVA
                $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                foreach ($processosBloco as $bloco) {
                    $vara = '';
                    $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                    if ($nro == '') continue;
                    if (!isset($processosTratados[$nro])) continue;

                    $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                    $index++;

                    $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                    $linha = end($linhas);
                    $linhaArr = explode(' - ', $linha->getText());
                    $infoSelected = end($linhaArr);
                    if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                        $countAux = count($linhaArr);
                        $infoSelected = $linhaArr[$countAux - 2];
                    }
                    $vara = $robo->cleanString($infoSelected);

                    if ($vara != '') {
                        $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                    }
                }
                //SEGUNDA LEVA SE EXISTIR
                while ($next = $robo->hasNext($driver, 'cssSelector', '#paginacaoSuperior > tbody > tr:nth-child(1) > td:nth-child(2) > div > a[title="Próxima página"]')) {

                    try {
                        $driver->navigate()->to($next);
                    } catch (\Exception $e) {
                        $driver->navigate()->refresh();
                    }

                    $processosBloco = $driver->findElements(WebDriverBy::cssSelector('div[id^=divProcesso]'));
                    foreach ($processosBloco as $bloco) {
                        $vara = '';
                        $nro = $robo->cleanString($bloco->findElement(WebDriverBy::cssSelector('div.nuProcesso'))->getText());
                        if ($nro == '') continue;
                        if (!isset($processosTratados[$nro])) continue;

                        $robo->log("({$robo->percentage($total,$index)}) {$index} de {$total} => " . $nro);
                        $index++;

                        $linhas = $bloco->findElements(WebDriverBy::cssSelector('div.espacamentoLinhas'));
                        $linha = end($linhas);
                        $linhaArr = explode(' - ', $linha->getText());
                        $infoSelected = end($linhaArr);
                        if (preg_match('/Unidade 100/', $infoSelected) || preg_match('/Digital/', $infoSelected)) {
                            $countAux = count($linhaArr);
                            $infoSelected = $linhaArr[$countAux - 2];
                        }
                        $vara = $robo->cleanString($infoSelected);

                        if ($vara != '') {
                            $robo->addVara($processosTratados[$nro], $vara, false, $driver->getCurrentURL());
                        }
                    }
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
                continue;
            }

        }

        $driver->quit();
        $robo->log("PRIMEIRA TENTATIVA - DONE!");
        die;
        */


        /**
         * SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
         * TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
         */
        $driver->navigate()->to($link);
        foreach ($processosTratados as $processo) {

            //$nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
//            $nroProcesso = $processo->getNumProcJust();
            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
//            if (strlen($nroProcesso) == 20) continue;
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

//            $nroProcessoArr = explode('.8.24.', $nroProcesso);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            if (count($nroProcessoArr) < 2) continue;//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->clear();//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="numeroDigitoAnoUnificado"]'))->sendKeys($nroProcessoArr[0]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ
//            $driver->findElement(WebDriverBy::xpath('//*[@id="foroNumeroUnificado"]'))->sendKeys($nroProcessoArr[1]);//SEGUNDA TENTATIVA PESQUISANDO PROCESSO POR PROCESSO PELO NRO CNJ

            $driver->findElement(WebDriverBy::xpath('//*[@id="radioNumeroAntigo"]'))->click();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->clear();//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            //ELIMINANDO DIGITO VERIFICADOR
            /*if (substr($nroProcesso, -2, 1) == '-') {
                $robo->log($nroProcesso);
                $nroProcesso = substr_replace($nroProcesso, '', -1, 2);
                $nroProcesso = rtrim($nroProcesso, '-');
                $robo->log($nroProcesso);
            }*/

            $driver->findElement(WebDriverBy::xpath('//*[@id="nuProcessoAntigoFormatado"]'))->sendKeys($nroProcesso);//TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO

            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '//*[@id="secaoFormConsulta"]/tbody/tr[11]/td[2]/div/div/div/div/iframe');
            if ($captcha) $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="pbEnviar"]'))->click();

            $robo->saveCache(__FUNCTION__, $nroProcesso);

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]')));
                $msg = $driver->findElement(WebDriverBy::xpath('//*[@id="spwTabelaMensagem"]'))->getText();
                $robo->log($msg);
                if (preg_match('/Não existem informações disponíveis para os parâmetros informados./', $msg)) continue;
                $driver->navigate()->back();
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="popupModalDiv"]')));
                $robo->log("SEGREDO DE JUSTIÇA => {$nroProcesso}");
                $processo->addObs("SEGREDO DE JUSTIÇA => {$link}");
                $processo->salvar(true);
                $driver->findElement(WebDriverBy::xpath('//*[@id="botaoFecharPopupSenha"]'))->click();
                sleep(2);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.nuProcesso > a')));
                $robo->log("LISTA DE PROCESSOS => {$nroProcesso}");
                $processo->addObs("LISTA DE PROCESSOS => {$link}");
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr[1]')));

                $dadosProcessoAux = $driver->findElements(WebDriverBy::xpath('/html/body/div/table[4]/tbody/tr/td/div[1]/table[2]/tbody/tr'));
                $dadosProcesso = $robo->extractVerticalTable($dadosProcessoAux, 2);

                if (isset($dadosProcesso['Distribuição']) && $dadosProcesso['Distribuição'] != '') {
                    $distribuicao = explode('; ', $dadosProcesso['Distribuição']);
                    $varaAux = (isset($distribuicao[1])) ? $distribuicao[1] : $dadosProcesso['Distribuição'];
                    $vara = rtrim(str_replace([';', ' - Unidade 100% Digital'], '', $varaAux));
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                }

            } catch (\Exception $e) {
                continue;
            }
        }

        $driver->quit();
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    //TRIBUNAL RORAIMA/RR
    public function tjrr1e2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL RORAIMA/RR 1° e 2° GRAU FÍSICO";

        $robo->log($title);
        $driver = $robo->startDriver();

        $comarcasOptions = '<option value="5">Alto Alegre</option>
          <option value="10">Boa Vista</option>
          <option value="90">Bonfim</option>
          <option value="20">Caracarai</option>
          <option value="30">Mucajai</option>
          <option value="45">Pacaraima</option>
          <option value="47">Rorainopolis</option>
          <option value="60">São Luiz</option>';

        $linkGet = "http://www.tjrr.jus.br/tjrr-siscom-webapp/pages/proc_resultado.jsp?lst_processos=&comrCodigo=[COMARCA]&numero=1&listaProcessos=[NRO_PROCESSO]&btn_pesquisar=Pesquisar";
//        $linkGet = "http://www.tjrr.jus.br/tjrr-siscom-webapp/pages/proc_resultado.jsp?comrCodigo=0&numero=2&listaProcessos=[NRO_PROCESSO]&btn_pesquisar=Pesquisar";//2° GRAU
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//            if (strlen($nroProcesso) < 10) continue;
//            if (!$robo->yearValidate($nroProcesso,0)) continue;
//            if (!$nroProcesso) continue;
            if (!$processo->getNumProcJust()) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
//            $nroProcesso = $processo->getNumProcJust();

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $doc = new \DOMDocument();
            $doc->loadHTML($comarcasOptions);
            $comarcas = [];
            foreach ($doc->getElementsByTagName('option') as $comarca) {
                $codeComarca = $comarca->getAttribute('value');
                similar_text(strtoupper($comarca->nodeValue), $processo->getDesComarca(), $percent);
                $comarcas[$percent] = $codeComarca;
            }
            ksort($comarcas);
            $comarcaCod = end($comarcas);

            $driver->get(str_replace(['[COMARCA]', '[NRO_PROCESSO]'], [$comarcaCod, $nroProcesso], $linkGet));

            try {
                $vara = '';

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function projuditjrr($processos, $params = [])
    {
        if (!count($params)) return false;
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $robo->log("TRIBUNAL RORAIMA/RR - PROJUDI 1° E 2° INSTÂNCIA");
        $driver = $robo->startDriver();

        $instancia = $params['instancia'];
        $func = __FUNCTION__ . $instancia . 'instancia';

        $processosTratados = [];
        foreach ($processos as $processo) {
//            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if ($nroProcesso) {
                if (!preg_match('/.8.23/', $nroProcesso)) continue;
                if (substr($nroProcesso, 0, 1) != 0) continue;
            } else {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
                if (!$nroProcesso) continue;
                $year = substr($nroProcesso, 4, 4);
                if ($year < 1970 || $year > date('Y')) continue;
            }

            if ($robo->cacheExists($func, $nroProcesso)) continue;
            $processosTratados[] = $processo;

        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 0;
        $encontrados = 0;
        $linkGet = 'https://projudi.tjrr.jus.br/projudi/processo/consultaPublica.do?actionType=iniciar';
        foreach ($processosTratados as $processo) {
            try {

                $cnj = true;
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if (!$nroProcesso) {
                    $cnj = false;
                    $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '###.####.###.###-#', 14);
                }

                $robo->log("{$robo->percentage($total, $index)} => {$nroProcesso}");
                $index++;

                if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                    $robo->log("CACHE SKIPPING => $nroProcesso");
                    continue;
                }
                $robo->saveCache(__FUNCTION__, $nroProcesso);

                while (true) {
                    $driver->get($linkGet);

                    //1° INSTÂNCIA
                    if ($instancia == '1') {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/fieldset/table/tbody/tr[3]/td[2]/table/tbody/tr/td[1]/input'))->click();
                    } else {
                        //2° INSTÂNCIA
                        $driver->findElement(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/fieldset/table/tbody/tr[3]/td[2]/table/tbody/tr/td[2]/input'))->click();
                    }

                    if ($cnj) {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="divFiltroNumeroProcesso"]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[1]/input'))->click();
                    } else {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="divFiltroNumeroProcesso"]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/input'))->click();
                    }

                    $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="numeroProcesso"]'))->sendKeys($nroProcesso);
                    $driver->findElement(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/table[1]/tbody/tr/td[2]/button[1]'))->click();

                    try {
                        $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha', 'xpath', '/html/body/div[2]/div[2]/iframe');
                        if (!$captcha) {
                            $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                        }
                        $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                        $driver->executeScript("onSubmit();", []);

                        $notFound = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]/div[3]/div/ul/li'))->getText());
                        $robo->log($notFound);
                        if (!preg_match('/Verificação do Captcha inválida/', $notFound)) break;
                    } catch (\Exception $e) {
                        break;
                    }
                    $robo->log("TRYING AGAIN");
                }

                $robo->saveCache($func, $processo->getNumProcJust());

                try {
                    $notFound = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="errorMessages"]/div[3]/div/ul/li'))->getText());
                    continue;
                } catch (\Exception $e) {
                }

                try {
                    //SE APARECER MAIS DE UM PROCESSO
                    $linksProcesso = $driver->findElements(WebDriverBy::xpath('//*[@id="buscaProcessoForm"]/table[2]/tbody/tr/td[1]/a'));
                    if (is_array($linksProcesso) && count($linksProcesso) > 0) {
                        $lastElem = end($linksProcesso);
                        $lastElem->click();
                    }
                } catch (\Exception $e) {
                }

                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="tabItemprefix0"]')));
                $driver->findElement(WebDriverBy::xpath('//*[@id="tabItemprefix0"]'))->click();

                $vara = '';
                $delimiter = 'Órgão Julgador:';
                if ($instancia == '1') {
                    $delimiter = 'Juízo:';
                }
                $array = explode($delimiter, $driver->findElement(WebDriverBy::xpath('//*[@id="tabprefix0"]'))->getText());
                if (count($array) < 1) continue;
                $vara = substr($array[1], 0, strpos($array[1], "\n"));
                $vara = $robo->cleanString($vara);
                if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                $encontrados++;

            } catch (\Exception $e) {
                $robo->log($e->getMessage());
            }
        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function tjrrpje1grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL RORAIMA/RR PJE 1° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://pje.tjrr.jus.br/pje/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.23./', $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);
            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $idDinamico = $driver->findElement(WebDriverBy::cssSelector('img[id$="captchaImg"]'))->getAttribute('id');
                $idDinamico = str_replace('captchaImg', '', $idDinamico);

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="' . $idDinamico . 'captchaImg"]',
                    '//*[@id="' . $idDinamico . 'verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function tjrrpje2grau($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRIBUNAL RORAIMA/RR PJE 2° GRAU';

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://pje2.tjrr.jus.br/pje/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/8.23./', $nroProcesso)) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"fPP:numProcessoDecoration:numProcesso\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $idDinamico = $driver->findElement(WebDriverBy::cssSelector('img[id$="captchaImg"]'))->getAttribute('id');
                $idDinamico = str_replace('captchaImg', '', $idDinamico);

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="' . $idDinamico . 'captchaImg"]',
                    '//*[@id="' . $idDinamico . 'verifyCaptcha"]',
                    '//*[@id="fPP:searchProcessos"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    /**
     * TRIBUNAIS SUPERIORES
     */
    //AC AM AP BA DF GO MA MG MT PA PI RO RR TO
    public function trf1($processos, $params = ['uf' => 'TRF1', 'codeTribunal' => '', 'tipoNro' => 'cnj'])
    {
        /**
         * $params = ['uf'=>string,'codeTribunal'=>string,'tipoNro'=>string]
         * @tipoNro
         * cnj => NNNNNNN-DD.AAAA.J.TR.OOOO (Res. CNJ n.65 de 16/12/2008)
         * A partir de 2010 e opcional para os anos anteriores (Numeração Única)
         *
         * cjf => AAAA.RE.OR.NNNNN-D (Res. CJF n.177 de 26/09/1996)
         * Entre 1997 e 2009
         *
         * antigo => AA.TR.NNNNN-D
         * Anterior a 1997
         *
         * sem_mascara => 111111111111 N quantidade de nros
         */
        if (!count($params)) return false;

        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //SEGUNDA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO SEM FORMATAÇÃO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $func = __FUNCTION__ . implode('', $params);
        $robo = new Robo();
        $driver = $robo->startDriver();
        $uf = $params['uf'];
        $codeT = $params['codeTribunal'];
        $tipoNro = $params['tipoNro'];
        $title = "{$uf} {$codeT} - JUSTIÇA FEDERAL TRF1";
        $robo->log($title);
        $linkGet = "https://processual.trf1.jus.br/consultaProcessual/processo.php?proc=[NRO_PROCESSO_SEM_MASCARA]&secao={$uf}&mostrarBaixados=S&seq_proc=1";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            if (!$processo->getNumProcJust()) continue;
            $nroProcesso = '';
            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if ($nroProcesso == false) continue;
                $year = substr($nroProcesso, 11, 4);
                if ($year < 1970 || $year > date('Y')) continue;
            }
            if ($tipoNro == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.#####-#', 15);
                if ($nroProcesso == false) continue;
                $year = substr($nroProcesso, 0, 4);
                if ($year < 1970 || $year > date('Y')) continue;
            }
            if ($tipoNro == 'antigo') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '##.##.#####-#', 10);
                if ($nroProcesso == false) continue;
                $year = substr($nroProcesso, 0, 2);
                if ($year < 70 || $year > 96) continue;
            }
            if ($tipoNro == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
                if ($nroProcesso == false) continue;
            }

            if ($codeT != '') {
                if (!preg_match("/{$codeT}/", $nroProcesso)) continue;
            }
            $processosTratados[] = $processo;

            $robo->log($nroProcesso);
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = '';
            if ($tipoNro == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            }
            if ($tipoNro == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.#####-#', 15);
            }
            if ($tipoNro == 'antigo') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '##.##.#####-#', 10);
            }
            if ($tipoNro == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            if ($robo->cacheExists($func, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
            $driver->navigate()->to($link);

            $robo->saveCache($func, $nroProcesso);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.error')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('div.error'))->getText());
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
                try {
                    $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="aba-processo"]/table/tbody/tr')));
                    $tableRows = $driver->findElements(WebDriverBy::xpath('//*[@id="aba-processo"]/table/tbody/tr'));

                    $vara = '';
                    foreach ($tableRows as $row) {
                        $key = $row->findElement(WebDriverBy::cssSelector("th"))->getText();
                        if (preg_match('/Vara/', $key)) {
                            $vara = $robo->cleanString($row->findElement(WebDriverBy::cssSelector("td"))->getText());
                            if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                            $encontrados++;
                            break;
                        }
                        if (preg_match('/Órgão Julgador/', $key)) {
                            $vara = $robo->cleanString($row->findElement(WebDriverBy::cssSelector("td"))->getText());
                            if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                            $encontrados++;
                            break;
                        }
                    }
                } catch (\Exception $e) {
                    $robo->log('PROCESSO NÃO ENCONTRADO');
                }
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function trf1secao($processos, $params = ['uf' => ''])
    {
        if (!count($params)) return false;
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $driver = $robo->startDriver();
        $uf = $params['uf'];

        $title = "{$uf} - JUSTIÇA FEDERAL TRF1";
        $robo->log($title);

        $secoes = '
<optgroup label="AC">
<option value="CZU" class="subsecao">Cruzeiro do Sul</option>
</optgroup>
<optgroup label="AM">
<option value="TBT" class="subsecao">Tabatinga</option>
<option value="TFE" class="subsecao">Tefé</option>
</optgroup>
<optgroup label="AP">
<option value="LJI">Laranjal do Jari</option>
<option value="OPQ">Oiapoque</option>
</optgroup>
<optgroup label="BA">
<option value="ALH" class="subsecao">Alagoinhas</option>
<option value="BES" class="subsecao">Barreiras</option>
<option value="BMP" class="subsecao">Bom Jesus da Lapa</option>
<option value="CFS" class="subsecao">Campo&nbsp;Formoso</option>
<option value="EUS" class="subsecao">Eunápolis</option>
<option value="FSA" class="subsecao">Feira de Santana</option>
<option value="GNB" class="subsecao">Guanambi</option>
<option value="ILS" class="subsecao">Ilhéus</option>
<option value="IEE" class="subsecao">Irecê</option>
<option value="ITB" class="subsecao">Itabuna</option>
<option value="JEE" class="subsecao">Jequié</option>
<option value="JZR" class="subsecao">Juazeiro</option>
<option value="PAF" class="subsecao">Paulo Afonso</option>
<option value="VCA" class="subsecao">Vitória da Conquista</option>
<option value="TAF" class="subsecao">Teixeira de Freitas</option>
</optgroup>
<optgroup label="GO">
<option value="ANS" class="subsecao">Anápolis</option>
<option value="ACG" class="subsecao">Aparecida de Goiânia</option>
<option value="LZA" class="subsecao">Luziânia</option>
<option value="RVD" class="subsecao">Rio Verde</option>
<option value="URC" class="subsecao">Uruaçu</option>
<option value="FRM" class="subsecao">Formosa</option>
<option value="JTI" class="subsecao">Jataí</option>
<option value="IUB" class="subsecao">Itumbiara</option>
</optgroup>
<optgroup label="MA">
<option value="BBL" class="subsecao">Bacabal</option>
<option value="BLA" class="subsecao">Balsas</option>
<option value="CXS" class="subsecao">Caxias</option>
<option value="ITZ" class="subsecao">Imperatriz</option>
</optgroup>
<optgroup label="MG">
<option value="CEM" class="subsecao">Contagem</option>
<option value="DVL" class="subsecao">Divinópolis</option>
<option value="GVS" class="subsecao">Governador&nbsp;Valadares</option>
<option value="IIG" class="subsecao">Ipatinga</option>
<option value="IUA" class="subsecao">Ituiutaba</option>
<option value="JUA" class="subsecao">Janaúba</option>
<option value="JFO" class="subsecao">Juiz&nbsp;de&nbsp;Fora</option>
<option value="LAV" class="subsecao">Lavras</option>
<option value="MNC" class="subsecao">Manhuaçu</option>
<option value="MCL" class="subsecao">Montes&nbsp;Claros</option>
<option value="MRE" class="subsecao">Muriaé</option>
<option value="PTU" class="subsecao">Paracatu</option>
<option value="PSS" class="subsecao">Passos</option>
<option value="PMS" class="subsecao">Patos&nbsp;de&nbsp;Minas</option>
<option value="PCS" class="subsecao">Poços&nbsp;de&nbsp;Caldas</option>
<option value="PNV" class="subsecao">Ponte Nova</option>
<option value="PSA" class="subsecao">Pouso&nbsp;Alegre</option>
<option value="PMS" class="subsecao">Patos&nbsp;de&nbsp;Minas</option>
<option value="SOE" class="subsecao">São João Del Rei</option>
<option value="SSP" class="subsecao">São&nbsp;Sebastião&nbsp;do&nbsp;Paraíso</option>
<option value="SLA" class="subsecao">Sete&nbsp;Lagoas</option>
<option value="TOT" class="subsecao">Teófilo Otoni</option>
<option value="UBE" class="subsecao">Uberaba</option>
<option value="UDI" class="subsecao">Uberlândia</option>
<option value="UNI" class="subsecao">Unaí</option>
<option value="VGA" class="subsecao">Varginha</option>
<option value="VCS" class="subsecao">Viçosa</option>
</optgroup>
<optgroup label="MT">
<option value="BAG" class="subsecao">Barra do Garças</option>
<option value="CCS" class="subsecao">Cáceres</option>
<option value="DIO" class="subsecao">Diamantino</option>
<option value="JNA" class="subsecao">Juína</option>
<option value="ROI" class="subsecao">Rondonópolis</option>
<option value="SNO" class="subsecao">Sinop</option>
</optgroup>
<optgroup label="PA">
<option value="ATM" class="subsecao">Altamira</option>
<option value="CAH" class="subsecao">Castanhal</option>
<option value="IAB" class="subsecao">Itaituba</option>
<option value="MBA" class="subsecao">Marabá</option>
<option value="PGN" class="subsecao">Paragominas</option>
<option value="RDO" class="subsecao">Redenção</option>
<option value="STM" class="subsecao">Santarém</option>
<option value="TUU" class="subsecao">Tucuruí</option>
</optgroup>
<optgroup label="PI">
<option value="CNT" class="subsecao">Corrente</option>
<option value="FLO" class="subsecao">Floriano</option>
<option value="PNA" class="subsecao">Parnaíba</option>
<option value="PCZ" class="subsecao">Picos</option>
<option value="SRN" class="subsecao">São Raimundo Nonato</option>
</optgroup>
<optgroup label="RO">
<option value="GUM" class="subsecao">Guajará-Mirim</option>
<option value="JIP" class="subsecao">Ji-paraná</option>
<option value="VHA" class="subsecao">Vilhena</option>
</optgroup>
<optgroup label="TO">
<option value="ARN" class="subsecao">Araguaína</option>
<option value="GUR" class="subsecao">Gurupi</option>
</optgroup>';

        $doc = new \DOMDocument();
        $doc->loadHTML($secoes);
        $secoes = [];
        foreach ($doc->getElementsByTagName('optgroup') as $ufSecao) {
            if ($ufSecao->getAttribute('label') != $uf) continue;
            foreach ($ufSecao->getElementsByTagName('option') as $secao) {
                $secoes[] = $secao->getAttribute('value');
            }
        }

//        print_r($secoes); return true;

        $func = __FUNCTION__ . implode('', $params);


        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroProcesso) continue;
            $processosTratados[$processo->getNumProcJust()] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $encontrados = 0;

        foreach ($secoes as $secao) {

            $index = 1;

            $func = __FUNCTION__ . implode('', $params) . $secao;


            $robo->log("{$uf} => {$secao} - JUSTIÇA FEDERAL TRF1");

            //$linkGet = "https://processual.trf1.jus.br/consultaProcessual/numeroProcesso.php?proc=[NRO_PROCESSO_SEM_MASCARA]&secao={$secao}&pg=1&mostrarBaixados=S";
            //$linkGet = "https://processual.trf1.jus.br/consultaProcessual/processo.php?proc=[NRO_PROCESSO_SEM_MASCARA]&secao={$secao}&mostrarBaixados=S";
            $linkGet = "https://processual.trf1.jus.br/consultaProcessual/processo.php?proc=[NRO_PROCESSO_SEM_MASCARA]&secao={$secao}&pg=1&enviar=Pesquisar&mostrarBaixados=S";
            //&seq_proc=1
            $driver->get($robo->host);

            foreach ($processosTratados as $processo) {

                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
                if (!$nroProcesso) continue;

//                if ($robo->cacheExists($func, $nroProcesso)) {
//                    $robo->log("CACHE SKIPPING => $nroProcesso");
//                    continue;
//                }

                $robo->log("{$robo->percentage($total,$index)} => {$uf}/{$secao} => {$nroProcesso}");
                $index++;

                $link = str_replace('[NRO_PROCESSO_SEM_MASCARA]', $nroProcesso, $linkGet);
                $driver->navigate()->to($link);

//                $robo->saveCache($func, $nroProcesso);

                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('div.error')));
                    $msg = $robo->cleanString($driver->findElement(WebDriverBy::cssSelector('div.error'))->getText());
                    $robo->log($msg);
                    continue;
                } catch (\Exception $e) {
                    try {
                        $driver->wait(60 * 3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="aba-processo"]/table/tbody/tr')));

                        $tableRows = $driver->findElements(WebDriverBy::xpath('//*[@id="aba-processo"]/table/tbody/tr'));

                        $vara = '';
                        foreach ($tableRows as $row) {
                            $key = $row->findElement(WebDriverBy::cssSelector("th"))->getText();
                            if (preg_match('/Vara/', $key)) {
                                $vara = $robo->cleanString($row->findElement(WebDriverBy::cssSelector("td"))->getText());
                                if ($vara != '') {
                                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                                    $encontrados++;
                                    unset($processosTratados[$processo->getNumProcJust()]);
                                    break;
                                }
                            }
                            if (preg_match('/Órgão Julgador/', $key)) {
                                $vara = $robo->cleanString($row->findElement(WebDriverBy::cssSelector("td"))->getText());
                                if ($vara != '') {
                                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                                    $encontrados++;
                                    unset($processosTratados[$processo->getNumProcJust()]);
                                    break;
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        $robo->log('PROCESSO NÃO ENCONTRADO');
                    }
                }
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function trf1pje1g($processos, $params = ['uf' => 'trf1'])
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRF1 PJE 1 GRAU - ' . $params['uf'] . ' ' . __FUNCTION__;

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje1g.trf1.jus.br/consultapublica/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/4.01/', $nroProcesso)) continue;
//            if (!$robo->yearValidate($nroProcesso, 11)) continue;
            if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        $driver->get($linkGet);

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $robo->log($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE 440 => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function trf1pje2g($processos, $params = ['uf' => 'trf1'])
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = 'TRF1 PJE 2 GRAU - ' . $params['uf'] . ' ' . __FUNCTION__;

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje2g.trf1.jus.br/consultapublica/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/4.01/', $nroProcesso)) continue;
            if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        $driver->get($linkGet);

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->clear();
            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:numProcesso-inputNumeroProcessoDecoration:numProcesso-inputNumeroProcesso"]'))->sendKeys($nroProcesso);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:searchProcessos"]'))->click();

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/div[8]/div[2]/iframe')));
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
            } catch (\Exception $e) {
            }

            $driver->executeScript("executarPesquisa();", []);

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')));
                $robo->log($driver->findElement(WebDriverBy::className('rich-messages'))->getText());
                $captcha = $robo->hasReCaptcha($driver, 'g-recaptcha');
                $robo->log("LINE 440 => $captcha");
                $driver->executeScript("document.getElementById(\"g-recaptcha-response\").innerHTML=\"{$captcha}\";", []);
                $driver->executeScript("executarPesquisa();", []);
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr')));

                $link = $driver->findElement(WebDriverBy::xpath('//*[@id="fPP:processosTable:tb"]/tr[1]/td[1]/a'))->getAttribute('onclick');
                $link = str_replace(["openPopUp('Consulta pública','", "')"], "", $link);
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function trf2($processos)
    {
        $robo = new Robo();
        $driver = $robo->startDriver();
        $encontrados = 0;
        $index = 1;
        $total = count($processos);
        foreach ($processos as $processo) {

            $robo->log($robo->percentage($total, $index) . " => " . $processo->getNumProcJust());
            $index++;

            if (is_null($processo->getNumProcJust())) {
                continue;
            }

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                continue;
            }

            $code = $robo->onlyDigits($processo->getNumProcJust());

            $url = "http://portal.trf2.jus.br/portal/consulta/cons_procs.asp";
            $driver->get($url);
            $captchaSolved = false;
            $vara = '';
            try {
                $driver->executeScript("document.getElementsByName('baixado')[0].checked = false;", []);
                $driver->findElement(WebDriverBy::xpath('//*[@id="NumProc"]'))->sendKeys($code);
                while (!$captchaSolved) {
                    $driver->executeScript("document.getElementsByName('baixado')[0].checked = false;", []);
                    $solution = $this->solveTrf2Captcha($driver);
                    if (($solution == '') || ($solution == 0)) {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td/font/span/input[2]'))->click();
                    } else {
                        $driver->findElement(WebDriverBy::xpath('//*[@id="captchacode"]'))->sendKeys($solution);
                    }

                    $driver->findElement(WebDriverBy::xpath('//*[@id="Pesquisar"]'))->click();
                    $text = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[2]/tbody/tr/td/font'))->getText();

                    if (strpos($text, 'Atenção: Processo não Localizado na base atual.') !== false) {
                        $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());
                    }

                    if ($text === 'Atenção: O valor digitado não corresponde a sequência de controle.') {
                        $captchaSolved = true;
                    }

                }
            } catch (NoSuchElementException $e) {
                // encontrei!
                $my_frame = $driver->findElement(WebDriverBy::xpath('//*[@id="container2"]/table[3]/tbody/tr[2]/td/table/tbody/tr/td[2]/p/font/span/iframe'));
                $driver->switchTo()->frame($my_frame);
                $text = $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="Resumo"]')))->getText();
                $text = $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ResInfoProc"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[4]/td')))->getText();
                if (preg_match('/Localização /', $text)) {
                    $vara = $robo->cleanString($text, ['Localização ']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    continue;
                }
                $text = $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="ResInfoProc"]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td')))->getText();
                if (preg_match('/Localização /', $text)) {
                    $vara = $robo->cleanString($text, ['Localização ']);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    continue;
                }




                $textareaArr = explode("\n", $text);

                $vara = '';
                foreach ($textareaArr as $textItem) {
                    if ((preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
                            preg_match('/([0-9]{1}) a. VARA FEDERAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª TURMA RECURSAL/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem)) &&
                        !preg_match('/Localização Atual:/', $textItem)) {
                        $vara = $robo->cleanString($textItem);
                        break;
                    }
                }
                if (!$vara) {
                    foreach ($textareaArr as $textItem) {
                        if (preg_match('/([0-9]{1}) a. Vara Federal/i', $textItem) ||
//                        preg_match('/([0-9]{1})ª VF Serra/i', $textItem) ||
                            preg_match('/([0-9]{1})ª VF /i', $textItem) ||
                            preg_match('/([0-9]{1})ª  Vara Federal Cível/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Vara Federal Cível/i', $textItem) ||
                            preg_match('/([0-9]{1})ª  Vara Federal/i', $textItem) ||
                            preg_match('/([0-9]{1})º Juizado Especial/i', $textItem) ||
                            preg_match('/Juizado Especial Federal/i', $textItem) ||
                            preg_match('/([0-9]{1})ª Turma Recursal/i', $textItem) ||
                            preg_match('/ÓRGÃO RESP/i', $textItem)) {
                            $vara = $robo->cleanString($textItem, [' - Juiz Titular', '- Juiz Substituto', 'ÓRGÃO RESP : ']);
                            break;
                        }
                    }
                }


                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                } else {
                    $robo->log("VARA NÃO LOCALIZADA PARA => {$processo->getNumProcJust()}");
                    die;
                }

            } catch (\Exception $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
                die;
            }
        }
        $driver->quit();
        $robo->log(__FUNCTION__);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function solveTrf2Captcha($driver)
    {
        $matches = [];
        $options = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td/font/span/b[2]'))->getText();
        $question = $driver->findElement(WebDriverBy::xpath('//*[@id="ConsProc"]/table/tbody/tr[3]/td/table[1]/tbody/tr[12]/td'))->getText();

        if (strpos($question, 'Quantos símbolos são números') !== false) {
            preg_match_all('/[0-9]/', $options, $matches);
            return count($matches[0]);
        }

        if (strpos($question, 'Quantos símbolos são consoantes') !== false) {
            preg_match_all('/[bcdfghjklmnpqrstvxzwy]/i', $options, $matches);
            return count($matches[0]);
        }

        if (strpos($question, 'Quantos símbolos são vogais') !== false) {
            preg_match_all('/[aeiou]/i', $options, $matches);
            return count($matches[0]);
        }

        if (strpos($question, 'Quais símbolos (informe os repetidos também, se houver) são números') !== false) {
            $answer = '';
            preg_match_all('!\d+!', $options, $matches);
            foreach ($matches[0] as $value) {
                $answer .= $value;
            }
            return $answer;
        }

        if (strpos($question, 'Quais símbolos (informe os repetidos também, se houver) são consoantes') !== false) {
            $answer = '';
            preg_match_all('/[bcdfghjklmnpqrstvxzwy]/i', $options, $matches);
            foreach ($matches[0] as $value) {
                $answer .= $value;
            }
            return $answer;
        }

        if (strpos($question, 'Quais símbolos (informe os repetidos também, se houver) são vogais') !== false) {
            $answer = '';
            preg_match_all('/[aeiou]/i', $options, $matches);
            foreach ($matches[0] as $value) {
                $answer .= $value;
            }
            return $answer;
        }

        throw new \Exception('Não consigo resolver esse captcha!');
    }

    public function jeftrf3($processos)
    {
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $service \Tribunal\Service\Tribunal */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();


        $robo->log("JUIZADO ESPECIAL FEDERAL CÍVEL TRF3");

        //PEGANDO AS COMARCAS
        $comarcas = [];
        $doc = new \DOMDocument();
        $doc->loadHTML("<option value=\"6316\">JEF Andradina</option><option value=\"6310\">JEF Americana</option><option value=\"6331\">JEF Araçatuba</option><option value=\"6322\">JEF Araraquara</option><option value=\"6334\">JEF Assis</option><option value=\"6308\">JEF Avaré</option><option value=\"6335\">JEF Barretos</option><option value=\"6342\">JEF Barueri</option><option value=\"6325\">JEF Bauru</option><option value=\"6329\">JEF Bragança Paulista</option><option value=\"6307\">JEF Botucatu</option><option value=\"6303\">JEF Campinas</option><option value=\"6201\">JEF Campo Grande</option><option value=\"6313\">JEF Caraguatatuba</option><option value=\"6314\">JEF Catanduva</option><option value=\"6207\">JEF Corumbá</option><option value=\"6206\">JEF Coxim</option><option value=\"6202\">JEF Dourados</option><option value=\"6318\">JEF Franca</option><option value=\"6340\">JEF Guaratinguetá</option><option value=\"6332\">JEF Guarulhos</option><option value=\"6341\">JEF Itapeva</option><option value=\"6337\">JEF Jales</option><option value=\"6336\">JEF Jaú</option><option value=\"6304\">JEF Jundiaí</option><option value=\"6333\">JEF Limeira</option><option value=\"6319\">JEF Lins</option><option value=\"6345\">JEF Marília</option><option value=\"6343\">JEF Mauá</option><option value=\"6309\">JEF Mogi das Cruzes</option><option value=\"6204\">JEF Naviraí</option><option value=\"6306\">JEF Osasco</option><option value=\"6323\">JEF Ourinhos</option><option value=\"6326\">JEF Piracicaba</option><option value=\"6309\">JEF Ponta Porã</option><option value=\"6328\">JEF Presidente Prudente</option><option value=\"6305\">JEF Registro</option><option value=\"6302\">JEF Ribeirão Preto</option><option value=\"6317\">JEF Santo André</option><option value=\"6311\">JEF Santos</option><option value=\"6338\">JEF São Bernardo do Campo</option><option value=\"6312\">JEF São Carlos</option><option value=\"6344\">JEF São João da Boa Vista</option><option value=\"6327\">JEF São José dos Campos</option><option value=\"6324\">JEF São José do Rio Preto</option><option value=\"6301\">JEF São Paulo</option><option value=\"6315\">JEF Sorocaba</option><option value=\"6321\">JEF São Vicente</option><option value=\"6330\">JEF Taubaté</option><option value=\"6303\">JEF Três Lagoas</option><option value=\"6339\">JEF Tupã</option><option value=\"9301\">Turma Recursal de São Paulo</option><option value=\"9201\">Turma Recursal de Campo Grande</option>");
        foreach ($doc->getElementsByTagName('option') as $comarca) {
            $comarcas[] = str_pad($comarca->getAttribute('value'), 4, 0, STR_PAD_LEFT);
        }

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/4.03/', $nroCnj)) continue;
            //if (substr($nroCnj, 0, 1) != '0') continue; //PRIMEIRA VARREDURA ENCONTROU 823
            //if (substr($nroCnj, 0, 1) != '5') continue; //SEGUNDA, ENCONTROU NADA "0" NO FILTRO
            //TERCEIRA VARREDURA SEM FILTRO DO PRIMEIRO NRO => 3 ENCONTRADOS
//            if (!in_array(substr($nroCnj, -4, 4), $comarcas)) continue; //SEGUNDA VARREDURA

            //OUTRO TENTATIVA
            $nroCnj = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (!$nroCnj) continue;

            $processosTratados[] = $processo;
        }

        $robo->log(count($processosTratados) . " PROCESSOS ON TARGET!");

        $link = 'http://jef.trf3.jus.br/consulta/consultapro.php?tela=1';

        $driver = $robo->startDriver();
        $driver->get($robo->host);
        $alterados = 0;
        foreach ($processosTratados as $processo) {
            //$driver->navigate()->to($link);
//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());

            //CLEAR NUMBER BEFORE PLACE IN THE FIELD
            $nroCnj = preg_replace('/\D/', '', $processo->getNumProcJust());
//            $nroCnj = preg_replace('/\D/', '', $nroCnj);

            $robo->log($nroCnj);

            if ($robo->cacheExists(__FUNCTION__, $nroCnj)) {
                $robo->log("CACHE SKIPPING => $nroCnj");
                continue;
            }

            $driver->get($link);

            $robo->saveCache(__FUNCTION__, $nroCnj);

            $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/center[1]/table/tbody/tr/td[1]/form/table/tbody/tr[1]/td[2]/input'))->sendKeys($nroCnj);
            $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/center[1]/input[1]'))->click();
            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
//                $processo->addObs($alertText);
//                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            $driver->findElement(WebDriverBy::xpath('//*[@id="conteudo"]/center[1]/table/tbody/tr/td[1]/form/table/tbody/tr[1]/td[2]/input'))->sendKeys($nroCnj);
            sleep(1);
            $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="conteudo"]/center[1]/table/tbody/tr/td[2]/img', '//*[@id="conteudo"]/center[1]/table/tbody/tr/td[1]/form/table/tbody/tr[2]/td[2]/input', '//*[@id="conteudo"]/center[1]/input[1]', false);

            try {
                //ALERT
                $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                $alertText = $driver->switchTo()->alert()->getText();
                $robo->log($alertText);
                $driver->switchTo()->alert()->accept();
                $processo->addObs($alertText);
                $processo->salvar(true);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $vara = '';
                $localizacao = '';

                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr')));
                $table = $driver->findElements(WebDriverBy::xpath('//*[@id="conteudo"]/table/tbody/tr'));
                $dados = $robo->extractVerticalTable($table, 2);

                if (isset($dados['Vara'])) {
                    $vara = $robo->cleanString($dados['Vara'], ['; ', ';']);
                }

                if (isset($dados['Localização'])) {
                    $localizacao = $robo->cleanString($dados['Localização'], ['; ', ';']);
                }

                if ($vara != '') {
                    if ($localizacao != '') $vara = $vara . ' - ' . $localizacao;
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $alterados++;
                } else {
                    if ($localizacao != '') {
                        $vara = $localizacao;
                        $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                        $alterados++;
                    }
                }
            } catch (\Exception $e) {
                $robo->log("NÃO ENCONTRADO OU CAPTCHA ERRADO");
            }

        }


        $robo->log("{$alterados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        $driver->quit();
        return true;
    }

    public function jftrf3($processos)
    {
        //TEM CAPTCHA ALFANUMERICO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */

        $robo = new Robo();


        $robo->log("JUSTIÇA FEDERAL TRF3");

        $processosTratados = [];
        foreach ($processos as $processo) {

            //PRIMEIRA TENTATIVA
//            $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
//            if (!preg_match('/4.03.6/', $nroCnj)) continue;
//            if (substr($nroCnj, 0, 1) != 0) continue;

            //SEGUNDA TENTATIVA
//            $nro = $robo->mask(preg_replace('/\D/', '', $processo->getNumProcJust()), '####.##.##.######-#');
//            if (strlen($nro) != 19) continue;
//            if (substr($nro, -1, 1) != 5) continue;
            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            if (strlen($nroProcesso) < 1) continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log($total . " PROCESSOS ON TARGET!");

        $driver = $robo->startDriver();
        $driver->get('http://web.trf3.jus.br/consultas/Internet/ConsultaProcessual');

        try {

            $alterados = 0;
            $index = 1;
            foreach ($processosTratados as $processo) {
//                $nroCnj = $robo->cnjNumberFormatter($processo->getNumProcJust());
                $nroCnj = preg_replace('/\D/', '', $processo->getNumProcJust());//PRIMEIRA TENTATIVA
                $robo->log($robo->percentage($total, $index) . " => " . $nroCnj);
                $index++;

                //CLEAR NUMBER BEFORE PLACE IN THE FIELD
//                $nroCnj = preg_replace('/\D/', '', $nroCnj); //PRIMEIRA TENTATIVA

                while (true) {

                    $driver->navigate()->to('http://web.trf3.jus.br/consultas/Internet/ConsultaProcessual');
//                    $driver->executeScript('$("#cabecalho").remove();$("#rodape").remove();$("#form > fieldset:nth-child(2)").remove();', []); //PRIMEIRA TENTATIVA
                    $driver->executeScript('$("#cabecalho").remove();$("#rodape").remove();$("#form > fieldset:nth-child(1)").remove();', []);


//                    $driver->findElement(WebDriverBy::xpath('//*[@id="NumeroProcesso"]'))->clear(); //PRIMEIRA TENTATIVA
//                    $driver->findElement(WebDriverBy::xpath('//*[@id="NumeroProcesso"]'))->sendKeys($nroCnj); //PRIMEIRA TENTATIVA
                    $driver->findElement(WebDriverBy::xpath('//*[@id="ProcessoOrigem"]'))->clear();
                    $driver->findElement(WebDriverBy::xpath('//*[@id="ProcessoOrigem"]'))->sendKeys($nroCnj);
                    $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="ImagemCaptcha"]', '//*[@id="Captcha"]', '//*[@id="form"]/div/input[1]', false);

                    try {
                        $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="mensagemErro"]')));
                        $msg = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="mensagemErro"]'))->getText());
                        $robo->log($msg);
                        if (!preg_match('/Preencha corretamente o código que aparece na imagem/', $msg)) {
                            throw new \Exception('PROCESSO NÃO ENCONTRADO');
                        }
                    } catch (\Exception $e) {
                        break;
                    }

                }

                try {
                    $vara = '';

                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="consultaProcesso"]/div[6]/div[2]')));

                    $vara = $driver->findElement(WebDriverBy::xpath('//*[@id="consultaProcesso"]/div[6]/div[2]'))->getText();
                    $robo->log("$vara FIRST TRY");
                    $text = $driver->findElement(WebDriverBy::xpath('//*[@id="consultaProcesso"]'))->getText();

                    $res = $robo->findBetween($text, 'Vara', 'Data de ');

                    $robo->log("$res SECOND TRY");

                    if ($vara != '') {
                        $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                        $alterados++;
                    }

                } catch (\Exception $e) {
                    $robo->log("NÃO ENCONTRADO OU CAPTCHA ERRADO");
                }
            }

            $robo->log("{$alterados} JUIZO|VARA ENCONTRADOS!");
            $driver->quit();
            return true;

        } catch (\Exception $e) {
            $robo->log($e->getMessage());
        }
    }


    public function trf4($processos, $params = ['uf' => 'TRF', 'codeTribunal' => '.4.04.', 'puppet' => '50058812020124047204', 'tipoNro' => 'cnj'])
    {
        if (!count($params)) return false;
        /**
         * $params = ['uf'=>string,'codeTribunal'=>string]
         */
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $uf = $params['uf'];
        $codeT = $params['codeTribunal'];
        $robo->log("{$uf} {$codeT} - JUSTIÇA FEDERAL TRF4");
        print_r($params);
        $func = __FUNCTION__ . implode('', $params);
        $driver = $robo->startDriver();

        $linkGet = "https://www2.trf4.jus.br/trf4/controlador.php?acao=consulta_processual_resultado_pesquisa&txtValor=[NRO_PROCESSO]&selOrigem={$uf}&chkMostrarBaixados=S&selForma=NU&txtDataFase=01/01/1970&hdnRefId=26602b2ac9400a0f208de20fb8ff44fd&txtPalavraGerada=";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
                if (substr($nroProcesso, 0, 4) < 1970 || substr($nroProcesso, 0, 4) > date('Y')) continue;
            }
            if ($params['tipoNro'] == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;
            if ($codeT) if (!preg_match("/$codeT/", $nroProcesso)) continue;

//            if (!in_array(strlen(preg_replace('/\D/', '', $nroProcesso)), [10, 15, 20])) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
            }

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->saveCache($func, $processo->getNumProcJust());

            try {
                $driver->wait(1)->until(WebDriverExpectedCondition::alertIsPresent());
                $robo->log($driver->switchTo()->alert()->getText());
                $driver->switchTo()->alert()->accept();
                sleep(2);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $content = $robo->cleanString($driver->findElement(WebDriverBy::xpath('//*[@id="divConteudo"]'))->getText());
                if (preg_match('/Acesso automatizado ao Portal do TRF4 detectado/', $content)) {
                    $driver->quit();
                    $robo->proxy = $robo->getBestProxy();
                    $driver = $robo->startDriver(false, true);
                    $driver->get(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));
                }
            } catch (\Exception $e) {
            }

            //PUPPET - VALIDA E MANTER ACESSO COM UM PROCESSO QUE EXISTE.
            $puppet = false;
            $puppetNroProcesso = $params['puppet'];
            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="divConteudo"]/form/div[1]/img')));
                $robo->log("PUPPET!!!");
                $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $puppetNroProcesso, $linkGet));
                $puppet = true;
            } catch (\Exception $e) {
            }
            $driver = $robo->hasCaptcha($driver, 'xpath', '//*[@id="divConteudo"]/form/div[1]/img', '//*[@id="divConteudo"]/form/div[2]/input', '//*[@id="divConteudo"]/form/input[3]', false, '//*[@id="divConteudo"]/form/input[2]');
            if ($puppet) {
                try {
                    $robo->log($driver->switchTo()->alert()->getText());
                    $driver->switchTo()->alert()->accept();
                } catch (\Exception $e) {
                }
                $linkGet = $driver->getCurrentURL();
                $linkGet = str_replace($puppetNroProcesso, '[NRO_PROCESSO]', $linkGet);
                $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));
            }

            try {
                $robo->log($driver->switchTo()->alert()->getText());
                $driver->switchTo()->alert()->accept();
            } catch (NoAlertOpenException $e) {

                $vara = '';
                $array = [];

                $driver->wait(15)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('//*[@id="divConteudo"]')));
                $conteudoProcesso = $driver->findElement(WebDriverBy::xpath('//*[@id="divConteudo"]'))->getText();

//                $textToFind = 'Originário: ';
//                if (preg_match("/$textToFind/",$conteudoProcesso)) {
//                    $driver->findElement(WebDriverBy::xpath("//a[contains(text(), 'Nº')]"))->click();
//                $conteudoProcesso = $driver->findElement(WebDriverBy::xpath('//*[@id="divConteudo"]'))->getText();
//                }

                $textToFind = 'Órgão Atual: ';
                if (preg_match("/RECURSO CÍVEL/", $conteudoProcesso)) {
                    $textToFind = 'Competência: ';
                }


                $array = explode($textToFind, $conteudoProcesso);
                if (count($array) < 2) {
                    $textToFind = 'Órgão Julgador: ';
                    $array = explode($textToFind, $conteudoProcesso);

                    if (count($array) < 2) {
                        $robo->log("PROCESSO NÃO ENCONTRADO");
                        continue;
                    }
                }

                $robo->log("FOUND AT $textToFind");
                $vara = substr($array[1], 0, strpos($array[1], "\n"));
                $vara = $robo->cleanString($vara, ['Juízo Substituto da ', 'Juízo Federal da ']);
                if ($vara != '') $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                $encontrados++;

                //REUSO DO LINK C/ CAPTCHA
                $linkGet = $driver->getCurrentURL();
                $linkGet = str_replace($nroProcesso, '[NRO_PROCESSO]', $linkGet);

            } catch (NoSuchElementException $e) {
                $robo->log("NAO FOI POSSIVEL ENCONTRAR O ELEMENTO {$nroProcesso}");
            } catch (TimeOutException $e) {
                $robo->log("TIMEOUT AO ACESSAR {$nroProcesso}");
            } catch (\Exception $e) {
                $robo->log("ERRO CATASTROFICO {$nroProcesso}");
            }


        }

        $driver->quit();
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function trf5($processos, $params = ['uf' => 'TRF', 'codeTribunal' => '.4.05.', 'tipoNro' => 'cnj'])
    {
        if (!count($params)) return false;
        /**
         * $params = ['uf'=>string,'codeTribunal'=>string,'tipoNro'=>string]
         */
        //PRIMEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO CNJ
        //TERCEIRA LEVA => BUSCANDO PROCESSO UM A UM A PARTIR DO NRO ANTIGO
        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $uf = $params['uf'];
        $codeT = $params['codeTribunal'];
        $title = "{$uf} {$codeT} - JUSTIÇA FEDERAL TRF5";
        $robo->log($title);
        print_r($params);
        $func = __FUNCTION__ . implode('', $params);
        $driver = $robo->startDriver();

        $linkGet = "http://www4.trf5.jus.br/processo/[NRO_PROCESSO]";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = '';
            if (!$processo->getNumProcJust()) continue;
            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
                if (!$nroProcesso) continue;
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
                if (substr($nroProcesso, 0, 4) < 1970 || substr($nroProcesso, 0, 4) > date('Y')) continue;
            }
            if ($params['tipoNro'] == 'sem_mascara') {
                $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            }
            if (!$nroProcesso) continue;
            if ($codeT) if (!preg_match("/$codeT/", $nroProcesso)) continue;

            //if (!in_array(strlen(preg_replace('/\D/', '', $nroProcesso)), [10, 15, 20])) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());

            if ($params['tipoNro'] == 'cnj') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            }
            if ($params['tipoNro'] == 'cjf') {
                $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '####.##.##.######-#', 15);
            }

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }

            $driver->navigate()->to(str_replace('[NRO_PROCESSO]', $nroProcesso, $linkGet));

            $robo->saveCache($func, $processo->getNumProcJust());

            try {
                $conteudoProcesso = $driver->findElement(WebDriverBy::xpath('/html/body/table[1]'))->getText();
                $textToFind = 'VARA: ';
                $array = explode($textToFind, $conteudoProcesso);
                if (count($array) < 2) {
                    $robo->log("PROCESSO NÃO ENCONTRADO");
                    continue;
                }

                $vara = $robo->cleanString(substr($array[1], 0, strpos($array[1], "\n")));
                if ($vara != '') {
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }
            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    public function trf5pje($processos)
    {

        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRF5 PJE";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje.trf5.jus.br/pje/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/4.05/', $nroProcesso)) continue;
            //if (!$robo->yearValidate($nroProcesso, 11)) continue;
            //if (substr($nroProcesso, 0, 1) != '1') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($title, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

//            $nroProcesso = '0800502-24.2016.4.05.8000';

            $driver->executeScript("document.getElementById(\"consultaPublicaForm:Processo:ProcessoDecoration:Processo\").value=\"{$nroProcesso}\";", []);

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="consultaPublicaForm:captcha:captchaImg"]',
                    '//*[@id="consultaPublicaForm:captcha:j_id224:verifyCaptcha"]',
                    '//*[@id="consultaPublicaForm:pesq"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a')));

                $linkAux = $driver->findElement(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a'))->getAttribute('onclick');
                $piece = '/pje/ConsultaPublica/DetalheProcessoConsultaPublica/listView.seam?signedIdProcessoTrf=';
                $token = $robo->findBetween($linkAux, $piece, "');;A4J.AJAX.Submit(");
                $link = $piece . $token;
                $robo->log($link);

                $driver->executeScript("window.open();", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::className('propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
//                    try {
//                        if (preg_match('/Órgão Julgador Colegiado/', $cell->findElement(WebDriverBy::cssSelector('div.value'))->getText())) {
//                            $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText(), ['Órgão Julgador Colegiado']);
//                            break;
//                        }
//                    } catch (\Exception $e) {
//                    }
                    try {
                        if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                            $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                            break;
                        }
                    } catch (\Exception $e) {
                    }
                }
                if ($vara == '') {
                    foreach ($cells as $cell) {
                        try {
                            if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                                $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                                break;
                            }
                        } catch (\Exception $e) {
                        }
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function jftrf5pje($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $uf = '';
        foreach ($processos as $processo) {
            $uf = $processo->getUf();
            break;
        }
        $title = "JUSTIÇA_{$uf}_JF_PJE";

        $ufLower = strtolower($uf);

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "https://pje.jf{$ufLower}.jus.br/pje/ConsultaPublica/listView.seam";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());
            if (!$nroProcesso) continue;
            //CHECK CODE TRIBUNAL
            if (!preg_match('/4.05./', $nroProcesso)) continue;
            //if (!$robo->yearValidate($nroProcesso, 11)) continue;
            if (substr($nroProcesso, 0, 2) != '08') continue;
            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->cnjNumberFormatter($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($title, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($title, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            $driver->executeScript("document.getElementById(\"consultaPublicaForm:Processo:ProcessoDecoration:Processo\").value=\"{$nroProcesso}\";", []);
            $driver->findElement(WebDriverBy::xpath('//*[@id="consultaPublicaForm:nomeParte:nomeParteDecoration:nomeParte"]'))->click();

            while (true) {

                $driver = $robo->hasCaptcha($driver,
                    'xpath',
                    '//*[@id="consultaPublicaForm:captcha:captchaImg"]',
                    '//*[@id="consultaPublicaForm:captcha:j_id224:verifyCaptcha"]',
                    '//*[@id="consultaPublicaForm:pesq"]', false);

                try {
                    $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.errorFields.errors')));
                    $msg = $driver->findElement(WebDriverBy::cssSelector('.errorFields.errors'))->getText();
                    $msg = $robo->cleanString($msg);
                    if (preg_match('/Resposta incorreta/', $msg)) continue;
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $msg = $driver->wait(2)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('rich-messages')))->getText();
                $msg = $robo->cleanString($msg);
                $robo->log($msg);
                continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a')));

                $linkAux = $driver->findElement(WebDriverBy::cssSelector('table#consultaPublicaList2 tbody tr td a'))->getAttribute('onclick');
                $piece = '/pje/ConsultaPublica/DetalheProcessoConsultaPublica/listView.seam?signedIdProcessoTrf=';
                $token = $robo->findBetween($linkAux, $piece, "');;A4J.AJAX.Submit(");
                $link = $piece . $token;
                $robo->log($link);

                $driver->navigate()->to($robo->getUrlDomain($linkGet) . $link);

                $cells = $driver->findElements(WebDriverBy::cssSelector('div.propertyView'));
                $vara = '';
                foreach ($cells as $cell) {
                    if (preg_match('/Órgão Julgador/', $cell->findElement(WebDriverBy::cssSelector('div.name'))->getText())) {
                        $vara = $robo->cleanString($cell->findElement(WebDriverBy::cssSelector('div.value'))->getText());
                    }
                }

                if ($vara) {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

                $driver->executeScript("window.close()", []);
                @$driver->switchTo()->window(end($driver->getWindowHandles()));
                $driver->navigate()->refresh();

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($title);
        return true;
    }

    public function trf5creta($processos, $params = ['link' => ''])
    {
        if (!isset($params['link']) || !$params['link']) return true;

        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $uf = '';
        foreach ($processos as $processo) {
            $uf = $processo->getUf();
            break;
        }
        $title = "TRIBUNAL $uf JF CRETA";
        $func = $uf . __FUNCTION__;

        $robo->log($title);
        $driver = $robo->startDriver(false, false);

        $linkGet = $params['link'];
        $driver->get($robo->host);

        $processosTratados = [];

        foreach ($processos as $processo) {

            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());
            if (strlen($nroProcesso) < 15) continue;
            if (!$nroProcesso) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {


            $nroProcesso = $robo->onlyDigits($processo->getNumProcJust());

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists($func, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache($func, $processo->getNumProcJust());

            $driver->navigate()->to($linkGet);

            while (true) {

                $input = '/html/body/table/tbody/tr[3]/td/form/table[3]/tbody/tr/td[2]/input';
                $driver->wait(30)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($input)));

                $driver->findElement(WebDriverBy::xpath($input))->clear();
                $driver->findElement(WebDriverBy::xpath($input))->sendKeys($nroProcesso);

                $img = '/html/body/table/tbody/tr[3]/td/form/table[6]/tbody/tr/td[2]/a[1]/img';
                try {
                    $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath($img)));
                } catch (\Exception $e) {
                    $driver->findElement(WebDriverBy::xpath($img))->click();
                    sleep(1);
                }

                $driver = $robo->hasCaptcha(
                    $driver,
                    'xpath',
                    $img,
                    '/html/body/table/tbody/tr[3]/td/form/table[6]/tbody/tr/td[2]/input',
                    '//*[@id="btn_pesquisar"]',
                    false
                );

                try {
                    $driver->wait(3)->until(WebDriverExpectedCondition::alertIsPresent());
                    $alertText = $robo->cleanString($driver->switchTo()->alert()->getText());
                    $robo->log($alertText);
                    $driver->switchTo()->alert()->accept();
                    if (preg_match('/Código da imagem de segurança inválido/', $alertText) ||
                        preg_match('/O código de segurança deve ser preenchido/', $alertText)) {
                        $driver->findElement(WebDriverBy::xpath($img))->click();
                        sleep(1);
                        continue;
                    }
                    if (preg_match('/Pelo menos um dos critérios tem que ser preenchido/', $alertText)) {
                        $driver->get($linkGet);
                        continue;
                    }
                } catch (\Exception $e) {
                    break;
                }

            }

            try {
                $result = $robo->cleanString($driver->findElement(WebDriverBy::className('grid_center'))->getText());
                $robo->log($result);
                if ($result == 'Nenhum processo encontrado.') continue;
            } catch (\Exception $e) {
            }

            try {
                $driver->wait(3)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('grid')));

                $vara = '';
                $cells = $driver->findElements(WebDriverBy::cssSelector('td.grid'));
                foreach ($cells as $cell) {
                    $cellText = $cell->getText();
                    if (preg_match('/Juizado: /', $cellText)) {
                        $vara = $robo->cleanString($cellText, ['Juizado: ']);
                        break;
                    }
                }

                $closeAll = false;
                if ($vara == '') {
                    foreach ($cells as $cell) {
                        $a = $cell->findElement(WebDriverBy::tagName('a'));
                        if (preg_match('/abrirDetalhe/', $a->getAttribute('href'))) {
                            $a->click();

                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                            $driver->wait(5)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::xpath('/html/body/table/tbody/tr[6]/td/form/table')));
                            $tables = $driver->findElements(WebDriverBy::xpath('/html/body/table/tbody/tr[6]/td/form/table'));
                            foreach ($tables as $tableItem) {
                                $tableText = $robo->cleanString($tableItem->getText());
                                $robo->log($tableText);
                                if (preg_match('/Juizado /', $tableText)) {
                                    $vara = $robo->cleanString($tableText, ['Juizado / Cargo']);
                                    $closeAll = true;
                                    break;
                                }
                            }

                            //fechar depois
                            $driver->executeScript("window.close()", []);
                            @$driver->switchTo()->window(end($driver->getWindowHandles()));
                            $closeAll = true;
                        }
                        if ($closeAll) break;
                    }
                }

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    //salvar campo
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache($func);
        return true;
    }

    //STF
    public function stf($processos)
    {

        /** @var $driver \Facebook\WebDriver\WebDriver */
        /** @var $processos \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "SUPERIOR TRIBUNAL FEDERAL";
        $robo->log($title);

        $driver = $robo->startDriver();

        $linkGet = "http://portal.stf.jus.br/processos/listarProcessos.asp?numeroUnico=[NRO_UNICO_SEM_MASCARA]";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {
            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);
            if (!$nroProcesso) continue;

            if (!$robo->yearValidate($nroProcesso, 11)) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

            $nroProcesso = $robo->processNumberFormatter($processo->getNumProcJust(), '#######-##.####.#.##.####', 20);

            $nroProcesso = preg_replace('/\D/', '', $nroProcesso);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $nroProcesso)) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $nroProcesso);

            $driver->navigate()->to(str_replace('[NRO_UNICO_SEM_MASCARA]', $nroProcesso, $linkGet));

            try {

                $driver->wait(1)->until(WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('message-404')));
                $msg = $robo->cleanString($driver->findElement(WebDriverBy::className('message-404'))->getText());
                $robo->log($msg);


                $vara = '';
                if ($vara != '') {
                    debug($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }
            } catch (\Exception $e) {
                debug($e->getMessage());
                $robo->log("PROCESSO NÃO ENCONTRADO");
            }
        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }


    /**
     * MODELO DE MÉTODO
     */
    public function modelo($processos)
    {
        /** @var $processos \Tribunal\Service\Job[] */
        /** @var $processosTratados \Tribunal\Service\Job[] */
        $robo = new Robo();
        $title = "TRIBUNAL NOME DO TRIBUNAL";

        $robo->log($title);
        $driver = $robo->startDriver();

        $linkGet = "http://www2.tjba.jus.br/consultaprocessual/juizadocivel/numero.wsp?parametro=[NRO_PROCESSO]&wi.page.prev=juizadocivel/processoparte";
        $session = "http://www2.tjba.jus.br/consultaprocessual/index.wsp";
        $driver->get($robo->host);

        $processosTratados = [];
        foreach ($processos as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
//            if (strlen($nroProcesso) < 10) continue;
//            if (!$robo->yearValidate($nroProcesso,0)) continue;
//            if (!$nroProcesso) continue;
            if (!$processo->getNumProcJust()) continue;

            $processosTratados[] = $processo;
        }

        $total = count($processosTratados);
        $robo->log("$total PROCESSOS ON TARGET");
        $index = 1;
        $encontrados = 0;

        foreach ($processosTratados as $processo) {

//            $nroProcesso = preg_replace('/\D/', '', $processo->getNumProcJust());
            $nroProcesso = $processo->getNumProcJust();

            $driver->navigate()->to($session);

            $robo->log("{$robo->percentage($total,$index)} => {$nroProcesso}");
            $index++;

            if ($robo->cacheExists(__FUNCTION__, $processo->getNumProcJust())) {
                $robo->log("CACHE SKIPPING => $nroProcesso");
                continue;
            }
            $robo->saveCache(__FUNCTION__, $processo->getNumProcJust());

            try {
                $vara = '';

                if ($vara != '') {
                    $vara = $robo->cleanString($vara);
                    $robo->addVara($processo, $vara, false, $driver->getCurrentURL());
                    $encontrados++;
                }

            } catch (\Exception $e) {
                $robo->log("PROCESSO NÃO ENCONTRADO!");
                continue;
            }

        }

        $driver->quit();
        $robo->log($title);
        $robo->log("{$encontrados} JUIZO|VARA ENCONTRADOS!");
        $robo->clearCache(__FUNCTION__);
        return true;
    }

    public function addObs($obs)
    {
        if ($this->getObs() != '') {
            $this->setObs($obs . '|' . $this->getObs());
        } else {
            $this->setObs($obs);
        }
    }

}