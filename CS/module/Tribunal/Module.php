<?php
namespace Tribunal;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(
                'JsonFormatter' => '\Tribunal\View\Helper\JsonFormatter',
                'JsonToArrayFormatter' => '\Tribunal\View\Helper\JsonToArrayFormatter',
            ),
        );
    }
}
