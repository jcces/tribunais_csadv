<?php
/**
 * Created by PhpStorm.
 * User: bruno.rosa
 * Date: 20/11/15
 * Time: 15:46
 */

namespace Manager\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Attach extends AbstractEstruturaService {

    protected $Id;
    protected $Origin;
    protected $IdOrigin;
    protected $Path;
    protected $DtCreated;
    protected $DtUpdated;
    protected $CreatedBy;
    protected $UpdatedBy;
    protected $Deleted;

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $IdOrigin
     */
    public function setIdOrigin($IdOrigin)
    {
        $this->IdOrigin = $IdOrigin;
    }

    /**
     * @return mixed
     */
    public function getIdOrigin()
    {
        return $this->IdOrigin;
    }

    /**
     * @param mixed $Origin
     */
    public function setOrigin($Origin)
    {
        $this->Origin = $Origin;
    }

    /**
     * @return mixed
     */
    public function getOrigin()
    {
        return $this->Origin;
    }

    /**
     * @param mixed $Path
     */
    public function setPath($Path)
    {
        $this->Path = $Path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->Path;
    }

    /**
     * @return mixed
     */
    public function getDtCreated()
    {
        return $this->DtCreated;
    }

    /**
     * @param mixed $DtCreated
     */
    public function setDtCreated($DtCreated)
    {
        $this->DtCreated = $DtCreated;
    }

    /**
     * @return mixed
     */
    public function getDtUpdated()
    {
        return $this->DtUpdated;
    }

    /**
     * @param mixed $DtUpdated
     */
    public function setDtUpdated($DtUpdated)
    {
        $this->DtUpdated = $DtUpdated;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->CreatedBy;
    }

    /**
     * @param mixed $CreatedBy
     */
    public function setCreatedBy($CreatedBy)
    {
        $this->CreatedBy = $CreatedBy;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->UpdatedBy;
    }

    /**
     * @param mixed $UpdatedBy
     */
    public function setUpdatedBy($UpdatedBy)
    {
        $this->UpdatedBy = $UpdatedBy;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->Deleted;
    }

    /**
     * @param mixed $Deleted
     */
    public function setDeleted($Deleted)
    {
        $this->Deleted = $Deleted;
    }

} 