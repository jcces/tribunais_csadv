<?php

namespace Manager\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Group extends AbstractEstruturaService{

        protected $Id; 
        protected $Name;
        protected $Permissao;
        protected $DtCreated; 
        protected $DtUpdated; 
        protected $UpdatedBy; 
        protected $CreatedBy; 
        protected $Deleted;

    /**
     * @return mixed
     */
    public function getPermissao()
    {
        return $this->Permissao;
    }

    /**
     * @param mixed $Permissao
     */
    public function setPermissao($Permissao)
    {
        $this->Permissao = $Permissao;
    }

        public function getId()
            {
                return $this->Id;
            } 
        public function setId($Id)
            {
                return $this->Id = $Id;
            } 
        public function getName()
            {
                return $this->Name;
            } 
        public function setName($Name)
            {
                return $this->Name = $Name;
            } 
        public function getDtCreated()
            {
                return $this->DtCreated;
            } 
        public function setDtCreated($DtCreated)
            {
                return $this->DtCreated = $DtCreated;
            } 
        public function getDtUpdated()
            {
                return $this->DtUpdated;
            } 
        public function setDtUpdated($DtUpdated)
            {
                return $this->DtUpdated = $DtUpdated;
            } 
        public function getUpdatedBy()
            {
                return $this->UpdatedBy;
            } 
        public function setUpdatedBy($UpdatedBy)
            {
                return $this->UpdatedBy = $UpdatedBy;
            } 
        public function getCreatedBy()
            {
                return $this->CreatedBy;
            } 
        public function setCreatedBy($CreatedBy)
            {
                return $this->CreatedBy = $CreatedBy;
            } 
        public function getDeleted()
            {
                return $this->Deleted;
            } 
        public function setDeleted($Deleted)
            {
                return $this->Deleted = $Deleted;
            } 

}