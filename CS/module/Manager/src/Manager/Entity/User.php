<?php
/**
 * Created by PhpStorm.
 * User: bruno.rosa
 * Date: 20/11/15
 * Time: 15:46
 */

namespace Manager\Entity;

use Estrutura\Service\AbstractEstruturaService;

class User extends AbstractEstruturaService {
    protected $Id;
    protected $Name;
    protected $Email;
    protected $Login;
    protected $Password;
    protected $ProfileImage;
    protected $Occupation;
    protected $Comments;
    protected $Admin;
    protected $DtCreated;
    protected $DtUpdated;
    protected $CreatedBy;
    protected $UpdatedBy;
    protected $Deleted;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * @param mixed $Email
     */
    public function setEmail($Email)
    {
        $this->Email = $Email;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->Login;
    }

    /**
     * @param mixed $Login
     */
    public function setLogin($Login)
    {
        $this->Login = $Login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @param mixed $Password
     */
    public function setPassword($Password)
    {
        $this->Password = $Password;
    }

    /**
     * @return mixed
     */
    public function getProfileImage()
    {
        return $this->ProfileImage;
    }

    /**
     * @param mixed $ProfileImage
     */
    public function setProfileImage($ProfileImage)
    {
        $this->ProfileImage = $ProfileImage;
    }

    /**
     * @return mixed
     */
    public function getOccupation()
    {
        return $this->Occupation;
    }

    /**
     * @param mixed $Occupation
     */
    public function setOccupation($Occupation)
    {
        $this->Occupation = $Occupation;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->Comments;
    }

    /**
     * @param mixed $Comments
     */
    public function setComments($Comments)
    {
        $this->Comments = $Comments;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->Admin;
    }

    /**
     * @param mixed $Admin
     */
    public function setAdmin($Admin)
    {
        $this->Admin = $Admin;
    }

    /**
     * @return mixed
     */
    public function getDtCreated()
    {
        return $this->DtCreated;
    }

    /**
     * @param mixed $DtCreated
     */
    public function setDtCreated($DtCreated)
    {
        $this->DtCreated = $DtCreated;
    }

    /**
     * @return mixed
     */
    public function getDtUpdated()
    {
        return $this->DtUpdated;
    }

    /**
     * @param mixed $DtUpdated
     */
    public function setDtUpdated($DtUpdated)
    {
        $this->DtUpdated = $DtUpdated;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->CreatedBy;
    }

    /**
     * @param mixed $CreatedBy
     */
    public function setCreatedBy($CreatedBy)
    {
        $this->CreatedBy = $CreatedBy;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->UpdatedBy;
    }

    /**
     * @param mixed $UpdatedBy
     */
    public function setUpdatedBy($UpdatedBy)
    {
        $this->UpdatedBy = $UpdatedBy;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->Deleted;
    }

    /**
     * @param mixed $Delete
     */
    public function setDeleted($Deleted)
    {
        $this->Deleted = $Deleted;
    }
} 