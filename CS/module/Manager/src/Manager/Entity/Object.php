<?php

namespace Manager\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Object extends AbstractEstruturaService {

    protected $Id;
    protected $Type;
    protected $Name;
    protected $Description;
    protected $Prefix;
    protected $DtCreated;
    protected $DtUpdated;
    protected $CreatedBy;
    protected $UpdatedBy;
    protected $Deleted;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * @param mixed $Type
     */
    public function setType($Type)
    {
        $this->Type = $Type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param mixed $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->Prefix;
    }

    /**
     * @param mixed $Prefix
     */
    public function setPrefix($Prefix)
    {
        $this->Prefix = $Prefix;
    }

    /**
     * @return mixed
     */
    public function getDtCreated()
    {
        return $this->DtCreated;
    }

    /**
     * @param mixed $DtCreated
     */
    public function setDtCreated($DtCreated)
    {
        $this->DtCreated = $DtCreated;
    }

    /**
     * @return mixed
     */
    public function getDtUpdated()
    {
        return $this->DtUpdated;
    }

    /**
     * @param mixed $DtUpdated
     */
    public function setDtUpdated($DtUpdated)
    {
        $this->DtUpdated = $DtUpdated;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->CreatedBy;
    }

    /**
     * @param mixed $CreatedBy
     */
    public function setCreatedBy($CreatedBy)
    {
        $this->CreatedBy = $CreatedBy;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->UpdatedBy;
    }

    /**
     * @param mixed $UpdatedBy
     */
    public function setUpdatedBy($UpdatedBy)
    {
        $this->UpdatedBy = $UpdatedBy;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->Deleted;
    }

    /**
     * @param mixed $Deleted
     */
    public function setDeleted($Deleted)
    {
        $this->Deleted = $Deleted;
    }

}