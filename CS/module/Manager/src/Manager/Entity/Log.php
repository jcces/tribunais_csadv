<?php
/**
 * Created by PhpStorm.
 * User: bruno.rosa
 * Date: 20/11/15
 * Time: 15:46
 */

namespace Manager\Entity;

use Estrutura\Service\AbstractEstruturaService;

class Log extends AbstractEstruturaService {

    protected $Id;
    protected $Type;
    protected $TableName;
    protected $Fields;
    protected $IdUser;
    protected $Ip;
    protected $Hostname;
    protected $Browser;
    protected $DtCreated;
    protected $DtUpdated;
    protected $CreatedBy;
    protected $UpdatedBy;
    protected $Deleted;

    /**
     * @return mixed
     */
    public function getHostname()
    {
        return $this->Hostname;
    }

    /**
     * @param mixed $Hostname
     */
    public function setHostname($Hostname)
    {
        $this->Hostname = $Hostname;
    }

    /**
     * @return mixed
     */
    public function getBrowser()
    {
        return $this->Browser;
    }

    /**
     * @param mixed $Browser
     */
    public function setBrowser($Browser)
    {
        $this->Browser = $Browser;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->Ip;
    }

    /**
     * @param mixed $Ip
     */
    public function setIp($Ip)
    {
        $this->Ip = $Ip;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * @param mixed $Type
     */
    public function setType($Type)
    {
        $this->Type = $Type;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->TableName;
    }

    /**
     * @param mixed $TableName
     */
    public function setTableName($TableName)
    {
        $this->TableName = $TableName;
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->Fields;
    }

    /**
     * @param mixed $Fields
     */
    public function setFields($Fields)
    {
        $this->Fields = $Fields;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->IdUser;
    }

    /**
     * @param mixed $IdUser
     */
    public function setIdUser($IdUser)
    {
        $this->IdUser = $IdUser;
    }

    /**
     * @return mixed
     */
    public function getDtCreated()
    {
        return $this->DtCreated;
    }

    /**
     * @param mixed $DtCreated
     */
    public function setDtCreated($DtCreated)
    {
        $this->DtCreated = $DtCreated;
    }

    /**
     * @return mixed
     */
    public function getDtUpdated()
    {
        return $this->DtUpdated;
    }

    /**
     * @param mixed $DtUpdated
     */
    public function setDtUpdated($DtUpdated)
    {
        $this->DtUpdated = $DtUpdated;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->CreatedBy;
    }

    /**
     * @param mixed $CreatedBy
     */
    public function setCreatedBy($CreatedBy)
    {
        $this->CreatedBy = $CreatedBy;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->UpdatedBy;
    }

    /**
     * @param mixed $UpdatedBy
     */
    public function setUpdatedBy($UpdatedBy)
    {
        $this->UpdatedBy = $UpdatedBy;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->Deleted;
    }

    /**
     * @param mixed $Deleted
     */
    public function setDeleted($Deleted)
    {
        $this->Deleted = $Deleted;
    }
} 