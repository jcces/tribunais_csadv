<?php
namespace Manager\Entity;

use Estrutura\Service\AbstractEstruturaService;

class PasswordRecovery extends AbstractEstruturaService {

    protected $Id;
    protected $IdUser;
    protected $Token;
    protected $Active;
    protected $DtUpdate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param mixed $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->IdUser;
    }

    /**
     * @param mixed $IdUser
     */
    public function setIdUser($IdUser)
    {
        $this->IdUser = $IdUser;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->Token;
    }

    /**
     * @param mixed $Token
     */
    public function setToken($Token)
    {
        $this->Token = $Token;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->Active;
    }

    /**
     * @param mixed $Active
     */
    public function setActive($Active)
    {
        $this->Active = $Active;
    }

    /**
     * @return mixed
     */
    public function getDtUpdate()
    {
        return $this->DtUpdate;
    }

    /**
     * @param mixed $DtUpdate
     */
    public function setDtUpdate($DtUpdate)
    {
        $this->DtUpdate = $DtUpdate;
    }

} 