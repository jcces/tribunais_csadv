<?php
namespace Manager\View\Helper;
use Manager\Service\Group;
use Manager\Service\User;
use Zend\Session\Container;
use Zend\View\Helper\AbstractHelper;

class Acl extends AbstractHelper
{
    public function __invoke($controller=false)
    {
        $group = new Group();
        $user = new User();

        $permissoes = $group->getPermissoesUser($user->checkLoggedUser('Id'));
        return in_array($controller,$permissoes);
    }
}