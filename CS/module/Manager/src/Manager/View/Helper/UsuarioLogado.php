<?php
namespace Manager\View\Helper;
use Zend\Session\Container;
use Zend\View\Helper\AbstractHelper;

class UsuarioLogado extends AbstractHelper
{
    /**
     * @param string $data
     * @param bool $formato
     * @return \DateTime
     */
    public function __invoke()
    {
        $sessao = new Container('autenticacao');
        return $sessao->autenticacao;
    }
}