<?php
namespace Manager\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Manager\Service\DocumentReader;

class ViewDocument extends AbstractHelper
{

    public function __invoke($filePath)
    {
        $docReader = new DocumentReader();
        return $docReader->readDocx($filePath);
    }

}