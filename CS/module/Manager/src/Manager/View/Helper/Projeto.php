<?php
namespace Manager\View\Helper;

use Estrutura\Service\Config;
use Zend\View\Helper\AbstractHelper;

class Projeto extends AbstractHelper
{
    /**
     * @param string $data
     * @param bool $formato
     * @return \DateTime
     */
    public function __invoke()
    {
        return Config::getConfig('nomeProjeto');
    }
}