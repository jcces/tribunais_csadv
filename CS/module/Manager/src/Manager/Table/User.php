<?php

namespace Manager\Table;

use Estrutura\Table\AbstractEstruturaTable;

class User extends AbstractEstruturaTable {

    public $table = 'MANAGER_USER';
    public $campos = [
        'ID' => 'Id',
        'NAME' => 'Name',
        'EMAIL' => 'Email',
        'LOGIN' => 'Login',
        'PASSWORD' => 'Password',
        'PROFILE_IMAGE' => 'ProfileImage',
        'OCCUPATION' => 'Occupation',
        'COMMENTS' => 'Comments',
        'ADMIN'=>'Admin',
        'DT_UPDATE' => 'DtUpdate',
        'DT_CREATED' => 'DtCreated',
        'CREATED_BY' => 'CreatedBy',
        'UPDATED_BY' => 'UpdatedBy',
        'DELETED'=>'Deleted'
    ];
} 