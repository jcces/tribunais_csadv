<?php

namespace Manager\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Log extends AbstractEstruturaTable {

    public $table = 'MANAGER_LOG';
    public $campos = [
        'ID' => 'Id',
        'TYPE' => 'Type',
        'TABLE_NAME' => 'TableName',
        'FIELDS' => 'Fields',
        'ID_USER' => 'IdUser',
        'IP' => 'Ip',
        'HOSTNAME' => 'Hostname',
        'BROWSER' => 'Browser',
        'DT_CREATED' => 'DtCreated',
        'DT_UPDATED' => 'DtUpdated',
        'CREATED_BY' => 'CreatedBy',
        'UPDATED_BY' => 'UpdatedBy',
        'DELETED'=>'Deleted'
    ];

} 