<?php

namespace Manager\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Object extends AbstractEstruturaTable {

    public $table = 'MANAGER_OBJECT';
    public $campos = [
        'ID' => 'Id',
        'TYPE' => 'Type',
        'NAME' => 'Name',
        'DESCRIPTION'=>'Description',
        'PREFIX'=>'Prefix',
        'DT_UPDATED' => 'DtUpdated',
        'DT_CREATED' => 'DtCreated',
        'CREATED_BY' => 'CreatedBy',
        'UPDATED_BY' => 'UpdatedBy',
        'DELETED'=>'Deleted'
    ];

} 