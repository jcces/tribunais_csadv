<?php

namespace Manager\Table;

use Estrutura\Table\AbstractEstruturaTable;

class GroupUser extends AbstractEstruturaTable{

    public $table = 'MANAGER_GROUP_USER';
    public $campos = [
        'ID'=>'Id', 
        'ID_USER'=>'IdUser', 
        'ID_GROUP'=>'IdGroup', 
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}