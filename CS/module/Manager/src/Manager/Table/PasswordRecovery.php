<?php

namespace Manager\Table;

use Estrutura\Table\AbstractEstruturaTable;

class PasswordRecovery extends AbstractEstruturaTable {

    public $table = 'MANAGER_PASSWORD_RECOVERY';
    public $campos = [
        'ID' => 'Id',
        'ID_USER' => 'IdUser',
        'TOKEN' => 'Token',
        'ACTIVE' => 'Active',
        'DT_UPDATE' => 'DtUpdate',
        'DT_CREATED' => 'DtCreated',
        'CREATED_BY' => 'CreatedBy',
        'UPDATED_BY' => 'UpdatedBy',
        'DELETED'=>'Deleted'
    ];

} 