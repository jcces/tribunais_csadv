<?php
namespace Manager\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Attach extends AbstractEstruturaTable {

    public $table = 'MANAGER_ATTACH';
    public $campos = [
        'ID' => 'Id',
        'ORIGIN' => 'Origin',
        'ID_ORIGIN' => 'IdOrigin',
        'PATH' => 'Path',
        'DT_UPDATE' => 'DtUpdate',
        'DT_CREATED' => 'DtCreated',
        'CREATED_BY' => 'CreatedBy',
        'UPDATED_BY' => 'UpdatedBy',
        'DELETED'=>'Deleted'
    ];

} 