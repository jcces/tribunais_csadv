<?php

namespace Manager\Table;

use Estrutura\Table\AbstractEstruturaTable;

class Group extends AbstractEstruturaTable{

    public $table = 'MANAGER_GROUP';
    public $campos = [
        'ID'=>'Id', 
        'NAME'=>'Name',
        'PERMISSAO'=>'Permissao',
        'DT_CREATED'=>'DtCreated', 
        'DT_UPDATED'=>'DtUpdated', 
        'UPDATED_BY'=>'UpdatedBy', 
        'CREATED_BY'=>'CreatedBy', 
        'DELETED'=>'Deleted', 

    ];

}