<?php
/**
 * Created by PhpStorm.
 * User: bruno.rosa
 * Date: 20/11/15
 * Time: 16:08
 */

namespace Manager\Service;

use \Manager\Entity\Attach as Entity;

class Attach extends Entity {

    public function toBase64()
    {
        if(file_exists($this->getPath())){
            return base64_encode(file_get_contents($this->getPath()));
        }
    }

    public function excluir($forced=false){
        $this->setDeleted(1);
        $this->salvar();
        //if($this->getPath()) unlink($this->getPath());
        //$arr = $this->hydrate();
        //$ret = $this->getTable()->delete($arr);
    }

    public function getType(){
        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'docx' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $data = explode('.',$this->getPath());
        $ext = end($data);

        if(isset($mime_types[$ext])) return $mime_types[$ext];
        debug('Extenção '.$ext.' Não encontrada \Service\Anexo - 87');
    }

    public function getName(){
       $data = explode("/",$this->getPath());
       return end($data);
    }

    public function deleteAttach(){
        return $this->getTable()->salvar(['DELETED'=>1],$this->hydrate());
    }

}
