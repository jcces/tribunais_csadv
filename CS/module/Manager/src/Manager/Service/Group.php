<?php

namespace Manager\Service;

use Estrutura\Service\Conexao;
use \Manager\Entity\Group as Entity;

class Group extends Entity{
    public function getUsers(){
        $sql = "SELECT USER.*,GP.ID AS ID_REL FROM MANAGER_GROUP_USER GP INNER JOIN MANAGER_USER USER ON USER.ID = GP.ID_USER WHERE USER.DELETED = 0 AND GP.DELETED = 0 AND GP.ID_GROUP = '{$this->getId()}'";
        $data = Conexao::listarSql($sql);
        return $this->populateService(new \Manager\Service\User(),$data);
    }

    public function getUserNotIn()
    {
        if(!$this->getId()) return [];
        $sql = "SELECT * FROM MANAGER_USER WHERE ID NOT IN(SELECT ID_USER FROM MANAGER_GROUP_USER WHERE ID_GROUP = '{$this->getId()}')";
        $data = Conexao::listarSql($sql);
        return $this->populateService(new \Manager\Service\User(),$data);
    }

    public function excluir($forced = false)
    {
        $sql = "UPDATE MANAGER_GROUP_USER SET DELETED = 1 WHERE ID_GROUP = '{$this->getId()}'";
        Conexao::execSql($sql);
        return parent::excluir($forced);
    }

    public function getModulos(){
        $modulos = ['Upload','Subsidio','Triagem','Protocolo','Usuarios','Tribunais'];
        $meus = explode(';',$this->getPermissao());

        $tratado = [];
        foreach($modulos as $item){
            $tratado[] = ['modulo'=>$item,'meu'=>in_array($item,$meus)];
        }
        return $tratado;
    }

    public function getPermissoesUser($id){
        $sql = "SELECT * FROM MANAGER_GROUP WHERE ID IN (SELECT ID_GROUP FROM MANAGER_GROUP_USER WHERE ID_USER = '{$id}')";
        $lista = Conexao::listarSql($sql);

        $permissoes = [];
        foreach($lista as $group){
            $perm = explode(';', $group->PERMISSAO);
            foreach($perm as $item){
                $permissoes[$item] = $item;
            }
        }
        return array_keys($permissoes);
    }
}