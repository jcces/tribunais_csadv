<?php
namespace Manager\Service;

use \Manager\Entity\Object as Entity;

class Object extends Entity {
    public function getTypes(){
        return [
            'CATEGORIA'=>'Categoria'
        ];
    }
}