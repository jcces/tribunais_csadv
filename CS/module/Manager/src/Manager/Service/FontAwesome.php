<?php
namespace Manager\Service;

class FontAwesome {

    public $fontAweseome = './public/assets/fontawesome/css/font-awesome.css';

    public function array_delete($array, $element) {
        return (is_array($element)) ? array_values(array_diff($array, $element)) : array_values(array_diff($array, array($element)));
    }

    public function getIcons()
    {
        $file = file_get_contents($this->fontAweseome);
        preg_match_all("/fa\-([a-zA-z0-9\-]+[^\:\.\,\s])/", $file, $matches);
        $exclude_icons = array("fa-lg", "fa-2x", "fa-3x", "fa-4x", "fa-5x", "fa-ul", "fa-li", "fa-fw", "fa-border", "fa-pulse", "fa-rotate-90", "fa-rotate-180", "fa-rotate-270", "fa-spin", "fa-flip-horizontal", "fa-flip-vertical", "fa-stack", "fa-stack-1x", "fa-stack-2x", "fa-inverse");
        $icons = $this->array_delete($matches[0], $exclude_icons);
        asort($icons);
        $tratados = [];
        $tratados[] = 'Selecione';
        foreach ($icons as $icon){
            $tratados[$icon] = ucwords(str_replace('-',' ',str_replace('fa-','',$icon)));
        }
        return $tratados;
    }

    public function getIconsCode()
    {
        $file = file_get_contents($this->fontAweseome);
        $icons = $this->getIcons();
        unset($icons[0]);
        $tratados = [];
        foreach ($icons as $class => $friendlyName) {
            $pieceStart = strpos($file,$class);
            $piece = substr($file,$pieceStart,100);
            $codeStop = strpos($piece,'";');
            $code = substr($piece,$codeStop-4,4);
            $tratados[$class] = $code;
        }

        return $tratados;

    }

}