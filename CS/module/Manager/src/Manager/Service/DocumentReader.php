<?php
namespace Manager\Service;

class DocumentReader {

    public function readDocx($filePath) {
        // Create new ZIP archive
        $zip = new \ZipArchive;
        $dataFile = 'word/document.xml';
        // Open received archive file
        if (true === $zip->open($filePath)) {
            // If done, search for the data file in the archive
            if (($index = $zip->locateName($dataFile)) !== false) {
                // If found, read it to the string
                $data = $zip->getFromIndex($index);
                // Close archive file
                $zip->close();
                // Load XML from a string
                // Skip errors and warnings
                $domD = new \DOMDocument();
                $domD->loadXML($data);
                // Return data without XML formatting tags
//                $contents = explode('\n',strip_tags($domD->saveXML()));
                $contents = strip_tags($domD->saveXML());
//                $contents = $domD->saveXML();
//                debug($contents);
                $text = '';

//                $simpleXML = new \SimpleXMLIterator($data);
//
//                foreach ($simpleXML as $item) {
//                    debug($item);
//                }
//                debug($simpleXML);
                return $contents;
                debug($contents);
                foreach($contents as $i=>$content) {
                    $text .= '<p>'.$contents[$i].'</p>';
                }
                return $text;
            }
            $zip->close();
        }
        // In case of failure return empty string
        return "";
    }

}