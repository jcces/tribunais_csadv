<?php
namespace Manager\Service;

use Estrutura\Service\Conexao;
use \Manager\Entity\Log as Entity;

class Log extends Entity {

    public function deleteExpiredLog($date)
    {
        $sql = 'UPDATE MANAGER_LOG
                SET
                DELETED = 1
                WHERE 
                DT_CREATED < "'.$date.'"';
        return Conexao::execSql($sql);
    }

    public function typeMap($status)
    {
        $map = ['insert'=>'Novo Registro','update'=>'Atualização','delete'=>'Exclusão'];
        $color = 'red';
        if($status == 'insert') $color='green';
        if($status == 'update') $color='blue';
        return '<span class="badge" style="background-color:'.$color.'">'.$map[$status].'</span>';
    }

    public function typeIconMap($status)
    {
        $map = ['insert'=>'fa-plus','update'=>'fa-edit','delete'=>'fa-trash'];
        $color = 'bg-red';
        if($status == 'insert') $color='bg-green';
        if($status == 'update') $color='bg-blue';
        return $map[$status].' '.$color;
    }

    public function friendlyTablename()
    {
        if(!$this->getTablename()) return '';
        $arr = explode('_',$this->getTablename());
        $title = implode(' ',array_splice($arr,1));
        return ucfirst(strtolower($title));
    }

}