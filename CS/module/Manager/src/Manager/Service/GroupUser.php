<?php

namespace Manager\Service;

use \Manager\Entity\GroupUser as Entity;

class GroupUser extends Entity{

        public $classUser = '\Manager\Service\User';
        public $classGroup = '\Manager\Service\Group';

        public function getObjUser(){
            $class = $this->classUser;
            $service = new $class;
            $service->setId($this->getIdUser());
            $service->load();
            return $service;
        }
        public function getObjGroup(){
            $class = $this->classGroup;
            $service = new $class;
            $service->setId($this->getIdGroup());
            $service->load();
            return $service;
        }

}