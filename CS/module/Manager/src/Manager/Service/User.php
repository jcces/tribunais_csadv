<?php
namespace Manager\Service;

use Estrutura\Service\Config;
use \Manager\Entity\User as Entity;
use Robot\Service\Archer;
use Zend\Session\Container;

class User extends Entity {

    private $salt;

    public function hashifyPass($pass){
        $this->salt = Config::getConfig('salt');
        $salt = hash('md5',$this->salt);
        $pass = $pass.$salt;
        $pass = hash('md5',$pass);
        return $pass;
    }

    public function validatePass($post){
        if($post['Password'] == '') throw new \Exception('Campo Senha é de preechimento obrigatório.');
        if($post['ConfirmarSenha'] == '' ) throw new \Exception('Campo Confirmar Senha é de preechimento obrigatório.');
        if($post['ConfirmarSenha'] != $post['Password']) throw new \Exception('Senha divergente.');
        $min = 8;
        $max = 16;
        $len = strlen($post['Password']);
        if($len < $min || $len > $max) throw new \Exception('A senha deve conter no mínimo 8 e no máximo 16 caracteres.');
        if(!preg_match('#[0-9]+#', $post['Password'])) throw new \Exception('A senha deve conter pelo menos um número.');
        if(!preg_match('#[a-zA-Z]+#', $post['Password'])) throw new \Exception('A senha deve conter pelo menos uma letra.');
        if(preg_match('/[^\w@]/', $post['Password'])) throw new \Exception('A senha não pode conter caracteres especiais, com exceção do "@".');
        $post['Password'] = $this->hashifyPass($post['Password']);

        return $post;
    }

    public function validateUserPass($post)
    {
        $sessao = new \Zend\Session\Container('autenticacao');
        $login = $sessao->autenticacao['Login'];

        $user = new User();
        $user->setLogin($login);
        $user->setPassword($this->hashifyPass($post['SenhaAtual']));
        $dados = $user->filtrarObjeto()->current();
        if(!$dados) throw new \Exception('Senha atual incorreta.');
        $dados = $dados->toArray();
        foreach($dados as $index => $valor){
            if($index != 'Id') continue;
            $post[$index] = $valor;
        }

        return $post;
    }

    public function validateUser($username)
    {
        $user = new User();
        $user->setLogin($username);
        $dados = $user->filtrarObjeto()->current();
        if($dados) throw new \Exception('Usuário já está sendo utilizado por outra pessoa.');
    }

    public function autenticar($dados)
    {
        $sessao = new Container('autenticacao');

        if(!$sessao->autenticacao){
            $user = new User();
            $user->setLogin($dados['Login']);
            $user->setPassword($this->hashifyPass($dados['Password']));
            $dados = $user->filtrarObjeto()->current();
            if(!$dados) throw new \Exception('Usuário ou Senha incorreta.');
            $dados = $dados->toArray();
            unset($dados['Password']);
            $dados['Token'] = ['Token'=>'','userId'=>$dados['Comments']];
            $sessao->autenticacao = $dados;
        }
    }

    public function checkLoggedUser($field='Login')
    {
        $sessao = new Container('autenticacao');
        return $sessao->autenticacao[$field];
    }

    public function validateUnlockScreen($dados)
    {
        if($this->checkLoggedUser() != $dados['Login']) throw new \Exception('Usuário incorreto.');
        $user = new User();
        $user->setLogin($dados['Login']);
        $user->setPassword($this->hashifyPass($dados['Password']));
        $dados = $user->filtrarObjeto()->current();
        if(!$dados) throw new \Exception('Senha incorreta.');

        return true;
    }

    public function lockIt($historyUrl)
    {
        $sessao = new Container('autenticacao');
        $sessao->autenticacao['lock'] = 1;
        if($historyUrl == '/'){
            if(!isset($sessao->autenticacao['historyUrl']) || $sessao->autenticacao['historyUrl'] == '') $sessao->autenticacao['historyUrl'] = $historyUrl;
        }else{
            $sessao->autenticacao['historyUrl'] = $historyUrl;
        }
    }

    public function unlockIt()
    {
        $sessao = new Container('autenticacao');
        $sessao->autenticacao['lock'] = 0;
        $historyUrl = $sessao->autenticacao['historyUrl'];
        $sessao->autenticacao['historyUrl'] = '';
        return $historyUrl;
    }

    public function atualizarBase($esse=false){
        $archer = new Archer();
        $users = $archer->getUsers();
        $atualizar = [];
        foreach($users as $user){
            if($esse){
                if($esse == $user['RequestedObject']['UserName']){
                    $atualizar[] = $user['RequestedObject'];
                }
            }else{
                $atualizar[] = $user['RequestedObject'];
            }
        }

        foreach ($atualizar as $item){
            $load = new User();
            $load->setLogin($item['UserName']);
            if(!$load->filtrarObjeto()->current()){
                $user = new User();
                $user->setName($item['UserName']);
                $user->setEmail($item['UserName'].'@costaesilvaadv.com.br');
                $user->setPassword($user->hashifyPass('CostaSilva123'));
                $user->setLogin($item['UserName']);
                $user->setProfileImage('./data/upload/default.jpg');
                $user->setComments($item['Id']);
                $user->salvar();
            }
        }
    }

}