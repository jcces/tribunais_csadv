<?php

namespace Manager\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Object extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('object');

        $service = new \Manager\Service\Object();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('object',$this,$this->inputFilter);
        $objForm->hidden('Id')->required(false);
        $objForm->text('Name')->required(true)->label('Nome')->setAttribute('col','col-md-6');
        $objForm->text('Prefix')->required(false)->label('Prefix')->setAttribute('col','col-md-6');
        $objForm->text('Description')->required(false)->label('Descrição')->setAttribute('col','col-md-6');
        $objForm->select('Type', $service->getTypes())->required(true)->label('Tipo')->setAttribute('col','col-md-6');

    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}