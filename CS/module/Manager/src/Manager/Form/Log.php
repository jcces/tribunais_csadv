<?php

namespace Manager\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Log extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('log');

        $service = new \Manager\Service\Log();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('log',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("")->setAttribute('col','col-md-6');  
        $objForm->text("Type")->required(true)->label("")->setAttribute('col','col-md-6');  
        $objForm->text("TableName")->required(true)->label("")->setAttribute('col','col-md-6');
        $objForm->text("Fields")->required(true)->label("")->setAttribute('col','col-md-6');  
        $objForm->text("IdUser")->required(true)->label("")->setAttribute('col','col-md-6');  
        $objForm->select("Ip", $service->optionsIp)->required(false)->label("Ip do Máquina do Usuário")->setAttribute('col','col-md-6');  
        $objForm->text("Hostname")->required(false)->label("Hostname da Máquina do Usuário")->setAttribute('col','col-md-6');  
        $objForm->text("Browser")->required(false)->label("Navegador da Máquina do Usuário")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}