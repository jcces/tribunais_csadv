<?php

namespace Manager\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class GroupUser extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('groupuser');

        $service = new \Manager\Service\GroupUser();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('groupuser',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("")->setAttribute('col','col-md-6');  
        $objForm->combo("IdUser", $service->classUser)->required(true)->label("Usuário")->setAttribute('col','col-md-6');  
        $objForm->combo("IdGroup", $service->classGroup)->required(true)->label("Grupo")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}