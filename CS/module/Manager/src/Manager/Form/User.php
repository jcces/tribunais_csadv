<?php

namespace Manager\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Estrutura\Service\Config;
use Zend\InputFilter\InputFilter;

class User extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('user');

        $permissoes = ['0'=>'Nenhuma','1'=>'Leitura','2'=>'Total'];

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('users',$this,$this->inputFilter);
        $objForm->hidden('Id')->required(false);
        $objForm->text('Name')->required(true)->label('Nome');
        $objForm->email('Email')->required(true)->label('E-mail');
        $objForm->text('Login')->required(true)->label('Usuário');
        $objForm->text('Occupation')->required(false)->label('Cargo');
        $objForm->file('ProfileImage')->required(false)->label('Foto Profile');
        $objForm->password('SenhaAtual',8,16)->required(false)->label('Senha Atual');
        $objForm->password('Password',8,16)->required(false)->label('Senha');
        $objForm->password('ConfirmarSenha',8,16)->required(false)->label('Confirmar Senha');
        $objForm->textarea('Comments')->required(false)->label('Observações');

        $objForm->tableRel('Attach', '\Manager\Service\Attach','IdOrigem',['ORIGEM'=>'Origem','PATH'=>'Caminho']);

    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}