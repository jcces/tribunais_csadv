<?php
namespace Manager\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class Attach extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('Attach');

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('attach',$this,$this->inputFilter);
        $objForm->hidden('Id')->required(false);

    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}