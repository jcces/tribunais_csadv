<?php

namespace Manager\Form;

use Estrutura\Form\AbstractForm;
use Estrutura\Form\FormObject;
use Zend\InputFilter\InputFilter;

class PasswordRecovery extends AbstractForm{
    public function __construct($options=[]){
        parent::__construct('passwordrecovery');

        $service = new \Manager\Service\PasswordRecovery();

        $this->inputFilter = new InputFilter();
        $objForm = new FormObject('passwordrecovery',$this,$this->inputFilter);
        $objForm->hidden("Id")->required(false)->label("")->setAttribute('col','col-md-6');  
        $objForm->combo("IdUser", $service->classUser)->required(true)->label("")->setAttribute('col','col-md-6');  
        $objForm->text("Token")->required(true)->label("")->setAttribute('col','col-md-6');  
        $objForm->select("Active", $service->optionsActive)->required(true)->label("")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtCreated")->required(false)->label("Data de Criação")->setAttribute('col','col-md-6');  
        $objForm->hidden("DtUpdated")->required(false)->label("Data de Atualização")->setAttribute('col','col-md-6');  
        $objForm->hidden("UpdatedBy")->required(false)->label("Criado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("CreatedBy")->required(false)->label("Atualizado Por")->setAttribute('col','col-md-6');  
        $objForm->hidden("Deleted")->required(false)->label("Deletado")->setAttribute('col','col-md-6');  

        $this->formObject = $objForm;
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}