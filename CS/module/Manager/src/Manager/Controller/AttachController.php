<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manager\Controller;

use Estrutura\Controller\AbstractCrudController;
use Manager\Service\Attach;
use Zend\View\Model\JsonModel;

class AttachController extends AbstractCrudController
{

    protected $service;
    protected $form;

    public function __construct()
    {
        parent::init();
    }

    public function objetosAction()
    {
        return parent::index($this->service, $this->form);
    }

    public function cadastroAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function excluirAction()
    {
        return parent::excluir($this->service,$this->form,[],true);
    }

    public function excluirAjaxAction(){
        $anexo = new Attach();
        $anexo->setId($this->params('id'));
        if($anexo->load()){
            $anexo->excluir();
        }
        return new JsonModel(['error'=>false]);
    }

}
