<?php

namespace Manager\Controller;

use Estrutura\Controller\AbstractCrudController;
use Manager\Service\Group;
use Manager\Service\GroupUser;
use Zend\View\Model\ViewModel;

class GroupController extends AbstractCrudController
{
    /**
     * @var \Manager\Service\Group
     */
    protected $service;

    /**
     * @var \Manager\Form\Group
     */
    protected $form;

    public function __construct(){
        parent::init();
    }

    public function indexAction()
    {
        return parent::index($this->service, $this->form);
    }

    public function gravarAction(){
        return parent::gravar($this->service, $this->form);
    }

    public function posSave(){
        $post = $this->getRequest()->getPost();
        if(isset($post['modulo'])){
            $permissao = implode(';', $post['modulo']);
            $this->service->setPermissao($permissao);
            $this->service->salvar();
        }else{
            $this->service->setPermissao('');
            $this->service->salvar();
        }
    }

    public function cadastroAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function excluirAction()
    {
        return parent::excluir($this->service, $this->form);
    }

    public function findUsersAction(){
        try{
            $group = new Group();
            $group->setId($this->params('id'));
            $users = $group->getUserNotIn();
            $view = new ViewModel(['list'=>$users]);
            $view->setTerminal(true);
            return $view;
        }catch(\Exception $e){
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/'.$this->params('controller'));
        }
    }

    public function removeAction(){
        if(!$this->params('id')) die('Falta ID');
        $service = new GroupUser();
        $service->setId($this->params('id'));
        $service->load();

        $service->excluir(true);
        $this->addSuccessMessage('Usuário Removido');

        return $this->redirect()->toUrl('/group/cadastro/'.$service->getIdGroup());
    }

    public function addUserAction(){
        try{
            $post = $this->getRequest()->getPost();

            if(!count($post['IdUser'])){
                $this->addErrorMessage('Favor informar pelo menos um usuário');
                return $this->redirect()->toUrl('/group/cadastro/'.$post['IdGroup']);
            }

            $service = new GroupUser();
            $service->setIdGroup($post['IdGroup']);
            foreach($post['IdUser'] as $user){
                $service->setIdUser($user);
                $service->salvar();
                $service->setId('');
            }

            $this->addSuccessMessage('Usuários adicionado com sucesso');
            return $this->redirect()->toUrl('/group/cadastro/'.$post['IdGroup']);
        }catch(\Exception $e){
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/group');
        }
    }
}
