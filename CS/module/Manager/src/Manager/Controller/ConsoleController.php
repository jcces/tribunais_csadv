<?php

namespace Manager\Controller;

use Estrutura\Controller\AbstractEstruturaController;
use Estrutura\Service\Conexao;
use Estrutura\Service\Config;
use Robot\Service\Archer;
use Robot\Service\ArcherSoap;
use Robot\Service\Autodelete;
use Robot\Service\Files;
use Robot\Service\Manifestacao;
use Robot\Service\PacoteRemessa;
use Robot\Service\PagamentoCt;
use Robot\Service\Pagamentos;
use Robot\Service\Processo;
use Robot\Service\Reanalise;
use Robot\Service\Regras;
use Robot\Service\Relacionamento;
use Robot\Service\Remessa;
use Robot\Service\SCPJud;
use Robot\Service\Tipo;
use Robot\Service\TipoDocumento;
use Estrutura\Service\PdfService;
use Robot\Service\UploadSubsidio;
use Symfony\Component\Process\Process;
use Zend\Uri\File;

class ConsoleController extends AbstractEstruturaController
{
    public $path = '';

    public function __construct(){
        $config = Config::getConfig('INTEGRA');
        $this->path = $config['folder'];
    }

    public function registerAction()
    {
        $processo = new Processo();
        $tipo = new Tipo();

        $listaProcesso = $processo->getMatriz();
        $listaTipo = $tipo->getMatriz();

        foreach(scandir($this->path) as $processo){
            if(in_array($processo, ['.','..'])) continue;
            foreach(scandir($this->path.DIRECTORY_SEPARATOR.$processo) as $tipoDoc){
                if(in_array($tipoDoc, ['.','..'])) continue;
                foreach(scandir($this->path.DIRECTORY_SEPARATOR.$processo.DIRECTORY_SEPARATOR.$tipoDoc) as $documento){
                    if(in_array($documento, ['.','..'])) continue;
                    $dataEx = explode('.',$documento);
                    $ext = end($dataEx);

                    $finalFile = $this->path.DIRECTORY_SEPARATOR.$processo.DIRECTORY_SEPARATOR.$tipoDoc.DIRECTORY_SEPARATOR.$documento;

                    $data = [];
                    $data['Size'] = filesize($finalFile);

                    $encTipoDoc = ($tipoDoc);
                    $encDocumento = ($documento);

                    $data['Path'] = base64_encode($finalFile);
                    $data['Hash'] = md5($finalFile);

                    $data['Name'] = $encDocumento;

                    $data['Ext'] = $ext;
                    $data['Status'] = 0;
                    $data['CodProcesso'] = '@'.$processo;
                    $data['CodTipoArquivo'] = '@'.$encTipoDoc;

                    $msg = [];
                    if(!isset($listaProcesso[$processo])){
                        $msg[] = 'Processo Não Localizado';
                        $data['Status'] = 1;
                    }else{
                        $data['CodProcesso'] = $listaProcesso[$processo];
                    }

                    if(!isset($listaTipo[strtolower($encTipoDoc)])){
                        $msg[] = 'Tipo de Peça Não Localizada';
                        $data['Status'] = 1;
                    }else{
                        $data['CodTipoArquivo'] = $listaTipo[strtolower($encTipoDoc)];
                    }

                    $data['MsgErro'] = implode('; ',$msg);

                    try{
                        $service = new Files();
                        $service->exchangeArray($data);
                        $service->salvar();

                        echo 'Documento '.$finalFile.' Identificado'.PHP_EOL;
                    }catch (\Exception $e){
                        echo 'Documento '.$e->getMessage().PHP_EOL;
                    }
                }
            }
        }
    }

    public function uploadAction()
    {
        $service = new Files();

        $processo = new Processo();
        $tipo = new Tipo();

        $listaProcesso = $processo->getMatriz();
        $listaTipo = $tipo->getMatriz();

        /// Tratar os arquivos com erro
        $erros = $service->getErroFiles();
        $msg = [];
        foreach($erros as $erro){
            echo 'Processando: '.$erro->getName().PHP_EOL;
            if(preg_match('/@/', $erro->getCodProcesso())){
                $cod = str_replace('@','', $erro->getCodProcesso());
                if(isset($listaProcesso[$cod])){
                    $erro->setCodProcesso($listaProcesso[$cod]);
                    echo 'Processo '.$cod.' Atualizado'.PHP_EOL;
                }else{
                    echo 'Processo '.$cod.' Não Localizado'.PHP_EOL;
                    $msg[] = 'Processo Não Localizado';
                }
            }

            if(preg_match('/@/', $erro->getCodTipoArquivo())){
                $cod = str_replace('@','', $erro->getCodTipoArquivo());
                if(isset($listaTipo[$cod])){
                    $erro->setCodTipoArquivo($listaTipo[$cod]);
                    echo 'Tipo Peça '.$cod.' Atualizada'.PHP_EOL;
                }else{
                    echo 'Tipo de Peça '.$cod.' Não Localizado'.PHP_EOL;
                    $msg[] = 'Tipo de Peça Não Localizada';
                }
            }

            $erro->setMsgErro(implode('; ',$msg));
            if(!count($msg)){
                $erro->setStatus(0);
            }
            $erro->salvar();
        }

        /// Upload dos arquivos OK
        $archer = new Archer();
        $lista = $service->getNovos();
        foreach($lista as $item){
            $success = $archer->addPeca($item->getCodProcesso(), $item->getCodTipoArquivo(), $item->getCodAnexo());
            if(isset($success['Id'])){
                $item->setId($item->getId());
                $item->setStatus(2);
                $item->salvar();
                echo 'Registro '.$item->getName().' Sincronizado'.PHP_EOL;
            }
        }
    }

    public function xmlLocalAction(){
        $this->log('Rotina do XML Iniciada','xml_local');
        $this->log('Finalizada Leitura da Triagem','xml_local');
        $this->log('Iniciando Leitura Local','xml_local');

        $path = "./data/xml-reader";
        $diretorio = dir($path);

        $attachments = [];
        while($arquivo = $diretorio -> read()){
            if(in_array($arquivo,['.','..'])) continue;
            $attachments[] = $arquivo;
        }
        $diretorio -> close();


        if($attachments) {
            foreach($attachments as $att){
                @$xml   = simplexml_load_string(file_get_contents($path.'/'.$att), 'SimpleXMLElement', LIBXML_NOCDATA);
                $array = json_decode(json_encode((array)$xml), TRUE);

                if(isset($array['Publicacoes']['Publicacao'])){
                    $processo = $array['Publicacoes'];
                    $processo['Sincronizado'] = 'Não';
                    $processo['Arquivo'] = $att;

                    if(is_array($processo['DataPub'])){
                        $processo['DataPub'] = implode(',',$processo['DataPub']);
                    }

                    if(is_array($processo['DataDiv'])){
                        $processo['DataDiv'] = implode(',',$processo['DataDiv']);
                    }

                    $updates[] = $processo;
                }else{
                    if(!isset($array['Publicacoes'])) continue;
                    foreach($array['Publicacoes'] as $processo){
                        $processo['Sincronizado'] = 'Não';
                        $processo['Arquivo'] = $att;

                        if(is_array($processo['DataPub'])){
                            $processo['DataPub'] = implode(',',$processo['DataPub']);
                        }

                        if(is_array($processo['DataDiv'])){
                            $processo['DataDiv'] = implode(',',$processo['DataDiv']);
                        }

                        $updates[] = $processo;
                    }
                }
            }
        }

        $this->log('Busca finalizada foram identificadas '.count($updates).' publicações','xml_local');

        $this->log('Consolidando dados do filtro de automatico publicação','xml_local');
        $auto = new Autodelete();
        $lista = $auto->fetchAll();
        $tratadoDeleted = [];
        foreach($lista as $itemDeleted){
            $tratadoDeleted[] = $itemDeleted->getTexto();
        }

        $ignoradoMsg = 'Ignorado pelo conteúdo da publicação';
        foreach($updates as $update){
            if($update['Processo'] == '***') continue;
            foreach($tratadoDeleted as $deletedKey){
                if(preg_match('/'.$deletedKey.'/', $update['Publicacao'])) $update['Sincronizado'] = $ignoradoMsg;
            }

            $permitido = ['DataDiv','DataPub','Nome','Processo','Diario','Identificacao','Publicacao','Sincronizado','Arquivo'];

            $update['Processo'] = trim($update['Processo']);

            $tratado = [];
            foreach($permitido as $perm){
                if(!isset($update[$perm])){
                    if($perm == 'Diario'){
                        $valorFinal = $update['Tribunal'];
                    }elseif($perm == 'Identificacao'){
                        $valorFinal = '';
                    }
                }else{
                    $valorFinal = $update[$perm];
                }
                $tratado[$perm] = $valorFinal;
            }

            $update = $tratado;

            $this->log('Tratando publicação '.$update['Processo'],'xml');

            if($update['Sincronizado'] != $ignoradoMsg){
                $filtrar = new Processo();
                $filtrar->setNumeroCnj(trim($update['Processo']));
                $filtrar->setNomeAutor(trim($update['Nome']));
                $dados = $filtrar->getProcessoFiltro();

                $triar = true;

                if(count($dados) == 1){ /// Dados
                    $archer = new Archer();
                    $id = $archer->addPublicacao($dados[0]->CODIGO_SISTEMA, $update['Publicacao']);
                    $update['Sincronizado'] = 'Processo Localizado, Publicado no Artéria';
                    $this->log($update['Processo'].' Processo Localizado, Publicado no Artéria','xml');
                    $triar = false;
                }else{
                    $this->log($update['Processo'].' Processo não localizado, buscando na triagem','xml');

                    $triagem = $filtrar->getTriagem();

                    if($triagem){
                        if(count($triagem) == 1){
                            if($triagem[0]->DELETED == 1){
                                $this->log($update['Processo'].' Processo Localizado na triagem com status ignorado','xml');
                                $update['Sincronizado'] = 'Processo Ignorado na Triagem';
                                $triar = false;
                            }
                            if(isset($triagem[0]->CODIGO_SISTEMA)){
                                $archer = new Archer();
                                $archer->addPublicacao($triagem[0]->CODIGO_SISTEMA, $update['Publicacao']);
                                $update['Sincronizado'] = 'Localizado na Triagem, Publicado no Artéria';
                                $this->log($update['Processo'].' Processo Localizado na triagem, Publicado no Artéria','xml');
                                $triar = false;
                            }
                        }
                    }

                    if($triar){
                        $this->log($update['Processo'].' Processo não localizado, adicionando na triagem','xml');
                        $msg = (count($dados) == 0) ? 'Nenhum regristro com autor relacionado' : 'Mais de um possível processo com autor relacionamento';
                        $update['Sincronizado'] = $msg;
                        $json = json_encode($dados);
                        $relacionamento = new Relacionamento();
                        $relacionamento->setProcessoOrigem(trim($update['Processo']));
                        $relacionamento->setOpcoes($json);
                        $relacionamento->setDadosPublicacao(json_encode($update));
                        $relacionamento->setMotivo($msg);
                        $relacionamento->setStatus(0);
                        $relacionamento->salvar();
                    }
                }
            }

            $key = implode(';',array_keys($update)).PHP_EOL;
            $value = '"'.implode('"!@!"',array_values($update)).'"';
            $value = utf8_decode($value);
            $value = str_replace(";", ".", $value);
            $value = str_replace("!@!", ";", $value);
            $value = str_replace("\n", " ", $value).PHP_EOL;

            $file = './data/xml-report/'.date('Y-m-d').'.csv';
            if(file_exists($file)){
                file_put_contents($file,$value,FILE_APPEND);
            }else{
                file_put_contents($file,$key.$value);
            }

            $this->log($update['Processo'].' Finalizado, adicionando resultado no CSV','xml');
        }

        $this->log('Rotina do XML Finalizada','xml');
        die;
    }

    public function xmlReaderAction(){
        $this->log('Rotina do XML Iniciada','xml');
        if(file_exists('./data/update-processos.txt')){
            $this->log('Processo de atualização abortado, base de processos sendo atualizada;','xml');
            die;
        }

        $this->log('Lendo dados da Triagem','xml');

        /// Publica os editados
        $relacionamento = new Relacionamento();
        $lista = $relacionamento->getParaSincronizar();
        $this->log(count($lista).' Registro da Triagem Identificados','xml');
        foreach($lista as $item){
            if(!$item->getCodigoSistema()){
                $item->setProcessoDestino('NULL');
            }else{
                $publicacao = json_decode($item->getDadosPublicacao(), true);
                $archer = new Archer();
                if(!isset($publicacao['Publicacao'])) continue;
                $id = $archer->addPublicacao($item->getCodigoSistema(), $publicacao['Publicacao']);
                $item->setStatus(1);

                $this->log('Publicação para o Processo: '.$item->getProcessoOrigem().' Adicionada','xml');
            }

            $item->salvar();
        }

        $this->log('Finalizada Leitura da Triagem','xml');
        $this->log('Iniciando Leitura do Email','xml');

        /* Ler um e-mail e salvar o anexo */ //Conecta-se ao MailServer
         $host = "imap.gmail.com";
         $usuario = "leituraxml@costaesilvaadv.com.br";
         $senha = "leituraxml221100@";
         $inbox = imap_open("{".$host.":993/imap/ssl}INBOX", $usuario, $senha) or die("can't connect: " . imap_last_error());
         $emails = imap_search($inbox,'UNSEEN');
         //$emails = imap_search($inbox,'ALL');
         $updates = [];

        $this->log('Buscando informação de publicação nos emails','xml');

        if($emails) {



            /* begin output var */
            $output = '';

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach($emails as $email_number) {
                echo 'Lendo Email: '.$email_number.PHP_EOL;

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox,$email_number,0);
                $message = imap_fetchbody($inbox,$email_number,2);
                $structure = imap_fetchstructure($inbox,$email_number);

                $attachments = array();
                if(isset($structure->parts) && count($structure->parts)) {
                    for($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => '');

                        if($structure->parts[$i]->ifdparameters) {
                            foreach($structure->parts[$i]->dparameters as $object) {
                                if(strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }

                        if($structure->parts[$i]->ifparameters) {
                            foreach($structure->parts[$i]->parameters as $object) {
                                if(strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i+1);
                            if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            }
                            elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    } // for($i = 0; $i < count($structure->parts); $i++)
                } // if(isset($structure->parts) && count($structure->parts))

                foreach($attachments as $att){
                    if(!preg_match('/.xml/',$att['filename'])) continue;
                    @$xml   = simplexml_load_string($att['attachment'], 'SimpleXMLElement', LIBXML_NOCDATA);
                    $array = json_decode(json_encode((array)$xml), TRUE);

                    echo 'Publicacao Encontrada'.PHP_EOL;

                    if(isset($array['Publicacoes']['Publicacao'])){
                        $processo = $array['Publicacoes'];
                        $processo['Sincronizado'] = 'Não';
                        $processo['Arquivo'] = $att['filename'];

                        /*$explode = explode(' ',$processo['Processo']);
                        if(count($explode) > 1){
                            $processo['Processo'] = ' '.$explode[1];
                        }else{
                            $processo['Processo'] = ' '.$explode[0];
                        }*/

                        if(is_array($processo['DataPub'])){
                            $processo['DataPub'] = implode(',',$processo['DataPub']);
                        }

                        if(is_array($processo['DataDiv'])){
                            $processo['DataDiv'] = implode(',',$processo['DataDiv']);
                        }

                        $updates[] = $processo;
                    }else{
                        foreach($array['Publicacoes'] as $processo){
                            $processo['Sincronizado'] = 'Não';
                            $processo['Arquivo'] = $att['filename'];

                           /* $explode = explode(' ',$processo['Processo']);
                            $implode = implode('-',$explode);
                            $processo['Processo'] = $implode;*/

                            if(is_array($processo['DataPub'])){
                                $processo['DataPub'] = implode(',',$processo['DataPub']);
                            }

                            if(is_array($processo['DataDiv'])){
                                $processo['DataDiv'] = implode(',',$processo['DataDiv']);
                            }

                            $updates[] = $processo;
                        }
                    }
                }
            }
        }
        imap_close($inbox);

        $this->log('Busca finalizada foram identificadas '.count($updates).' publicações','xml');

        $this->log('Consolidando dados do filtro de automatico publicação','xml');
        $auto = new Autodelete();
        $lista = $auto->fetchAll();
        $tratadoDeleted = [];
        foreach($lista as $itemDeleted){
            $tratadoDeleted[] = $itemDeleted->getTexto();
        }

        /*foreach($updates as $up){
            if($up['Processo'] == '***') continue;
            file_put_contents('./data/validaProcesso.txt',str_pad($up['Processo'],30, ' ').' - Arquivo: '.$up['Arquivo'].PHP_EOL,FILE_APPEND);
        }
        echo 'Feito';
        die;*/

        $ignoradoMsg = 'Ignorado pelo conteúdo da publicação';
        foreach($updates as $update){
            if($update['Processo'] == '***') continue;
            foreach($tratadoDeleted as $deletedKey){
                if(preg_match('/'.$deletedKey.'/', $update['Publicacao'])) $update['Sincronizado'] = $ignoradoMsg;
            }

            $permitido = ['DataDiv','DataPub','Nome','Processo','Diario','Identificacao','Publicacao','Sincronizado','Arquivo'];
            /*foreach($update as $key => $valor){
                if(!in_array($key,$permitido)) unset($update[$key]);
            }*/

            $update['Processo'] = trim($update['Processo']);

            $tratado = [];
            foreach($permitido as $perm){
                if(!isset($update[$perm])){
                    if($perm == 'Diario'){
                        $valorFinal = $update['Tribunal'];
                    }elseif($perm == 'Identificacao'){
                        $valorFinal = '';
                    }
                }else{
                    $valorFinal = $update[$perm];
                }
                $tratado[$perm] = $valorFinal;
            }

            $update = $tratado;

            $this->log('Tratando publicação '.$update['Processo'],'xml');

            if($update['Sincronizado'] != $ignoradoMsg){
                $filtrar = new Processo();
                $filtrar->setNumeroCnj(trim($update['Processo']));
                $filtrar->setNomeAutor(trim($update['Nome']));
                $dados = $filtrar->getProcessoFiltro();

                $triar = true;

                if(count($dados) == 1){ /// Dados
                    $archer = new Archer();
                    $id = $archer->addPublicacao($dados[0]->CODIGO_SISTEMA, $update['Publicacao']);
                    $update['Sincronizado'] = 'Processo Localizado, Publicado no Artéria';
                    $this->log($update['Processo'].' Processo Localizado, Publicado no Artéria','xml');
                    $triar = false;
                }else{
                    $this->log($update['Processo'].' Processo não localizado, buscando na triagem','xml');

                    $triagem = $filtrar->getTriagem();

                    if($triagem){
                        if(count($triagem) == 1){
                            if($triagem[0]->DELETED == 1){
                                $this->log($update['Processo'].' Processo Localizado na triagem com status ignorado','xml');
                                $update['Sincronizado'] = 'Processo Ignorado na Triagem';
                                $triar = false;
                            }
                            if(isset($triagem[0]->CODIGO_SISTEMA)){
                                $archer = new Archer();
                                $archer->addPublicacao($triagem[0]->CODIGO_SISTEMA, $update['Publicacao']);
                                $update['Sincronizado'] = 'Localizado na Triagem, Publicado no Artéria';
                                $this->log($update['Processo'].' Processo Localizado na triagem, Publicado no Artéria','xml');
                                $triar = false;
                            }
                        }
                    }

                    if($triar){
                        $this->log($update['Processo'].' Processo não localizado, adicionando na triagem','xml');
                        $msg = (count($dados) == 0) ? 'Nenhum regristro com autor relacionado' : 'Mais de um possível processo com autor relacionamento';
                        $update['Sincronizado'] = $msg;
                        $json = json_encode($dados);
                        $relacionamento = new Relacionamento();
                        $relacionamento->setProcessoOrigem(trim($update['Processo']));
                        $relacionamento->setOpcoes($json);
                        $relacionamento->setDadosPublicacao(json_encode($update));
                        $relacionamento->setMotivo($msg);
                        $relacionamento->setStatus(0);
                        $relacionamento->salvar();
                    }
                }
            }

            $key = implode(';',array_keys($update)).PHP_EOL;
            $value = '"'.implode('"!@!"',array_values($update)).'"';
            $value = utf8_decode($value);
            $value = str_replace(";", ".", $value);
            $value = str_replace("!@!", ";", $value);
            $value = str_replace("\n", " ", $value).PHP_EOL;

            $file = './data/xml-report/'.date('Y-m-d').'.csv';
            if(file_exists($file)){
                file_put_contents($file,$value,FILE_APPEND);
            }else{
                file_put_contents($file,$key.$value);
            }

            $this->log($update['Processo'].' Finalizado, adicionando resultado no CSV','xml');
        }

        $this->log('Rotina do XML Finalizada','xml');
        die;
    }

    public function tokenAction()
    {
        $archer = new ArcherSoap();
        $report = $archer->getProcessos();
    }

    public function viewProcessosAction(){
        $archer = new ArcherSoap();
        $archer->getProcessos(true);
    }

    public function tratarArquivoAction(){
        $sql = 'SELECT PROCESSO_ORIGEM, DADOS_PUBLICACAO FROM ROBOT_RELACIONAMENTO ORDER BY PROCESSO_ORIGEM DESC';
        $tratado = [];
        foreach(Conexao::listarSql($sql) as $linha){
            $data = json_decode($linha->DADOS_PUBLICACAO, true);
            $tratado[$linha->PROCESSO_ORIGEM][] = $data['Arquivo'];
        }

        debug($tratado);
    }

    public function atualizarProcessosAction(){
        $this->log('Iniciando Rotina dos Processos','processos');
        file_put_contents('./data/update-processos.txt','1');
        $archer = new ArcherSoap();

        $this->log('Lendo Processos do Artéria','processos');
        $archer->updateProcessos();

        $archerService = new Archer();
        $processo = new Processo();

        $this->log('Buscando dados da Triagem Antiga','processos');
        $triagem = new Relacionamento();
        $lista = $triagem->buscarNaoTriados(false);
        $lista = array_merge($lista,$triagem->buscarAguardando());
        $deletar = [];
        foreach($lista as $item){
            $processo->setNumeroCnj($item->getProcessoOrigem());
            $dados = $processo->filtrarObjeto()->current();

            if($dados){
                echo 'Publicacao de Triagem Localizada: '.PHP_EOL;
                $pub = json_decode($item->getDadosPublicacao(),true);
                $archerService->addPublicacao($dados->getCodigoSistema(),$pub['Publicacao']);
                $deletar[] = $item->getId();
            }
        }

        $this->log(count($deletar).'Publicações Localizadas e Inseridas no Artéria','processos');

        if(count($deletar)){
            $triagem->deletarArray($deletar);
        }

        unlink('./data/update-processos.txt');
        $this->log('Finalizando Rotina do Processos','processos');
        die;
    }

    public function log($msg,$arquivo,$id=false,$clear=false){
        $file = './data/log/'.date('Y-m-d').'_'.$arquivo.'.txt';
        $msg = '['.date('H:i:s').'] '.$msg.PHP_EOL;
        echo $msg;
        file_put_contents($file,$msg,FILE_APPEND);

        if($clear) unlink('./data/log/'.$arquivo.'_'.$id.'.txt');
        if($id)file_put_contents('./data/log/'.$arquivo.'_'.$id.'.txt',$msg,FILE_APPEND);
    }

    public function loadRegras(){
        $file = fopen ('./data/carga_regra.txt',"r");
        $i=0;
        $tratado = [];
        $reg = [];
        while (!feof ($file)) {
            $linha = fgets($file,4096);
            $linha = str_replace(PHP_EOL,'',$linha);
            if(!$linha){
                $i++;
                $reg = [];
            }

            if(preg_match('/Escolha o Pagamento/',$linha)){
                $valor = str_replace('[Escolha o Pagamento]:','',$linha);
                $reg['EscolhaPagamento'] = trim($valor);;
            }
            elseif(preg_match('/Modelo de Pagamento/',$linha)){
                $valor = str_replace('[Modelo de Pagamento]:','',$linha);
                $reg['ModeloPagamento'] = trim($valor);;
            }
            elseif(preg_match('/Tipo de Pagamento/',$linha)){
                $valor = str_replace('[Tipo de Pagamento]:','',$linha);
                $reg['TipoPagamento'] = trim($valor);;
            }
            elseif(preg_match('/Referência/',$linha)){
                $valor = str_replace('[Referência]:','',$linha);
                $reg['Referencia'] = trim($valor);;
            }
            elseif(preg_match('/TIPO:/',$linha)){
                $valor = str_replace('TIPO:','',$linha);
                $reg['Tipo'] = trim($valor);
            }elseif(preg_match('/FASE:/',$linha)){
                $valor = str_replace('FASE:','',$linha);
                $reg['Fase'] = trim($valor);;
            }else{
                $reg['Regra'] = str_replace(['“','”'],'',trim($linha));
            }

            $tratado[$i] = $reg;
        }

        foreach($tratado as $regra){
            $objRegra = new Regras();
            $objRegra->exchangeArray($regra);
            $objRegra->salvar();
        }
        debug('Feito');
    }

    public function reportProcessadoAction()
    {
        $folder = 'C:' . DIRECTORY_SEPARATOR . 'Bruno' . DIRECTORY_SEPARATOR . 'ARQUIVOS PROCESSADOS' . DIRECTORY_SEPARATOR;
        $diretorio = dir($folder);

        $csv = [];
        $csv[] = ['processo','subsidio','pecas'];
        while ($processo = $diretorio->read()) {
            if (in_array($processo, ['.', '..'])) continue;
            if (is_dir($folder . DIRECTORY_SEPARATOR . $processo)) {
                $nDir = dir($folder . DIRECTORY_SEPARATOR . $processo);
                while ($subsidio = $nDir->read()) {
                    if (in_array($subsidio, ['.', '..'])) continue;
                    if (is_dir($folder . DIRECTORY_SEPARATOR . $processo . DIRECTORY_SEPARATOR . $subsidio)) {
                        $folderSubsidio = dir($folder . DIRECTORY_SEPARATOR . $processo . DIRECTORY_SEPARATOR . $subsidio);
                        $totalSub = 0;
                        while ($peca = $folderSubsidio->read()) {
                            $peca = htmlentities($peca, ENT_QUOTES, 'UTF-8');

                            if (in_array($peca, ['.', '..'])) continue;
                            $explode = explode('_', $peca);
                            if (!isset($explode[1])) continue;

                            $totalSub++;
                        }
                    }
                    $csv[] = [$processo,$subsidio,$totalSub];
                }
            }
        }

        $file = './data/report_subsidio.csv';
        foreach($csv as $item){
            file_put_contents($file,implode(';',$item).PHP_EOL,FILE_APPEND);
        }
        debug('Feito');
    }

    public function atualizarPagamentosAction($continuar=false){
        $erros = [];
        $erros[] = ['ID BAJ','PAGAMENTO','DESCRICAO'];
        $archer = new ArcherSoap();
        $this->log('Iniciando Rotina do Pagamento','rotina-pagamento');

        $this->log('Atualizando Pecas','rotina-pagamento');
        $pecas = $archer->getPecas();
        file_put_contents('./data/pecas.json',json_encode($pecas));

        $this->log('Atualizando Recibos','rotina-pagamento');
        $recibo = $archer->getRecibos();
        file_put_contents('./data/recibos.json',json_encode($recibo));

        $recibos = json_decode(file_get_contents('./data/recibos.json'), true);
        $pecas = json_decode(file_get_contents('./data/pecas.json'), true);

        $tPecas = [];
        $tipoPecas = [];
        foreach ($pecas as $peca){
            if(!isset($peca['IdProcesso']) || !isset($peca['TipoPeca'])) continue;
            $tPecas[$peca['IdProcesso']][] = $peca;
            $tipoPecas[$peca['IdProcesso']][] = $peca['TipoPeca'];
        }
        $pecas = $tPecas;

        $tRecibo = [];
        foreach ($recibos as $reci){
            $tRecibo[$reci['CodigoPagamento']][] = $reci;
        }
        $recibos = $tRecibo;

        unset($tRecibo);
        unset($tPecas);

        $this->log('Atualizando Pagamentos','rotina-pagamento');
        $pagamentos = $archer->getPagamentos();
       // debug($pagamentos);

        $regra = new TipoDocumento();
        $pag = new Pagamentos();
        $pag->deleteAll();
        foreach($pagamentos as $pagamento){
            echo 'Processando: '.$pagamento['Scpjud'].PHP_EOL;
            if(!isset($tipoPecas[$pagamento['IdProcesso']])){
                echo 'Pagamento Sem Peça '.$pagamento['NumeroCliente'].' - '.$pagamento['IdProcesso'].PHP_EOL;
                $erros[] = [$pagamento['NumeroCliente'],$pagamento['IdRegistro'],'Sem peça cadastrada'];
                $archerService = new Archer();
                $archerService->retornarPagamento($pagamento['IdRegistro'],'Peças Não encontradas');
                continue;
                die('Nao tem Peca');
            }

            $regra = new Regras();
            if(isset($pagamento['TipoDespesa'])){
                $regra->setTipoPagamento($pagamento['TipoDespesa']);
            }

            $regra->setFase($pagamento['Fase']);
            $regra->setEscolhaPagamento($pagamento['EscolhaPagamento']);
            $regra->setModeloPagamento($pagamento['ModeloPagamento']);
            @$regra->setReferencia($pagamento['Referencia']);
            $filtro = $regra->toArray();
            //debug($filtro);
            $load = $regra->filtrarObjeto()->current();
            //debug($load->getRegra());
            if(!$load){
                $erros[] = [$pagamento['NumeroCliente'],$pagamento['IdRegistro'],'Sem regra CEHAG'];
                echo 'Sem Regras CEHAG'.PHP_EOL;
                echo json_encode($filtro).PHP_EOL;
                $regra->setId('');
                $regra->exchangeArray($filtro);
                $regra->salvar();
                $errorRegra = true;
                continue;
            }
            $data = explode(';',$load->getRegra());
            if(count($data) != 4){
                $erros[] = [$pagamento['NumeroCliente'],$pagamento['IdRegistro'],'Erro na Regra: '.$load->getRegra().' ID '.$load->getId()];
                echo 'Erro na Regra '.$load->getId().PHP_EOL;
                $errorRegra = true;
                continue;
            }

            $obrigatoria = new TipoDocumento();
            $obrigatoria->setDespesa($data[0]);
            $obrigatoria->setTipoDespesa($data[1]);
            $obrigatoria->setCategoria($data[2]);
            $obrigatoria->setFase($data[3]);
            $dataFiltro = $obrigatoria->toArray();

            $docs = $obrigatoria->filtrarObjeto()->current();
            if(!$docs){
                echo 'Sem Regras DOCUMENTOS'.PHP_EOL;
                echo json_encode($dataFiltro).PHP_EOL;

                $erros[] = [$pagamento['NumeroCliente'],$pagamento['IdRegistro'],'Sem regra DOCUMENTOS '.json_encode($dataFiltro)];

                $obrigatoria->setId('');
                $obrigatoria->exchangeArray($dataFiltro);
                $obrigatoria->salvar();
                continue;
            }

            $pag = new Pagamentos();
            $pag->setIdProcesso($pagamento['IdProcesso']);
            $retornoRegra = $pag->validarRegra($docs->getListaDocumentos());
            if(count($retornoRegra)){
                echo 'Pagamento Sem Peças Obrigatórias: '.$pagamento['NumeroCliente'].' - '.$pagamento['IdProcesso'].implode(', ',$retornoRegra).PHP_EOL;
                $erros[] = [$pagamento['NumeroCliente'],$pagamento['IdRegistro'],'Sem peças obrigatórias: '.implode(', ',$retornoRegra)];
                $archerService = new Archer();
                $archerService->retornarPagamento($pagamento['IdRegistro'],'Peças Não encontradas: '.implode(', ',$retornoRegra));
                continue;
            }

           // $pecasProcesso = $tipoPecas[$pagamento['IdProcesso']];

            echo 'Pagamento Salvo'.PHP_EOL;
            $pag->exchangeArray($pagamento);
            $pag->setTipoRegistro('P');
            $pag->salvar();
        }

        file_put_contents('./data/erros_pagamento.csv',json_encode($erros));

        $this->log('Atualizando a Discriminacao','rotina-pagamento');
        $archer->updateDiscriminacaoValor();

        $this->log('Finalizando Rotina de Pagamentos','rotina-pagamento');
        if(!$continuar) die;
    }

    public function atualizarSubsidioAction($continuar=false){
        $this->log('Iniciando Rotina de Atualização dos Subsidios','subsidios-log');
        $soap = new ArcherSoap();
        $dados = $soap->getSubsidios();

        $old = json_decode(file_get_contents('./data/subsidio_full.txt'), true);
        if(!is_array($old)) $old = [];
        $novo = array_merge($old,$dados);

        $tratadoNovo = [];
        $temRegistro = [];
        foreach($novo as $item){
            if(isset($temRegistro[$item['Processo']][$item['IdEnvolvido']])) continue;
            $temRegistro[$item['Processo']][$item['IdEnvolvido']] = 1;
            $tratadoNovo[] = $item;
        }
        $novo = $tratadoNovo;

        file_put_contents('./data/subsidio_full.txt', json_encode($novo));

        $this->log(count($novo).' Subsidios Localizados, agrupando por Processo','subsidios-log');
        $tratado = [];
        $mapa = [];
        foreach($novo as $item){
            if(isset($mapa[$item['Processo']][$item['IdEnvolvido']])) continue;
            $mapa[$item['Processo']][$item['IdEnvolvido']] = 1;
            if(isset($item['Processo']) && isset($item['IdEnvolvido']) && isset($item['Autor'])){
                $tratado[$item['Processo']][] = $item;
            }
        }

        $this->log('Dados Organizado, Salvando Arquivo','subsidios-log');
        file_put_contents('./data/subsidios.json',json_encode($tratado));
        $this->log('Rotina dos subsidios finalizada','subsidios-log');
        if(!$continuar) die;
    }

    public function soapAction(){
        try{
            $soap = new SCPJud();
            //debug($soap->detalharProcesso('528 MS'));
            debug($soap->consutarProcesso('EM ANDAMENTO'));
        }catch (\Exception $e){
            debug($e->getMessage());
        }

    }

    public function buildZipRemessaAction(){
        ini_set('memory_limit',999999999999);
        $fileTime = './data/rodando_zip.txt';
        if(file_exists($fileTime)) die('Rotina em Execução');
        file_put_contents($fileTime,1);

        $this->log('Iniciando criacao dos zips','zip-remessa');
        $path = "./data/remessa_build";
        $diretorio = dir($path);

        $files = [];
        while($arquivo = $diretorio -> read()){
            if(in_array($arquivo,['.','..'])) continue;
            $files[] = $arquivo;
        }

        $this->log(count($files).' Solicitacoes encontradas','zip-remessa');

        $archer = new Archer();

        $diretorio -> close();
        foreach ($files as $file){
            $this->log('Trabalhando na Remessa '.$file,'zip-remessa',$file,true);
            $remessa = new Remessa();
            $remessa->setId($file);
            $remessa->load();

            if($remessa->getTipo() == 'R'){
                $this->processarReanalise($file,$remessa,$archer);
                continue;
            }elseif(in_array($remessa->getTipo(),['I','O'])){
                $this->processarManifestacao($file,$remessa,$archer);
                continue;
            }

            $pagamentos = $remessa->getPagamentos();
            $remessa->buildExcel('./data/excel-remessa/'.$remessa->getId().'.xlsx',$pagamentos);

            $regras = file_get_contents('./data/remessa_build/'.$file);
            if($regras){
                $regras = json_decode($regras, true);

                $tratadoPag = [];
                foreach($pagamentos as $pag){
                    if(!in_array($pag->getIdBaj(),array_keys($regras['regras']))) continue;
                    $tratadoPag[] = $pag;
                }
                $pagamentos = $tratadoPag;
            }

            $this->log(' Apagando Remessa antiga','zip-remessa',$file);
            @unlink('./public/remessas/'.$remessa->getId().'.zip');

            $arqPagamento = [];

            $this->log(' Abrindo arquivo ZIP','zip-remessa');
            $zip = new \ZipArchive();
            if ($zip->open('./public/remessas/'.$remessa->getId().'.zip', \ZipArchive::CREATE) === TRUE) {
                $this->log('Trabalhando nos Pagamentos','zip-remessa',$file);
                foreach($pagamentos as $pagamento){

                    $this->log('Processando o Pagamento: '.$pagamento->getIdRegistro(),'zip-remessa',$file);
                    $this->log('Criando Pasta do IDBAJ '.$pagamento->getIdBaj(),'zip-remessa',$file);
                    $folder = str_pad($pagamento->getIdBaj(),10,0,STR_PAD_LEFT);
                    $zip->addEmptyDir($folder);

                    if(!$regras || (isset($regras['regras'][$pagamento->getIdBaj()]['pagamentos']) && in_array($pagamento->getIdRegistro(), $regras['regras'][$pagamento->getIdBaj()]['pagamentos']))){
                        $recibo = $pagamento->getRecibos();

                        $contentRecibo = [];
                        foreach($recibo as $rec){
                            $peca = $archer->getPeca($rec['recibo']);
                            $contentRecibo[] = base64_decode($peca['AttachmentBytes']);
                        }

                        if(count($contentRecibo) == 0) continue;

                        $this->log(count($contentRecibo).' Recibos encontrados','zip-remessa',$file);
                        $this->log('Unindo os Recibos','zip-remessa',$file);

                        try{
                            $pdf = new PdfService();
                            $pdf->setFilesByContent($contentRecibo);
                            $pdf->concat();
                            $reciboFinal = './data/pdf_temp/'.rand(0,9999999).'_final.pdf';
                            $pdf->output($reciboFinal);
                        }catch (\Exception $e){
                            $pdf->clearTemp();

                            $this->log('Erro ao Juntar os PDFS','zip-remessa',$file);
                            $pdf = new PdfService();
                            $pdf->setFilesByContent($contentRecibo);
                            $reciboFinal = $pdf->byContent[0];
                        }
                        $contentRecibo = file_get_contents($reciboFinal);
                        $pdf->clearTemp();
                        @unlink($reciboFinal);

//                        debug($recibo);
                        $nameRecibo = $folder.$recibo[0]['autor'].$pagamento->getCodigoPedido().$pagamento->getData().$pagamento->getMes().'018';

                        if(!isset($arqPagamento[$pagamento->getIdBaj()][$nameRecibo])) $arqPagamento[$pagamento->getIdBaj()][$nameRecibo] = [];
                        $arqPagamento[$pagamento->getIdBaj()][$nameRecibo][] = 1;
                        $seq = str_pad(count($arqPagamento[$pagamento->getIdBaj()][$nameRecibo]),3,'0', STR_PAD_LEFT);
                        $nameRecibo .= $seq;
                        $zip->addFromString($folder.'/'.$nameRecibo.'.pdf',$contentRecibo);
                        $this->log('Adicionando o Recibo','zip-remessa',$file);
                    }else{
                        $this->log('Pagamento excluido do Reprotoloco','zip-remessa',$file);
                    }

                    if($remessa->getTipo() != 'E'){

                        $this->log('Adicionando CT','zip-remessa',$file);
                        $dataOficio = $remessa->toDate('Data','d/m/Y')->format('dmY');
                        $nameOficio = $folder.'0000000000'.'000'.$dataOficio.'000000'.'073'.'001';
                        if(!$remessa->getCt()){
                            $this->log('Sem CT para a REMESSA','zip-remessa',$file);
                            continue;
                        }
                        $content = file_get_contents($remessa->getCt()->getArquivo());
                        $zip->addFromString($folder.'/'.$nameOficio.'.pdf',$content);
                        $this->log('Oficio adicionado a pasta','zip-remessa',$file);
                    }else{
                        $this->log('Pulando CT, Remessa tipo Existente','zip-remessa',$file);
                    }

                    $arquivos = $pagamento->getFiles();

                    if($regras){
                        if(isset($regras['regras'][$pagamento->getIdBaj()]['pecas'])){
                            $filesThis = $regras['regras'][$pagamento->getIdBaj()]['pecas'];
                            $tratadoFiles = [];
                            foreach($arquivos as $arq){
                                if(in_array($arq['IdRastreamento'], $filesThis) && !isset($enviado[$pagamento->getIdBaj()][$arq['IdRastreamento']])){
                                    $tratadoFiles[] = $arq;
                                    $enviado[$pagamento->getIdBaj()][$arq['IdRastreamento']] = 1;
                                }
                            }
                            $arquivos = $tratadoFiles;
                        }else{
                            $arquivos = [];
                        }
                    }

                    $this->log(count($arquivos).' arquivos encontrados para o pagamento','zip-remessa',$file);
                    foreach($arquivos as $arquivo){
                        if(!$regras){
                            $pacote = new PacoteRemessa();
                            $pacote->setProcesso($pagamento->getNumeroProcesso());
                            $pacote->setDocumento($arquivo['IdRastreamento']);

                            $codigoPedigo = $pagamento->getCodigoPedido();
                            $valorTotal = (float) str_replace(',','.',$pagamento->getValor());
                            $all = ($codigoPedigo == '249' && $valorTotal > 199999.00) ? true : false;

                            if($pacote->filtrarObjeto()->current() && $all == false){
                                $this->log($arquivo['TipoPeca'].' Peça já enviada para o processo '.$pagamento->getNumeroProcesso(),'zip-remessa',$file);
                                continue;
                            }else{
                                $pacote->setTipo($arquivo['TipoPeca']);
                                $pacote->setName($arquivo['Peca']['nome']);
                                if($all == false){
                                    $pacote->salvar();
                                }
                            }
                        }

                        $peca = $archer->getPeca($arquivo['Peca']['id']);
                        if(!$peca){
                            continue;
                        }

                        $nameFile = $folder.$pagamento->generateNameFile($arquivo);
                        if(!isset($arqPagamento[$pagamento->getIdBaj()][$nameFile])) $arqPagamento[$pagamento->getIdBaj()][$nameFile] = [];
                        $arqPagamento[$pagamento->getIdBaj()][$nameFile][] = 1;
                        $seq = str_pad(count($arqPagamento[$pagamento->getIdBaj()][$nameFile]),3,'0', STR_PAD_LEFT);
                        $nameFile = $nameFile.$seq;

                        $content = base64_decode($peca['AttachmentBytes']);
                        $zip->addFromString($folder.'/'.$nameFile.'.pdf',$content);
                    }
                }
                $this->log('Criando planilha no zip','zip-remessa',$file);
                $zip->addFile('./data/excel-remessa/'.$remessa->getId().'.xlsx',$remessa->getPlanilhaName());
                $zip->close();

                unlink('./data/excel-remessa/'.$remessa->getId().'.xlsx');
            } else {
                echo 'failed to open zip';
            }

            $rem = new Remessa();
            $rem->setId($file);
            $rem->setDataZip(date('Y-m-d'));
            $rem->salvar();

            $this->log('Apagando solicitação de arquivo','zip-remessa');
            unlink('./data/remessa_build/'.$file);
        }
        $this->log('Rotina do zip finalizada', 'zip-remessa');
        unlink($fileTime);
        die;
    }

    public function processarReanalise($file,$remessa,$archer){
        $pagamentos = $remessa->getPagamentos();

        $regras = file_get_contents('./data/remessa_build/'.$file);
        if($regras){
            $regras = json_decode($regras, true);

            $tratadoPag = [];
            foreach($pagamentos as $pag){
                if(!in_array($pag->getIdBaj(),array_keys($regras['regras']))) continue;
                $tratadoPag[] = $pag;
            }
            $pagamentos = $tratadoPag;
        }

        $remessa->buildExcelReanalise('./data/excel-remessa/'.$remessa->getId().'.xlsx',$pagamentos);

        $this->log(' Apagando Remessa antiga','zip-remessa',$file);
        @unlink('./public/remessas/'.$remessa->getId().'.zip');

        $arqPagamento = [];

        $this->log(' Abrindo arquivo ZIP','zip-remessa');
        $zip = new \ZipArchive();
        if ($zip->open('./public/remessas/'.$remessa->getId().'.zip', \ZipArchive::CREATE) === TRUE) {
            $this->log('Trabalhando nos Pagamentos','zip-remessa',$file);
            foreach($pagamentos as $pagamento){

                $this->log('Processando o Pagamento: '.$pagamento->getIdRegistro(),'zip-remessa',$file);
                $this->log('Criando Pasta do IDBAJ '.$pagamento->getIdBaj(),'zip-remessa',$file);
                $folder = str_pad($pagamento->getIdBaj(),10,0,STR_PAD_LEFT);
                $zip->addEmptyDir($folder);

                if(!$regras || (isset($regras['regras'][$pagamento->getIdBaj()]['pagamentos']) && in_array($pagamento->getIdRegistro(), $regras['regras'][$pagamento->getIdBaj()]['pagamentos']))){
                    $recibo = $pagamento->getRecibos();

                    $contentRecibo = [];
                    foreach($recibo as $rec){
                        $peca = $archer->getPeca($rec);
                        $contentRecibo[] = base64_decode($peca['AttachmentBytes']);
                    }

                    $this->log(count($contentRecibo).' Recibos encontrados','zip-remessa',$file);
                    $this->log('Unindo os Recibos','zip-remessa',$file);

                    try{
                        $pdf = new PdfService();
                        $pdf->setFilesByContent($contentRecibo);
                        $pdf->concat();
                        $reciboFinal = './data/pdf_temp/'.rand(0,9999999).'_final.pdf';
                        $pdf->output($reciboFinal);
                    }catch (\Exception $e){
                        $pdf->clearTemp();

                        $this->log('Erro ao Juntar os PDFS','zip-remessa',$file);
                        $pdf = new PdfService();
                        $pdf->setFilesByContent($contentRecibo);
                        $reciboFinal = $pdf->byContent[0];
                    }
                    $contentRecibo = file_get_contents($reciboFinal);
                    $pdf->clearTemp();
                    @unlink($reciboFinal);

                    $nameRecibo = $folder.'0000000000'.$pagamento->getCodigoPedido().$pagamento->getData().$pagamento->getMes().'018';
                    if(!isset($arqPagamento[$pagamento->getIdBaj()][$nameRecibo])) $arqPagamento[$pagamento->getIdBaj()][$nameRecibo] = [];
                    $arqPagamento[$pagamento->getIdBaj()][$nameRecibo][] = 1;
                    $seq = str_pad(count($arqPagamento[$pagamento->getIdBaj()][$nameRecibo]),3,'0', STR_PAD_LEFT);
                    $nameRecibo .= $seq;
                    $zip->addFromString($folder.'/'.$nameRecibo.'.pdf',$contentRecibo);
                    $this->log('Adicionando o Recibo','zip-remessa',$file);
                }else{
                    $this->log('Pagamento excluido do Reprotoloco','zip-remessa',$file);
                }

                $this->log('Adicionando CT','zip-remessa',$file);
                $dataOficio = $remessa->toDate('Data','d/m/Y')->format('dmY');
                $nameOficio = $folder.'0000000000'.'000'.$dataOficio.'000000'.'161'.'001';
                $content = file_get_contents($remessa->getCt()->getArquivo());
                $zip->addFromString($folder.'/'.$nameOficio.'.pdf',$content);
                $this->log('Oficio adicionado a pasta','zip-remessa',$file);

                $arquivos = $pagamento->getFiles();

                if($regras){
                    if(isset($regras['regras'][$pagamento->getIdBaj()]['pecas'])){
                        $filesThis = $regras['regras'][$pagamento->getIdBaj()]['pecas'];
                        $tratadoFiles = [];
                        foreach($arquivos as $arq){
                            if(in_array($arq['IdRastreamento'], $filesThis) && !isset($enviado[$pagamento->getIdBaj()][$arq['IdRastreamento']])){
                                $tratadoFiles[] = $arq;
                                $enviado[$pagamento->getIdBaj()][$arq['IdRastreamento']] = 1;
                            }
                        }
                        $arquivos = $tratadoFiles;
                    }else{
                        $arquivos = [];
                    }
                }

                $this->log(count($arquivos).' arquivos encontrados para o pagamento','zip-remessa',$file);
                foreach($arquivos as $arquivo){
                    if(!$regras){
                        $pacote = new PacoteRemessa();
                        $pacote->setProcesso($pagamento->getNumeroProcesso());
                        $pacote->setDocumento($arquivo['IdRastreamento']);
                        if($pacote->filtrarObjeto()->current()){
                            $this->log($arquivo['TipoPeca'].' Peça já enviada para o processo '.$pagamento->getNumeroProcesso(),'zip-remessa',$file);
                            continue;
                        }else{
                            $pacote->setTipo($arquivo['TipoPeca']);
                            $pacote->setName($arquivo['Peca']['nome']);
                            $pacote->salvar();
                        }
                    }

                    $peca = $archer->getPeca($arquivo['Peca']['id']);
                    if(!$peca){
                        continue;
                    }

                    $nameFile = $folder.$pagamento->generateNameFile($arquivo);
                    if(!isset($arqPagamento[$pagamento->getIdBaj()][$nameFile])) $arqPagamento[$pagamento->getIdBaj()][$nameFile] = [];
                    $arqPagamento[$pagamento->getIdBaj()][$nameFile][] = 1;
                    $seq = str_pad(count($arqPagamento[$pagamento->getIdBaj()][$nameFile]),3,'0', STR_PAD_LEFT);
                    $nameFile = $nameFile.$seq;

                    $content = base64_decode($peca['AttachmentBytes']);
                    $zip->addFromString($folder.'/'.$nameFile.'.pdf',$content);
                }
            }
            $this->log('Criando planilha no zip','zip-remessa',$file);
            $zip->addFile('./data/excel-remessa/'.$remessa->getId().'.xlsx',$remessa->getPlanilhaName());
            $zip->close();

            unlink('./data/excel-remessa/'.$remessa->getId().'.xlsx');
        } else {
            echo 'failed to open zip';
        }

        $rem = new Remessa();
        $rem->setId($file);
        $rem->setDataZip(date('Y-m-d'));
        $rem->salvar();

        $this->log('Apagando solicitação de arquivo','zip-remessa');
        unlink('./data/remessa_build/'.$file);
    }

    public function atualizarReanaliseAction($continuar=false)
    {
        $soap = new ArcherSoap();
        $dados = $soap->getReanalise();

        $reanalise = new Reanalise();
        $reanalise->deleteAll();

        foreach ($dados as $item) {
            echo 'Item: '.PHP_EOL;
            echo json_encode($item).PHP_EOL;
            $pagamento = new Pagamentos();
            $pagamento->setIdRegistro($item['Pagamento']);

            if(!$pagamento->filtrarObjeto()->current()){
                echo 'Não achou pagamento'.PHP_EOL;
                continue;
            }

            $reanalise = new Reanalise();
            $reanalise->exchangeArray($item);

            echo 'Salvo'.PHP_EOL;
            $reanalise->salvar();
        }

        if(!$continuar) die('Atualizado');
    }

    public function updateCtsAction(){
        $this->log('Rotina de atualização do CT iniciada','update-ct');

        $archerApi = new Archer();
        $archer = new ArcherSoap();
        $cts = $archer->getCts();
        $this->log(count($cts).' Localizadas no Artéria','update-ct');

        $ctService = new PagamentoCt();
        $ctService->deleteAll();

        $tratado = [];
        foreach($cts as $ct){
            $this->log($ct['ct'].' Processando','update-ct');
            $this->log($ct['ct'].' Pegando conteudo da peça','update-ct');
            $peca = $archerApi->getPeca($ct['peca']);
            $name = './data/cts/'.$ct['peca'];

            $this->log($ct['ct'].' Salvando peça no disco','update-ct');
            file_put_contents($name,base64_decode($peca['AttachmentBytes']));

            $this->log($ct['ct'].' Atualizando Banco de Dados','update-ct');
            $ctService = new PagamentoCt();
            $ctService->exchangeArray([
                'Ct'=>$ct['ct'],
                'Arquivo'=> $name
            ]);
            $ctService->salvar();
            $this->log($ct['ct'].' Finalizado','update-ct');
        }

        $this->log(count($cts).' CTS Atualizadas','update-ct');
        die;
    }

    public function uploadSubsidioAction(){
        //$this->reportProcessadoAction();
        $this->relacionarSubsidio();
        header('Content-Type: text/html; charset =utf-8');
        $this->log('Upload dos subsidios iniciado','upload-subsidio');
        $folder = 'C:'.DIRECTORY_SEPARATOR.'Bruno'.DIRECTORY_SEPARATOR.'ARQUIVOS HABITACIONAL';
        $processadoFolder = 'C:'.DIRECTORY_SEPARATOR.'Bruno'.DIRECTORY_SEPARATOR.'ARQUIVOS PROCESSADOS'.DIRECTORY_SEPARATOR;
        $diretorio = dir($folder);

        $report = [
            ['arquivo','situacao','observacao']
        ];

        $archer = new Archer();
        $files = [];
        while($processo = $diretorio -> read()){
            if(in_array($processo,['.','..'])) continue;
            if(is_dir($folder.DIRECTORY_SEPARATOR.$processo)){
                $nDir = dir($folder.DIRECTORY_SEPARATOR.$processo);
                while($subsidio = $nDir->read()){
                    if(in_array($subsidio,['.','..'])) continue;
                    if(is_dir($folder.DIRECTORY_SEPARATOR.$processo.DIRECTORY_SEPARATOR.$subsidio)){
                        $folderSubsidio = dir($folder.DIRECTORY_SEPARATOR.$processo.DIRECTORY_SEPARATOR.$subsidio);
                        while($peca = $folderSubsidio->read()){
                            $erro = false;
                            if(in_array($peca,['.','..'])) continue;

                            $fileTest = $folderSubsidio->path.DIRECTORY_SEPARATOR.$peca;

                            $explode = explode('_',$peca);
                            if(!isset($explode[1])) continue;

                            $fullFile = $processo.DIRECTORY_SEPARATOR.$subsidio.DIRECTORY_SEPARATOR.$peca;

                            $this->log('Lendo Arquivo: '.$fullFile,'upload-subsidio');

                            $tipo = $explode[0];
                            $data = str_replace('-','/',$explode[1]);

                            unset($explode[0]);
                            unset($explode[1]);

                            $arquivo = implode($explode);

                            $rel = new Relacionamento();
                            if($rel->alterarProcesso($processo)){
                                $processo = $rel->alterarProcesso($processo);
                            }

                            $processoService = new Processo();
                            $processoService->setNumeroCnj($processo);
                            $processoService = $processoService->filtrarObjeto()->current();

                            if(!$processoService){
                                $this->log('Processo não localizado: '.$processo,'upload-subsidio');
                                $report[] = [$fullFile,'Processo não localizado',$processo];
                                if(!file_exists($processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio)) mkdir($processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio,0777,true);
                                rename($fileTest, $processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio.DIRECTORY_SEPARATOR.$peca);
                                continue;
                            }
                            $idProcesso = $processoService->getCodigoSistema();

                            $tipoService = new Tipo();
                            $tipoService->setChave($tipo);
                            $tipoService = $tipoService->getByCode();
                            if(!$tipoService){
                                $this->log('Tipo de peça não localizado: '.$tipo,'upload-subsidio');
                                $report[] = [$fullFile,'Tipo de Peça não localizada',$tipo];
                                if(!file_exists($processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio)) mkdir($processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio,0777,true);
                                rename($fileTest, $processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio.DIRECTORY_SEPARATOR.$peca);
                                continue;
                            }
                            $idTipoDocumento = $tipoService->getValor();


                            $this->log('Arquivo Colocado na fila de upload: '.$fullFile,'upload-subsidio');
                            $file = [
                                'tipo'=>$idTipoDocumento,
                                'subsidio'=>$subsidio,
                                'processo'=>$idProcesso,
                                'data'=>$data,
                                'peca'=>$arquivo,
                                'local'=>$fileTest,
                                'fullFile'=>$fullFile,
                                'pastas'=>$processo.DIRECTORY_SEPARATOR.$subsidio
                            ];

                            try{
                                $anexo = $archer->addFile($file['peca'],$file['local']);

                                if(!$anexo){
                                    $report[] = [$fullFile,'Não Foi Possível fazer upload do arquivo',''];
                                    if(!file_exists($processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio)) mkdir($processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio,0777,true);
                                    rename($fileTest, $processadoFolder.$processo.DIRECTORY_SEPARATOR.$subsidio.DIRECTORY_SEPARATOR.$peca);
                                    continue;
                                }

                                $uploadService = new UploadSubsidio();
                                $uploadService->setTipo($file['tipo']);
                                $uploadService->setSubsidio($file['subsidio']);
                                $uploadService->setProcesso($file['processo']);
                                $uploadService->setData($file['data']);
                                $uploadService->setCaminho($file['local']);
                                $uploadService->setName($file['peca']);
                                $uploadService->setFileId($anexo);
                                $uploadService->salvar();

                                $this->log('Arquivo Salvo: '.$anexo,'upload-subsidio');

                            }catch (\Exception $e){
                                $this->log('Erro Upload: Erro Geral','upload-subsidio');
                                $report[] = [$file['fullFile'],'Erro no UPLOAD','Erro geral'];
                            }
                        }
                    }
                }
            }

        }

        //$this->log(count($files).' Arquivos prontos para upload','upload-subsidio');

        $this->log('Salvando Log de Processamento','upload-subsidio');
        $csv = '';
        foreach($report as $item){
            $csv .= implode(';',$item).PHP_EOL;
        }
        file_put_contents('./data/upload_subsidio_report.csv',$csv);
        $this->log('Upload subsidios finalizado','upload-subsidio');
    }

    public function relacionarSubsidio(){
        $this->log('Inicio do Relacionamento','relacionar-subsidio');
        $up = new UploadSubsidio();
        $lista = $up->filtrarObjeto();

        $this->log(count($lista).' Registros para Relacionar','relacionar-subsidio');

        $archer = new Archer();

        $processado = 0;
        foreach($lista as $item){
            $processado++;
            /*if($processado <= 4173){
                echo 'Total: '.$processado.' - Passando'.PHP_EOL;
                continue;
            }*/
            $status = $archer->addSubsidio($item->getSubsidio(),$item->getTipo(),$item->getFileId(),$item->getProcesso(),$item->getData());
            if(!isset($status['Id'])){
                $this->log('Erro Upload: Erro ano relacionar com subsidio: '.$item->getFileId(),'relacionar-subsidio');
            }else{
                $item->excluir(true);
                $this->log('Upload Efetuado','relacionar-subsidio');
            }
            echo 'Total: '.$processado.' - '.$item->getSubsidio().PHP_EOL;
        }
        die('Feito');
    }

    public function manifestacaoAction($continuar=false){
        $archer = new ArcherSoap();
        $lista = $archer->getManifestacaoInteresse();
        $manifestacao = new Manifestacao();
        $manifestacao->deleteAll();

        $tratado = [];
        foreach($lista as $item){
            if(!isset($item['IdBaj'])) continue;
            if(!isset($item['IdProcesso'])) continue;
            $tratado[$item['IdBaj']][] = $item;
            $processos[$item['IdBaj']] = $item;
        }

        file_put_contents('./data/manifestacoes.json',json_encode($tratado));

        foreach($processos as $item){
            $item['DataPublicacao'] = end($item['RecordPub'])['DataPub'];
            unset($item['RecordPub']);

            $manifestacao->setId('');
            $manifestacao->exchangeArray($item);
            $manifestacao->salvar();
        }

        echo 'Feito';
        if(!$continuar) die;
    }

    private function processarManifestacao($file, $remessa, $archer)
    {
        $pagamentos = $remessa->getPagamentos();
        $remessa->buildExcelManifestacao('./data/excel-remessa/'.$remessa->getId().'.xlsx',$pagamentos);

        $regras = file_get_contents('./data/remessa_build/'.$file);
        if($regras){
            $regras = json_decode($regras, true);

            $tratadoPag = [];
            foreach($pagamentos as $pag){
                if(!in_array($pag->getIdBaj(),array_keys($regras['regras']))) continue;
                $tratadoPag[] = $pag;
            }
            $pagamentos = $tratadoPag;
        }

        $this->log(' Apagando Remessa antiga','zip-remessa',$file);
        @unlink('./public/remessas/'.$remessa->getId().'.zip');

        $arqPagamento = [];

        $this->log(' Abrindo arquivo ZIP','zip-remessa');
        $zip = new \ZipArchive();
        if ($zip->open('./public/remessas/'.$remessa->getId().'.zip', \ZipArchive::CREATE) === TRUE) {
            $this->log('Trabalhando nos Pagamentos','zip-remessa',$file);
            foreach($pagamentos as $pagamento){
                $this->log('Criando Pasta do IDBAJ '.$pagamento->getIdBaj(),'zip-remessa',$file);
                $folder = str_pad($pagamento->getIdBaj(),10,0,STR_PAD_LEFT);
                $zip->addEmptyDir($folder);

                $this->log('Adicionando CT','zip-remessa',$file);
                $dataOficio = $remessa->toDate('Data','d/m/Y')->format('dmY');
                $nameOficio = $folder.'0000000000'.'000'.$dataOficio.'000000'.'073'.'001';
                $content = file_get_contents($remessa->getCt()->getArquivo());
                $zip->addFromString($folder.'/'.$nameOficio.'.pdf',$content);
                $this->log('Oficio adicionado a pasta','zip-remessa',$file);

                $arquivos = $pagamento->getFiles();
                $perm = ['097','016','060','042','008','009','156','119'];
                $tratadoArq = [];
                foreach($arquivos as $arq){
                    $ext = substr($arq['TipoPeca'],0,3);
                    if(in_array($ext,$perm)) $tratadoArq[] = $arq;
                }
                $arquivos = $tratadoArq;

                if($regras){
                    if(isset($regras['regras'][$pagamento->getIdBaj()]['pecas'])){
                        $filesThis = $regras['regras'][$pagamento->getIdBaj()]['pecas'];
                        $tratadoFiles = [];
                        foreach($arquivos as $arq){
                            if(in_array($arq['IdRastreamento'], $filesThis) && !isset($enviado[$pagamento->getIdBaj()][$arq['IdRastreamento']])){
                                $tratadoFiles[] = $arq;
                                $enviado[$pagamento->getIdBaj()][$arq['IdRastreamento']] = 1;
                            }
                        }
                        $arquivos = $tratadoFiles;
                    }else{
                        $arquivos = [];
                    }
                }

                $this->log(count($arquivos).' arquivos encontrados para o pagamento','zip-remessa',$file);
                foreach($arquivos as $arquivo){
                    if(!$regras){
                        $pacote = new PacoteRemessa();
                        $pacote->setProcesso($pagamento->offsetData['NUMERO_CNJ']);
                        $pacote->setDocumento($arquivo['IdRastreamento']);
                        if($pacote->filtrarObjeto()->current()){
                            $this->log($arquivo['TipoPeca'].' Peça já enviada para o processo '.$pagamento->getNumeroProcesso(),'zip-remessa',$file);
                            continue;
                        }else{
                            $pacote->setTipo($arquivo['TipoPeca']);
                            $pacote->setName($arquivo['Peca']['nome']);
                            $pacote->salvar();
                        }
                    }

                    $peca = $archer->getPeca($arquivo['Peca']['id']);
                    if(!$peca){
                        continue;
                    }

                    $nameFile = $folder.$pagamento->generateNameFile($arquivo);
                    if(!isset($arqPagamento[$pagamento->getIdBaj()][$nameFile])) $arqPagamento[$pagamento->getIdBaj()][$nameFile] = [];
                    $arqPagamento[$pagamento->getIdBaj()][$nameFile][] = 1;
                    $seq = str_pad(count($arqPagamento[$pagamento->getIdBaj()][$nameFile]),3,'0', STR_PAD_LEFT);
                    $nameFile = $nameFile.$seq;

                    $content = base64_decode($peca['AttachmentBytes']);
                    $zip->addFromString($folder.'/'.$nameFile.'.pdf',$content);
                }
            }
            $this->log('Criando planilha no zip','zip-remessa',$file);
            $zip->addFile('./data/excel-remessa/'.$remessa->getId().'.xlsx',$remessa->getPlanilhaName());
            $zip->close();

            unlink('./data/excel-remessa/'.$remessa->getId().'.xlsx');
        } else {
            echo 'failed to open zip';
        }

        $rem = new Remessa();
        $rem->setId($file);
        $rem->setDataZip(date('Y-m-d'));
        $rem->salvar();

        $this->log('Apagando solicitação de arquivo','zip-remessa');
        unlink('./data/remessa_build/'.$file);
    }

    public function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }

    public function inserirScpjudAction(){
        //$this->log('Iniciado processo INSERIR SCPJUD','inserir-scpjud');
        $archerSoap = new ArcherSoap();
        $dados = $archerSoap->getProcessosScpJud();
        $this->log(count($dados).' Processos a serem inseridos no SCPJUD', 'inserir-scpjud');

        foreach($dados as $item){
            $valoracao = explode(';',$item['Valoracao']);
            $item['desTipoRisco'] = $valoracao[0];
            $item['numSinistro'] = $valoracao[1];
            $item['codDesRisco'] = $valoracao[2];
            $item['seqRamo'] = $valoracao[3];
            $item['seqProduto'] = $valoracao[4];
            $item['vlrRisco'] = $valoracao[5];
            $item['pctPerda'] = $valoracao[6];
            $item['desNumApolice'] = $valoracao[7];
            $item['indAvisoJudicial'] = $valoracao[8];
            $item['codCota'] = $valoracao[9];
            $item['codGrupo'] = $valoracao[10];
            $item['CodPln'] = $valoracao[11];
            $item['UfRisco'] = $valoracao[12];
            $item['TipoProcesso'] = 'CÍVEL';

            $codPedido  = explode(' - ', $item['CodigoPedidoPrincipal']);
            $item['CodigoPedidoCod'] = $codPedido[0];
            $item['CodPedidoText'] = $codPedido[1];

            $tratadoEnvolvido = [];
            foreach($item['Envolvidos'] as $envolvidos){
                $tratadoEnvolvido[$envolvidos['Nome']] = $envolvidos;
            }

            @$item['CodigoAdvAutor'] = $tratadoEnvolvido[$item['AdvogadoAutor']]['CodigoXml'];
            $item['CodigoAdvAutor'] = 11111;
            @$item['CodigoAdvReu'] = $tratadoEnvolvido[$item['AdvogadoReu']]['CodigoXml'];
            @$item['tratar']['CodPeritoAssistenteAutor'] = $tratadoEnvolvido[$item['AssistenteAutor']]['CodigoXml'];
            @$item['tratar']['CodPeritoAssistenteReu'] = $tratadoEnvolvido[$item['AssistenteReu']]['CodigoXml'];
            @$item['tratar']['CodJuiz'] = $tratadoEnvolvido[$item['Juiz']]['CodigoXml'];
            @$item['tratar']['CodPeritoJudicial'] = $tratadoEnvolvido[$item['PeritoJudicial']]['CodigoXml'];

            $null = [
                'CodPeritoAssistenteAutor'=>'codAssistenteParteContraria',
                'CodPeritoAssistenteReu'=>'codAssistenteParteSeguradora',
                'CodJuiz'=>'codJuiz',
                'CodPeritoJudicial'=>'codPeritoJudicial'
                ];

            foreach($item['tratar'] as $key => $valor){
                if(!$valor){
                    $item[$key] = '';
                }else{
                    $item[$key] = '<c:'.$null[$key].'>'.$valor.'</c:'.$null[$key].'>';
                }
            }

            $principal = $tratadoEnvolvido[$item['AutorPrincipal']];

            $xmlAutor = '<d:Autor>
                                <eTag i:nil="true" xmlns="http://jw.caixaseguradora.com.br/BaseModel/v3"/>
                                <d:cpfcnpj>'.str_replace(['-','.','/'],'',$principal['Cpf']).'</d:cpfcnpj>
                                <d:indAutorPrincipal>S</d:indAutorPrincipal>
                                <d:nomAutor>'.$principal['Nome'].'</d:nomAutor>
                            </d:Autor>'.PHP_EOL;

            foreach(explode(';',$item['DemaisAutores']) as $demais){
                $envolvido = $tratadoEnvolvido[$demais];
                if(!isset($envolvido['Cpf'])) continue;
                $xmlAutor .= '<d:Autor>
                                <eTag i:nil="true" xmlns="http://jw.caixaseguradora.com.br/BaseModel/v3"/>
                                <d:cpfcnpj>'.str_replace(['-','.','/'],'',$envolvido['Cpf']).'</d:cpfcnpj>
                                <d:indAutorPrincipal>N</d:indAutorPrincipal>
                                <d:nomAutor>'.$envolvido['Nome'].'</d:nomAutor>
                            </d:Autor>'.PHP_EOL;
            }

            $xmlReu = '';
            foreach(explode(';',$item['Reu']) as $reu){
                $reu = $tratadoEnvolvido[$reu];
                if(!isset($reu['Cpf'])) continue;
                $principal = (!$xmlReu) ? 'S' : 'N';
                $xmlReu .= '<d:Reu>
                                <eTag i:nil="true" xmlns="http://jw.caixaseguradora.com.br/BaseModel/v3"/>
                                <d:cpfcnpj>'.str_replace(['-','.','/'],'',$reu['Cpf']).'</d:cpfcnpj>
                                <d:indReuPrincipal>'.$principal.'</d:indReuPrincipal>
                                <d:nomReu>'.$reu['Nome'].'</d:nomReu>
                            </d:Reu>'.PHP_EOL;
            }

            $item['XmlAutor'] = $xmlAutor;
            $item['XmlReu'] = $xmlReu;

            $item['CodProcedimento'] = 2; /// Aguardando a inserção no XML;
            $item['NumeroClienteProcessoOrigem'] = (isset($item['NumeroClienteProcessoOrigem'])) ?
                '<b:codProcessoSCPJUDOrigem>[NumeroClienteProcessoOrigem]</b:codProcessoSCPJUDOrigem>' :
                '<b:codProcessoSCPJUDOrigem i:nil="true"/>';

            $scpjud = new SCPJud();
            $dados = $scpjud->inserirProcesso($item);
            $codigoScpjud = $dados['inserirProcessoResponse']['dadosRetorno']['a:objProcesso']['b:codProcessoSCPJUD'];

            $archer = new Archer();
            $archer->updateScpjud($item['IdSistema'],$codigoScpjud);
            echo 'Processado: '.$codigoScpjud.PHP_EOL;
            debug('Feito');

        }

    }

    public function rotinaReescritaAction(){
        $this->atualizarPagamentosAction(true);
        $this->atualizarReanaliseAction(true);
        $this->manifestacaoAction(true);
        //$this->buildZipRemessaAction();
        die;
    }

    public function atualizarTokenAction(){
        echo 'Atualizando Token'.PHP_EOL;
        $archer = new Archer();
        $archer->autenticar(true);
        echo 'Token Atualizado'.PHP_EOL;
        die();
    }

    public function atualizarRelacionamentoAction(){
        $file = fopen('./data/nao_triados_tratado.csv','r');
        $data = [];
        $header = false;
        $erro  = [];
        while($csv = fgetcsv($file,false,';')){
            if(!$header){
                $header = $csv;
                continue;
            }
            $registro = Conexao::lerSql("SELECT TRIM(NUMERO_CNJ) AS ProcessoDestino, CODIGO_SISTEMA  FROM ROBOT_PROCESSO WHERE NUMERO_CLIENTE = '".$csv[1]."'");
            if($registro){
                $sql = 'UPDATE ROBOT_RELACIONAMENTO SET ID_ANALISE = "513de1c6-2faa-4d2c-9474-0fb2a469ca2e", PROCESSO_DESTINO = "'.$registro->ProcessoDestino.'", CODIGO_SISTEMA = "'.$registro->CODIGO_SISTEMA.'" WHERE PROCESSO_ORIGEM like "%'.$csv[0].'%"';
                Conexao::execSql($sql);
                $processado[] = $registro;
                echo 'Executado'.PHP_EOL;
            }else{
                $erro[] = $csv;
                echo 'Nao Executado'.PHP_EOL;
            }
        }
        file_put_contents('./public/erro_relacinamento.json',json_encode($erro));
        debug($erro);
    }

    public function genericoAction(){
        $processo = new Processo();
        $triagem = new Relacionamento();
        $mapa = [];
        foreach($processo->filtrarObjeto() as $pro){
            $mapa[trim($pro->getNumeroCliente())] = ['id'=>$pro->getCodigoSistema(),'processo'=>$pro->getNumeroCnj()];
        };
        $csv = file_get_contents('./data/dados_triagem.csv');
        $dados = $this->csvstring_to_array($csv,';');
        $header = [];
        $tratado = [];
        foreach($dados as $item){
            if(!$header){
                $header = $item;
                continue;
            }

            $reg = [];
            foreach($item as $key => $value){
                $reg[$header[$key]] = $value;
            }
            $tratado[] = $reg;
        }

        $ignorar = [];
        $triar = [];
        $aguardando = [];
        foreach($tratado as $item){
            if($item['IGNORAR'] == 'x'){
                $ignorar[] = $item['Processo'];
//                $sql = 'UPDATE ROBOT_RELACIONAMENTO SET DELETED = 1 WHERE PROCESSO_ORIGEM = "'.$item['Processo'].'"';
//                Conexao::execSql($sql);
                echo $item['Processo'].' IGNORADO'.PHP_EOL;
                continue;
            }
            if($item['SCPJUD'] != ''){
                if(!isset($mapa[trim($item['SCPJUD'])])){
                    file_put_contents('./public/rotina_erro.txt',$item['SCPJUD'].PHP_EOL,FILE_APPEND);
                    continue;
                }
                $destino = $mapa[trim($item['SCPJUD'])];
                $sql = 'UPDATE ROBOT_RELACIONAMENTO SET PROCESSO_DESTINO = "'.$destino['processo'].'", CODIGO_SISTEMA = "'.$destino['id'].'" WHERE PROCESSO_ORIGEM = "'.$item['Processo'].'"';
                Conexao::execSql($sql);
                echo $item['Processo'].' TRIADO'.PHP_EOL;
                continue;
            }
            if($item['AGUAR. CAD'] == 'x'){
                $aguardando[] = $item['Processo'];
                //$sql = 'UPDATE ROBOT_RELACIONAMENTO SET DELETED = 2 WHERE PROCESSO_ORIGEM = "'.$item['Processo'].'"';
                //Conexao::execSql($sql);
                echo $item['Processo'].' AGUARDANDO'.PHP_EOL;
                continue;
            }
        }

        $registros = '"'.implode('","',$ignorar).'"';
        $sql = 'UPDATE ROBOT_RELACIONAMENTO SET DELETED = 1 WHERE PROCESSO_ORIGEM in ('.$registros.')';
        Conexao::execSql($sql);
        echo 'Ignorados'.PHP_EOL;

        $registros = '"'.implode('","',$aguardando).'"';
        $sql = 'UPDATE ROBOT_RELACIONAMENTO SET DELETED = 2 WHERE PROCESSO_ORIGEM in ('.$registros.')';
        Conexao::execSql($sql);
        echo 'Aguardando'.PHP_EOL;


        die('Rotina Finalizada');
    }

    function csvstring_to_array($string, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n") {
        // @author: Klemen Nagode
        $array = array();
        $size = strlen($string);
        $columnIndex = 0;
        $rowIndex = 0;
        $fieldValue="";
        $isEnclosured = false;
        for($i=0; $i<$size;$i++) {

            $char = $string{$i};
            $addChar = "";

            if($isEnclosured) {
                if($char==$enclosureChar) {

                    if($i+1<$size && $string{$i+1}==$enclosureChar){
                        // escaped char
                        $addChar=$char;
                        $i++; // dont check next char
                    }else{
                        $isEnclosured = false;
                    }
                }else {
                    $addChar=$char;
                }
            }else {
                if($char==$enclosureChar) {
                    $isEnclosured = true;
                }else {

                    if($char==$separatorChar) {

                        $array[$rowIndex][$columnIndex] = $fieldValue;
                        $fieldValue="";

                        $columnIndex++;
                    }elseif($char==$newlineChar) {
                        echo $char;
                        $array[$rowIndex][$columnIndex] = $fieldValue;
                        $fieldValue="";
                        $columnIndex=0;
                        $rowIndex++;
                    }else {
                        $addChar=$char;
                    }
                }
            }
            if($addChar!=""){
                $fieldValue.=$addChar;

            }
        }

        if($fieldValue) { // save last field
            $array[$rowIndex][$columnIndex] = $fieldValue;
        }
        return $array;
    }
}
