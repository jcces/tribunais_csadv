<?php

namespace Manager\Controller;

use Estrutura\Controller\AbstractEstruturaController;
use Manager\Service\Attach;

class DownloadController extends AbstractEstruturaController
{
    public function attachAction()
    {
        $file = $this->params('id');
        return $this->downloadIt('./data/upload/'.$file);
    }
}
