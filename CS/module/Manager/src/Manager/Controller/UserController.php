<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manager\Controller;

use Estrutura\Controller\AbstractCrudController;
use Estrutura\Service\Email;
use Manager\Service\PasswordRecovery;
use Manager\Service\User;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class UserController extends AbstractCrudController
{

    protected $service;
    protected $form;

    public function __construct(){
        parent::init();
    }

    public function indexAction()
    {
        return parent::index($this->service, $this->form);
    }

    public function cadastroAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function alterarSenhaAction()
    {
        return parent::cadastro($this->service, $this->form);
    }

    public function gravarAction()
    {
        try{
            $post = $this->getRequest()->getPost();
            if(empty($post['Id'])) {
                $validate = new User();
                $validate->validateUser($post['Login']);
                $post = $validate->validatePass($post);
            };

            return parent::gravar($this->service, $this->form, $post);
        }catch(\Exception $e){
            $this->setPost($post);
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/user/cadastro');
        }
    }

    public function gravarSenhaAction()
    {
        try{
            $controller = str_replace("manager-", "", $this->params('controller'));
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos.');
            }

            $post = $request->getPost()->toArray();
            $validate = new User();
            $post = $validate->validateUserPass($post);
            $post = $validate->validatePass($post);

            $service = new User();
            $service->exchangeArray($post);
            $service->salvar();

            $this->addSuccessMessage('Senha alterada com sucesso!');
            $this->redirect()->toRoute('navegacao',array('controller'=>$controller,'action'=>'alterar-senha'));
            return false;
        }catch (\Exception $e){
            $this->setPost($post);
            $this->addErrorMessage($e->getMessage());
            $this->redirect()->toRoute('navegacao',array('controller'=>$controller,'action'=>'alterar-senha'));
            return false;
        }
    }

    public function excluirAction()
    {
        return parent::excluir($this->service, $this->form);
    }

    public function entrarAction()
    {
        $this->layout('layout-login');
        return parent::cadastro($this->service, $this->form);
    }

    public function autenticarAction()
    {
        $this->layout('layout-login');

        try{
            $controller = str_replace("manager-", "", $this->params('controller'));
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos.');
            }

            $post = $request->getPost()->toArray();

            $validate = new User();
            $validate->autenticar($post);
            unset($post['Password']);

            $sessao = new Container('LoginRedirect');
            $url = ($sessao->offsetExists('url')) ? $sessao->offsetGet('url') : '/';
            $sessao->offsetUnset('url');

            return $this->redirect()->toUrl($url);
        }catch (\Exception $e){
            $this->setPost($post);
            $this->addErrorMessage($e->getMessage());
            return $this->redirect()->toUrl('/user/entrar');
        }
    }

    public function sairAction()
    {
        $sessao = new Container('autenticacao');
        $sessao->autenticacao = '';

        $this->addSuccessMessage('Usuário Desconectado');

        return $this->redirect()->toUrl('/');
    }

    public function esqueceuASenhaAction()
    {
        $this->layout('layout-login');
        return new ViewModel();
    }

    public function enviarNovaSenhaAction()
    {
        try{
            $controller = str_replace("manager-", "", $this->params('controller'));
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos.');
            }

            $post = $request->getPost()->toArray();

            $user = new User();
            $user->setLogin($post['Login']);
            $dados = $user->filtrarObjeto()->current();

            if(!$dados) throw new \Exception('Login não encontrado!');
            if(!$dados->getEmail()) throw new \Exception('Você precisa de um e-mail cadastrado em sua conta para que o processo de alteração de senha seja realizado. Fale com o administrator do sistema.');

            $hash = $this->hashifyIt($dados);

            $temRegistroAtivo = new PasswordRecovery();
            $temRegistroAtivo->setIdUsuarios($dados->getId());
            $temRegistroAtivo->setAtivo(1);
            $registros = $temRegistroAtivo->filtrarObjeto();

            foreach ($registros as $registro) {
                $desativar = new PasswordRecovery();
                $desativar->setId($registro->getId());
                $desativar->setAtivo(0);
                $desativar->salvar();
            }

            $esqueceuSenha = new PasswordRecovery();
            $esqueceuSenha->setAtivo(1);
            $esqueceuSenha->setIdUsuarios($dados->getId());
            $esqueceuSenha->setToken($hash);
            $esqueceuSenha->salvar();

            $this->logRegister($post,'enviar_nova_senha');

            $email = new Email();
            $email->assunto('[Soulu Manager] Alteração de senha');
            $email->para($dados->getEmail(),$dados->getNome());
            $email->body("
            <h2>Processo de alteração de senha</h2>
            <p>Clique <a href='{$_SERVER['HTTP_HOST']}/manager/user/processo-alteracao-senha/{$hash}'>aqui</a> para iniciar o processo.</p>
            <br>
            <small>Administração Soulu Manager</small>
            ");
            $email->enviar();

            $this->addSuccessMessage('Link para alteração de senha enviado por e-mail.');

            $this->redirect()->toRoute('navegacao',array('controller'=>'user','action'=>'entrar'));
            return false;
        }catch (\Exception $e){
            $this->setPost($post);
            $this->addErrorMessage($e->getMessage());
            $this->redirect()->toRoute('navegacao',array('controller'=>$controller,'action'=>'entrar'));
            return false;
        }
    }

    public function processoAlteracaoSenhaAction()
    {
        try{
            $this->layout('layout-login');

            $controller = str_replace("manager-", "", $this->params('controller'));
            $request = $this->getRequest();

            $dados = [];

            if($this->params('id')){
                $dados['Hash'] = $this->params('id');
            }else{
                $this->addErrorMessage('Não foi possível encontrar o Hash para o processamento. Solicite a alteração de senha novamente.');
                $this->redirect()->toRoute('navegacao',array('controller'=>'user','action'=>'esqueceu-a-senha'));
                return false;
            }

            if($request->isPost()){
                $dados = $request->getPost()->toArray();
                if(!$dados['Hash']){
                    $this->addErrorMessage('Não foi possível encontrar o Hash para o processamento. Solicite a alteração de senha novamente.');
                    $this->redirect()->toRoute('navegacao',array('controller'=>'user','action'=>'esqueceu-a-senha'));
                    return false;
                }

                $esqueceuSenha = new PasswordRecovery();
                $esqueceuSenha->setToken($dados['Hash']);
                $esqueceuSenha->setAtivo(1);
                $ret = $esqueceuSenha->filtrarObjeto();

                if(!count($ret)){
                    $this->addErrorMessage('Não foi possível encontrar o Hash para o processamento. Solicite a alteração de senha novamente.');
                    $this->redirect()->toRoute('navegacao',array('controller'=>'user','action'=>'esqueceu-a-senha'));
                    return false;
                }
                $user = '';
                foreach ($ret as $item) {
                    $user = $item->getIdUsuarios();
                    $item->setAtivo(0);
                    $item->salvar();
                }

                $user = new User();
                $dados = $user->validatePass($dados);

                $user->setId($user);
                $user->setSenha($dados["Senha"]);
                $user->setAlteradoPor('Administrador Soulu Manager');
                $user->salvar();

                $this->addSuccessMessage('Senha alterada com sucesso.');
                $this->redirect()->toRoute('navegacao',array('controller'=>'user','action'=>'entrar'));
                return false;
            }


            return new ViewModel(['dados'=>$dados]);

        }catch (\Exception $e){
            $this->setPost($dados);
            $this->addErrorMessage($e->getMessage());
            $this->redirect()->toRoute('navegacao',array('controller'=>$controller,'action'=>'processo-alteracao-senha','id'=>$dados['Hash']));
            return false;
        }
    }

    public function gerarSenhaAction()
    {
        $id = $this->params('id');
        $service = new User();
        debug($service->hashifyPass($id));
    }

    public function sideBarToggleAction()
    {
        try{
            $request = $this->getRequest();
            if($request->isPost()){
                $dados = $request->getPost()->toArray();
                $toggle = $dados['toggle'];

                $sessao = new \Zend\Session\Container('autenticacao');
                $sessao->autenticacao['sideBarToggleOpen'] = $toggle;
            }

        }catch(\Exception $e){
            return new JsonModel(['error'=>true,'message'=>'Erro ao executar requisição sideBarToggle.']);
        }

        return new JsonModel(['error'=>false,'message'=>'','dados'=>[]]);
    }

    public function lockScreenAction()
    {
        $this->layout('layout-login');
        try{
            $controller = str_replace("manager-", "", $this->params('controller'));
            $request = $this->getRequest();

            $user = new User();

            if($request->isPost()){
                $post = $request->getPost()->toArray();
                $user->lockIt($post['historyUrl']);
            }else{
                $user->lockIt('/');
            }

            $form = new \Manager\Form\User();

        }catch (\Exception $e){
            $this->addErrorMessage($e->getMessage());
            $this->redirect()->toRoute('navegacao',array('controller'=>$controller,'action'=>''));
            return false;
        }

        return new ViewModel(['form'=>$form]);
    }

    public function unlockScreenAction()
    {
        $this->layout('layout-login');
        try{
            $controller = str_replace("manager-", "", $this->params('controller'));
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos.');
            }

            $post = $request->getPost()->toArray();

            if(!isset($post['Login']) || !isset($post['Password'])) throw new \Exception('Dados não enviados!');

            $validate = new User();
            $validate->validateUnlockScreen($post);
            $redirect = $validate->unlockIt();
            unset($post['Password']);

        }catch (\Exception $e){
            $this->addErrorMessage($e->getMessage());
            if($e->getMessage() == 'Sessão expirada.'){
                $this->redirect()->toUrl('/user/entrar');
                return false;
            }
            $this->redirect()->toUrl("/{$controller}/lock-screen");
            return false;
        }
        $this->redirect()->toUrl($redirect);
    }

}
