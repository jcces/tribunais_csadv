<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manager\Controller;

use Estrutura\Controller\AbstractCrudController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AbstractController extends AbstractCrudController
{

    public function formAction()
    {
        try{
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos');
            }

            $post = $request->getPost()->toArray();

            if(!$post) throw new \Exception('Form não enviado!');

            $form = str_replace('Service','Form',$post['service']);
            $service = $post['service'];
            $id = $post['id'];
            $parent = $post['parent'];
            $table = $post['table'];
            $callback = $post['callback'];
            $fieldRel = $post['dataRelField'];

            $form = new $form();
            if($id){
                $service = new $service;
                $service->setId($id);
                $service->load();

                if(method_exists($service,'getObjRelationship')) $service->getObjRelationship();
                $form->setData($service->toArray());

                $service = '\\'.get_class($service);
            }

            $form->get($fieldRel)->setValue($parent);
            $view = new ViewModel(['form'=>$form,'service'=>$service,'table'=>$table,'id'=>$id,'parent'=>$parent,'callback'=>$callback,'relField'=>$fieldRel]);
            $view->setTerminal(true);
            return $view;

        }catch (\Exception $e){
            return new JsonModel(['error'=>true,'message'=>$e->getMessage()]);
        }
    }

    public function saveAction()
    {
        try{
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos');
            }

            $post = $request->getPost()->toArray();

            if(!$post) throw new \Exception('Form não enviado!');

            $service = $post['service'];
            $form = str_replace('Service','Form',$service);

            $service = new $service();
            $form = new $form();

            $form->setData($post);

            if(!$form->isValid()) {
                $msgs_tratadas = $this->getValidateMessages($form,'string');
                throw new \Exception($msgs_tratadas);
            }

            $service->exchangeArray($form->getData());
            $service->salvar();

            if(method_exists($service,'buildObjToReturn')) $service->buildObjToReturn();

            return new JsonModel(['error'=>false,'message'=>'Registro salvo com sucesso!','dados'=>$service->toArray()]);

        }catch (\Exception $e){
            return new JsonModel(['error'=>true,'message'=>$e->getMessage()]);
        }
    }

    public function excluirAction()
    {
        try{
            $request = $this->getRequest();

            if(!$request->isPost()){
                throw new \Exception('Dados Inválidos');
            }

            $post = $request->getPost()->toArray();

            if(!$post) throw new \Exception('Form não enviado!');

            $service = $post['service'];
            if(!$post['id']) throw new \Exception('Id não informado.');
            $id = $post['id'];
            $serviceObj = new $service();
            $serviceObj->setId($id);
            $serviceObj->excluir();

            return new JsonModel(['error'=>false,'message'=>'Item excluído com sucesso.','dados'=>['Id'=>$id]]);

        }catch (\Exception $e){
            return new JsonModel(['error'=>true,'message'=>$e->getMessage()]);
        }
    }

    public function selectInsertAction(){
        $post = $this->getRequest()->getPost();
        $service = new $post['service'];
        $form = $service->getForm();
        $data = json_decode($post['dataPadrao'],true);
        $form->setData($data);
        $view = new ViewModel(['form'=>$form,'service'=>$post['service'],'callbackField'=>$post['calbackField']]);
        $view->setTerminal(true);
        return $view;
    }

    public function saveSelectInsertAction(){
        $post = $this->getRequest()->getPost();
        $service = new $post['serviceSend'];
        $service->exchangeArray($post);
        $service->salvar();
        return new JsonModel(['callback'=>$post['callbackFiled'],'data'=>['id'=>$service->getId(),'nome'=>$service->getName()]]);
    }

}
