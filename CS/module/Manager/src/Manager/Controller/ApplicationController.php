<?php

namespace Manager\Controller;

use Estrutura\Controller\AbstractEstruturaController;
use Estrutura\Service\Email;
use Estrutura\Service\ExcelIO;
use Estrutura\Service\OpenLayers;
use Manager\Service\FontAwesome;
use Tribunal\Service\Tribunal;
use Zend\View\Model\ViewModel;

class ApplicationController extends AbstractEstruturaController
{

    public function indexAction()
    {
        $tribunal = new Tribunal();
        $tribunais = $tribunal->getTotais();
        $processosProcessados = $tribunal->getTotaisProcessados();
        return new ViewModel(['totais' => $tribunais, 'processados' => $processosProcessados]);
    }

}
