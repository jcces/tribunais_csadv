<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manager\Controller;

use Estrutura\Controller\AbstractEstruturaController;
use Manager\Service\Log;
use Zend\View\Model\ViewModel;

class LogController extends AbstractEstruturaController
{

    public function indexAction()
    {
        $service = new Log();
        $lista = $service->fetchAll();
        return new ViewModel(['lista'=>$lista]);
    }

}
