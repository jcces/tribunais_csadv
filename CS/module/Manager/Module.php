<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manager;

use Manager\Service\Group;
use Manager\Service\User;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Estrutura\Service\AbstractEstruturaService;
use Estrutura\Form\AbstractForm;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $moduleManager = $e->getApplication()->getServiceManager()->get('modulemanager');
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach('Zend\Mvc\Controller\AbstractController', MvcEvent::EVENT_DISPATCH, array($this, 'controllerDispatch'), 100);
    }

    /**
     * @param MvcEvent $e
     * @return null|\Zend\Http\PhpEnvironment\Response
     */
    public function controllerDispatch(MvcEvent $e)
    {

        AbstractEstruturaService::setServiceManager($e->getTarget()->getServiceLocator());
        AbstractForm::setServiceManager($e->getTarget()->getServiceLocator());
        $locator = $e->getTarget()->getServiceLocator();
        $route = $e->getTarget()->getEvent()->getRouteMatch()->getParams();
        $controller = $e->getTarget();

        /// Rota de Console
        if ($controller->getRequest() instanceof \Zend\Console\Request) return true;

        $route['controller'] = str_replace("manager-", "", $route['controller']);

        if ($route['controller'] == 'user' && $route['action'] == 'gerarSenha') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'entrar') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'sair') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'autenticar') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'alterar-senha') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'esqueceu-a-senha') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'enviar-nova-senha') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'processo-alteracao-senha') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'lock-screen') return true;
        if ($route['controller'] == 'user' && $route['action'] == 'unlock-screen') return true;
        if ($route['controller'] == 'acesso-negado' && $route['action'] == 'index') return true;
        if ($route['controller'] == 'api') return true;

        /// Autenticação
        if ($route['controller'] == 'ws') {
            try {
                $header = $controller->getRequest()->getHeaders()->toArray();

                if (isset($header['Access-Control-Request-Method']) && $header['Access-Control-Request-Method'] == 'POST') return true;

                if (!isset($header['Token'])) throw new \Exception('Token não enviado!');
                $token = $header['Token'];
                if ($token != 'teste-token') throw new \Exception('Token inválido!');
            } catch (\Exception $e) {
                echo json_encode(['error' => true, 'message' => $e->getMessage()]);
                die;
            }

            return true;
        }

        $sessao = new Container('autenticacao');

        if (isset($sessao->autenticacao['lock'])) {
            if ($sessao->autenticacao['lock'] && $route['controller'] != 'user' && $route['action'] != 'lock-screen') {
                $controller->plugin('redirect')->toUrl('/user/lock-screen');
                $e->stopPropagation();
                return false;
            }
        }

        if (!$sessao->autenticacao) {
            $sessao = new Container('LoginRedirect');
            $sessao->offsetSet('url', $_SERVER['REQUEST_URI']);

            if (isset($header['X-Requested-With']) && $header['X-Requested-With'] == 'XMLHttpRequest') {
                header('HTTP/1.1 999 session timeout');
                header('Content-Type: application/json; charset=UTF-8');
                echo json_encode(['error' => true, 'message' => 'Conecte-se para utilizar o sistema.']);
                die;
            }

            $controller->addErrorMessage('Conecte-se para utilizar o sistema.');
            $controller->plugin('redirect')->toUrl('/user/entrar');
            $e->stopPropagation();
            return false;
        } else {
            if (!$this->checkPermission($sessao->autenticacao, $route, $controller)) {
                $e->stopPropagation();
                return false;
            }
        }
    }

    public function checkPermission($sessao, $route, $controller)
    {
        return true;

        $routeOriginal = $route;
        $routeOriginal['controller'] = str_replace("manager-", "", $routeOriginal['controller']);
        $route['controller'] = str_replace(" ", "", ucwords(str_replace("-", " ", $route['controller'])));
        $permission = (isset($sessao[$route['controller']])) ? $sessao[$route['controller']] : 0;
        if (in_array($route['controller'], ['Attach', 'Abstract'])) $permission = 2;
        $actions = ['gravar', 'salvar', 'excluir'];
        if ($permission == 0) {
            $controller->plugin('redirect')->toUrl('/acesso-negado');
            return false;
        } else {
            if ($permission == 1 && in_array($route['action'], $actions)) {
                //throw new \Exception('Você não tem permissão para executar essa operação. Favor entrar em contato com o administrador do sistema.');
                $controller->addErrorMessage('Você não tem permissão para executar essa operação. Favor entrar em contato com o administrador do sistema.');
                $controller->plugin('redirect')->toUrl('/' . $routeOriginal['controller']);
                return false;
            } else {
                return true;
            }
        }
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(
                'Acl' => '\Manager\View\Helper\Acl',
                'UsuarioLogado' => '\Manager\View\Helper\UsuarioLogado',
                'Projeto' => '\Manager\View\Helper\Projeto',
                'ViewDocument' => '\Manager\View\Helper\ViewDocument',
            ),
        );
    }


}
