<?php
return array(
    'router' => array(
        'routes' => array(
            'navegacao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/[/:controller[/:action[/:id]]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => '',
                        'action'     => '',
                    ),
                ),
            ),
            'home-manager' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/[:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'home',
                        'action'     => 'index',
                    ),
                ),
            ),
            'user' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/user[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'user',
                        'action'     => 'index',
                    ),
                ),
            ),
            'anexos' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/anexos[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'anexos',
                        'action'     => 'index',
                    ),
                ),
            ),
            'acesso-negado' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/acesso-negado[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'acesso-negado',
                        'action'     => 'index',
                    ),
                ),
            ),
            'log' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/log[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'log',
                        'action'     => 'index',
                    ),
                ),
            ),
            'abstract' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/abstract[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'abstract',
                        'action'     => 'index',
                    ),
                ),
            ),
            'ws' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/ws[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'ws',
                        'action'     => 'index',
                    ),
                ),
            ),
            'manutencao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/manutencao[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'manager-manutencao',
                        'action'     => 'manutencao',
                    ),
                ),
            ),
            'api' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/api[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'api',
                        'action'     => 'index',
                    ),
                ),
            ),
            'download' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/download[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'download',
                        'action'     => 'index',
                    ),
                ),
            ),
            'group' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/group[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'group',
                        'action'     => 'index',
                    ),
                ),
            ),
            'console' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/console[/:action[/:id]]',
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'console',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'module_layouts' => array(
        'Manager' => 'layout',
    ),
    'controllers' => array(
        'invokables' => array(
            'home' => 'Manager\Controller\ApplicationController',
            'user' => 'Manager\Controller\UserController',
            'acesso-negado' => 'Manager\Controller\AcessoNegadoController',
            'log' => 'Manager\Controller\LogController',
            'manutencao' => 'Manager\Controller\ManutencaoController',
            'abstract' => 'Manager\Controller\AbstractController',
            'ws' => 'Manager\Controller\WsController',
            'anexos' => 'Manager\Controller\AttachController',
            'console' => 'Manager\Controller\ConsoleController',
            'api' => 'Manager\Controller\ApiController',
            'download' => 'Manager\Controller\DownloadController',
            'group' => 'Manager\Controller\GroupController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'error/layout'           => __DIR__ . '/../view/error/error-layout.phtml',
            'layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/clear'           => __DIR__ . '/../view/layout/layout-clear.phtml',
            'header'           => __DIR__ . '/../view/layout/header.phtml',
            'page-header'           => __DIR__ . '/../view/layout/page-header.phtml',
            'footer'           => __DIR__ . '/../view/layout/footer.phtml',
            'leftsidebar'           => __DIR__ . '/../view/layout/leftsidebar.phtml',
            'modals'           => __DIR__ . '/../view/layout/modals.phtml',
            'controlsidebar'           => __DIR__ . '/../view/layout/controlsidebar.phtml',
            'layout-login'           => __DIR__ . '/../view/layout/login.phtml',
            'risk-project-modals'           => __DIR__ . '/../view/manager/risk-project/risk-project-modals.phtml',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(

            ),
        ),
    ),
);