<?php

return array(
    'router' => array(
        'routes' => array(
            'site-home' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/site[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'site',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'site' => 'Site\Controller\SiteController'
        ),
    ),
    'module_layouts'=>[
        'Site'=>'layout/site'
    ],
    'view_manager'=>array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            'layout/site'               => __DIR__ . '/../view/layout/layout-site.phtml',
        )
    ),
);
