<?php

namespace Site\Controller;

use Estrutura\Controller\AbstractEstruturaController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractEstruturaController
{
    public function indexAction()
    {
        return new ViewModel();
    }
}
